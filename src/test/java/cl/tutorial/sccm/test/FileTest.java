package cl.tutorial.sccm.test;

/*import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.DAP;
import cl.tutorial.sccm.domain.TotalesBonos;
import cl.tutorial.sccm.service.dto.DapDto;
import cl.tutorial.sccm.service.dto.TotalesBonosDto;
import cl.tutorial.sccm.service.mapper.DapMapper;
import cl.tutorial.sccm.service.mapper.TotalesBonosMapper;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import static java.time.temporal.ChronoUnit.DAYS;*/

public class FileTest {

    /*@Test
    public void shouldMapDapToDapDto() {
        // given
        final LocalDate localDate = LocalDate.of(2018, 8, 5);
        final LocalDate localDate2 = LocalDate.of(2018, 12, 25);
        //        Double df = 34.7;
        //        df.equals(0);

        DAP car = new DAP(localDate, localDate2, localDate, localDate2, 0.0, 81585241.66, 81585241.56, 449265.0, 0.0, 20.5, 449265.0, new BigInteger("59"), BigInteger.ZERO,
            BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE, new BigInteger("77413730"), BigInteger.ONE, BigInteger.ONE, new BigInteger("392"), "10290407450",
            new BigDecimal("449265"), new Double("82034506"), BigInteger.ZERO, BigInteger.ZERO, new BigInteger("2310001"), BigInteger.ZERO, new BigInteger("360"), BigInteger.ZERO,
            new BigInteger("2310003"), "0", "F", "S", "N", "0", "N", "NO", "2", "N", "0", "FCC", new AnnoTributa(5L, 2018, 2017));

        // when
        DapDto dapDto = DapMapper.INSTANCE.dapToDapDto(car);
        System.out.println(dapDto);

        // then
        Assert.assertNotNull(dapDto);
        Assert.assertEquals(dapDto.getFecVcto(), "05/08/2018");
        Assert.assertEquals(dapDto.getCapital(), "81585241,6600");
        Assert.assertEquals(dapDto.getImpExt(), "0");
    }

    @Test
    public void shouldMapTotalesBonosToTotalesBonosDto() {
        // given
        final ZonedDateTime localDate = ZonedDateTime.now();
        final ZonedDateTime localDate2 = localDate.minusDays(45);

        TotalesBonos car = new TotalesBonos(new Long("2543"), new Long("5"), localDate, localDate2, localDate, "BICETE0705", 998L, new BigDecimal("0"), new BigDecimal("54.566"),
            "CRAWFORD" + " CARVALLO S.A.", new Long("2"), "ADM", "96879310-1", 10L, 3L, new BigDecimal("10000000"));

        // when
        TotalesBonosDto dapDto = TotalesBonosMapper.INSTANCE.totalesBonosToTotalesBonosDto(car);
        System.out.println(dapDto);

        // then
        Assert.assertNotNull(dapDto);
        Assert.assertEquals(dapDto.getMontoPagadoMo(), "0");
        Assert.assertEquals(dapDto.getFechaInversion(), "02/07/2018");
        Assert.assertEquals(dapDto.getFechaPago(), "16/08/2018");
        Assert.assertEquals(dapDto.getMontoPagadoValor(), "54,5660");
        Assert.assertEquals(dapDto.getRutBeneficiario(), "96879310-1");
        //        Assert.assertEquals(dapDto.getFechaEvento(), DateFormat.getDateInstance(DateFormat.SHORT));
    }

    @Test
    public void BigDecimalAndDoubleCompare() {
        String value = "158888";
        Double doubleValue = Double.valueOf(value);
        BigDecimal bigDecimalValue = new BigDecimal(value);
        System.out.println((bigDecimalValue.subtract(BigDecimal.valueOf(doubleValue))));
        System.out.println(BigDecimal.ZERO);
        Assert.assertTrue((bigDecimalValue.subtract(BigDecimal.valueOf(doubleValue))).equals(BigDecimal.ZERO));
        // Assert.assertTrue(Objects.equals(doubleValue, bigDecimalValue));

    }

    @Test
    public void LocalDateCompare() {
        LocalDate fechaPago1 = LocalDate.parse("2017-10-06");
        LocalDate fechaInv1 = LocalDate.parse("2017-09-25");
        LocalDate fechaPago2 = LocalDate.parse("2017-07-28");
        LocalDate fechaInv2 = LocalDate.parse("2017-07-03");
        LocalDate fechaPago3 = LocalDate.parse("2017-06-09");
        LocalDate fechaInv3 = LocalDate.parse("2017-06-08");
        final long between1 = DAYS.between(fechaInv1, fechaPago1);
        final long between2 = DAYS.between(fechaInv2, fechaPago2);
        final long between3 = DAYS.between(fechaInv3, fechaPago3);
        System.out.println(between1 <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue());
        System.out.println(between2 <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue());
        System.out.println(between3 <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue());
        Assert.assertFalse(between1 <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue());
        Assert.assertFalse(between2 <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue());
        Assert.assertTrue(between3 <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue());
        // Assert.assertTrue(Objects.equals(doubleValue, bigDecimalValue));

    }

    @Test
    public void executeBatFile() {
        final BatRunner batRunner = new BatRunner();
        String batfile = "run_jar.bat";
        String directory = "C:\\TONY\\SOURCE_TREE\\sccm_Oficial\\JAR\\PAGO_DIVIDENDO\\GENERA_DJ";
        Boolean runProcess = null;
        try {
            runProcess = batRunner.runProcess(batfile, directory);
        } catch (CommandLineException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(runProcess);
        Assert.assertTrue(runProcess);
    }*/

}
