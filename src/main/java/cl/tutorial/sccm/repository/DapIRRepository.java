package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.DapIR;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the DapIR entity.
 */
@Repository
public interface DapIRRepository extends JpaRepository<DapIR, BigInteger> {

    List<DapIR> findAllByAnnoTributa(BigInteger annoId);

    Page<DapIR> findAllByAnnoTributa(BigInteger annoId, Pageable pageable);

    Page<DapIR> findAllByAnnoTributaAndCodMon(BigInteger annoId, Long codMon, Pageable pageable);

}
