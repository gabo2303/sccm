package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CorrecMoneta;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CorrecMoneta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CorrecMonetaRepository extends JpaRepository<CorrecMoneta, Long> {

}
