package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CertPagdiv;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the CertPagdiv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CertPagdivRepository extends JpaRepository<CertPagdiv, Long> {
    List<CertPagdiv> findAllByAnnoTributaId(@Param("annoId") Long annoId);
}
