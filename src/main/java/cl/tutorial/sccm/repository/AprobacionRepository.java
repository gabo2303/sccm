package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.Aprobacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Aprobacion entity.
 */
@Repository
public interface AprobacionRepository extends JpaRepository<Aprobacion, Long> {

    List<Aprobacion> findAllByProducto(String product);

    List<Aprobacion> findAllByAnnoTributaAndProducto(AnnoTributa annoTributa, String product);

    Long countByAnnoTributaAndAprobacionProductoAndCuadraturaContableAndInteresesPagados(AnnoTributa annoTributa,
        Boolean aprobacionProducto, Boolean cuadraturaContable, Boolean interesesPagados);
}
