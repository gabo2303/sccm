package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Cart57B;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Cart57B entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Cart57BRepository extends JpaRepository<Cart57B, Long> {

    List<Cart57B> findAllByAnnoTributaId(@Param("annoId") Long annoId);

}
