package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AhorroCC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Ahorro entity.
 */
@Repository
public interface AhorroCCRepository extends JpaRepository<AhorroCC, Long> {

    List<AhorroCC> findAllByAnnoTributa(@Param("annoId") Long annoId);

}
