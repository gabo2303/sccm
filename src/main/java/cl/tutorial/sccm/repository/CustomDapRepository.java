package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.DAP;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Custom repository for the DAP entity.
 */
public interface CustomDapRepository {

    Page<DAP> findByTaxYearIdAndDiffDaysAndPaymentIndicator(Long taxYearId, Integer diffDays, String paymentIndicator, Pageable pageable);

    List<DAP> findAllByTaxYearIdAndDiffDaysAndPaymentIndicator(Long taxYearId, Integer diffDays, String paymentIndicator);
}
