package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CabeceraControlCambio;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CabeceraControlCambio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CabeceraControlCambioRepository extends JpaRepository<CabeceraControlCambio, Long>, JpaSpecificationExecutor<CabeceraControlCambio> {

}
