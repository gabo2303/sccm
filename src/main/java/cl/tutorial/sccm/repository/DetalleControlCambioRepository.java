package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.DetalleControlCambio;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DetalleControlCambio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetalleControlCambioRepository extends JpaRepository<DetalleControlCambio, Long>, JpaSpecificationExecutor<DetalleControlCambio> {

}
