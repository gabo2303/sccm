package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.LHBonos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the LHBonos entity.
 */
@Repository
public interface LHBonosRepository extends JpaRepository<LHBonos, Long> {

    List<Object> findAllByAnnoTributaIdAndRut(Long taxYearId, BigInteger rut);

    List<Object> findAllByAnnoTributaIdAndRutAndFolio(Long taxYearId, BigInteger rut, String folio);

    List<Object> findAllByAnnoTributaIdAndFolio(Long taxYearId, String folio);
}
