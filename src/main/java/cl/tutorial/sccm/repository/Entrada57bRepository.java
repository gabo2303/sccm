package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Entrada57b;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import javax.persistence.QueryHint;
import java.util.List;


/**
 * Spring Data JPA repository for the Entrada57b entity.
 */
@Repository
public interface Entrada57bRepository extends JpaRepository<Entrada57b, Long> {

    List<Entrada57b> findAllByAnnoTributaId(@Param("annoId") Long annoId);
}

