package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Fut;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Fut entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FutRepository extends JpaRepository<Fut, Long> {

    List<Fut> findAllByAnnoTributaId(@Param("annoId") Long annoId);

}
