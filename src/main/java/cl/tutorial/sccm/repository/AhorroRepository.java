package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Ahorro;
import cl.tutorial.sccm.domain.AnnoTributa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the Ahorro entity.
 */
@Repository
public interface AhorroRepository extends JpaRepository<Ahorro, Long> {

    List<Ahorro> findAllByAnnoTributaId(@Param("annoId") Long annoId);

    Page<Ahorro> findAllByAnnoTributa(AnnoTributa annoTributa, Pageable pageable);

    List<Ahorro> findAllByAnnoTributa(AnnoTributa annoTributa);

    Page<Ahorro> findAllByAnnoTributaAndEstado(AnnoTributa annoTributa, String estado, Pageable pageable);

    List<Ahorro> findAllByAnnoTributaAndEstado(AnnoTributa annoTributa, String estado);

    Page<Ahorro> findAllByAnnoTributaAndNacionalidad(AnnoTributa annoTributa, String esExtranjero, Pageable pageable);

    List<Ahorro> findAllByAnnoTributaAndNacionalidad(AnnoTributa annoTributa, String esExtranjero);

    Page<Ahorro> findAllByAnnoTributaAndNumGiroGreaterThanAndReajPagadoAndIntPagado(AnnoTributa annoTributa, Integer numGiros, Double reajPagado, Double intPagado,
        Pageable pageable);

    List<Ahorro> findAllByAnnoTributaAndNumGiroGreaterThanAndReajPagadoAndIntPagado(AnnoTributa annoTributa, Integer numGiros, Double reajPagado, Double intPagado);

    Page<Ahorro> findAllByAnnoTributaAndNumGiroIn(AnnoTributa annoTributa, List<Integer> numGiros, Pageable pageable);

    List<Ahorro> findAllByAnnoTributaAndNumGiroIn(AnnoTributa annoTributa, List<Integer> numGiros);

    Page<Ahorro> findAllByAnnoTributaAndCapitalLessThanOrIntPagadoLessThan(AnnoTributa annoTributa, Double capital, Double intPagado, Pageable pageable);

    List<Ahorro> findAllByAnnoTributaAndCapitalLessThanOrIntPagadoLessThan(AnnoTributa annoTributa, Double capital, Double intPagado);

    @Query(value = "SELECT * FROM AHORRO WHERE ANNO_TRIBUTA_ID = :annoTributaId AND INT_REAL < INT_PAGADO ORDER BY ?#{#pageable}",
        countQuery = "SELECT count(*) FROM AHORRO WHERE ANNO_TRIBUTA_ID = :annoTributaId AND INT_REAL < INT_PAGADO ORDER BY ?#{#pageable}", nativeQuery = true)
    Page<Ahorro> findAllBy(@Param("annoTributaId") Long annoTributaId, Pageable pageable);

    @Query(value = "SELECT * FROM AHORRO WHERE ANNO_TRIBUTA_ID = :annoTributaId AND INT_REAL < INT_PAGADO", nativeQuery = true)
    List<Ahorro> findAllBy(@Param("annoTributaId") Long annoTributaId);

    List<Object> findAllByAnnoTributaIdAndRut(Long taxYearId, BigInteger rut);

    List<Object> findAllByAnnoTributaIdAndRutAndFolio(Long taxYearId, BigInteger rut, String folio);

    List<Object> findAllByAnnoTributaIdAndFolio(Long taxYearId, String folio);
}
