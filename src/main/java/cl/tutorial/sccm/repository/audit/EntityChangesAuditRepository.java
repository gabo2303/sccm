package cl.tutorial.sccm.repository.audit;

import cl.tutorial.sccm.common.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class EntityChangesAuditRepository {

    private final Logger log = LoggerFactory.getLogger(EntityChangesAuditRepository.class);

    private final EntityManager entityManager;

    public EntityChangesAuditRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    public List<?> getEntityChangesByTableNameAndDates(String tableName, LocalDate startDate, LocalDate endDate) {
        final String auditTableName = tableName.toUpperCase() + "_AUD";
        String qlString = "SELECT elem.*, revInfo.AUDITOR, revInfo.TIMESTAMP FROM " + auditTableName + " elem CROSS JOIN REVISIONS_INFO "
            + "revInfo WHERE elem.REV = revInfo.ID AND :startDate <= revInfo.TIMESTAMP AND :endDate >= revInfo.TIMESTAMP ORDER BY elem.REV ASC";

        Query q = entityManager.createNativeQuery(qlString, Tuple.class);

        LocalDateTime startLdt = LocalDateTime.of(startDate, LocalTime.MIN);
        Timestamp startTimestamp = Timestamp.valueOf(startLdt);

        LocalDateTime endLdt = LocalDateTime.of(endDate, LocalTime.MAX);
        Timestamp endTimestamp = Timestamp.valueOf(endLdt);

        q.setParameter("startDate", startTimestamp.getTime());
        q.setParameter("endDate", endTimestamp.getTime());

        final List<Tuple> resultList = q.getResultList();

        List<ObjectNode> json = _toJson(resultList);

        return json;
    }

    @Transactional(readOnly = true)
    public List<?> getEntityChangesByTableName(String tableName) {
        final String auditTableName = tableName.toUpperCase() + "_AUD";
        String qlString = "SELECT elem.*, revInfo.AUDITOR, revInfo.TIMESTAMP FROM " + auditTableName + " elem CROSS JOIN REVISIONS_INFO "
            + "revInfo WHERE elem.REV = revInfo.ID ORDER BY elem.REV ASC";

        Query q = entityManager.createNativeQuery(qlString, Tuple.class);

        final List<Tuple> resultList = q.getResultList();

        List<ObjectNode> json = _toJson(resultList);

        return json;
    }

    private List<ObjectNode> _toJson(List<Tuple> results) {

        List<ObjectNode> json = new ArrayList<ObjectNode>();

        ObjectMapper mapper = new ObjectMapper();

        for (Tuple t : results) {
            List<TupleElement<?>> cols = t.getElements();

            ObjectNode one = mapper.createObjectNode();

            for (TupleElement col : cols) {
                Object value = null;
                String name = col.getAlias() != null ? col.getAlias() : "";
                try {
                    value = t.get(col.getAlias());
                } catch (InvalidDataAccessApiUsageException | IllegalArgumentException ex) {
                    log.debug("Error buscando propiedad del resultset con valor NULL. " + ex.getMessage());
                }
                if (value == null) {
                    value = "";
                } else {

                    if (Constants.ENVERS_AUDIT_COLUMN_NAME_TIMESTAMP.equalsIgnoreCase(name)) {
                        value = DateFormatUtils.format(new Long(value.toString()), Constants.PATTERN_DATE_TIME_LONG).replace(Constants.ENVERS_AUDIT_ZERO_HOUR, "");
                    } else if (value instanceof Date) {
                        value = DateFormatUtils.format((Date) value, Constants.PATTERN_DATE_TIME_LONG).replace(Constants.ENVERS_AUDIT_ZERO_HOUR, "");
                    } else if (value instanceof java.sql.Date) {
                        value = DateFormatUtils.format((java.sql.Date) value, Constants.PATTERN_DATE_TIME_LONG).replace(Constants.ENVERS_AUDIT_ZERO_HOUR, "");
                    } else if (value instanceof Timestamp) {
                        value = DateFormatUtils.format((Timestamp) value, Constants.PATTERN_DATE_TIME_LONG).replace(Constants.ENVERS_AUDIT_ZERO_HOUR, "");
                    }

                    if (Constants.ENVERS_AUDIT_COLUMN_NAME_REVTYPE.equalsIgnoreCase(name)) {
                        value = evaluateChangeType(value.toString());
                    }
                }
                // Si el nombre de la columna contiene la palabra password se ignora para que no sea mostrado.
                if (!name.toUpperCase().contains(Constants.ENVERS_AUDIT_COLUMN_NAME_PASSWORD)) {
                    one.put(name, Objects.toString(value, ""));
                }
            }

            json.add(one);
        }

        return json;
    }

    private String evaluateChangeType(String integerChangeType) {
        if (integerChangeType == null || integerChangeType.trim().isEmpty()) {
            return "";
        }
        return Constants.ENVERS_CHANGE_TYPE_CREATE.equalsIgnoreCase(integerChangeType) ? "CREADO" :
            (Constants.ENVERS_CHANGE_TYPE_UPDATE.equalsIgnoreCase(integerChangeType) ? "MODIFICADO" :
                (Constants.ENVERS_CHANGE_TYPE_DELETE.equalsIgnoreCase(integerChangeType) ? "ELIMINADO" : ("ACCIÓN DESCONOCIDA [" + integerChangeType + "]")));
    }
}
