package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AhorroIR;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the Pactos entity.
 */
@Repository
public interface AhorroIRRepository extends JpaRepository<AhorroIR, BigInteger> {

    List<AhorroIR> findAllByAnnoTributa(BigInteger annoId);

}
