package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CargaMoneta;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CargaMoneta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CargaMonetaRepository extends JpaRepository<CargaMoneta, Long> {

}
