package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Temporal1890;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Temporal1890 entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Temporal1890Repository extends JpaRepository<Temporal1890, Long>, JpaSpecificationExecutor<Temporal1890> {

}
