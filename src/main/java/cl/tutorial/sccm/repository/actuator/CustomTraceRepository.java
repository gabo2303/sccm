package cl.tutorial.sccm.repository.actuator;

import cl.tutorial.sccm.domain.AuditServiceAccess;
import cl.tutorial.sccm.repository.AuditServiceAccessRepository;
import cl.tutorial.sccm.repository.mapper.AuditServiceAccessMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.trace.InMemoryTraceRepository;
import org.springframework.boot.actuate.trace.Trace;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Map;

@Repository
public class CustomTraceRepository extends InMemoryTraceRepository {

    private static final Logger log = LoggerFactory.getLogger(CustomTraceRepository.class);

    private final AuditServiceAccessRepository auditServiceAccessRepository;

    public CustomTraceRepository(AuditServiceAccessRepository auditServiceAccessRepository) {
        this.auditServiceAccessRepository = auditServiceAccessRepository;
        super.setCapacity(200);
    }

    @Override
    public void add(Map<String, Object> map) {
        super.add(map);
        Trace trace = new Trace(new Date(), map);
        AuditServiceAccess auditServiceAccess = AuditServiceAccessMapper.INSTANCE.traceToAuditServiceAccess(trace);
        this.auditServiceAccessRepository.save(auditServiceAccess);
        log.info("traced object: {}", auditServiceAccess);
    }
}
