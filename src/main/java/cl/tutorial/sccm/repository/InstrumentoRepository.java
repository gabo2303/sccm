package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Instrumento;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Instrumento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InstrumentoRepository extends JpaRepository<Instrumento, Long> {

}
