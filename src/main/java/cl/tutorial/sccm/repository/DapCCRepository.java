package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.DapCC;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the DapCC entity.
 */
@Repository
public interface DapCCRepository extends JpaRepository<DapCC, Long> {

    List<DapCC> findAllByAnnoTributaId(BigInteger idAnno);

    Page<DapCC> findAllByAnnoTributaId(BigInteger idAnno, Pageable pageable);

    Page<DapCC> findAllByAnnoTributaIdAndCodMoneda(BigInteger idAnno, Long currency, Pageable pageable);
}
