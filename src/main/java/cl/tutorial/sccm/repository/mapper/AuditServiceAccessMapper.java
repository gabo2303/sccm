package cl.tutorial.sccm.repository.mapper;

import cl.tutorial.sccm.domain.AuditServiceAccess;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.actuate.trace.Trace;

@Mapper
public interface AuditServiceAccessMapper {

    AuditServiceAccessMapper INSTANCE = Mappers.getMapper(AuditServiceAccessMapper.class);

    @Mappings(
        { @Mapping(expression = "java((String) elem.getInfo().get(\"method\"))", target = "method"),
            @Mapping(expression = "java((String) elem.getInfo().get(\"path\"))", target = "path"),
            @Mapping(expression = "java((String) elem.getInfo().get(\"remoteAddress\"))", target = "remoteAddress"),
            @Mapping(expression = "java( new java.lang.Long((String) elem.getInfo().get(\"timeTaken\")) )", target = "timeTaken"),
            @Mapping(expression = "java((String) elem.getInfo().get(\"userPrincipal\"))", target = "userPrincipal"),
            @Mapping(expression = "java((String) elem.getInfo().get(\"remoteUser\"))", target = "remoteUser"),
            @Mapping(expression = "java((String) elem.getInfo().get(\"query\"))", target = "query"), })
    AuditServiceAccess traceToAuditServiceAccess(Trace elem);
}
