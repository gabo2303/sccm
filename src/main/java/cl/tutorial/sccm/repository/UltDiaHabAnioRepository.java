package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.UltDiaHabAnio;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UltDiaHabAnio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UltDiaHabAnioRepository extends JpaRepository<UltDiaHabAnio, Long> {  }