package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.TablaMoneda;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TablaMoneda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TablaMonedaRepository extends JpaRepository<TablaMoneda, Long> {

}
