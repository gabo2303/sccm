package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Certi57BFcc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Certi57BFcc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Certi57BFccRepository extends JpaRepository<Certi57BFcc, Long> {

}
