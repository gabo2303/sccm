package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AnnoTributa;
import org.springframework.stereotype.Repository;

import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AnnoTributa entity.
 */
@Repository
public interface AnnoTributaRepository extends JpaRepository<AnnoTributa, Long> 
{
	List<AnnoTributa> findAllByAnnoActivo(String annoActivo);
	
	@Query("SELECT id, annoT, annoC, annoActivo FROM AnnoTributa WHERE id != :idFalso")
    public List<AnnoTributa> findAllByIdNotEqual(@Param("idFalso") Long idFalso);
}