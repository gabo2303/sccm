package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Euro;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Euro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EuroRepository extends JpaRepository<Euro, Long> {

}
