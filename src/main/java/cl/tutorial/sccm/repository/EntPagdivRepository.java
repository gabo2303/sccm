package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CertPagdiv;
import cl.tutorial.sccm.domain.EntPagdiv;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the EntPagdiv entity.
 @SuppressWarnings("unused")
 */
@Repository
public interface EntPagdivRepository extends JpaRepository<EntPagdiv, Long> {
    List<EntPagdiv> findAllByAnnoTributaId(@Param("annoId") Long annoId);

}
