package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.SalProxPer;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SalProxPer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SalProxPerRepository extends JpaRepository<SalProxPer, Long> {

}
