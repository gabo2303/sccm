package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.TotalProd;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TotalProd entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TotalProdRepository extends JpaRepository<TotalProd, Long> {

}
