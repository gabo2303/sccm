package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CertificadosAnteriores;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CertificadosAnteriores entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CertificadosAnterioresRepository extends JpaRepository<CertificadosAnteriores, Long> {

}
