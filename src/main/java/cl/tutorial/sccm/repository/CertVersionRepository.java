package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.CertVersion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CertVersion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CertVersionRepository extends JpaRepository<CertVersion, Long> {

}
