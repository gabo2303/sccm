package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.LetrasBonosCuadratura;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the LetrasBonosCuadratura entity.
 */
@Repository
public interface LetrasBonosCuadraturaRepository extends JpaRepository<LetrasBonosCuadratura, Long> {

    Page<LetrasBonosCuadratura> findAllByAnnoTributaId(Long idAnno, Pageable pageable);

    List<LetrasBonosCuadratura> findAllByAnnoTributaId(Long idAnno);
}
