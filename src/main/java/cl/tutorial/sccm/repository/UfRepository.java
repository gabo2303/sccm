package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Uf;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Uf entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UfRepository extends JpaRepository<Uf, Long> {

}
