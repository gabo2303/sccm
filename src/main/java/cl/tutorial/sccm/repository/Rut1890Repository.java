package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Rut1890;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.math.BigInteger;
import java.util.List;
/**
 * Spring Data JPA repository for the Rut1890 entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Rut1890Repository extends JpaRepository<Rut1890, Long> 
{
	List<Object> findAllByRut(Integer rut);
}