package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Glosa;
import cl.tutorial.sccm.domain.Representante;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Representante entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RepresentanteRepository extends JpaRepository<Representante, Long> {
    List<Representante> findAllByAnnoTributaIdAndInstrumentoId(@Param("annoId") Long annoId, @Param("instId") Long instId);
}
