package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Clientes1890mantencion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Clientes1890mantencion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Clientes1890mantencionRepository extends JpaRepository<Clientes1890mantencion, Long>, JpaSpecificationExecutor<Clientes1890mantencion> {

}
