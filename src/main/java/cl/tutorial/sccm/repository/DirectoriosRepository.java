package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Directorios;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Directorios entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirectoriosRepository extends JpaRepository<Directorios, Long> {

}
