package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.TipoAlerta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the TipoAlerta entity.
 */
@Repository
public interface TipoAlertaRepository extends JpaRepository<TipoAlerta, Long> {

    List<TipoAlerta> findAllByEsDap(Boolean isDap);
}
