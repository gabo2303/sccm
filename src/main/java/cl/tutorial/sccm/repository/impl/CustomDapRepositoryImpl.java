package cl.tutorial.sccm.repository.impl;

import cl.tutorial.sccm.domain.DAP;
import cl.tutorial.sccm.repository.CustomDapRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class CustomDapRepositoryImpl implements CustomDapRepository {

    private final EntityManager entityManager;

    public CustomDapRepositoryImpl(EntityManager entityManager) {this.entityManager = entityManager;}

    @Override
    public Page<DAP> findByTaxYearIdAndDiffDaysAndPaymentIndicator(Long taxYearId, Integer diffDays, String paymentIndicator, Pageable pageable) {
        return null;
    }

    @Override
    public List<DAP> findAllByTaxYearIdAndDiffDaysAndPaymentIndicator(Long taxYearId, Integer diffDays, String paymentIndicator) {

        Calendar calendar = new GregorianCalendar();
        // calendar.setTimeZone(TimeZone.getTimeZone("UTC+1"));//Munich time
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -diffDays); // substract the number of days to look back
        Date dateToLookBackAfter = calendar.getTime();

        String qlString = "SELECT d FROM DAP d WHERE d.annoTributa.id = :annoTributaId AND d.indPago = :indicadorPago AND d.fechaPago - d.fechaInv <= :dateToLookBackAfter";
        //        String qlString = "SELECT p FROM Podcast p where p.insertionDate > :dateToLookBackAfter";
        TypedQuery<DAP> query = entityManager.createQuery(qlString, DAP.class);
        query.setParameter("annoTributaId", taxYearId);
        query.setParameter("indicadorPago", paymentIndicator);
        query.setParameter("dateToLookBackAfter", dateToLookBackAfter, TemporalType.DATE);

        return query.getResultList();
    }
}
