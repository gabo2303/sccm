package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.TotalesBonos;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the TotalesBonos entity.
 */
@Repository
public interface TotalesBonosRepository extends JpaRepository<TotalesBonos, Long> {

    Page<TotalesBonos> findAllByAnnoTributaId(Long idAnno, Pageable pageable);

    List<TotalesBonos> findAllByAnnoTributaId(Long idAnno);

    List<TotalesBonos> findAllByAnnoTributaIdAndRutBeneficiario(Long idAnno, String rutBeneficiario);

    List<TotalesBonos> findAllByAnnoTributaIdAndRutBeneficiarioNot(Long idAnno, String rutBeneficiario);
}
