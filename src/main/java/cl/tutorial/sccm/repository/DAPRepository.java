package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.DAP;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the DAP entity.
 */
@Repository
public interface DAPRepository extends JpaRepository<DAP, Long> {

    Page<DAP> findAllByAnnoTributa(AnnoTributa annoTributa, Pageable pageable);

    List<DAP> findAllByAnnoTributa(AnnoTributa annoTributa);

    List<DAP> findAllByAnnoTributaId(@Param("annoId") Long annoId);

    Page<DAP> findAllByAnnoTributaAndTipPlazo(AnnoTributa annoTributa, String tipPlazo, Pageable pageable);

    @Query(value = "select id, anno_tributa_id, bipersonal, cap_mon, capital, cod_mon, cod_tipo_int, correlativo, cta_cap, cta_int, "
        + "cta_reaj, dv, est_cta, extranjero, fec_cont, fec_vcto, fecha_inv, fecha_pago, folio, imp_ext, ind_pago, int_nom, "
        + "int_pagado, int_real, mon_fi, mon_tasa, moneda, num_cupon_pago, num_cupones, num_giro, onp, origen, p_57_bis, per_tasa, "
        + "plazo, producto, reaj_pagado, reajustabilidad, rut, sucursal, tasa_op, tip_plazo, tipo_int from dap where anno_tributa_id=:annoTributaId and tip_plazo=:tipPlazo GROUP"
        + " BY id, anno_tributa_id, bipersonal, " + "cap_mon, capital, cod_mon, cod_tipo_int, correlativo, cta_cap, cta_int, cta_reaj, dv, est_cta, extranjero, fec_cont, "
        + "fec_vcto, fecha_inv, fecha_pago, folio, imp_ext, ind_pago, int_nom, int_pagado, int_real, mon_fi, mon_tasa, moneda, "
        + "num_cupon_pago, num_cupones, num_giro, onp, origen, p_57_bis, per_tasa, plazo, producto, reaj_pagado, reajustabilidad, "
        + "rut, sucursal, tasa_op, tip_plazo, tipo_int ORDER BY ?#{#pageable}, FOLIO, FECHA_INV", countQuery = "SELECT count(*) from dap where anno_tributa_id=:annoTributaId and  "
        + "tip_plazo=:tipPlazo GROUP BY id, anno_tributa_id, bipersonal, cap_mon, capital, cod_mon, cod_tipo_int, correlativo, cta_cap, "
        + "cta_int, cta_reaj, dv, est_cta, extranjero, fec_cont, fec_vcto, fecha_inv, fecha_pago, folio, imp_ext, ind_pago, "
        + "int_nom, int_pagado, int_real, mon_fi, mon_tasa, moneda, num_cupon_pago, num_cupones, num_giro, onp, origen, "
        + "p_57_bis, per_tasa, plazo, producto, reaj_pagado, reajustabilidad, rut, sucursal, tasa_op, tip_plazo, tipo_int  ORDER BY " + "?#{#pageable}, FOLIO, FECHA_INV",
        nativeQuery = true)
    Page<DAP> findAllByAnnoTributaAndTipPlazo1(@Param("annoTributaId") Long annoTributaId, @Param("tipPlazo") String tipPlazo, Pageable pageable);

    @Query(value = "SELECT d FROM DAP d WHERE d.annoTributa.id=:annoTributaId AND d.tipPlazo=:tipPlazo AND d.folio in "
        + "(SELECT d1.folio FROM DAP d1 WHERE d1.annoTributa.id=:annoTributaId and d1.tipPlazo=:tipPlazo GROUP BY d1.folio HAVING COUNT(d1.folio) > 1) "
        + "GROUP BY d.folio , d.id, d.annoTributa.id, d.bipersonal " + ", d.capMon, d.capital, d.codMon, d.codTipoInt"
        + ", d.correlativo, d.ctaCap, d.ctaInt, d.ctaReaj, d.dv, d.estCta "
        + ", d.extranjero, d.fecCont, d.fecVcto, d.fechaInv, d.fechaPago, d.impExt, d.indPago, d.intNom, d.intPagado " + ", d.intReal, d.monFi, d.monTasa, d.moneda, d.numCuponPago"
        + ", d.numCupones, d.numGiro, d.onp, d.origen, d.p57Bis " + ", d.perTasa, d.plazo, d.producto, d.reajPagado, d.reajustabilidad, d.rut, d.sucursal, d.tasaOp, d.tipPlazo "
        + ", d.tipoInt, d.interesNominalPesos, d.reajusteNegativo ORDER BY d.folio, d.fechaInv")
    List<DAP> findByAnnoTributaIdAndTipPlazoGroupByFolioBiggerThanOne(@Param("annoTributaId") Long annoTributaId, @Param("tipPlazo") String tipPlazo);

    Page<DAP> findAllByAnnoTributaAndIndPago(AnnoTributa annoTributa, String indPago, Pageable pageable);

    List<DAP> findAllByAnnoTributaAndIndPago(AnnoTributa annoTributa, String indPago);

    Page<DAP> findAllByAnnoTributaAndReajPagadoLessThanOrderByFecCont(AnnoTributa annoTributa, BigInteger reajPagado, Pageable pageable);

    List<DAP> findAllByAnnoTributaAndReajPagadoLessThanOrderByFecCont(AnnoTributa annoTributa, BigInteger reajPagado);

    Page<DAP> findAllByAnnoTributaAndP57Bis(AnnoTributa annoTributa, String p57Bis, Pageable pageable);

    List<DAP> findAllByAnnoTributaAndP57Bis(AnnoTributa annoTributa, String p57Bis);

    @Query(value = "SELECT d FROM DAP d WHERE d.annoTributa.id = :annoTributaId AND d.indPago = :indicadorPago AND d.fechaPago - d.fechaInv <= :diffDays ORDER BY "
        + "?#{#pageable}",
        countQuery = "SELECT count(d1) FROM DAP d1 WHERE d1.annoTributa.id = :annoTributaId AND d1.indPago = :indicadorPago AND d1.fechaPago - d1.fechaInv <= :diffDays ORDER BY "
            + "?#{#pageable}")
    /*@Query(value = "SELECT * FROM DAP d WHERE d.ANNO_TRIBUTA_ID = :annoTributaId AND d.IND_PAGO = :indicadorPago AND DATEDIFF(d.FECHA_PAGO, d.FECHA_INV) <= :diffDays ORDER BY "
        + "?#{#pageable}",
        countQuery = "SELECT count(*) FROM DAP WHERE ANNO_TRIBUTA_ID = :annoTributaId AND IND_PAGO = :indicadorPago AND DATEDIFF(FECHA_PAGO, FECHA_INV) <= :diffDays ORDER BY "
            + "?#{#pageable}", nativeQuery = true)*/
    Page<DAP> findAllBy(@Param("annoTributaId") Long annoTributaId, @Param("diffDays") LocalDate localDate, @Param("indicadorPago") String indicadorPago, Pageable pageable);

    @Query(value = "SELECT d FROM DAP d WHERE d.annoTributa.id = :annoTributaId AND d.indPago = :indicadorPago AND d.fechaPago - d.fechaInv <= :diffDays")
    List<DAP> findAllBy(@Param("annoTributaId") Long annoTributaId, @Param("diffDays") Integer diffDays, @Param("indicadorPago") String indicadorPago);

    List<Object> findAllByAnnoTributaIdAndRut(Long taxYearId, BigInteger rut);

    List<Object> findAllByAnnoTributaIdAndRutAndFolio(Long taxYearId, BigInteger rut, String folio);

    List<Object> findAllByAnnoTributaIdAndFolio(Long taxYearId, String folio);
}
