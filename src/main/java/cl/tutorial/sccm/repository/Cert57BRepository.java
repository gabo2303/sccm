package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Cert57B;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Cert57B entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Cert57BRepository extends JpaRepository<Cert57B, Long> {

    List<Cert57B> findAllByAnnoTributaId(@Param("annoId") Long annoId);
}
