package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Cliente1890;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Cliente1890 entity.
 */
//@SuppressWarnings("unused")
@Repository
public interface Cliente1890Repository extends JpaRepository<Cliente1890, Long> 
{
	Page<Cliente1890> findAll(Pageable pageable);	
}