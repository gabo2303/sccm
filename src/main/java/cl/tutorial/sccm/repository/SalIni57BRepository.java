package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.SalIni57B;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SalIni57B entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SalIni57BRepository extends JpaRepository<SalIni57B, Long> {

}
