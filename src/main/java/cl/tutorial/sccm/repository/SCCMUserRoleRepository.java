package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.SCCMUserRole;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SCCMUserRole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SCCMUserRoleRepository extends JpaRepository<SCCMUserRole, Long> {

}
