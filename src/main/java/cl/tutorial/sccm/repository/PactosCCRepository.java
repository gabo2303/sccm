package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.PactosCC;
import cl.tutorial.sccm.domain.PactosIR;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the Pactos entity.
 */
@Repository
public interface PactosCCRepository extends JpaRepository<PactosCC, Long> {

    List<PactosCC> findAllByAnnoTributa(@Param("annoId") Long annoId);

    //@Query("SELECT id,rut,dv,folio,cod_mon,fecha_pago,fecha_inv,int_real, int_real_cal,diferencia,fecha_cont from vw_pactos_int_real where anno_tributa_id=:annoId")
    //List<PactosIR> getIntRealPactos(@Param("annoId") Long annoId);

}
