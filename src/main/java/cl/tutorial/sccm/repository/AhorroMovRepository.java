package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AhorroMov;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AhorroMov entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AhorroMovRepository extends JpaRepository<AhorroMov, Long> {

}
