package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Contabilidad;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Contabilidad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContabilidadRepository extends JpaRepository<Contabilidad, Long> {

}
