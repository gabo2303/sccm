package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Rut_temp;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Rut_temp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Rut_tempRepository extends JpaRepository<Rut_temp, Long> {

}
