package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Usd;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Usd entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsdRepository extends JpaRepository<Usd, Long> {

}
