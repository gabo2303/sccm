package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Glosa;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Glosa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlosaRepository extends JpaRepository<Glosa, Long> {
    List<Glosa> findAllByAnnoTributaIdAndInstrumentoId(@Param("annoId") Long annoId, @Param("instId") Long instId);
}
