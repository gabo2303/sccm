package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.Pactos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the Pactos entity.
 */
@Repository
public interface PactosRepository extends JpaRepository<Pactos, Long> {

    List<Pactos> findAllByAnnoTributaId(@Param("annoId") Long annoId);

    List<Object> findAllByAnnoTributaIdAndRut(Long taxYearId, BigInteger rut);

    List<Object> findAllByAnnoTributaIdAndRutAndFolio(Long taxYearId, BigInteger rut, String folio);

    List<Object> findAllByAnnoTributaIdAndFolio(Long taxYearId, String folio);
}
