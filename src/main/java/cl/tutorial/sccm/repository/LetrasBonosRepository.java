package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.LetrasBonos;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the LetrasBonos entity.
 */
@Repository
public interface LetrasBonosRepository extends JpaRepository<LetrasBonos, Long> {

    Page<LetrasBonos> findAllByAnnoTributaId(Long idAnno, Pageable pageable);

    List<LetrasBonos> findAllByAnnoTributaId(Long idAnno);
}
