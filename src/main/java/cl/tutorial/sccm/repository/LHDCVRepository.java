package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.LHDCV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Spring Data JPA repository for the LHDCV entity.
 */
@Repository
public interface LHDCVRepository extends JpaRepository<LHDCV, Long> {

    List<Object> findAllByAnnoTributaIdAndRut(Long taxYearId, BigInteger rut);

    List<Object> findAllByAnnoTributaIdAndRutAndFolio(Long taxYearId, BigInteger rut, String folio);

    List<Object> findAllByAnnoTributaIdAndFolio(Long taxYearId, String folio);
}
