package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.AuditServiceAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AuditServiceAccess entity.
 */
@Repository
public interface AuditServiceAccessRepository extends JpaRepository<AuditServiceAccess, Long> {

}
