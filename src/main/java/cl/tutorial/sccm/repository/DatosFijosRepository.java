package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.DatosFijos;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DatosFijos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatosFijosRepository extends JpaRepository<DatosFijos, Long> {

}
