package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.ClienteBice;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClienteBice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClienteBiceRepository extends JpaRepository<ClienteBice, Long> {

}
