package cl.tutorial.sccm.repository;

import cl.tutorial.sccm.domain.UltDiaHabMes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the UltDiaHabMes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UltDiaHabMesRepository extends JpaRepository<UltDiaHabMes, Long> {  }