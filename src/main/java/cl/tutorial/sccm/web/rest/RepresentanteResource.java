package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Representante;
import cl.tutorial.sccm.service.RepresentanteService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.io.IOException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Representante.
 */
@RestController
@RequestMapping("/api")
public class RepresentanteResource 
{
    private final Logger log = LoggerFactory.getLogger(RepresentanteResource.class);

    private static final String ENTITY_NAME = "representante";

    private final RepresentanteService representanteService;	
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public RepresentanteResource(RepresentanteService representanteService, CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService) 
	{ 
		this.representanteService = representanteService; 
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
	}

    /**
     * POST  /representantes : Create a new representante.
     *
     * @param representante the representante to create
     * @return the ResponseEntity with status 201 (Created) and with body the new representante, or with status 400 (Bad Request) if the representante has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/representantes")
    @Timed
    public ResponseEntity<CabeceraControlCambio> createRepresentante(@RequestBody Representante representante) throws URISyntaxException, IOException 
	{
        log.debug("REST request to save Representante : {}", representante);
        
		if (representante.getId() != null) { throw new BadRequestAlertException("A new representante cannot already have an ID", ENTITY_NAME, "idexists"); }
		
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (CREAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("REPRESENTANTE_LEGAL");
		cabeceraControlCambio.setIdRegistro(new Long(0));		
		cabeceraControlCambio.setAccion("CREAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				
		DetalleControlCambio resultado = guardaCampo(result, "nombre", representante.getNombre(), "C");
		resultado = guardaCampo(result, "cargo", representante.getCargo(), "C");
		resultado = guardaCampo(result, "rut", representante.getRut(), "C");
		resultado = guardaCampo(result, "instrumento", String.valueOf(representante.getInstrumento().getId()), "L");
		resultado = guardaCampo(result, "annoTributa", String.valueOf(representante.getAnnoTributa().getId()), "L");
		
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(result);
		detalleControlCambio.setNombreCampo("firma");		
		detalleControlCambio.setTipoCampo("I");		
		detalleControlCambio.setImagen(representante.getFirma()); 
		
		resultado = detalleControlCambioService.save(detalleControlCambio);
		
		return ResponseEntity.created(new URI("/api/cabeceraControlCambio/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString()))
            .body(result);
    }
	
	/**
     * POST  /representantes : Create a new representante.
     *
     * @param representante the representante to create
     * @return the ResponseEntity with status 201 (Created) and with body the new representante, or with status 400 (Bad Request) if the representante has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/representantes/crear")
    @Timed
    public ResponseEntity<Representante> crearRepresentante(@RequestBody Representante representante) throws URISyntaxException 
	{
        log.debug("REST request to save Representante : {}", representante);
        
		if (representante.getId() != null) { throw new BadRequestAlertException("A new representante cannot already have an ID", ENTITY_NAME, "idexists"); }
		
        Representante result = representanteService.save(representante);
        
		return ResponseEntity.created(new URI("/api/representantes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /representantes : Updates an existing representante.
     *
     * @param representante the representante to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated representante,
     * or with status 400 (Bad Request) if the representante is not valid,
     * or with status 500 (Internal Server Error) if the representante couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/representantes")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateRepresentante(@RequestBody Representante representante) throws URISyntaxException 
	{
        log.debug("REST request to update Representante : {}", representante);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ACTUALIZAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("REPRESENTANTE_LEGAL");
		cabeceraControlCambio.setIdRegistro(representante.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				
		DetalleControlCambio resultado = guardaCampo(result, "nombre", representante.getNombre(), "C");
		resultado = guardaCampo(result, "cargo", representante.getCargo(), "C");
		resultado = guardaCampo(result, "rut", representante.getRut(), "C");
		resultado = guardaCampo(result, "instrumento", String.valueOf(representante.getInstrumento().getId()), "L");
		resultado = guardaCampo(result, "annoTributa", String.valueOf(representante.getAnnoTributa().getId()), "L");
		
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(result);
		detalleControlCambio.setNombreCampo("firma");		
		detalleControlCambio.setTipoCampo("I");		
		detalleControlCambio.setImagen(representante.getFirma()); 
		
		resultado = detalleControlCambioService.save(detalleControlCambio);
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("cabeceraControlCambio", result.getId().toString())).body(result);
    }
	
	/**
     * PUT  /representantes : Updates an existing representante.
     *
     * @param representante the representante to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated representante,
     * or with status 400 (Bad Request) if the representante is not valid,
     * or with status 500 (Internal Server Error) if the representante couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/representantes/actualizar")
    @Timed
    public ResponseEntity<Representante> actualizarRepresentante(@RequestBody Representante representante) throws URISyntaxException 
	{
        log.debug("REST request to update Representante : {}", representante);
        
        Representante result = representanteService.save(representante);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, representante.getId().toString())).body(result);
    }

    /**
     * GET  /representantes : get all the representantes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of representantes in body
     */
    @GetMapping("/representantes")
    @Timed
    public List<Representante> getAllRepresentantes() 
	{
        log.debug("REST request to get all Representantes");
        
		return representanteService.findAll();
    }

    /**
     * GET  /representantes/:id : get the "id" representante.
     *
     * @param id the id of the representante to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the representante, or with status 404 (Not Found)
     */
    @GetMapping("/representantes/{id}")
    @Timed
    public ResponseEntity<Representante> getRepresentante(@PathVariable Long id) 
	{
        log.debug("REST request to get Representante : {}", id);
        
		Representante representante = representanteService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(representante));
    }

    /**
     * DELETE  /representantes/:id : delete the "id" representante.
     *
     * @param id the id of the representante to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/representantes/{id}")
    @Timed
    public ResponseEntity<Void> deleteRepresentante(@PathVariable Long id) 
	{
        log.debug("REST request to delete Representante : {}", id);
        		
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("REPRESENTANTE_LEGAL");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /representantes/:id : delete the "id" representante.
     *
     * @param id the id of the representante to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/representantes/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarRepresentante(@PathVariable Long id) 
	{
        log.debug("REST request to delete Representante : {}", id);
        
		representanteService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	
    @GetMapping("/representantes/{idAnno}/{idInst}")
    @Timed
    public List<Representante>  findAllByAnnoTributaIdAndInstrumentoId(@PathVariable Long idAnno, @PathVariable Long idInst)
	{
        log.debug("REST request to reprocessE57B:");
		
        return  this.representanteService.findAllByAnnoTributaIdAndInstrumentoId(idAnno,idInst);
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}