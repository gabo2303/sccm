package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Glosa;
import cl.tutorial.sccm.service.GlosaService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Glosa.
 */
@RestController
@RequestMapping("/api")
public class GlosaResource 
{
    private final Logger log = LoggerFactory.getLogger(GlosaResource.class);

    private static final String ENTITY_NAME = "glosa";

    private final GlosaService glosaService;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public GlosaResource(GlosaService glosaService, CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService) 
	{
        this.glosaService = glosaService;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /glosas : Create a new glosa.
     *
     * @param glosa the glosa to create
     * @return the ResponseEntity with status 201 (Created) and with body the new glosa, or with status 400 (Bad Request) if the glosa has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/glosas")
    @Timed
    public ResponseEntity<CabeceraControlCambio> createGlosa(@Valid @RequestBody Glosa glosa) throws URISyntaxException 
	{
        log.debug("REST request to save Glosa : {}", glosa);
        
		if (glosa.getId() != null) { throw new BadRequestAlertException("A new glosa cannot already have an ID", ENTITY_NAME, "idexists"); }
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (CREAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("GLOSA");
		cabeceraControlCambio.setIdRegistro(new Long(0));		
		cabeceraControlCambio.setAccion("CREAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				
		DetalleControlCambio resultado = guardaCampo(result, "nombre", glosa.getNombre(), "C");
		resultado = guardaCampo(result, "valor", glosa.getValor(), "C");
		resultado = guardaCampo(result, "annoTributa", String.valueOf(glosa.getAnnoTributa().getId()), "L");
		resultado = guardaCampo(result, "instrumento", String.valueOf(glosa.getInstrumento().getId()), "L");
				        
		return ResponseEntity.created(new URI("/api/cabeceraControlCambio/" + result.getId()))
			   .headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString()))
               .body(result);
    }
	 
	/**
     * POST  /glosas : Create a new glosa.
     *
     * @param glosa the glosa to create
     * @return the ResponseEntity with status 201 (Created) and with body the new glosa, or with status 400 (Bad Request) if the glosa has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/glosas/crearGlosa")
    @Timed
    public ResponseEntity<Glosa> crearGlosa(@Valid @RequestBody Glosa glosa) throws URISyntaxException 
	{
        log.debug("REST request to save Glosa : {}", glosa);
        
		if (glosa.getId() != null) { throw new BadRequestAlertException("A new glosa cannot already have an ID", ENTITY_NAME, "idexists"); }
        
		Glosa result = glosaService.save(glosa);
        
		return ResponseEntity.created(new URI("/api/glosa/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /glosas : Updates an existing glosa.
     *
     * @param glosa the glosa to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated glosa,
     * or with status 400 (Bad Request) if the glosa is not valid,
     * or with status 500 (Internal Server Error) if the glosa couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/glosas")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateGlosa(@Valid @RequestBody Glosa glosa) throws URISyntaxException 
	{
        log.debug("REST request to update Glosa : {}", glosa);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ACTUALIZAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("GLOSA");
		cabeceraControlCambio.setIdRegistro(glosa.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				
		DetalleControlCambio resultado = guardaCampo(result, "nombre", glosa.getNombre(), "C");
		resultado = guardaCampo(result, "valor", glosa.getValor(), "C");
		resultado = guardaCampo(result, "annoTributa", String.valueOf(glosa.getAnnoTributa().getId()), "L");
		resultado = guardaCampo(result, "instrumento", String.valueOf(glosa.getInstrumento().getId()), "L");
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("cabeceraControlCambio", result.getId().toString())).body(result);
    }
	
	/**
     * PUT  /glosas : Updates an existing glosa.
     *
     * @param glosa the glosa to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated glosa,
     * or with status 400 (Bad Request) if the glosa is not valid,
     * or with status 500 (Internal Server Error) if the glosa couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/glosas/actualizar")
    @Timed
    public ResponseEntity<Glosa> actualizarGlosa(@Valid @RequestBody Glosa glosa) throws URISyntaxException 
	{
        log.debug("REST request to update Glosa : {}", glosa);
        
        Glosa result = glosaService.save(glosa);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, glosa.getId().toString())).body(result);
    }

    /**
     * GET  /glosas : get all the glosas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of glosas in body
     */
    @GetMapping("/glosas")
    @Timed
    public List<Glosa> getAllGlosas() 
	{
        log.debug("REST request to get all Glosas");
		
        return glosaService.findAll();
    }

    /**
     * GET  /glosas/:id : get the "id" glosa.
     *
     * @param id the id of the glosa to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the glosa, or with status 404 (Not Found)
     */
    @GetMapping("/glosas/{id}")
    @Timed
    public ResponseEntity<Glosa> getGlosa(@PathVariable Long id) 
	{
        log.debug("REST request to get Glosa : {}", id);
        
		Glosa glosa = glosaService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(glosa));
    }

    /**
     * DELETE  /glosas/:id : delete the "id" glosa.
     *
     * @param id the id of the glosa to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/glosas/{id}")
    @Timed
    public ResponseEntity<Void> deleteGlosa(@PathVariable Long id) 
	{
        log.debug("REST request to delete Glosa : {}", id);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("GLOSA");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /glosas/:id : delete the "id" glosa.
     *
     * @param id the id of the glosa to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/glosas/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarGlosa(@PathVariable Long id) 
	{
        log.debug("REST request to delete Glosa : {}", id);
        
		glosaService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/glosas/{idAnno}/{idInst}")
    @Timed
    public List<Glosa>  findAllByAnnoTributaIdAndInstrumentoId(@PathVariable Long idAnno, @PathVariable Long idInst){
        log.debug("REST request to reprocessE57B:");
        return  this.glosaService.findAllByAnnoTributaIdAndInstrumentoId(idAnno,idInst);
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);
		detalleControlCambio.setValorCampo(valorCampo);
		detalleControlCambio.setTipoCampo(tipoCampo);		
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}
