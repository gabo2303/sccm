package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.TablaMoneda;
import cl.tutorial.sccm.service.TablaMonedaService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TablaMoneda.
 */
@RestController
@RequestMapping("/api")
public class TablaMonedaResource {

    private final Logger log = LoggerFactory.getLogger(TablaMonedaResource.class);

    private static final String ENTITY_NAME = "tablaMoneda";

    private final TablaMonedaService tablaMonedaService;

    public TablaMonedaResource(TablaMonedaService tablaMonedaService) {
        this.tablaMonedaService = tablaMonedaService;
    }

    /**
     * POST  /tabla-monedas : Create a new tablaMoneda.
     *
     * @param tablaMoneda the tablaMoneda to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tablaMoneda, or with status 400 (Bad Request) if the tablaMoneda has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tabla-monedas")
    @Timed
    public ResponseEntity<TablaMoneda> createTablaMoneda(@RequestBody TablaMoneda tablaMoneda) throws URISyntaxException {
        log.debug("REST request to save TablaMoneda : {}", tablaMoneda);
        if (tablaMoneda.getId() != null) {
            throw new BadRequestAlertException("A new tablaMoneda cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TablaMoneda result = tablaMonedaService.save(tablaMoneda);
        return ResponseEntity.created(new URI("/api/tabla-monedas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tabla-monedas : Updates an existing tablaMoneda.
     *
     * @param tablaMoneda the tablaMoneda to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tablaMoneda,
     * or with status 400 (Bad Request) if the tablaMoneda is not valid,
     * or with status 500 (Internal Server Error) if the tablaMoneda couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tabla-monedas")
    @Timed
    public ResponseEntity<TablaMoneda> updateTablaMoneda(@RequestBody TablaMoneda tablaMoneda) throws URISyntaxException {
        log.debug("REST request to update TablaMoneda : {}", tablaMoneda);
        if (tablaMoneda.getId() == null) {
            return createTablaMoneda(tablaMoneda);
        }
        TablaMoneda result = tablaMonedaService.save(tablaMoneda);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tablaMoneda.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tabla-monedas : get all the tablaMonedas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tablaMonedas in body
     */
    @GetMapping("/tabla-monedas")
    @Timed
    public List<TablaMoneda> getAllTablaMonedas() {
        log.debug("REST request to get all TablaMonedas");
        return tablaMonedaService.findAll();
        }

    /**
     * GET  /tabla-monedas/:id : get the "id" tablaMoneda.
     *
     * @param id the id of the tablaMoneda to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tablaMoneda, or with status 404 (Not Found)
     */
    @GetMapping("/tabla-monedas/{id}")
    @Timed
    public ResponseEntity<TablaMoneda> getTablaMoneda(@PathVariable Long id) {
        log.debug("REST request to get TablaMoneda : {}", id);
        TablaMoneda tablaMoneda = tablaMonedaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tablaMoneda));
    }

    /**
     * DELETE  /tabla-monedas/:id : delete the "id" tablaMoneda.
     *
     * @param id the id of the tablaMoneda to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tabla-monedas/{id}")
    @Timed
    public ResponseEntity<Void> deleteTablaMoneda(@PathVariable Long id) {
        log.debug("REST request to delete TablaMoneda : {}", id);
        tablaMonedaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
