package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.SalProxPer;
import cl.tutorial.sccm.service.SalProxPerService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SalProxPer.
 */
@RestController
@RequestMapping("/api")
public class SalProxPerResource {

    private final Logger log = LoggerFactory.getLogger(SalProxPerResource.class);

    private static final String ENTITY_NAME = "salProxPer";

    private final SalProxPerService salProxPerService;

    public SalProxPerResource(SalProxPerService salProxPerService) {
        this.salProxPerService = salProxPerService;
    }

    /**
     * POST  /sal-prox-pers : Create a new salProxPer.
     *
     * @param salProxPer the salProxPer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salProxPer, or with status 400 (Bad Request) if the salProxPer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sal-prox-pers")
    @Timed
    public ResponseEntity<SalProxPer> createSalProxPer(@RequestBody SalProxPer salProxPer) throws URISyntaxException {
        log.debug("REST request to save SalProxPer : {}", salProxPer);
        if (salProxPer.getId() != null) {
            throw new BadRequestAlertException("A new salProxPer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SalProxPer result = salProxPerService.save(salProxPer);
        return ResponseEntity.created(new URI("/api/sal-prox-pers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sal-prox-pers : Updates an existing salProxPer.
     *
     * @param salProxPer the salProxPer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salProxPer,
     * or with status 400 (Bad Request) if the salProxPer is not valid,
     * or with status 500 (Internal Server Error) if the salProxPer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sal-prox-pers")
    @Timed
    public ResponseEntity<SalProxPer> updateSalProxPer(@RequestBody SalProxPer salProxPer) throws URISyntaxException {
        log.debug("REST request to update SalProxPer : {}", salProxPer);
        if (salProxPer.getId() == null) {
            return createSalProxPer(salProxPer);
        }
        SalProxPer result = salProxPerService.save(salProxPer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salProxPer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sal-prox-pers : get all the salProxPers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of salProxPers in body
     */
    @GetMapping("/sal-prox-pers")
    @Timed
    public List<SalProxPer> getAllSalProxPers() {
        log.debug("REST request to get all SalProxPers");
        return salProxPerService.findAll();
        }

    /**
     * GET  /sal-prox-pers/:id : get the "id" salProxPer.
     *
     * @param id the id of the salProxPer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salProxPer, or with status 404 (Not Found)
     */
    @GetMapping("/sal-prox-pers/{id}")
    @Timed
    public ResponseEntity<SalProxPer> getSalProxPer(@PathVariable Long id) {
        log.debug("REST request to get SalProxPer : {}", id);
        SalProxPer salProxPer = salProxPerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salProxPer));
    }

    /**
     * DELETE  /sal-prox-pers/:id : delete the "id" salProxPer.
     *
     * @param id the id of the salProxPer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sal-prox-pers/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalProxPer(@PathVariable Long id) {
        log.debug("REST request to delete SalProxPer : {}", id);
        salProxPerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
