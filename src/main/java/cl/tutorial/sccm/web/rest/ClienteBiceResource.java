package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.ClienteBice;
import cl.tutorial.sccm.service.ClienteBiceService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClienteBice.
 */
@RestController
@RequestMapping("/api")
public class ClienteBiceResource {

    private final Logger log = LoggerFactory.getLogger(ClienteBiceResource.class);

    private static final String ENTITY_NAME = "clienteBice";

    private final ClienteBiceService clienteBiceService;

    public ClienteBiceResource(ClienteBiceService clienteBiceService) {
        this.clienteBiceService = clienteBiceService;
    }

    /**
     * POST  /cliente-bices : Create a new clienteBice.
     *
     * @param clienteBice the clienteBice to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clienteBice, or with status 400 (Bad Request) if the clienteBice has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cliente-bices")
    @Timed
    public ResponseEntity<ClienteBice> createClienteBice(@RequestBody ClienteBice clienteBice) throws URISyntaxException {
        log.debug("REST request to save ClienteBice : {}", clienteBice);
        if (clienteBice.getId() != null) {
            throw new BadRequestAlertException("A new clienteBice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClienteBice result = clienteBiceService.save(clienteBice);
        return ResponseEntity.created(new URI("/api/cliente-bices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cliente-bices : Updates an existing clienteBice.
     *
     * @param clienteBice the clienteBice to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clienteBice,
     * or with status 400 (Bad Request) if the clienteBice is not valid,
     * or with status 500 (Internal Server Error) if the clienteBice couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cliente-bices")
    @Timed
    public ResponseEntity<ClienteBice> updateClienteBice(@RequestBody ClienteBice clienteBice) throws URISyntaxException {
        log.debug("REST request to update ClienteBice : {}", clienteBice);
        if (clienteBice.getId() == null) {
            return createClienteBice(clienteBice);
        }
        ClienteBice result = clienteBiceService.save(clienteBice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clienteBice.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cliente-bices : get all the clienteBices.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clienteBices in body
     */
    @GetMapping("/cliente-bices")
    @Timed
    public List<ClienteBice> getAllClienteBices() {
        log.debug("REST request to get all ClienteBices");
        return clienteBiceService.findAll();
        }

    /**
     * GET  /cliente-bices/:id : get the "id" clienteBice.
     *
     * @param id the id of the clienteBice to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clienteBice, or with status 404 (Not Found)
     */
    @GetMapping("/cliente-bices/{id}")
    @Timed
    public ResponseEntity<ClienteBice> getClienteBice(@PathVariable Long id) {
        log.debug("REST request to get ClienteBice : {}", id);
        ClienteBice clienteBice = clienteBiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clienteBice));
    }

    /**
     * DELETE  /cliente-bices/:id : delete the "id" clienteBice.
     *
     * @param id the id of the clienteBice to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cliente-bices/{id}")
    @Timed
    public ResponseEntity<Void> deleteClienteBice(@PathVariable Long id) {
        log.debug("REST request to delete ClienteBice : {}", id);
        clienteBiceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
