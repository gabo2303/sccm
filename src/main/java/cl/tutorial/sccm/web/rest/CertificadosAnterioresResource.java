package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.CertificadosAnteriores;
import cl.tutorial.sccm.service.CertificadosAnterioresService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.*;
import io.github.jhipster.web.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import java.io.*;

/**
 * REST controller for managing CertificadosAnteriores.
 */
@RestController
@RequestMapping("/api")
public class CertificadosAnterioresResource 
{
    private final Logger log = LoggerFactory.getLogger(CertificadosAnterioresResource.class);

    private static final String ENTITY_NAME = "certificadosAnteriores";

    private final CertificadosAnterioresService certificadosAnterioresService;

	private static String OS = System.getProperty("os.name").toLowerCase();

    public CertificadosAnterioresResource(CertificadosAnterioresService certificadosAnterioresService) {
        this.certificadosAnterioresService = certificadosAnterioresService;
    }

    /**
     * POST  /certificados-anteriores : Create a new certificadosAnteriores.
     *
     * @param certificadosAnteriores the certificadosAnteriores to create
     * @return the ResponseEntity with status 201 (Created) and with body the new certificadosAnteriores, or with status 400 (Bad Request) if the certificadosAnteriores has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/certificados-anteriores")
    @Timed
    public ResponseEntity<CertificadosAnteriores> createCertificadosAnteriores(@RequestBody CertificadosAnteriores certificadosAnteriores) throws URISyntaxException {
        log.debug("REST request to save CertificadosAnteriores : {}", certificadosAnteriores);
        if (certificadosAnteriores.getId() != null) {
            throw new BadRequestAlertException("A new certificadosAnteriores cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CertificadosAnteriores result = certificadosAnterioresService.save(certificadosAnteriores);
        return ResponseEntity.created(new URI("/api/certificados-anteriores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /certificados-anteriores : Updates an existing certificadosAnteriores.
     *
     * @param certificadosAnteriores the certificadosAnteriores to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated certificadosAnteriores,
     * or with status 400 (Bad Request) if the certificadosAnteriores is not valid,
     * or with status 500 (Internal Server Error) if the certificadosAnteriores couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/certificados-anteriores")
    @Timed
    public ResponseEntity<CertificadosAnteriores> updateCertificadosAnteriores(@RequestBody CertificadosAnteriores certificadosAnteriores) throws URISyntaxException {
        log.debug("REST request to update CertificadosAnteriores : {}", certificadosAnteriores);
        if (certificadosAnteriores.getId() == null) {
            return createCertificadosAnteriores(certificadosAnteriores);
        }
        CertificadosAnteriores result = certificadosAnterioresService.save(certificadosAnteriores);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, certificadosAnteriores.getId().toString()))
            .body(result);
    }

    /**
     * GET  /certificados-anteriores : get all the certificadosAnteriores.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of certificadosAnteriores in body
     */
    @GetMapping("/certificados-anteriores")
    @Timed
    public ResponseEntity<byte[]> getAllCertificadosAnteriores(@RequestParam(name = "ruta", required = false) String ruta,
	@RequestParam(name = "archivo", required = false) String nombreArchivo) 
	{
		final String currentDir = System.getProperty("user.dir");
		
		log.debug(currentDir);
        log.debug("Le voy a devolver el BLOB");
		log.debug("La ruta es " + ruta);
		log.debug("El archivo es " + nombreArchivo);
		
		if(isWindows()) { ruta = "C:" + ruta; } 
		
		else { ruta = currentDir + ruta; }
		
		File dir = new File(ruta);
		
		File[] matches = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				return name.startsWith(nombreArchivo) && name.endsWith(".pdf");
			}
		});
		
		log.debug("VA A ENTRAR AL CICLO");
		log.debug(String.valueOf(matches.length));
		
		File archivo = null;
		
		for (int i = 0; i < matches.length; i++) 
		{
			log.debug(matches[i].getName());
			log.debug(matches[i].getAbsolutePath());
			
			archivo = matches[i];
		}	 
		
        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
		try 
		{
            inputStream = new FileInputStream(archivo.getAbsolutePath());

            byte[] buffer = new byte[1024];
            baos = new ByteArrayOutputStream();
			
            int bytesRead;
            
			while ((bytesRead = inputStream.read(buffer)) != -1) { baos.write(buffer, 0, bytesRead); }       
		} 
		catch (FileNotFoundException e) { e.printStackTrace(); } 
		
		catch (IOException e) { e.printStackTrace(); } 
		
		finally { if (inputStream != null) { try { inputStream.close(); } catch (IOException e) { e.printStackTrace(); } } }
		
        return new ResponseEntity<byte[]>(baos.toByteArray(), HttpStatus.OK);
    }

    /**
     * DELETE  /certificados-anteriores/:id : delete the "id" certificadosAnteriores.
     *
     * @param id the id of the certificadosAnteriores to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/certificados-anteriores/{id}")
    @Timed
    public ResponseEntity<Void> deleteCertificadosAnteriores(@PathVariable Long id) {
        log.debug("REST request to delete CertificadosAnteriores : {}", id);
        certificadosAnterioresService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	
	public static boolean isWindows() { return (OS.indexOf("win") >= 0); }

    public static boolean isUnix() { return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0); }
}
