package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Cliente1890;
import cl.tutorial.sccm.service.Cliente1890Service;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Cliente1890.
 */
@RestController
@RequestMapping("/api")
public class Cliente1890Resource 
{
    private final Logger log = LoggerFactory.getLogger(Cliente1890Resource.class);

    private static final String ENTITY_NAME = "cliente1890";

    private final Cliente1890Service cliente1890Service;

    public Cliente1890Resource(Cliente1890Service cliente1890Service) {
        this.cliente1890Service = cliente1890Service;
    }

    /**
     * POST  /cliente-1890-s : Create a new cliente1890.
     *
     * @param cliente1890 the cliente1890 to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cliente1890, or with status 400 (Bad Request) if the cliente1890 has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cliente-1890-s")
    @Timed
    public ResponseEntity<Cliente1890> createCliente1890(@RequestBody Cliente1890 cliente1890) throws URISyntaxException 
	{
        log.debug("REST request to save Cliente1890 : {}", cliente1890);
        
		if (cliente1890.getId() != null) {
            throw new BadRequestAlertException("A new cliente1890 cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Cliente1890 result = cliente1890Service.save(cliente1890);
        return ResponseEntity.created(new URI("/api/cliente-1890-s/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cliente-1890-s : Updates an existing cliente1890.
     *
     * @param cliente1890 the cliente1890 to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cliente1890,
     * or with status 400 (Bad Request) if the cliente1890 is not valid,
     * or with status 500 (Internal Server Error) if the cliente1890 couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cliente-1890-s")
    @Timed
    public ResponseEntity<Cliente1890> updateCliente1890(@RequestBody Cliente1890 cliente1890) throws URISyntaxException {
        log.debug("REST request to update Cliente1890 : {}", cliente1890);
        if (cliente1890.getId() == null) {
            return createCliente1890(cliente1890);
        }
        Cliente1890 result = cliente1890Service.save(cliente1890);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cliente1890.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cliente-1890-s : get all the cliente1890S.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cliente1890S in body
     */
    @GetMapping("/cliente-1890-s")
    @Timed
    public List<Cliente1890> getAllCliente1890S() 
	{
        log.info("ENTRA AL SERVICIO GET ALL CLIENTE 1890 REST REALIZA EL REQUEST PARA TRAER TODOS LOS CLIENTES 1890");
        
		return cliente1890Service.findAll();
    }

    /**
     * GET  /cliente-1890-s/:id : get the "id" cliente1890.
     *
     * @param id the id of the cliente1890 to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cliente1890, or with status 404 (Not Found)
     */
    @GetMapping("/cliente-1890-s/{id}")
    @Timed
    public ResponseEntity<Cliente1890> getCliente1890(@PathVariable Long id) {
        log.debug("REST request to get Cliente1890 : {}", id);
        Cliente1890 cliente1890 = cliente1890Service.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cliente1890));
    }

    /**
     * DELETE  /cliente-1890-s/:id : delete the "id" cliente1890.
     *
     * @param id the id of the cliente1890 to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cliente-1890-s/{id}")
    @Timed
    public ResponseEntity<Void> deleteCliente1890(@PathVariable Long id) {
        log.debug("REST request to delete Cliente1890 : {}", id);
        cliente1890Service.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }	
}
