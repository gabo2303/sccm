package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.Bonos;
import cl.tutorial.sccm.service.BonosService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Bonos.
 */
@RestController
@RequestMapping("/api")
public class BonosResource 
{
    private final Logger log = LoggerFactory.getLogger(BonosResource.class);

    private static final String ENTITY_NAME = "bonos";

    private final BonosService bonosService;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public BonosResource(CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService, BonosService bonosService) 
	{
        this.bonosService = bonosService;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /bonos : Create a new bonos.
     *
     * @param bonos
     * 		the bonos to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new bonos, or with status 400 (Bad Request) if the bonos has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/bonos")
    @Timed
    public ResponseEntity<Bonos> createBonos(@RequestBody Bonos bonos) throws URISyntaxException 
	{
        log.debug("REST request to save Bonos : {}", bonos);
        
		if (bonos.getId() != null) { throw new BadRequestAlertException("A new bonos cannot already have an ID", ENTITY_NAME, "idexists"); }
		
        Bonos result = bonosService.save(bonos);
        
		return ResponseEntity.created(new URI("/api/bonos/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /bonos : Updates an existing bonos.
     *
     * @param bonos
     * 		the bonos to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated bonos, or with status 400 (Bad Request) if the bonos is not valid, or with status 500 (Internal
     * Server Error) if the bonos couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/bonos")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateBonos(@RequestBody Bonos bonos) throws URISyntaxException 
	{
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("BONOS");
		cabeceraControlCambio.setIdRegistro(bonos.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
		
		DetalleControlCambio resultado = guardaCampo(result, "rut", String.valueOf(bonos.getRut()), "C");
		resultado = guardaCampo(result, "dv", bonos.getDv(), "C");		
		resultado = guardaCampo(result, "producto", String.valueOf(bonos.getProducto()), "L");
		resultado = guardaCampo(result, "fechaPago", bonos.getFechaPago().toString(), "F");
		resultado = guardaCampo(result, "correlativo", String.valueOf(bonos.getCorrelativo().toString()), "C");	
		resultado = guardaCampo(result, "fechaInv", bonos.getFechaInv().toString(), "F");	
		resultado = guardaCampo(result, "folio", bonos.getFolio(), "C");
		resultado = guardaCampo(result, "capital", String.format("%.2f", bonos.getCapital()).replace(",", "."), "C");
		resultado = guardaCampo(result, "capMon", String.format("%.2f", bonos.getCapMon()).replace(",", "."), "C");
		resultado = guardaCampo(result, "intNom", bonos.getIntNom().toString(), "C");
		resultado = guardaCampo(result, "intReal", bonos.getIntReal().toString(), "C");
		resultado = guardaCampo(result, "monFi", bonos.getMonFi().toString(), "C");
		resultado = guardaCampo(result, "numGiro", bonos.getNumGiro().toString(), "C");
		resultado = guardaCampo(result, "codMon", bonos.getCodMon().toString(), "C");	
		resultado = guardaCampo(result, "monTasa", bonos.getMonTasa().toString(), "C");
		resultado = guardaCampo(result, "tasaEmi", bonos.getTasaEmi().toString(), "C");
		resultado = guardaCampo(result, "nomCli", bonos.getNomCli(), "C");
		resultado = guardaCampo(result, "codInst", bonos.getCodInst().toString(), "C");
		resultado = guardaCampo(result, "serieInst", bonos.getSerieInst(), "C");
		resultado = guardaCampo(result, "perTasa", bonos.getPerTasa().toString(), "C");	
		resultado = guardaCampo(result, "tipoInt", bonos.getTipoInt(), "C");
		resultado = guardaCampo(result, "codTipoInt", bonos.getCodTipoInt().toString(), "C");	
		resultado = guardaCampo(result, "fecVcto", bonos.getFecVcto().toString(), "F");
		resultado = guardaCampo(result, "onp", bonos.getOnp(), "C");	
		resultado = guardaCampo(result, "estCta", bonos.getEstCta(), "C");	
		resultado = guardaCampo(result, "fecCont", bonos.getFecCont().toString(), "F");
		resultado = guardaCampo(result, "bipersonal", bonos.getBipersonal(), "C");
		resultado = guardaCampo(result, "reajustabilidad", bonos.getReajustabilidad(), "C");	
		resultado = guardaCampo(result, "plazo", bonos.getPlazo().toString(), "C");
		resultado = guardaCampo(result, "indPago", bonos.getIndPago(), "C");	
		resultado = guardaCampo(result, "extranjero", bonos.getExtranjero(), "C");	
		resultado = guardaCampo(result, "impExt", bonos.getImpExt().toString(), "C");
		resultado = guardaCampo(result, "numCupones", bonos.getNumCupones().toString(), "C");
		resultado = guardaCampo(result, "numCuponPago", bonos.getNumCuponPago().toString(), "C");		
		resultado = guardaCampo(result, "ctaCap", bonos.getCtaCap().toString(), "C");			
		resultado = guardaCampo(result, "ctaReaj", bonos.getCtaReaj().toString(), "C");	
		resultado = guardaCampo(result, "ctaInt", bonos.getCtaInt().toString(), "C");	
		resultado = guardaCampo(result, "origen", bonos.getOrigen(), "C");	
		resultado = guardaCampo(result, "annoTributa", String.valueOf(bonos.getAnnoTributa().getId()), "L");
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).body(result);	
    }
	
	/**
     * PUT  /bonos : Updates an existing bonos.
     *
     * @param bonos
     * 		the bonos to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated bonos, or with status 400 (Bad Request) if the bonos is not valid, or with status 500 (Internal
     * Server Error) if the bonos couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/bonos/actualizar")
    @Timed
    public ResponseEntity<Bonos> actualizarBonos(@RequestBody Bonos bonos) throws URISyntaxException 
	{
        log.debug("REST request to update Bonos : {}", bonos);
        
        Bonos result = bonosService.save(bonos);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bonos.getId().toString())).body(result);
    }

    /**
     * GET  /bonos : get all the bonos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of bonos in body
     */
    @GetMapping("/bonos")
    @Timed
    public List<Bonos> getAllBonos() {
        log.debug("REST request to get all Bonos");
        return bonosService.findAll();
    }

    /**
     * GET  /bonos/:id : get the "id" bonos.
     *
     * @param id
     * 		the id of the bonos to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the bonos, or with status 404 (Not Found)
     */
    @GetMapping("/bonos/{id}")
    @Timed
    public ResponseEntity<Bonos> getBonos(@PathVariable Long id) 
	{
        log.debug("REST request to get Bonos : {}", id);
        
		Bonos bonos = bonosService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bonos));
    }

    /**
     * DELETE  /bonos/:id : delete the "id" bonos.
     *
     * @param id
     * 		the id of the bonos to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bonos/{id}")
    @Timed
    public ResponseEntity<Void> deleteBonos(@PathVariable Long id) 
	{
        log.debug("REST request to delete Bonos : {}", id);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("BONOS");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /bonos/:id : delete the "id" bonos.
     *
     * @param id
     * 		the id of the bonos to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bonos/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarBonos(@PathVariable Long id) 
	{
        log.debug("REST request to delete Bonos : {}", id);
        
		bonosService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}