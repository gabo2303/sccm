package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.AhorroMov;
import cl.tutorial.sccm.service.AhorroMovService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AhorroMov.
 */
@RestController
@RequestMapping("/api")
public class AhorroMovResource {

    private final Logger log = LoggerFactory.getLogger(AhorroMovResource.class);

    private static final String ENTITY_NAME = "ahorroMov";

    private final AhorroMovService ahorroMovService;

    public AhorroMovResource(AhorroMovService ahorroMovService) {
        this.ahorroMovService = ahorroMovService;
    }

    /**
     * POST  /ahorro-movs : Create a new ahorroMov.
     *
     * @param ahorroMov the ahorroMov to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ahorroMov, or with status 400 (Bad Request) if the ahorroMov has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ahorro-movs")
    @Timed
    public ResponseEntity<AhorroMov> createAhorroMov(@RequestBody AhorroMov ahorroMov) throws URISyntaxException {
        log.debug("REST request to save AhorroMov : {}", ahorroMov);
        if (ahorroMov.getId() != null) {
            throw new BadRequestAlertException("A new ahorroMov cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AhorroMov result = ahorroMovService.save(ahorroMov);
        return ResponseEntity.created(new URI("/api/ahorro-movs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ahorro-movs : Updates an existing ahorroMov.
     *
     * @param ahorroMov the ahorroMov to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ahorroMov,
     * or with status 400 (Bad Request) if the ahorroMov is not valid,
     * or with status 500 (Internal Server Error) if the ahorroMov couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ahorro-movs")
    @Timed
    public ResponseEntity<AhorroMov> updateAhorroMov(@RequestBody AhorroMov ahorroMov) throws URISyntaxException {
        log.debug("REST request to update AhorroMov : {}", ahorroMov);
        if (ahorroMov.getId() == null) {
            return createAhorroMov(ahorroMov);
        }
        AhorroMov result = ahorroMovService.save(ahorroMov);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ahorroMov.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ahorro-movs : get all the ahorroMovs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ahorroMovs in body
     */
    @GetMapping("/ahorro-movs")
    @Timed
    public List<AhorroMov> getAllAhorroMovs() {
        log.debug("REST request to get all AhorroMovs");
        return ahorroMovService.findAll();
        }

    /**
     * GET  /ahorro-movs/:id : get the "id" ahorroMov.
     *
     * @param id the id of the ahorroMov to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ahorroMov, or with status 404 (Not Found)
     */
    @GetMapping("/ahorro-movs/{id}")
    @Timed
    public ResponseEntity<AhorroMov> getAhorroMov(@PathVariable Long id) {
        log.debug("REST request to get AhorroMov : {}", id);
        AhorroMov ahorroMov = ahorroMovService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ahorroMov));
    }

    /**
     * DELETE  /ahorro-movs/:id : delete the "id" ahorroMov.
     *
     * @param id the id of the ahorroMov to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ahorro-movs/{id}")
    @Timed
    public ResponseEntity<Void> deleteAhorroMov(@PathVariable Long id) {
        log.debug("REST request to delete AhorroMov : {}", id);
        ahorroMovService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
