package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Clientes1890mantencion;
import cl.tutorial.sccm.service.Clientes1890mantencionService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.service.dto.Clientes1890mantencionCriteria;
import cl.tutorial.sccm.service.Clientes1890mantencionQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Clientes1890mantencion.
 */
@RestController
@RequestMapping("/api")
public class Clientes1890mantencionResource {

    private final Logger log = LoggerFactory.getLogger(Clientes1890mantencionResource.class);

    private static final String ENTITY_NAME = "clientes1890mantencion";

    private final Clientes1890mantencionService clientes1890mantencionService;

    private final Clientes1890mantencionQueryService clientes1890mantencionQueryService;

    public Clientes1890mantencionResource(Clientes1890mantencionService clientes1890mantencionService, Clientes1890mantencionQueryService clientes1890mantencionQueryService) {
        this.clientes1890mantencionService = clientes1890mantencionService;
        this.clientes1890mantencionQueryService = clientes1890mantencionQueryService;
    }

    /**
     * GET  /clientes-1890-mantencions : get all the clientes1890mantencions.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of clientes1890mantencions in body
     */
    @GetMapping("/clientes-1890-mantencions")
    @Timed
    public ResponseEntity<List<Clientes1890mantencion>> getAllClientes1890mantencions(Clientes1890mantencionCriteria criteria) {
        log.debug("REST request to get Clientes1890mantencions by criteria: {}", criteria);
        List<Clientes1890mantencion> entityList = clientes1890mantencionQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /clientes-1890-mantencions/:id : get the "id" clientes1890mantencion.
     *
     * @param id the id of the clientes1890mantencion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientes1890mantencion, or with status 404 (Not Found)
     */
    @GetMapping("/clientes-1890-mantencions/{id}")
    @Timed
    public ResponseEntity<Clientes1890mantencion> getClientes1890mantencion(@PathVariable Long id) {
        log.debug("REST request to get Clientes1890mantencion : {}", id);
        Clientes1890mantencion clientes1890mantencion = clientes1890mantencionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientes1890mantencion));
    }

    /**
     * DELETE  /clientes-1890-mantencions/:id : delete the "id" clientes1890mantencion.
     *
     * @param id the id of the clientes1890mantencion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/clientes-1890-mantencions/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientes1890mantencion(@PathVariable Long id) {
        log.debug("REST request to delete Clientes1890mantencion : {}", id);
        clientes1890mantencionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
