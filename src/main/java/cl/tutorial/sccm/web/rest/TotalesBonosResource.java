package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.config.ApplicationProperties;
import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.TotalesBonos;
import cl.tutorial.sccm.repository.TotalesBonosRepository;
import cl.tutorial.sccm.service.dto.TotalesBonosDto;
import cl.tutorial.sccm.service.mapper.TotalesBonosMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TotalesBonos.
 */
@RestController
@RequestMapping("/api")
public class TotalesBonosResource {

    private final Logger log = LoggerFactory.getLogger(TotalesBonosResource.class);

    private static final String ENTITY_NAME = "totalesBonos";

    private final TotalesBonosRepository totalesBonosRepository;
    private final ApplicationProperties  applicationProperties;

    public TotalesBonosResource(TotalesBonosRepository totalesBonosRepository, ApplicationProperties applicationProperties) {
        this.totalesBonosRepository = totalesBonosRepository;
        this.applicationProperties = applicationProperties;
    }

    /**
     * POST  /totales-bonos : Create a new totalesBonos.
     *
     * @param totalesBonos
     * 		the totalesBonos to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new totalesBonos, or with status 400 (Bad Request) if the totalesBonos has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/totales-bonos")
    @Timed
    public ResponseEntity<TotalesBonos> createTotalesBonos(@RequestBody TotalesBonos totalesBonos) throws URISyntaxException {
        log.debug("REST request to save TotalesBonos : {}", totalesBonos);
        if (totalesBonos.getId() != null) {
            throw new BadRequestAlertException("A new totalesBonos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TotalesBonos result = totalesBonosRepository.save(totalesBonos);
        return ResponseEntity.created(new URI("/api/totales-bonos/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /totales-bonos : Updates an existing totalesBonos.
     *
     * @param totalesBonos
     * 		the totalesBonos to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated totalesBonos, or with status 400 (Bad Request) if the totalesBonos is not valid, or with status 500
     * (Internal Server Error) if the totalesBonos couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/totales-bonos")
    @Timed
    public ResponseEntity<TotalesBonos> updateTotalesBonos(@RequestBody TotalesBonos totalesBonos) throws URISyntaxException {
        log.debug("REST request to update TotalesBonos : {}", totalesBonos);
        if (totalesBonos.getId() == null) {
            return createTotalesBonos(totalesBonos);
        }
        TotalesBonos result = totalesBonosRepository.save(totalesBonos);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, totalesBonos.getId().toString())).body(result);
    }

    /**
     * GET  /totales-bonos : get all the totalesBonos.
     *
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of totalesBonos in body
     */
    @GetMapping("/totales-bonos")
    @Timed
    public ResponseEntity<List<TotalesBonos>> getAllTotalesBonos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TotalesBonos");
        Page<TotalesBonos> page = totalesBonosRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/totales-bonos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /totales-bonos/:id : get the "id" totalesBonos.
     *
     * @param id
     * 		the id of the totalesBonos to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the totalesBonos, or with status 404 (Not Found)
     */
    @GetMapping("/totales-bonos/{id}")
    @Timed
    public ResponseEntity<TotalesBonos> getTotalesBonos(@PathVariable Long id) {
        log.debug("REST request to get TotalesBonos : {}", id);
        TotalesBonos totalesBonos = totalesBonosRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(totalesBonos));
    }

    /**
     * DELETE  /totales-bonos/:id : delete the "id" totalesBonos.
     *
     * @param id
     * 		the id of the totalesBonos to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/totales-bonos/{id}")
    @Timed
    public ResponseEntity<Void> deleteTotalesBonos(@PathVariable Long id) {
        log.debug("REST request to delete TotalesBonos : {}", id);
        totalesBonosRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  "/totales-bonos/{idAnno}/findByAllByTaxYearIdPaginated : get all totalesBonos by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of totalesBonos in body
     */
    @GetMapping("/totales-bonos/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<TotalesBonos>> findByAllByTaxYearIdPaginated(@PathVariable Long idAnno, @ApiParam Pageable pageable) {
        log.debug("REST request to get all TotalesBonos by tax year with pagination");
        Page<TotalesBonos> page = totalesBonosRepository.findAllByAnnoTributaId(idAnno, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/totales-bonos/{idAnno}/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/totales-bonos/findByAllByTaxYearId : get all TotalesBonosDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of TotalesBonosDto in body
     */
    @GetMapping("/totales-bonos/findByAllByTaxYearId")
    @Timed
    public List<TotalesBonosDto> findByAllByTaxYearId(@RequestParam(name = "idAnno") Long idAnno, @RequestParam(name = "queryOption") String queryOption) {
        String message = "";
        Boolean isValidOption = true;
        log.debug("REST request to get all TotalesBonosDto by tax year");
        List<TotalesBonos> allByAnnoTributa = new ArrayList<>();
        switch (queryOption) {
            case Constants.EXCEL_OPTION_BICE:
                allByAnnoTributa = totalesBonosRepository.findAllByAnnoTributaIdAndRutBeneficiario(idAnno, this.applicationProperties.getRutBice());
                break;
            case Constants.EXCEL_OPTION_NO_BICE:
                allByAnnoTributa = totalesBonosRepository.findAllByAnnoTributaIdAndRutBeneficiarioNot(idAnno, this.applicationProperties.getRutBice());
                break;
            case Constants.EXCEL_OPTION_ALL:
                allByAnnoTributa = totalesBonosRepository.findAllByAnnoTributaId(idAnno);
                break;
            default:
                isValidOption = false;
                message = "La opción del servicio no es válida. Opción: " + queryOption + ".";
                break;
        }
        final List<TotalesBonosDto> dapDtos = TotalesBonosMapper.INSTANCE.totalesBonosToTotalesBonosDto(allByAnnoTributa);
        log.debug("REST request to get all TotalesBonosDto by tax year COUNT : {}", dapDtos.size());
        return dapDtos;
    }
}
