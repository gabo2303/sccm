package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.AhorroCC;
import cl.tutorial.sccm.service.AhorroCCService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing Pactos.
 */
@RestController
@RequestMapping("/api")
public class AhorroCCResource {

    private final Logger log = LoggerFactory.getLogger(AhorroCCResource.class);

    private static final String ENTITY_NAME = "ahorrosCC";

    private final AhorroCCService ahorroCCService;

    public AhorroCCResource(AhorroCCService ahorroCCService) {
        this.ahorroCCService = ahorroCCService;
    }

    /**
     * GET  /pactos : get all the pactos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pactos in body
     */
    @GetMapping("/ahorrosCC/{idAnno}/getByAnnoId")
    @Timed
    public List<AhorroCC> findAllByAnnoTributa(@PathVariable Long idAnno) {
        log.debug("REST request to get all Ahorro Cuadratura Contable");
        return ahorroCCService.findAllByAnnoTributa(idAnno);
    }
}
