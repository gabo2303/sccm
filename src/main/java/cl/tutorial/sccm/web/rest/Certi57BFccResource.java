package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Certi57BFcc;
import cl.tutorial.sccm.service.Certi57BFccService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Certi57BFcc.
 */
@RestController
@RequestMapping("/api")
public class Certi57BFccResource {

    private final Logger log = LoggerFactory.getLogger(Certi57BFccResource.class);

    private static final String ENTITY_NAME = "certi57BFcc";

    private final Certi57BFccService certi57BFccService;

    public Certi57BFccResource(Certi57BFccService certi57BFccService) {
        this.certi57BFccService = certi57BFccService;
    }

    /**
     * POST  /certi-57-b-fccs : Create a new certi57BFcc.
     *
     * @param certi57BFcc the certi57BFcc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new certi57BFcc, or with status 400 (Bad Request) if the certi57BFcc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/certi-57-b-fccs")
    @Timed
    public ResponseEntity<Certi57BFcc> createCerti57BFcc(@Valid @RequestBody Certi57BFcc certi57BFcc) throws URISyntaxException {
        log.debug("REST request to save Certi57BFcc : {}", certi57BFcc);
        if (certi57BFcc.getId() != null) {
            throw new BadRequestAlertException("A new certi57BFcc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Certi57BFcc result = certi57BFccService.save(certi57BFcc);
        return ResponseEntity.created(new URI("/api/certi-57-b-fccs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /certi-57-b-fccs : Updates an existing certi57BFcc.
     *
     * @param certi57BFcc the certi57BFcc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated certi57BFcc,
     * or with status 400 (Bad Request) if the certi57BFcc is not valid,
     * or with status 500 (Internal Server Error) if the certi57BFcc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/certi-57-b-fccs")
    @Timed
    public ResponseEntity<Certi57BFcc> updateCerti57BFcc(@Valid @RequestBody Certi57BFcc certi57BFcc) throws URISyntaxException {
        log.debug("REST request to update Certi57BFcc : {}", certi57BFcc);
        if (certi57BFcc.getId() == null) {
            return createCerti57BFcc(certi57BFcc);
        }
        Certi57BFcc result = certi57BFccService.save(certi57BFcc);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, certi57BFcc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /certi-57-b-fccs : get all the certi57BFccs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of certi57BFccs in body
     */
    @GetMapping("/certi-57-b-fccs")
    @Timed
    public List<Certi57BFcc> getAllCerti57BFccs() {
        log.debug("REST request to get all Certi57BFccs");
        return certi57BFccService.findAll();
        }

    /**
     * GET  /certi-57-b-fccs/:id : get the "id" certi57BFcc.
     *
     * @param id the id of the certi57BFcc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the certi57BFcc, or with status 404 (Not Found)
     */
    @GetMapping("/certi-57-b-fccs/{id}")
    @Timed
    public ResponseEntity<Certi57BFcc> getCerti57BFcc(@PathVariable Long id) {
        log.debug("REST request to get Certi57BFcc : {}", id);
        Certi57BFcc certi57BFcc = certi57BFccService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(certi57BFcc));
    }

    /**
     * DELETE  /certi-57-b-fccs/:id : delete the "id" certi57BFcc.
     *
     * @param id the id of the certi57BFcc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/certi-57-b-fccs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCerti57BFcc(@PathVariable Long id) {
        log.debug("REST request to delete Certi57BFcc : {}", id);
        certi57BFccService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
