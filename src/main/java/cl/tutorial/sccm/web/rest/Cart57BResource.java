package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Cart57B;
import cl.tutorial.sccm.service.Cart57BService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Cart57B.
 */
@RestController
@RequestMapping("/api")
public class Cart57BResource {

    private final Logger log = LoggerFactory.getLogger(Cart57BResource.class);

    private static final String ENTITY_NAME = "cart57B";

    private final Cart57BService cart57BService;

    public Cart57BResource(Cart57BService cart57BService) {
        this.cart57BService = cart57BService;
    }

    /**
     * POST  /cart-57-bs : Create a new cart57B.
     *
     * @param cart57B the cart57B to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cart57B, or with status 400 (Bad Request) if the cart57B has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cart-57-bs")
    @Timed
    public ResponseEntity<Cart57B> createCart57B(@Valid @RequestBody Cart57B cart57B) throws URISyntaxException {
        log.debug("REST request to save Cart57B : {}", cart57B);
        if (cart57B.getId() != null) {
            throw new BadRequestAlertException("A new cart57B cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Cart57B result = cart57BService.save(cart57B);
        return ResponseEntity.created(new URI("/api/cart-57-bs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cart-57-bs : Updates an existing cart57B.
     *
     * @param cart57B the cart57B to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cart57B,
     * or with status 400 (Bad Request) if the cart57B is not valid,
     * or with status 500 (Internal Server Error) if the cart57B couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cart-57-bs")
    @Timed
    public ResponseEntity<Cart57B> updateCart57B(@Valid @RequestBody Cart57B cart57B) throws URISyntaxException {
        log.debug("REST request to update Cart57B : {}", cart57B);
        if (cart57B.getId() == null) {
            return createCart57B(cart57B);
        }
        Cart57B result = cart57BService.save(cart57B);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cart57B.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cart-57-bs : get all the cart57BS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cart57BS in body
     */
    @GetMapping("/cart-57-bs")
    @Timed
    public List<Cart57B> getAllCart57BS() {
        log.debug("REST request to get all Cart57BS");
        return cart57BService.findAll();
        }

    /**
     * GET  /cart-57-bs/:id : get the "id" cart57B.
     *
     * @param id the id of the cart57B to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cart57B, or with status 404 (Not Found)
     */
    @GetMapping("/cart-57-bs/{id}")
    @Timed
    public ResponseEntity<Cart57B> getCart57B(@PathVariable Long id) {
        log.debug("REST request to get Cart57B : {}", id);
        Cart57B cart57B = cart57BService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cart57B));
    }

    /**
     * DELETE  /cart-57-bs/:id : delete the "id" cart57B.
     *
     * @param id the id of the cart57B to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cart-57-bs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCart57B(@PathVariable Long id) {
        log.debug("REST request to delete Cart57B : {}", id);
        cart57BService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/cart-57-bs/{idAnno}/getByAnnoId")
    @Timed
    public List<Cart57B>  findAllByAnnoTributaId(@PathVariable Long idAnno){
        log.debug("REST request to reprocessE57B:");
        return  this.cart57BService.findAllByAnnoTributaId(idAnno);
    }

}
