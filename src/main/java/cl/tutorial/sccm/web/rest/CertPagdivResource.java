package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.Cert57B;
import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.CertPagdiv;
import cl.tutorial.sccm.service.CertPagdivService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CertPagdiv.
 */
@RestController
@RequestMapping("/api")
public class CertPagdivResource {

    private final Logger log = LoggerFactory.getLogger(CertPagdivResource.class);

    private static final String ENTITY_NAME = "certPagdiv";
    private static final String SUCEESS_EXE_LOAD_CERT_PAG_DIV = "successCertPagDiv";

    private final CertPagdivService certPagdivService;

    public CertPagdivResource(CertPagdivService certPagdivService) {
        this.certPagdivService = certPagdivService;
    }

    /**
     * POST  /cert-pagdivs : Create a new certPagdiv.
     *
     * @param certPagdiv the certPagdiv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new certPagdiv, or with status 400 (Bad Request) if the certPagdiv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cert-pagdivs")
    @Timed
    public ResponseEntity<CertPagdiv> createCertPagdiv(@RequestBody CertPagdiv certPagdiv) throws URISyntaxException {
        log.debug("REST request to save CertPagdiv : {}", certPagdiv);
        if (certPagdiv.getId() != null) {
            throw new BadRequestAlertException("A new certPagdiv cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CertPagdiv result = certPagdivService.save(certPagdiv);
        return ResponseEntity.created(new URI("/api/cert-pagdivs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cert-pagdivs : Updates an existing certPagdiv.
     *
     * @param certPagdiv the certPagdiv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated certPagdiv,
     * or with status 400 (Bad Request) if the certPagdiv is not valid,
     * or with status 500 (Internal Server Error) if the certPagdiv couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cert-pagdivs")
    @Timed
    public ResponseEntity<CertPagdiv> updateCertPagdiv(@RequestBody CertPagdiv certPagdiv) throws URISyntaxException {
        log.debug("REST request to update CertPagdiv : {}", certPagdiv);
        if (certPagdiv.getId() == null) {
            return createCertPagdiv(certPagdiv);
        }
        CertPagdiv result = certPagdivService.save(certPagdiv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, certPagdiv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cert-pagdivs : get all the certPagdivs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of certPagdivs in body
     */
    @GetMapping("/cert-pagdivs")
    @Timed
    public List<CertPagdiv> getAllCertPagdivs() {
        log.debug("REST request to get all CertPagdivs");
        return certPagdivService.findAll();
        }

    /**
     * GET  /cert-pagdivs/:id : get the "id" certPagdiv.
     *
     * @param id the id of the certPagdiv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the certPagdiv, or with status 404 (Not Found)
     */
    @GetMapping("/cert-pagdivs/{id}")
    @Timed
    public ResponseEntity<CertPagdiv> getCertPagdiv(@PathVariable Long id) {
        log.debug("REST request to get CertPagdiv : {}", id);
        CertPagdiv certPagdiv = certPagdivService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(certPagdiv));
    }

    /**
     * DELETE  /cert-pagdivs/:id : delete the "id" certPagdiv.
     *
     * @param id the id of the certPagdiv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cert-pagdivs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCertPagdiv(@PathVariable Long id) {
        log.debug("REST request to delete CertPagdiv : {}", id);
        certPagdivService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/cert-pagdivs/{idAnno}/getByAnnoId")
    @Timed
    public List<CertPagdiv>  findAllByAnnoTributaId(@PathVariable Long idAnno){
        log.debug("REST request to reprocessE57B:");
        return  this.certPagdivService.findAllByAnnoTributaId(idAnno);
    }

    @GetMapping("/cert-pagdivs/executeSpLoadCertPagDiv")
    @Timed
    public  ResponseEntity<Void> executeSpLoadCertPagDiv(){
        log.debug("REST request to executeSpLoadCertPagDiv:");
        this.certPagdivService.executeSpLoadCertPagDiv();
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCustomAlert(ENTITY_NAME, "1",SUCEESS_EXE_LOAD_CERT_PAG_DIV)).build();
    }

}
