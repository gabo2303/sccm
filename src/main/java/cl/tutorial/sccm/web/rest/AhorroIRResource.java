package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.AhorroIR;
import cl.tutorial.sccm.domain.PactosIR;
import cl.tutorial.sccm.service.AhorroIRService;
import cl.tutorial.sccm.service.PactosIRService;
import com.codahale.metrics.annotation.Timed;
import javassist.expr.Instanceof;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.List;

/**
 * REST controller for managing Pactos.
 */
@RestController
@RequestMapping("/api")
public class AhorroIRResource {

    private final Logger log = LoggerFactory.getLogger(AhorroIRResource.class);

    private static final String ENTITY_NAME = "ahorrosIR";

    private final AhorroIRService ahorroIRService;

    public AhorroIRResource(AhorroIRService ahorroIRService) {
        this.ahorroIRService = ahorroIRService;
    }

    /**
     * GET  /pactos : get all the pactos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pactos in body
     */
    @GetMapping("/ahorrosIR/{idAnno}/getByAnnoId")
    @Timed
    public List<AhorroIR> findAllByAnnoTributa(@PathVariable BigInteger idAnno) {
        log.debug("REST request to get all Ahorro Interés Real");
        return ahorroIRService.findAllByAnnoTributa(idAnno);
    }
}
