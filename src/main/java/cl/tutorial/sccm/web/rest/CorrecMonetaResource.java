package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.CorrecMoneta;
import cl.tutorial.sccm.service.CorrecMonetaService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CorrecMoneta.
 */
@RestController
@RequestMapping("/api")
public class CorrecMonetaResource {

    private final Logger log = LoggerFactory.getLogger(CorrecMonetaResource.class);

    private static final String ENTITY_NAME = "correcMoneta";

    private final CorrecMonetaService correcMonetaService;

    public CorrecMonetaResource(CorrecMonetaService correcMonetaService) {
        this.correcMonetaService = correcMonetaService;
    }

    /**
     * POST  /correc-monetas : Create a new correcMoneta.
     *
     * @param correcMoneta the correcMoneta to create
     * @return the ResponseEntity with status 201 (Created) and with body the new correcMoneta, or with status 400 (Bad Request) if the correcMoneta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/correc-monetas")
    @Timed
    public ResponseEntity<CorrecMoneta> createCorrecMoneta(@RequestBody CorrecMoneta correcMoneta) throws URISyntaxException {
        log.debug("REST request to save CorrecMoneta : {}", correcMoneta);
        if (correcMoneta.getId() != null) {
            throw new BadRequestAlertException("A new correcMoneta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CorrecMoneta result = correcMonetaService.save(correcMoneta);
        return ResponseEntity.created(new URI("/api/correc-monetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /correc-monetas : Updates an existing correcMoneta.
     *
     * @param correcMoneta the correcMoneta to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated correcMoneta,
     * or with status 400 (Bad Request) if the correcMoneta is not valid,
     * or with status 500 (Internal Server Error) if the correcMoneta couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/correc-monetas")
    @Timed
    public ResponseEntity<CorrecMoneta> updateCorrecMoneta(@RequestBody CorrecMoneta correcMoneta) throws URISyntaxException {
        log.debug("REST request to update CorrecMoneta : {}", correcMoneta);
        if (correcMoneta.getId() == null) {
            return createCorrecMoneta(correcMoneta);
        }
        CorrecMoneta result = correcMonetaService.save(correcMoneta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, correcMoneta.getId().toString()))
            .body(result);
    }

    /**
     * GET  /correc-monetas : get all the correcMonetas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of correcMonetas in body
     */
    @GetMapping("/correc-monetas")
    @Timed
    public List<CorrecMoneta> getAllCorrecMonetas() {
        log.debug("REST request to get all CorrecMonetas");
        return correcMonetaService.findAll();
        }

    /**
     * GET  /correc-monetas/:id : get the "id" correcMoneta.
     *
     * @param id the id of the correcMoneta to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the correcMoneta, or with status 404 (Not Found)
     */
    @GetMapping("/correc-monetas/{id}")
    @Timed
    public ResponseEntity<CorrecMoneta> getCorrecMoneta(@PathVariable Long id) {
        log.debug("REST request to get CorrecMoneta : {}", id);
        CorrecMoneta correcMoneta = correcMonetaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(correcMoneta));
    }

    /**
     * DELETE  /correc-monetas/:id : delete the "id" correcMoneta.
     *
     * @param id the id of the correcMoneta to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/correc-monetas/{id}")
    @Timed
    public ResponseEntity<Void> deleteCorrecMoneta(@PathVariable Long id) {
        log.debug("REST request to delete CorrecMoneta : {}", id);
        correcMonetaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
