package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.DapCC;
import cl.tutorial.sccm.repository.DapCCRepository;
import cl.tutorial.sccm.service.dto.DapCcDto;
import cl.tutorial.sccm.service.mapper.DapCcMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DapCC.
 */
@RestController
@RequestMapping("/api")
public class DapCCResource {

    private final Logger log = LoggerFactory.getLogger(DapCCResource.class);

    private static final String ENTITY_NAME = "dapCC";

    private final DapCCRepository dapCCRepository;

    public DapCCResource(DapCCRepository dapCCRepository) {
        this.dapCCRepository = dapCCRepository;
    }

    /**
     * POST  /dap-ccs : Create a new dapCC.
     *
     * @param dapCC
     * 		the dapCC to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new dapCC, or with status 400 (Bad Request) if the dapCC has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/dap-ccs")
    @Timed
    public ResponseEntity<DapCC> createDapCC(@RequestBody DapCC dapCC) throws URISyntaxException {
        log.debug("REST request to save DapCC : {}", dapCC);
        if (dapCC.getId() != null) {
            throw new BadRequestAlertException("A new dapCC cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DapCC result = dapCCRepository.save(dapCC);
        return ResponseEntity.created(new URI("/api/dap-ccs/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /dap-ccs : Updates an existing dapCC.
     *
     * @param dapCC
     * 		the dapCC to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated dapCC, or with status 400 (Bad Request) if the dapCC is not valid, or with status 500 (Internal
     * Server Error) if the dapCC couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/dap-ccs")
    @Timed
    public ResponseEntity<DapCC> updateDapCC(@RequestBody DapCC dapCC) throws URISyntaxException {
        log.debug("REST request to update DapCC : {}", dapCC);
        if (dapCC.getId() == null) {
            return createDapCC(dapCC);
        }
        DapCC result = dapCCRepository.save(dapCC);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dapCC.getId().toString())).body(result);
    }

    /**
     * GET  /dap-ccs : get all the dapCCS.
     *
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dapCCS in body
     */
    @GetMapping("/dap-ccs")
    @Timed
    public ResponseEntity<List<DapCC>> getAllDapCCS(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DapCCS");
        Page<DapCC> page = dapCCRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dap-ccs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dap-ccs/:id : get the "id" dapCC.
     *
     * @param id
     * 		the id of the dapCC to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the dapCC, or with status 404 (Not Found)
     */
    @GetMapping("/dap-ccs/{id}")
    @Timed
    public ResponseEntity<DapCC> getDapCC(@PathVariable Long id) {
        log.debug("REST request to get DapCC : {}", id);
        DapCC dapCC = dapCCRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dapCC));
    }

    /**
     * DELETE  /dap-ccs/:id : delete the "id" dapCC.
     *
     * @param id
     * 		the id of the dapCC to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dap-ccs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDapCC(@PathVariable Long id) {
        log.debug("REST request to delete DapCC : {}", id);
        dapCCRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /dap-ccs/{idAnno}/getByYearId : get all DapCC by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapCC in body
     */
    @GetMapping("/dap-ccs/{idAnno}/getByYearId")
    @Timed
    public List<DapCC> getByYearId(@PathVariable BigInteger idAnno) {
        log.debug("REST request to get all DapCC by tax year");
        return dapCCRepository.findAllByAnnoTributaId(idAnno);
    }

    /**
     * GET  /dap-ccs/{idAnno}/findByAllByTaxYearIdPaginated : get all DapCC by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapCC in body
     */
    @GetMapping("/dap-ccs/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<DapCC>> findByAllByTaxYearIdPaginated(@PathVariable BigInteger idAnno, @ApiParam Pageable pageable) {
        log.debug("REST request to get all DapCC by tax year with pagination");
        Page<DapCC> page = dapCCRepository.findAllByAnnoTributaId(idAnno, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/dap-ccs/{idAnno}/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/dap-ccs/findByAllByTaxYearIdCurrencyPaginated : get all DapCC by tax year and currency with pagination.
     *
     * @param idAnno
     * 		Id del año tributario
     * @param currency
     * 		El tipo de moneda
     * @param pageable
     * 		The pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapCC in body
     */
    @GetMapping("/dap-ccs/findByAllByTaxYearIdCurrencyPaginated")
    @Timed
    public ResponseEntity<List<DapCC>> findByAllByTaxYearIdCurrencyPaginated(@RequestParam(name = "idAnno", required = true) BigInteger idAnno, @RequestParam(name =
        "currency", required = false) Long currency, @ApiParam Pageable pageable) {

        log.debug("REST request to get all DapCC by tax year and currency with pagination");
        Page<DapCC> page;
        if (currency != null) {
            page = dapCCRepository.findAllByAnnoTributaIdAndCodMoneda(idAnno, currency, pageable);
        } else {
            page = dapCCRepository.findAllByAnnoTributaId(idAnno, pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/dap-ccs/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/d-ccs/{idAnno}/findByAllByTaxYearId : get all DapCcDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapCcDto in body
     */
    @GetMapping("/dap-ccs/{idAnno}/findByAllByTaxYearId")
    @Timed
    public List<DapCcDto> findByAllByTaxYearId(@PathVariable BigInteger idAnno) {
        log.debug("REST request to get all DapCcDto by tax year");
        final List<DapCC> allByAnnoTributa = dapCCRepository.findAllByAnnoTributaId(idAnno);
        final List<DapCcDto> dapDtos = DapCcMapper.INSTANCE.dapCcToDapCcDto(allByAnnoTributa);
        log.debug("REST request to get all DapCcDto by tax year COUNT : {}", dapDtos.size());
        return dapDtos;
    }
}
