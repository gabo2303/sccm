package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.EntPagdiv;
import cl.tutorial.sccm.service.EntPagdivService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EntPagdiv.
 */
@RestController
@RequestMapping("/api")
public class EntPagdivResource {

    private final Logger log = LoggerFactory.getLogger(EntPagdivResource.class);

    private static final String ENTITY_NAME = "entPagdiv";

    private final EntPagdivService entPagdivService;

    public EntPagdivResource(EntPagdivService entPagdivService) {
        this.entPagdivService = entPagdivService;
    }

    /**
     * POST  /ent-pagdivs : Create a new entPagdiv.
     *
     * @param entPagdiv the entPagdiv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new entPagdiv, or with status 400 (Bad Request) if the entPagdiv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ent-pagdivs")
    @Timed
    public ResponseEntity<EntPagdiv> createEntPagdiv(@RequestBody EntPagdiv entPagdiv) throws URISyntaxException {
        log.debug("REST request to save EntPagdiv : {}", entPagdiv);
        if (entPagdiv.getId() != null) {
            throw new BadRequestAlertException("A new entPagdiv cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EntPagdiv result = entPagdivService.save(entPagdiv);
        return ResponseEntity.created(new URI("/api/ent-pagdivs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ent-pagdivs : Updates an existing entPagdiv.
     *
     * @param entPagdiv the entPagdiv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated entPagdiv,
     * or with status 400 (Bad Request) if the entPagdiv is not valid,
     * or with status 500 (Internal Server Error) if the entPagdiv couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ent-pagdivs")
    @Timed
    public ResponseEntity<EntPagdiv> updateEntPagdiv(@RequestBody EntPagdiv entPagdiv) throws URISyntaxException {
        log.debug("REST request to update EntPagdiv : {}", entPagdiv);
        if (entPagdiv.getId() == null) {
            return createEntPagdiv(entPagdiv);
        }
        EntPagdiv result = entPagdivService.save(entPagdiv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, entPagdiv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ent-pagdivs : get all the entPagdivs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of entPagdivs in body
     */
    @GetMapping("/ent-pagdivs")
    @Timed
    public List<EntPagdiv> getAllEntPagdivs() {
        log.debug("REST request to get all EntPagdivs");
        return entPagdivService.findAll();
        }

    /**
     * GET  /ent-pagdivs/:id : get the "id" entPagdiv.
     *
     * @param id the id of the entPagdiv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the entPagdiv, or with status 404 (Not Found)
     */
    @GetMapping("/ent-pagdivs/{id}")
    @Timed
    public ResponseEntity<EntPagdiv> getEntPagdiv(@PathVariable Long id) {
        log.debug("REST request to get EntPagdiv : {}", id);
        EntPagdiv entPagdiv = entPagdivService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(entPagdiv));
    }

    /**
     * DELETE  /ent-pagdivs/:id : delete the "id" entPagdiv.
     *
     * @param id the id of the entPagdiv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ent-pagdivs/{id}")
    @Timed
    public ResponseEntity<Void> deleteEntPagdiv(@PathVariable Long id) {
        log.debug("REST request to delete EntPagdiv : {}", id);
        entPagdivService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/ent-pagdivs/{idAnno}/getByAnnoId")
    @Timed
    public List<EntPagdiv>  findAllByAnnoTributaId(@PathVariable Long idAnno){
        log.debug("REST request to reprocessE57B:");
        return  this.entPagdivService.findAllByAnnoTributaId(idAnno);
    }
}
