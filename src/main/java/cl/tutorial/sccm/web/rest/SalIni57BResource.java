package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.SalIni57B;
import cl.tutorial.sccm.service.SalIni57BService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SalIni57B.
 */
@RestController
@RequestMapping("/api")
public class SalIni57BResource {

    private final Logger log = LoggerFactory.getLogger(SalIni57BResource.class);

    private static final String ENTITY_NAME = "salIni57B";

    private final SalIni57BService salIni57BService;

    public SalIni57BResource(SalIni57BService salIni57BService) {
        this.salIni57BService = salIni57BService;
    }

    /**
     * POST  /sal-ini-57-bs : Create a new salIni57B.
     *
     * @param salIni57B the salIni57B to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salIni57B, or with status 400 (Bad Request) if the salIni57B has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sal-ini-57-bs")
    @Timed
    public ResponseEntity<SalIni57B> createSalIni57B(@RequestBody SalIni57B salIni57B) throws URISyntaxException {
        log.debug("REST request to save SalIni57B : {}", salIni57B);
        if (salIni57B.getId() != null) {
            throw new BadRequestAlertException("A new salIni57B cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SalIni57B result = salIni57BService.save(salIni57B);
        return ResponseEntity.created(new URI("/api/sal-ini-57-bs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sal-ini-57-bs : Updates an existing salIni57B.
     *
     * @param salIni57B the salIni57B to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salIni57B,
     * or with status 400 (Bad Request) if the salIni57B is not valid,
     * or with status 500 (Internal Server Error) if the salIni57B couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sal-ini-57-bs")
    @Timed
    public ResponseEntity<SalIni57B> updateSalIni57B(@RequestBody SalIni57B salIni57B) throws URISyntaxException {
        log.debug("REST request to update SalIni57B : {}", salIni57B);
        if (salIni57B.getId() == null) {
            return createSalIni57B(salIni57B);
        }
        SalIni57B result = salIni57BService.save(salIni57B);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salIni57B.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sal-ini-57-bs : get all the salIni57BS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of salIni57BS in body
     */
    @GetMapping("/sal-ini-57-bs")
    @Timed
    public List<SalIni57B> getAllSalIni57BS() {
        log.debug("REST request to get all SalIni57BS");
        return salIni57BService.findAll();
        }

    /**
     * GET  /sal-ini-57-bs/:id : get the "id" salIni57B.
     *
     * @param id the id of the salIni57B to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salIni57B, or with status 404 (Not Found)
     */
    @GetMapping("/sal-ini-57-bs/{id}")
    @Timed
    public ResponseEntity<SalIni57B> getSalIni57B(@PathVariable Long id) {
        log.debug("REST request to get SalIni57B : {}", id);
        SalIni57B salIni57B = salIni57BService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salIni57B));
    }

    /**
     * DELETE  /sal-ini-57-bs/:id : delete the "id" salIni57B.
     *
     * @param id the id of the salIni57B to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sal-ini-57-bs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalIni57B(@PathVariable Long id) {
        log.debug("REST request to delete SalIni57B : {}", id);
        salIni57BService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
