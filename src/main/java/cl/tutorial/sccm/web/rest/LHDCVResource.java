package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.LHDCV;
import cl.tutorial.sccm.service.LHDCVService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LHDCV.
 */
@RestController
@RequestMapping("/api")
public class LHDCVResource 
{
    private final Logger log = LoggerFactory.getLogger(LHDCVResource.class);

    private static final String ENTITY_NAME = "lHDCV";

    private final LHDCVService lHDCVService;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public LHDCVResource(CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService, LHDCVService lHDCVService) 
	{
        this.lHDCVService = lHDCVService;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /l-hdcvs : Create a new lHDCV.
     *
     * @param lHDCV the lHDCV to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lHDCV, or with status 400 (Bad Request) if the lHDCV has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/l-hdcvs")
    @Timed
    public ResponseEntity<LHDCV> createLHDCV(@RequestBody LHDCV lHDCV) throws URISyntaxException 
	{
        log.debug("REST request to save LHDCV : {}", lHDCV);
        
		if (lHDCV.getId() != null) { throw new BadRequestAlertException("A new lHDCV cannot already have an ID", ENTITY_NAME, "idexists"); }
		
        LHDCV result = lHDCVService.save(lHDCV);
        
		return ResponseEntity.created(new URI("/api/l-hdcvs/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /l-hdcvs : Updates an existing lHDCV.
     *
     * @param lHDCV the lHDCV to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lHDCV,
     * or with status 400 (Bad Request) if the lHDCV is not valid,
     * or with status 500 (Internal Server Error) if the lHDCV couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/l-hdcvs")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateLHDCV(@RequestBody LHDCV lHDCV) throws URISyntaxException 
	{
        log.debug("REST request to update LHDCV : {}", lHDCV);
        
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("LHDCV");
		cabeceraControlCambio.setIdRegistro(lHDCV.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
		
		DetalleControlCambio resultado = guardaCampo(result, "rut", String.valueOf(lHDCV.getRut()), "C");
		resultado = guardaCampo(result, "dv", lHDCV.getDv(), "C");		
		resultado = guardaCampo(result, "producto", String.valueOf(lHDCV.getProducto()), "L");
		resultado = guardaCampo(result, "fechaPago", lHDCV.getFechaPago().toString(), "F");
		resultado = guardaCampo(result, "correlativo", String.valueOf(lHDCV.getCorrelativo().toString()), "C");	
		resultado = guardaCampo(result, "fechaInv", lHDCV.getFechaInv().toString(), "F");	
		resultado = guardaCampo(result, "folio", lHDCV.getFolio(), "C");
		resultado = guardaCampo(result, "capital", String.format("%.2f", lHDCV.getCapital()).replace(",", "."), "C");
		resultado = guardaCampo(result, "capMon", String.format("%.2f", lHDCV.getCapMon()).replace(",", "."), "C");
		resultado = guardaCampo(result, "intNom", lHDCV.getIntNom().toString(), "C");
		resultado = guardaCampo(result, "intReal", lHDCV.getIntReal().toString(), "C");
		resultado = guardaCampo(result, "monFi", lHDCV.getMonFi().toString(), "C");
		resultado = guardaCampo(result, "numGiro", lHDCV.getNumGiro().toString(), "C");
		resultado = guardaCampo(result, "codMon", lHDCV.getCodMon().toString(), "C");	
		resultado = guardaCampo(result, "monTasa", lHDCV.getMonTasa().toString(), "C");
		resultado = guardaCampo(result, "tasaEmi", lHDCV.getTasaEmi().toString(), "C");
		resultado = guardaCampo(result, "nomCli", lHDCV.getNomCli(), "C");
		resultado = guardaCampo(result, "codInst", lHDCV.getCodInst().toString(), "C");
		resultado = guardaCampo(result, "serieInst", lHDCV.getSerieInst(), "C");
		resultado = guardaCampo(result, "perTasa", lHDCV.getPerTasa().toString(), "C");	
		resultado = guardaCampo(result, "tipoInt", lHDCV.getTipoInt(), "C");
		resultado = guardaCampo(result, "codTipoInt", lHDCV.getCodTipoInt().toString(), "C");	
		resultado = guardaCampo(result, "fecVcto", lHDCV.getFecVcto().toString(), "F");
		resultado = guardaCampo(result, "onp", lHDCV.getOnp(), "C");	
		resultado = guardaCampo(result, "estCta", lHDCV.getEstCta(), "C");	
		resultado = guardaCampo(result, "fecCont", lHDCV.getFecCont().toString(), "F");
		resultado = guardaCampo(result, "bipersonal", lHDCV.getBipersonal(), "C");
		resultado = guardaCampo(result, "reajustabilidad", lHDCV.getReajustabilidad(), "C");	
		resultado = guardaCampo(result, "plazo", lHDCV.getPlazo().toString(), "C");
		resultado = guardaCampo(result, "indPago", lHDCV.getIndPago(), "C");	
		resultado = guardaCampo(result, "extranjero", lHDCV.getExtranjero(), "C");	
		resultado = guardaCampo(result, "impExt", lHDCV.getImpExt().toString(), "C");
		resultado = guardaCampo(result, "numCupones", lHDCV.getNumCupones().toString(), "C");
		resultado = guardaCampo(result, "numCuponPago", lHDCV.getNumCuponPago().toString(), "C");		
		resultado = guardaCampo(result, "ctaCap", lHDCV.getCtaCap().toString(), "C");			
		resultado = guardaCampo(result, "ctaReaj", lHDCV.getCtaReaj().toString(), "C");	
		resultado = guardaCampo(result, "ctaInt", lHDCV.getCtaInt().toString(), "C");	
		resultado = guardaCampo(result, "origen", lHDCV.getOrigen(), "C");	
		resultado = guardaCampo(result, "annoTributa", String.valueOf(lHDCV.getAnnoTributa().getId()), "L");
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).body(result);	
    }
	
	/**
     * PUT  /l-hdcvs : Updates an existing lHDCV.
     *
     * @param lHDCV the lHDCV to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lHDCV,
     * or with status 400 (Bad Request) if the lHDCV is not valid,
     * or with status 500 (Internal Server Error) if the lHDCV couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/l-hdcvs/actualizar")
    @Timed
    public ResponseEntity<LHDCV> actualizarLHDCV(@RequestBody LHDCV lHDCV) throws URISyntaxException 
	{
        log.debug("REST request to update LHDCV : {}", lHDCV);
        
        LHDCV result = lHDCVService.save(lHDCV);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lHDCV.getId().toString())).body(result);
    }
	
    /**
     * GET  /l-hdcvs : get all the lHDCVS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of lHDCVS in body
     */
    @GetMapping("/l-hdcvs")
    @Timed
    public List<LHDCV> getAllLHDCVS() 
	{
        log.debug("REST request to get all LHDCVS");
        
		return lHDCVService.findAll();
    }

    /**
     * GET  /l-hdcvs/:id : get the "id" lHDCV.
     *
     * @param id the id of the lHDCV to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lHDCV, or with status 404 (Not Found)
     */
    @GetMapping("/l-hdcvs/{id}")
    @Timed
    public ResponseEntity<LHDCV> getLHDCV(@PathVariable Long id) 
	{
        log.debug("REST request to get LHDCV : {}", id);
        
		LHDCV lHDCV = lHDCVService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lHDCV));
    }

    /**
     * DELETE  /l-hdcvs/:id : delete the "id" lHDCV.
     *
     * @param id the id of the lHDCV to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/l-hdcvs/{id}")
    @Timed
    public ResponseEntity<Void> deleteLHDCV(@PathVariable Long id) 
	{
        log.debug("REST request to delete LHDCV : {}", id);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("LHDCV");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /l-hdcvs/:id : delete the "id" lHDCV.
     *
     * @param id the id of the lHDCV to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/l-hdcvs/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarLHDCV(@PathVariable Long id) 
	{
        log.debug("REST request to delete LHDCV : {}", id);
        
		lHDCVService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}
