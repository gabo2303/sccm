package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.Entrada57b;
import cl.tutorial.sccm.service.Entrada57bService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Entrada57b.
 */
@RestController
@RequestMapping("/api")
public class Entrada57bResource {

    private final Logger log = LoggerFactory.getLogger(Entrada57bResource.class);

    private static final String ENTITY_NAME          = "entrada57b";
    private static final String SUCEESS_REPRO        = "successReprocess";
    private static final String SUCEESS_EXE_FIRST_SP = "successExecuteFirstSp";

    @Autowired
    private final Entrada57bService entrada57bService;

    public Entrada57bResource(Entrada57bService entrada57bService) {
        this.entrada57bService = entrada57bService;

    }

    /**
     * POST  /entrada-57-bs : Create a new entrada57b.
     *
     * @param entrada57b
     * 		the entrada57b to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new entrada57b, or with status 400 (Bad Request) if the entrada57b has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/entrada-57-bs")
    @Timed
    public ResponseEntity<Entrada57b> createEntrada57b(@RequestBody Entrada57b entrada57b) throws URISyntaxException {
        log.debug("REST request to save Entrada57b : {}", entrada57b);
        if (entrada57b.getId() != null) {
            throw new BadRequestAlertException("A new entrada57b cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Entrada57b result = entrada57bService.save(entrada57b);
        return ResponseEntity.created(new URI("/api/entrada-57-bs/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /entrada-57-bs : Updates an existing entrada57b.
     *
     * @param entrada57b
     * 		the entrada57b to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated entrada57b, or with status 400 (Bad Request) if the entrada57b is not valid, or with status 500
     * (Internal Server Error) if the entrada57b couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/entrada-57-bs")
    @Timed
    public ResponseEntity<Entrada57b> updateEntrada57b(@RequestBody Entrada57b entrada57b) throws URISyntaxException {
        log.debug("REST request to update Entrada57b : {}", entrada57b);
        if (entrada57b.getId() == null) {
            return createEntrada57b(entrada57b);
        }
        Entrada57b result = entrada57bService.save(entrada57b);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, entrada57b.getId().toString())).body(result);
    }

    /**
     * GET  /entrada-57-bs : get all the entrada57bs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of entrada57bs in body
     */
    @GetMapping("/entrada-57-bs")
    @Timed
    public List<Entrada57b> getAllEntrada57bs() {
        log.debug("REST request to get all Entrada57bs");
        return entrada57bService.findAll();
    }

    /**
     * GET  /entrada-57-bs/:id : get the "id" entrada57b.
     *
     * @param id
     * 		the id of the entrada57b to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the entrada57b, or with status 404 (Not Found)
     */
    @GetMapping("/entrada-57-bs/{id}")
    @Timed
    public ResponseEntity<Entrada57b> getEntrada57b(@PathVariable Long id) {
        log.debug("REST request to get Entrada57b : {}", id);
        Entrada57b entrada57b = entrada57bService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(entrada57b));
    }

    /**
     * DELETE  /entrada-57-bs/:id : delete the "id" entrada57b.
     *
     * @param id
     * 		the id of the entrada57b to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/entrada-57-bs/{id}")
    @Timed
    public ResponseEntity<Void> deleteEntrada57b(@PathVariable Long id) {
        log.debug("REST request to delete Entrada57b : {}", id);
        entrada57bService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/entrada-57-bs/executeSp1InsertaEntrada57B")
    @Timed
    public ResponseEntity<Void> executeSp1InsertaEntrada57B() {
        log.debug("REST request to executeSp1InsertaEntrada57B:");
        this.entrada57bService.executeSp1InsertaEntrada57B();
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCustomAlert(ENTITY_NAME, "1", SUCEESS_EXE_FIRST_SP)).build();
    }

    @GetMapping("/entrada-57-bs/{idAnno}/reprocessE57B")
    @Timed
    public ResponseEntity<Void> reprocessE57B(@PathVariable Integer idAnno) {
        log.debug("REST request to reprocessE57B:");
        this.entrada57bService.reprocessE57B(idAnno);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCustomAlert(ENTITY_NAME, "1", SUCEESS_REPRO)).build();
    }

    @GetMapping("/entrada-57-bs/{idAnno}/getByAnnoId")
    @Timed
    public List<Entrada57b> findAllByAnnoTributaId(@PathVariable Long idAnno) {
        log.debug("REST request to reprocessE57B:");
        return this.entrada57bService.findAllByAnnoTributaId(idAnno);
    }
}
