package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.Aprobacion;
import cl.tutorial.sccm.repository.AnnoTributaRepository;
import cl.tutorial.sccm.repository.AprobacionRepository;
import cl.tutorial.sccm.security.SecurityUtils;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Aprobacion.
 */
@RestController
@RequestMapping("/api")
public class AprobacionResource {

    private final Logger log = LoggerFactory.getLogger(AprobacionResource.class);

    private static final String ENTITY_NAME = "aprobacion";

    private final AprobacionRepository  aprobacionRepository;
    private final AnnoTributaRepository annoTributaRepository;

    public AprobacionResource(AprobacionRepository aprobacionRepository, AnnoTributaRepository annoTributaRepository) {
        this.aprobacionRepository = aprobacionRepository;
        this.annoTributaRepository = annoTributaRepository;
    }

    /**
     * POST  /aprobacions : Create a new aprobacion.
     *
     * @param aprobacion
     * 		the aprobacion to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new aprobacion, or with status 400 (Bad Request) if the aprobacion has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/aprobacions")
    @Timed
    public ResponseEntity<Aprobacion> createAprobacion(@RequestBody Aprobacion aprobacion) throws URISyntaxException {
        log.debug("REST request to save Aprobacion : {}", aprobacion);
        if (aprobacion.getId() != null) {
            throw new BadRequestAlertException("A new aprobacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String userName = SecurityUtils.getCurrentUserLogin();
        aprobacion.setUsuario(userName);
        aprobacion.setFechaUltimaModificacion(LocalDate.now());

        final AnnoTributa annoTributa = annoTributaRepository.findOne(aprobacion.getAnnoTributa().getId());
        final List<Aprobacion> aprobacionList = aprobacionRepository.findAllByAnnoTributaAndProducto(annoTributa, aprobacion.getProducto());
        if (aprobacionList != null && !aprobacionList.isEmpty()) {
            throw new BadRequestAlertException("No pueden existir dos aprobaciones para el mismo producto y el mismo año tributario.", ENTITY_NAME, "duplicateApproval");
        }
        Aprobacion result = aprobacionRepository.save(aprobacion);
        return ResponseEntity.created(new URI("/api/aprobacions/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /aprobacions : Updates an existing aprobacion.
     *
     * @param aprobacion
     * 		the aprobacion to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated aprobacion, or with status 400 (Bad Request) if the aprobacion is not valid, or with status 500
     * (Internal Server Error) if the aprobacion couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/aprobacions")
    @Timed
    public ResponseEntity<Aprobacion> updateAprobacion(@RequestBody Aprobacion aprobacion) throws URISyntaxException {
        log.debug("REST request to update Aprobacion : {}", aprobacion);
        if (aprobacion.getId() == null) {
            return createAprobacion(aprobacion);
        }
        Aprobacion result = aprobacionRepository.save(aprobacion);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aprobacion.getId().toString())).body(result);
    }

    /**
     * GET  /aprobacions : get all the aprobacions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of aprobacions in body
     */
    @GetMapping("/aprobacions")
    @Timed
    public List<Aprobacion> getAllAprobacions() {
        log.debug("REST request to get all Aprobacions");
        return aprobacionRepository.findAll();
    }

    /**
     * GET  /aprobacions/:id : get the "id" aprobacion.
     *
     * @param id
     * 		the id of the aprobacion to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the aprobacion, or with status 404 (Not Found)
     */
    @GetMapping("/aprobacions/{id}")
    @Timed
    public ResponseEntity<Aprobacion> getAprobacion(@PathVariable Long id) {
        log.debug("REST request to get Aprobacion : {}", id);
        Aprobacion aprobacion = aprobacionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(aprobacion));
    }

    /**
     * DELETE  /aprobacions/:id : delete the "id" aprobacion.
     *
     * @param id
     * 		the id of the aprobacion to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/aprobacions/{id}")
    @Timed
    public ResponseEntity<Void> deleteAprobacion(@PathVariable Long id) {
        log.debug("REST request to delete Aprobacion : {}", id);
        aprobacionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /findAllByProduct : get all the aprobacions by product.
     *
     * @param product
     * 		nombre del producto
     *
     * @return the ResponseEntity with status 200 (OK) and the list of aprobacions in body
     */
    @GetMapping("/aprobacions/findAllByProduct")
    @Timed
    public List<Aprobacion> findAllByProducto(@RequestParam(name = "product") String product) {
        log.debug("REST request to get all Aprobacions by product -> " + product);
        return aprobacionRepository.findAllByProducto(product);
    }
}
