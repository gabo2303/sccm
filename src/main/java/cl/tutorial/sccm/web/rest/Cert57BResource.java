package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Cert57B;
import cl.tutorial.sccm.service.Cert57BService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Cert57B.
 */
@RestController
@RequestMapping("/api")
public class Cert57BResource {

    private final Logger log = LoggerFactory.getLogger(Cert57BResource.class);

    private static final String ENTITY_NAME = "cert57B";

    private final Cert57BService cert57BService;

    public Cert57BResource(Cert57BService cert57BService) {
        this.cert57BService = cert57BService;
    }

    /**
     * POST  /cert-57-bs : Create a new cert57B.
     *
     * @param cert57B the cert57B to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cert57B, or with status 400 (Bad Request) if the cert57B has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cert-57-bs")
    @Timed
    public ResponseEntity<Cert57B> createCert57B(@Valid @RequestBody Cert57B cert57B) throws URISyntaxException {
        log.debug("REST request to save Cert57B : {}", cert57B);
        if (cert57B.getId() != null) {
            throw new BadRequestAlertException("A new cert57B cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Cert57B result = cert57BService.save(cert57B);
        return ResponseEntity.created(new URI("/api/cert-57-bs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cert-57-bs : Updates an existing cert57B.
     *
     * @param cert57B the cert57B to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cert57B,
     * or with status 400 (Bad Request) if the cert57B is not valid,
     * or with status 500 (Internal Server Error) if the cert57B couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cert-57-bs")
    @Timed
    public ResponseEntity<Cert57B> updateCert57B(@Valid @RequestBody Cert57B cert57B) throws URISyntaxException {
        log.debug("REST request to update Cert57B : {}", cert57B);
        if (cert57B.getId() == null) {
            return createCert57B(cert57B);
        }
        Cert57B result = cert57BService.save(cert57B);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cert57B.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cert-57-bs : get all the cert57BS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cert57BS in body
     */
    @GetMapping("/cert-57-bs")
    @Timed
    public List<Cert57B> getAllCert57BS() {
        log.debug("REST request to get all Cert57BS");
        return cert57BService.findAll();
        }

    /**
     * GET  /cert-57-bs/:id : get the "id" cert57B.
     *
     * @param id the id of the cert57B to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cert57B, or with status 404 (Not Found)
     */
    @GetMapping("/cert-57-bs/{id}")
    @Timed
    public ResponseEntity<Cert57B> getCert57B(@PathVariable Long id) {
        log.debug("REST request to get Cert57B : {}", id);
        Cert57B cert57B = cert57BService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cert57B));
    }

    /**
     * DELETE  /cert-57-bs/:id : delete the "id" cert57B.
     *
     * @param id the id of the cert57B to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cert-57-bs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCert57B(@PathVariable Long id) {
        log.debug("REST request to delete Cert57B : {}", id);
        cert57BService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/cert-57-bs/{idAnno}/getByAnnoId")
    @Timed
    public List<Cert57B>  findAllByAnnoTributaId(@PathVariable Long idAnno){
        log.debug("REST request to reprocessE57B:");
        return  this.cert57BService.findAllByAnnoTributaId(idAnno);
    }
}
