package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.service.StorageService;
import cl.tutorial.sccm.service.exception.JarExecutionException;
import cl.tutorial.sccm.service.exception.NotAllProductsAreApprovedException;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import java.io.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/api/storage")
public class StorageController 
{
    private Logger       log   = LoggerFactory.getLogger(this.getClass().getName());
    private List<String> files = new ArrayList<>();
	private static String OS = System.getProperty("os.name").toLowerCase();
	
    private StorageService storageService;

    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/getallfiles")
    @Timed
    public ResponseEntity<List<String>> getListFiles(Model model) {
        List<String> fileNames =
            files.stream().map(fileName -> MvcUriComponentsBuilder.fromMethodName(StorageController.class, "getFile", fileName).build().toString()).collect(Collectors.toList());

        return ResponseEntity.ok().body(fileNames);
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    @Timed
    @Deprecated
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.loadFile(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    //COMIENZO MÉTODOS PAGO DIVIDENDOS
    @PostMapping("/uploadInterfaces")
    @Timed
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) 
	{
        String message = "";
		
		this.log.info("ENTRA AL UPLOAD INTERFACES");
        
		try 
		{
            storageService.store(file);
            files.add(file.getOriginalFilename());
			
            message = "Interface " + file.getOriginalFilename() + " cargada!";
            System.out.println("file.getOriginalFilename(): " + file.getOriginalFilename());
               
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (Exception e) 
		{
            message = "Falló la carga de la interface. " + file.getOriginalFilename() + "!";
            
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/listaCertificados")
    @Timed
    public ResponseEntity<String> listaCertificados() 
	{
        String message = "";
        String lista = "";
        
		try 
		{
            lista = storageService.emiteTxtCertificados();
            return ResponseEntity.status(HttpStatus.OK).body(lista);
        }
		catch (Exception e)
		{
            message = "Falló la emisión de certificados. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/mergeInterfaces")
    @Timed
    public ResponseEntity<String> mergeInterface() {
        String message = "";
        try {
            storageService.mergeUploadedIFaces();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Falló la unión de la interfaces. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }
	
    @PostMapping("/load1941/{filename:.+}")
    @Timed
    public ResponseEntity<String> loadInterface1941(@PathVariable String filename) 
	{
        String message = "";
        
		this.log.info("ENTRA AL LOAD 1941");
		
		try 
		{
            storageService.loadInterface1941(filename);
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (Exception e) 
		{
            message = "Falló la carga de la interface. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/validateInterfaces")
    @Timed
    public ResponseEntity<String> validateInterface() {
        String message = "";
        try {
            if ("200".equals(storageService.validateUploadedIFaces())) {
                return ResponseEntity.status(HttpStatus.OK).body(message);
            } else {
                message = " La interfaz contiene errores. ";
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(message);
            }
        } catch (Exception e) {
            message = "Ocurrió un error validando interfaces: " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/emiteCertificados")
    @Timed
    public ResponseEntity<String> emiteCertificadosPagoDvidendos(@RequestParam("rut") String rut, @RequestParam("anno") String anno) 
	{
        String message = "";
        
		try 
		{
            if (storageService.emiteCertificadosPagoDvidendos(rut, anno)) 
			{
                message = "OK, Se generaron certificados correctamente";
				
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
			
            message = "ERROR, Ocurrió un error en la emisión del certificado.";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (JarExecutionException e) 
		{
            message = e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        } 
		catch (Exception e) 
		{
            message = "ERROR, Falló la emisión de certificados. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/emiteDJ")
    @Timed
    public ResponseEntity<String> emiteDJPagoDvidendos(@RequestParam("anno") String anno) 
	{
        String message = "";
        
		try 
		{
            if (storageService.emiteDjPagoDvidendos(anno)) 
			{
                message = "OK, Se generó declaración jurada correctamente";
                
				return ResponseEntity.status(HttpStatus.OK).body(message);
            }
			
            message = "ERROR, Ocurrió un error en la emisión de la declaración jurada.";
            return ResponseEntity.status(HttpStatus.OK).body(message);			
        } 
		catch (JarExecutionException e) 
		{
            message = e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        } 
		catch (Exception e) 
		{
            message = "ERROR, Falló la emisión de declaración jurada. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }
    //FIN MÉTODOS PAGO DIVIDENDOS

    //COMIENZO MÉTODOS 57 BIS
    @PostMapping("/uploadInterfaces57Bis")
    @Timed
    public ResponseEntity<String> handleFileUpload57Bis(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            storageService.store57Bis(file);
            files.add(file.getOriginalFilename());
            message = "Interface " + file.getOriginalFilename() + " cargada!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Falló la carga de la interface. " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/executeAllSps57Bis")
    @Timed
    public ResponseEntity<String> executeAllSps57Bis() {
        String message = "";
        try {
            if (storageService.executeAllSps()) {
                message = "Se cargaron tablas correctamete";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
            message = "Falló la carga de la interface. ";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);

        } catch (javax.persistence.PersistenceException e) {
            message = "Falló la carga de la interface. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/emiteCertificados57Bis")
    @Timed
    public ResponseEntity<String> emiteCertificados57Bis(@RequestParam("rut") String rut, @RequestParam("anno") String anno) {
        String message = "";
        try {
            if (storageService.emiteCertificados57Bis(rut, anno)) {
                message = "OK, Se generaron certificados correctamente";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
            message = "ERROR, Ocurrió un error en la emisión del certificado.";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (JarExecutionException e) {
            message = e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        } catch (Exception e) {
            message = "ERROR, Falló la emisión de certificados. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/emiteDJ57B")
    @Timed
    public ResponseEntity<String> emiteDJ57B(@RequestParam("anno") String anno) {
        String message = "";
        try {
            if (storageService.emiteDJInversiones57Bis(anno)) {
                message = "OK, Se generó declaración jurada correctamente.";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
            message = "ERROR, Ocurrió un error en la emisión de la declaración jurada.";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (JarExecutionException e) {
            message = e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        } catch (Exception e) {
            message = "ERROR, Falló la emisión de declaración jurada. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }
    //FIN MÉTODOS 57 BIS

    //COMIENZO OPERACIÓN CAPTACIÓN

    /**
     * Servicio para emitir los certificados de operación de captación.
     * @param rut
     * @param taxYear
     * @return
     */
    @PostMapping("/emiteCertificadosOperacionesCaptacion")
    @Timed
    public ResponseEntity<String> emiteCertificadosOperacionesCaptacion(@RequestParam("rut") String rut, @RequestParam("anno") String taxYear) 
	{
        String message = "";
        
		try 
		{
            if (storageService.emiteCertificadosOperacionesCaptacion(rut, taxYear)) 
			{
                message = "OK, Se generaron certificados correctamente.";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
            
			message = "ERROR, Ocurrió un error en la emisión del certificado.";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (JarExecutionException | NotAllProductsAreApprovedException e) 
		{
            message = "ERROR, " + e.getMessage();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (Exception e) 
		{
            message = "ERROR, Falló la emisión de certificados. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        }
    }

    /**
     * Servicio para emitir la declaración jurada de operación de captación.
     * @param taxYear
     * @return
     */
    @PostMapping("/emiteDJOperacionesCaptacion")
    @Timed
    public ResponseEntity<String> emiteDJOperacionesCaptacion(@RequestParam("anno") String taxYear) 
	{
        String message = "";
        
		try 
		{
            if (storageService.emiteDJOperacionesCaptacion(taxYear)) 
			{
                message = "OK, Se generó declaración jurada correctamente.";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
            
			message = "ERROR, Ocurrió un error en la emisión de la declaración jurada.";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (JarExecutionException | NotAllProductsAreApprovedException e) 
		{
            message = "ERROR, " + e.getMessage();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (Exception e) 
		{
            message = "ERROR, Falló la emisión de la declaración jurada. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        }
    }

    /**
     * Servicio para emitir la rectificatoria de operación de captación.
     * @param taxYear
     * @return
     */
    @PostMapping("/emiteRectifOperacionesCaptacion")
    @Timed
    public ResponseEntity<String> emiteRectifOperacionesCaptacion(@RequestParam("anno") String taxYear) {
        String message = "";
        try {
            if (storageService.emiteRectifOperacionesCaptacion(taxYear)) {
                message = "OK, Se generó la rectificatoria correctamente.";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            }
            message = "ERROR, Ocurrió un error en la emisión de la rectificatoria.";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (JarExecutionException | NotAllProductsAreApprovedException e) {
            message = "ERROR, " + e.getMessage();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "ERROR, Falló la emisión de la rectificatoria. " + e.getMessage();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        }
    }

    @PostMapping("/uplIntOpeCapPactos")
    @Timed
    public ResponseEntity<String> hanFilUplOpeCapPactos(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            storageService.storeOpeCapPactos(file);
            files.add(file.getOriginalFilename());
            message = "Interface " + file.getOriginalFilename() + " cargada!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Falló la carga de la interface. " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/uplIntOpeCapAhorro")
    @Timed
    public ResponseEntity<String> hanFilUplOpeCapAhorro(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            System.out.println("INIT SERVICE uplIntOpeCapAhorro");
            storageService.storeOpeCapAhorro(file);
            files.add(file.getOriginalFilename());
            message = "Interface " + file.getOriginalFilename() + " cargada!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Falló la carga de la interface. " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping("/uplIntOpeCapAhorroMov")
    @Timed
    public ResponseEntity<String> hanFilUplOpeCapAhorroMov(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            System.out.println("INIT SERVICE uplIntOpeCapAhorroMov");
            storageService.storeOpeCapAhorroMov(file);
            files.add(file.getOriginalFilename());
            message = "Interface " + file.getOriginalFilename() + " cargada!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Falló la carga de la interface. " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    //FIN MÉTODO OPERACIÓN CAPTACIÓN

    //INICIO MÉTODOS CARGA DE INTERFACES, BONOS Y LETRAS

    @PostMapping("/uploadEtlDataWithOption")
    @Timed
    public ResponseEntity<String> handleFileUploadCargaInterfaces(@RequestParam("file") MultipartFile file, @RequestParam("option") String option) 
	{
        String message = "";
        Boolean isValidOption = true;
        this.log.info("CALLING SERVICE uploadEtlDataWithOption whit option => " + option);
        
		try 
		{
            switch (option) 
			{
                case Constants.INTERFACES_LOADING_ACCOUNTING:
                case Constants.INTERFACES_LOADING_EURO:
                case Constants.INTERFACES_LOADING_PARITY:
                case Constants.INTERFACES_LOADING_UF:
                case Constants.INTERFACES_LOADING_USD:
                case Constants.BONOS_LETRAS_LOAD_BONOS:
                case Constants.BONOS_LETRAS_LOAD_LHBONOS:
                case Constants.BONOS_LETRAS_LOAD_LHDCV:
                case Constants.DATA_LOAD_ETL_DAP:
                case Constants.DATA_LOAD_ETL_PACTOS:
                case Constants.DATA_LOAD_ETL_AHORROS:
                case Constants.DATA_LOAD_ETL_AHORROS_MOVIMIENTOS:
				case Constants.DATA_LOAD_ETL_RUT_1890:
				case Constants.DATA_LOAD_ETL_CLIENTE_1890:
                    storageService.handleFileUploadCargaInterfaces(file, option);
                    files.add(file.getOriginalFilename());
                    break;
                default:
                    isValidOption = false;
                    message = "La opción del servicio no es válida. Opción: " + option + ".";
                    break;
            }
			
            if (!isValidOption) 
			{
                this.log.error(message);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
            }
			
            message = "¡Interface " + file.getOriginalFilename() + " cargada!";
            this.log.info(message);
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } 
		catch (Exception e) 
		{
            message = "¡Falló la carga del archivo " + file.getOriginalFilename() + " para " + option + "!";
            
			this.log.error(message);			
            this.log.error(e.getMessage());
			
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    //FIN MÉTODOS CARGA DE INTERFACES, BONOS Y LETRAS
	
	/**
     * GET  /getDoc : .
     *
     * @return the ResponseEntity with status 200 (OK) and the list of  in body
     */
    @GetMapping("/getDocxFile")
    @Timed
    public ResponseEntity<byte[]> getManuales(@RequestParam(name = "archivo", required = false) String nombreArchivo) 
	{
		final String currentDir = System.getProperty("user.dir");
		
		log.debug(currentDir);
        log.debug("Le voy a devolver el BLOB");
		log.debug("El archivo es " + nombreArchivo);
		
		String ruta = "";
		
		if(isWindows()) { ruta = "C:/" + ruta; }  
		
		else { ruta = currentDir + "/paso/log"; }
		
		File dir = new File(ruta);
		
		File[] matches = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				return name.startsWith(nombreArchivo) && name.endsWith(".docx");
			}
		});
		
		log.debug("VA A ENTRAR AL CICLO");
		log.debug(String.valueOf(matches.length));
		
		File archivo = null;
		
		for (int i = 0; i < matches.length; i++) 
		{
			log.debug(matches[i].getName());
			log.debug(matches[i].getAbsolutePath());
			
			archivo = matches[i];
		}	 
		
        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
		try 
		{
            inputStream = new FileInputStream(archivo.getAbsolutePath());

            byte[] buffer = new byte[1024];
            baos = new ByteArrayOutputStream();
			
            int bytesRead;
            
			while ((bytesRead = inputStream.read(buffer)) != -1) { baos.write(buffer, 0, bytesRead); }       
		} 
		catch (FileNotFoundException e) { e.printStackTrace(); } 
		
		catch (IOException e) { e.printStackTrace(); } 
		
		finally { if (inputStream != null) { try { inputStream.close(); } catch (IOException e) { e.printStackTrace(); } } }
		
        return new ResponseEntity<byte[]>(baos.toByteArray(), HttpStatus.OK);
    }
	
	/**
     * GET  /getDoc : .
     *
     * @return the ResponseEntity with status 200 (OK) and the list of  in body
     */
    @GetMapping("/getLogsEtl")
    @Timed
    public ResponseEntity<byte[]> getAnyLogsEtl(@RequestParam(name = "archivo", required = false) String nombreArchivo) 
	{
		final String currentDir = System.getProperty("user.dir");
		
		log.debug(currentDir);
        log.debug("Le voy a devolver el BLOB");
		log.debug("El archivo es " + nombreArchivo);
		
		String ruta = "";
		
		if(isWindows()) { ruta = "C:/"; }  
		
		else { ruta = currentDir + "/paso/log"; }
		
		File dir = new File(ruta);
		
		File[] matches = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				return name.startsWith(nombreArchivo) && name.endsWith(".csv");
			}
		});
		
		log.debug("VA A ENTRAR AL CICLO");
		log.debug(String.valueOf(matches.length));
		
		File archivo = null;
		
		for (int i = 0; i < matches.length; i++) 
		{
			log.debug(matches[i].getName());
			log.debug(matches[i].getAbsolutePath());
			
			archivo = matches[i];
		}	 
		
        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
		try 
		{
            inputStream = new FileInputStream(archivo.getAbsolutePath());

            byte[] buffer = new byte[1024];
            baos = new ByteArrayOutputStream();
			
            int bytesRead;
            
			while ((bytesRead = inputStream.read(buffer)) != -1) { baos.write(buffer, 0, bytesRead); }       
		} 
		catch (FileNotFoundException e) { e.printStackTrace(); } 
		
		catch (IOException e) { e.printStackTrace(); } 
		
		finally { if (inputStream != null) { try { inputStream.close(); } catch (IOException e) { e.printStackTrace(); } } }
		
        return new ResponseEntity<byte[]>(baos.toByteArray(), HttpStatus.OK);
    }
	
	public static boolean isWindows() { return (OS.indexOf("win") >= 0); }
}