package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Euro;
import cl.tutorial.sccm.service.EuroService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Euro.
 */
@RestController
@RequestMapping("/api")
public class EuroResource {

    private final Logger log = LoggerFactory.getLogger(EuroResource.class);

    private static final String ENTITY_NAME = "euro";

    private final EuroService euroService;

    public EuroResource(EuroService euroService) {
        this.euroService = euroService;
    }

    /**
     * POST  /euros : Create a new euro.
     *
     * @param euro the euro to create
     * @return the ResponseEntity with status 201 (Created) and with body the new euro, or with status 400 (Bad Request) if the euro has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/euros")
    @Timed
    public ResponseEntity<Euro> createEuro(@RequestBody Euro euro) throws URISyntaxException {
        log.debug("REST request to save Euro : {}", euro);
        if (euro.getId() != null) {
            throw new BadRequestAlertException("A new euro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Euro result = euroService.save(euro);
        return ResponseEntity.created(new URI("/api/euros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /euros : Updates an existing euro.
     *
     * @param euro the euro to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated euro,
     * or with status 400 (Bad Request) if the euro is not valid,
     * or with status 500 (Internal Server Error) if the euro couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/euros")
    @Timed
    public ResponseEntity<Euro> updateEuro(@RequestBody Euro euro) throws URISyntaxException {
        log.debug("REST request to update Euro : {}", euro);
        if (euro.getId() == null) {
            return createEuro(euro);
        }
        Euro result = euroService.save(euro);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, euro.getId().toString()))
            .body(result);
    }

    /**
     * GET  /euros : get all the euros.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of euros in body
     */
    @GetMapping("/euros")
    @Timed
    public List<Euro> getAllEuros() {
        log.debug("REST request to get all Euros");
        return euroService.findAll();
        }

    /**
     * GET  /euros/:id : get the "id" euro.
     *
     * @param id the id of the euro to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the euro, or with status 404 (Not Found)
     */
    @GetMapping("/euros/{id}")
    @Timed
    public ResponseEntity<Euro> getEuro(@PathVariable Long id) {
        log.debug("REST request to get Euro : {}", id);
        Euro euro = euroService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(euro));
    }

    /**
     * DELETE  /euros/:id : delete the "id" euro.
     *
     * @param id the id of the euro to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/euros/{id}")
    @Timed
    public ResponseEntity<Void> deleteEuro(@PathVariable Long id) {
        log.debug("REST request to delete Euro : {}", id);
        euroService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
