package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Usd;
import cl.tutorial.sccm.service.UsdService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Usd.
 */
@RestController
@RequestMapping("/api")
public class UsdResource {

    private final Logger log = LoggerFactory.getLogger(UsdResource.class);

    private static final String ENTITY_NAME = "usd";

    private final UsdService usdService;

    public UsdResource(UsdService usdService) {
        this.usdService = usdService;
    }

    /**
     * POST  /usds : Create a new usd.
     *
     * @param usd the usd to create
     * @return the ResponseEntity with status 201 (Created) and with body the new usd, or with status 400 (Bad Request) if the usd has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/usds")
    @Timed
    public ResponseEntity<Usd> createUsd(@RequestBody Usd usd) throws URISyntaxException {
        log.debug("REST request to save Usd : {}", usd);
        if (usd.getId() != null) {
            throw new BadRequestAlertException("A new usd cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Usd result = usdService.save(usd);
        return ResponseEntity.created(new URI("/api/usds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /usds : Updates an existing usd.
     *
     * @param usd the usd to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated usd,
     * or with status 400 (Bad Request) if the usd is not valid,
     * or with status 500 (Internal Server Error) if the usd couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/usds")
    @Timed
    public ResponseEntity<Usd> updateUsd(@RequestBody Usd usd) throws URISyntaxException {
        log.debug("REST request to update Usd : {}", usd);
        if (usd.getId() == null) {
            return createUsd(usd);
        }
        Usd result = usdService.save(usd);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, usd.getId().toString()))
            .body(result);
    }

    /**
     * GET  /usds : get all the usds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of usds in body
     */
    @GetMapping("/usds")
    @Timed
    public List<Usd> getAllUsds() {
        log.debug("REST request to get all Usds");
        return usdService.findAll();
        }

    /**
     * GET  /usds/:id : get the "id" usd.
     *
     * @param id the id of the usd to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the usd, or with status 404 (Not Found)
     */
    @GetMapping("/usds/{id}")
    @Timed
    public ResponseEntity<Usd> getUsd(@PathVariable Long id) {
        log.debug("REST request to get Usd : {}", id);
        Usd usd = usdService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usd));
    }

    /**
     * DELETE  /usds/:id : delete the "id" usd.
     *
     * @param id the id of the usd to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/usds/{id}")
    @Timed
    public ResponseEntity<Void> deleteUsd(@PathVariable Long id) {
        log.debug("REST request to delete Usd : {}", id);
        usdService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
