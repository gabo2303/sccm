package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Fut;
import cl.tutorial.sccm.service.FutService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Fut.
 */
@RestController
@RequestMapping("/api")
public class FutResource {

    private final Logger log = LoggerFactory.getLogger(FutResource.class);

    private static final String ENTITY_NAME = "fut";

    private final FutService futService;

    public FutResource(FutService futService) {
        this.futService = futService;
    }

    /**
     * POST  /futs : Create a new fut.
     *
     * @param fut the fut to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fut, or with status 400 (Bad Request) if the fut has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/futs")
    @Timed
    public ResponseEntity<Fut> createFut(@RequestBody Fut fut) throws URISyntaxException {
        log.debug("REST request to save Fut : {}", fut);
        if (fut.getId() != null) {
            throw new BadRequestAlertException("A new fut cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fut result = futService.save(fut);
        return ResponseEntity.created(new URI("/api/futs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /futs : Updates an existing fut.
     *
     * @param fut the fut to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fut,
     * or with status 400 (Bad Request) if the fut is not valid,
     * or with status 500 (Internal Server Error) if the fut couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/futs")
    @Timed
    public ResponseEntity<Fut> updateFut(@RequestBody Fut fut) throws URISyntaxException {
        log.debug("REST request to update Fut : {}", fut);
        if (fut.getId() == null) {
            return createFut(fut);
        }
        Fut result = futService.save(fut);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, fut.getId().toString()))
            .body(result);
    }

    /**
     * GET  /futs : get all the futs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of futs in body
     */
    @GetMapping("/futs")
    @Timed
    public List<Fut> getAllFuts() {
        log.debug("REST request to get all Futs");
        return futService.findAll();
        }

    /**
     * GET  /futs/:id : get the "id" fut.
     *
     * @param id the id of the fut to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fut, or with status 404 (Not Found)
     */
    @GetMapping("/futs/{id}")
    @Timed
    public ResponseEntity<Fut> getFut(@PathVariable Long id) {
        log.debug("REST request to get Fut : {}", id);
        Fut fut = futService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(fut));
    }

    /**
     * DELETE  /futs/:id : delete the "id" fut.
     *
     * @param id the id of the fut to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/futs/{id}")
    @Timed
    public ResponseEntity<Void> deleteFut(@PathVariable Long id) {
        log.debug("REST request to delete Fut : {}", id);
        futService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/futs/{idAnno}/getByAnnoId")
    @Timed
    public List<Fut>  findAllByAnnoTributaId(@PathVariable Long idAnno){
        log.debug("REST request to Fut findAllByAnnoTributaId:");
        return  this.futService.findAllByAnnoTributaId(idAnno);
    }
}
