package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Directorios;
import cl.tutorial.sccm.service.DirectoriosService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Directorios.
 */
@RestController
@RequestMapping("/api")
public class DirectoriosResource {

    private final Logger log = LoggerFactory.getLogger(DirectoriosResource.class);

    private static final String ENTITY_NAME = "directorios";

    private final DirectoriosService directoriosService;

    public DirectoriosResource(DirectoriosService directoriosService) {
        this.directoriosService = directoriosService;
    }

    /**
     * POST  /directorios : Create a new directorios.
     *
     * @param directorios the directorios to create
     * @return the ResponseEntity with status 201 (Created) and with body the new directorios, or with status 400 (Bad Request) if the directorios has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/directorios")
    @Timed
    public ResponseEntity<Directorios> createDirectorios(@RequestBody Directorios directorios) throws URISyntaxException {
        log.debug("REST request to save Directorios : {}", directorios);
        if (directorios.getId() != null) {
            throw new BadRequestAlertException("A new directorios cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Directorios result = directoriosService.save(directorios);
        return ResponseEntity.created(new URI("/api/directorios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /directorios : Updates an existing directorios.
     *
     * @param directorios the directorios to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated directorios,
     * or with status 400 (Bad Request) if the directorios is not valid,
     * or with status 500 (Internal Server Error) if the directorios couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/directorios")
    @Timed
    public ResponseEntity<Directorios> updateDirectorios(@RequestBody Directorios directorios) throws URISyntaxException {
        log.debug("REST request to update Directorios : {}", directorios);
        if (directorios.getId() == null) {
            return createDirectorios(directorios);
        }
        Directorios result = directoriosService.save(directorios);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, directorios.getId().toString()))
            .body(result);
    }

    /**
     * GET  /directorios : get all the directorios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of directorios in body
     */
    @GetMapping("/directorios")
    @Timed
    public ResponseEntity<List<Directorios>> getAllDirectorios(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Directorios");
        Page<Directorios> page = directoriosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/directorios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /directorios/:id : get the "id" directorios.
     *
     * @param id the id of the directorios to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the directorios, or with status 404 (Not Found)
     */
    @GetMapping("/directorios/{id}")
    @Timed
    public ResponseEntity<Directorios> getDirectorios(@PathVariable Long id) {
        log.debug("REST request to get Directorios : {}", id);
        Directorios directorios = directoriosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(directorios));
    }

    /**
     * DELETE  /directorios/:id : delete the "id" directorios.
     *
     * @param id the id of the directorios to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/directorios/{id}")
    @Timed
    public ResponseEntity<Void> deleteDirectorios(@PathVariable Long id) {
        log.debug("REST request to delete Directorios : {}", id);
        directoriosService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
