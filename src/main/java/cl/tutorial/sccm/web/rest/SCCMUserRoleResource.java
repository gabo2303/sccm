package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.SCCMUserRole;
import cl.tutorial.sccm.service.SCCMUserRoleService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SCCMUserRole.
 */
@RestController
@RequestMapping("/api")
public class SCCMUserRoleResource {

    private final Logger log = LoggerFactory.getLogger(SCCMUserRoleResource.class);

    private static final String ENTITY_NAME = "sCCMUserRole";

    private final SCCMUserRoleService sCCMUserRoleService;

    public SCCMUserRoleResource(SCCMUserRoleService sCCMUserRoleService) {
        this.sCCMUserRoleService = sCCMUserRoleService;
    }

    /**
     * POST  /s-ccm-user-roles : Create a new sCCMUserRole.
     *
     * @param sCCMUserRole the sCCMUserRole to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sCCMUserRole, or with status 400 (Bad Request) if the sCCMUserRole has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/s-ccm-user-roles")
    @Timed
    public ResponseEntity<SCCMUserRole> createSCCMUserRole(@RequestBody SCCMUserRole sCCMUserRole) throws URISyntaxException {
        log.debug("REST request to save SCCMUserRole : {}", sCCMUserRole);
        if (sCCMUserRole.getId() != null) {
            throw new BadRequestAlertException("A new sCCMUserRole cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SCCMUserRole result = sCCMUserRoleService.save(sCCMUserRole);
        return ResponseEntity.created(new URI("/api/s-ccm-user-roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /s-ccm-user-roles : Updates an existing sCCMUserRole.
     *
     * @param sCCMUserRole the sCCMUserRole to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sCCMUserRole,
     * or with status 400 (Bad Request) if the sCCMUserRole is not valid,
     * or with status 500 (Internal Server Error) if the sCCMUserRole couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/s-ccm-user-roles")
    @Timed
    public ResponseEntity<SCCMUserRole> updateSCCMUserRole(@RequestBody SCCMUserRole sCCMUserRole) throws URISyntaxException {
        log.debug("REST request to update SCCMUserRole : {}", sCCMUserRole);
        if (sCCMUserRole.getId() == null) {
            return createSCCMUserRole(sCCMUserRole);
        }
        SCCMUserRole result = sCCMUserRoleService.save(sCCMUserRole);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sCCMUserRole.getId().toString()))
            .body(result);
    }

    /**
     * GET  /s-ccm-user-roles : get all the sCCMUserRoles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sCCMUserRoles in body
     */
    @GetMapping("/s-ccm-user-roles")
    @Timed
    public List<SCCMUserRole> getAllSCCMUserRoles() {
        log.debug("REST request to get all SCCMUserRoles");
        return sCCMUserRoleService.findAll();
        }

    /**
     * GET  /s-ccm-user-roles/:id : get the "id" sCCMUserRole.
     *
     * @param id the id of the sCCMUserRole to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sCCMUserRole, or with status 404 (Not Found)
     */
    @GetMapping("/s-ccm-user-roles/{id}")
    @Timed
    public ResponseEntity<SCCMUserRole> getSCCMUserRole(@PathVariable Long id) {
        log.debug("REST request to get SCCMUserRole : {}", id);
        SCCMUserRole sCCMUserRole = sCCMUserRoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sCCMUserRole));
    }

    /**
     * DELETE  /s-ccm-user-roles/:id : delete the "id" sCCMUserRole.
     *
     * @param id the id of the sCCMUserRole to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/s-ccm-user-roles/{id}")
    @Timed
    public ResponseEntity<Void> deleteSCCMUserRole(@PathVariable Long id) {
        log.debug("REST request to delete SCCMUserRole : {}", id);
        sCCMUserRoleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
