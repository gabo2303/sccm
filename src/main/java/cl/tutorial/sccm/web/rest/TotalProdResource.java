package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.TotalProd;
import cl.tutorial.sccm.service.TotalProdService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TotalProd.
 */
@RestController
@RequestMapping("/api")
public class TotalProdResource {

    private final Logger log = LoggerFactory.getLogger(TotalProdResource.class);

    private static final String ENTITY_NAME = "totalProd";

    private final TotalProdService totalProdService;

    public TotalProdResource(TotalProdService totalProdService) {
        this.totalProdService = totalProdService;
    }

    /**
     * POST  /total-prods : Create a new totalProd.
     *
     * @param totalProd the totalProd to create
     * @return the ResponseEntity with status 201 (Created) and with body the new totalProd, or with status 400 (Bad Request) if the totalProd has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/total-prods")
    @Timed
    public ResponseEntity<TotalProd> createTotalProd(@RequestBody TotalProd totalProd) throws URISyntaxException {
        log.debug("REST request to save TotalProd : {}", totalProd);
        if (totalProd.getId() != null) {
            throw new BadRequestAlertException("A new totalProd cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TotalProd result = totalProdService.save(totalProd);
        return ResponseEntity.created(new URI("/api/total-prods/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /total-prods : Updates an existing totalProd.
     *
     * @param totalProd the totalProd to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated totalProd,
     * or with status 400 (Bad Request) if the totalProd is not valid,
     * or with status 500 (Internal Server Error) if the totalProd couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/total-prods")
    @Timed
    public ResponseEntity<TotalProd> updateTotalProd(@RequestBody TotalProd totalProd) throws URISyntaxException {
        log.debug("REST request to update TotalProd : {}", totalProd);
        if (totalProd.getId() == null) {
            return createTotalProd(totalProd);
        }
        TotalProd result = totalProdService.save(totalProd);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, totalProd.getId().toString()))
            .body(result);
    }

    /**
     * GET  /total-prods : get all the totalProds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of totalProds in body
     */
    @GetMapping("/total-prods")
    @Timed
    public List<TotalProd> getAllTotalProds() {
        log.debug("REST request to get all TotalProds");
        return totalProdService.findAll();
        }

    /**
     * GET  /total-prods/:id : get the "id" totalProd.
     *
     * @param id the id of the totalProd to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the totalProd, or with status 404 (Not Found)
     */
    @GetMapping("/total-prods/{id}")
    @Timed
    public ResponseEntity<TotalProd> getTotalProd(@PathVariable Long id) {
        log.debug("REST request to get TotalProd : {}", id);
        TotalProd totalProd = totalProdService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(totalProd));
    }

    /**
     * DELETE  /total-prods/:id : delete the "id" totalProd.
     *
     * @param id the id of the totalProd to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/total-prods/{id}")
    @Timed
    public ResponseEntity<Void> deleteTotalProd(@PathVariable Long id) {
        log.debug("REST request to delete TotalProd : {}", id);
        totalProdService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
