package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.service.dto.DetalleControlCambioCriteria;
import cl.tutorial.sccm.service.DetalleControlCambioQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DetalleControlCambio.
 */
@RestController
@RequestMapping("/api")
public class DetalleControlCambioResource {

    private final Logger log = LoggerFactory.getLogger(DetalleControlCambioResource.class);

    private static final String ENTITY_NAME = "detalleControlCambio";

    private final DetalleControlCambioService detalleControlCambioService;

    private final DetalleControlCambioQueryService detalleControlCambioQueryService;

    public DetalleControlCambioResource(DetalleControlCambioService detalleControlCambioService, DetalleControlCambioQueryService detalleControlCambioQueryService) {
        this.detalleControlCambioService = detalleControlCambioService;
        this.detalleControlCambioQueryService = detalleControlCambioQueryService;
    }

    /**
     * POST  /detalle-control-cambios : Create a new detalleControlCambio.
     *
     * @param detalleControlCambio the detalleControlCambio to create
     * @return the ResponseEntity with status 201 (Created) and with body the new detalleControlCambio, or with status 400 (Bad Request) if the detalleControlCambio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/detalle-control-cambios")
    @Timed
    public ResponseEntity<DetalleControlCambio> createDetalleControlCambio(@RequestBody DetalleControlCambio detalleControlCambio) throws URISyntaxException {
        log.debug("REST request to save DetalleControlCambio : {}", detalleControlCambio);
        if (detalleControlCambio.getId() != null) {
            throw new BadRequestAlertException("A new detalleControlCambio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetalleControlCambio result = detalleControlCambioService.save(detalleControlCambio);
        return ResponseEntity.created(new URI("/api/detalle-control-cambios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /detalle-control-cambios : Updates an existing detalleControlCambio.
     *
     * @param detalleControlCambio the detalleControlCambio to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated detalleControlCambio,
     * or with status 400 (Bad Request) if the detalleControlCambio is not valid,
     * or with status 500 (Internal Server Error) if the detalleControlCambio couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/detalle-control-cambios")
    @Timed
    public ResponseEntity<DetalleControlCambio> updateDetalleControlCambio(@RequestBody DetalleControlCambio detalleControlCambio) throws URISyntaxException {
        log.debug("REST request to update DetalleControlCambio : {}", detalleControlCambio);
        if (detalleControlCambio.getId() == null) {
            return createDetalleControlCambio(detalleControlCambio);
        }
        DetalleControlCambio result = detalleControlCambioService.save(detalleControlCambio);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, detalleControlCambio.getId().toString()))
            .body(result);
    }

    /**
     * GET  /detalle-control-cambios : get all the detalleControlCambios.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of detalleControlCambios in body
     */
    @GetMapping("/detalle-control-cambios")
    @Timed
    public ResponseEntity<List<DetalleControlCambio>> getAllDetalleControlCambios(DetalleControlCambioCriteria criteria) 
	{
        log.debug("REST request to get DetalleControlCambios by criteria: {}", criteria);
		
		//criteria.setDetcabcontrolId(new Long(9003));
        List<DetalleControlCambio> entityList = detalleControlCambioQueryService.findByCriteria(criteria);
        
		return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /detalle-control-cambios/:id : get the "id" detalleControlCambio.
     *
     * @param id the id of the detalleControlCambio to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the detalleControlCambio, or with status 404 (Not Found)
     */
    @GetMapping("/detalle-control-cambios/{id}")
    @Timed
    public ResponseEntity<DetalleControlCambio> getDetalleControlCambio(@PathVariable Long id) {
        log.debug("REST request to get DetalleControlCambio : {}", id);
        DetalleControlCambio detalleControlCambio = detalleControlCambioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(detalleControlCambio));
    }

    /**
     * DELETE  /detalle-control-cambios/:id : delete the "id" detalleControlCambio.
     *
     * @param id the id of the detalleControlCambio to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/detalle-control-cambios/{id}")
    @Timed
    public ResponseEntity<Void> deleteDetalleControlCambio(@PathVariable Long id) {
        log.debug("REST request to delete DetalleControlCambio : {}", id);
        detalleControlCambioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
