package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Instrumento;
import cl.tutorial.sccm.service.InstrumentoService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Instrumento.
 */
@RestController
@RequestMapping("/api")
public class InstrumentoResource {

    private final Logger log = LoggerFactory.getLogger(InstrumentoResource.class);

    private static final String ENTITY_NAME = "instrumento";

    private final InstrumentoService instrumentoService;

    public InstrumentoResource(InstrumentoService instrumentoService) {
        this.instrumentoService = instrumentoService;
    }

    /**
     * POST  /instrumentos : Create a new instrumento.
     *
     * @param instrumento the instrumento to create
     * @return the ResponseEntity with status 201 (Created) and with body the new instrumento, or with status 400 (Bad Request) if the instrumento has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/instrumentos")
    @Timed
    public ResponseEntity<Instrumento> createInstrumento(@RequestBody Instrumento instrumento) throws URISyntaxException {
        log.debug("REST request to save Instrumento : {}", instrumento);
        if (instrumento.getId() != null) {
            throw new BadRequestAlertException("A new instrumento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Instrumento result = instrumentoService.save(instrumento);
        return ResponseEntity.created(new URI("/api/instrumentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /instrumentos : Updates an existing instrumento.
     *
     * @param instrumento the instrumento to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated instrumento,
     * or with status 400 (Bad Request) if the instrumento is not valid,
     * or with status 500 (Internal Server Error) if the instrumento couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/instrumentos")
    @Timed
    public ResponseEntity<Instrumento> updateInstrumento(@RequestBody Instrumento instrumento) throws URISyntaxException {
        log.debug("REST request to update Instrumento : {}", instrumento);
        if (instrumento.getId() == null) {
            return createInstrumento(instrumento);
        }
        Instrumento result = instrumentoService.save(instrumento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, instrumento.getId().toString()))
            .body(result);
    }

    /**
     * GET  /instrumentos : get all the instrumentos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of instrumentos in body
     */
    @GetMapping("/instrumentos")
    @Timed
    public List<Instrumento> getAllInstrumentos() {
        log.debug("REST request to get all Instrumentos");
        return instrumentoService.findAll();
        }

    /**
     * GET  /instrumentos/:id : get the "id" instrumento.
     *
     * @param id the id of the instrumento to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the instrumento, or with status 404 (Not Found)
     */
    @GetMapping("/instrumentos/{id}")
    @Timed
    public ResponseEntity<Instrumento> getInstrumento(@PathVariable Long id) {
        log.debug("REST request to get Instrumento : {}", id);
        Instrumento instrumento = instrumentoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(instrumento));
    }

    /**
     * DELETE  /instrumentos/:id : delete the "id" instrumento.
     *
     * @param id the id of the instrumento to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/instrumentos/{id}")
    @Timed
    public ResponseEntity<Void> deleteInstrumento(@PathVariable Long id) {
        log.debug("REST request to delete Instrumento : {}", id);
        instrumentoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
