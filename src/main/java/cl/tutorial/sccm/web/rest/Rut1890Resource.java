package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Rut1890;
import cl.tutorial.sccm.service.Rut1890Service;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Rut1890.
 */
@RestController
@RequestMapping("/api")
public class Rut1890Resource {

    private final Logger log = LoggerFactory.getLogger(Rut1890Resource.class);

    private static final String ENTITY_NAME = "rut1890";

    private final Rut1890Service rut1890Service;

    public Rut1890Resource(Rut1890Service rut1890Service) {
        this.rut1890Service = rut1890Service;
    }

    /**
     * POST  /rut-1890-s : Create a new rut1890.
     *
     * @param rut1890 the rut1890 to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rut1890, or with status 400 (Bad Request) if the rut1890 has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rut-1890-s")
    @Timed
    public ResponseEntity<Rut1890> createRut1890(@RequestBody Rut1890 rut1890) throws URISyntaxException {
        log.debug("REST request to save Rut1890 : {}", rut1890);
        if (rut1890.getId() != null) {
            throw new BadRequestAlertException("A new rut1890 cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Rut1890 result = rut1890Service.save(rut1890);
        return ResponseEntity.created(new URI("/api/rut-1890-s/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rut-1890-s : Updates an existing rut1890.
     *
     * @param rut1890 the rut1890 to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rut1890,
     * or with status 400 (Bad Request) if the rut1890 is not valid,
     * or with status 500 (Internal Server Error) if the rut1890 couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rut-1890-s")
    @Timed
    public ResponseEntity<Rut1890> updateRut1890(@RequestBody Rut1890 rut1890) throws URISyntaxException {
        log.debug("REST request to update Rut1890 : {}", rut1890);
        if (rut1890.getId() == null) {
            return createRut1890(rut1890);
        }
        Rut1890 result = rut1890Service.save(rut1890);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rut1890.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rut-1890-s : get all the rut1890S.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of rut1890S in body
     */
    @GetMapping("/rut-1890-s")
    @Timed
    public List<Rut1890> getAllRut1890S() {
        log.debug("REST request to get all Rut1890S");
        return rut1890Service.findAll();
        }

    /**
     * GET  /rut-1890-s/:id : get the "id" rut1890.
     *
     * @param id the id of the rut1890 to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rut1890, or with status 404 (Not Found)
     */
    @GetMapping("/rut-1890-s/{id}")
    @Timed
    public ResponseEntity<Rut1890> getRut1890(@PathVariable Long id) {
        log.debug("REST request to get Rut1890 : {}", id);
        Rut1890 rut1890 = rut1890Service.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rut1890));
    }

    /**
     * DELETE  /rut-1890-s/:id : delete the "id" rut1890.
     *
     * @param id the id of the rut1890 to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rut-1890-s/{id}")
    @Timed
    public ResponseEntity<Void> deleteRut1890(@PathVariable Long id) {
        log.debug("REST request to delete Rut1890 : {}", id);
        rut1890Service.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	
	/**
     * GET  "/rut-1890-s/findAllByRutDataTypePaginated : get all clients by rut with pagination.
     *
     * @param rut
     * 		rut de la persona
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Object in body
     */
    @GetMapping("/rut-1890-s/findAllByRutDataTypePaginated")
    @Timed
    public ResponseEntity<List<Object>> findAllByRut(@RequestParam(name = "rut", required = false) Integer rut) throws Exception 
	{
		log.debug("ENTRA AL METODO FIND ALL BY RUT DATA TYPE PAGINATED");
		
        String message = "";
        Boolean isValidOption = true;
        
		if (rut == null) 
		{
            isValidOption = false;
            message = "¡Debe ingresar un rut!";
        }
		
        log.debug("REST request to get all clients by Rut with pagination");
        
		List<Object> lista = this.rut1890Service.findAllByRut(rut);
        
		log.debug("SALE DEL LISTADO");
		
		if (!isValidOption)
		{
            this.log.error(message);
            
			return new ResponseEntity(message, HttpStatus.BAD_REQUEST);
        }
		
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }
}