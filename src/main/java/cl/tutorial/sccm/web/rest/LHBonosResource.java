package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.LHBonos;
import cl.tutorial.sccm.service.LHBonosService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LHBonos.
 */
@RestController
@RequestMapping("/api")
public class LHBonosResource 
{
    private final Logger log = LoggerFactory.getLogger(LHBonosResource.class);

    private static final String ENTITY_NAME = "lHBonos";

    private final LHBonosService lHBonosService;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public LHBonosResource(CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService, LHBonosService lHBonosService) 
	{
        this.lHBonosService = lHBonosService;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /l-h-bonos : Create a new lHBonos.
     *
     * @param lHBonos the lHBonos to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lHBonos, or with status 400 (Bad Request) if the lHBonos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/l-h-bonos")
    @Timed
    public ResponseEntity<LHBonos> createLHBonos(@RequestBody LHBonos lHBonos) throws URISyntaxException 
	{
        log.debug("REST request to save LHBonos : {}", lHBonos);
        
        LHBonos result = lHBonosService.save(lHBonos);
        
		return ResponseEntity.created(new URI("/api/l-h-bonos/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /l-h-bonos : Updates an existing lHBonos.
     *
     * @param lHBonos the lHBonos to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lHBonos,
     * or with status 400 (Bad Request) if the lHBonos is not valid,
     * or with status 500 (Internal Server Error) if the lHBonos couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/l-h-bonos")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateLHBonos(@RequestBody LHBonos lHBonos) throws URISyntaxException 
	{
        log.debug("REST request to update LHBonos : {}", lHBonos);
        
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("LHBONOS");
		cabeceraControlCambio.setIdRegistro(lHBonos.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
		
		DetalleControlCambio resultado = guardaCampo(result, "rut", String.valueOf(lHBonos.getRut()), "C");
		resultado = guardaCampo(result, "dv", lHBonos.getDv(), "C");		
		resultado = guardaCampo(result, "producto", String.valueOf(lHBonos.getProducto()), "L");
		resultado = guardaCampo(result, "fechaPago", lHBonos.getFechaPago().toString(), "F");
		resultado = guardaCampo(result, "correlativo", String.valueOf(lHBonos.getCorrelativo().toString()), "C");	
		resultado = guardaCampo(result, "fechaInv", lHBonos.getFechaInv().toString(), "F");	
		resultado = guardaCampo(result, "folio", lHBonos.getFolio(), "C");
		resultado = guardaCampo(result, "capital", String.format("%.2f", lHBonos.getCapital()).replace(",", "."), "C");
		resultado = guardaCampo(result, "capMon", String.format("%.2f", lHBonos.getCapMon()).replace(",", "."), "C");
		resultado = guardaCampo(result, "intNom", lHBonos.getIntNom().toString(), "C");
		resultado = guardaCampo(result, "intReal", lHBonos.getIntReal().toString(), "C");
		resultado = guardaCampo(result, "monFi", lHBonos.getMonFi().toString(), "C");
		resultado = guardaCampo(result, "numGiro", lHBonos.getNumGiro().toString(), "C");
		resultado = guardaCampo(result, "codMon", lHBonos.getCodMon().toString(), "C");	
		resultado = guardaCampo(result, "monTasa", lHBonos.getMonTasa().toString(), "C");
		resultado = guardaCampo(result, "tasaEmi", lHBonos.getTasaEmi().toString(), "C");
		resultado = guardaCampo(result, "nomCli", lHBonos.getNomCli(), "C");
		resultado = guardaCampo(result, "codInst", lHBonos.getCodInst().toString(), "C");
		resultado = guardaCampo(result, "serieInst", lHBonos.getSerieInst(), "C");
		resultado = guardaCampo(result, "perTasa", lHBonos.getPerTasa().toString(), "C");	
		resultado = guardaCampo(result, "tipoInt", lHBonos.getTipoInt(), "C");
		resultado = guardaCampo(result, "codTipoInt", lHBonos.getCodTipoInt().toString(), "C");	
		resultado = guardaCampo(result, "fecVcto", lHBonos.getFecVcto().toString(), "F");
		resultado = guardaCampo(result, "onp", lHBonos.getOnp(), "C");	
		resultado = guardaCampo(result, "estCta", lHBonos.getEstCta(), "C");	
		resultado = guardaCampo(result, "fecCont", lHBonos.getFecCont().toString(), "F");
		resultado = guardaCampo(result, "bipersonal", lHBonos.getBipersonal(), "C");
		resultado = guardaCampo(result, "reajustabilidad", lHBonos.getReajustabilidad(), "C");	
		resultado = guardaCampo(result, "plazo", lHBonos.getPlazo().toString(), "C");
		resultado = guardaCampo(result, "indPago", lHBonos.getIndPago(), "C");	
		resultado = guardaCampo(result, "extranjero", lHBonos.getExtranjero(), "C");	
		resultado = guardaCampo(result, "impExt", lHBonos.getImpExt().toString(), "C");
		resultado = guardaCampo(result, "numCupones", lHBonos.getNumCupones().toString(), "C");
		resultado = guardaCampo(result, "numCuponPago", lHBonos.getNumCuponPago().toString(), "C");		
		resultado = guardaCampo(result, "ctaCap", lHBonos.getCtaCap().toString(), "C");			
		resultado = guardaCampo(result, "ctaReaj", lHBonos.getCtaReaj().toString(), "C");	
		resultado = guardaCampo(result, "ctaInt", lHBonos.getCtaInt().toString(), "C");	
		resultado = guardaCampo(result, "origen", lHBonos.getOrigen(), "C");	
		resultado = guardaCampo(result, "annoTributa", String.valueOf(lHBonos.getAnnoTributa().getId()), "L");
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).body(result);
    }	
	
    /**
     * PUT  /l-h-bonos : Updates an existing lHBonos.
     *
     * @param lHBonos the lHBonos to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lHBonos,
     * or with status 400 (Bad Request) if the lHBonos is not valid,
     * or with status 500 (Internal Server Error) if the lHBonos couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/l-h-bonos/actualizar")
    @Timed
    public ResponseEntity<LHBonos> actualizarLHBonos(@RequestBody LHBonos lHBonos) throws URISyntaxException 
	{
        log.debug("REST request to update LHBonos : {}", lHBonos);
        
        LHBonos result = lHBonosService.save(lHBonos);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lHBonos.getId().toString())).body(result);
    }

    /**
     * GET  /l-h-bonos : get all the lHBonos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of lHBonos in body
     */
    @GetMapping("/l-h-bonos")
    @Timed
    public List<LHBonos> getAllLHBonos() 
	{
        log.debug("REST request to get all LHBonos");
        
		return lHBonosService.findAll();
    }

    /**
     * GET  /l-h-bonos/:id : get the "id" lHBonos.
     *
     * @param id the id of the lHBonos to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lHBonos, or with status 404 (Not Found)
     */
    @GetMapping("/l-h-bonos/{id}")
    @Timed
    public ResponseEntity<LHBonos> getLHBonos(@PathVariable Long id) 
	{
        log.debug("REST request to get LHBonos : {}", id);
        
		LHBonos lHBonos = lHBonosService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lHBonos));
    }

    /**
     * DELETE  /l-h-bonos/:id : delete the "id" lHBonos.
     *
     * @param id the id of the lHBonos to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/l-h-bonos/{id}")
    @Timed
    public ResponseEntity<Void> deleteLHBonos(@PathVariable Long id) 
	{
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("LHBONOS");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();        
    }
	
	/**
     * DELETE  /l-h-bonos/:id : delete the "id" lHBonos.
     *
     * @param id the id of the lHBonos to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/l-h-bonos/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarLHBonos(@PathVariable Long id) 
	{
        log.debug("REST request to delete LHBonos : {}", id);
        
		lHBonosService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}