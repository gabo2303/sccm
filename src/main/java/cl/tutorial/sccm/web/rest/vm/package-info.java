/**
 * View Models used by Spring MVC REST controllers.
 */
package cl.tutorial.sccm.web.rest.vm;
