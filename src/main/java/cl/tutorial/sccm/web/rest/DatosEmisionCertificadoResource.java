package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.DAP;
import cl.tutorial.sccm.service.DAPService;
import cl.tutorial.sccm.service.DatosEmisionCertificadoService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DAP.
 */
@RestController
@RequestMapping("/api")
public class DatosEmisionCertificadoResource {

    private final Logger log = LoggerFactory.getLogger(DatosEmisionCertificadoResource.class);

    private static final String ENTITY_NAME = "datos-emi-cert";

    private final DAPService                     dAPService;
    private final DatosEmisionCertificadoService datosEmisionCertificadoService;

    public DatosEmisionCertificadoResource(DAPService dAPService, DatosEmisionCertificadoService datosEmisionCertificadoService) {
        this.dAPService = dAPService;
        this.datosEmisionCertificadoService = datosEmisionCertificadoService;
    }

    /**
     * POST  /datos-emi-cert : Create a new dAP.
     *
     * @param dAP
     * 		the dAP to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new dAP, or with status 400 (Bad Request) if the dAP has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/datos-emi-cert")
    @Timed
    public ResponseEntity<DAP> createDAP(@RequestBody DAP dAP) throws URISyntaxException {
        log.debug("REST request to save DAP : {}", dAP);
        if (dAP.getId() != null) {
            throw new BadRequestAlertException("A new dAP cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DAP result = dAPService.save(dAP);
        return ResponseEntity.created(new URI("/api/datos-emi-cert/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /datos-emi-cert : Updates an existing dAP.
     *
     * @param dAP
     * 		the dAP to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated dAP, or with status 400 (Bad Request) if the dAP is not valid, or with status 500 (Internal Server
     * Error) if the dAP couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/datos-emi-cert")
    @Timed
    public ResponseEntity<DAP> updateDAP(@RequestBody DAP dAP) throws URISyntaxException {
        log.debug("REST request to update DAP : {}", dAP);
        if (dAP.getId() == null) {
            return createDAP(dAP);
        }
        DAP result = dAPService.save(dAP);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dAP.getId().toString())).body(result);
    }

    /**
     * GET  /datos-emi-cert : get all the dAPS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dAPS in body
     */
    @GetMapping("/datos-emi-cert")
    @Timed
    public List<DAP> getAllDAPS() {
        log.debug("REST request to get all DAPS");
        return dAPService.findAll();
    }

    /**
     * GET  /datos-emi-cert/:id : get the "id" dAP.
     *
     * @param id
     * 		the id of the dAP to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the dAP, or with status 404 (Not Found)
     */
    @GetMapping("/datos-emi-cert/{id}")
    @Timed
    public ResponseEntity<DAP> getDAP(@PathVariable Long id) {
        log.debug("REST request to get DAP : {}", id);
        DAP dAP = dAPService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dAP));
    }

    /**
     * DELETE  /datos-emi-cert/:id : delete the "id" dAP.
     *
     * @param id
     * 		the id of the dAP to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/datos-emi-cert/{id}")
    @Timed
    public ResponseEntity<Void> deleteDAP(@PathVariable Long id) {
        log.debug("REST request to delete DAP : {}", id);
        dAPService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  "/datos-emi-cert/findAllByTaxYearIdRutDataTypePaginated : get all dAP by tax year and Alert Type Unique Code with pagination.
     *
     * @param taxYearId
     * 		id del año tributario
     * @param dataType
     * 		código del tipo de datos a buscar
     * @param rut
     * 		rut de la persona
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Object (depending on parameter "dataType") in body
     */
    @GetMapping("/datos-emi-cert/findAllByTaxYearIdRutDataTypePaginated")
    @Timed
    public ResponseEntity<List<Object>> findAllByTaxYearIdRutDataTypePaginated(@RequestParam(name = "taxYearId") Long taxYearId, @RequestParam(name = "dataType") Integer dataType,
        @RequestParam(name = "rut", required = false) BigInteger rut, @RequestParam(name = "folio", required = false) String folio, @ApiParam Pageable pageable) throws Exception {

        String message = "";
        Boolean isValidOption = true;
        if ((rut == null) && (folio == null || folio.isEmpty())) {
            isValidOption = false;
            message = "¡Debe ingresar un rut o un folio!";
        }
        log.debug("REST request to get all by tax year and Data Type And Rut with pagination");
        Page<Object> page = new PageImpl(new ArrayList());
        switch (dataType) {
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS: // Pactos
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_AHORRO: // Ahorro
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_DAP: // Depósito a Plazo
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS: // Bonos
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI: // GPI
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM: // DCV ASICOM
                page = this.datosEmisionCertificadoService.findAllByTaxYearIdRutDataType(taxYearId, dataType, rut, folio);
                break;
            default:
                isValidOption = false;
                message = "La opción del servicio no es válida. Opción: " + dataType + ".";
                break;
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/datos-emi-cert/findAllByTaxYearIdRutDataTypePaginated");
        if (!isValidOption) {
            this.log.error(message);
            return new ResponseEntity(message, headers, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
