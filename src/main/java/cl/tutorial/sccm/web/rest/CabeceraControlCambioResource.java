package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.service.dto.CabeceraControlCambioCriteria;
import cl.tutorial.sccm.service.CabeceraControlCambioQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CabeceraControlCambio.
 */
@RestController
@RequestMapping("/api")
public class CabeceraControlCambioResource {

    private final Logger log = LoggerFactory.getLogger(CabeceraControlCambioResource.class);

    private static final String ENTITY_NAME = "cabeceraControlCambio";

    private final CabeceraControlCambioService cabeceraControlCambioService;

    private final CabeceraControlCambioQueryService cabeceraControlCambioQueryService;

    public CabeceraControlCambioResource(CabeceraControlCambioService cabeceraControlCambioService, CabeceraControlCambioQueryService cabeceraControlCambioQueryService) {
        this.cabeceraControlCambioService = cabeceraControlCambioService;
        this.cabeceraControlCambioQueryService = cabeceraControlCambioQueryService;
    }

    /**
     * POST  /cabecera-control-cambios : Create a new cabeceraControlCambio.
     *
     * @param cabeceraControlCambio the cabeceraControlCambio to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cabeceraControlCambio, or with status 400 (Bad Request) if the cabeceraControlCambio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cabecera-control-cambios")
    @Timed
    public ResponseEntity<CabeceraControlCambio> createCabeceraControlCambio(@RequestBody CabeceraControlCambio cabeceraControlCambio) throws URISyntaxException {
        log.debug("REST request to save CabeceraControlCambio : {}", cabeceraControlCambio);
        if (cabeceraControlCambio.getId() != null) {
            throw new BadRequestAlertException("A new cabeceraControlCambio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
        return ResponseEntity.created(new URI("/api/cabecera-control-cambios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cabecera-control-cambios : Updates an existing cabeceraControlCambio.
     *
     * @param cabeceraControlCambio the cabeceraControlCambio to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cabeceraControlCambio,
     * or with status 400 (Bad Request) if the cabeceraControlCambio is not valid,
     * or with status 500 (Internal Server Error) if the cabeceraControlCambio couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cabecera-control-cambios")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateCabeceraControlCambio(@RequestBody CabeceraControlCambio cabeceraControlCambio) throws URISyntaxException {
        log.debug("REST request to update CabeceraControlCambio : {}", cabeceraControlCambio);
        if (cabeceraControlCambio.getId() == null) {
            return createCabeceraControlCambio(cabeceraControlCambio);
        }
        CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cabeceraControlCambio.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cabecera-control-cambios : get all the cabeceraControlCambios.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of cabeceraControlCambios in body
     */
    @GetMapping("/cabecera-control-cambios")
    @Timed
    public ResponseEntity<List<CabeceraControlCambio>> getAllCabeceraControlCambios(CabeceraControlCambioCriteria criteria) {
        log.debug("REST request to get CabeceraControlCambios by criteria: {}", criteria);
        List<CabeceraControlCambio> entityList = cabeceraControlCambioQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /cabecera-control-cambios/:id : get the "id" cabeceraControlCambio.
     *
     * @param id the id of the cabeceraControlCambio to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cabeceraControlCambio, or with status 404 (Not Found)
     */
    @GetMapping("/cabecera-control-cambios/{id}")
    @Timed
    public ResponseEntity<CabeceraControlCambio> getCabeceraControlCambio(@PathVariable Long id) {
        log.debug("REST request to get CabeceraControlCambio : {}", id);
        CabeceraControlCambio cabeceraControlCambio = cabeceraControlCambioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cabeceraControlCambio));
    }

    /**
     * DELETE  /cabecera-control-cambios/:id : delete the "id" cabeceraControlCambio.
     *
     * @param id the id of the cabeceraControlCambio to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cabecera-control-cambios/{id}")
    @Timed
    public ResponseEntity<Void> deleteCabeceraControlCambio(@PathVariable Long id) {
        log.debug("REST request to delete CabeceraControlCambio : {}", id);
        cabeceraControlCambioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
