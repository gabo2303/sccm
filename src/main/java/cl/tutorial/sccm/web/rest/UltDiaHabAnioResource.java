package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.UltDiaHabAnio;
import cl.tutorial.sccm.service.UltDiaHabAnioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UltDiaHabAnio.
 */
@RestController
@RequestMapping("/api")
public class UltDiaHabAnioResource 
{
    private final Logger log = LoggerFactory.getLogger(UltDiaHabAnioResource.class);

    private static final String ENTITY_NAME = "ultDiaHabAnio";

    private final UltDiaHabAnioService ultDiaHabAnioService;

    public UltDiaHabAnioResource(UltDiaHabAnioService ultDiaHabAnioService) { this.ultDiaHabAnioService = ultDiaHabAnioService; }

    /**
     * POST  /ult-dia-hab-anios : Create a new ultDiaHabAnio.
     *
     * @param ultDiaHabAnio the ultDiaHabAnio to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ultDiaHabAnio, or with status 400 (Bad Request) if the ultDiaHabAnio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ult-dia-hab-anios")
    @Timed
    public ResponseEntity<UltDiaHabAnio> createUltDiaHabAnio(@RequestBody UltDiaHabAnio ultDiaHabAnio) throws URISyntaxException 
	{
        log.debug("REST request to save UltDiaHabAnio : {}", ultDiaHabAnio);
        
		if (ultDiaHabAnio.getId() != null) 
		{
            throw new BadRequestAlertException("A new ultDiaHabAnio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
		UltDiaHabAnio result = ultDiaHabAnioService.save(ultDiaHabAnio);
        
		return ResponseEntity.created(new URI("/api/ult-dia-hab-anios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ult-dia-hab-anios : Updates an existing ultDiaHabAnio.
     *
     * @param ultDiaHabAnio the ultDiaHabAnio to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ultDiaHabAnio,
     * or with status 400 (Bad Request) if the ultDiaHabAnio is not valid,
     * or with status 500 (Internal Server Error) if the ultDiaHabAnio couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ult-dia-hab-anios")
    @Timed
    public ResponseEntity<UltDiaHabAnio> updateUltDiaHabAnio(@RequestBody UltDiaHabAnio ultDiaHabAnio) throws URISyntaxException 
	{
        log.debug("REST request to update UltDiaHabAnio : {}", ultDiaHabAnio);
        
		if (ultDiaHabAnio.getId() == null) { return createUltDiaHabAnio(ultDiaHabAnio); }
		
        UltDiaHabAnio result = ultDiaHabAnioService.save(ultDiaHabAnio);
        
		return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ultDiaHabAnio.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ult-dia-hab-anios : get all the ultDiaHabAnios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ultDiaHabAnios in body
     */
    @GetMapping("/ult-dia-hab-anios")
    @Timed
    public ResponseEntity<List<UltDiaHabAnio>> getAllUltDiaHabAnios(@ApiParam Pageable pageable) 
	{
        log.debug("REST request to get a page of UltDiaHabAnios");
        
		Page<UltDiaHabAnio> page = ultDiaHabAnioService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ult-dia-hab-anios");
        
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /ult-dia-hab-anios/:id : get the "id" ultDiaHabAnio.
     *
     * @param id the id of the ultDiaHabAnio to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ultDiaHabAnio, or with status 404 (Not Found)
     */
    @GetMapping("/ult-dia-hab-anios/{id}")
    @Timed
    public ResponseEntity<UltDiaHabAnio> getUltDiaHabAnio(@PathVariable Long id) {
        log.debug("REST request to get UltDiaHabAnio : {}", id);
        UltDiaHabAnio ultDiaHabAnio = ultDiaHabAnioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ultDiaHabAnio));
    }

    /**
     * DELETE  /ult-dia-hab-anios/:id : delete the "id" ultDiaHabAnio.
     *
     * @param id the id of the ultDiaHabAnio to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ult-dia-hab-anios/{id}")
    @Timed
    public ResponseEntity<Void> deleteUltDiaHabAnio(@PathVariable Long id) {
        log.debug("REST request to delete UltDiaHabAnio : {}", id);
        ultDiaHabAnioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}