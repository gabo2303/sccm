package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.PactosCC;
import cl.tutorial.sccm.domain.PactosIR;
import cl.tutorial.sccm.service.PactosCCService;
import cl.tutorial.sccm.service.PactosIRService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing Pactos.
 */
@RestController
@RequestMapping("/api")
public class PactosCCResource {

    private final Logger log = LoggerFactory.getLogger(PactosCCResource.class);

    private static final String ENTITY_NAME = "pactosCC";

    private final PactosCCService pactosCCService;

    public PactosCCResource(PactosCCService pactosCCService) {
        this.pactosCCService = pactosCCService;
    }

    /**
     * GET  /pactos : get all the pactos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pactos in body
     */
    @GetMapping("/pactosCC/{idAnno}/getByAnnoId")
    @Timed
    public List<PactosCC> findAllByAnnoTributa(@PathVariable Long idAnno) {
        log.debug("REST request to get all Pactos Cuadratura Contable");
        return pactosCCService.findAllByAnnoTributa(idAnno);
    }
}
