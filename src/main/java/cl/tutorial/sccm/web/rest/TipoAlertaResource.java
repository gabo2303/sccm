package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.TipoAlerta;
import cl.tutorial.sccm.repository.TipoAlertaRepository;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TipoAlerta.
 */
@RestController
@RequestMapping("/api")
public class TipoAlertaResource {

    private final Logger log = LoggerFactory.getLogger(TipoAlertaResource.class);

    private static final String ENTITY_NAME = "tipoAlerta";

    private final TipoAlertaRepository tipoAlertaRepository;

    public TipoAlertaResource(TipoAlertaRepository tipoAlertaRepository) {
        this.tipoAlertaRepository = tipoAlertaRepository;
    }

    /**
     * POST  /tipo-alertas : Create a new tipoAlerta.
     *
     * @param tipoAlerta
     * 		the tipoAlerta to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoAlerta, or with status 400 (Bad Request) if the tipoAlerta has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-alertas")
    @Timed
    public ResponseEntity<TipoAlerta> createTipoAlerta(@RequestBody TipoAlerta tipoAlerta) throws URISyntaxException {
        log.debug("REST request to save TipoAlerta : {}", tipoAlerta);
        if (tipoAlerta.getId() != null) {
            throw new BadRequestAlertException("A new tipoAlerta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoAlerta result = tipoAlertaRepository.save(tipoAlerta);
        return ResponseEntity.created(new URI("/api/tipo-alertas/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-alertas : Updates an existing tipoAlerta.
     *
     * @param tipoAlerta
     * 		the tipoAlerta to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoAlerta, or with status 400 (Bad Request) if the tipoAlerta is not valid, or with status 500
     * (Internal Server Error) if the tipoAlerta couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-alertas")
    @Timed
    public ResponseEntity<TipoAlerta> updateTipoAlerta(@RequestBody TipoAlerta tipoAlerta) throws URISyntaxException {
        log.debug("REST request to update TipoAlerta : {}", tipoAlerta);
        if (tipoAlerta.getId() == null) {
            return createTipoAlerta(tipoAlerta);
        }
        TipoAlerta result = tipoAlertaRepository.save(tipoAlerta);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoAlerta.getId().toString())).body(result);
    }

    /**
     * GET  /tipo-alertas : get all the tipoAlertas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipoAlertas in body
     */
    @GetMapping("/tipo-alertas")
    @Timed
    public List<TipoAlerta> getAllTipoAlertas(@RequestParam(name = "isDap", required = false) Boolean isDap) {
        log.debug("REST request to get all TipoAlertas");
        return isDap != null ? tipoAlertaRepository.findAllByEsDap(isDap) : tipoAlertaRepository.findAll();
    }

    /**
     * GET  /tipo-alertas/:id : get the "id" tipoAlerta.
     *
     * @param id
     * 		the id of the tipoAlerta to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the tipoAlerta, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-alertas/{id}")
    @Timed
    public ResponseEntity<TipoAlerta> getTipoAlerta(@PathVariable Long id) {
        log.debug("REST request to get TipoAlerta : {}", id);
        TipoAlerta tipoAlerta = tipoAlertaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoAlerta));
    }

    /**
     * DELETE  /tipo-alertas/:id : delete the "id" tipoAlerta.
     *
     * @param id
     * 		the id of the tipoAlerta to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-alertas/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipoAlerta(@PathVariable Long id) {
        log.debug("REST request to delete TipoAlerta : {}", id);
        tipoAlertaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
