package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.LetrasBonosCuadratura;
import cl.tutorial.sccm.repository.LetrasBonosCuadraturaRepository;
import cl.tutorial.sccm.service.dto.LetrasBonosCuadraturaDto;
import cl.tutorial.sccm.service.mapper.LetrasBonosCuadraturaMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LetrasBonosCuadratura.
 */
@RestController
@RequestMapping("/api")
public class LetrasBonosCuadraturaResource {

    private final Logger log = LoggerFactory.getLogger(LetrasBonosCuadraturaResource.class);

    private static final String ENTITY_NAME = "letrasBonosCuadratura";

    private final LetrasBonosCuadraturaRepository letrasBonosCuadraturaRepository;

    public LetrasBonosCuadraturaResource(LetrasBonosCuadraturaRepository letrasBonosCuadraturaRepository) {
        this.letrasBonosCuadraturaRepository = letrasBonosCuadraturaRepository;
    }

    /**
     * POST  /letras-bonos-cuadraturas : Create a new letrasBonosCuadratura.
     *
     * @param letrasBonosCuadratura
     * 		the letrasBonosCuadratura to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new letrasBonosCuadratura, or with status 400 (Bad Request) if the letrasBonosCuadratura has already
     * an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/letras-bonos-cuadraturas")
    @Timed
    public ResponseEntity<LetrasBonosCuadratura> createLetrasBonosCuadratura(@RequestBody LetrasBonosCuadratura letrasBonosCuadratura) throws URISyntaxException {
        log.debug("REST request to save LetrasBonosCuadratura : {}", letrasBonosCuadratura);
        if (letrasBonosCuadratura.getId() != null) {
            throw new BadRequestAlertException("A new letrasBonosCuadratura cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LetrasBonosCuadratura result = letrasBonosCuadraturaRepository.save(letrasBonosCuadratura);
        return ResponseEntity.created(new URI("/api/letras-bonos-cuadraturas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /letras-bonos-cuadraturas : Updates an existing letrasBonosCuadratura.
     *
     * @param letrasBonosCuadratura
     * 		the letrasBonosCuadratura to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated letrasBonosCuadratura, or with status 400 (Bad Request) if the letrasBonosCuadratura is not valid,
     * or with status 500 (Internal Server Error) if the letrasBonosCuadratura couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/letras-bonos-cuadraturas")
    @Timed
    public ResponseEntity<LetrasBonosCuadratura> updateLetrasBonosCuadratura(@RequestBody LetrasBonosCuadratura letrasBonosCuadratura) throws URISyntaxException {
        log.debug("REST request to update LetrasBonosCuadratura : {}", letrasBonosCuadratura);
        if (letrasBonosCuadratura.getId() == null) {
            return createLetrasBonosCuadratura(letrasBonosCuadratura);
        }
        LetrasBonosCuadratura result = letrasBonosCuadraturaRepository.save(letrasBonosCuadratura);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, letrasBonosCuadratura.getId().toString())).body(result);
    }

    /**
     * GET  /letras-bonos-cuadraturas : get all the letrasBonosCuadraturas.
     *
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of letrasBonosCuadraturas in body
     */
    @GetMapping("/letras-bonos-cuadraturas")
    @Timed
    public ResponseEntity<List<LetrasBonosCuadratura>> getAllLetrasBonosCuadraturas(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LetrasBonosCuadraturas");
        Page<LetrasBonosCuadratura> page = letrasBonosCuadraturaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/letras-bonos-cuadraturas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /letras-bonos-cuadraturas/:id : get the "id" letrasBonosCuadratura.
     *
     * @param id
     * 		the id of the letrasBonosCuadratura to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the letrasBonosCuadratura, or with status 404 (Not Found)
     */
    @GetMapping("/letras-bonos-cuadraturas/{id}")
    @Timed
    public ResponseEntity<LetrasBonosCuadratura> getLetrasBonosCuadratura(@PathVariable Long id) {
        log.debug("REST request to get LetrasBonosCuadratura : {}", id);
        LetrasBonosCuadratura letrasBonosCuadratura = letrasBonosCuadraturaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(letrasBonosCuadratura));
    }

    /**
     * DELETE  /letras-bonos-cuadraturas/:id : delete the "id" letrasBonosCuadratura.
     *
     * @param id
     * 		the id of the letrasBonosCuadratura to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/letras-bonos-cuadraturas/{id}")
    @Timed
    public ResponseEntity<Void> deleteLetrasBonosCuadratura(@PathVariable Long id) {
        log.debug("REST request to delete LetrasBonosCuadratura : {}", id);
        letrasBonosCuadraturaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  "/letras-bonos-cuadraturas/{idAnno}/findByAllByTaxYearIdPaginated : get all letrasBonosCuadratura by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of letrasBonosCuadratura in body
     */
    @GetMapping("/letras-bonos-cuadraturas/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<LetrasBonosCuadratura>> findByAllByTaxYearIdPaginated(@PathVariable Long idAnno, @ApiParam Pageable pageable) {
        log.debug("REST request to get all LetrasBonosCuadratura by tax year with pagination");
        Page<LetrasBonosCuadratura> page = letrasBonosCuadraturaRepository.findAllByAnnoTributaId(idAnno, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/letras-bonos-cuadraturas/{idAnno}/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/letras-bonos-cuadraturas/{idAnno}/findByAllByTaxYearId : get all LetrasBonosCuadraturaDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of LetrasBonosCuadraturaDto in body
     */
    @GetMapping("/letras-bonos-cuadraturas/{idAnno}/findByAllByTaxYearId")
    @Timed
    public List<LetrasBonosCuadraturaDto> findByAllByTaxYearId(@PathVariable Long idAnno) {
        log.debug("REST request to get all LetrasBonosCuadraturaDto by tax year");
        final List<LetrasBonosCuadratura> allByAnnoTributa = letrasBonosCuadraturaRepository.findAllByAnnoTributaId(idAnno);
        final List<LetrasBonosCuadraturaDto> dapDtos = LetrasBonosCuadraturaMapper.INSTANCE.letrasBonosCuadraturaToLetrasBonosCuadraturaDto(allByAnnoTributa);
        log.debug("REST request to get all LetrasBonosCuadraturaDto by tax year COUNT : {}", dapDtos.size());
        return dapDtos;
    }
}
