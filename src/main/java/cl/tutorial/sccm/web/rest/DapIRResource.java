package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.DapIR;
import cl.tutorial.sccm.repository.DapIRRepository;
import cl.tutorial.sccm.service.dto.DapIrDto;
import cl.tutorial.sccm.service.mapper.DapIrMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DapIR.
 */
@RestController
@RequestMapping("/api")
public class DapIRResource {

    private final Logger log = LoggerFactory.getLogger(DapIRResource.class);

    private static final String ENTITY_NAME = "dapIR";

    private final DapIRRepository dapIRRepository;

    public DapIRResource(DapIRRepository dapIRRepository) {
        this.dapIRRepository = dapIRRepository;
    }

    /**
     * POST  /dap-irs : Create a new dapIR.
     *
     * @param dapIR
     * 		the dapIR to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new dapIR, or with status 400 (Bad Request) if the dapIR has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/dap-irs")
    @Timed
    public ResponseEntity<DapIR> createDapIR(@RequestBody DapIR dapIR) throws URISyntaxException {
        log.debug("REST request to save DapIR : {}", dapIR);
        if (dapIR.getId() != null) {
            throw new BadRequestAlertException("A new dapIR cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DapIR result = dapIRRepository.save(dapIR);
        return ResponseEntity.created(new URI("/api/dap-irs/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /dap-irs : Updates an existing dapIR.
     *
     * @param dapIR
     * 		the dapIR to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated dapIR, or with status 400 (Bad Request) if the dapIR is not valid, or with status 500 (Internal
     * Server Error) if the dapIR couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/dap-irs")
    @Timed
    public ResponseEntity<DapIR> updateDapIR(@RequestBody DapIR dapIR) throws URISyntaxException {
        log.debug("REST request to update DapIR : {}", dapIR);
        if (dapIR.getId() == null) {
            return createDapIR(dapIR);
        }
        DapIR result = dapIRRepository.save(dapIR);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dapIR.getId().toString())).body(result);
    }

    /**
     * GET  /dap-irs : get all the dapIRS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dapIRS in body
     */
    @GetMapping("/dap-irs")
    @Timed
    public List<DapIR> getAllDapIRS() {
        log.debug("REST request to get all DapIRS");
        return dapIRRepository.findAll();
    }

    /**
     * GET  /dap-irs-paginated : get all the dapIRS with pagination.
     *
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dapIRS in body
     */
    @GetMapping("/dap-irs-paginated")
    @Timed
    public ResponseEntity<List<DapIR>> getAllDapIRSPaginated(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DapIRS");
        Page<DapIR> page = dapIRRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dap-irs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dap-irs/:id : get the "id" dapIR.
     *
     * @param id
     * 		the id of the dapIR to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the dapIR, or with status 404 (Not Found)
     */
    @GetMapping("/dap-irs/{id}")
    @Timed
    public ResponseEntity<DapIR> getDapIR(@PathVariable BigInteger id) {
        log.debug("REST request to get DapIR : {}", id);
        DapIR dapIR = dapIRRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dapIR));
    }

    /**
     * DELETE  /dap-irs/:id : delete the "id" dapIR.
     *
     * @param id
     * 		the id of the dapIR to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dap-irs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDapIR(@PathVariable BigInteger id) {
        log.debug("REST request to delete DapIR : {}", id);
        dapIRRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /dap-irs/{idAnno}/getByYearId : get all DapIR by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapIR in body
     */
    @GetMapping("/dap-irs/{idAnno}/getByYearId")
    @Timed
    public List<DapIR> getByYearId(@PathVariable BigInteger idAnno) {
        log.debug("REST request to get all DapIR by tax year");
        return dapIRRepository.findAllByAnnoTributa(idAnno);
    }

    /**
     * GET  "/dap-irs/{idAnno}/findByAllByTaxYearIdPaginated : get all DapIR by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapIR in body
     */
    @GetMapping("/dap-irs/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<DapIR>> findByAllByTaxYearIdPaginated(@PathVariable BigInteger idAnno, @ApiParam Pageable pageable) {
        log.debug("REST request to get all DapIR by tax year with pagination");
        Page<DapIR> page = dapIRRepository.findAllByAnnoTributa(idAnno, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/dap-irs/{idAnno}/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/dap-irs/findByAllByTaxYearIdCurrencyPaginated : get all DapIR by tax year and currency with pagination.
     *
     * @param idAnno
     * 		Id del año tributario
     * @param currency
     * 		El tipo de moneda
     * @param pageable
     * 		The pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapIR in body
     */
    @GetMapping("/dap-irs/findByAllByTaxYearIdCurrencyPaginated")
    @Timed
    public ResponseEntity<List<DapIR>> findByAllByTaxYearIdCurrencyPaginated(@RequestParam(name = "idAnno", required = true) BigInteger idAnno, @RequestParam(name =
        "currency", required = false) Long currency, @ApiParam Pageable pageable) {

        log.debug("REST request to get all DapIR by tax year and currency with pagination");
        Page<DapIR> page;
        if (currency != null) {
            page = dapIRRepository.findAllByAnnoTributaAndCodMon(idAnno, currency, pageable);
        } else {
            page = dapIRRepository.findAllByAnnoTributa(idAnno, pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/dap-irs/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/d-aps/{idAnno}/findByAllByTaxYearId : get all DapIrDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapIrDto in body
     */
    @GetMapping("/dap-irs/{idAnno}/findByAllByTaxYearId")
    @Timed
    public List<DapIrDto> findByAllByTaxYearId(@PathVariable BigInteger idAnno) {
        log.debug("REST request to get all DapIrDto by tax year");
        final List<DapIR> allByAnnoTributa = dapIRRepository.findAllByAnnoTributa(idAnno);
        final List<DapIrDto> dapDtos = DapIrMapper.INSTANCE.daIrToDapIrDto(allByAnnoTributa);
        log.debug("REST request to get all DapIrDto by tax year COUNT : {}", dapDtos.size());
        return dapDtos;
    }
}
