package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.Pactos;
import cl.tutorial.sccm.domain.PactosIR;
import cl.tutorial.sccm.service.PactosIRService;
import cl.tutorial.sccm.service.PactosService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Pactos.
 */
@RestController
@RequestMapping("/api")
public class PactosIRResource {

    private final Logger log = LoggerFactory.getLogger(PactosIRResource.class);

    private static final String ENTITY_NAME = "pactosIR";

    private final PactosIRService pactosIRService;

    public PactosIRResource(PactosIRService pactosIRService) {
        this.pactosIRService = pactosIRService;
    }

    /**
     * GET  /pactos : get all the pactos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pactos in body
     */
    @GetMapping("/pactosIR/{idAnno}/getByAnnoId")
    @Timed
    public List<PactosIR> findAllByAnnoTributa(@PathVariable Long idAnno) {
        log.debug("REST request to get all Pactos Interés Real");
        return pactosIRService.findAllByAnnoTributa(idAnno);
    }
}
