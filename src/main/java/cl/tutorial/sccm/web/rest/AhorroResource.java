package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.Ahorro;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.repository.AhorroRepository;
import cl.tutorial.sccm.service.AhorroService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.service.AnnoTributaService;
import cl.tutorial.sccm.service.dto.AhorroDto;
import cl.tutorial.sccm.service.mapper.AhorroMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Ahorro.
 */
@RestController
@RequestMapping("/api")
public class AhorroResource 
{
    private final        Logger log         = LoggerFactory.getLogger(AhorroResource.class);
    private static final String ENTITY_NAME = "ahorro";

    private final AhorroRepository   ahorroRepository;
    private final AhorroService      ahorroService;
    private final AnnoTributaService annoTributaService;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public AhorroResource(AhorroRepository ahorroRepository, AhorroService ahorroService, AnnoTributaService annoTributaService, 
						  CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService) 
	{
        this.ahorroRepository = ahorroRepository;
        this.ahorroService = ahorroService;
        this.annoTributaService = annoTributaService;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /ahorros : Create a new ahorro.
     *
     * @param ahorro
     * 		the ahorro to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new ahorro, or with status 400 (Bad Request) if the ahorro has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/ahorros")
    @Timed
    public ResponseEntity<Ahorro> createAhorro(@RequestBody Ahorro ahorro) throws URISyntaxException 
	{
        log.debug("REST request to save Ahorro : {}", ahorro);
		
        if (ahorro.getId() != null) 
		{
            throw new BadRequestAlertException("A new ahorro cannot already have an ID", ENTITY_NAME, "idexists");
        }
		
        Ahorro result = ahorroService.save(ahorro);
		
        return ResponseEntity.created(new URI("/api/ahorros/" + result.getId()))
			   .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
			   .body(result);
    }
	

    /**
     * PUT  /ahorros : Updates an existing ahorro.
     *
     * @param ahorro
     * 		the ahorro to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated ahorro, or with status 400 (Bad Request) if the ahorro is not valid, or with status 500 (Internal
     * Server Error) if the ahorro couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/ahorros")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateAhorro(@RequestBody Ahorro ahorro) throws URISyntaxException 
	{
        log.debug("REST request to update Ahorro : {}", ahorro);
		
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ACTUALIZAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("AHORRO");
		cabeceraControlCambio.setIdRegistro(ahorro.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
		
		DetalleControlCambio resultado = guardaCampo(result, "rut", String.valueOf(ahorro.getRut()), "C");
		resultado = guardaCampo(result, "dv", ahorro.getDv(), "C");
		resultado = guardaCampo(result, "producto", String.valueOf(ahorro.getProducto()), "L");
		resultado = guardaCampo(result, "moneda", String.valueOf(ahorro.getMoneda()), "L");
		resultado = guardaCampo(result, "fechaPago", ahorro.getFechaPago().toString(), "F");
		resultado = guardaCampo(result, "correlativo", String.valueOf(ahorro.getCorrelativo().toString()), "C");
		resultado = guardaCampo(result, "fechaInv", ahorro.getFechaInv().toString(), "F");
		resultado = guardaCampo(result, "folio", ahorro.getFolio(), "C");
		resultado = guardaCampo(result, "capital", ahorro.getCapital().toString(), "C");
		resultado = guardaCampo(result, "monOp", ahorro.getMonOp().toString(), "C");
		resultado = guardaCampo(result, "intPagado", ahorro.getIntPagado().toString(), "C");
		resultado = guardaCampo(result, "intReal", ahorro.getIntReal().toString(), "C");
		resultado = guardaCampo(result, "monFi", ahorro.getMonFi().toString(), "C");
		resultado = guardaCampo(result, "numGiro", ahorro.getNumGiro().toString(), "C");
		resultado = guardaCampo(result, "codMon", ahorro.getCodMon().toString(), "C");
		resultado = guardaCampo(result, "bipersonal", ahorro.getBipersonal(), "C");
		resultado = guardaCampo(result, "sucursal", ahorro.getSucursal(), "C");
		resultado = guardaCampo(result, "tasaOp", ahorro.getTasaOp().toString(), "C");		
		resultado = guardaCampo(result, "nacionalidad", ahorro.getNacionalidad(), "C");
		resultado = guardaCampo(result, "impto35", ahorro.getImpto35().toString(), "C");		
		resultado = guardaCampo(result, "nombre", ahorro.getNombre(), "C");		
		resultado = guardaCampo(result, "direccion", ahorro.getDireccion(), "C");		
		resultado = guardaCampo(result, "comuna", ahorro.getComuna(), "C");		
		resultado = guardaCampo(result, "ciudad", ahorro.getCiudad(), "C");		
		resultado = guardaCampo(result, "fechaApe", ahorro.getFechaApe().toString(), "F");
		resultado = guardaCampo(result, "fechaCon", ahorro.getFechaCon().toString(), "F");
		resultado = guardaCampo(result, "cuentaCap", ahorro.getCuentaCap().toString(), "C");
		resultado = guardaCampo(result, "cuentaInt", ahorro.getCuentaInt().toString(), "C");
		resultado = guardaCampo(result, "cuentaReaj", ahorro.getCuentaReaj().toString(), "C");
		resultado = guardaCampo(result, "reajPagado", ahorro.getReajPagado().toString(), "C");
		resultado = guardaCampo(result, "fechaVen", ahorro.getFechaVen().toString(), "F");
		resultado = guardaCampo(result, "reajustabilidad", ahorro.getReajustabilidad(), "C");
		resultado = guardaCampo(result, "estado", ahorro.getEstado(), "C");
		resultado = guardaCampo(result, "p57Bis", ahorro.getp57Bis(), "C");
		resultado = guardaCampo(result, "movInmaduros", ahorro.getMovInmaduros(), "C");
		resultado = guardaCampo(result, "origen", ahorro.getOrigen(), "C");		
		resultado = guardaCampo(result, "annoTributa", String.valueOf(ahorro.getAnnoTributa().getId()), "L");
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("cabeceraControlCambio", result.getId().toString())).body(result);		
    }
	
	/**
     * PUT  /ahorros : Updates an existing ahorro.
     *
     * @param ahorro
     * 		the ahorro to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated ahorro, or with status 400 (Bad Request) if the ahorro is not valid, or with status 500 (Internal
     * Server Error) if the ahorro couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/ahorros/actualizar")
    @Timed
    public ResponseEntity<Ahorro> actualizarAhorro(@RequestBody Ahorro ahorro) throws URISyntaxException 
	{
        log.debug("REST request to update Ahorro : {}", ahorro);
        		
        Ahorro result = ahorroService.save(ahorro);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ahorro.getId().toString())).body(result);
    }

    /**
     * GET  /ahorros : get all the ahorros.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ahorros in body
     */
    @GetMapping("/ahorros")
    @Timed
    public List<Ahorro> getAllAhorros() 
	{
        log.debug("REST request to get all Ahorros");
        
		return ahorroService.findAll();
    }

    /**
     * GET  /ahorros/:id : get the "id" ahorro.
     *
     * @param id
     * 		the id of the ahorro to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the ahorro, or with status 404 (Not Found)
     */
    @GetMapping("/ahorros/{id}")
    @Timed
    public ResponseEntity<Ahorro> getAhorro(@PathVariable Long id) 
	{
        log.debug("REST request to get Ahorro : {}", id);
        
		Ahorro ahorro = ahorroService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ahorro));
    }

    /**
     * DELETE  /ahorros/:id : delete the "id" ahorro.
     *
     * @param id
     * 		the id of the ahorro to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ahorros/{id}")
    @Timed
    public ResponseEntity<Void> deleteAhorro(@PathVariable Long id) 
	{
        log.debug("REST request to delete Ahorro : {}", id);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("AHORRO");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /ahorros/:id : delete the "id" ahorro.
     *
     * @param id
     * 		the id of the ahorro to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ahorros/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarAhorro(@PathVariable Long id) 
	{
        log.debug("REST request to d elete Ahorro : {}", id);
        
		ahorroService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/ahorros/{idAnno}/getByAnnoId")
    @Timed
    public List<Ahorro> findAllByAnnoTributaId(@PathVariable Long idAnno) 
	{
        log.debug("REST request to reprocessE57B:");
        
		return this.ahorroService.findAllByAnnoTributaId(idAnno);
    }

    /**
     * GET  "/ahorros/{idAnno}/findByAllByTaxYearIdPaginated : get all Ahorro by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Ahorro in body
     */
    @GetMapping("/ahorros/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<Ahorro>> findByAllByTaxYearIdPaginated(@PathVariable Long idAnno, @ApiParam Pageable pageable) 
	{
        log.debug("REST request to get all Ahorro by tax year with pagination");
		
        final AnnoTributa annoTributa = annoTributaService.findOne(idAnno);
        Page<Ahorro> page = ahorroRepository.findAllByAnnoTributa(annoTributa, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/ahorros/{idAnno}/findByAllByTaxYearIdPaginated");
        
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/ahorros/{idAnno}/findByAllByTaxYearId : get all AhorroDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of AhorroDto in body
     */
    @GetMapping("/ahorros/{idAnno}/findByAllByTaxYearId")
    @Timed
    public List<AhorroDto> findByAllByTaxYearId(@PathVariable Long idAnno) 
	{
        log.debug("REST request to get all AhorroDto by tax year");
        final List<Ahorro> allByAnnoTributa = ahorroRepository.findAllByAnnoTributaId(idAnno);
        final List<AhorroDto> dtos = AhorroMapper.INSTANCE.ahorroToAhorroDto(allByAnnoTributa);
        log.debug("REST request to get all AhorroDto by tax year COUNT : {}", dtos.size());
        
		return dtos;
    }

    /**
     * GET  "/ahorros/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCode : get all AhorroDto by tax year and Alert Type Unique Code.
     *
     * @param idAnno
     * 		id del año tributario
     * @param alarmUniqueCode
     * 		código del tipo de alarma
     *
     * @return the ResponseEntity with status 200 (OK) and the list of AhorroDto in body
     */
    @GetMapping("/ahorros/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCode")
    @Timed
    public List<AhorroDto> findByAllByTaxYearIdAndAlertTypeUniqueCode(@PathVariable Long idAnno, @RequestParam(name = "alarmUniqueCode", required = false) String alarmUniqueCode) 
	{
        String message = "";
        Boolean isValidOption = true;
        log.debug("REST request to get all AhorroDto by tax year and Alert Type Unique Code");
        List<Ahorro> page = new ArrayList();
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        
		switch (queryOption) 
		{
            case Constants.ALARMA_CAPITAL_E_INTERES_PAGADO_NEGATIVO:
            case Constants.ALARMA_CLIENTE_EXTRANJERO:
            case Constants.ALARMA_CUENTA_CERRADA:
            case Constants.ALARMA_INTERES_REAL_MAYOR_QUE_INTERES_PAGADO:
            case Constants.ALARMA_MAS_DE_6_GIROS_SIN_REAJUSTES_NI_INTERESES:
            case Constants.ALARMA_MAS_DE_6_GIROS:
            case Constants.ALARMA_OPCION_NULL:
                page = this.ahorroService.findByAllByTaxYearIdAndAlertTypeUniqueCode(idAnno, alarmUniqueCode);
                break;
            default:
                isValidOption = false;
                message = "La opción del servicio no es válida. Opción: " + queryOption + ".";
                break;
        }
        
		final List<AhorroDto> dtos = AhorroMapper.INSTANCE.ahorroToAhorroDto(page);
        log.debug("REST request to get all AhorroDto by tax year COUNT : {}", dtos.size());
        
		return dtos;
    }

    /**
     * GET  "/ahorros/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated : get all Ahorro by tax year and Alert Type Unique Code with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param alarmUniqueCode
     * 		código del tipo de alarma
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Ahorro in body
     */
    @GetMapping("/ahorros/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated")
    @Timed
    public ResponseEntity<List<Ahorro>> findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(@PathVariable Long idAnno,
        @RequestParam(name = "alarmUniqueCode", required = false) String alarmUniqueCode, @ApiParam Pageable pageable) {

        String message = "";
        Boolean isValidOption = true;
        log.debug("REST request to get all Ahorro by tax year and Alert Type Unique Code with pagination");
        Page<Ahorro> page = new PageImpl(new ArrayList());
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        switch (queryOption) {
            case Constants.ALARMA_CAPITAL_E_INTERES_PAGADO_NEGATIVO:
            case Constants.ALARMA_CLIENTE_EXTRANJERO:
            case Constants.ALARMA_CUENTA_CERRADA:
            case Constants.ALARMA_INTERES_REAL_MAYOR_QUE_INTERES_PAGADO:
            case Constants.ALARMA_MAS_DE_6_GIROS_SIN_REAJUSTES_NI_INTERESES:
            case Constants.ALARMA_MAS_DE_6_GIROS:
            case Constants.ALARMA_OPCION_NULL:
                page = this.ahorroService.findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(idAnno, alarmUniqueCode, pageable);
                break;
            default:
                isValidOption = false;
                message = "La opción del servicio no es válida. Opción: " + queryOption + ".";
                break;
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/ahorros/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}
