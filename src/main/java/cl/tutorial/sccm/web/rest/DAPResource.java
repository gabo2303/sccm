package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.DAP;
import cl.tutorial.sccm.repository.DAPRepository;
import cl.tutorial.sccm.service.AnnoTributaService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.service.DAPService;
import cl.tutorial.sccm.service.dto.DapDto;
import cl.tutorial.sccm.service.mapper.DapMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DAP.
 */
@RestController
@RequestMapping("/api")
public class DAPResource 
{
    private final Logger log = LoggerFactory.getLogger(DAPResource.class);

    private static final String ENTITY_NAME = "dAP";

    private final DAPService         dAPService;
    private final AnnoTributaService annoTributaService;
    private final DAPRepository      dAPRepository;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public DAPResource(CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService, DAPService dAPService, 
					   AnnoTributaService annoTributaService, DAPRepository dAPRepository) 
	{
        this.dAPService = dAPService;
        this.annoTributaService = annoTributaService;
        this.dAPRepository = dAPRepository;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /d-aps : Create a new dAP.
     *
     * @param dAP
     * 		the dAP to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new dAP, or with status 400 (Bad Request) if the dAP has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/d-aps")
    @Timed
    public ResponseEntity<DAP> createDAP(@RequestBody DAP dAP) throws URISyntaxException 
	{
        log.debug("REST request to save DAP : {}", dAP);
        
		if (dAP.getId() != null) { throw new BadRequestAlertException("A new dAP cannot already have an ID", ENTITY_NAME, "idexists"); }
		
        DAP result = dAPService.save(dAP);
        
		return ResponseEntity.created(new URI("/api/d-aps/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /d-aps : Updates an existing dAP.
     *
     * @param dAP
     * 		the dAP to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated dAP, or with status 400 (Bad Request) if the dAP is not valid, or with status 500 (Internal Server
     * Error) if the dAP couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/d-aps")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updateDAP(@RequestBody DAP dap) throws URISyntaxException 
	{
        log.debug("REST request to update DAP : {}", dap);
        
        log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ACTUALIZAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("DAP");
		cabeceraControlCambio.setIdRegistro(dap.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
		
		DetalleControlCambio resultado = guardaCampo(result, "fecVcto", dap.getFecVcto().toString(), "F");
		resultado = guardaCampo(result, "fechaInv", dap.getFechaInv().toString(), "F");
		resultado = guardaCampo(result, "fecCont", dap.getFecCont().toString(), "F");
		resultado = guardaCampo(result, "fechaPago", dap.getFechaPago().toString(), "F");
		resultado = guardaCampo(result, "impExt", dap.getImpExt().toString(), "C");
		resultado = guardaCampo(result, "capital", String.format("%.2f", dap.getCapital()).replace(",", "."), "C");
		resultado = guardaCampo(result, "capMon", String.format("%.2f", dap.getCapMon()).replace(",", "."), "C");
		resultado = guardaCampo(result, "intNom", dap.getIntNom().toString(), "C");
		resultado = guardaCampo(result, "monTasa", dap.getMonTasa().toString(), "C");
		resultado = guardaCampo(result, "tasaOp", dap.getTasaOp().toString(), "C");
		resultado = guardaCampo(result, "intPagado", dap.getIntPagado().toString(), "C");
		resultado = guardaCampo(result, "plazo", dap.getPlazo().toString(), "C");
		resultado = guardaCampo(result, "numCupones", dap.getNumCupones().toString(), "C");
		resultado = guardaCampo(result, "numCuponPago", dap.getNumCuponPago().toString(), "C");
		resultado = guardaCampo(result, "reajPagado", dap.getReajPagado().toString(), "C");
		resultado = guardaCampo(result, "sucursal", dap.getSucursal().toString(), "C");
		resultado = guardaCampo(result, "rut", String.valueOf(dap.getRut()), "C");
		resultado = guardaCampo(result, "producto", String.valueOf(dap.getProducto()), "L");
		resultado = guardaCampo(result, "moneda", String.valueOf(dap.getMoneda()), "L");		
		resultado = guardaCampo(result, "correlativo", String.valueOf(dap.getCorrelativo().toString()), "C");		
		resultado = guardaCampo(result, "folio", dap.getFolio(), "C");
		resultado = guardaCampo(result, "intReal", dap.getIntReal().toString(), "C");
		resultado = guardaCampo(result, "monFi", String.format("%.2f", dap.getMonFi()).replace(",", "."), "C");
		resultado = guardaCampo(result, "numGiro", dap.getNumGiro().toString(), "C");
		resultado = guardaCampo(result, "codMon", dap.getCodMon().toString(), "C");	
		resultado = guardaCampo(result, "ctaCap", dap.getCtaCap().toString(), "C");	
		resultado = guardaCampo(result, "codTipoInt", dap.getCodTipoInt().toString(), "C");
		resultado = guardaCampo(result, "perTasa", dap.getPerTasa().toString(), "C");
		resultado = guardaCampo(result, "ctaReaj", dap.getCtaReaj().toString(), "C");	
		resultado = guardaCampo(result, "ctaInt", dap.getCtaInt().toString(), "C");	
		resultado = guardaCampo(result, "dv", dap.getDv(), "C");						
		resultado = guardaCampo(result, "tipPlazo", dap.getTipPlazo(), "C");	
		resultado = guardaCampo(result, "tipoInt", dap.getTipoInt(), "C");	
		resultado = guardaCampo(result, "onp", dap.getOnp(), "C");	
		resultado = guardaCampo(result, "estCta", dap.getEstCta(), "C");	
		resultado = guardaCampo(result, "bipersonal", dap.getBipersonal(), "C");
		resultado = guardaCampo(result, "reajustabilidad", dap.getReajustabilidad(), "C");	
		resultado = guardaCampo(result, "indPago", dap.getIndPago(), "C");	
		resultado = guardaCampo(result, "p57Bis", dap.getp57Bis(), "C");	
		resultado = guardaCampo(result, "extranjero", dap.getExtranjero(), "C");	
		resultado = guardaCampo(result, "origen", dap.getOrigen(), "C");	
		resultado = guardaCampo(result, "annoTributa", String.valueOf(dap.getAnnoTributa().getId()), "L");
				
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).body(result);	
    }
	
	/**
     * PUT  /d-aps : Updates an existing dAP.
     *
     * @param dAP
     * 		the dAP to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated dAP, or with status 400 (Bad Request) if the dAP is not valid, or with status 500 (Internal Server
     * Error) if the dAP couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/d-aps/actualizar")
    @Timed
    public ResponseEntity<DAP> actualizaDAP(@RequestBody DAP dAP) throws URISyntaxException 
	{
        log.debug("REST request to update DAP : {}", dAP);
        
        DAP result = dAPService.save(dAP);
		
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dAP.getId().toString())).body(result);
    }
	
    /**
     * GET  /d-aps : get all the dAPS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dAPS in body
     */
    @GetMapping("/d-aps")
    @Timed
    public List<DAP> getAllDAPS() 
	{
        log.debug("REST request to get all DAPS");
        
		return dAPService.findAll();
    }

    /**
     * GET  /d-aps/:id : get the "id" dAP.
     *
     * @param id
     * 		the id of the dAP to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the dAP, or with status 404 (Not Found)
     */
    @GetMapping("/d-aps/{id}")
    @Timed
    public ResponseEntity<DAP> getDAP(@PathVariable Long id) 
	{
        log.debug("REST request to get DAP : {}", id);
        
		DAP dAP = dAPService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dAP));
    }

    /**
     * DELETE  /d-aps/:id : delete the "id" dAP.
     *
     * @param id
     * 		the id of the dAP to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/d-aps/{id}")
    @Timed
    public ResponseEntity<Void> deleteDAP(@PathVariable Long id) 
	{
        log.debug("REST request to delete DAP : {}", id);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("DAP");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /d-aps/:id : delete the "id" dAP.
     *
     * @param id
     * 		the id of the dAP to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/d-aps/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarDAP(@PathVariable Long id) 
	{
        log.debug("REST request to delete DAP : {}", id);
        
		dAPService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  "/d-aps/{idAnno}/findByAllByTaxYearIdPaginated : get all dAP by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dAP in body
     */
    @GetMapping("/d-aps/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<DAP>> findByAllByTaxYearIdPaginated(@PathVariable Long idAnno, @ApiParam Pageable pageable) 
	{
        log.debug("REST request to get all DAP by tax year with pagination");
        
		final AnnoTributa annoTributa = annoTributaService.findOne(idAnno);
        
		Page<DAP> page = dAPRepository.findAllByAnnoTributa(annoTributa, pageable);
        
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/d-aps/{idAnno}/findByAllByTaxYearIdPaginated");
        
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/d-aps/{idAnno}/findByAllByTaxYearId : get all DapDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapDto in body
     */
    @GetMapping("/d-aps/{idAnno}/findByAllByTaxYearId")
    @Timed
    public List<DapDto> findByAllByTaxYearId(@PathVariable Long idAnno) 
	{
        log.debug("REST request to get all DapDto by tax year");
        
		final AnnoTributa annoTributa = annoTributaService.findOne(idAnno);
        
		final List<DAP> allByAnnoTributa = dAPRepository.findAllByAnnoTributa(annoTributa);
        
		final List<DapDto> dapDtos = DapMapper.INSTANCE.dapToDapDto(allByAnnoTributa);
        
		log.debug("REST request to get all DapDto by tax year COUNT : {}", dapDtos.size());
        
		return dapDtos;
    }

    /**
     * GET  "/d-aps/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCode : get all DapDto by tax year and Alert Type Unique Code.
     *
     * @param idAnno
     * 		id del año tributario
     * @param alarmUniqueCode
     * 		código del tipo de alarma
     *
     * @return the ResponseEntity with status 200 (OK) and the list of DapDto in body
     */
    @GetMapping("/d-aps/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCode")
    @Timed
    public List<DapDto> findByAllByTaxYearIdAndAlertTypeUniqueCode(@PathVariable Long idAnno, @RequestParam(name = "alarmUniqueCode", required = false) String alarmUniqueCode) 
	{
        String message = "";
        Boolean isValidOption = true;
        log.debug("REST request to get all DapDto by tax year and Alert Type Unique Code");
        List<DAP> page = new ArrayList();
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        
		switch (queryOption) 
		{
            case Constants.ALARMA_DAP_RENOVACIONES:
            case Constants.ALARMA_DAP_PAGOS_ANTICIPADOS:
            case Constants.ALARMA_DAP_REAJUSTES_NEGATIVOS:
            case Constants.ALARMA_DAP_EXCLUIR_CLIENTES_57_BIS:
            case Constants.ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS:
            case Constants.ALARMA_OPCION_NULL:
                page = this.dAPService.findByAllByTaxYearIdAndAlertTypeUniqueCode(idAnno, alarmUniqueCode);
                break;
            default:
                isValidOption = false;
                message = "La opción del servicio no es válida. Opción: " + queryOption + ".";
                break;
        }
		
        final List<DapDto> dtos = DapMapper.INSTANCE.dapToDapDto(page);
        
		log.debug("REST request to get all DapDto by tax year COUNT : {}", dtos.size());
        
		return dtos;
    }

    /**
     * GET  "/d-aps/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated : get all dAP by tax year and Alert Type Unique Code with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param alarmUniqueCode
     * 		código del tipo de alarma
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dAP in body
     */
    @GetMapping("/d-aps/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated")
    @Timed
    public ResponseEntity<List<DAP>> findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(@PathVariable Long idAnno,
        @RequestParam(name = "alarmUniqueCode", required = false) String alarmUniqueCode, @ApiParam Pageable pageable) 
	{
        String message = "";
        Boolean isValidOption = true;
        log.debug("REST request to get all dAP by tax year and Alert Type Unique Code with pagination");
        Page<DAP> page = new PageImpl(new ArrayList());
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        
		switch (queryOption) 
		{
            case Constants.ALARMA_DAP_RENOVACIONES:
            case Constants.ALARMA_DAP_PAGOS_ANTICIPADOS:
            case Constants.ALARMA_DAP_REAJUSTES_NEGATIVOS:
            case Constants.ALARMA_DAP_EXCLUIR_CLIENTES_57_BIS:
            case Constants.ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS:
            case Constants.ALARMA_OPCION_NULL:
                page = this.dAPService.findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(idAnno, alarmUniqueCode, pageable);
                break;
            default:
                isValidOption = false;
                message = "La opción del servicio no es válida. Opción: " + queryOption + ".";
                break;
        }
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/d-aps/{idAnno}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated");
        
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}