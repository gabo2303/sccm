package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.CargaMoneta;
import cl.tutorial.sccm.service.CargaMonetaService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CargaMoneta.
 */
@RestController
@RequestMapping("/api")
public class CargaMonetaResource {

    private final Logger log = LoggerFactory.getLogger(CargaMonetaResource.class);

    private static final String ENTITY_NAME = "cargaMoneta";

    private final CargaMonetaService cargaMonetaService;

    public CargaMonetaResource(CargaMonetaService cargaMonetaService) {
        this.cargaMonetaService = cargaMonetaService;
    }

    /**
     * POST  /carga-monetas : Create a new cargaMoneta.
     *
     * @param cargaMoneta the cargaMoneta to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cargaMoneta, or with status 400 (Bad Request) if the cargaMoneta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/carga-monetas")
    @Timed
    public ResponseEntity<CargaMoneta> createCargaMoneta(@RequestBody CargaMoneta cargaMoneta) throws URISyntaxException {
        log.debug("REST request to save CargaMoneta : {}", cargaMoneta);
        if (cargaMoneta.getId() != null) {
            throw new BadRequestAlertException("A new cargaMoneta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CargaMoneta result = cargaMonetaService.save(cargaMoneta);
        return ResponseEntity.created(new URI("/api/carga-monetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /carga-monetas : Updates an existing cargaMoneta.
     *
     * @param cargaMoneta the cargaMoneta to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cargaMoneta,
     * or with status 400 (Bad Request) if the cargaMoneta is not valid,
     * or with status 500 (Internal Server Error) if the cargaMoneta couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/carga-monetas")
    @Timed
    public ResponseEntity<CargaMoneta> updateCargaMoneta(@RequestBody CargaMoneta cargaMoneta) throws URISyntaxException {
        log.debug("REST request to update CargaMoneta : {}", cargaMoneta);
        if (cargaMoneta.getId() == null) {
            return createCargaMoneta(cargaMoneta);
        }
        CargaMoneta result = cargaMonetaService.save(cargaMoneta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cargaMoneta.getId().toString()))
            .body(result);
    }

    /**
     * GET  /carga-monetas : get all the cargaMonetas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cargaMonetas in body
     */
    @GetMapping("/carga-monetas")
    @Timed
    public List<CargaMoneta> getAllCargaMonetas() {
        log.debug("REST request to get all CargaMonetas");
        return cargaMonetaService.findAll();
        }

    /**
     * GET  /carga-monetas/:id : get the "id" cargaMoneta.
     *
     * @param id the id of the cargaMoneta to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cargaMoneta, or with status 404 (Not Found)
     */
    @GetMapping("/carga-monetas/{id}")
    @Timed
    public ResponseEntity<CargaMoneta> getCargaMoneta(@PathVariable Long id) {
        log.debug("REST request to get CargaMoneta : {}", id);
        CargaMoneta cargaMoneta = cargaMonetaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cargaMoneta));
    }

    /**
     * DELETE  /carga-monetas/:id : delete the "id" cargaMoneta.
     *
     * @param id the id of the cargaMoneta to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/carga-monetas/{id}")
    @Timed
    public ResponseEntity<Void> deleteCargaMoneta(@PathVariable Long id) {
        log.debug("REST request to delete CargaMoneta : {}", id);
        cargaMonetaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
