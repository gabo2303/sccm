package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.LetrasBonos;
import cl.tutorial.sccm.repository.LetrasBonosRepository;
import cl.tutorial.sccm.service.dto.LetrasBonosDto;
import cl.tutorial.sccm.service.mapper.LetrasBonosMapper;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LetrasBonos.
 */
@RestController
@RequestMapping("/api")
public class LetrasBonosResource {

    private final Logger log = LoggerFactory.getLogger(LetrasBonosResource.class);

    private static final String ENTITY_NAME = "letrasBonos";

    private final LetrasBonosRepository letrasBonosRepository;

    public LetrasBonosResource(LetrasBonosRepository letrasBonosRepository) {
        this.letrasBonosRepository = letrasBonosRepository;
    }

    /**
     * POST  /letras-bonos : Create a new letrasBonos.
     *
     * @param letrasBonos
     * 		the letrasBonos to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new letrasBonos, or with status 400 (Bad Request) if the letrasBonos has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/letras-bonos")
    @Timed
    public ResponseEntity<LetrasBonos> createLetrasBonos(@RequestBody LetrasBonos letrasBonos) throws URISyntaxException {
        log.debug("REST request to save LetrasBonos : {}", letrasBonos);
        if (letrasBonos.getId() != null) {
            throw new BadRequestAlertException("A new letrasBonos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LetrasBonos result = letrasBonosRepository.save(letrasBonos);
        return ResponseEntity.created(new URI("/api/letras-bonos/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /letras-bonos : Updates an existing letrasBonos.
     *
     * @param letrasBonos
     * 		the letrasBonos to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated letrasBonos, or with status 400 (Bad Request) if the letrasBonos is not valid, or with status 500
     * (Internal Server Error) if the letrasBonos couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/letras-bonos")
    @Timed
    public ResponseEntity<LetrasBonos> updateLetrasBonos(@RequestBody LetrasBonos letrasBonos) throws URISyntaxException {
        log.debug("REST request to update LetrasBonos : {}", letrasBonos);
        if (letrasBonos.getId() == null) {
            return createLetrasBonos(letrasBonos);
        }
        LetrasBonos result = letrasBonosRepository.save(letrasBonos);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, letrasBonos.getId().toString())).body(result);
    }

    /**
     * GET  /letras-bonos : get all the letrasBonos.
     *
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of letrasBonos in body
     */
    @GetMapping("/letras-bonos")
    @Timed
    public ResponseEntity<List<LetrasBonos>> getAllLetrasBonos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LetrasBonos");
        Page<LetrasBonos> page = letrasBonosRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/letras-bonos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /letras-bonos/:id : get the "id" letrasBonos.
     *
     * @param id
     * 		the id of the letrasBonos to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the letrasBonos, or with status 404 (Not Found)
     */
    @GetMapping("/letras-bonos/{id}")
    @Timed
    public ResponseEntity<LetrasBonos> getLetrasBonos(@PathVariable Long id) {
        log.debug("REST request to get LetrasBonos : {}", id);
        LetrasBonos letrasBonos = letrasBonosRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(letrasBonos));
    }

    /**
     * DELETE  /letras-bonos/:id : delete the "id" letrasBonos.
     *
     * @param id
     * 		the id of the letrasBonos to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/letras-bonos/{id}")
    @Timed
    public ResponseEntity<Void> deleteLetrasBonos(@PathVariable Long id) {
        log.debug("REST request to delete LetrasBonos : {}", id);
        letrasBonosRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  "/letras-bonos/{idAnno}/findByAllByTaxYearIdPaginated : get all letrasBonos by tax year with pagination.
     *
     * @param idAnno
     * 		id del año tributario
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of letrasBonos in body
     */
    @GetMapping("/letras-bonos/{idAnno}/findByAllByTaxYearIdPaginated")
    @Timed
    public ResponseEntity<List<LetrasBonos>> findByAllByTaxYearIdPaginated(@PathVariable Long idAnno, @ApiParam Pageable pageable) {
        log.debug("REST request to get all LetrasBonos by tax year with pagination");
        Page<LetrasBonos> page = letrasBonosRepository.findAllByAnnoTributaId(idAnno, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/letras-bonos/{idAnno}/findByAllByTaxYearIdPaginated");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  "/letras-bonos/{idAnno}/findByAllByTaxYearId : get all LetrasBonosDto by tax year.
     *
     * @param idAnno
     * 		id del año tributario
     *
     * @return the ResponseEntity with status 200 (OK) and the list of LetrasBonosDto in body
     */
    @GetMapping("/letras-bonos/{idAnno}/findByAllByTaxYearId")
    @Timed
    public List<LetrasBonosDto> findByAllByTaxYearId(@PathVariable Long idAnno) {
        log.debug("REST request to get all LetrasBonosDto by tax year");
        final List<LetrasBonos> allByAnnoTributa = letrasBonosRepository.findAllByAnnoTributaId(idAnno);
        final List<LetrasBonosDto> dapDtos = LetrasBonosMapper.INSTANCE.letrasBonosToLetrasBonosDto(allByAnnoTributa);
        log.debug("REST request to get all LetrasBonosDto by tax year COUNT : {}", dapDtos.size());
        return dapDtos;
    }
}
