package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.CertVersion;
import cl.tutorial.sccm.service.CertVersionService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CertVersion.
 */
@RestController
@RequestMapping("/api")
public class CertVersionResource {

    private final Logger log = LoggerFactory.getLogger(CertVersionResource.class);

    private static final String ENTITY_NAME = "certVersion";

    private final CertVersionService certVersionService;

    public CertVersionResource(CertVersionService certVersionService) {
        this.certVersionService = certVersionService;
    }

    /**
     * POST  /cert-versions : Create a new certVersion.
     *
     * @param certVersion the certVersion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new certVersion, or with status 400 (Bad Request) if the certVersion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cert-versions")
    @Timed
    public ResponseEntity<CertVersion> createCertVersion(@RequestBody CertVersion certVersion) throws URISyntaxException {
        log.debug("REST request to save CertVersion : {}", certVersion);
        if (certVersion.getId() != null) {
            throw new BadRequestAlertException("A new certVersion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CertVersion result = certVersionService.save(certVersion);
        return ResponseEntity.created(new URI("/api/cert-versions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cert-versions : Updates an existing certVersion.
     *
     * @param certVersion the certVersion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated certVersion,
     * or with status 400 (Bad Request) if the certVersion is not valid,
     * or with status 500 (Internal Server Error) if the certVersion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cert-versions")
    @Timed
    public ResponseEntity<CertVersion> updateCertVersion(@RequestBody CertVersion certVersion) throws URISyntaxException {
        log.debug("REST request to update CertVersion : {}", certVersion);
        if (certVersion.getId() == null) {
            return createCertVersion(certVersion);
        }
        CertVersion result = certVersionService.save(certVersion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, certVersion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cert-versions : get all the certVersions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of certVersions in body
     */
    @GetMapping("/cert-versions")
    @Timed
    public List<CertVersion> getAllCertVersions() {
        log.debug("REST request to get all CertVersions");
        return certVersionService.findAll();
        }

    /**
     * GET  /cert-versions/:id : get the "id" certVersion.
     *
     * @param id the id of the certVersion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the certVersion, or with status 404 (Not Found)
     */
    @GetMapping("/cert-versions/{id}")
    @Timed
    public ResponseEntity<CertVersion> getCertVersion(@PathVariable Long id) {
        log.debug("REST request to get CertVersion : {}", id);
        CertVersion certVersion = certVersionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(certVersion));
    }

    /**
     * DELETE  /cert-versions/:id : delete the "id" certVersion.
     *
     * @param id the id of the certVersion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cert-versions/{id}")
    @Timed
    public ResponseEntity<Void> deleteCertVersion(@PathVariable Long id) {
        log.debug("REST request to delete CertVersion : {}", id);
        certVersionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
