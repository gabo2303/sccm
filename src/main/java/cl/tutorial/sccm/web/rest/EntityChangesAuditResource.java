package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.service.audit.EntityChangesAuditService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

/**
 * REST controller for managing EntityChangesAuditResource.
 */
@RestController
@RequestMapping("/api")
public class EntityChangesAuditResource {

    private final Logger log = LoggerFactory.getLogger(EntityChangesAuditResource.class);

    private static final String ENTITY_NAME = "entityChangesAudit";

    private final EntityChangesAuditService auditServiceAccessRepository;

    public EntityChangesAuditResource(EntityChangesAuditService auditServiceAccessRepository) {
        this.auditServiceAccessRepository = auditServiceAccessRepository;
    }

    @GetMapping("/entity-changes-audit/findByTableName")
    @Timed
    public List getAllByTableName(String tableName) {
        log.debug("REST request to get audited elements for table " + tableName);
        final List entityChanges = auditServiceAccessRepository.getEntityChangesByTableName(tableName);
        return entityChanges;
    }

    @GetMapping(path = "/entity-changes-audit/findByTableNameAndDates", params = { "startDate", "endDate" })
    @Timed
    public List getAllByTableNameAndDates(@RequestParam(value = "tableName") String tableName, @RequestParam(value = "startDate") LocalDate startDate,
        @RequestParam(value = "endDate") LocalDate endDate) {

        log.debug("REST request to get audited elements for table " + tableName + " between date " + startDate + " and " + endDate);
        final List entityChanges = auditServiceAccessRepository.getEntityChangesByTableNameAndDates(tableName, startDate, endDate);
        return entityChanges;
    }
}
