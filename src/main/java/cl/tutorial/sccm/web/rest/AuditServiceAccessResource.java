package cl.tutorial.sccm.web.rest;

import cl.tutorial.sccm.domain.AuditServiceAccess;
import cl.tutorial.sccm.repository.AuditServiceAccessRepository;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AuditServiceAccess.
 */
@RestController
@RequestMapping("/api")
public class AuditServiceAccessResource {

    private final Logger log = LoggerFactory.getLogger(AuditServiceAccessResource.class);

    private static final String ENTITY_NAME = "auditServiceAccess";

    private final AuditServiceAccessRepository auditServiceAccessRepository;

    public AuditServiceAccessResource(AuditServiceAccessRepository auditServiceAccessRepository) {
        this.auditServiceAccessRepository = auditServiceAccessRepository;
    }

    /**
     * POST  /audit-service-accesses : Create a new auditServiceAccess.
     *
     * @param auditServiceAccess
     * 		the auditServiceAccess to create
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new auditServiceAccess, or with status 400 (Bad Request) if the auditServiceAccess has already an ID
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PostMapping("/audit-service-accesses")
    @Timed
    public ResponseEntity<AuditServiceAccess> createAuditServiceAccess(@RequestBody AuditServiceAccess auditServiceAccess) throws URISyntaxException {
        log.debug("REST request to save AuditServiceAccess : {}", auditServiceAccess);
        if (auditServiceAccess.getId() != null) {
            throw new BadRequestAlertException("A new auditServiceAccess cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditServiceAccess result = auditServiceAccessRepository.save(auditServiceAccess);
        return ResponseEntity.created(new URI("/api/audit-service-accesses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /audit-service-accesses : Updates an existing auditServiceAccess.
     *
     * @param auditServiceAccess
     * 		the auditServiceAccess to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated auditServiceAccess, or with status 400 (Bad Request) if the auditServiceAccess is not valid, or
     * with status 500 (Internal Server Error) if the auditServiceAccess couldn't be updated
     *
     * @throws URISyntaxException
     * 		if the Location URI syntax is incorrect
     */
    @PutMapping("/audit-service-accesses")
    @Timed
    public ResponseEntity<AuditServiceAccess> updateAuditServiceAccess(@RequestBody AuditServiceAccess auditServiceAccess) throws URISyntaxException {
        log.debug("REST request to update AuditServiceAccess : {}", auditServiceAccess);
        if (auditServiceAccess.getId() == null) {
            return createAuditServiceAccess(auditServiceAccess);
        }
        AuditServiceAccess result = auditServiceAccessRepository.save(auditServiceAccess);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, auditServiceAccess.getId().toString())).body(result);
    }

    /**
     * GET  /audit-service-accesses : get all the auditServiceAccesses.
     *
     * @param pageable
     * 		the pagination information
     *
     * @return the ResponseEntity with status 200 (OK) and the list of auditServiceAccesses in body
     */
    @GetMapping("/audit-service-accesses")
    @Timed
    public ResponseEntity<List<AuditServiceAccess>> getAllAuditServiceAccesses(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AuditServiceAccesses");
        final Sort sort = new Sort(new Sort.Order(Sort.Direction.DESC, "timestamp"));
        final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
        Page<AuditServiceAccess> page = auditServiceAccessRepository.findAll(pageRequest);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audit-service-accesses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /audit-service-accesses/:id : get the "id" auditServiceAccess.
     *
     * @param id
     * 		the id of the auditServiceAccess to retrieve
     *
     * @return the ResponseEntity with status 200 (OK) and with body the auditServiceAccess, or with status 404 (Not Found)
     */
    @GetMapping("/audit-service-accesses/{id}")
    @Timed
    public ResponseEntity<AuditServiceAccess> getAuditServiceAccess(@PathVariable Long id) {
        log.debug("REST request to get AuditServiceAccess : {}", id);
        AuditServiceAccess auditServiceAccess = auditServiceAccessRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(auditServiceAccess));
    }

    /**
     * DELETE  /audit-service-accesses/:id : delete the "id" auditServiceAccess.
     *
     * @param id
     * 		the id of the auditServiceAccess to delete
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/audit-service-accesses/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuditServiceAccess(@PathVariable Long id) {
        log.debug("REST request to delete AuditServiceAccess : {}", id);
        auditServiceAccessRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /audit-service-accesses/findAll : get all the auditServiceAccesses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of auditServiceAccesses in body
     */
    @GetMapping("/audit-service-accesses/findAll")
    @Timed
    public List<AuditServiceAccess> getAllAuditServiceAccessesWithoutPagination() {
        log.debug("REST request to get a page of AuditServiceAccesses");
        final Sort sortBy = new Sort(new Sort.Order(Sort.Direction.DESC, "timestamp"));
        List<AuditServiceAccess> page = auditServiceAccessRepository.findAll(sortBy);
        return page;
    }
}
