package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.DatosFijos;
import cl.tutorial.sccm.service.DatosFijosService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DatosFijos.
 */
@RestController
@RequestMapping("/api")
public class DatosFijosResource {

    private final Logger log = LoggerFactory.getLogger(DatosFijosResource.class);

    private static final String ENTITY_NAME = "datosFijos";

    private final DatosFijosService datosFijosService;

    public DatosFijosResource(DatosFijosService datosFijosService) {
        this.datosFijosService = datosFijosService;
    }

    /**
     * POST  /datos-fijos : Create a new datosFijos.
     *
     * @param datosFijos the datosFijos to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datosFijos, or with status 400 (Bad Request) if the datosFijos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/datos-fijos")
    @Timed
    public ResponseEntity<DatosFijos> createDatosFijos(@RequestBody DatosFijos datosFijos) throws URISyntaxException {
        log.debug("REST request to save DatosFijos : {}", datosFijos);
        if (datosFijos.getId() != null) {
            throw new BadRequestAlertException("A new datosFijos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DatosFijos result = datosFijosService.save(datosFijos);
        return ResponseEntity.created(new URI("/api/datos-fijos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /datos-fijos : Updates an existing datosFijos.
     *
     * @param datosFijos the datosFijos to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datosFijos,
     * or with status 400 (Bad Request) if the datosFijos is not valid,
     * or with status 500 (Internal Server Error) if the datosFijos couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/datos-fijos")
    @Timed
    public ResponseEntity<DatosFijos> updateDatosFijos(@RequestBody DatosFijos datosFijos) throws URISyntaxException {
        log.debug("REST request to update DatosFijos : {}", datosFijos);
        if (datosFijos.getId() == null) {
            return createDatosFijos(datosFijos);
        }
        DatosFijos result = datosFijosService.save(datosFijos);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, datosFijos.getId().toString()))
            .body(result);
    }

    /**
     * GET  /datos-fijos : get all the datosFijos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of datosFijos in body
     */
    @GetMapping("/datos-fijos")
    @Timed
    public List<DatosFijos> getAllDatosFijos() {
        log.debug("REST request to get all DatosFijos");
        return datosFijosService.findAll();
        }

    /**
     * GET  /datos-fijos/:id : get the "id" datosFijos.
     *
     * @param id the id of the datosFijos to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the datosFijos, or with status 404 (Not Found)
     */
    @GetMapping("/datos-fijos/{id}")
    @Timed
    public ResponseEntity<DatosFijos> getDatosFijos(@PathVariable Long id) {
        log.debug("REST request to get DatosFijos : {}", id);
        DatosFijos datosFijos = datosFijosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(datosFijos));
    }

    /**
     * DELETE  /datos-fijos/:id : delete the "id" datosFijos.
     *
     * @param id the id of the datosFijos to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/datos-fijos/{id}")
    @Timed
    public ResponseEntity<Void> deleteDatosFijos(@PathVariable Long id) {
        log.debug("REST request to delete DatosFijos : {}", id);
        datosFijosService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
