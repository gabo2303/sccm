package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Contabilidad;
import cl.tutorial.sccm.service.ContabilidadService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Contabilidad.
 */
@RestController
@RequestMapping("/api")
public class ContabilidadResource {

    private final Logger log = LoggerFactory.getLogger(ContabilidadResource.class);

    private static final String ENTITY_NAME = "contabilidad";

    private final ContabilidadService contabilidadService;

    public ContabilidadResource(ContabilidadService contabilidadService) {
        this.contabilidadService = contabilidadService;
    }

    /**
     * POST  /contabilidads : Create a new contabilidad.
     *
     * @param contabilidad the contabilidad to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contabilidad, or with status 400 (Bad Request) if the contabilidad has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contabilidads")
    @Timed
    public ResponseEntity<Contabilidad> createContabilidad(@RequestBody Contabilidad contabilidad) throws URISyntaxException {
        log.debug("REST request to save Contabilidad : {}", contabilidad);
        if (contabilidad.getId() != null) {
            throw new BadRequestAlertException("A new contabilidad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Contabilidad result = contabilidadService.save(contabilidad);
        return ResponseEntity.created(new URI("/api/contabilidads/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contabilidads : Updates an existing contabilidad.
     *
     * @param contabilidad the contabilidad to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contabilidad,
     * or with status 400 (Bad Request) if the contabilidad is not valid,
     * or with status 500 (Internal Server Error) if the contabilidad couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contabilidads")
    @Timed
    public ResponseEntity<Contabilidad> updateContabilidad(@RequestBody Contabilidad contabilidad) throws URISyntaxException {
        log.debug("REST request to update Contabilidad : {}", contabilidad);
        if (contabilidad.getId() == null) {
            return createContabilidad(contabilidad);
        }
        Contabilidad result = contabilidadService.save(contabilidad);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contabilidad.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contabilidads : get all the contabilidads.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contabilidads in body
     */
    @GetMapping("/contabilidads")
    @Timed
    public List<Contabilidad> getAllContabilidads() {
        log.debug("REST request to get all Contabilidads");
        return contabilidadService.findAll();
        }

    /**
     * GET  /contabilidads/:id : get the "id" contabilidad.
     *
     * @param id the id of the contabilidad to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contabilidad, or with status 404 (Not Found)
     */
    @GetMapping("/contabilidads/{id}")
    @Timed
    public ResponseEntity<Contabilidad> getContabilidad(@PathVariable Long id) {
        log.debug("REST request to get Contabilidad : {}", id);
        Contabilidad contabilidad = contabilidadService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(contabilidad));
    }

    /**
     * DELETE  /contabilidads/:id : delete the "id" contabilidad.
     *
     * @param id the id of the contabilidad to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contabilidads/{id}")
    @Timed
    public ResponseEntity<Void> deleteContabilidad(@PathVariable Long id) {
        log.debug("REST request to delete Contabilidad : {}", id);
        contabilidadService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
