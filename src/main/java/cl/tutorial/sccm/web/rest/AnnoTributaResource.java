package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.service.AnnoTributaService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AnnoTributa.
 */
@RestController
@RequestMapping("/api")
public class AnnoTributaResource {
	
    private final Logger log = LoggerFactory.getLogger(AnnoTributaResource.class);

    private static final String ENTITY_NAME = "annoTributa";

    private final AnnoTributaService annoTributaService;

    public AnnoTributaResource(AnnoTributaService annoTributaService) {
        this.annoTributaService = annoTributaService;
    }

    /**
     * POST  /anno-tributas : Create a new annoTributa.
     *
     * @param annoTributa the annoTributa to create
     * @return the ResponseEntity with status 201 (Created) and with body the new annoTributa, or with status 400 (Bad Request) if the annoTributa has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/anno-tributas")
    @Timed
    public ResponseEntity<AnnoTributa> createAnnoTributa(@RequestBody AnnoTributa annoTributa) throws URISyntaxException 
	{
        log.debug("REST request to save AnnoTributa : {}", annoTributa);
        
		/*if (annoTributa.getId() != null) 
		{
            throw new BadRequestAlertException("A new annoTributa cannot already have an ID", ENTITY_NAME, "idexists");
        }*/
        
		AnnoTributa result = annoTributaService.save(annoTributa);
        
		return ResponseEntity.created(new URI("/api/anno-tributas/" + result.getId()))
			   .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
			   .body(result);
    }

    /**
     * PUT  /anno-tributas : Updates an existing annoTributa.
     *
     * @param annoTributa the annoTributa to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated annoTributa,
     * or with status 400 (Bad Request) if the annoTributa is not valid,
     * or with status 500 (Internal Server Error) if the annoTributa couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/anno-tributas")
    @Timed
    public ResponseEntity<AnnoTributa> updateAnnoTributa(@RequestBody AnnoTributa annoTributa) throws URISyntaxException 
	{
        log.debug("REST request to update AnnoTributa : {}", annoTributa);
        
		if (annoTributa.getId() == null) { return createAnnoTributa(annoTributa); }
		
        AnnoTributa result = annoTributaService.save(annoTributa);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, annoTributa.getId().toString())).body(result);
    }

    /**
     * GET  /anno-tributas : get all the annoTributas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of annoTributas in body
     */
    @GetMapping("/anno-tributas")
    @Timed
    public List<AnnoTributa> getAllAnnoTributas() 
	{
        log.debug("REST request to get all AnnoTributas activos");
        return annoTributaService.findAllByAnnoActivo("S");
    }
	
	/**
     * GET  /anno-tributas : get all the annoTributas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of annoTributas in body
     */
    @GetMapping("/anno-tributas/todos-los-annios")
    @Timed
    public List<AnnoTributa> getAll() 
	{
        log.debug("REST request to get all AnnoTributas");
        return annoTributaService.findAllByIdNotEqual(new Long(0));
    }

    /**
     * GET  /anno-tributas/:id : get the "id" annoTributa.
     *
     * @param id the id of the annoTributa to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the annoTributa, or with status 404 (Not Found)
     */
    @GetMapping("/anno-tributas/{id}")
    @Timed
    public ResponseEntity<AnnoTributa> getAnnoTributa(@PathVariable Long id) {
        log.debug("REST request to get AnnoTributa : {}", id);
        AnnoTributa annoTributa = annoTributaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(annoTributa));
    }

    /**
     * DELETE  /anno-tributas/:id : delete the "id" annoTributa.
     *
     * @param id the id of the annoTributa to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/anno-tributas/{id}")
    @Timed
    public ResponseEntity<Void> deleteAnnoTributa(@PathVariable Long id) {
        log.debug("REST request to delete AnnoTributa : {}", id);
        annoTributaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}