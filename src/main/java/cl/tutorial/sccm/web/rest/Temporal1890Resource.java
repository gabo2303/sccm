package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Temporal1890;
import cl.tutorial.sccm.service.Temporal1890Service;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import cl.tutorial.sccm.service.dto.Temporal1890Criteria;
import cl.tutorial.sccm.service.Temporal1890QueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Temporal1890.
 */
@RestController
@RequestMapping("/api")
public class Temporal1890Resource {

    private final Logger log = LoggerFactory.getLogger(Temporal1890Resource.class);

    private static final String ENTITY_NAME = "temporal1890";

    private final Temporal1890Service temporal1890Service;

    private final Temporal1890QueryService temporal1890QueryService;

    public Temporal1890Resource(Temporal1890Service temporal1890Service, Temporal1890QueryService temporal1890QueryService) {
        this.temporal1890Service = temporal1890Service;
        this.temporal1890QueryService = temporal1890QueryService;
    }

    /**
     * GET  /temporal-1890-s : get all the temporal1890S.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of temporal1890S in body
     */
    @GetMapping("/temporal-1890-s")
    @Timed
    public ResponseEntity<List<Temporal1890>> getAllTemporal1890S(Temporal1890Criteria criteria) {
        log.debug("REST request to get Temporal1890S by criteria: {}", criteria);
        List<Temporal1890> entityList = temporal1890QueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /temporal-1890-s/:id : get the "id" temporal1890.
     *
     * @param id the id of the temporal1890 to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the temporal1890, or with status 404 (Not Found)
     */
    @GetMapping("/temporal-1890-s/{id}")
    @Timed
    public ResponseEntity<Temporal1890> getTemporal1890(@PathVariable Long id) {
        log.debug("REST request to get Temporal1890 : {}", id);
        Temporal1890 temporal1890 = temporal1890Service.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(temporal1890));
    }

    /**
     * DELETE  /temporal-1890-s/:id : delete the "id" temporal1890.
     *
     * @param id the id of the temporal1890 to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/temporal-1890-s/{id}")
    @Timed
    public ResponseEntity<Void> deleteTemporal1890(@PathVariable Long id) {
        log.debug("REST request to delete Temporal1890 : {}", id);
        temporal1890Service.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
