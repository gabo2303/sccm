package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Pactos;
import cl.tutorial.sccm.service.PactosService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Pactos.
 */
@RestController
@RequestMapping("/api")
public class PactosResource 
{
    private final Logger log = LoggerFactory.getLogger(PactosResource.class);

    private static final String ENTITY_NAME = "pactos";

    private final PactosService pactosService;
	private final CabeceraControlCambioService cabeceraControlCambioService;
	private final DetalleControlCambioService detalleControlCambioService;

    public PactosResource(PactosService pactosService, CabeceraControlCambioService cabeceraControlCambioService, DetalleControlCambioService detalleControlCambioService) 
	{
        this.pactosService = pactosService;
		this.cabeceraControlCambioService = cabeceraControlCambioService;
		this.detalleControlCambioService = detalleControlCambioService;
    }

    /**
     * POST  /pactos : Create a new pactos.
     *
     * @param pactos the pactos to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pactos, or with status 400 (Bad Request) if the pactos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pactos")
    @Timed
    public ResponseEntity<Pactos> createPactos(@RequestBody Pactos pactos) throws URISyntaxException 
	{
        log.debug("REST request to save Pactos : {}", pactos);
        
		if (pactos.getId() != null) { throw new BadRequestAlertException("A new pactos cannot already have an ID", ENTITY_NAME, "idexists"); }
		
        Pactos result = pactosService.save(pactos);
		
        return ResponseEntity.created(new URI("/api/pactos/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /pactos : Updates an existing pactos.
     *
     * @param pactos the pactos to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pactos,
     * or with status 400 (Bad Request) if the pactos is not valid,
     * or with status 500 (Internal Server Error) if the pactos couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pactos")
    @Timed
    public ResponseEntity<CabeceraControlCambio> updatePactos(@RequestBody Pactos pactos) throws URISyntaxException 
	{
        log.debug("REST request to update Pactos : {}", pactos);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ACTUALIZAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("PACTOS");
		cabeceraControlCambio.setIdRegistro(pactos.getId());		
		cabeceraControlCambio.setAccion("ACTUALIZAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
		
		DetalleControlCambio resultado = guardaCampo(result, "rut", String.valueOf(pactos.getRut()), "C");
		resultado = guardaCampo(result, "dv", pactos.getDv(), "C");
		resultado = guardaCampo(result, "producto", String.valueOf(pactos.getProducto()), "L");
		resultado = guardaCampo(result, "moneda", String.valueOf(pactos.getMoneda()), "L");
		resultado = guardaCampo(result, "fechaPago", pactos.getFechaPago().toString(), "F");
		resultado = guardaCampo(result, "correlativo", String.valueOf(pactos.getCorrelativo().toString()), "C");
		resultado = guardaCampo(result, "fechaInv", pactos.getFechaInv().toString(), "F");
		resultado = guardaCampo(result, "folio", pactos.getFolio(), "C");
		resultado = guardaCampo(result, "filler", pactos.getFiller(), "C");
		resultado = guardaCampo(result, "capital", String.format("%.2f", pactos.getCapital()).replace(",", "."), "C");
		resultado = guardaCampo(result, "capMonOri", String.format("%.2f", pactos.getCapMonOri()).replace(",", "."), "C");
		resultado = guardaCampo(result, "intNom", pactos.getIntNom().toString(), "C");
		resultado = guardaCampo(result, "intReal", pactos.getIntReal().toString(), "C");
		resultado = guardaCampo(result, "monFi", String.format("%.2f", pactos.getMonFi()).replace(",", "."), "C");
		resultado = guardaCampo(result, "numGiro", pactos.getNumGiro().toString(), "C");
		resultado = guardaCampo(result, "codMon", pactos.getCodMon().toString(), "C");		
		resultado = guardaCampo(result, "monPac", pactos.getMonPac(), "C");		
		resultado = guardaCampo(result, "tasMon", pactos.getTasMon(), "C");
		resultado = guardaCampo(result, "tasa", pactos.getTasa().toString(), "C");
		resultado = guardaCampo(result, "perTasa", pactos.getPerTasa().toString(), "C");
		resultado = guardaCampo(result, "reajuste", pactos.getReajuste().toString(), "C");
		resultado = guardaCampo(result, "tipoInt", pactos.getTipoInt(), "C");
		resultado = guardaCampo(result, "codTipInt", pactos.getCodTipInt(), "C");
		resultado = guardaCampo(result, "fecConta", pactos.getFecConta().toString(), "F");
		resultado = guardaCampo(result, "bipersonal", pactos.getBipersonal(), "C");
		resultado = guardaCampo(result, "reajustabilidad", pactos.getReajustabilidad(), "C");
		resultado = guardaCampo(result, "plazo", pactos.getPlazo().toString(), "C");
		resultado = guardaCampo(result, "tipoPago", pactos.getTipoPago(), "C");
		resultado = guardaCampo(result, "origen", pactos.getOrigen(), "C");		
		resultado = guardaCampo(result, "annoTributa", String.valueOf(pactos.getAnnoTributa().getId()), "L");
				
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).body(result);	
    }
	
	/**
     * PUT  /pactos : Updates an existing pactos.
     *
     * @param pactos the pactos to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pactos,
     * or with status 400 (Bad Request) if the pactos is not valid,
     * or with status 500 (Internal Server Error) if the pactos couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pactos/actualizar")
    @Timed
    public ResponseEntity<Pactos> actualizaPactos(@RequestBody Pactos pactos) throws URISyntaxException 
	{
        log.debug("REST request to update Pactos : {}", pactos);
        		
        Pactos result = pactosService.save(pactos);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pactos.getId().toString())).body(result);
    }

    /**
     * GET  /pactos : get all the pactos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pactos in body
     */
    @GetMapping("/pactos")
    @Timed
    public List<Pactos> getAllPactos() 
	{
        log.debug("REST request to get all Pactos");
		
        return pactosService.findAll();
    }

    /**
     * GET  /pactos/:id : get the "id" pactos.
     *
     * @param id the id of the pactos to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pactos, or with status 404 (Not Found)
     */
    @GetMapping("/pactos/{id}")
    @Timed
    public ResponseEntity<Pactos> getPactos(@PathVariable Long id) 
	{
        log.debug("REST request to get Pactos : {}", id);
        
		Pactos pactos = pactosService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pactos));
    }

    /**
     * DELETE  /pactos/:id : delete the "id" pactos.
     *
     * @param id the id of the pactos to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pactos/{id}")
    @Timed
    public ResponseEntity<Void> deletePactos(@PathVariable Long id) 
	{
        log.debug("REST request to delete Pactos : {}", id);
        
		log.debug("REST GUARDAR EN CONTROL POR OPOSICION (ELIMINAR)");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CabeceraControlCambio cabeceraControlCambio = new CabeceraControlCambio();
		
		cabeceraControlCambio.setNombreTabla("PACTOS");
		cabeceraControlCambio.setIdRegistro(id);		
		cabeceraControlCambio.setAccion("ELIMINAR");		
		cabeceraControlCambio.setUsuario(((UserDetails)principal).getUsername());
		cabeceraControlCambio.setEstado("PENDIENTE");
		
		CabeceraControlCambio result = cabeceraControlCambioService.save(cabeceraControlCambio);
				        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cabeceraControlCambio", result.getId().toString())).build();
    }
	
	/**
     * DELETE  /pactos/:id : delete the "id" pactos.
     *
     * @param id the id of the pactos to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pactos/eliminar/{id}")
    @Timed
    public ResponseEntity<Void> eliminarPactos(@PathVariable Long id) 
	{
        log.debug("REST request to delete Pactos : {}", id);
        
		pactosService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/pactos/{idAnno}/getByAnnoId")
    @Timed
    public List<Pactos> findAllByAnnoTributaId(@PathVariable Long idAnno) 
	{
        log.debug("REST request to get all Pactos.");
		
		List<Pactos> pactos = pactosService.findAllByAnnoTributaId(idAnno);
		
        return pactos;
    }
	
	public DetalleControlCambio guardaCampo(CabeceraControlCambio cabecera, String nombreCampo, String valorCampo, String tipoCampo)
	{
		DetalleControlCambio detalleControlCambio = new DetalleControlCambio();
		
		detalleControlCambio.setDetcabcontrol(cabecera);
		detalleControlCambio.setNombreCampo(nombreCampo);		
		detalleControlCambio.setTipoCampo(tipoCampo);		
		detalleControlCambio.setValorCampo(valorCampo); 
		
		return detalleControlCambioService.save(detalleControlCambio);
	}
}