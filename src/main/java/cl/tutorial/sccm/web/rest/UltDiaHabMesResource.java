package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.UltDiaHabMes;
import cl.tutorial.sccm.service.UltDiaHabMesService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UltDiaHabMes.
 */
@RestController
@RequestMapping("/api")
public class UltDiaHabMesResource 
{
    private final Logger log = LoggerFactory.getLogger(UltDiaHabMesResource.class);

    private static final String ENTITY_NAME = "ultDiaHabMes";

    private final UltDiaHabMesService ultDiaHabMesService;

    public UltDiaHabMesResource(UltDiaHabMesService ultDiaHabMesService) { this.ultDiaHabMesService = ultDiaHabMesService; }

    /**
     * POST  /ult-dia-hab-mes : Create a new ultDiaHabMes.
     *
     * @param ultDiaHabMes the ultDiaHabMes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ultDiaHabMes, or with status 400 (Bad Request) if the ultDiaHabMes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ult-dia-hab-mes")
    @Timed
    public ResponseEntity<UltDiaHabMes> createUltDiaHabMes(@RequestBody UltDiaHabMes ultDiaHabMes) throws URISyntaxException 
	{
        log.debug("REST request to save UltDiaHabMes : {}", ultDiaHabMes);
        
		if (ultDiaHabMes.getId() != null) 
		{
            throw new BadRequestAlertException("A new ultDiaHabMes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
		UltDiaHabMes result = ultDiaHabMesService.save(ultDiaHabMes);
        
		return ResponseEntity.created(new URI("/api/ult-dia-hab-mes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ult-dia-hab-mes : Updates an existing ultDiaHabMes.
     *
     * @param ultDiaHabMes the ultDiaHabMes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ultDiaHabMes,
     * or with status 400 (Bad Request) if the ultDiaHabMes is not valid,
     * or with status 500 (Internal Server Error) if the ultDiaHabMes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ult-dia-hab-mes")
    @Timed
    public ResponseEntity<UltDiaHabMes> updateUltDiaHabMes(@RequestBody UltDiaHabMes ultDiaHabMes) throws URISyntaxException 
	{
        log.debug("REST request to update UltDiaHabMes : {}", ultDiaHabMes);
        
		if (ultDiaHabMes.getId() == null) { return createUltDiaHabMes(ultDiaHabMes); }
		
        UltDiaHabMes result = ultDiaHabMesService.save(ultDiaHabMes);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ultDiaHabMes.getId().toString())).body(result);
    }

    /**
     * GET  /ult-dia-hab-mes : get all the ultDiaHabMes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ultDiaHabMes in body
     */
    @GetMapping("/ult-dia-hab-mes")
    @Timed
    public List<UltDiaHabMes> getAllUltDiaHabMes() 
	{
        log.debug("REST request to get all UltDiaHabMes");
        
		return ultDiaHabMesService.findAll();
    }

    /**
     * GET  /ult-dia-hab-mes/:id : get the "id" ultDiaHabMes.
     *
     * @param id the id of the ultDiaHabMes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ultDiaHabMes, or with status 404 (Not Found)
     */
    @GetMapping("/ult-dia-hab-mes/{id}")
    @Timed
    public ResponseEntity<UltDiaHabMes> getUltDiaHabMes(@PathVariable Long id) 
	{
        log.debug("REST request to get UltDiaHabMes : {}", id);
        UltDiaHabMes ultDiaHabMes = ultDiaHabMesService.findOne(id);
        
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ultDiaHabMes));
    }

    /**
     * DELETE  /ult-dia-hab-mes/:id : delete the "id" ultDiaHabMes.
     *
     * @param id the id of the ultDiaHabMes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ult-dia-hab-mes/{id}")
    @Timed
    public ResponseEntity<Void> deleteUltDiaHabMes(@PathVariable Long id) 
	{
        log.debug("REST request to delete UltDiaHabMes : {}", id);
        ultDiaHabMesService.delete(id);
        
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}