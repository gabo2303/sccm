package cl.tutorial.sccm.web.rest;

import com.codahale.metrics.annotation.Timed;
import cl.tutorial.sccm.domain.Rut_temp;
import cl.tutorial.sccm.service.Rut_tempService;
import cl.tutorial.sccm.web.rest.errors.BadRequestAlertException;
import cl.tutorial.sccm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Rut_temp.
 */
@RestController
@RequestMapping("/api")
public class Rut_tempResource {

    private final Logger log = LoggerFactory.getLogger(Rut_tempResource.class);

    private static final String ENTITY_NAME = "rut_temp";

    private final Rut_tempService rut_tempService;

    public Rut_tempResource(Rut_tempService rut_tempService) {
        this.rut_tempService = rut_tempService;
    }


    /**
     * GET  /rut-temps : get all the rut_temps.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of rut_temps in body
     */
    @GetMapping("/rut-temps")
    @Timed
    public List<Rut_temp> getAllRut_temps() {
        log.debug("REST request to get all Rut_temps");
        return rut_tempService.findAll();
        }

    /**
     * GET  /rut-temps/:id : get the "id" rut_temp.
     *
     * @param id the id of the rut_temp to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rut_temp, or with status 404 (Not Found)
     */
    @GetMapping("/rut-temps/{id}")
    @Timed
    public ResponseEntity<Rut_temp> getRut_temp(@PathVariable Long id) {
        log.debug("REST request to get Rut_temp : {}", id);
        Rut_temp rut_temp = rut_tempService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rut_temp));
    }

    /**
     * DELETE  /rut-temps/:id : delete the "id" rut_temp.
     *
     * @param id the id of the rut_temp to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rut-temps/{id}")
    @Timed
    public ResponseEntity<Void> deleteRut_temp(@PathVariable Long id) {
        log.debug("REST request to delete Rut_temp : {}", id);
        rut_tempService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
