package cl.tutorial.sccm.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.domain.*; // for static metamodels
import cl.tutorial.sccm.repository.DetalleControlCambioRepository;
import cl.tutorial.sccm.service.dto.DetalleControlCambioCriteria;


/**
 * Service for executing complex queries for DetalleControlCambio entities in the database.
 * The main input is a {@link DetalleControlCambioCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link DetalleControlCambio} or a {@link Page} of {%link DetalleControlCambio} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class DetalleControlCambioQueryService extends QueryService<DetalleControlCambio> {

    private final Logger log = LoggerFactory.getLogger(DetalleControlCambioQueryService.class);


    private final DetalleControlCambioRepository detalleControlCambioRepository;

    public DetalleControlCambioQueryService(DetalleControlCambioRepository detalleControlCambioRepository) {
        this.detalleControlCambioRepository = detalleControlCambioRepository;
    }

    /**
     * Return a {@link List} of {%link DetalleControlCambio} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DetalleControlCambio> findByCriteria(DetalleControlCambioCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<DetalleControlCambio> specification = createSpecification(criteria);
        return detalleControlCambioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link DetalleControlCambio} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DetalleControlCambio> findByCriteria(DetalleControlCambioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<DetalleControlCambio> specification = createSpecification(criteria);
        return detalleControlCambioRepository.findAll(specification, page);
    }

    /**
     * Function to convert DetalleControlCambioCriteria to a {@link Specifications}
     */
    private Specifications<DetalleControlCambio> createSpecification(DetalleControlCambioCriteria criteria) {
        Specifications<DetalleControlCambio> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DetalleControlCambio_.id));
            }
            if (criteria.getNombreCampo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNombreCampo(), DetalleControlCambio_.nombreCampo));
            }
            if (criteria.getValorCampo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValorCampo(), DetalleControlCambio_.valorCampo));
            }
            if (criteria.getTipoCampo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoCampo(), DetalleControlCambio_.tipoCampo));
            }
            if (criteria.getDetcabcontrolId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getDetcabcontrolId(), DetalleControlCambio_.detcabcontrol, CabeceraControlCambio_.id));
            }
        }
        return specification;
    }

}
