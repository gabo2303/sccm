package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.DatosFijos;
import java.util.List;

/**
 * Service Interface for managing DatosFijos.
 */
public interface DatosFijosService {

    /**
     * Save a datosFijos.
     *
     * @param datosFijos the entity to save
     * @return the persisted entity
     */
    DatosFijos save(DatosFijos datosFijos);

    /**
     *  Get all the datosFijos.
     *
     *  @return the list of entities
     */
    List<DatosFijos> findAll();

    /**
     *  Get the "id" datosFijos.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DatosFijos findOne(Long id);

    /**
     *  Delete the "id" datosFijos.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
