package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.CertificadosAnteriores;
import java.util.List;

/**
 * Service Interface for managing CertificadosAnteriores.
 */
public interface CertificadosAnterioresService {

    /**
     * Save a certificadosAnteriores.
     *
     * @param certificadosAnteriores the entity to save
     * @return the persisted entity
     */
    CertificadosAnteriores save(CertificadosAnteriores certificadosAnteriores);

    /**
     *  Get all the certificadosAnteriores.
     *
     *  @return the list of entities
     */
    List<CertificadosAnteriores> findAll();

    /**
     *  Get the "id" certificadosAnteriores.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CertificadosAnteriores findOne(Long id);

    /**
     *  Delete the "id" certificadosAnteriores.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
