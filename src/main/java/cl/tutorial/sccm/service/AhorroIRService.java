package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.AhorroIR;
import cl.tutorial.sccm.domain.PactosIR;

import java.math.BigInteger;
import java.util.List;

/**
 * Service Interface for managing Pactos.
 */
public interface AhorroIRService {

    List<AhorroIR> findAllByAnnoTributa(BigInteger annoId);
}
