package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.LHDCV;
import java.util.List;

/**
 * Service Interface for managing LHDCV.
 */
public interface LHDCVService {

    /**
     * Save a lHDCV.
     *
     * @param lHDCV the entity to save
     * @return the persisted entity
     */
    LHDCV save(LHDCV lHDCV);

    /**
     *  Get all the lHDCVS.
     *
     *  @return the list of entities
     */
    List<LHDCV> findAll();

    /**
     *  Get the "id" lHDCV.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LHDCV findOne(Long id);

    /**
     *  Delete the "id" lHDCV.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
