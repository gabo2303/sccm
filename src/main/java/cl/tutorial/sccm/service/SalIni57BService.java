package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.SalIni57B;
import java.util.List;

/**
 * Service Interface for managing SalIni57B.
 */
public interface SalIni57BService {

    /**
     * Save a salIni57B.
     *
     * @param salIni57B the entity to save
     * @return the persisted entity
     */
    SalIni57B save(SalIni57B salIni57B);

    /**
     *  Get all the salIni57BS.
     *
     *  @return the list of entities
     */
    List<SalIni57B> findAll();

    /**
     *  Get the "id" salIni57B.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SalIni57B findOne(Long id);

    /**
     *  Delete the "id" salIni57B.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
