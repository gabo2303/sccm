package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Temporal1890;
import java.util.List;

/**
 * Service Interface for managing Temporal1890.
 */
public interface Temporal1890Service {

    /**
     * Save a temporal1890.
     *
     * @param temporal1890 the entity to save
     * @return the persisted entity
     */
    Temporal1890 save(Temporal1890 temporal1890);

    /**
     *  Get all the temporal1890S.
     *
     *  @return the list of entities
     */
    List<Temporal1890> findAll();

    /**
     *  Get the "id" temporal1890.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Temporal1890 findOne(Long id);

    /**
     *  Delete the "id" temporal1890.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
