package cl.tutorial.sccm.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import cl.tutorial.sccm.domain.Clientes1890mantencion;
import cl.tutorial.sccm.domain.*; // for static metamodels
import cl.tutorial.sccm.repository.Clientes1890mantencionRepository;
import cl.tutorial.sccm.service.dto.Clientes1890mantencionCriteria;


/**
 * Service for executing complex queries for Clientes1890mantencion entities in the database.
 * The main input is a {@link Clientes1890mantencionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Clientes1890mantencion} or a {@link Page} of {%link Clientes1890mantencion} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class Clientes1890mantencionQueryService extends QueryService<Clientes1890mantencion> {

    private final Logger log = LoggerFactory.getLogger(Clientes1890mantencionQueryService.class);


    private final Clientes1890mantencionRepository clientes1890mantencionRepository;

    public Clientes1890mantencionQueryService(Clientes1890mantencionRepository clientes1890mantencionRepository) {
        this.clientes1890mantencionRepository = clientes1890mantencionRepository;
    }

    /**
     * Return a {@link List} of {%link Clientes1890mantencion} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Clientes1890mantencion> findByCriteria(Clientes1890mantencionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Clientes1890mantencion> specification = createSpecification(criteria);
        return clientes1890mantencionRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Clientes1890mantencion} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Clientes1890mantencion> findByCriteria(Clientes1890mantencionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Clientes1890mantencion> specification = createSpecification(criteria);
        return clientes1890mantencionRepository.findAll(specification, page);
    }

    /**
     * Function to convert Clientes1890mantencionCriteria to a {@link Specifications}
     */
    private Specifications<Clientes1890mantencion> createSpecification(Clientes1890mantencionCriteria criteria) {
        Specifications<Clientes1890mantencion> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getRut() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRut(), Clientes1890mantencion_.rut));
            }
        }
        return specification;
    }

}
