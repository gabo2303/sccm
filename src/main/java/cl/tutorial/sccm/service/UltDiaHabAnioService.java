package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.UltDiaHabAnio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing UltDiaHabAnio.
 */
public interface UltDiaHabAnioService {

    /**
     * Save a ultDiaHabAnio.
     *
     * @param ultDiaHabAnio the entity to save
     * @return the persisted entity
     */
    UltDiaHabAnio save(UltDiaHabAnio ultDiaHabAnio);

    /**
     *  Get all the ultDiaHabAnios.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UltDiaHabAnio> findAll(Pageable pageable);

    /**
     *  Get the "id" ultDiaHabAnio.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UltDiaHabAnio findOne(Long id);

    /**
     *  Delete the "id" ultDiaHabAnio.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
