package cl.tutorial.sccm.service.audit;

import cl.tutorial.sccm.repository.audit.EntityChangesAuditRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class EntityChangesAuditService {

    private final EntityChangesAuditRepository entityChangesAuditRepository;

    public EntityChangesAuditService(EntityChangesAuditRepository entityChangesAuditRepository1) {
        this.entityChangesAuditRepository = entityChangesAuditRepository1;
    }

    public List getEntityChangesByTableName(String tableName) {
        return entityChangesAuditRepository.getEntityChangesByTableName(tableName);
    }

    public List getEntityChangesByTableNameAndDates(String tableName, LocalDate startDate, LocalDate endDate) {
        return entityChangesAuditRepository.getEntityChangesByTableNameAndDates(tableName, startDate, endDate);
    }
}
