package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.UltDiaHabMes;
import java.util.List;

/**
 * Service Interface for managing UltDiaHabMes.
 */
public interface UltDiaHabMesService 
{
    /**
     * Save a ultDiaHabMes.
     *
     * @param ultDiaHabMes the entity to save
     * @return the persisted entity
     */
    UltDiaHabMes save(UltDiaHabMes ultDiaHabMes);

    /**
     *  Get all the ultDiaHabMes.
     *
     *  @return the list of entities
     */
    List<UltDiaHabMes> findAll();

    /**
     *  Get the "id" ultDiaHabMes.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UltDiaHabMes findOne(Long id);

    /**
     *  Delete the "id" ultDiaHabMes.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}