package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Usd;
import java.util.List;

/**
 * Service Interface for managing Usd.
 */
public interface UsdService {

    /**
     * Save a usd.
     *
     * @param usd the entity to save
     * @return the persisted entity
     */
    Usd save(Usd usd);

    /**
     *  Get all the usds.
     *
     *  @return the list of entities
     */
    List<Usd> findAll();

    /**
     *  Get the "id" usd.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Usd findOne(Long id);

    /**
     *  Delete the "id" usd.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
