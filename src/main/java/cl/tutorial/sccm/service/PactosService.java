package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Pactos;
import java.util.List;

/**
 * Service Interface for managing Pactos.
 */
public interface PactosService {

    /**
     * Save a pactos.
     *
     * @param pactos the entity to save
     * @return the persisted entity
     */
    Pactos save(Pactos pactos);

    /**
     *  Get all the pactos.
     *
     *  @return the list of entities
     */
    List<Pactos> findAll();

    /**
     *  Get the "id" pactos.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Pactos findOne(Long id);

    /**
     *  Delete the "id" pactos.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<Pactos> findAllByAnnoTributaId(Long annoId);
}
