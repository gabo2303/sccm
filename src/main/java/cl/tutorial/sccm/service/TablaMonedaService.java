package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.TablaMoneda;
import java.util.List;

/**
 * Service Interface for managing TablaMoneda.
 */
public interface TablaMonedaService {

    /**
     * Save a tablaMoneda.
     *
     * @param tablaMoneda the entity to save
     * @return the persisted entity
     */
    TablaMoneda save(TablaMoneda tablaMoneda);

    /**
     *  Get all the tablaMonedas.
     *
     *  @return the list of entities
     */
    List<TablaMoneda> findAll();

    /**
     *  Get the "id" tablaMoneda.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TablaMoneda findOne(Long id);

    /**
     *  Delete the "id" tablaMoneda.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
