package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.LetrasBonos;
import cl.tutorial.sccm.service.dto.LetrasBonosDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LetrasBonosMapper {

    LetrasBonosMapper INSTANCE = Mappers.getMapper(LetrasBonosMapper.class);

    @Mapping(target = "fechaInv", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaPago", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fecCont", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fecVcto", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "capital", expression =
        "java( elem.getCapital() == null ? \"\" : (!elem.getCapital().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getCapital()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "capMon",
        expression = "java( elem.getCapMon() == null ? \"\" : (!elem.getCapMon().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getCapMon()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "impExt", expression =
        "java( elem.getImpExt() == null ? \"\" : (!elem.getImpExt().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getImpExt()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "intNom", expression =
        "java( elem.getIntNom() == null ? \"\" : (!elem.getIntNom().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getIntNom()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "intReal", expression =
        "java( elem.getIntReal() == null ? \"\" : (!elem.getIntReal().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getIntReal()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "monTasa", expression =
        "java( elem.getMonTasa() == null ? \"\" : (!elem.getMonTasa().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getMonTasa()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "tasaEmi", expression =
        "java( elem.getTasaEmi() == null ? \"\" : (!elem.getTasaEmi().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getTasaEmi()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "valorNominal",
        expression = "java( elem.getValorNominal() == null ? \"\" : (!elem.getValorNominal().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getValorNominal()).replace('.', ',') : \"0\" ))")
    LetrasBonosDto letrasBonosToLetrasBonosDto(LetrasBonos elem);

    List<LetrasBonosDto> letrasBonosToLetrasBonosDto(List<LetrasBonos> list);
}
