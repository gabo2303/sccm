package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.DapIR;
import cl.tutorial.sccm.service.dto.DapIrDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DapIrMapper {

    DapIrMapper INSTANCE = Mappers.getMapper(DapIrMapper.class);

    @Mapping(target = "fechaPago", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "intReal", expression =
        "java( elem.getIntReal() == null ? \"\" : (!elem.getIntReal().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getIntReal())" + ".replace('.', ',') : \"0\" ))")
    @Mapping(target = "intRealCal",
        expression = "java( elem.getIntRealCal() == null ? \"\" : (!elem.getIntRealCal().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getIntRealCal()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "diferencia",
        expression = "java( elem.getDiferencia() == null ? \"\" : (!elem.getDiferencia().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getDiferencia()).replace('.', ',') : \"0\" ))")
    DapIrDto daIrToDapIrDto(DapIR elem);

    List<DapIrDto> daIrToDapIrDto(List<DapIR> list);
}
