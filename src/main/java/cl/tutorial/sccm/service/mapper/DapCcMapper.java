package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.DapCC;
import cl.tutorial.sccm.service.dto.DapCcDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DapCcMapper {

    DapCcMapper INSTANCE = Mappers.getMapper(DapCcMapper.class);

    @Mapping(target = "fechaContable", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fcc", expression =
        "java( elem.getFcc() == null ? \"\" : (!elem.getFcc().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getFcc())" + ".replace('.', ',') : \"0\" ))")
    @Mapping(target = "contabilidad",
        expression = "java( elem.getContabilidad() == null ? \"\" : (!elem.getContabilidad().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getContabilidad()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "diferencia", expression =
        "java( elem.getDiferencia() == null ? \"\" : (!elem.getDiferencia().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getDiferencia()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "diferenciaAbs",
        expression = "java( elem.getDiferenciaAbs() == null ? \"\" : (!elem.getDiferenciaAbs().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getDiferenciaAbs()).replace('.', ',') : \"0\" ))")
    DapCcDto dapCcToDapCcDto(DapCC elem);

    List<DapCcDto> dapCcToDapCcDto(List<DapCC> list);
}
