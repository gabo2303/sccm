package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.DAP;
import cl.tutorial.sccm.service.dto.DapDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DapMapper {

    DapMapper INSTANCE = Mappers.getMapper(DapMapper.class);

    @Mapping(target = "fecVcto", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaInv", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fecCont", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaPago", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "capital", expression =
        "java( elem.getCapital() == null ? \"\" : (!elem.getCapital().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getCapital())" + ".replace('.', ',') : \"0\" ))")
    @Mapping(target = "capMon", expression =
        "java( elem.getCapMon() == null ? \"\" : (!elem.getCapMon().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getCapMon()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "intNom", expression =
        "java( elem.getIntNom() == null ? \"\" : (!elem.getIntNom().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT,elem"
            + ".getIntNom()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "tasaOp", expression =
        "java( elem.getTasaOp() == null ? \"\" : (!elem.getTasaOp().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getTasaOp()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "intPagado", expression =
        "java( elem.getIntPagado() == null ? \"\" : (!elem.getIntPagado().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getIntPagado()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "monFi", expression =
        "java( elem.getMonFi() == null ? \"\" : (!elem.getMonFi().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getMonFi()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "monTasa", expression =
        "java( elem.getMonTasa() == null ? \"\" : (!elem.getMonTasa().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getMonTasa()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "impExt", expression =
        "java( elem.getImpExt() == null ? \"\" : (!elem.getImpExt().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getImpExt()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "interesNominalPesos", expression =
        "java( elem.getInteresNominalPesos() == null ? \"\" : (!elem.getInteresNominalPesos().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getInteresNominalPesos()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "reajusteNegativo", expression =
        "java( elem.getReajusteNegativo() == null ? \"\" : (!elem.getReajusteNegativo().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getReajusteNegativo()).replace('.', ',') : \"0\" ))")
    DapDto dapToDapDto(DAP elem);

    List<DapDto> dapToDapDto(List<DAP> list);
}
