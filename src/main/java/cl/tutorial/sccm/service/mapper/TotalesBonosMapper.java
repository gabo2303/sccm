package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.TotalesBonos;
import cl.tutorial.sccm.service.dto.TotalesBonosDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TotalesBonosMapper {

    TotalesBonosMapper INSTANCE = Mappers.getMapper(TotalesBonosMapper.class);

    @Mapping(target = "fechaEvento", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaInversion", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaPago", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "montoPagadoMo",
        expression = "java( elem.getMontoPagadoMo() == null ? \"\" : (!elem.getMontoPagadoMo().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getMontoPagadoMo())" + ".replace('.', ',') : \"0\" ))")
    @Mapping(target = "montoPagadoValor", expression =
        "java( elem.getMontoPagadoValor() == null ? \"\" : (!elem.getMontoPagadoValor().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getMontoPagadoValor()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "valorNominal",
        expression = "java( elem.getValorNominal() == null ? \"\" : (!elem.getValorNominal().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getValorNominal()).replace('.', ',') : \"0\" ))")
    TotalesBonosDto totalesBonosToTotalesBonosDto(TotalesBonos elem);

    List<TotalesBonosDto> totalesBonosToTotalesBonosDto(List<TotalesBonos> list);
}
