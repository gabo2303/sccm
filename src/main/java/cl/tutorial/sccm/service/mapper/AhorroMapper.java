package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.Ahorro;
import cl.tutorial.sccm.service.dto.AhorroDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AhorroMapper {

    AhorroMapper INSTANCE = Mappers.getMapper(AhorroMapper.class);

    @Mapping(target = "fechaPago", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaInv", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaApe", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaCon", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "fechaVen", dateFormat = Constants.PATTERN_DATE_SHORT)
    @Mapping(target = "capital", expression =
        "java( elem.getCapital() == null ? \"\" : (!elem.getCapital().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getCapital())" + ".replace('.', ',') : \"0\" ))")
    @Mapping(target = "intPagado", expression =
        "java( elem.getIntPagado() == null ? \"\" : (!elem.getIntPagado().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, "
            + "elem.getIntPagado()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "intReal", expression =
        "java( elem.getIntReal() == null ? \"\" : (!elem.getIntReal().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT, elem"
            + ".getIntReal()).replace('.', ',') : \"0\" ))")
    @Mapping(target = "reajPagado", expression =
        "java( elem.getReajPagado() == null ? \"\" : (!elem.getReajPagado().equals(Double.valueOf(\"0\")) ? String.format(cl.tutorial.sccm.common.Constants.STRING_DOUBLE_FORMAT,"
            + " elem"
            + ".getReajPagado()).replace('.', ',') : \"0\" ))")
    AhorroDto ahorroToAhorroDto(Ahorro elem);

    List<AhorroDto> ahorroToAhorroDto(List<Ahorro> list);
}
