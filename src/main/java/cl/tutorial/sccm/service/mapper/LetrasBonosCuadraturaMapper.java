package cl.tutorial.sccm.service.mapper;

import cl.tutorial.sccm.domain.LetrasBonosCuadratura;
import cl.tutorial.sccm.service.dto.LetrasBonosCuadraturaDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LetrasBonosCuadraturaMapper {

    LetrasBonosCuadraturaMapper INSTANCE = Mappers.getMapper(LetrasBonosCuadraturaMapper.class);

    @Mapping(target = "montoPagado",
        expression = "java( elem.getMontoPagado() == null ? \"\" : (!elem.getMontoPagado().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getMontoPagado())" + ".replace('.', ',') : \"0\" ))")
    @Mapping(target = "valorNominal",
        expression = "java( elem.getValorNominal() == null ? \"\" : (!elem.getValorNominal().equals(java.math.BigDecimal.ZERO) ? String.format(cl.tutorial.sccm.common.Constants"
            + ".STRING_DOUBLE_FORMAT, elem.getValorNominal()).replace('.', ',') : \"0\" ))")
    LetrasBonosCuadraturaDto letrasBonosCuadraturaToLetrasBonosCuadraturaDto(LetrasBonosCuadratura elem);

    List<LetrasBonosCuadraturaDto> letrasBonosCuadraturaToLetrasBonosCuadraturaDto(List<LetrasBonosCuadratura> list);
}
