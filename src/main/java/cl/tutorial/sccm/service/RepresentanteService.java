package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Representante;
import java.util.List;

/**
 * Service Interface for managing Representante.
 */
public interface RepresentanteService {

    /**
     * Save a representante.
     *
     * @param representante the entity to save
     * @return the persisted entity
     */
    Representante save(Representante representante);

    /**
     *  Get all the representantes.
     *
     *  @return the list of entities
     */
    List<Representante> findAll();

    /**
     *  Get the "id" representante.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Representante findOne(Long id);

    /**
     *  Delete the "id" representante.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<Representante> findAllByAnnoTributaIdAndInstrumentoId(Long annoId, Long instId);
}
