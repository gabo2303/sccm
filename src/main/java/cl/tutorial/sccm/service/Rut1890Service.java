package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Rut1890;
import java.util.List;
import java.math.BigInteger;
import java.util.ArrayList;
import org.springframework.data.domain.Page;

/**
 * Service Interface for managing Rut1890.
 */
public interface Rut1890Service 
{
    /**
     * Save a rut1890.
     *
     * @param rut1890 the entity to save
     * @return the persisted entity
     */
    Rut1890 save(Rut1890 rut1890);

    /**
     *  Get all the rut1890S.
     *
     *  @return the list of entities
     */
    List<Rut1890> findAll();

    /**
     *  Get the "id" rut1890.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Rut1890 findOne(Long id);

    /**
     *  Delete the "id" rut1890.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
	
	List<Object> findAllByRut(Integer rut);
}