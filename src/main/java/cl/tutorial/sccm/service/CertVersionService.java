package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.CertVersion;
import java.util.List;

/**
 * Service Interface for managing CertVersion.
 */
public interface CertVersionService {

    /**
     * Save a certVersion.
     *
     * @param certVersion the entity to save
     * @return the persisted entity
     */
    CertVersion save(CertVersion certVersion);

    /**
     *  Get all the certVersions.
     *
     *  @return the list of entities
     */
    List<CertVersion> findAll();

    /**
     *  Get the "id" certVersion.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CertVersion findOne(Long id);

    /**
     *  Delete the "id" certVersion.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
