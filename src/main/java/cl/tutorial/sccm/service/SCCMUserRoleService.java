package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.SCCMUserRole;
import java.util.List;

/**
 * Service Interface for managing SCCMUserRole.
 */
public interface SCCMUserRoleService {

    /**
     * Save a sCCMUserRole.
     *
     * @param sCCMUserRole the entity to save
     * @return the persisted entity
     */
    SCCMUserRole save(SCCMUserRole sCCMUserRole);

    /**
     *  Get all the sCCMUserRoles.
     *
     *  @return the list of entities
     */
    List<SCCMUserRole> findAll();

    /**
     *  Get the "id" sCCMUserRole.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SCCMUserRole findOne(Long id);

    /**
     *  Delete the "id" sCCMUserRole.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
