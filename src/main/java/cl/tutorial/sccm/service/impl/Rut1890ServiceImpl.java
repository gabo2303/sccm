package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Rut1890Service;
import cl.tutorial.sccm.domain.Rut1890;
import cl.tutorial.sccm.repository.Rut1890Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Service Implementation for managing Rut1890.
 */
@Service
@Transactional
public class Rut1890ServiceImpl implements Rut1890Service
{
    private final Logger log = LoggerFactory.getLogger(Rut1890ServiceImpl.class);

    private final Rut1890Repository rut1890Repository;

    public Rut1890ServiceImpl(Rut1890Repository rut1890Repository) { this.rut1890Repository = rut1890Repository; }

    /**
     * Save a rut1890.
     *
     * @param rut1890 the entity to save
     * @return the persisted entity
     */
    @Override
    public Rut1890 save(Rut1890 rut1890) 
	{
        log.debug("Request to save Rut1890 : {}", rut1890);
        
		return rut1890Repository.save(rut1890);
    }

    /**
     *  Get all the rut1890S.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Rut1890> findAll() 
	{
        log.debug("Request to get all Rut1890S");
        
		return rut1890Repository.findAll();
    }

    /**
     *  Get one rut1890 by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Rut1890 findOne(Long id) 
	{
        log.debug("Request to get Rut1890 : {}", id);
        
		return rut1890Repository.findOne(id);
    }

    /**
     *  Delete the  rut1890 by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) 
	{
        log.debug("Request to delete Rut1890 : {}", id);
        
		rut1890Repository.delete(id);
    }
	
	@Override
    @SuppressWarnings("Duplicates")
    public List<Object> findAllByRut(Integer rut) 
	{
		log.debug("Request to match with RUT : {}", rut);
		
        List<Object> list;
		
        list = rut1890Repository.findAllByRut(rut);
        
		return list;
    }
}