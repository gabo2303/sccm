package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.LHBonosService;
import cl.tutorial.sccm.domain.LHBonos;
import cl.tutorial.sccm.repository.LHBonosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing LHBonos.
 */
@Service
@Transactional
public class LHBonosServiceImpl implements LHBonosService{

    private final Logger log = LoggerFactory.getLogger(LHBonosServiceImpl.class);

    private final LHBonosRepository lHBonosRepository;

    public LHBonosServiceImpl(LHBonosRepository lHBonosRepository) {
        this.lHBonosRepository = lHBonosRepository;
    }

    /**
     * Save a lHBonos.
     *
     * @param lHBonos the entity to save
     * @return the persisted entity
     */
    @Override
    public LHBonos save(LHBonos lHBonos) {
        log.debug("Request to save LHBonos : {}", lHBonos);
        return lHBonosRepository.save(lHBonos);
    }

    /**
     *  Get all the lHBonos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LHBonos> findAll() {
        log.debug("Request to get all LHBonos");
        return lHBonosRepository.findAll();
    }

    /**
     *  Get one lHBonos by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LHBonos findOne(Long id) {
        log.debug("Request to get LHBonos : {}", id);
        return lHBonosRepository.findOne(id);
    }

    /**
     *  Delete the  lHBonos by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LHBonos : {}", id);
        lHBonosRepository.delete(id);
    }
}
