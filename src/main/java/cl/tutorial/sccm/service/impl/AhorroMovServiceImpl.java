package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.AhorroMovService;
import cl.tutorial.sccm.domain.AhorroMov;
import cl.tutorial.sccm.repository.AhorroMovRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing AhorroMov.
 */
@Service
@Transactional
public class AhorroMovServiceImpl implements AhorroMovService{

    private final Logger log = LoggerFactory.getLogger(AhorroMovServiceImpl.class);

    private final AhorroMovRepository ahorroMovRepository;

    public AhorroMovServiceImpl(AhorroMovRepository ahorroMovRepository) {
        this.ahorroMovRepository = ahorroMovRepository;
    }

    /**
     * Save a ahorroMov.
     *
     * @param ahorroMov the entity to save
     * @return the persisted entity
     */
    @Override
    public AhorroMov save(AhorroMov ahorroMov) {
        log.debug("Request to save AhorroMov : {}", ahorroMov);
        return ahorroMovRepository.save(ahorroMov);
    }

    /**
     *  Get all the ahorroMovs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AhorroMov> findAll() {
        log.debug("Request to get all AhorroMovs");
        return ahorroMovRepository.findAll();
    }

    /**
     *  Get one ahorroMov by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AhorroMov findOne(Long id) {
        log.debug("Request to get AhorroMov : {}", id);
        return ahorroMovRepository.findOne(id);
    }

    /**
     *  Delete the  ahorroMov by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AhorroMov : {}", id);
        ahorroMovRepository.delete(id);
    }
}
