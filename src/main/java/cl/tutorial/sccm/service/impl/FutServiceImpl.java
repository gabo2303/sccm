package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.FutService;
import cl.tutorial.sccm.domain.Fut;
import cl.tutorial.sccm.repository.FutRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Fut.
 */
@Service
@Transactional
public class FutServiceImpl implements FutService{

    private final Logger log = LoggerFactory.getLogger(FutServiceImpl.class);

    private final FutRepository futRepository;

    public FutServiceImpl(FutRepository futRepository) {
        this.futRepository = futRepository;
    }

    /**
     * Save a fut.
     *
     * @param fut the entity to save
     * @return the persisted entity
     */
    @Override
    public Fut save(Fut fut) {
        log.debug("Request to save Fut : {}", fut);
        return futRepository.save(fut);
    }

    /**
     *  Get all the futs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Fut> findAll() {
        log.debug("Request to get all Futs");
        return futRepository.findAll();
    }

    /**
     *  Get one fut by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Fut findOne(Long id) {
        log.debug("Request to get Fut : {}", id);
        return futRepository.findOne(id);
    }

    /**
     *  Delete the  fut by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Fut : {}", id);
        futRepository.delete(id);
    }

    @Override
    public List<Fut> findAllByAnnoTributaId(Long annoId) {
        return  futRepository.findAllByAnnoTributaId(annoId);
    }
}
