package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.DatosFijosService;
import cl.tutorial.sccm.domain.DatosFijos;
import cl.tutorial.sccm.repository.DatosFijosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing DatosFijos.
 */
@Service
@Transactional
public class DatosFijosServiceImpl implements DatosFijosService{

    private final Logger log = LoggerFactory.getLogger(DatosFijosServiceImpl.class);

    private final DatosFijosRepository datosFijosRepository;

    public DatosFijosServiceImpl(DatosFijosRepository datosFijosRepository) {
        this.datosFijosRepository = datosFijosRepository;
    }

    /**
     * Save a datosFijos.
     *
     * @param datosFijos the entity to save
     * @return the persisted entity
     */
    @Override
    public DatosFijos save(DatosFijos datosFijos) {
        log.debug("Request to save DatosFijos : {}", datosFijos);
        return datosFijosRepository.save(datosFijos);
    }

    /**
     *  Get all the datosFijos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DatosFijos> findAll() {
        log.debug("Request to get all DatosFijos");
        return datosFijosRepository.findAll();
    }

    /**
     *  Get one datosFijos by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DatosFijos findOne(Long id) {
        log.debug("Request to get DatosFijos : {}", id);
        return datosFijosRepository.findOne(id);
    }

    /**
     *  Delete the  datosFijos by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DatosFijos : {}", id);
        datosFijosRepository.delete(id);
    }
}
