package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.Bonos;
import cl.tutorial.sccm.repository.BonosRepository;
import cl.tutorial.sccm.service.BonosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Bonos.
 */
@Service
@Transactional
public class BonosServiceImpl implements BonosService {

    private final Logger log = LoggerFactory.getLogger(BonosServiceImpl.class);

    private final BonosRepository bonosRepository;

    public BonosServiceImpl(BonosRepository bonosRepository) {
        this.bonosRepository = bonosRepository;
    }

    /**
     * Save a bonos.
     *
     * @param bonos
     * 		the entity to save
     *
     * @return the persisted entity
     */
    @Override
    public Bonos save(Bonos bonos) {
        log.debug("Request to save Bonos : {}", bonos);
        return bonosRepository.save(bonos);
    }

    /**
     * Get all the bonos.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Bonos> findAll() {
        log.debug("Request to get all Bonos");
        return bonosRepository.findAll();
    }

    /**
     * Get one bonos by id.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Bonos findOne(Long id) {
        log.debug("Request to get Bonos : {}", id);
        return bonosRepository.findOne(id);
    }

    /**
     * Delete the  bonos by id.
     *
     * @param id
     * 		the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bonos : {}", id);
        bonosRepository.delete(id);
    }
}
