package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.Pactos;
import cl.tutorial.sccm.domain.PactosIR;
import cl.tutorial.sccm.repository.PactosIRRepository;
import cl.tutorial.sccm.repository.PactosRepository;
import cl.tutorial.sccm.service.PactosIRService;
import cl.tutorial.sccm.service.PactosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Service Implementation for managing Pactos.
 */
@Service
@Transactional
public class PactosIRServiceImpl implements PactosIRService{

    private final PactosIRRepository pactosIRRepository;

    public PactosIRServiceImpl(PactosIRRepository pactosIRRepository) {
        this.pactosIRRepository = pactosIRRepository;
    }


    @Override
    public List<PactosIR> findAllByAnnoTributa(Long annoId) {
        return this.pactosIRRepository.findAllByAnnoTributa(annoId);
    }
}
