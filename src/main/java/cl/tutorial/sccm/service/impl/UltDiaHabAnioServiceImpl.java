package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.UltDiaHabAnioService;
import cl.tutorial.sccm.domain.UltDiaHabAnio;
import cl.tutorial.sccm.repository.UltDiaHabAnioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UltDiaHabAnio.
 */
@Service
@Transactional
public class UltDiaHabAnioServiceImpl implements UltDiaHabAnioService
{
    private final Logger log = LoggerFactory.getLogger(UltDiaHabAnioServiceImpl.class);

    private final UltDiaHabAnioRepository ultDiaHabAnioRepository;

    public UltDiaHabAnioServiceImpl(UltDiaHabAnioRepository ultDiaHabAnioRepository) { this.ultDiaHabAnioRepository = ultDiaHabAnioRepository; }

    /**
     * Save a ultDiaHabAnio.
     *
     * @param ultDiaHabAnio the entity to save
     * @return the persisted entity
     */
    @Override
    public UltDiaHabAnio save(UltDiaHabAnio ultDiaHabAnio) 
	{
        log.debug("Request to save UltDiaHabAnio : {}", ultDiaHabAnio);
        
		return ultDiaHabAnioRepository.save(ultDiaHabAnio);
    }

    /**
     *  Get all the ultDiaHabAnios.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UltDiaHabAnio> findAll(Pageable pageable) 
	{
        log.debug("Request to get all UltDiaHabAnios");
		
        return ultDiaHabAnioRepository.findAll(pageable);
    }

    /**
     *  Get one ultDiaHabAnio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UltDiaHabAnio findOne(Long id) 
	{
        log.debug("Request to get UltDiaHabAnio : {}", id);
		
        return ultDiaHabAnioRepository.findOne(id);
    }

    /**
     *  Delete the  ultDiaHabAnio by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) 
	{
        log.debug("Request to delete UltDiaHabAnio : {}", id);
		
        ultDiaHabAnioRepository.delete(id);
    }
}