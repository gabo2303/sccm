package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.TotalProdService;
import cl.tutorial.sccm.domain.TotalProd;
import cl.tutorial.sccm.repository.TotalProdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing TotalProd.
 */
@Service
@Transactional
public class TotalProdServiceImpl implements TotalProdService{

    private final Logger log = LoggerFactory.getLogger(TotalProdServiceImpl.class);

    private final TotalProdRepository totalProdRepository;

    public TotalProdServiceImpl(TotalProdRepository totalProdRepository) {
        this.totalProdRepository = totalProdRepository;
    }

    /**
     * Save a totalProd.
     *
     * @param totalProd the entity to save
     * @return the persisted entity
     */
    @Override
    public TotalProd save(TotalProd totalProd) {
        log.debug("Request to save TotalProd : {}", totalProd);
        return totalProdRepository.save(totalProd);
    }

    /**
     *  Get all the totalProds.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TotalProd> findAll() {
        log.debug("Request to get all TotalProds");
        return totalProdRepository.findAll();
    }

    /**
     *  Get one totalProd by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TotalProd findOne(Long id) {
        log.debug("Request to get TotalProd : {}", id);
        return totalProdRepository.findOne(id);
    }

    /**
     *  Delete the  totalProd by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TotalProd : {}", id);
        totalProdRepository.delete(id);
    }
}
