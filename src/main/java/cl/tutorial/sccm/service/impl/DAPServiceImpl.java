package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.DAP;
import cl.tutorial.sccm.repository.AnnoTributaRepository;
import cl.tutorial.sccm.repository.CustomDapRepository;
import cl.tutorial.sccm.repository.DAPRepository;
import cl.tutorial.sccm.service.DAPService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Service Implementation for managing DAP.
 */
@Service
@Transactional
public class DAPServiceImpl implements DAPService {

    private final Logger log = LoggerFactory.getLogger(DAPServiceImpl.class);

    private final DAPRepository         dAPRepository;
    private final AnnoTributaRepository annoTributaRepository;
    private final CustomDapRepository   customDapRepository;

    public DAPServiceImpl(DAPRepository dAPRepository, AnnoTributaRepository annoTributaRepository, CustomDapRepository customDapRepository) {
        this.dAPRepository = dAPRepository;
        this.annoTributaRepository = annoTributaRepository;
        this.customDapRepository = customDapRepository;
    }

    /**
     * Save a dAP.
     *
     * @param dAP
     * 		the entity to save
     *
     * @return the persisted entity
     */
    @Override
    public DAP save(DAP dAP) {
        log.debug("Request to save DAP : {}", dAP);
        return dAPRepository.save(dAP);
    }

    /**
     * Get all the dAPS.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DAP> findAll() {
        log.debug("Request to get all DAPS");
        return dAPRepository.findAll();
    }

    /**
     * Get one dAP by id.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DAP findOne(Long id) {
        log.debug("Request to get DAP : {}", id);
        return dAPRepository.findOne(id);
    }

    /**
     * Delete the  dAP by id.
     *
     * @param id
     * 		the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DAP : {}", id);
        dAPRepository.delete(id);
    }

    @Override
    public Page<DAP> findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(Long idAnno, String alarmUniqueCode, Pageable pageable) {
        Page<DAP> page;
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        final AnnoTributa annoTributa = annoTributaRepository.findOne(idAnno);
        List<DAP> otherList;
        switch (queryOption) {
            case Constants.ALARMA_DAP_RENOVACIONES:
                // WHERE (((DAP.TIPO_PLAZO)="i"));
                // page = dAPRepository.findAllByAnnoTributaAndTipPlazo(annoTributa, Constants.TIPO_PLAZO_INDEFINIDO, pageable);
                otherList = dAPRepository.findByAnnoTributaIdAndTipPlazoGroupByFolioBiggerThanOne(annoTributa.getId(), Constants.TIPO_PLAZO_INDEFINIDO);
                List<DAP> finalList = new ArrayList<>();
                filterRenovations(otherList, finalList);
                // Paginando manualmente
                //                int start = pageable.getOffset();
                //                int end = (start + pageable.getPageSize()) > finalList.size() ? finalList.size() : (start + pageable.getPageSize());
                //                page = new PageImpl<>(finalList.subList(start, end), pageable, finalList.size());
                page = new PageImpl<>(finalList);
                break;
            case Constants.ALARMA_DAP_PAGOS_ANTICIPADOS:
                // WHERE (((DAP.IND_PAGO)="A"));
                page = dAPRepository.findAllByAnnoTributaAndIndPago(annoTributa, Constants.INDICADOR_PAGO_ANTICIPADO, pageable);
                break;
            case Constants.ALARMA_DAP_REAJUSTES_NEGATIVOS:
                // WHERE (((DAP.REAJ_PAGADO)<0)) ORDER BY DAP.FECHA_CONTABLE;
                page = dAPRepository.findAllByAnnoTributaAndReajPagadoLessThanOrderByFecCont(annoTributa, BigInteger.ZERO, pageable);
                break;
            case Constants.ALARMA_DAP_EXCLUIR_CLIENTES_57_BIS:
                // WHERE (((DAP.[57BIS])="S"));
                page = dAPRepository.findAllByAnnoTributaAndP57Bis(annoTributa, Constants.INDICADOR_EXCLUIR_CLIENTES_57_BIS, pageable);
                break;
            case Constants.ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS:
                // WHERE (((DAP.IND_PAGO)="A") AND (([FECHA_PAGO]-[FECHA_INV])<=7)); -> Se hace el filtro manual... no en BD
                List<DAP> list = dAPRepository.findAllByAnnoTributaAndIndPago(annoTributa, Constants.INDICADOR_PAGO_ANTICIPADO);
                // List<DAP> filteredList = list.stream().filter((dap) -> (dap.getId().longValue() == new Long("3837498").longValue())).collect(Collectors.toList());
                List<DAP> filteredList =
                    list.stream().filter((dap) -> (DAYS.between(dap.getFechaPago(), dap.getFechaInv()) <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue()))
                        .collect(Collectors.toList());
                page = new PageImpl<>(filteredList);
                /*
                Calendar calendar = new GregorianCalendar();
                // calendar.setTimeZone(TimeZone.getTimeZone("UTC+1"));// Munich time
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, -Constants.INDICADOR_DIAS_DIFERENCIA); // substract the number of days to look back
                Date dateToLookBackAfter = calendar.getTime();
                final LocalDate localDate = Instant.ofEpochMilli(dateToLookBackAfter.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
                //page = dAPRepository.findAllBy(annoTributa.getId(), localDate, Constants.INDICADOR_PAGO_ANTICIPADO, pageable);
                page = new PageImpl<>(customDapRepository
                    .findAllByTaxYearIdAndDiffDaysAndPaymentIndicator(annoTributa.getId(), Constants.INDICADOR_DIAS_DIFERENCIA, Constants.INDICADOR_PAGO_ANTICIPADO));*/
                break;
            case Constants.ALARMA_OPCION_NULL:
                page = dAPRepository.findAllByAnnoTributa(annoTributa, pageable);
                break;
            default:
                page = new PageImpl<>(new ArrayList());
                break;
        }
        return page;
    }

    private void filterRenovations(List<DAP> dataList, List<DAP> finalList) 
	{
        IntStream.range(0, dataList.size() - 1).forEach(i -> 
		{
            if ((i + 1) < (dataList.size() - 1)) 
			{
                if ( Objects.equals(dataList.get(i).getFolio(), dataList.get(i + 1).getFolio()) && 
					(!Objects.equals(dataList.get(i).getFechaPago(), dataList.get(i + 1).getFechaInv()) || 
					 !Objects.equals(dataList.get(i).getMonFi(), dataList.get(i + 1).getCapital())) ) 
				{
                    finalList.add(dataList.get(i));
                } else if (!Objects.equals(dataList.get(i).getFolio(), dataList.get(i + 1).getFolio())) {
                    // En caso que sea el último elemento de un folio X ¿qué se hace?
                    // finalList.add(otherList.get(i));
                }
            }
        });
    }

    @Override
    public List<DAP> findByAllByTaxYearIdAndAlertTypeUniqueCode(Long idAnno, String alarmUniqueCode) {
        List<DAP> list;
        List<DAP> otherList;
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        final AnnoTributa annoTributa = annoTributaRepository.findOne(idAnno);
        switch (queryOption) {
            case Constants.ALARMA_DAP_RENOVACIONES:
                // WHERE (((DAP.TIPO_PLAZO)="i"));
                otherList = dAPRepository.findByAnnoTributaIdAndTipPlazoGroupByFolioBiggerThanOne(annoTributa.getId(), Constants.TIPO_PLAZO_INDEFINIDO);
                List<DAP> finalList = new ArrayList<>();
                filterRenovations(otherList, finalList);
                list = finalList;
                break;
            //list.stream().filter((dap -> {dap.})); break;
            case Constants.ALARMA_DAP_PAGOS_ANTICIPADOS:
                // WHERE (((DAP.IND_PAGO)="A"));
                list = dAPRepository.findAllByAnnoTributaAndIndPago(annoTributa, Constants.INDICADOR_PAGO_ANTICIPADO);
                break;
            case Constants.ALARMA_DAP_REAJUSTES_NEGATIVOS:
                // WHERE (((DAP.REAJ_PAGADO)<0)) ORDER BY DAP.FECHA_CONTABLE;
                list = dAPRepository.findAllByAnnoTributaAndReajPagadoLessThanOrderByFecCont(annoTributa, BigInteger.ZERO);
                break;
            case Constants.ALARMA_DAP_EXCLUIR_CLIENTES_57_BIS:
                // WHERE (((DAP.[57BIS])="S"));
                list = dAPRepository.findAllByAnnoTributaAndP57Bis(annoTributa, Constants.INDICADOR_EXCLUIR_CLIENTES_57_BIS);
                break;
            case Constants.ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS:
                // WHERE (((DAP.IND_PAGO)="A") AND (([FECHA_PAGO]-[FECHA_INV])<=7)); -> Se hace el filtro manual... no en BD
                list = dAPRepository.findAllByAnnoTributaAndIndPago(annoTributa, Constants.INDICADOR_PAGO_ANTICIPADO);
                list = list.stream().filter(dap -> DAYS.between(dap.getFechaInv(), dap.getFechaPago()) <= Constants.INDICADOR_DIAS_DIFERENCIA_LONG.longValue())
                    .collect(Collectors.toList());
                // list = dAPRepository.findAllBy(annoTributa.getId(), Constants.INDICADOR_DIAS_DIFERENCIA, Constants.INDICADOR_PAGO_ANTICIPADO);
                break;
            case Constants.ALARMA_OPCION_NULL:
                list = dAPRepository.findAllByAnnoTributa(annoTributa);
                break;
            default:
                list = new ArrayList<>();
                break;
        }
        return list;
    }
}
