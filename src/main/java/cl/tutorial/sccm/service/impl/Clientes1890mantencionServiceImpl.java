package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Clientes1890mantencionService;
import cl.tutorial.sccm.domain.Clientes1890mantencion;
import cl.tutorial.sccm.repository.Clientes1890mantencionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Clientes1890mantencion.
 */
@Service
@Transactional
public class Clientes1890mantencionServiceImpl implements Clientes1890mantencionService{

    private final Logger log = LoggerFactory.getLogger(Clientes1890mantencionServiceImpl.class);

    private final Clientes1890mantencionRepository clientes1890mantencionRepository;

    public Clientes1890mantencionServiceImpl(Clientes1890mantencionRepository clientes1890mantencionRepository) {
        this.clientes1890mantencionRepository = clientes1890mantencionRepository;
    }

    /**
     * Save a clientes1890mantencion.
     *
     * @param clientes1890mantencion the entity to save
     * @return the persisted entity
     */
    @Override
    public Clientes1890mantencion save(Clientes1890mantencion clientes1890mantencion) {
        log.debug("Request to save Clientes1890mantencion : {}", clientes1890mantencion);
        return clientes1890mantencionRepository.save(clientes1890mantencion);
    }

    /**
     *  Get all the clientes1890mantencions.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Clientes1890mantencion> findAll() {
        log.debug("Request to get all Clientes1890mantencions");
        return clientes1890mantencionRepository.findAll();
    }

    /**
     *  Get one clientes1890mantencion by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Clientes1890mantencion findOne(Long id) {
        log.debug("Request to get Clientes1890mantencion : {}", id);
        return clientes1890mantencionRepository.findOne(id);
    }

    /**
     *  Delete the  clientes1890mantencion by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Clientes1890mantencion : {}", id);
        clientes1890mantencionRepository.delete(id);
    }
}
