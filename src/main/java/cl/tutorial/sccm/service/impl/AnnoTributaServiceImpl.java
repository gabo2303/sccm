package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.repository.AnnoTributaRepository;
import cl.tutorial.sccm.service.AnnoTributaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import javax.persistence.*;

import java.math.BigDecimal;

/**
 * Service Implementation for managing AnnoTributa.
 */
@Service
@Transactional
public class AnnoTributaServiceImpl implements AnnoTributaService 
{
    private final Logger log = LoggerFactory.getLogger(AnnoTributaServiceImpl.class);

    private final AnnoTributaRepository annoTributaRepository;
	
	@Autowired
    private EntityManager em;
	
    public AnnoTributaServiceImpl(AnnoTributaRepository annoTributaRepository) 
	{ 
		this.annoTributaRepository = annoTributaRepository; 
	}

    /**
     * Save a annoTributa.
     *
     * @param annoTributa
     * 		the entity to save
     *
     * @return the persisted entity
     */
    @Override
    public AnnoTributa save(AnnoTributa annoTributa) 
	{
        log.debug("Request to save AnnoTributa : {}", annoTributa);
		
		Query query = em.createNativeQuery(  "update anno_tributa set anno_activo = ?, anno_c = ?, anno_t = ? where id = ?");  
		
		query.setParameter(1, annoTributa.getAnnoActivo());  
		query.setParameter(2, annoTributa.getAnnoC());  
		query.setParameter(3, annoTributa.getAnnoT());  
		query.setParameter(4, annoTributa.getId());  
		
		query.executeUpdate();
		
        return annoTributaRepository.save(annoTributa);
    }

    /**
     * Get all the annoTributas.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AnnoTributa> findAllByIdNotEqual(Long id) 
	{
        log.debug("Request to get all AnnoTributas");
        return annoTributaRepository.findAllByIdNotEqual(id);
    }
	
    @Override
    public List<AnnoTributa> findAllByAnnoActivo(String annoActivo) 
	{
        log.debug("Request to get all AnnoTributas activos");
        return annoTributaRepository.findAllByAnnoActivo(annoActivo);
    }

    /**
     * Get one annoTributa by id.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AnnoTributa findOne(Long id) {
        log.debug("Request to get AnnoTributa : {}", id);
        return annoTributaRepository.findOne(id);
    }

    /**
     * Delete the  annoTributa by id.
     *
     * @param id
     * 		the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AnnoTributa : {}", id);
        annoTributaRepository.delete(id);
    }
}
