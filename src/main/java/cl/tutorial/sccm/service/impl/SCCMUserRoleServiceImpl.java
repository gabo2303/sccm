package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.SCCMUserRole;
import cl.tutorial.sccm.repository.SCCMUserRoleRepository;
import cl.tutorial.sccm.service.SCCMUserRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing SCCMUserRole.
 */
@Service
@Transactional
public class SCCMUserRoleServiceImpl implements SCCMUserRoleService{

    private final Logger log = LoggerFactory.getLogger(SCCMUserRoleServiceImpl.class);

    private final SCCMUserRoleRepository sCCMUserRoleRepository;

    public SCCMUserRoleServiceImpl(SCCMUserRoleRepository sCCMUserRoleRepository) {
        this.sCCMUserRoleRepository = sCCMUserRoleRepository;
    }

    /**
     * Save a sCCMUserRole.
     *
     * @param sCCMUserRole the entity to save
     * @return the persisted entity
     */
    @Override
    public SCCMUserRole save(SCCMUserRole sCCMUserRole) {
        log.debug("Request to save SCCMUserRole : {}", sCCMUserRole);
        return sCCMUserRoleRepository.save(sCCMUserRole);
    }

    /**
     *  Get all the sCCMUserRoles.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SCCMUserRole> findAll() {
        log.debug("Request to get all SCCMUserRoles");
        return sCCMUserRoleRepository.findAll();
    }

    /**
     *  Get one sCCMUserRole by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SCCMUserRole findOne(Long id) {
        log.debug("Request to get SCCMUserRole : {}", id);
        return sCCMUserRoleRepository.findOne(id);
    }

    /**
     *  Delete the  sCCMUserRole by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SCCMUserRole : {}", id);
        sCCMUserRoleRepository.delete(id);
    }
}
