package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.MonedaService;
import cl.tutorial.sccm.domain.Moneda;
import cl.tutorial.sccm.repository.MonedaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Moneda.
 */
@Service
@Transactional
public class MonedaServiceImpl implements MonedaService{

    private final Logger log = LoggerFactory.getLogger(MonedaServiceImpl.class);

    private final MonedaRepository monedaRepository;

    public MonedaServiceImpl(MonedaRepository monedaRepository) {
        this.monedaRepository = monedaRepository;
    }

    /**
     * Save a moneda.
     *
     * @param moneda the entity to save
     * @return the persisted entity
     */
    @Override
    public Moneda save(Moneda moneda) {
        log.debug("Request to save Moneda : {}", moneda);
        return monedaRepository.save(moneda);
    }

    /**
     *  Get all the monedas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Moneda> findAll() {
        log.debug("Request to get all Monedas");
        return monedaRepository.findAll();
    }

    /**
     *  Get one moneda by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Moneda findOne(Long id) {
        log.debug("Request to get Moneda : {}", id);
        return monedaRepository.findOne(id);
    }

    /**
     *  Delete the  moneda by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Moneda : {}", id);
        monedaRepository.delete(id);
    }
}
