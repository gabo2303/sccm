package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Cert57BService;
import cl.tutorial.sccm.domain.Cert57B;
import cl.tutorial.sccm.repository.Cert57BRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Cert57B.
 */
@Service
@Transactional
public class Cert57BServiceImpl implements Cert57BService{

    private final Logger log = LoggerFactory.getLogger(Cert57BServiceImpl.class);

    private final Cert57BRepository cert57BRepository;

    public Cert57BServiceImpl(Cert57BRepository cert57BRepository) {
        this.cert57BRepository = cert57BRepository;
    }

    /**
     * Save a cert57B.
     *
     * @param cert57B the entity to save
     * @return the persisted entity
     */
    @Override
    public Cert57B save(Cert57B cert57B) {
        log.debug("Request to save Cert57B : {}", cert57B);
        return cert57BRepository.save(cert57B);
    }

    /**
     *  Get all the cert57BS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Cert57B> findAll() {
        log.debug("Request to get all Cert57BS");
        return cert57BRepository.findAll();
    }

    /**
     *  Get one cert57B by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Cert57B findOne(Long id) {
        log.debug("Request to get Cert57B : {}", id);
        return cert57BRepository.findOne(id);
    }

    /**
     *  Delete the  cert57B by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Cert57B : {}", id);
        cert57BRepository.delete(id);
    }

    @Override
    public List<Cert57B> findAllByAnnoTributaId(Long annoId) {
        return  this.cert57BRepository.findAllByAnnoTributaId(annoId);
    }


}
