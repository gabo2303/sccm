package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.EuroService;
import cl.tutorial.sccm.domain.Euro;
import cl.tutorial.sccm.repository.EuroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Euro.
 */
@Service
@Transactional
public class EuroServiceImpl implements EuroService{

    private final Logger log = LoggerFactory.getLogger(EuroServiceImpl.class);

    private final EuroRepository euroRepository;

    public EuroServiceImpl(EuroRepository euroRepository) {
        this.euroRepository = euroRepository;
    }

    /**
     * Save a euro.
     *
     * @param euro the entity to save
     * @return the persisted entity
     */
    @Override
    public Euro save(Euro euro) {
        log.debug("Request to save Euro : {}", euro);
        return euroRepository.save(euro);
    }

    /**
     *  Get all the euros.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Euro> findAll() {
        log.debug("Request to get all Euros");
        return euroRepository.findAll();
    }

    /**
     *  Get one euro by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Euro findOne(Long id) {
        log.debug("Request to get Euro : {}", id);
        return euroRepository.findOne(id);
    }

    /**
     *  Delete the  euro by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Euro : {}", id);
        euroRepository.delete(id);
    }
}
