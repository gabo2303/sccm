package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.domain.Ahorro;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.repository.AhorroRepository;
import cl.tutorial.sccm.repository.AnnoTributaRepository;
import cl.tutorial.sccm.service.AhorroService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Service Implementation for managing Ahorro.
 */
@Service
@Transactional
public class AhorroServiceImpl implements AhorroService {

    private final Logger log = LoggerFactory.getLogger(AhorroServiceImpl.class);

    private final AhorroRepository      ahorroRepository;
    private final AnnoTributaRepository annoTributaRepository;

    public AhorroServiceImpl(AhorroRepository ahorroRepository, AnnoTributaRepository annoTributaRepository) {
        this.ahorroRepository = ahorroRepository;
        this.annoTributaRepository = annoTributaRepository;
    }

    /**
     * Save a ahorro.
     *
     * @param ahorro
     * 		the entity to save
     *
     * @return the persisted entity
     */
    @Override
    public Ahorro save(Ahorro ahorro) {
        log.debug("Request to save Ahorro : {}", ahorro);
        return ahorroRepository.save(ahorro);
    }

    /**
     * Get all the ahorros.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Ahorro> findAll() {
        log.debug("Request to get all Ahorros");
        return ahorroRepository.findAll();
    }

    /**
     * Get one ahorro by id.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Ahorro findOne(Long id) {
        log.debug("Request to get Ahorro : {}", id);
        return ahorroRepository.findOne(id);
    }

    /**
     * Delete the  ahorro by id.
     *
     * @param id
     * 		the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Ahorro : {}", id);
        ahorroRepository.delete(id);
    }

    @Override
    public List<Ahorro> findAllByAnnoTributaId(Long annoId) {
        return this.ahorroRepository.findAllByAnnoTributaId(annoId);
    }

    @Override
    public Page<Ahorro> findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(Long idAnno, String alarmUniqueCode, Pageable pageable) {
        Page<Ahorro> page;
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        final AnnoTributa annoTributa = annoTributaRepository.findOne(idAnno);
        switch (queryOption) {
            case Constants.ALARMA_CAPITAL_E_INTERES_PAGADO_NEGATIVO:
                // WHERE (((AHORRO.CAPITAL)<0)) OR (((AHORRO.INT_PAGADOS)<0));
                page = ahorroRepository.findAllByAnnoTributaAndCapitalLessThanOrIntPagadoLessThan(annoTributa, Constants.DOUBLE_VALUE_ZERO, Constants.DOUBLE_VALUE_ZERO, pageable);
                break;
            case Constants.ALARMA_CLIENTE_EXTRANJERO:
                // WHERE (((AHORRO.IND_NAC_EXT)="S"));
                page = ahorroRepository.findAllByAnnoTributaAndNacionalidad(annoTributa, Constants.NACIONALIDAD_EXTRANJERO, pageable);
                break;
            case Constants.ALARMA_CUENTA_CERRADA:
                // WHERE (((AHORRO.ESTADO_CTA_AHO)="C"));
                page = ahorroRepository.findAllByAnnoTributaAndEstado(annoTributa, Constants.ESTADO_CUENTA_CERRADA, pageable);
                break;
            case Constants.ALARMA_INTERES_REAL_MAYOR_QUE_INTERES_PAGADO:
                // WHERE ((([INT_REAL]-[INT_PAGADOS])<0));
                page = ahorroRepository.findAllBy(annoTributa.getId(), pageable);
                break;
            case Constants.ALARMA_MAS_DE_6_GIROS:
                // WHERE (((AHORRO.NUM_GIROS)=5 Or (AHORRO.NUM_GIROS)=6));
                List<Integer> numGiros = new ArrayList<>();
                numGiros.add(5);
                numGiros.add(6);
                page = ahorroRepository.findAllByAnnoTributaAndNumGiroIn(annoTributa, numGiros, pageable);
                break;
            case Constants.ALARMA_MAS_DE_6_GIROS_SIN_REAJUSTES_NI_INTERESES:
                // WHERE (((AHORRO.NUM_GIROS)>6)) AND REAJ_PAGADO = 0 AND INT_PAG = 0;
                page = ahorroRepository.findAllByAnnoTributaAndNumGiroGreaterThanAndReajPagadoAndIntPagado(annoTributa, Constants.NUMERO_DE_GIROS_6, Constants.DOUBLE_VALUE_ZERO,
                    Constants.DOUBLE_VALUE_ZERO, pageable);
                break;
            case Constants.ALARMA_OPCION_NULL:
                page = ahorroRepository.findAllByAnnoTributa(annoTributa, pageable);
                break;
            default:
                page = new PageImpl(new ArrayList());
                break;
        }
        return page;
    }

    @Override
    public List<Ahorro> findByAllByTaxYearIdAndAlertTypeUniqueCode(Long idAnno, String alarmUniqueCode) {
        List<Ahorro> list;
        String queryOption = (alarmUniqueCode != null && !alarmUniqueCode.isEmpty() ? alarmUniqueCode : Constants.ALARMA_OPCION_NULL);
        final AnnoTributa annoTributa = annoTributaRepository.findOne(idAnno);
        switch (queryOption) {
            case Constants.ALARMA_CAPITAL_E_INTERES_PAGADO_NEGATIVO:
                // WHERE (((AHORRO.CAPITAL)<0)) OR (((AHORRO.INT_PAGADOS)<0));
                list = ahorroRepository.findAllByAnnoTributaAndCapitalLessThanOrIntPagadoLessThan(annoTributa, Constants.DOUBLE_VALUE_ZERO, Constants.DOUBLE_VALUE_ZERO);
                break;
            case Constants.ALARMA_CLIENTE_EXTRANJERO:
                // WHERE (((AHORRO.IND_NAC_EXT)="S"));
                list = ahorroRepository.findAllByAnnoTributaAndNacionalidad(annoTributa, Constants.NACIONALIDAD_EXTRANJERO);
                break;
            case Constants.ALARMA_CUENTA_CERRADA:
                // WHERE (((AHORRO.ESTADO_CTA_AHO)="C"));
                list = ahorroRepository.findAllByAnnoTributaAndEstado(annoTributa, Constants.ESTADO_CUENTA_CERRADA);
                break;
            case Constants.ALARMA_INTERES_REAL_MAYOR_QUE_INTERES_PAGADO:
                // WHERE ((([INT_REAL]-[INT_PAGADOS])<0));
                list = ahorroRepository.findAllBy(annoTributa.getId());
                break;
            case Constants.ALARMA_MAS_DE_6_GIROS:
                // WHERE (((AHORRO.NUM_GIROS)=5 Or (AHORRO.NUM_GIROS)=6));
                List<Integer> numGiros = new ArrayList<>();
                numGiros.add(5);
                numGiros.add(6);
                list = ahorroRepository.findAllByAnnoTributaAndNumGiroIn(annoTributa, numGiros);
                break;
            case Constants.ALARMA_MAS_DE_6_GIROS_SIN_REAJUSTES_NI_INTERESES:
                // WHERE (((AHORRO.NUM_GIROS)>6)) AND REAJ_PAGADO = 0 AND INT_PAG = 0;
                list = ahorroRepository.findAllByAnnoTributaAndNumGiroGreaterThanAndReajPagadoAndIntPagado(annoTributa, Constants.NUMERO_DE_GIROS_6, Constants.DOUBLE_VALUE_ZERO,
                    Constants.DOUBLE_VALUE_ZERO);
                break;
            case Constants.ALARMA_OPCION_NULL:
                list = ahorroRepository.findAllByAnnoTributa(annoTributa);
                break;
            default:
                list = new ArrayList<>();
                break;
        }
        return list;
    }
}
