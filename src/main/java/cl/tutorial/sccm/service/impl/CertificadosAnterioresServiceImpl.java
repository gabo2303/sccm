package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.CertificadosAnterioresService;
import cl.tutorial.sccm.domain.CertificadosAnteriores;
import cl.tutorial.sccm.repository.CertificadosAnterioresRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CertificadosAnteriores.
 */
@Service
@Transactional
public class CertificadosAnterioresServiceImpl implements CertificadosAnterioresService{

    private final Logger log = LoggerFactory.getLogger(CertificadosAnterioresServiceImpl.class);

    private final CertificadosAnterioresRepository certificadosAnterioresRepository;

    public CertificadosAnterioresServiceImpl(CertificadosAnterioresRepository certificadosAnterioresRepository) {
        this.certificadosAnterioresRepository = certificadosAnterioresRepository;
    }

    /**
     * Save a certificadosAnteriores.
     *
     * @param certificadosAnteriores the entity to save
     * @return the persisted entity
     */
    @Override
    public CertificadosAnteriores save(CertificadosAnteriores certificadosAnteriores) {
        log.debug("Request to save CertificadosAnteriores : {}", certificadosAnteriores);
        return certificadosAnterioresRepository.save(certificadosAnteriores);
    }

    /**
     *  Get all the certificadosAnteriores.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CertificadosAnteriores> findAll() {
        log.debug("Request to get all CertificadosAnteriores");
        return certificadosAnterioresRepository.findAll();
    }

    /**
     *  Get one certificadosAnteriores by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CertificadosAnteriores findOne(Long id) {
        log.debug("Request to get CertificadosAnteriores : {}", id);
        return certificadosAnterioresRepository.findOne(id);
    }

    /**
     *  Delete the  certificadosAnteriores by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CertificadosAnteriores : {}", id);
        certificadosAnterioresRepository.delete(id);
    }
}
