package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Cart57BService;
import cl.tutorial.sccm.domain.Cart57B;
import cl.tutorial.sccm.repository.Cart57BRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Cart57B.
 */
@Service
@Transactional
public class Cart57BServiceImpl implements Cart57BService{

    private final Logger log = LoggerFactory.getLogger(Cart57BServiceImpl.class);

    private final Cart57BRepository cart57BRepository;

    public Cart57BServiceImpl(Cart57BRepository cart57BRepository) {
        this.cart57BRepository = cart57BRepository;
    }

    /**
     * Save a cart57B.
     *
     * @param cart57B the entity to save
     * @return the persisted entity
     */
    @Override
    public Cart57B save(Cart57B cart57B) {
        log.debug("Request to save Cart57B : {}", cart57B);
        return cart57BRepository.save(cart57B);
    }

    /**
     *  Get all the cart57BS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Cart57B> findAll() {
        log.debug("Request to get all Cart57BS");
        return cart57BRepository.findAll();
    }

    /**
     *  Get one cart57B by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Cart57B findOne(Long id) {
        log.debug("Request to get Cart57B : {}", id);
        return cart57BRepository.findOne(id);
    }

    /**
     *  Delete the  cart57B by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Cart57B : {}", id);
        cart57BRepository.delete(id);
    }

    @Override
    public List<Cart57B> findAllByAnnoTributaId(Long annoId) {
        return  this.cart57BRepository.findAllByAnnoTributaId(annoId);
    }

}
