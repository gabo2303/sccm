package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.SalProxPerService;
import cl.tutorial.sccm.domain.SalProxPer;
import cl.tutorial.sccm.repository.SalProxPerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing SalProxPer.
 */
@Service
@Transactional
public class SalProxPerServiceImpl implements SalProxPerService{

    private final Logger log = LoggerFactory.getLogger(SalProxPerServiceImpl.class);

    private final SalProxPerRepository salProxPerRepository;

    public SalProxPerServiceImpl(SalProxPerRepository salProxPerRepository) {
        this.salProxPerRepository = salProxPerRepository;
    }

    /**
     * Save a salProxPer.
     *
     * @param salProxPer the entity to save
     * @return the persisted entity
     */
    @Override
    public SalProxPer save(SalProxPer salProxPer) {
        log.debug("Request to save SalProxPer : {}", salProxPer);
        return salProxPerRepository.save(salProxPer);
    }

    /**
     *  Get all the salProxPers.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SalProxPer> findAll() {
        log.debug("Request to get all SalProxPers");
        return salProxPerRepository.findAll();
    }

    /**
     *  Get one salProxPer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SalProxPer findOne(Long id) {
        log.debug("Request to get SalProxPer : {}", id);
        return salProxPerRepository.findOne(id);
    }

    /**
     *  Delete the  salProxPer by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SalProxPer : {}", id);
        salProxPerRepository.delete(id);
    }
}
