package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.CorrecMonetaService;
import cl.tutorial.sccm.domain.CorrecMoneta;
import cl.tutorial.sccm.repository.CorrecMonetaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CorrecMoneta.
 */
@Service
@Transactional
public class CorrecMonetaServiceImpl implements CorrecMonetaService{

    private final Logger log = LoggerFactory.getLogger(CorrecMonetaServiceImpl.class);

    private final CorrecMonetaRepository correcMonetaRepository;

    public CorrecMonetaServiceImpl(CorrecMonetaRepository correcMonetaRepository) {
        this.correcMonetaRepository = correcMonetaRepository;
    }

    /**
     * Save a correcMoneta.
     *
     * @param correcMoneta the entity to save
     * @return the persisted entity
     */
    @Override
    public CorrecMoneta save(CorrecMoneta correcMoneta) {
        log.debug("Request to save CorrecMoneta : {}", correcMoneta);
        return correcMonetaRepository.save(correcMoneta);
    }

    /**
     *  Get all the correcMonetas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CorrecMoneta> findAll() {
        log.debug("Request to get all CorrecMonetas");
        return correcMonetaRepository.findAll();
    }

    /**
     *  Get one correcMoneta by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CorrecMoneta findOne(Long id) {
        log.debug("Request to get CorrecMoneta : {}", id);
        return correcMonetaRepository.findOne(id);
    }

    /**
     *  Delete the  correcMoneta by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CorrecMoneta : {}", id);
        correcMonetaRepository.delete(id);
    }
}
