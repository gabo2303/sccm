package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.CabeceraControlCambioService;
import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.repository.CabeceraControlCambioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CabeceraControlCambio.
 */
@Service
@Transactional
public class CabeceraControlCambioServiceImpl implements CabeceraControlCambioService{

    private final Logger log = LoggerFactory.getLogger(CabeceraControlCambioServiceImpl.class);

    private final CabeceraControlCambioRepository cabeceraControlCambioRepository;

    public CabeceraControlCambioServiceImpl(CabeceraControlCambioRepository cabeceraControlCambioRepository) {
        this.cabeceraControlCambioRepository = cabeceraControlCambioRepository;
    }

    /**
     * Save a cabeceraControlCambio.
     *
     * @param cabeceraControlCambio the entity to save
     * @return the persisted entity
     */
    @Override
    public CabeceraControlCambio save(CabeceraControlCambio cabeceraControlCambio) {
        log.debug("Request to save CabeceraControlCambio : {}", cabeceraControlCambio);
        return cabeceraControlCambioRepository.save(cabeceraControlCambio);
    }

    /**
     *  Get all the cabeceraControlCambios.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CabeceraControlCambio> findAll() {
        log.debug("Request to get all CabeceraControlCambios");
        return cabeceraControlCambioRepository.findAll();
    }

    /**
     *  Get one cabeceraControlCambio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CabeceraControlCambio findOne(Long id) {
        log.debug("Request to get CabeceraControlCambio : {}", id);
        return cabeceraControlCambioRepository.findOne(id);
    }

    /**
     *  Delete the  cabeceraControlCambio by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CabeceraControlCambio : {}", id);
        cabeceraControlCambioRepository.delete(id);
    }
}
