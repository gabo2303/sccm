package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.TablaMonedaService;
import cl.tutorial.sccm.domain.TablaMoneda;
import cl.tutorial.sccm.repository.TablaMonedaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing TablaMoneda.
 */
@Service
@Transactional
public class TablaMonedaServiceImpl implements TablaMonedaService{

    private final Logger log = LoggerFactory.getLogger(TablaMonedaServiceImpl.class);

    private final TablaMonedaRepository tablaMonedaRepository;

    public TablaMonedaServiceImpl(TablaMonedaRepository tablaMonedaRepository) {
        this.tablaMonedaRepository = tablaMonedaRepository;
    }

    /**
     * Save a tablaMoneda.
     *
     * @param tablaMoneda the entity to save
     * @return the persisted entity
     */
    @Override
    public TablaMoneda save(TablaMoneda tablaMoneda) {
        log.debug("Request to save TablaMoneda : {}", tablaMoneda);
        return tablaMonedaRepository.save(tablaMoneda);
    }

    /**
     *  Get all the tablaMonedas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TablaMoneda> findAll() {
        log.debug("Request to get all TablaMonedas");
        return tablaMonedaRepository.findAll();
    }

    /**
     *  Get one tablaMoneda by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TablaMoneda findOne(Long id) {
        log.debug("Request to get TablaMoneda : {}", id);
        return tablaMonedaRepository.findOne(id);
    }

    /**
     *  Delete the  tablaMoneda by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TablaMoneda : {}", id);
        tablaMonedaRepository.delete(id);
    }
}
