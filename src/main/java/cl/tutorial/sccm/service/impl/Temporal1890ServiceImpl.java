package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Temporal1890Service;
import cl.tutorial.sccm.domain.Temporal1890;
import cl.tutorial.sccm.repository.Temporal1890Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Temporal1890.
 */
@Service
@Transactional
public class Temporal1890ServiceImpl implements Temporal1890Service{

    private final Logger log = LoggerFactory.getLogger(Temporal1890ServiceImpl.class);

    private final Temporal1890Repository temporal1890Repository;

    public Temporal1890ServiceImpl(Temporal1890Repository temporal1890Repository) {
        this.temporal1890Repository = temporal1890Repository;
    }

    /**
     * Save a temporal1890.
     *
     * @param temporal1890 the entity to save
     * @return the persisted entity
     */
    @Override
    public Temporal1890 save(Temporal1890 temporal1890) {
        log.debug("Request to save Temporal1890 : {}", temporal1890);
        return temporal1890Repository.save(temporal1890);
    }

    /**
     *  Get all the temporal1890S.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Temporal1890> findAll() {
        log.debug("Request to get all Temporal1890S");
        return temporal1890Repository.findAll();
    }

    /**
     *  Get one temporal1890 by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Temporal1890 findOne(Long id) {
        log.debug("Request to get Temporal1890 : {}", id);
        return temporal1890Repository.findOne(id);
    }

    /**
     *  Delete the  temporal1890 by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Temporal1890 : {}", id);
        temporal1890Repository.delete(id);
    }
}
