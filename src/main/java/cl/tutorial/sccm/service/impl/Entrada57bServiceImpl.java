package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Entrada57bService;
import cl.tutorial.sccm.domain.Entrada57b;
import cl.tutorial.sccm.repository.Entrada57bRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.lang.reflect.Array;
import java.util.List;

/**
 * Service Implementation for managing Entrada57b.
 */
@Service
@Transactional
public class Entrada57bServiceImpl implements Entrada57bService {

    @Autowired
    private EntityManager em;

    private final Logger log = LoggerFactory.getLogger(Entrada57bServiceImpl.class);

    private final Entrada57bRepository entrada57bRepository;

    public Entrada57bServiceImpl(Entrada57bRepository entrada57bRepository) {
        this.entrada57bRepository = entrada57bRepository;
    }

    /**
     * Save a entrada57b.
     *
     * @param entrada57b the entity to save
     * @return the persisted entity
     */
    @Override
    public Entrada57b save(Entrada57b entrada57b) {
        log.debug("Request to save Entrada57b : {}", entrada57b);
        return entrada57bRepository.save(entrada57b);
    }

    /**
     * Get all the entrada57bs.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Entrada57b> findAll() {
        log.debug("Request to get all Entrada57bs");
        return entrada57bRepository.findAll();
    }

    /**
     * Get one entrada57b by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Entrada57b findOne(Long id) {
        log.debug("Request to get Entrada57b : {}", id);
        return entrada57bRepository.findOne(id);
    }

    /**
     * Delete the entrada57b by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Entrada57b : {}", id);
        entrada57bRepository.delete(id);
    }

    @Override
    public Integer executeSp1InsertaEntrada57B() {
        try {
        Integer flag =0;
        String query = "SP_1_INSERTA_ENTRADA_57_B";
        StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
        storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("at_id", Integer.class, ParameterMode.OUT);
        storedProcedureQuery.execute();
        Integer annoId = (Integer)storedProcedureQuery.getOutputParameterValue("at_id");
        if ((Integer)storedProcedureQuery.getOutputParameterValue("STATUS") == 1){
            flag++;
            if(this.executeSp2InsertaMovIniCartola(annoId)==1){
                flag++;
                if(this.executeSp3InsertaMovtosCartola(annoId)==1){
                    flag++;
                    if(this.executeSp4InsertaMovCert57B(annoId)==1){
                        flag++;
                        if(this.executeSp5InsertaSalProxPer(annoId)==1){
                            flag++;
                            if(this.executeSp6InsertaSalIni57B(annoId)==1){
                                flag++;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("flag: " + flag);
        return flag;
    }catch (Exception ex){
        System.out.println("Excepcion : " + ex.getMessage());
        return 0;
    }

}

    @Override
    public Integer executeSp2InsertaMovIniCartola(Integer annoId) {
        String query = "SP_2_INSERTA_MOV_INI_CARTOLA";
        StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
        storedProcedureQuery.registerStoredProcedureParameter("ATID",  Integer.class , ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("ATIDO", Integer.class, ParameterMode.OUT);
        storedProcedureQuery.setParameter("ATID",annoId);
        storedProcedureQuery.execute();
        System.out.println( storedProcedureQuery.getOutputParameterValue("STATUS"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_CODE"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_ERRM"));
        return (Integer) storedProcedureQuery.getOutputParameterValue("STATUS");
    }

    @Override
    public Integer executeSp3InsertaMovtosCartola(Integer annoId) {
        String query = "SP_3_INSERTA_MOVTOS_CARTOLA";
        StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
        storedProcedureQuery.registerStoredProcedureParameter("ATID",  Integer.class , ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("ATIDO", Integer.class, ParameterMode.OUT);
        storedProcedureQuery.setParameter("ATID",annoId);
        storedProcedureQuery.execute();
        System.out.println( storedProcedureQuery.getOutputParameterValue("STATUS"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_CODE"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_ERRM"));
        return (Integer) storedProcedureQuery.getOutputParameterValue("STATUS");
    }

    @Override
    public Integer executeSp4InsertaMovCert57B(Integer annoId) {
        String query = "SP_4_INSERTA_MOV_CERT_57B";
        StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
        storedProcedureQuery.registerStoredProcedureParameter("ATID",  Integer.class , ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("ATIDO", Integer.class, ParameterMode.OUT);
        storedProcedureQuery.setParameter("ATID",annoId);
        storedProcedureQuery.execute();
        System.out.println( storedProcedureQuery.getOutputParameterValue("STATUS"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_CODE"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_ERRM"));
        return (Integer) storedProcedureQuery.getOutputParameterValue("STATUS");
    }

    @Override
    public Integer executeSp5InsertaSalProxPer(Integer annoId) {
        String query = "SP_5_INSERTA_SAL_PROX_PER";
        StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
        storedProcedureQuery.registerStoredProcedureParameter("ATID",  Integer.class , ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("ATIDO", Integer.class, ParameterMode.OUT);
        storedProcedureQuery.setParameter("ATID",annoId);
        storedProcedureQuery.execute();
        System.out.println( storedProcedureQuery.getOutputParameterValue("STATUS"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_CODE"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_ERRM"));
        return (Integer) storedProcedureQuery.getOutputParameterValue("STATUS");
    }

    @Override
    public Integer executeSp6InsertaSalIni57B(Integer annoId) {
        String query = "SP_6_INSERTA_SAL_INI_57_B";
        StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
        storedProcedureQuery.registerStoredProcedureParameter("ATID",  Integer.class , ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("ATIDO", Integer.class, ParameterMode.OUT);
        storedProcedureQuery.setParameter("ATID",annoId);
        storedProcedureQuery.execute();
        System.out.println( storedProcedureQuery.getOutputParameterValue("STATUS"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_CODE"));
        System.out.println( storedProcedureQuery.getOutputParameterValue("V_ERRM"));
        return (Integer) storedProcedureQuery.getOutputParameterValue("STATUS");
    }

    @Override
    public Integer reprocessE57B(Integer annoId) {
        try {
            Integer flag =0;
            String query = "SP_3_INSERTA_MOVTOS_CARTOLA";
            StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
            storedProcedureQuery.registerStoredProcedureParameter("ATID",  Integer.class , ParameterMode.IN);
            storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
            storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
            storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
            storedProcedureQuery.registerStoredProcedureParameter("ATIDO", Integer.class, ParameterMode.OUT);
            storedProcedureQuery.setParameter("ATID",annoId);
            storedProcedureQuery.execute();
            System.out.println( storedProcedureQuery.getOutputParameterValue("STATUS"));
            System.out.println( storedProcedureQuery.getOutputParameterValue("V_CODE"));
            System.out.println( storedProcedureQuery.getOutputParameterValue("V_ERRM"));
            if ((Integer)storedProcedureQuery.getOutputParameterValue("STATUS") == 1){
                flag++;
                if(this.executeSp4InsertaMovCert57B(annoId)==1){
                    flag++;
                    if(this.executeSp5InsertaSalProxPer(annoId)==1){
                        flag++;
                        if(this.executeSp6InsertaSalIni57B(annoId)==1){
                            flag++;
                        }
                    }
                }
            }

            System.out.println("flag: " + flag);
            return flag;
        }catch (Exception ex){
            System.out.println("Excepcion : " + ex.getMessage());
            return 0;
        }


    }

    @Override
    public List<Entrada57b> findAllByAnnoTributaId(Long annoId) {
        return  this.entrada57bRepository.findAllByAnnoTributaId(annoId);
    }


}
