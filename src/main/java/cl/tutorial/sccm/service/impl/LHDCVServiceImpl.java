package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.LHDCVService;
import cl.tutorial.sccm.domain.LHDCV;
import cl.tutorial.sccm.repository.LHDCVRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing LHDCV.
 */
@Service
@Transactional
public class LHDCVServiceImpl implements LHDCVService{

    private final Logger log = LoggerFactory.getLogger(LHDCVServiceImpl.class);

    private final LHDCVRepository lHDCVRepository;

    public LHDCVServiceImpl(LHDCVRepository lHDCVRepository) {
        this.lHDCVRepository = lHDCVRepository;
    }

    /**
     * Save a lHDCV.
     *
     * @param lHDCV the entity to save
     * @return the persisted entity
     */
    @Override
    public LHDCV save(LHDCV lHDCV) {
        log.debug("Request to save LHDCV : {}", lHDCV);
        return lHDCVRepository.save(lHDCV);
    }

    /**
     *  Get all the lHDCVS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LHDCV> findAll() {
        log.debug("Request to get all LHDCVS");
        return lHDCVRepository.findAll();
    }

    /**
     *  Get one lHDCV by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LHDCV findOne(Long id) {
        log.debug("Request to get LHDCV : {}", id);
        return lHDCVRepository.findOne(id);
    }

    /**
     *  Delete the  lHDCV by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LHDCV : {}", id);
        lHDCVRepository.delete(id);
    }
}
