package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.UsdService;
import cl.tutorial.sccm.domain.Usd;
import cl.tutorial.sccm.repository.UsdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Usd.
 */
@Service
@Transactional
public class UsdServiceImpl implements UsdService{

    private final Logger log = LoggerFactory.getLogger(UsdServiceImpl.class);

    private final UsdRepository usdRepository;

    public UsdServiceImpl(UsdRepository usdRepository) {
        this.usdRepository = usdRepository;
    }

    /**
     * Save a usd.
     *
     * @param usd the entity to save
     * @return the persisted entity
     */
    @Override
    public Usd save(Usd usd) {
        log.debug("Request to save Usd : {}", usd);
        return usdRepository.save(usd);
    }

    /**
     *  Get all the usds.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Usd> findAll() {
        log.debug("Request to get all Usds");
        return usdRepository.findAll();
    }

    /**
     *  Get one usd by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Usd findOne(Long id) {
        log.debug("Request to get Usd : {}", id);
        return usdRepository.findOne(id);
    }

    /**
     *  Delete the  usd by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Usd : {}", id);
        usdRepository.delete(id);
    }
}
