package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.PactosService;
import cl.tutorial.sccm.domain.Pactos;
import cl.tutorial.sccm.repository.PactosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Pactos.
 */
@Service
@Transactional
public class PactosServiceImpl implements PactosService{

    private final Logger log = LoggerFactory.getLogger(PactosServiceImpl.class);

    private final PactosRepository pactosRepository;

    public PactosServiceImpl(PactosRepository pactosRepository) {
        this.pactosRepository = pactosRepository;
    }

    /**
     * Save a pactos.
     *
     * @param pactos the entity to save
     * @return the persisted entity
     */
    @Override
    public Pactos save(Pactos pactos) {
        log.debug("Request to save Pactos : {}", pactos);
        return pactosRepository.save(pactos);
    }

    /**
     *  Get all the pactos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Pactos> findAll() {
        log.debug("Request to get all Pactos");
        return pactosRepository.findAll();
    }

    /**
     *  Get one pactos by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Pactos findOne(Long id) {
        log.debug("Request to get Pactos : {}", id);
        return pactosRepository.findOne(id);
    }

    /**
     *  Delete the  pactos by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pactos : {}", id);
        pactosRepository.delete(id);
    }


    @Override
    public List<Pactos> findAllByAnnoTributaId(Long annoId) {
        return this.pactosRepository.findAllByAnnoTributaId(annoId);
    }
}
