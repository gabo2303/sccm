package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.AhorroCC;
import cl.tutorial.sccm.repository.AhorroCCRepository;
import cl.tutorial.sccm.service.AhorroCCService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Pactos.
 */
@Service
@Transactional
public class AhorroCCServiceImpl implements AhorroCCService {

    private final AhorroCCRepository ahorroCCRepository;

    public AhorroCCServiceImpl(AhorroCCRepository ahorroCCRepository) {
        this.ahorroCCRepository = ahorroCCRepository;
    }

    @Override
    public List<AhorroCC> findAllByAnnoTributa(Long annoId) {
        return this.ahorroCCRepository.findAllByAnnoTributa(annoId);
    }
}
