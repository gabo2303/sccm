package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.RepresentanteService;
import cl.tutorial.sccm.domain.Representante;
import cl.tutorial.sccm.repository.RepresentanteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Representante.
 */
@Service
@Transactional
public class RepresentanteServiceImpl implements RepresentanteService{

    private final Logger log = LoggerFactory.getLogger(RepresentanteServiceImpl.class);

    private final RepresentanteRepository representanteRepository;

    public RepresentanteServiceImpl(RepresentanteRepository representanteRepository) {
        this.representanteRepository = representanteRepository;
    }

    /**
     * Save a representante.
     *
     * @param representante the entity to save
     * @return the persisted entity
     */
    @Override
    public Representante save(Representante representante) {
        log.debug("Request to save Representante : {}", representante);
        return representanteRepository.save(representante);
    }

    /**
     *  Get all the representantes.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Representante> findAll() {
        log.debug("Request to get all Representantes");
        return representanteRepository.findAll();
    }

    /**
     *  Get one representante by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Representante findOne(Long id) {
        log.debug("Request to get Representante : {}", id);
        return representanteRepository.findOne(id);
    }

    /**
     *  Delete the  representante by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Representante : {}", id);
        representanteRepository.delete(id);
    }

    @Override
    public List<Representante> findAllByAnnoTributaIdAndInstrumentoId(Long annoId, Long instId) {
        log.debug("Request to findAllByAnnoTributaIdAndInstrumentoId Glosa : {}", annoId, instId);
        return representanteRepository.findAllByAnnoTributaIdAndInstrumentoId(annoId,instId);
    }
}
