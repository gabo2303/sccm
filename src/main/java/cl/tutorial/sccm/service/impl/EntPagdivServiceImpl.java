package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.EntPagdivService;
import cl.tutorial.sccm.domain.EntPagdiv;
import cl.tutorial.sccm.repository.EntPagdivRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing EntPagdiv.
 */
@Service
@Transactional
public class EntPagdivServiceImpl implements EntPagdivService{

    private final Logger log = LoggerFactory.getLogger(EntPagdivServiceImpl.class);

    private final EntPagdivRepository entPagdivRepository;

    public EntPagdivServiceImpl(EntPagdivRepository entPagdivRepository) {
        this.entPagdivRepository = entPagdivRepository;
    }

    /**
     * Save a entPagdiv.
     *
     * @param entPagdiv the entity to save
     * @return the persisted entity
     */
    @Override
    public EntPagdiv save(EntPagdiv entPagdiv) {
        log.debug("Request to save EntPagdiv : {}", entPagdiv);
        return entPagdivRepository.save(entPagdiv);
    }

    /**
     *  Get all the entPagdivs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<EntPagdiv> findAll() {
        log.debug("Request to get all EntPagdivs");
        return entPagdivRepository.findAll();
    }

    /**
     *  Get one entPagdiv by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EntPagdiv findOne(Long id) {
        log.debug("Request to get EntPagdiv : {}", id);
        return entPagdivRepository.findOne(id);
    }

    /**
     *  Delete the  entPagdiv by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EntPagdiv : {}", id);
        entPagdivRepository.delete(id);
    }

    @Override
    public List<EntPagdiv> findAllByAnnoTributaId(Long annoId) {
        return entPagdivRepository.findAllByAnnoTributaId(annoId);
    }
}
