package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Cliente1890Service;
import cl.tutorial.sccm.domain.Cliente1890;
import cl.tutorial.sccm.repository.Cliente1890Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Cliente1890.
 */
@Service
@Transactional
public class Cliente1890ServiceImpl implements Cliente1890Service
{
    private final Logger log = LoggerFactory.getLogger(Cliente1890ServiceImpl.class);

    private final Cliente1890Repository cliente1890Repository;

    public Cliente1890ServiceImpl(Cliente1890Repository cliente1890Repository) { this.cliente1890Repository = cliente1890Repository; }

    /**
     * Save a cliente1890.
     *
     * @param cliente1890 the entity to save
     * @return the persisted entity
     */
    @Override
    public Cliente1890 save(Cliente1890 cliente1890) 
	{
        log.debug("Request to save Cliente1890 : {}", cliente1890);
        
		return cliente1890Repository.save(cliente1890);
    }

    /**
     *  Get all the cliente1890S.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Cliente1890> findAll() 
	{
        log.debug("ENTRA AL REQUEST QUE DEBERIA DEVOLVER TODOS LOS CLIENTES 1890");
        log.debug("CAMBIE LA PORQUERIA");
		return cliente1890Repository.findAll();
    }

    /**
     *  Get one cliente1890 by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Cliente1890 findOne(Long id) 
	{
        log.debug("Request to get Cliente1890 : {}", id);
        
		return cliente1890Repository.findOne(id);
    }

    /**
     *  Delete the  cliente1890 by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) 
	{
        log.debug("Request to delete Cliente1890 : {}", id);
        
		cliente1890Repository.delete(id);
    }
}