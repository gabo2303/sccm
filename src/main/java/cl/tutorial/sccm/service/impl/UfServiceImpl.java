package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.UfService;
import cl.tutorial.sccm.domain.Uf;
import cl.tutorial.sccm.repository.UfRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Uf.
 */
@Service
@Transactional
public class UfServiceImpl implements UfService{

    private final Logger log = LoggerFactory.getLogger(UfServiceImpl.class);

    private final UfRepository ufRepository;

    public UfServiceImpl(UfRepository ufRepository) {
        this.ufRepository = ufRepository;
    }

    /**
     * Save a uf.
     *
     * @param uf the entity to save
     * @return the persisted entity
     */
    @Override
    public Uf save(Uf uf) {
        log.debug("Request to save Uf : {}", uf);
        return ufRepository.save(uf);
    }

    /**
     *  Get all the ufs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Uf> findAll() {
        log.debug("Request to get all Ufs");
        return ufRepository.findAll();
    }

    /**
     *  Get one uf by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Uf findOne(Long id) {
        log.debug("Request to get Uf : {}", id);
        return ufRepository.findOne(id);
    }

    /**
     *  Delete the  uf by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Uf : {}", id);
        ufRepository.delete(id);
    }
}
