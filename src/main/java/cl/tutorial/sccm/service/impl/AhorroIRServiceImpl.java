package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.AhorroIR;
import cl.tutorial.sccm.domain.PactosIR;
import cl.tutorial.sccm.repository.AhorroIRRepository;
import cl.tutorial.sccm.repository.PactosIRRepository;
import cl.tutorial.sccm.service.AhorroIRService;
import cl.tutorial.sccm.service.AhorroService;
import cl.tutorial.sccm.service.PactosIRService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

/**
 * Service Implementation for managing Pactos.
 */
@Service
@Transactional
public class AhorroIRServiceImpl implements AhorroIRService {

    private final AhorroIRRepository ahorroIRRepository;

    public AhorroIRServiceImpl(AhorroIRRepository ahorroIRRepository) {
        this.ahorroIRRepository = ahorroIRRepository;
    }

    @Override
    public List<AhorroIR> findAllByAnnoTributa(BigInteger annoId) {
        return this.ahorroIRRepository.findAllByAnnoTributa(annoId);
    }
}
