package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.DirectoriosService;
import cl.tutorial.sccm.domain.Directorios;
import cl.tutorial.sccm.repository.DirectoriosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Directorios.
 */
@Service
@Transactional
public class DirectoriosServiceImpl implements DirectoriosService{

    private final Logger log = LoggerFactory.getLogger(DirectoriosServiceImpl.class);

    private final DirectoriosRepository directoriosRepository;

    public DirectoriosServiceImpl(DirectoriosRepository directoriosRepository) {
        this.directoriosRepository = directoriosRepository;
    }

    /**
     * Save a directorios.
     *
     * @param directorios the entity to save
     * @return the persisted entity
     */
    @Override
    public Directorios save(Directorios directorios) {
        log.debug("Request to save Directorios : {}", directorios);
        return directoriosRepository.save(directorios);
    }

    /**
     *  Get all the directorios.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Directorios> findAll(Pageable pageable) {
        log.debug("Request to get all Directorios");
        return directoriosRepository.findAll(pageable);
    }

    /**
     *  Get one directorios by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Directorios findOne(Long id) {
        log.debug("Request to get Directorios : {}", id);
        return directoriosRepository.findOne(id);
    }

    /**
     *  Delete the  directorios by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Directorios : {}", id);
        directoriosRepository.delete(id);
    }
}
