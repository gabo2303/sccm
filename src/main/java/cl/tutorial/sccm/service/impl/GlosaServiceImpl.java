package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.GlosaService;
import cl.tutorial.sccm.domain.Glosa;
import cl.tutorial.sccm.repository.GlosaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Glosa.
 */
@Service
@Transactional
public class GlosaServiceImpl implements GlosaService{

    private final Logger log = LoggerFactory.getLogger(GlosaServiceImpl.class);

    private final GlosaRepository glosaRepository;

    public GlosaServiceImpl(GlosaRepository glosaRepository) {
        this.glosaRepository = glosaRepository;
    }

    /**
     * Save a glosa.
     *
     * @param glosa the entity to save
     * @return the persisted entity
     */
    @Override
    public Glosa save(Glosa glosa) {
        log.debug("Request to save Glosa : {}", glosa);
        return glosaRepository.save(glosa);
    }

    /**
     *  Get all the glosas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Glosa> findAll() {
        log.debug("Request to get all Glosas");
        return glosaRepository.findAll();
    }

    /**
     *  Get one glosa by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Glosa findOne(Long id) {
        log.debug("Request to get Glosa : {}", id);
        return glosaRepository.findOne(id);
    }

    /**
     *  Delete the  glosa by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Glosa : {}", id);
        glosaRepository.delete(id);
    }

    @Override
    public List<Glosa> findAllByAnnoTributaIdAndInstrumentoId(Long annoId, Long instId) {
        log.debug("Request to findAllByAnnoTributaIdAndInstrumentoId Glosa : {}", annoId, instId);
        return glosaRepository.findAllByAnnoTributaIdAndInstrumentoId(annoId,instId);
    }


}
