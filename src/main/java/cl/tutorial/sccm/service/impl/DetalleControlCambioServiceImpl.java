package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.DetalleControlCambioService;
import cl.tutorial.sccm.domain.DetalleControlCambio;
import cl.tutorial.sccm.repository.DetalleControlCambioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing DetalleControlCambio.
 */
@Service
@Transactional
public class DetalleControlCambioServiceImpl implements DetalleControlCambioService{

    private final Logger log = LoggerFactory.getLogger(DetalleControlCambioServiceImpl.class);

    private final DetalleControlCambioRepository detalleControlCambioRepository;

    public DetalleControlCambioServiceImpl(DetalleControlCambioRepository detalleControlCambioRepository) {
        this.detalleControlCambioRepository = detalleControlCambioRepository;
    }

    /**
     * Save a detalleControlCambio.
     *
     * @param detalleControlCambio the entity to save
     * @return the persisted entity
     */
    @Override
    public DetalleControlCambio save(DetalleControlCambio detalleControlCambio) {
        log.debug("Request to save DetalleControlCambio : {}", detalleControlCambio);
        return detalleControlCambioRepository.save(detalleControlCambio);
    }

    /**
     *  Get all the detalleControlCambios.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DetalleControlCambio> findAll() {
        log.debug("Request to get all DetalleControlCambios");
        return detalleControlCambioRepository.findAll();
    }

    /**
     *  Get one detalleControlCambio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DetalleControlCambio findOne(Long id) {
        log.debug("Request to get DetalleControlCambio : {}", id);
        return detalleControlCambioRepository.findOne(id);
    }

    /**
     *  Delete the  detalleControlCambio by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DetalleControlCambio : {}", id);
        detalleControlCambioRepository.delete(id);
    }
}
