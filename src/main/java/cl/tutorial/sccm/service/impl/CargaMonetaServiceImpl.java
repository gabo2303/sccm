package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.CargaMonetaService;
import cl.tutorial.sccm.domain.CargaMoneta;
import cl.tutorial.sccm.repository.CargaMonetaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CargaMoneta.
 */
@Service
@Transactional
public class CargaMonetaServiceImpl implements CargaMonetaService{

    private final Logger log = LoggerFactory.getLogger(CargaMonetaServiceImpl.class);

    private final CargaMonetaRepository cargaMonetaRepository;

    public CargaMonetaServiceImpl(CargaMonetaRepository cargaMonetaRepository) {
        this.cargaMonetaRepository = cargaMonetaRepository;
    }

    /**
     * Save a cargaMoneta.
     *
     * @param cargaMoneta the entity to save
     * @return the persisted entity
     */
    @Override
    public CargaMoneta save(CargaMoneta cargaMoneta) {
        log.debug("Request to save CargaMoneta : {}", cargaMoneta);
        return cargaMonetaRepository.save(cargaMoneta);
    }

    /**
     *  Get all the cargaMonetas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CargaMoneta> findAll() {
        log.debug("Request to get all CargaMonetas");
        return cargaMonetaRepository.findAll();
    }

    /**
     *  Get one cargaMoneta by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CargaMoneta findOne(Long id) {
        log.debug("Request to get CargaMoneta : {}", id);
        return cargaMonetaRepository.findOne(id);
    }

    /**
     *  Delete the  cargaMoneta by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CargaMoneta : {}", id);
        cargaMonetaRepository.delete(id);
    }
}
