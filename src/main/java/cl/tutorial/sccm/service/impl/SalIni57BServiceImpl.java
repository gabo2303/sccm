package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.SalIni57BService;
import cl.tutorial.sccm.domain.SalIni57B;
import cl.tutorial.sccm.repository.SalIni57BRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing SalIni57B.
 */
@Service
@Transactional
public class SalIni57BServiceImpl implements SalIni57BService{

    private final Logger log = LoggerFactory.getLogger(SalIni57BServiceImpl.class);

    private final SalIni57BRepository salIni57BRepository;

    public SalIni57BServiceImpl(SalIni57BRepository salIni57BRepository) {
        this.salIni57BRepository = salIni57BRepository;
    }

    /**
     * Save a salIni57B.
     *
     * @param salIni57B the entity to save
     * @return the persisted entity
     */
    @Override
    public SalIni57B save(SalIni57B salIni57B) {
        log.debug("Request to save SalIni57B : {}", salIni57B);
        return salIni57BRepository.save(salIni57B);
    }

    /**
     *  Get all the salIni57BS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SalIni57B> findAll() {
        log.debug("Request to get all SalIni57BS");
        return salIni57BRepository.findAll();
    }

    /**
     *  Get one salIni57B by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SalIni57B findOne(Long id) {
        log.debug("Request to get SalIni57B : {}", id);
        return salIni57BRepository.findOne(id);
    }

    /**
     *  Delete the  salIni57B by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SalIni57B : {}", id);
        salIni57BRepository.delete(id);
    }
}
