package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.CertVersionService;
import cl.tutorial.sccm.domain.CertVersion;
import cl.tutorial.sccm.repository.CertVersionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CertVersion.
 */
@Service
@Transactional
public class CertVersionServiceImpl implements CertVersionService{

    private final Logger log = LoggerFactory.getLogger(CertVersionServiceImpl.class);

    private final CertVersionRepository certVersionRepository;

    public CertVersionServiceImpl(CertVersionRepository certVersionRepository) {
        this.certVersionRepository = certVersionRepository;
    }

    /**
     * Save a certVersion.
     *
     * @param certVersion the entity to save
     * @return the persisted entity
     */
    @Override
    public CertVersion save(CertVersion certVersion) {
        log.debug("Request to save CertVersion : {}", certVersion);
        return certVersionRepository.save(certVersion);
    }

    /**
     *  Get all the certVersions.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CertVersion> findAll() {
        log.debug("Request to get all CertVersions");
        return certVersionRepository.findAll();
    }

    /**
     *  Get one certVersion by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CertVersion findOne(Long id) {
        log.debug("Request to get CertVersion : {}", id);
        return certVersionRepository.findOne(id);
    }

    /**
     *  Delete the  certVersion by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CertVersion : {}", id);
        certVersionRepository.delete(id);
    }
}
