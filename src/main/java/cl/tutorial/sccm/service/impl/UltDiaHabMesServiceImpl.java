package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.UltDiaHabMesService;
import cl.tutorial.sccm.domain.UltDiaHabMes;
import cl.tutorial.sccm.repository.UltDiaHabMesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing UltDiaHabMes.
 */
@Service
@Transactional
public class UltDiaHabMesServiceImpl implements UltDiaHabMesService
{
    private final Logger log = LoggerFactory.getLogger(UltDiaHabMesServiceImpl.class);

    private final UltDiaHabMesRepository ultDiaHabMesRepository;

    public UltDiaHabMesServiceImpl(UltDiaHabMesRepository ultDiaHabMesRepository) 
	{
        this.ultDiaHabMesRepository = ultDiaHabMesRepository;
    }

    /**
     * Save a ultDiaHabMes.
     *
     * @param ultDiaHabMes the entity to save
     * @return the persisted entity
     */
    @Override
    public UltDiaHabMes save(UltDiaHabMes ultDiaHabMes) 
	{
        log.debug("Request to save UltDiaHabMes : {}", ultDiaHabMes);
        
		return ultDiaHabMesRepository.save(ultDiaHabMes);
    }

    /**
     *  Get all the ultDiaHabMes.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<UltDiaHabMes> findAll() 
	{
        log.debug("Request to get all UltDiaHabMes");
        
		return ultDiaHabMesRepository.findAll();
    }

    /**
     *  Get one ultDiaHabMes by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UltDiaHabMes findOne(Long id) 
	{
        log.debug("Request to get UltDiaHabMes : {}", id);
        
		return ultDiaHabMesRepository.findOne(id);
    }

    /**
     *  Delete the  ultDiaHabMes by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) 
	{
        log.debug("Request to delete UltDiaHabMes : {}", id);
        
		ultDiaHabMesRepository.delete(id);
    }
}