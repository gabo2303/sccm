package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Certi57BFccService;
import cl.tutorial.sccm.domain.Certi57BFcc;
import cl.tutorial.sccm.repository.Certi57BFccRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Certi57BFcc.
 */
@Service
@Transactional
public class Certi57BFccServiceImpl implements Certi57BFccService{

    private final Logger log = LoggerFactory.getLogger(Certi57BFccServiceImpl.class);

    private final Certi57BFccRepository certi57BFccRepository;

    public Certi57BFccServiceImpl(Certi57BFccRepository certi57BFccRepository) {
        this.certi57BFccRepository = certi57BFccRepository;
    }

    /**
     * Save a certi57BFcc.
     *
     * @param certi57BFcc the entity to save
     * @return the persisted entity
     */
    @Override
    public Certi57BFcc save(Certi57BFcc certi57BFcc) {
        log.debug("Request to save Certi57BFcc : {}", certi57BFcc);
        return certi57BFccRepository.save(certi57BFcc);
    }

    /**
     *  Get all the certi57BFccs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Certi57BFcc> findAll() {
        log.debug("Request to get all Certi57BFccs");
        return certi57BFccRepository.findAll();
    }

    /**
     *  Get one certi57BFcc by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Certi57BFcc findOne(Long id) {
        log.debug("Request to get Certi57BFcc : {}", id);
        return certi57BFccRepository.findOne(id);
    }

    /**
     *  Delete the  certi57BFcc by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Certi57BFcc : {}", id);
        certi57BFccRepository.delete(id);
    }
}
