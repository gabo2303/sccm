package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.ClienteBiceService;
import cl.tutorial.sccm.domain.ClienteBice;
import cl.tutorial.sccm.repository.ClienteBiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing ClienteBice.
 */
@Service
@Transactional
public class ClienteBiceServiceImpl implements ClienteBiceService{

    private final Logger log = LoggerFactory.getLogger(ClienteBiceServiceImpl.class);

    private final ClienteBiceRepository clienteBiceRepository;

    public ClienteBiceServiceImpl(ClienteBiceRepository clienteBiceRepository) {
        this.clienteBiceRepository = clienteBiceRepository;
    }

    /**
     * Save a clienteBice.
     *
     * @param clienteBice the entity to save
     * @return the persisted entity
     */
    @Override
    public ClienteBice save(ClienteBice clienteBice) {
        log.debug("Request to save ClienteBice : {}", clienteBice);
        return clienteBiceRepository.save(clienteBice);
    }

    /**
     *  Get all the clienteBices.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ClienteBice> findAll() {
        log.debug("Request to get all ClienteBices");
        return clienteBiceRepository.findAll();
    }

    /**
     *  Get one clienteBice by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ClienteBice findOne(Long id) {
        log.debug("Request to get ClienteBice : {}", id);
        return clienteBiceRepository.findOne(id);
    }

    /**
     *  Delete the  clienteBice by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClienteBice : {}", id);
        clienteBiceRepository.delete(id);
    }
}
