package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.Rut_tempService;
import cl.tutorial.sccm.domain.Rut_temp;
import cl.tutorial.sccm.repository.Rut_tempRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Rut_temp.
 */
@Service
@Transactional
public class Rut_tempServiceImpl implements Rut_tempService{

    private final Logger log = LoggerFactory.getLogger(Rut_tempServiceImpl.class);

    private final Rut_tempRepository rut_tempRepository;

    public Rut_tempServiceImpl(Rut_tempRepository rut_tempRepository) {
        this.rut_tempRepository = rut_tempRepository;
    }

    /**
     * Save a rut_temp.
     *
     * @param rut_temp the entity to save
     * @return the persisted entity
     */
    @Override
    public Rut_temp save(Rut_temp rut_temp) {
        log.debug("Request to save Rut_temp : {}", rut_temp);
        return rut_tempRepository.save(rut_temp);
    }

    /**
     *  Get all the rut_temps.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Rut_temp> findAll() {
        log.debug("Request to get all Rut_temps");
        return rut_tempRepository.findAll();
    }

    /**
     *  Get one rut_temp by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Rut_temp findOne(Long id) {
        log.debug("Request to get Rut_temp : {}", id);
        return rut_tempRepository.findOne(id);
    }

    /**
     *  Delete the  rut_temp by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Rut_temp : {}", id);
        rut_tempRepository.delete(id);
    }
}
