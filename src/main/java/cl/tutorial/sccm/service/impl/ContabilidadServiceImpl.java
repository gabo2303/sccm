package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.ContabilidadService;
import cl.tutorial.sccm.domain.Contabilidad;
import cl.tutorial.sccm.repository.ContabilidadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Contabilidad.
 */
@Service
@Transactional
public class ContabilidadServiceImpl implements ContabilidadService{

    private final Logger log = LoggerFactory.getLogger(ContabilidadServiceImpl.class);

    private final ContabilidadRepository contabilidadRepository;

    public ContabilidadServiceImpl(ContabilidadRepository contabilidadRepository) {
        this.contabilidadRepository = contabilidadRepository;
    }

    /**
     * Save a contabilidad.
     *
     * @param contabilidad the entity to save
     * @return the persisted entity
     */
    @Override
    public Contabilidad save(Contabilidad contabilidad) {
        log.debug("Request to save Contabilidad : {}", contabilidad);
        return contabilidadRepository.save(contabilidad);
    }

    /**
     *  Get all the contabilidads.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Contabilidad> findAll() {
        log.debug("Request to get all Contabilidads");
        return contabilidadRepository.findAll();
    }

    /**
     *  Get one contabilidad by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Contabilidad findOne(Long id) {
        log.debug("Request to get Contabilidad : {}", id);
        return contabilidadRepository.findOne(id);
    }

    /**
     *  Delete the  contabilidad by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Contabilidad : {}", id);
        contabilidadRepository.delete(id);
    }
}
