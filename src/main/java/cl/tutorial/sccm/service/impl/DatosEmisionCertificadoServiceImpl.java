package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.repository.*;
import cl.tutorial.sccm.service.DatosEmisionCertificadoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DatosEmisionCertificadoServiceImpl implements DatosEmisionCertificadoService {

    private final Logger log = LoggerFactory.getLogger(DatosEmisionCertificadoServiceImpl.class);

    private final PactosRepository  pactosRepository;
    private final AhorroRepository  ahorroRepository;
    private final DAPRepository     dapRepository;
    private final BonosRepository   bonosRepository;
    private final LHBonosRepository lhBonosRepository;
    private final LHDCVRepository   lhdcvRepository;

    public DatosEmisionCertificadoServiceImpl(PactosRepository pactosRepository, AhorroRepository ahorroRepository, DAPRepository dapRepository, BonosRepository bonosRepository,
        LHBonosRepository lhBonosRepository, LHDCVRepository lhdcvRepository) {
        this.pactosRepository = pactosRepository;
        this.ahorroRepository = ahorroRepository;
        this.dapRepository = dapRepository;
        this.bonosRepository = bonosRepository;
        this.lhBonosRepository = lhBonosRepository;
        this.lhdcvRepository = lhdcvRepository;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public Page<Object> findAllByTaxYearIdRutDataType(Long taxYearId, Integer dataType, BigInteger rut, String folio) {
        Page<Object> page;
        List<Object> list;
        final boolean twoFilters = rut != null && folio != null && !folio.isEmpty();
        final boolean oneFilter = rut != null && (folio == null || folio.isEmpty());
        switch (dataType) {
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS: // Pactos
                if (twoFilters) {
                    list = pactosRepository.findAllByAnnoTributaIdAndRutAndFolio(taxYearId, rut, folio);
                } else if (oneFilter) {
                    list = pactosRepository.findAllByAnnoTributaIdAndRut(taxYearId, rut);
                } else {
                    list = pactosRepository.findAllByAnnoTributaIdAndFolio(taxYearId, folio);
                }
                break;
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_AHORRO: // Ahorro
                if (twoFilters) {
                    list = ahorroRepository.findAllByAnnoTributaIdAndRutAndFolio(taxYearId, rut, folio);
                } else if (oneFilter) {
                    list = ahorroRepository.findAllByAnnoTributaIdAndRut(taxYearId, rut);
                } else {
                    list = ahorroRepository.findAllByAnnoTributaIdAndFolio(taxYearId, folio);
                }
                break;
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_DAP: // Depósito a Plazo
                if (twoFilters) {
                    list = dapRepository.findAllByAnnoTributaIdAndRutAndFolio(taxYearId, rut, folio);
                } else if (oneFilter) {
                    list = dapRepository.findAllByAnnoTributaIdAndRut(taxYearId, rut);
                } else {
                    list = dapRepository.findAllByAnnoTributaIdAndFolio(taxYearId, folio);
                }
                break;
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS: // Bonos -> Tabla - Bonos
                if (twoFilters) {
                    list = bonosRepository.findAllByAnnoTributaIdAndRutAndFolio(taxYearId, rut, folio);
                } else if (oneFilter) {
                    list = bonosRepository.findAllByAnnoTributaIdAndRut(taxYearId, rut);
                } else {
                    list = bonosRepository.findAllByAnnoTributaIdAndFolio(taxYearId, folio);
                }
                break;
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI: // GPI -> Tabla - LHBonos
                if (twoFilters) {
                    list = lhBonosRepository.findAllByAnnoTributaIdAndRutAndFolio(taxYearId, rut, folio);
                } else if (oneFilter) {
                    list = lhBonosRepository.findAllByAnnoTributaIdAndRut(taxYearId, rut);
                } else {
                    list = lhBonosRepository.findAllByAnnoTributaIdAndFolio(taxYearId, folio);
                }
                break;
            case Constants.SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM: // DCV ASICOM -> Tabla - LHDCV
                if (twoFilters) {
                    list = lhdcvRepository.findAllByAnnoTributaIdAndRutAndFolio(taxYearId, rut, folio);
                } else if (oneFilter) {
                    list = lhdcvRepository.findAllByAnnoTributaIdAndRut(taxYearId, rut);
                } else {
                    list = lhdcvRepository.findAllByAnnoTributaIdAndFolio(taxYearId, folio);
                }
                break;
            default:
                list = new ArrayList();
                break;
        }
        page = new PageImpl<>(list);
        return page;
    }
}
