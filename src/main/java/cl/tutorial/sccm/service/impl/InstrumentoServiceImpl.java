package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.InstrumentoService;
import cl.tutorial.sccm.domain.Instrumento;
import cl.tutorial.sccm.repository.InstrumentoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Instrumento.
 */
@Service
@Transactional
public class InstrumentoServiceImpl implements InstrumentoService{

    private final Logger log = LoggerFactory.getLogger(InstrumentoServiceImpl.class);

    private final InstrumentoRepository instrumentoRepository;

    public InstrumentoServiceImpl(InstrumentoRepository instrumentoRepository) {
        this.instrumentoRepository = instrumentoRepository;
    }

    /**
     * Save a instrumento.
     *
     * @param instrumento the entity to save
     * @return the persisted entity
     */
    @Override
    public Instrumento save(Instrumento instrumento) {
        log.debug("Request to save Instrumento : {}", instrumento);
        return instrumentoRepository.save(instrumento);
    }

    /**
     *  Get all the instrumentos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Instrumento> findAll() {
        log.debug("Request to get all Instrumentos");
        return instrumentoRepository.findAll();
    }

    /**
     *  Get one instrumento by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Instrumento findOne(Long id) {
        log.debug("Request to get Instrumento : {}", id);
        return instrumentoRepository.findOne(id);
    }

    /**
     *  Delete the  instrumento by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Instrumento : {}", id);
        instrumentoRepository.delete(id);
    }
}
