package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.service.CertPagdivService;
import cl.tutorial.sccm.domain.CertPagdiv;
import cl.tutorial.sccm.repository.CertPagdivRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

/**
 * Service Implementation for managing CertPagdiv.
 */
@Service
@Transactional
public class CertPagdivServiceImpl implements CertPagdivService{

    private final Logger log = LoggerFactory.getLogger(CertPagdivServiceImpl.class);

    private final CertPagdivRepository certPagdivRepository;

    public CertPagdivServiceImpl(CertPagdivRepository certPagdivRepository) {
        this.certPagdivRepository = certPagdivRepository;
    }

    @Autowired
    private EntityManager em;
    /**
     * Save a certPagdiv.
     *
     * @param certPagdiv the entity to save
     * @return the persisted entity
     */
    @Override
    public CertPagdiv save(CertPagdiv certPagdiv) {
        log.debug("Request to save CertPagdiv : {}", certPagdiv);
        return certPagdivRepository.save(certPagdiv);
    }

    /**
     *  Get all the certPagdivs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CertPagdiv> findAll() {
        log.debug("Request to get all CertPagdivs");
        return certPagdivRepository.findAll();
    }

    /**
     *  Get one certPagdiv by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CertPagdiv findOne(Long id) {
        log.debug("Request to get CertPagdiv : {}", id);
        return certPagdivRepository.findOne(id);
    }

    /**
     *  Delete the  certPagdiv by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CertPagdiv : {}", id);
        certPagdivRepository.delete(id);
    }

    @Override
    public Integer executeSpLoadCertPagDiv() {
        try {
            Integer flag =0;
            String query = " SP_LOAD_CERT_PAG_DIV";
            StoredProcedureQuery storedProcedureQuery = this.em.createStoredProcedureQuery(query);
            storedProcedureQuery.registerStoredProcedureParameter("STATUS",  Integer.class , ParameterMode.OUT);
            storedProcedureQuery.registerStoredProcedureParameter("V_CODE", String.class, ParameterMode.OUT);
            storedProcedureQuery.registerStoredProcedureParameter("V_ERRM", String.class, ParameterMode.OUT);
            storedProcedureQuery.registerStoredProcedureParameter("at_id", Integer.class, ParameterMode.OUT);
            storedProcedureQuery.execute();
            if ((Integer)storedProcedureQuery.getOutputParameterValue("STATUS") == 1){
                flag++;
            }
            System.out.println("flag: " + flag);
            return flag;
        }catch (Exception ex){
            System.out.println("Excepcion : " + ex.getMessage());
            return 0;
        }
    }

    @Override
    public List<CertPagdiv> findAllByAnnoTributaId(Long annoId) {
        return certPagdivRepository.findAllByAnnoTributaId(annoId);
    }
}
