package cl.tutorial.sccm.service.impl;

import cl.tutorial.sccm.domain.PactosCC;
import cl.tutorial.sccm.domain.PactosIR;
import cl.tutorial.sccm.repository.PactosCCRepository;
import cl.tutorial.sccm.repository.PactosIRRepository;
import cl.tutorial.sccm.service.PactosCCService;
import cl.tutorial.sccm.service.PactosIRService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Pactos.
 */
@Service
@Transactional
public class PactosCCServiceImpl implements PactosCCService{

    private final PactosCCRepository pactosCCRepository;

    public PactosCCServiceImpl(PactosCCRepository pactosCCRepository) {
        this.pactosCCRepository = pactosCCRepository;
    }


    @Override
    public List<PactosCC> findAllByAnnoTributa(Long annoId) {
        return this.pactosCCRepository.findAllByAnnoTributa(annoId);
    }
}
