package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.ClienteBice;
import java.util.List;

/**
 * Service Interface for managing ClienteBice.
 */
public interface ClienteBiceService {

    /**
     * Save a clienteBice.
     *
     * @param clienteBice the entity to save
     * @return the persisted entity
     */
    ClienteBice save(ClienteBice clienteBice);

    /**
     *  Get all the clienteBices.
     *
     *  @return the list of entities
     */
    List<ClienteBice> findAll();

    /**
     *  Get the "id" clienteBice.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ClienteBice findOne(Long id);

    /**
     *  Delete the "id" clienteBice.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
