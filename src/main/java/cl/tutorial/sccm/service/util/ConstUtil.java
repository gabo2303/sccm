package cl.tutorial.sccm.service.util;

public class ConstUtil {

    public static final Integer LARGO_LINEA_INTERFACE = 96;
    public static final String DIRECTORIO_UPLOAD_INTERFACE = "interface\\upload";
    public static final String DIRECTORIO_TEMP_INTERFACE = "interface\\temp";
    public static final String BICE_INTERFAZ_1941 = "bice1941.dta";
    public static final String ETL_CAMINO_INTERFAZ_1941 = "../../interface/temp/";

}
