package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Bonos;

import java.util.List;

/**
 * Service Interface for managing Bonos.
 */
public interface BonosService {

    /**
     * Save a bonos.
     *
     * @param bonos
     * 		the entity to save
     *
     * @return the persisted entity
     */
    Bonos save(Bonos bonos);

    /**
     * Get all the bonos.
     *
     * @return the list of entities
     */
    List<Bonos> findAll();

    /**
     * Get the "id" bonos.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    Bonos findOne(Long id);

    /**
     * Delete the "id" bonos.
     *
     * @param id
     * 		the id of the entity
     */
    void delete(Long id);
}
