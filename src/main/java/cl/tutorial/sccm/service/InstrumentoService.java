package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Instrumento;
import java.util.List;

/**
 * Service Interface for managing Instrumento.
 */
public interface InstrumentoService {

    /**
     * Save a instrumento.
     *
     * @param instrumento the entity to save
     * @return the persisted entity
     */
    Instrumento save(Instrumento instrumento);

    /**
     *  Get all the instrumentos.
     *
     *  @return the list of entities
     */
    List<Instrumento> findAll();

    /**
     *  Get the "id" instrumento.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Instrumento findOne(Long id);

    /**
     *  Delete the "id" instrumento.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
