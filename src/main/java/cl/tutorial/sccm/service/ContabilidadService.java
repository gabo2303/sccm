package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Contabilidad;
import java.util.List;

/**
 * Service Interface for managing Contabilidad.
 */
public interface ContabilidadService {

    /**
     * Save a contabilidad.
     *
     * @param contabilidad the entity to save
     * @return the persisted entity
     */
    Contabilidad save(Contabilidad contabilidad);

    /**
     *  Get all the contabilidads.
     *
     *  @return the list of entities
     */
    List<Contabilidad> findAll();

    /**
     *  Get the "id" contabilidad.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Contabilidad findOne(Long id);

    /**
     *  Delete the "id" contabilidad.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
