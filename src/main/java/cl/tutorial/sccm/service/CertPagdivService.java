package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Cert57B;
import cl.tutorial.sccm.domain.CertPagdiv;
import java.util.List;

/**
 * Service Interface for managing CertPagdiv.
 */
public interface CertPagdivService {

    /**
     * Save a certPagdiv.
     *
     * @param certPagdiv the entity to save
     * @return the persisted entity
     */
    CertPagdiv save(CertPagdiv certPagdiv);

    /**
     *  Get all the certPagdivs.
     *
     *  @return the list of entities
     */
    List<CertPagdiv> findAll();

    /**
     *  Get the "id" certPagdiv.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CertPagdiv findOne(Long id);

    /**
     *  Delete the "id" certPagdiv.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    Integer executeSpLoadCertPagDiv();

    List<CertPagdiv> findAllByAnnoTributaId(Long annoId);
}
