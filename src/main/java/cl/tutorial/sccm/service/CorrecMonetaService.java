package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.CorrecMoneta;
import java.util.List;

/**
 * Service Interface for managing CorrecMoneta.
 */
public interface CorrecMonetaService {

    /**
     * Save a correcMoneta.
     *
     * @param correcMoneta the entity to save
     * @return the persisted entity
     */
    CorrecMoneta save(CorrecMoneta correcMoneta);

    /**
     *  Get all the correcMonetas.
     *
     *  @return the list of entities
     */
    List<CorrecMoneta> findAll();

    /**
     *  Get the "id" correcMoneta.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CorrecMoneta findOne(Long id);

    /**
     *  Delete the "id" correcMoneta.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
