package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Entrada57b;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Service Interface for managing Entrada57b.
 */

public interface Entrada57bService {

    /**
     * Save a entrada57b.
     *
     * @param entrada57b the entity to save
     * @return the persisted entity
     */
    Entrada57b save(Entrada57b entrada57b);

    /**
     * Get all the entrada57bs.
     *
     * @return the list of entities
     */
    List<Entrada57b> findAll();

    /**
     * Get the "id" entrada57b.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Entrada57b findOne(Long id);

    /**
     * Delete the "id" entrada57b.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    Integer executeSp1InsertaEntrada57B();

    Integer executeSp2InsertaMovIniCartola(Integer annoId);

    Integer executeSp3InsertaMovtosCartola(Integer annoId);

    Integer executeSp4InsertaMovCert57B(Integer annoId);

    Integer executeSp5InsertaSalProxPer(Integer annoId);

    Integer executeSp6InsertaSalIni57B(Integer annoId);

    Integer reprocessE57B(Integer annoId);

    List<Entrada57b> findAllByAnnoTributaId(Long annoId);
}
