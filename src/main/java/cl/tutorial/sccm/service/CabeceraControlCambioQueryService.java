package cl.tutorial.sccm.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import cl.tutorial.sccm.domain.CabeceraControlCambio;
import cl.tutorial.sccm.domain.*; // for static metamodels
import cl.tutorial.sccm.repository.CabeceraControlCambioRepository;
import cl.tutorial.sccm.service.dto.CabeceraControlCambioCriteria;


/**
 * Service for executing complex queries for CabeceraControlCambio entities in the database.
 * The main input is a {@link CabeceraControlCambioCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link CabeceraControlCambio} or a {@link Page} of {%link CabeceraControlCambio} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class CabeceraControlCambioQueryService extends QueryService<CabeceraControlCambio> {

    private final Logger log = LoggerFactory.getLogger(CabeceraControlCambioQueryService.class);


    private final CabeceraControlCambioRepository cabeceraControlCambioRepository;

    public CabeceraControlCambioQueryService(CabeceraControlCambioRepository cabeceraControlCambioRepository) {
        this.cabeceraControlCambioRepository = cabeceraControlCambioRepository;
    }

    /**
     * Return a {@link List} of {%link CabeceraControlCambio} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CabeceraControlCambio> findByCriteria(CabeceraControlCambioCriteria criteria) 
	{
        log.debug("find by criteria : {}", criteria);
        final Specifications<CabeceraControlCambio> specification = createSpecification(criteria);
        return cabeceraControlCambioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link CabeceraControlCambio} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CabeceraControlCambio> findByCriteria(CabeceraControlCambioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CabeceraControlCambio> specification = createSpecification(criteria);
        return cabeceraControlCambioRepository.findAll(specification, page);
    }

    /**
     * Function to convert CabeceraControlCambioCriteria to a {@link Specifications}
     */
    private Specifications<CabeceraControlCambio> createSpecification(CabeceraControlCambioCriteria criteria) {
        Specifications<CabeceraControlCambio> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CabeceraControlCambio_.id));
            }
            if (criteria.getEstado() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEstado(), CabeceraControlCambio_.estado));
            }
            if (criteria.getNombreTabla() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNombreTabla(), CabeceraControlCambio_.nombreTabla));
            }
            if (criteria.getAccion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAccion(), CabeceraControlCambio_.accion));
            }
            if (criteria.getUsuario() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsuario(), CabeceraControlCambio_.usuario));
            }
            if (criteria.getFecha() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFecha(), CabeceraControlCambio_.fecha));
            }
            if (criteria.getIdRegistro() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdRegistro(), CabeceraControlCambio_.idRegistro));
            }
        }
        return specification;
    }

}
