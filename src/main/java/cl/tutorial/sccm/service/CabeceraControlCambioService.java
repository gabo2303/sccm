package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.CabeceraControlCambio;
import java.util.List;

/**
 * Service Interface for managing CabeceraControlCambio.
 */
public interface CabeceraControlCambioService {

    /**
     * Save a cabeceraControlCambio.
     *
     * @param cabeceraControlCambio the entity to save
     * @return the persisted entity
     */
    CabeceraControlCambio save(CabeceraControlCambio cabeceraControlCambio);

    /**
     *  Get all the cabeceraControlCambios.
     *
     *  @return the list of entities
     */
    List<CabeceraControlCambio> findAll();

    /**
     *  Get the "id" cabeceraControlCambio.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CabeceraControlCambio findOne(Long id);

    /**
     *  Delete the "id" cabeceraControlCambio.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
