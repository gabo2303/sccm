package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Cliente1890;
import java.util.List;

/**
 * Service Interface for managing Cliente1890.
 */
public interface Cliente1890Service 
{
    /**
     * Save a cliente1890.
     *
     * @param cliente1890 the entity to save
     * @return the persisted entity
     */
    Cliente1890 save(Cliente1890 cliente1890);

    /**
     *  Get all the cliente1890S.
     *
     *  @return the list of entities
     */
    List<Cliente1890> findAll();

    /**
     *  Get the "id" cliente1890.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Cliente1890 findOne(Long id);

    /**
     *  Delete the "id" cliente1890.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
