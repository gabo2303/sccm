package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Rut_temp;
import java.util.List;

/**
 * Service Interface for managing Rut_temp.
 */
public interface Rut_tempService {

    /**
     * Save a rut_temp.
     *
     * @param rut_temp the entity to save
     * @return the persisted entity
     */
    Rut_temp save(Rut_temp rut_temp);

    /**
     *  Get all the rut_temps.
     *
     *  @return the list of entities
     */
    List<Rut_temp> findAll();

    /**
     *  Get the "id" rut_temp.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Rut_temp findOne(Long id);

    /**
     *  Delete the "id" rut_temp.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
