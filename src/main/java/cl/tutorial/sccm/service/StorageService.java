package cl.tutorial.sccm.service;

import cl.tutorial.sccm.common.Constants;
import cl.tutorial.sccm.config.ETLConfiguration;
import cl.tutorial.sccm.config.JasperConfiguration;
import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.exception.EtlExecutionException;
import cl.tutorial.sccm.repository.AnnoTributaRepository;
import cl.tutorial.sccm.repository.AprobacionRepository;
import cl.tutorial.sccm.security.SecurityUtils;
import cl.tutorial.sccm.service.exception.JarExecutionException;
import cl.tutorial.sccm.service.exception.NotAllProductsAreApprovedException;
import cl.tutorial.sccm.service.util.ConstUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import java.text.DecimalFormat;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

import java.math.BigDecimal;

@Service
public class StorageService 
{
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    
	//LINUX
	//private final Path rootLocation = Paths.get("/u01/app/wls12212/interface/upload");
	//private final Path ifaceLocation = Paths.get("/u01/app/wls12212/interface/temp");
	
	//Windows
	private final Path rootLocation = Paths.get("interface/upload");
    private final Path ifaceLocation = Paths.get("interface/temp");
		
    private ETLConfiguration etlConfiguration;
    private JasperConfiguration jasperConfiguration;
	
    private final AprobacionRepository aprobacionRepository;
    private final AnnoTributaRepository annoTributaRepository;
	
    private static String OS = System.getProperty("os.name").toLowerCase();		
	
	@Autowired
    private EntityManager em;
	
    public StorageService(ETLConfiguration etlConfiguration, JasperConfiguration jasperConfiguration, AprobacionRepository aprobacionRepository,
                          AnnoTributaRepository annoTributaRepository) {
        this.etlConfiguration = etlConfiguration;
        this.jasperConfiguration = jasperConfiguration;
        this.aprobacionRepository = aprobacionRepository;
        this.annoTributaRepository = annoTributaRepository;
    }

    // FUSIONAR INTERFACES SUBIDAS
    public void mergeUploadedIFaces() throws IOException 
	{
        // Establezco Directorio Actual
        final String currentDir = System.getProperty("user.dir");
        
		// Asigno Nombre al archivo de interfaz final y si no existe lo creo
        Path filePath = Paths.get(currentDir + "/" + ifaceLocation + "/" + ConstUtil.BICE_INTERFAZ_1941);
        
		if (!Files.exists(filePath)) { Files.createFile(filePath); }
		
        File folder = new File(currentDir + "/" + rootLocation);
        File[] listOfFiles = folder.listFiles();
        // BICE_INTERFAZ_1941
        PrintWriter pw = new PrintWriter(filePath.toString());

        for (int i = 0; i < listOfFiles.length; i++) 
		{
            if (listOfFiles[i].isFile()) 
			{
                log.info("File " + listOfFiles[i].getName());
                BufferedReader br = new BufferedReader(new FileReader(currentDir + "/" + rootLocation + "/" + listOfFiles[i].getName()));
                String line = br.readLine();
                
				while (line != null) 
				{
                    pw.println(line);
                    log.info("LARGO DE LINEA: " + ((line.length())));
                    line = br.readLine();
                }
				
                br.close();
            }
        }
        pw.flush();
        pw.close();
    }

    public String emiteTxtCertificados() throws IOException {
        // Establezco Directorio Actual
        final String currentDir = "C:\\paso\\Pago_Dividendos\\AT_2018\\Certificados";
        String listado = "";

        log.debug("emiteTxtCertificados: " + currentDir);

        File folder = new File(currentDir);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                if (listado != "")
                    listado = listado + ",";

                listado = listado + listOfFiles[i].getName();
                log.info("File " + listOfFiles[i].getName());
            }
        }

        log.debug(listado);

        return listado;
    }

    public void store(MultipartFile file) 
	{
        try 
		{
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
        } 
		catch (Exception e) { throw new RuntimeException("Error escribiendo fichero " + e.getMessage()); }
    }

    public Resource loadFile(String filename) 
	{
        try 
		{
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            
			if (resource.exists() || resource.isReadable()) { return resource; } else { throw new RuntimeException("FAIL!"); }
        } 
		catch (MalformedURLException e) { throw new RuntimeException("FAIL!"); }
    }

    public void deleteAll() { FileSystemUtils.deleteRecursively(rootLocation.toFile()); }

    public void initDir() 
	{
        try 
		{
            if (!Files.exists(rootLocation)) {
                Files.createDirectory(rootLocation);
            }
            ;
        } catch (IOException e) {
            throw new RuntimeException("No se pudo inicializar el Directorio! " + e.getMessage());
        }
    }

    public static String getSccmUser() {
        // SecurityProperties.User user = (SecurityProperties.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return SecurityUtils.getCurrentUserLogin();
    }

    public String convertAllBackSlash(String value) {
        return value.replaceAll("\\\\", "/");
    }

    public Boolean loadInterface1941(String filename) throws Exception 
	{
		String finalNameFile = "";
        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");
		
        if (isWindows()) 
		{
			finalNameFile = convertAllBackSlash(currentDir + "/" + rootLocation + "/" + filename);
			
            arrayList.add(currentDir + "\\ETL\\PAGO_DIVIDENDO\\CARGA_ENTRADA_1941\\carga_dividendos_pagados\\carga_dividendos_pagados_run.bat");
            System.out.println("This is Windows");
        } 
		else if (isUnix()) 
		{
			finalNameFile = convertAllBackSlash(this.rootLocation + "/" + filename);
			
            arrayList.add(currentDir + "/ETL/PAGO_DIVIDENDO/CARGA_ENTRADA_1941/carga_dividendos_pagados/carga_dividendos_pagados_run.sh");
            System.out.println("This is Unix or Linux");
        }

		log.debug("RUTA FINAL: " + finalNameFile);
	
        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());
        arrayList.add("--context_param:bice_file=" + finalNameFile);
        
		return executeEtl(arrayList);
    }

    public String validateUploadedIFaces() throws IOException 
	{
        String Message = "200";
        final String currentDir = System.getProperty("user.dir");
        File folder = new File(currentDir + "/" + rootLocation);
        File[] listOfFiles = folder.listFiles();
        
		// BICE_INTERFAZ_1941
        // PrintWriter pw = new PrintWriter( filePath.toString());

        for (int i = 0; i < listOfFiles.length; i++) 
		{
            if (listOfFiles[i].isFile()) 
			{
                log.info("Interface " + listOfFiles[i].getName());
                BufferedReader br = new BufferedReader(new FileReader(currentDir + "/" + rootLocation + "/" + listOfFiles[i].getName()));
                String line = br.readLine();
                
				while (line != null) 
				{
                    line = br.readLine();
                    
					if (line != null) 
					{
                        if (line.length() != ConstUtil.LARGO_LINEA_INTERFACE) 
						{
                            log.error("La interfaz " + listOfFiles[i].getName() + " tiene datos no válidos.");
                            // break;
                            Message = "406";
                        }
                    }
                }
				
                br.close();
            }
        }
		
        return Message;
    }

    public void store57Bis(MultipartFile file) {
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            log.info("Archivo subido a directorio: " + file.getOriginalFilename());
            if (this.loadInterface57Bis(file.getOriginalFilename())) {
                log.info("Se cargo interface 57 Bis correctamente...");
            } else {
                log.info("NO SE CARGO INTERFACE DEL TIO OSCIEL");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error escribiendo fichero " + e.getMessage());
        }
    }

    public Boolean loadInterface57Bis(String filename) throws Exception 
	{
        String line;

        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");
        final String finalNameFile = convertAllBackSlash(currentDir + "/" + rootLocation + "/" + filename);

        if (isWindows()) 
		{
            arrayList.add(currentDir + "\\ETL\\INVERSIONES_57BIS\\CARGA_ENTRADA_57BIS\\carga_tabla_certi_57_b_fcc\\carga_tabla_certi_57_b_fcc_run.bat");

            System.out.println("This is Windows");
        } 
		else if (isMac()) {
            System.out.println("This is Mac");
        } 
		else if (isUnix()) 
		{
            arrayList.add(currentDir + "/ETL/INVERSIONES_57BIS/CARGA_ENTRADA_57BIS/carga_tabla_certi_57_b_fcc/carga_tabla_certi_57_b_fcc_run.sh");
	
            System.out.println("This is Unix or Linux");
        }

        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());
        arrayList.add("--context_param:bice_file=" + finalNameFile);

        return executeEtl(arrayList);
    }

    public Boolean executeAllSps() {
        log.info("executeAllSp Ini");

        String line;
        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");

        if (isWindows()) {
            arrayList.add(currentDir + "\\ETL\\INVERSIONES_57BIS\\EXECUTE_ALL_SP_57_B\\ejecuta_SP_EXECUTE_ALL_SP_57B\\ejecuta_SP_EXECUTE_ALL_SP_57B_run.bat");

            System.out.println("This is Windows");
        } else if (isMac()) {
            System.out.println("This is Mac");
        } else if (isUnix()) {
            arrayList.add(currentDir + "/ETL/INVERSIONES_57BIS/EXECUTE_ALL_SP_57_B/ejecuta_SP_EXECUTE_ALL_SP_57B/ejecuta_SP_EXECUTE_ALL_SP_57B_run.sh");

            System.out.println("This is Unix or Linux");
        }

        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());

        try {
            String[] etlLauncher = new String[arrayList.size()];
            Process p = Runtime.getRuntime().exec(arrayList.toArray(etlLauncher));

            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((line = input.readLine()) != null)
                log.info(line);

            while ((line = error.readLine()) != null)
                log.info(line);

            input.close();
            error.close();
            p.waitFor();
            log.info("Se ejecutaron SPS correctamente");

            return true;
        } catch (IOException e) {
            log.info("NO se ejecutaron SPS correctamente: - " + e);
            return false;
        } catch (Exception e) {
            log.info("NO se ejecutaron SPS correctamente: - " + e);
            return false;
        }
    }

    public boolean emiteCertificados57Bis(String rut, String taxYear) throws JarExecutionException 
	{
		if(isUnix())
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getCertInversiones57BisJarPath(), 
										   jasperConfiguration.getCertInversiones57BisExFileNameLin(),
										   jasperConfiguration.getCertInversiones57BisJarName(), 
										   jasperConfiguration.getCertInversiones57BisDestinationPathLin(), rut);
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getCertInversiones57BisJarPath(), 
										   jasperConfiguration.getCertInversiones57BisExFileNameWin(),
										   jasperConfiguration.getCertInversiones57BisJarName(), 
										   jasperConfiguration.getCertInversiones57BisDestinationPathWin(), rut);
		}	
    }

    public boolean emiteDJInversiones57Bis(String taxYear) throws JarExecutionException 
	{
		if(isUnix())
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getDeclJurInversiones57BisJarPath(), 
										   jasperConfiguration.getDeclJurInversiones57BisExFileNameLin(), 
									       jasperConfiguration.getDeclJurInversiones57BisJarName(), 
									       jasperConfiguration.getDeclJurInversiones57BisDestinationPathLin(), "");
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getDeclJurInversiones57BisJarPath(), 
										   jasperConfiguration.getDeclJurInversiones57BisExFileNameWin(), 
									       jasperConfiguration.getDeclJurInversiones57BisJarName(), 
									       jasperConfiguration.getDeclJurInversiones57BisDestinationPathWin(), "");
		}
    }

    public boolean emiteCertificadosPagoDvidendos(String rut, String taxYear) throws JarExecutionException 
	{		
		Query query = em.createNativeQuery(  "select ruta from directorios where producto_id = 1 and instrumento_id = 1 and descripcion = 'Certificados' and anno_tributa_id = ?");  
	
		query.setParameter(1, taxYear);  
		
		String ruta = query.getSingleResult().toString();	
			
		if(isUnix())
		{		
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getCertPagoDvidendosJarPath(), 
										   jasperConfiguration.getCertPagoDvidendosExFileNameLin(), 
										   jasperConfiguration.getCertPagoDvidendosJarName(), 
										   ruta, rut);
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getCertPagoDvidendosJarPath(), 
										   jasperConfiguration.getCertPagoDvidendosExFileNameWin(), 
										   jasperConfiguration.getCertPagoDvidendosJarName(), 
										   ruta, rut);
		}
    }

    public boolean emiteDjPagoDvidendos(String taxYear) throws JarExecutionException 
	{
		Query query = em.createNativeQuery(  "select ruta from directorios where producto_id = 1 and instrumento_id = 1 and descripcion = 'DJ' and anno_tributa_id = ?");  
	
		query.setParameter(1, taxYear);  
		
		String ruta = query.getSingleResult().toString();
		
		if(isUnix())
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getDeclJurPagoDvidendosJarPath(), 
									   jasperConfiguration.getDeclJurPagoDvidendosExFileNameLin(), 
									   jasperConfiguration.getDeclJurPagoDvidendosJarName(), 
									   ruta, "");
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getDeclJurPagoDvidendosJarPath(), 
									   jasperConfiguration.getDeclJurPagoDvidendosExFileNameWin(), 
									   jasperConfiguration.getDeclJurPagoDvidendosJarName(), 
									   ruta, "");
		}	
    }

    public boolean emiteCertificadosOperacionesCaptacion(String rut, String taxYear) 
	throws JarExecutionException, NotAllProductsAreApprovedException 
	{
        areAllProductsApproved(taxYear, "No se pueden emitir los certificados.");
		
		Query query = em.createNativeQuery(  "select ruta from directorios where producto_id = 3 and instrumento_id = 3 and descripcion = 'Certificados' and anno_tributa_id = ?");  
	
		query.setParameter(1, taxYear);  
		
		String ruta = query.getSingleResult().toString();
		
		if(isUnix())
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getCertOperCaptacionJarPath(), 
										   jasperConfiguration.getCertOperCaptacionExFileNameLin(), jasperConfiguration.getCertOperCaptacionJarName(), 
									       ruta, rut);
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getCertOperCaptacionJarPath(), 
										   jasperConfiguration.getCertOperCaptacionExFileNameWin(), jasperConfiguration.getCertOperCaptacionJarName(), 
									       ruta, rut);
		}
    }

    // Validando que todos los productos estén aprobados.
    private void areAllProductsApproved(String taxYear, String type) throws NotAllProductsAreApprovedException 
	{
        Long taxYearId = new Long(taxYear);
        final AnnoTributa annoTributa = annoTributaRepository.findOne(taxYearId);
        Boolean aprobacionProducto = true, cuadraturaContable = true, interesesPagados = true;
        
		if (this.aprobacionRepository
            .countByAnnoTributaAndAprobacionProductoAndCuadraturaContableAndInteresesPagados(annoTributa, aprobacionProducto, cuadraturaContable, interesesPagados).intValue()
            < Constants.TOTAL_PRODUCTS_TO_BE_APPROVED) 
		{

            final String message = type + " Quedan productos sin aprobar o parcialmente aprobados para el año tributario " + annoTributa.getAnnoT() + ".";
            log.info(message);
			
            throw new NotAllProductsAreApprovedException(message);
        }
    }

    public boolean emiteDJOperacionesCaptacion(String taxYear) throws JarExecutionException, NotAllProductsAreApprovedException 
	{
        areAllProductsApproved(taxYear, "No se puede emitir la declaración jurada.");
		
		Query query = em.createNativeQuery(  "select ruta from directorios where producto_id = 3 and instrumento_id = 3 and descripcion = 'DJ' and anno_tributa_id = ?");  
	
		query.setParameter(1, taxYear);  
		
		String ruta = query.getSingleResult().toString();
		
        if(isUnix())
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getDeclJurOperCaptacionJarPath(), 
										   jasperConfiguration.getDeclJurOperCaptacionExFileNameLin(),
										   jasperConfiguration.getDeclJurOperCaptacionJarName(), 
										   ruta, "");
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getDeclJurOperCaptacionJarPath(), 
										   jasperConfiguration.getDeclJurOperCaptacionExFileNameWin(),
										   jasperConfiguration.getDeclJurOperCaptacionJarName(), 
										   ruta, "");
		}
    }

    public boolean emiteRectifOperacionesCaptacion(String taxYear) throws JarExecutionException, NotAllProductsAreApprovedException 
	{
        areAllProductsApproved(taxYear, "No se puede emitir la rectificatoria.");
        
		Query query = em.createNativeQuery(  "select ruta from directorios where producto_id = 3 and instrumento_id = 3 and descripcion = 'Rectificatoria' and anno_tributa_id = ?");  
	
		query.setParameter(1, taxYear);  
		
		String ruta = query.getSingleResult().toString();
		
		if(isUnix())
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getRectifOperCaptacionJarPath(), 
										   jasperConfiguration.getRectifOperCaptacionExFileNameLin(), 
										   jasperConfiguration.getRectifOperCaptacionJarName(), 
										   ruta, "");
		}
		else
		{
			return createAndExecuteBatFile(taxYear, jasperConfiguration.getRectifOperCaptacionJarPath(), 
										   jasperConfiguration.getRectifOperCaptacionExFileNameWin(), 
										   jasperConfiguration.getRectifOperCaptacionJarName(), 
										   ruta, "");
		}	
    }

    public boolean createAndExecuteBatFile(String taxYear, String jarPath, String exFileName, String jarName, String destinationPath, String rut)
    throws JarExecutionException 
	{
        // Establezco Directorio Actual
        final String currentDir = System.getProperty("user.dir") + jarPath;
        log.info("JAR PATH PARA LA OPERACION -> " + currentDir);
		
        FileWriter fichero;
        PrintWriter pw;

        try 
		{
			//if (isUnix()) { destinationPath = System.getProperty("user.dir") + destinationPath; }
			
            fichero = new FileWriter(currentDir + exFileName);
            pw = new PrintWriter(fichero);
            pw.println(Constants.COMMAND_CD + currentDir);
            int taxYearId = Integer.parseInt(taxYear);

            pw.println(Constants.COMMAND_JAVA_MINUS_JAR + jarName + " " + etlConfiguration.getEtldatabaseServerHost() + " " + etlConfiguration.getEtldatabaseServerPort() + " "
                + etlConfiguration.getEtldatabaseServerSid() + " " + etlConfiguration.getEtldatabaseServerUser() + " " + etlConfiguration.getEtldatabaseServerPassword() + " "
                + destinationPath + " " + taxYearId + ((!rut.isEmpty() && !Constants.OPTION_TODOS.toUpperCase().equals(rut.toUpperCase())) ? (" " + rut) : ""));
			
            pw.println(Constants.COMMAND_EXIT);
			
            fichero.close();

            String comando = Constants.COMMAND_CMD_SLASH_C + currentDir + exFileName;
            
			if(isUnix()) { comando = currentDir + exFileName; }
			
			log.info("COMANDO A EJECUTAR -> " + comando);
			
            Process pr = Runtime.getRuntime().exec(comando);
            BufferedReader stdOut = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String s;
			
            log.info("READING BUFFER... ");

            while ((s = stdOut.readLine()) != null) 
			{
                log.info(s);

                // Si se imprime por consola alguno de estos mensajes, es porque ocurrió un error.
                if (s.contains(jasperConfiguration.getStartErrorMessageJarExecution())) { throw new JarExecutionException(s); }
            }

            return true;
        } 
		catch (JarExecutionException e) 
		{
            throw e;
        } 
		catch (Exception e) 
		{
            log.error(e.getMessage(), e);

            return false;
        }
    }

    public void storeOpeCapPactos(MultipartFile file) 
	{
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            log.info("Archivo subido a directorio: " + file.getOriginalFilename());
            if (this.loadIntOpeCapPactos(file.getOriginalFilename())) {
                log.info("Se cargo operacion captación - pactos correctamente...");
            } else {
                log.info("NO SE CARGO INTERFACE DEL TIO OSCIEL OPE CAP - pactos");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error escribiendo fichero " + e.getMessage());
        }
    }

    public Boolean loadIntOpeCapPactos(String filename) throws Exception {
        String line;
        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");
        final String finalNameFile = convertAllBackSlash(currentDir + "/" + rootLocation + "/" + filename);

        if (isWindows()) 
		{
            arrayList.add(currentDir + "\\ETL\\OPERACION_CAPTACION\\CARGA_PACTOS\\BICEERTI_BAC_AT\\BICEERTI_BAC_AT_run.bat");

            System.out.println("This is Windows");
        } 
		else if (isMac()) 
		{
            System.out.println("This is Mac");
        } 
		else if (isUnix()) 
		{
            arrayList.add(currentDir + "/ETL/OPERACION_CAPTACION/CARGA_PACTOS/BICEERTI_BAC_AT/BICEERTI_BAC_AT_run.sh");

            System.out.println("This is Unix or Linux");
        }

        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());
        arrayList.add("--context_param:bice_file=" + finalNameFile);

        return executeEtl(arrayList);
    }

    public void storeOpeCapAhorro(MultipartFile file) {
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            log.info("Archivo subido a directorio: " + file.getOriginalFilename());
            if (this.loadIntOpeCapAhorro(file.getOriginalFilename())) {
                log.info("Se cargo operacion captación - ahorro correctamente...");
            } else {
                log.info("NO SE CARGO INTERFACE DEL TIO OSCIEL OPE CAP - ahorro");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error escribiendo fichero " + e.getMessage());
        }
    }

    public Boolean loadIntOpeCapAhorro(String filename) throws Exception {
        String line;
        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");
        final String finalNameFile = convertAllBackSlash(currentDir + "/" + rootLocation + "/" + filename);

        if (isWindows()) {
            arrayList.add(currentDir + "\\ETL\\OPERACION_CAPTACION\\CARGA_AHORRO\\BICEERTI_AHO_AT2018\\BICEERTI_AHO_AT2018_run.bat");

            System.out.println("This is Windows");
        } else if (isMac()) {
            System.out.println("This is Mac");
        } 
		else if (isUnix()) 
		{
            arrayList.add(currentDir + "/ETL/OPERACION_CAPTACION/CARGA_AHORRO/BICEERTI_AHO_AT2018/BICEERTI_AHO_AT2018_run.sh");

            System.out.println("This is Unix or Linux");
        }
		
        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());
        arrayList.add("--context_param:bice_file=" + finalNameFile);

        return executeEtl(arrayList);
    }

    public void storeOpeCapAhorroMov(MultipartFile file) 
	{
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            log.info("Archivo subido a directorio: " + file.getOriginalFilename());
            if (this.loadIntOpeCapAhorroMov(file.getOriginalFilename())) {
                log.info("Se cargo operacion captación - ahorro MOV correctamente...");
            } else {
                log.info("NO SE CARGO INTERFACE DEL TIO OSCIEL OPE CAP - ahorro MOV");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error escribiendo fichero " + e.getMessage());
        }
    }

    public Boolean loadIntOpeCapAhorroMov(String filename) throws Exception 
	{
        String line;
        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");
        final String finalNameFile = convertAllBackSlash(currentDir + "/" + rootLocation + "/" + filename);

        if (isWindows()) 
		{
            arrayList.add(currentDir + "\\ETL\\OPERACION_CAPTACION\\CARGA_AHORRO_MOV\\BICEERTI_AHO_MOV_AT2018\\BICEERTI_AHO_MOV_AT2018_run.bat");
        } 
		else if (isUnix()) 
		{
            arrayList.add(currentDir + "/ETL/OPERACION_CAPTACION/CARGA_AHORRO_MOV/BICEERTI_AHO_MOV_AT2018/BICEERTI_AHO_MOV_AT2018_run.sh");
        }

        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());
        arrayList.add("--context_param:bice_file=" + finalNameFile);

        return executeEtl(arrayList);
    }

    public void handleFileUploadCargaInterfaces(MultipartFile file, String option) 
	{
        try 
		{
            String etlExecutorPath = "";
            
			switch (option) 
			{
                case Constants.INTERFACES_LOADING_ACCOUNTING:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesAccountingWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesAccountingLin(); }

                    break;

                case Constants.INTERFACES_LOADING_EURO:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesEuroWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesEuroLin(); }

                    break;

                case Constants.INTERFACES_LOADING_PARITY:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesParityWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesParityLin(); }

                    break;

                case Constants.INTERFACES_LOADING_UF:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesUfWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesUfLin(); }

                    break;

                case Constants.INTERFACES_LOADING_USD:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesUsdWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathLoadInterfacesUsdLin(); }

                    break;

                case Constants.BONOS_LETRAS_LOAD_BONOS:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathBonosLetrasLoadBonosWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathBonosLetrasLoadBonosLin(); }

                    break;

                case Constants.BONOS_LETRAS_LOAD_LHBONOS:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathBonosLetrasLoadLhbonosWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathBonosLetrasLoadLhbonosLin(); }

                    break;

                case Constants.BONOS_LETRAS_LOAD_LHDCV:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathBonosLetrasLoadLhdcvWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathBonosLetrasLoadLhdcvLin(); }

                    break;

                case Constants.DATA_LOAD_ETL_DAP:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathDapWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathDapLin(); }

                    break;

                case Constants.DATA_LOAD_ETL_PACTOS:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathPactosWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathPactosLin(); }

                    break;

                case Constants.DATA_LOAD_ETL_AHORROS:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathAhorrosWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathAhorrosLin(); }

                    break;

                case Constants.DATA_LOAD_ETL_AHORROS_MOVIMIENTOS:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathAhorrosMovimientosWin(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathAhorrosMovimientosLin(); }

                    break;
				
				case Constants.DATA_LOAD_ETL_RUT_1890:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathRut1890Win(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathRut1890Lin(); }

                    break;
				
				case Constants.DATA_LOAD_ETL_CLIENTE_1890:

                    if (isWindows()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathCliente1890Win(); } 
					
					else if (isUnix()) { etlExecutorPath = etlConfiguration.getEtlExecutorPathCliente1890Lin(); }

                    break;
				
                default:
                    throw new Exception("La opción del servicio no es válida. Opción: " + option + ".");
            }
			
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
						
			log.info("ARCHIVO SUBIDO AL SIGUIENTE DIRECTORIO: " + file.getOriginalFilename());
			
			if (this.loadInterfacesData(file.getOriginalFilename(), etlExecutorPath)) 
			{
				log.info("Se cargaron correctamente los datos para etl con opción -> " + option);
			}
			else 
			{
				log.info("Error. No se cargaron los datos para etl con opción -> " + option);
				
				throw new Exception("Error. Ha ocurrido un error ejecutando la ETL con la opción -> " + option + ".");
			}			
        }
		catch (Exception e) { throw new RuntimeException(e.getMessage()); }
    }

    private boolean loadInterfacesData(String filename, String etlExecutorPath) throws Exception 
	{
		String finalNameFile = "";
        List<String> arrayList = new ArrayList<>();
        final String currentDir = System.getProperty("user.dir");		
		
		if(isWindows())
		{
			finalNameFile = convertAllBackSlash(currentDir + "/" + rootLocation + "/" + filename);
		}
		else { finalNameFile = convertAllBackSlash(this.rootLocation + "/" + filename); }
        
		arrayList.add(currentDir + etlExecutorPath);
		
		log.info("DIRECTORIO ACTUAL: " + currentDir);
		log.info("LOCACION INICIAL: " + this.rootLocation);
		log.info("NOMBRE FINAL DE ARCHIVO: " + finalNameFile);
		
        arrayList.add("--context_param:bice_Login=" + etlConfiguration.getEtldatabaseServerUser());
        arrayList.add("--context_param:bice_Password=" + etlConfiguration.getEtldatabaseServerPassword());
        arrayList.add("--context_param:bice_Port=" + etlConfiguration.getEtldatabaseServerPort());
        arrayList.add("--context_param:bice_Schema=" + etlConfiguration.getEtldatabaseSchema());
        arrayList.add("--context_param:bice_Server=" + etlConfiguration.getEtldatabaseServerHost());
        arrayList.add("--context_param:bice_Sid=" + etlConfiguration.getEtldatabaseServerSid());
        arrayList.add("--context_param:bice_file=" + finalNameFile);
		
        return executeEtl(arrayList);
    }
	
    private boolean executeEtl(List<String> arrayList) throws Exception 
	{
        String line;
        boolean isSuccess = true;
        
		try 
		{
            StringBuilder stringBuilder = new StringBuilder("Errores: ").append(Constants.LINE_SEPARATOR);
            String[] etlLauncher = new String[arrayList.size()];
            Process p = Runtime.getRuntime().exec(arrayList.toArray(etlLauncher));
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			
			while ((line = input.readLine()) != null) { log.info(line); }
			
            while ((line = error.readLine()) != null) 
			{
                stringBuilder.append(line).append(Constants.LINE_SEPARATOR);
                
				if (isSuccess) { isSuccess = false; }
            }
			
            input.close();
            error.close();
            p.waitFor();
            
			if (!isSuccess) { throw new EtlExecutionException(stringBuilder.toString()); }
			
            return isSuccess;
        } 
		catch (IOException e) 
		{
            log.error("Ha ocurrido un error: - " + e);
            
			return false;
        } 
		catch (EtlExecutionException e) 
		{
            log.error("Ha ocurrido un error: - " + e);
            
			return false;
        }
    }

    public static boolean isWindows() { return (OS.indexOf("win") >= 0); }

    public static boolean isMac() { return (OS.indexOf("mac") >= 0); }

    public static boolean isUnix() { return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0); }
}
