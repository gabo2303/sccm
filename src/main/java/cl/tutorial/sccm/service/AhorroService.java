package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Ahorro;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Ahorro.
 */
public interface AhorroService {

    /**
     * Save a ahorro.
     *
     * @param ahorro
     * 		the entity to save
     *
     * @return the persisted entity
     */
    Ahorro save(Ahorro ahorro);

    /**
     * Get all the ahorros.
     *
     * @return the list of entities
     */
    List<Ahorro> findAll();

    /**
     * Get the "id" ahorro.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    Ahorro findOne(Long id);

    /**
     * Delete the "id" ahorro.
     *
     * @param id
     * 		the id of the entity
     */
    void delete(Long id);

    List<Ahorro> findAllByAnnoTributaId(Long annoId);

    Page<Ahorro> findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(Long idAnno, String alarmUniqueCode, Pageable pageable);

    List<Ahorro> findByAllByTaxYearIdAndAlertTypeUniqueCode(Long idAnno, String alarmUniqueCode);
}
