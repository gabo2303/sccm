package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Moneda;
import java.util.List;

/**
 * Service Interface for managing Moneda.
 */
public interface MonedaService {

    /**
     * Save a moneda.
     *
     * @param moneda the entity to save
     * @return the persisted entity
     */
    Moneda save(Moneda moneda);

    /**
     *  Get all the monedas.
     *
     *  @return the list of entities
     */
    List<Moneda> findAll();

    /**
     *  Get the "id" moneda.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Moneda findOne(Long id);

    /**
     *  Delete the "id" moneda.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
