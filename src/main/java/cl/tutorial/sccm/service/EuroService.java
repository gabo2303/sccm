package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Euro;
import java.util.List;

/**
 * Service Interface for managing Euro.
 */
public interface EuroService {

    /**
     * Save a euro.
     *
     * @param euro the entity to save
     * @return the persisted entity
     */
    Euro save(Euro euro);

    /**
     *  Get all the euros.
     *
     *  @return the list of entities
     */
    List<Euro> findAll();

    /**
     *  Get the "id" euro.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Euro findOne(Long id);

    /**
     *  Delete the "id" euro.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
