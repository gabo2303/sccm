package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Clientes1890mantencion;
import java.util.List;

/**
 * Service Interface for managing Clientes1890mantencion.
 */
public interface Clientes1890mantencionService {

    /**
     * Save a clientes1890mantencion.
     *
     * @param clientes1890mantencion the entity to save
     * @return the persisted entity
     */
    Clientes1890mantencion save(Clientes1890mantencion clientes1890mantencion);

    /**
     *  Get all the clientes1890mantencions.
     *
     *  @return the list of entities
     */
    List<Clientes1890mantencion> findAll();

    /**
     *  Get the "id" clientes1890mantencion.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Clientes1890mantencion findOne(Long id);

    /**
     *  Delete the "id" clientes1890mantencion.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
