package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.LHBonos;
import java.util.List;

/**
 * Service Interface for managing LHBonos.
 */
public interface LHBonosService {

    /**
     * Save a lHBonos.
     *
     * @param lHBonos the entity to save
     * @return the persisted entity
     */
    LHBonos save(LHBonos lHBonos);

    /**
     *  Get all the lHBonos.
     *
     *  @return the list of entities
     */
    List<LHBonos> findAll();

    /**
     *  Get the "id" lHBonos.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LHBonos findOne(Long id);

    /**
     *  Delete the "id" lHBonos.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
