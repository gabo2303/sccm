package cl.tutorial.sccm.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import cl.tutorial.sccm.domain.Temporal1890;
import cl.tutorial.sccm.domain.*; // for static metamodels
import cl.tutorial.sccm.repository.Temporal1890Repository;
import cl.tutorial.sccm.service.dto.Temporal1890Criteria;


/**
 * Service for executing complex queries for Temporal1890 entities in the database.
 * The main input is a {@link Temporal1890Criteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Temporal1890} or a {@link Page} of {%link Temporal1890} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class Temporal1890QueryService extends QueryService<Temporal1890> {

    private final Logger log = LoggerFactory.getLogger(Temporal1890QueryService.class);


    private final Temporal1890Repository temporal1890Repository;

    public Temporal1890QueryService(Temporal1890Repository temporal1890Repository) {
        this.temporal1890Repository = temporal1890Repository;
    }

    /**
     * Return a {@link List} of {%link Temporal1890} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Temporal1890> findByCriteria(Temporal1890Criteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Temporal1890> specification = createSpecification(criteria);
        return temporal1890Repository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Temporal1890} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Temporal1890> findByCriteria(Temporal1890Criteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Temporal1890> specification = createSpecification(criteria);
        return temporal1890Repository.findAll(specification, page);
    }

    /**
     * Function to convert Temporal1890Criteria to a {@link Specifications}
     */
    private Specifications<Temporal1890> createSpecification(Temporal1890Criteria criteria) {
        Specifications<Temporal1890> specification = Specifications.where(null);
        if (criteria != null) {
            
            if (criteria.getRut() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRut(), Temporal1890_.rut));
            }
            if (criteria.getDv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDv(), Temporal1890_.dv));
            }
            if (criteria.getTotalcoluno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcoluno(), Temporal1890_.totalcoluno));
            }
            if (criteria.getTotalcolunoneg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolunoneg(), Temporal1890_.totalcolunoneg));
            }
            if (criteria.getTotalcoldos() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcoldos(), Temporal1890_.totalcoldos));
            }
            if (criteria.getTotalcoldosneg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcoldosneg(), Temporal1890_.totalcoldosneg));
            }
            if (criteria.getTotalcoltres() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcoltres(), Temporal1890_.totalcoltres));
            }
            if (criteria.getTotalcoltresneg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcoltresneg(), Temporal1890_.totalcoltresneg));
            }
            if (criteria.getTotalcolcuatro() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolcuatro(), Temporal1890_.totalcolcuatro));
            }
            if (criteria.getTotalcolcuatroneg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolcuatroneg(), Temporal1890_.totalcolcuatroneg));
            }
            if (criteria.getTotalcolcinco() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolcinco(), Temporal1890_.totalcolcinco));
            }
            if (criteria.getTotalcolcinconeg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolcinconeg(), Temporal1890_.totalcolcinconeg));
            }
            if (criteria.getTotalcolseis() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolseis(), Temporal1890_.totalcolseis));
            }
            if (criteria.getTotalcolseisneg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalcolseisneg(), Temporal1890_.totalcolseisneg));
            }
            if (criteria.getTip_16() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTip_16(), Temporal1890_.tip_16));
            }
            if (criteria.getTip_17() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTip_17(), Temporal1890_.tip_17));
            }
            if (criteria.getNro_cer() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNro_cer(), Temporal1890_.nro_cer));
            }
        }
        return specification;
    }

}
