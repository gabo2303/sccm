package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.CertPagdiv;
import cl.tutorial.sccm.domain.EntPagdiv;
import java.util.List;

/**
 * Service Interface for managing EntPagdiv.
 */
public interface EntPagdivService {

    /**
     * Save a entPagdiv.
     *
     * @param entPagdiv the entity to save
     * @return the persisted entity
     */
    EntPagdiv save(EntPagdiv entPagdiv);

    /**
     *  Get all the entPagdivs.
     *
     *  @return the list of entities
     */
    List<EntPagdiv> findAll();

    /**
     *  Get the "id" entPagdiv.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EntPagdiv findOne(Long id);

    /**
     *  Delete the "id" entPagdiv.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<EntPagdiv> findAllByAnnoTributaId(Long annoId);
}
