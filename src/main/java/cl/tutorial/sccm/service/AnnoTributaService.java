package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.AnnoTributa;

import java.util.List;

/**
 * Service Interface for managing AnnoTributa.
 */
public interface AnnoTributaService 
{
    /**
     * Save a annoTributa.
     *
     * @param annoTributa
     * 		the entity to save
     *
     * @return the persisted entity
     */
    AnnoTributa save(AnnoTributa annoTributa);

    /**
     * Get all the annoTributas.
     *
     * @return the list of entities
     */
    List<AnnoTributa> findAllByIdNotEqual(Long id);

	/**
     * Get all the annoTributas activos.
     *
     * @return the list of entities
     */
    List<AnnoTributa> findAllByAnnoActivo(String annoActivo);

    /**
     * Get the "id" annoTributa.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    AnnoTributa findOne(Long id);

    /**
     * Delete the "id" annoTributa.
     *
     * @param id
     * 		the id of the entity
     */
    void delete(Long id);
}
