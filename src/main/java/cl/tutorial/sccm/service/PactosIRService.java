package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Pactos;
import cl.tutorial.sccm.domain.PactosIR;

import java.util.List;

/**
 * Service Interface for managing Pactos.
 */
public interface PactosIRService {

        List<PactosIR> findAllByAnnoTributa(Long annoId);
}
