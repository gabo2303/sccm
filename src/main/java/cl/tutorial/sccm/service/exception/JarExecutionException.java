package cl.tutorial.sccm.service.exception;

public class JarExecutionException extends Exception {

    public JarExecutionException() {
    }

    public JarExecutionException(String message) {
        super(message);
    }

    public JarExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public JarExecutionException(Throwable cause) {
        super(cause);
    }
}
