package cl.tutorial.sccm.service.exception;

public class NotAllProductsAreApprovedException extends Exception {

    public NotAllProductsAreApprovedException() {
    }

    public NotAllProductsAreApprovedException(String message) {
        super(message);
    }

    public NotAllProductsAreApprovedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAllProductsAreApprovedException(Throwable cause) {
        super(cause);
    }
}
