package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Ahorro;
import cl.tutorial.sccm.domain.DAP;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing DAP.
 */
public interface DAPService {

    /**
     * Save a dAP.
     *
     * @param dAP
     * 		the entity to save
     *
     * @return the persisted entity
     */
    DAP save(DAP dAP);

    /**
     * Get all the dAPS.
     *
     * @return the list of entities
     */
    List<DAP> findAll();

    /**
     * Get the "id" dAP.
     *
     * @param id
     * 		the id of the entity
     *
     * @return the entity
     */
    DAP findOne(Long id);

    /**
     * Delete the "id" dAP.
     *
     * @param id
     * 		the id of the entity
     */
    void delete(Long id);

    Page<DAP> findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(Long idAnno, String alarmUniqueCode, Pageable pageable);

    List<DAP> findByAllByTaxYearIdAndAlertTypeUniqueCode(Long idAnno, String alarmUniqueCode);

}
