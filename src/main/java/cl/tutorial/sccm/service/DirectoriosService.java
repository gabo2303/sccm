package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Directorios;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Directorios.
 */
public interface DirectoriosService {

    /**
     * Save a directorios.
     *
     * @param directorios the entity to save
     * @return the persisted entity
     */
    Directorios save(Directorios directorios);

    /**
     *  Get all the directorios.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Directorios> findAll(Pageable pageable);

    /**
     *  Get the "id" directorios.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Directorios findOne(Long id);

    /**
     *  Delete the "id" directorios.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
