package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.DetalleControlCambio;
import java.util.List;

/**
 * Service Interface for managing DetalleControlCambio.
 */
public interface DetalleControlCambioService {

    /**
     * Save a detalleControlCambio.
     *
     * @param detalleControlCambio the entity to save
     * @return the persisted entity
     */
    DetalleControlCambio save(DetalleControlCambio detalleControlCambio);

    /**
     *  Get all the detalleControlCambios.
     *
     *  @return the list of entities
     */
    List<DetalleControlCambio> findAll();

    /**
     *  Get the "id" detalleControlCambio.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DetalleControlCambio findOne(Long id);

    /**
     *  Delete the "id" detalleControlCambio.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
