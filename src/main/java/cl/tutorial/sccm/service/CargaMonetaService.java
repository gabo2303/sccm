package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.CargaMoneta;
import java.util.List;

/**
 * Service Interface for managing CargaMoneta.
 */
public interface CargaMonetaService {

    /**
     * Save a cargaMoneta.
     *
     * @param cargaMoneta the entity to save
     * @return the persisted entity
     */
    CargaMoneta save(CargaMoneta cargaMoneta);

    /**
     *  Get all the cargaMonetas.
     *
     *  @return the list of entities
     */
    List<CargaMoneta> findAll();

    /**
     *  Get the "id" cargaMoneta.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CargaMoneta findOne(Long id);

    /**
     *  Delete the "id" cargaMoneta.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
