package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Glosa;
import java.util.List;

/**
 * Service Interface for managing Glosa.
 */
public interface GlosaService {

    /**
     * Save a glosa.
     *
     * @param glosa the entity to save
     * @return the persisted entity
     */
    Glosa save(Glosa glosa);

    /**
     *  Get all the glosas.
     *
     *  @return the list of entities
     */
    List<Glosa> findAll();

    /**
     *  Get the "id" glosa.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Glosa findOne(Long id);

    /**
     *  Delete the "id" glosa.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<Glosa> findAllByAnnoTributaIdAndInstrumentoId(Long annoId, Long instId);
}
