package cl.tutorial.sccm.service;

import org.springframework.data.domain.Page;

import java.math.BigInteger;

public interface DatosEmisionCertificadoService {

    Page<Object> findAllByTaxYearIdRutDataType(Long taxYearId, Integer dataType, BigInteger rut, String folio);
}
