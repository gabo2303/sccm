package cl.tutorial.sccm.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Temporal1890 entity. This class is used in Temporal1890Resource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /temporal-1890-s?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class Temporal1890Criteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter rut;

    private StringFilter dv;

    private LongFilter totalcoluno;

    private LongFilter totalcolunoneg;

    private LongFilter totalcoldos;

    private LongFilter totalcoldosneg;

    private LongFilter totalcoltres;

    private LongFilter totalcoltresneg;

    private LongFilter totalcolcuatro;

    private LongFilter totalcolcuatroneg;

    private LongFilter totalcolcinco;

    private LongFilter totalcolcinconeg;

    private LongFilter totalcolseis;

    private LongFilter totalcolseisneg;

    private IntegerFilter tip_16;

    private IntegerFilter tip_17;

    private IntegerFilter nro_cer;


    public Temporal1890Criteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRut() {
        return rut;
    }

    public void setRut(IntegerFilter rut) {
        this.rut = rut;
    }

    public StringFilter getDv() {
        return dv;
    }

    public void setDv(StringFilter dv) {
        this.dv = dv;
    }

    public LongFilter getTotalcoluno() {
        return totalcoluno;
    }

    public void setTotalcoluno(LongFilter totalcoluno) {
        this.totalcoluno = totalcoluno;
    }

    public LongFilter getTotalcolunoneg() {
        return totalcolunoneg;
    }

    public void setTotalcolunoneg(LongFilter totalcolunoneg) {
        this.totalcolunoneg = totalcolunoneg;
    }

    public LongFilter getTotalcoldos() {
        return totalcoldos;
    }

    public void setTotalcoldos(LongFilter totalcoldos) {
        this.totalcoldos = totalcoldos;
    }

    public LongFilter getTotalcoldosneg() {
        return totalcoldosneg;
    }

    public void setTotalcoldosneg(LongFilter totalcoldosneg) {
        this.totalcoldosneg = totalcoldosneg;
    }

    public LongFilter getTotalcoltres() {
        return totalcoltres;
    }

    public void setTotalcoltres(LongFilter totalcoltres) {
        this.totalcoltres = totalcoltres;
    }

    public LongFilter getTotalcoltresneg() {
        return totalcoltresneg;
    }

    public void setTotalcoltresneg(LongFilter totalcoltresneg) {
        this.totalcoltresneg = totalcoltresneg;
    }

    public LongFilter getTotalcolcuatro() {
        return totalcolcuatro;
    }

    public void setTotalcolcuatro(LongFilter totalcolcuatro) {
        this.totalcolcuatro = totalcolcuatro;
    }

    public LongFilter getTotalcolcuatroneg() {
        return totalcolcuatroneg;
    }

    public void setTotalcolcuatroneg(LongFilter totalcolcuatroneg) {
        this.totalcolcuatroneg = totalcolcuatroneg;
    }

    public LongFilter getTotalcolcinco() {
        return totalcolcinco;
    }

    public void setTotalcolcinco(LongFilter totalcolcinco) {
        this.totalcolcinco = totalcolcinco;
    }

    public LongFilter getTotalcolcinconeg() {
        return totalcolcinconeg;
    }

    public void setTotalcolcinconeg(LongFilter totalcolcinconeg) {
        this.totalcolcinconeg = totalcolcinconeg;
    }

    public LongFilter getTotalcolseis() {
        return totalcolseis;
    }

    public void setTotalcolseis(LongFilter totalcolseis) {
        this.totalcolseis = totalcolseis;
    }

    public LongFilter getTotalcolseisneg() {
        return totalcolseisneg;
    }

    public void setTotalcolseisneg(LongFilter totalcolseisneg) {
        this.totalcolseisneg = totalcolseisneg;
    }

    public IntegerFilter getTip_16() {
        return tip_16;
    }

    public void setTip_16(IntegerFilter tip_16) {
        this.tip_16 = tip_16;
    }

    public IntegerFilter getTip_17() {
        return tip_17;
    }

    public void setTip_17(IntegerFilter tip_17) {
        this.tip_17 = tip_17;
    }

    public IntegerFilter getNro_cer() {
        return nro_cer;
    }

    public void setNro_cer(IntegerFilter nro_cer) {
        this.nro_cer = nro_cer;
    }

    @Override
    public String toString() {
        return "Temporal1890Criteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (rut != null ? "rut=" + rut + ", " : "") +
                (dv != null ? "dv=" + dv + ", " : "") +
                (totalcoluno != null ? "totalcoluno=" + totalcoluno + ", " : "") +
                (totalcolunoneg != null ? "totalcolunoneg=" + totalcolunoneg + ", " : "") +
                (totalcoldos != null ? "totalcoldos=" + totalcoldos + ", " : "") +
                (totalcoldosneg != null ? "totalcoldosneg=" + totalcoldosneg + ", " : "") +
                (totalcoltres != null ? "totalcoltres=" + totalcoltres + ", " : "") +
                (totalcoltresneg != null ? "totalcoltresneg=" + totalcoltresneg + ", " : "") +
                (totalcolcuatro != null ? "totalcolcuatro=" + totalcolcuatro + ", " : "") +
                (totalcolcuatroneg != null ? "totalcolcuatroneg=" + totalcolcuatroneg + ", " : "") +
                (totalcolcinco != null ? "totalcolcinco=" + totalcolcinco + ", " : "") +
                (totalcolcinconeg != null ? "totalcolcinconeg=" + totalcolcinconeg + ", " : "") +
                (totalcolseis != null ? "totalcolseis=" + totalcolseis + ", " : "") +
                (totalcolseisneg != null ? "totalcolseisneg=" + totalcolseisneg + ", " : "") +
                (tip_16 != null ? "tip_16=" + tip_16 + ", " : "") +
                (tip_17 != null ? "tip_17=" + tip_17 + ", " : "") +
                (nro_cer != null ? "nro_cer=" + nro_cer + ", " : "") +
            "}";
    }

}
