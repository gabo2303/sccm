package cl.tutorial.sccm.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the CabeceraControlCambio entity. This class is used in CabeceraControlCambioResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /cabecera-control-cambios?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CabeceraControlCambioCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter estado;

    private StringFilter nombreTabla;

    private StringFilter accion;

    private StringFilter usuario;

    private LocalDateFilter fecha;

    private LongFilter idRegistro;

    public CabeceraControlCambioCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEstado() {
        return estado;
    }

    public void setEstado(StringFilter estado) {
        this.estado = estado;
    }

    public StringFilter getNombreTabla() {
        return nombreTabla;
    }

    public void setNombreTabla(StringFilter nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public StringFilter getAccion() {
        return accion;
    }

    public void setAccion(StringFilter accion) {
        this.accion = accion;
    }

    public StringFilter getUsuario() {
        return usuario;
    }

    public void setUsuario(StringFilter usuario) {
        this.usuario = usuario;
    }

    public LocalDateFilter getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateFilter fecha) {
        this.fecha = fecha;
    }

    public LongFilter getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(LongFilter idRegistro) {
        this.idRegistro = idRegistro;
    }

    @Override
    public String toString() {
        return "CabeceraControlCambioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (estado != null ? "estado=" + estado + ", " : "") +
                (nombreTabla != null ? "nombreTabla=" + nombreTabla + ", " : "") +
                (accion != null ? "accion=" + accion + ", " : "") +
                (usuario != null ? "usuario=" + usuario + ", " : "") +
                (fecha != null ? "fecha=" + fecha + ", " : "") +
                (idRegistro != null ? "idRegistro=" + idRegistro + ", " : "") +
            "}";
    }

}
