package cl.tutorial.sccm.service.dto;

/**
 * A DTO representing a LetrasBonos Entity.
 *
 * @author Antonio Marrero Palomino.
 */
public class LetrasBonosDto {

    private String bipersonal;
    private String capital;
    private String capMon;
    private Long   codInst;
    private Long   codMon;
    private Long   codTipoInt;
    private Long   correlativo;
    private Long   corrMoneda;
    private Long   ctaCap;
    private Long   ctaInt;
    private Long   ctaReaj;
    private String dv;
    private String estCta;
    private String extranjero;
    private String fechaInv;
    private String fechaPago;
    private String fecCont;
    private String fecVcto;
    private String folio;
    private String impExt;
    private String indPago;
    private String intNom;
    private String intReal;
    private Long   monFi;
    private Long   monPac;
    private String monTasa;
    private String nemotecnico;
    private String nomCli;
    private Long   numCupones;
    private Long   numCuponPago;
    private Long   numGiro;
    private String onp;
    private String origen;
    private Long   perTasa;
    private Long   plazo;
    private Long   producto;
    private String reajustabilidad;
    private Long   rut;
    private String serieInst;
    private String tasaEmi;
    private String tipoInt;
    private String valorNominal;

    public String getBipersonal() {
        return bipersonal;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getCapMon() {
        return capMon;
    }

    public void setCapMon(String capMon) {
        this.capMon = capMon;
    }

    public Long getCodInst() {
        return codInst;
    }

    public void setCodInst(Long codInst) {
        this.codInst = codInst;
    }

    public Long getCodMon() {
        return codMon;
    }

    public void setCodMon(Long codMon) {
        this.codMon = codMon;
    }

    public Long getCodTipoInt() {
        return codTipoInt;
    }

    public void setCodTipoInt(Long codTipoInt) {
        this.codTipoInt = codTipoInt;
    }

    public Long getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(Long correlativo) {
        this.correlativo = correlativo;
    }

    public Long getCorrMoneda() {
        return corrMoneda;
    }

    public void setCorrMoneda(Long corrMoneda) {
        this.corrMoneda = corrMoneda;
    }

    public Long getCtaCap() {
        return ctaCap;
    }

    public void setCtaCap(Long ctaCap) {
        this.ctaCap = ctaCap;
    }

    public Long getCtaInt() {
        return ctaInt;
    }

    public void setCtaInt(Long ctaInt) {
        this.ctaInt = ctaInt;
    }

    public Long getCtaReaj() {
        return ctaReaj;
    }

    public void setCtaReaj(Long ctaReaj) {
        this.ctaReaj = ctaReaj;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public String getEstCta() {
        return estCta;
    }

    public void setEstCta(String estCta) {
        this.estCta = estCta;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }

    public String getFechaInv() {
        return fechaInv;
    }

    public void setFechaInv(String fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getFecCont() {
        return fecCont;
    }

    public void setFecCont(String fecCont) {
        this.fecCont = fecCont;
    }

    public String getFecVcto() {
        return fecVcto;
    }

    public void setFecVcto(String fecVcto) {
        this.fecVcto = fecVcto;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getImpExt() {
        return impExt;
    }

    public void setImpExt(String impExt) {
        this.impExt = impExt;
    }

    public String getIndPago() {
        return indPago;
    }

    public void setIndPago(String indPago) {
        this.indPago = indPago;
    }

    public String getIntNom() {
        return intNom;
    }

    public void setIntNom(String intNom) {
        this.intNom = intNom;
    }

    public String getIntReal() {
        return intReal;
    }

    public void setIntReal(String intReal) {
        this.intReal = intReal;
    }

    public Long getMonFi() {
        return monFi;
    }

    public void setMonFi(Long monFi) {
        this.monFi = monFi;
    }

    public Long getMonPac() {
        return monPac;
    }

    public void setMonPac(Long monPac) {
        this.monPac = monPac;
    }

    public String getMonTasa() {
        return monTasa;
    }

    public void setMonTasa(String monTasa) {
        this.monTasa = monTasa;
    }

    public String getNemotecnico() {
        return nemotecnico;
    }

    public void setNemotecnico(String nemotecnico) {
        this.nemotecnico = nemotecnico;
    }

    public String getNomCli() {
        return nomCli;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public Long getNumCupones() {
        return numCupones;
    }

    public void setNumCupones(Long numCupones) {
        this.numCupones = numCupones;
    }

    public Long getNumCuponPago() {
        return numCuponPago;
    }

    public void setNumCuponPago(Long numCuponPago) {
        this.numCuponPago = numCuponPago;
    }

    public Long getNumGiro() {
        return numGiro;
    }

    public void setNumGiro(Long numGiro) {
        this.numGiro = numGiro;
    }

    public String getOnp() {
        return onp;
    }

    public void setOnp(String onp) {
        this.onp = onp;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getPerTasa() {
        return perTasa;
    }

    public void setPerTasa(Long perTasa) {
        this.perTasa = perTasa;
    }

    public Long getPlazo() {
        return plazo;
    }

    public void setPlazo(Long plazo) {
        this.plazo = plazo;
    }

    public Long getProducto() {
        return producto;
    }

    public void setProducto(Long producto) {
        this.producto = producto;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public Long getRut() {
        return rut;
    }

    public void setRut(Long rut) {
        this.rut = rut;
    }

    public String getSerieInst() {
        return serieInst;
    }

    public void setSerieInst(String serieInst) {
        this.serieInst = serieInst;
    }

    public String getTasaEmi() {
        return tasaEmi;
    }

    public void setTasaEmi(String tasaEmi) {
        this.tasaEmi = tasaEmi;
    }

    public String getTipoInt() {
        return tipoInt;
    }

    public void setTipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
    }

    public String getValorNominal() {
        return valorNominal;
    }

    public void setValorNominal(String valorNominal) {
        this.valorNominal = valorNominal;
    }

    @Override
    public String toString() {
        return "LetrasBonosDto{" + "bipersonal='" + bipersonal + '\'' + ", capital='" + capital + '\'' + ", capMon='" + capMon + '\'' + ", codInst=" + codInst + ", codMon="
            + codMon + ", codTipoInt=" + codTipoInt + ", correlativo=" + correlativo + ", corrMoneda=" + corrMoneda + ", ctaCap=" + ctaCap + ", ctaInt=" + ctaInt + ", ctaReaj="
            + ctaReaj + ", dv='" + dv + '\'' + ", estCta='" + estCta + '\'' + ", extranjero='" + extranjero + '\'' + ", fechaInv='" + fechaInv + '\'' + ", fechaPago='" + fechaPago
            + '\'' + ", fecCont='" + fecCont + '\'' + ", fecVcto='" + fecVcto + '\'' + ", folio='" + folio + '\'' + ", impExt='" + impExt + '\'' + ", indPago='" + indPago + '\''
            + ", intNom='" + intNom + '\'' + ", intReal='" + intReal + '\'' + ", monFi=" + monFi + ", monPac=" + monPac + ", monTasa='" + monTasa + '\'' + ", nemotecnico='"
            + nemotecnico + '\'' + ", nomCli='" + nomCli + '\'' + ", numCupones=" + numCupones + ", numCuponPago=" + numCuponPago + ", numGiro=" + numGiro + ", onp='" + onp + '\''
            + ", origen='" + origen + '\'' + ", perTasa=" + perTasa + ", plazo=" + plazo + ", producto=" + producto + ", reajustabilidad='" + reajustabilidad + '\'' + ", rut="
            + rut + ", serieInst='" + serieInst + '\'' + ", tasaEmi='" + tasaEmi + '\'' + ", tipoInt='" + tipoInt + '\'' + ", valorNominal='" + valorNominal + '\'' + '}';
    }
}
