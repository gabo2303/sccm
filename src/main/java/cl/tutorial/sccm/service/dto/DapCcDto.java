package cl.tutorial.sccm.service.dto;

/**
 * A DTO representing a DapCC Entity.
 */
public class DapCcDto {

    private Long    sucursal;
    private Long    cuentaInt;
    private Long    codMoneda;
    private Integer monedaContable;
    private String  tipoPlazo;
    private String  fechaContable;
    private String  fcc;
    private String  contabilidad;
    private String  diferencia;
    private String  diferenciaAbs;

    public Long getSucursal() {
        return sucursal;
    }

    public DapCcDto sucursal(Long sucursal) {
        this.sucursal = sucursal;
        return this;
    }

    public void setSucursal(Long sucursal) {
        this.sucursal = sucursal;
    }

    public Long getCuentaInt() {
        return cuentaInt;
    }

    public DapCcDto cuentaInt(Long cuentaInt) {
        this.cuentaInt = cuentaInt;
        return this;
    }

    public void setCuentaInt(Long cuentaInt) {
        this.cuentaInt = cuentaInt;
    }

    public Long getCodMoneda() {
        return codMoneda;
    }

    public DapCcDto codMoneda(Long codMoneda) {
        this.codMoneda = codMoneda;
        return this;
    }

    public void setCodMoneda(Long codMoneda) {
        this.codMoneda = codMoneda;
    }

    public Integer getMonedaContable() {
        return monedaContable;
    }

    public DapCcDto monedaContable(Integer monedaContable) {
        this.monedaContable = monedaContable;
        return this;
    }

    public void setMonedaContable(Integer monedaContable) {
        this.monedaContable = monedaContable;
    }

    public String getTipoPlazo() {
        return tipoPlazo;
    }

    public DapCcDto tipoPlazo(String tipoPlazo) {
        this.tipoPlazo = tipoPlazo;
        return this;
    }

    public void setTipoPlazo(String tipoPlazo) {
        this.tipoPlazo = tipoPlazo;
    }

    public String getFechaContable() {
        return fechaContable;
    }

    public DapCcDto fechaContable(String fechaContable) {
        this.fechaContable = fechaContable;
        return this;
    }

    public void setFechaContable(String fechaContable) {
        this.fechaContable = fechaContable;
    }

    public String getFcc() {
        return fcc;
    }

    public DapCcDto fcc(String fcc) {
        this.fcc = fcc;
        return this;
    }

    public void setFcc(String fcc) {
        this.fcc = fcc;
    }

    public String getContabilidad() {
        return contabilidad;
    }

    public DapCcDto contabilidad(String contabilidad) {
        this.contabilidad = contabilidad;
        return this;
    }

    public void setContabilidad(String contabilidad) {
        this.contabilidad = contabilidad;
    }

    public String getDiferencia() {
        return diferencia;
    }

    public DapCcDto diferencia(String diferencia) {
        this.diferencia = diferencia;
        return this;
    }

    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }

    public String getDiferenciaAbs() {
        return diferenciaAbs;
    }

    public DapCcDto diferenciaAbs(String diferenciaAbs) {
        this.diferenciaAbs = diferenciaAbs;
        return this;
    }

    public void setDiferenciaAbs(String diferenciaAbs) {
        this.diferenciaAbs = diferenciaAbs;
    }

    @Override
    public String toString() {
        return "DapCcDto{" + "sucursal=" + sucursal + ", cuentaInt=" + cuentaInt + ", codMoneda=" + codMoneda + ", monedaContable=" + monedaContable + ", tipoPlazo='" + tipoPlazo
            + '\'' + ", fechaContable='" + fechaContable + '\'' + ", fcc='" + fcc + '\'' + ", contabilidad='" + contabilidad + '\'' + ", diferencia='" + diferencia + '\''
            + ", diferenciaAbs='" + diferenciaAbs + '\'' + '}';
    }
}
