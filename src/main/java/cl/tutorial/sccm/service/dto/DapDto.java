package cl.tutorial.sccm.service.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * A DTO representing a DAP Entity.
 *
 * @author Antonio Marrero Palomino.
 */
public class DapDto {

    private String     fecVcto;
    private String     fechaInv;
    private String     fecCont;
    private String     fechaPago;
    private String     capital;
    private String     capMon;
    private String     intNom;
    private String     intReal;
    private String     monFi;
    private String     tasaOp;
    private String     intPagado;
    private String     monTasa;
    private String     impExt;
    private BigInteger plazo;
    private BigInteger numCupones;
    private BigInteger numCuponPago;
    private BigInteger reajPagado;
    private BigInteger sucursal;
    private BigInteger rut;
    private BigInteger producto;
    private BigInteger moneda;
    private BigInteger correlativo;
    private String folio;
    private BigInteger numGiro;
    private BigInteger codMon;
    private BigInteger ctaCap;
    private BigInteger codTipoInt;
    private BigInteger perTasa;
    private BigInteger ctaReaj;
    private BigInteger ctaInt;
    private String     dv;
    private String     tipPlazo;
    private String     tipoInt;
    private String     onp;
    private String     estCta;
    private String     bipersonal;
    private String     reajustabilidad;
    private String     indPago;
    private String     p57Bis;
    private String     extranjero;
    private String     origen;
    private String     interesNominalPesos;
    private String     reajusteNegativo;

    public String getInteresNominalPesos() {
        return interesNominalPesos;
    }

    public void setInteresNominalPesos(String interesNominalPesos) {
        this.interesNominalPesos = interesNominalPesos;
    }

    public String getReajusteNegativo() {
        return reajusteNegativo;
    }

    public void setReajusteNegativo(String reajusteNegativo) {
        this.reajusteNegativo = reajusteNegativo;
    }

    public BigInteger getRut() {
        return rut;
    }

    public void setRut(BigInteger rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public DapDto dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public BigInteger getProducto() {
        return producto;
    }

    public DapDto producto(BigInteger producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(BigInteger producto) {
        this.producto = producto;
    }

    public BigInteger getMoneda() {
        return moneda;
    }

    public DapDto moneda(BigInteger moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(BigInteger moneda) {
        this.moneda = moneda;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public DapDto fechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public BigInteger getCorrelativo() {
        return correlativo;
    }

    public DapDto correlativo(BigInteger correlativo) {
        this.correlativo = correlativo;
        return this;
    }

    public void setCorrelativo(BigInteger correlativo) {
        this.correlativo = correlativo;
    }

    public String getFechaInv() {
        return fechaInv;
    }

    public DapDto fechaInv(String fechaInv) {
        this.fechaInv = fechaInv;
        return this;
    }

    public void setFechaInv(String fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFolio() {
        return folio;
    }

    public DapDto folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCapital() {
        return capital;
    }

    public DapDto capital(String capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getCapMon() {
        return capMon;
    }

    public DapDto capMon(String capMon) {
        this.capMon = capMon;
        return this;
    }

    public void setCapMon(String capMon) {
        this.capMon = capMon;
    }

    public String getIntNom() {
        return intNom;
    }

    public DapDto intNom(String intNom) {
        this.intNom = intNom;
        return this;
    }

    public void setIntNom(String intNom) {
        this.intNom = intNom;
    }

    public String getIntReal() {
        return intReal;
    }

    public DapDto intReal(String intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(String intReal) {
        this.intReal = intReal;
    }

    public String getMonFi() {
        return monFi;
    }

    public DapDto monFi(String monFi) {
        this.monFi = monFi;
        return this;
    }

    public void setMonFi(String monFi) {
        this.monFi = monFi;
    }

    public BigInteger getNumGiro() {
        return numGiro;
    }

    public DapDto numGiro(BigInteger numGiro) {
        this.numGiro = numGiro;
        return this;
    }

    public void setNumGiro(BigInteger numGiro) {
        this.numGiro = numGiro;
    }

    public BigInteger getCodMon() {
        return codMon;
    }

    public DapDto codMon(BigInteger codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(BigInteger codMon) {
        this.codMon = codMon;
    }

    public String getTipPlazo() {
        return tipPlazo;
    }

    public DapDto tipPlazo(String tipPlazo) {
        this.tipPlazo = tipPlazo;
        return this;
    }

    public void setTipPlazo(String tipPlazo) {
        this.tipPlazo = tipPlazo;
    }

    public String getMonTasa() {
        return monTasa;
    }

    public DapDto monTasa(String monTasa) {
        this.monTasa = monTasa;
        return this;
    }

    public void setMonTasa(String monTasa) {
        this.monTasa = monTasa;
    }

    public String getTasaOp() {
        return tasaOp;
    }

    public DapDto tasaOp(String tasaOp) {
        this.tasaOp = tasaOp;
        return this;
    }

    public void setTasaOp(String tasaOp) {
        this.tasaOp = tasaOp;
    }

    public BigInteger getCtaCap() {
        return ctaCap;
    }

    public DapDto ctaCap(BigInteger ctaCap) {
        this.ctaCap = ctaCap;
        return this;
    }

    public void setCtaCap(BigInteger ctaCap) {
        this.ctaCap = ctaCap;
    }

    public BigInteger getCtaInt() {
        return ctaInt;
    }

    public DapDto ctaInt(BigInteger ctaInt) {
        this.ctaInt = ctaInt;
        return this;
    }

    public void setCtaInt(BigInteger ctaInt) {
        this.ctaInt = ctaInt;
    }

    public BigInteger getCtaReaj() {
        return ctaReaj;
    }

    public DapDto ctaReaj(BigInteger ctaReaj) {
        this.ctaReaj = ctaReaj;
        return this;
    }

    public void setCtaReaj(BigInteger ctaReaj) {
        this.ctaReaj = ctaReaj;
    }

    public BigInteger getPerTasa() {
        return perTasa;
    }

    public DapDto perTasa(BigInteger perTasa) {
        this.perTasa = perTasa;
        return this;
    }

    public void setPerTasa(BigInteger perTasa) {
        this.perTasa = perTasa;
    }

    public String getTipoInt() {
        return tipoInt;
    }

    public DapDto tipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
        return this;
    }

    public void setTipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
    }

    public BigInteger getCodTipoInt() {
        return codTipoInt;
    }

    public DapDto codTipoInt(BigInteger codTipoInt) {
        this.codTipoInt = codTipoInt;
        return this;
    }

    public void setCodTipoInt(BigInteger codTipoInt) {
        this.codTipoInt = codTipoInt;
    }

    public String getFecVcto() {
        return fecVcto;
    }

    public DapDto fecVcto(String fecVcto) {
        this.fecVcto = fecVcto;
        return this;
    }

    public void setFecVcto(String fecVcto) {
        this.fecVcto = fecVcto;
    }

    public String getOnp() {
        return onp;
    }

    public DapDto onp(String onp) {
        this.onp = onp;
        return this;
    }

    public void setOnp(String onp) {
        this.onp = onp;
    }

    public String getEstCta() {
        return estCta;
    }

    public DapDto estCta(String estCta) {
        this.estCta = estCta;
        return this;
    }

    public void setEstCta(String estCta) {
        this.estCta = estCta;
    }

    public String getFecCont() {
        return fecCont;
    }

    public DapDto fecCont(String fecCont) {
        this.fecCont = fecCont;
        return this;
    }

    public void setFecCont(String fecCont) {
        this.fecCont = fecCont;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public DapDto bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public DapDto reajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
        return this;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public BigInteger getPlazo() {
        return plazo;
    }

    public DapDto plazo(BigInteger plazo) {
        this.plazo = plazo;
        return this;
    }

    public void setPlazo(BigInteger plazo) {
        this.plazo = plazo;
    }

    public String getIndPago() {
        return indPago;
    }

    public DapDto indPago(String indPago) {
        this.indPago = indPago;
        return this;
    }

    public void setIndPago(String indPago) {
        this.indPago = indPago;
    }

    public String getp57Bis() {
        return p57Bis;
    }

    public DapDto p57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
        return this;
    }

    public void setp57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public DapDto extranjero(String extranjero) {
        this.extranjero = extranjero;
        return this;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }

    public String getImpExt() {
        return impExt;
    }

    public DapDto impExt(String impExt) {
        this.impExt = impExt;
        return this;
    }

    public void setImpExt(String impExt) {
        this.impExt = impExt;
    }

    public BigInteger getNumCupones() {
        return numCupones;
    }

    public DapDto numCupones(BigInteger numCupones) {
        this.numCupones = numCupones;
        return this;
    }

    public void setNumCupones(BigInteger numCupones) {
        this.numCupones = numCupones;
    }

    public BigInteger getNumCuponPago() {
        return numCuponPago;
    }

    public DapDto numCuponPago(BigInteger numCuponPago) {
        this.numCuponPago = numCuponPago;
        return this;
    }

    public void setNumCuponPago(BigInteger numCuponPago) {
        this.numCuponPago = numCuponPago;
    }

    public String getOrigen() {
        return origen;
    }

    public DapDto origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public BigInteger getReajPagado() {
        return reajPagado;
    }

    public DapDto reajPagado(BigInteger reajPagado) {
        this.reajPagado = reajPagado;
        return this;
    }

    public void setReajPagado(BigInteger reajPagado) {
        this.reajPagado = reajPagado;
    }

    public String getIntPagado() {
        return intPagado;
    }

    public DapDto intPagado(String intPagado) {
        this.intPagado = intPagado;
        return this;
    }

    public void setIntPagado(String intPagado) {
        this.intPagado = intPagado;
    }

    public BigInteger getSucursal() {
        return sucursal;
    }

    public DapDto sucursal(BigInteger sucursal) {
        this.sucursal = sucursal;
        return this;
    }

    public void setSucursal(BigInteger sucursal) {
        this.sucursal = sucursal;
    }

    @Override
    public String toString() {
        return "DapDto{" + "fecVcto='" + fecVcto + '\'' + ", fechaInv='" + fechaInv + '\'' + ", fecCont='" + fecCont + '\'' + ", fechaPago='" + fechaPago + '\'' + ", capital='"
            + capital + '\'' + ", capMon='" + capMon + '\'' + ", intNom='" + intNom + '\'' + ", intReal='" + intReal + '\'' + ", monFi='" + monFi + '\'' + ", tasaOp='" + tasaOp
            + '\'' + ", intPagado='" + intPagado + '\'' + ", monTasa='" + monTasa + '\'' + ", impExt='" + impExt + '\'' + ", plazo=" + plazo + ", numCupones=" + numCupones
            + ", numCuponPago=" + numCuponPago + ", reajPagado=" + reajPagado + ", sucursal=" + sucursal + ", rut=" + rut + ", producto=" + producto + ", moneda=" + moneda
            + ", correlativo=" + correlativo + ", folio=" + folio + ", numGiro=" + numGiro + ", codMon=" + codMon + ", ctaCap=" + ctaCap + ", codTipoInt=" + codTipoInt
            + ", perTasa=" + perTasa + ", ctaReaj=" + ctaReaj + ", ctaInt=" + ctaInt + ", dv='" + dv + '\'' + ", tipPlazo='" + tipPlazo + '\'' + ", tipoInt='" + tipoInt + '\''
            + ", onp='" + onp + '\'' + ", estCta='" + estCta + '\'' + ", bipersonal='" + bipersonal + '\'' + ", reajustabilidad='" + reajustabilidad + '\'' + ", indPago='"
            + indPago + '\'' + ", p57Bis='" + p57Bis + '\'' + ", extranjero='" + extranjero + '\'' + ", origen='" + origen + '\'' + ", interesNominalPesos=" + interesNominalPesos
            + ", reajusteNegativo=" + reajusteNegativo + '}';
    }
}
