package cl.tutorial.sccm.service.dto;

/**
 * A DTO representing a LetrasBonosCuadratura Entity.
 *
 * @author Antonio Marrero Palomino.
 */
public class LetrasBonosCuadraturaDto {

    private String anno;
    private String instrumento;
    private String mes;
    private Long   moneda;
    private String montoPagado;
    private String origen;
    private String valorNominal;

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Long getMoneda() {
        return moneda;
    }

    public void setMoneda(Long moneda) {
        this.moneda = moneda;
    }

    public String getMontoPagado() {
        return montoPagado;
    }

    public void setMontoPagado(String montoPagado) {
        this.montoPagado = montoPagado;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getValorNominal() {
        return valorNominal;
    }

    public void setValorNominal(String valorNominal) {
        this.valorNominal = valorNominal;
    }

    @Override
    public String toString() {
        return "LetrasBonosCuadraturaDto{" + "anno='" + anno + '\'' + ", instrumento='" + instrumento + '\'' + ", mes='" + mes + '\'' + ", moneda=" + moneda + ", montoPagado='"
            + montoPagado + '\'' + ", origen='" + origen + '\'' + ", valorNominal='" + valorNominal + '\'' + '}';
    }
}
