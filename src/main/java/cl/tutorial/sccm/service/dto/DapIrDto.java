package cl.tutorial.sccm.service.dto;

/**
 * A DTO representing a DapIR Entity.
 *
 * @author Antonio Marrero Palomino.
 */
public class DapIrDto {

    private String  folio;
    private Long   codMon;
    private String fechaPago;
    private String intReal;
    private String intRealCal;
    private String diferencia;

    public String getFolio() {
        return folio;
    }

    public DapIrDto folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Long getCodMon() {
        return codMon;
    }

    public DapIrDto codMon(Long codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Long codMon) {
        this.codMon = codMon;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public DapIrDto fechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getIntReal() {
        return intReal;
    }

    public DapIrDto intReal(String intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(String intReal) {
        this.intReal = intReal;
    }

    public String getIntRealCal() {
        return intRealCal;
    }

    public DapIrDto intRealCal(String intRealCal) {
        this.intRealCal = intRealCal;
        return this;
    }

    public void setIntRealCal(String intRealCal) {
        this.intRealCal = intRealCal;
    }

    public String getDiferencia() {
        return diferencia;
    }

    public DapIrDto diferencia(String diferencia) {
        this.diferencia = diferencia;
        return this;
    }

    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }

    @Override
    public String toString() {
        return "DapIrDto{" + "folio=" + folio + ", codMon=" + codMon + ", fechaPago=" + fechaPago + ", intReal=" + intReal + ", intRealCal=" + intRealCal + ", diferencia="
            + diferencia + '}';
    }
}
