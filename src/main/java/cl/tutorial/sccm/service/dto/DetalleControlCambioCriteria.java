package cl.tutorial.sccm.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the DetalleControlCambio entity. This class is used in DetalleControlCambioResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /detalle-control-cambios?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DetalleControlCambioCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter nombreCampo;

    private StringFilter valorCampo;

    private StringFilter tipoCampo;

    private LongFilter detcabcontrolId;

    public DetalleControlCambioCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNombreCampo() {
        return nombreCampo;
    }

    public void setNombreCampo(StringFilter nombreCampo) {
        this.nombreCampo = nombreCampo;
    }

    public StringFilter getValorCampo() {
        return valorCampo;
    }

    public void setValorCampo(StringFilter valorCampo) {
        this.valorCampo = valorCampo;
    }

    public StringFilter getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(StringFilter tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public LongFilter getDetcabcontrolId() {
        return detcabcontrolId;
    }

    public void setDetcabcontrolId(LongFilter detcabcontrolId) {
        this.detcabcontrolId = detcabcontrolId;
    }

    @Override
    public String toString() {
        return "DetalleControlCambioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nombreCampo != null ? "nombreCampo=" + nombreCampo + ", " : "") +
                (valorCampo != null ? "valorCampo=" + valorCampo + ", " : "") +
                (tipoCampo != null ? "tipoCampo=" + tipoCampo + ", " : "") +
                (detcabcontrolId != null ? "detcabcontrolId=" + detcabcontrolId + ", " : "") +
            "}";
    }

}
