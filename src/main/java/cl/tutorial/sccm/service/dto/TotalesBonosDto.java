package cl.tutorial.sccm.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO representing a TotalesBonos Entity.
 *
 * @author Antonio Marrero Palomino.
 */
public class TotalesBonosDto {

    private String fechaEvento;
    private String fechaInversion;
    private String fechaPago;
    private String instrumento;
    private Long   monedaPago;
    private String montoPagadoMo;
    private String montoPagadoValor;
    private String nombreBeneficiario;
    private Long   numCupon;
    private String origen;
    private String rutBeneficiario;
    private Long   serie;
    private Long   tipoIrf;
    private String valorNominal;

    public String getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(String fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public String getFechaInversion() {
        return fechaInversion;
    }

    public void setFechaInversion(String fechaInversion) {
        this.fechaInversion = fechaInversion;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public Long getMonedaPago() {
        return monedaPago;
    }

    public void setMonedaPago(Long monedaPago) {
        this.monedaPago = monedaPago;
    }

    public String getMontoPagadoMo() {
        return montoPagadoMo;
    }

    public void setMontoPagadoMo(String montoPagadoMo) {
        this.montoPagadoMo = montoPagadoMo;
    }

    public String getMontoPagadoValor() {
        return montoPagadoValor;
    }

    public void setMontoPagadoValor(String montoPagadoValor) {
        this.montoPagadoValor = montoPagadoValor;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public Long getNumCupon() {
        return numCupon;
    }

    public void setNumCupon(Long numCupon) {
        this.numCupon = numCupon;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getRutBeneficiario() {
        return rutBeneficiario;
    }

    public void setRutBeneficiario(String rutBeneficiario) {
        this.rutBeneficiario = rutBeneficiario;
    }

    public Long getSerie() {
        return serie;
    }

    public void setSerie(Long serie) {
        this.serie = serie;
    }

    public Long getTipoIrf() {
        return tipoIrf;
    }

    public void setTipoIrf(Long tipoIrf) {
        this.tipoIrf = tipoIrf;
    }

    public String getValorNominal() {
        return valorNominal;
    }

    public void setValorNominal(String valorNominal) {
        this.valorNominal = valorNominal;
    }

    @Override
    public String toString() {
        return "TotalesBonosDto{" + "fechaEvento='" + fechaEvento + '\'' + ", fechaInversion='" + fechaInversion + '\'' + ", fechaPago='" + fechaPago + '\'' + ", instrumento='"
            + instrumento + '\'' + ", monedaPago=" + monedaPago + ", montoPagadoMo='" + montoPagadoMo + '\'' + ", montoPagadoValor='" + montoPagadoValor + '\''
            + ", nombreBeneficiario='" + nombreBeneficiario + '\'' + ", numCupon=" + numCupon + ", origen='" + origen + '\'' + ", rutBeneficiario='" + rutBeneficiario + '\''
            + ", serie=" + serie + ", tipoIrf=" + tipoIrf + ", valorNominal=" + valorNominal + '}';
    }
}
