package cl.tutorial.sccm.service.dto;

/**
 * A DTO representing a Ahorro Entity.
 */
public class AhorroDto {

    private Integer rut;
    private String  dv;
    private Integer producto;
    private Integer moneda;
    private String  fechaPago;
    private Integer correlativo;
    private String  fechaInv;
    private String  folio;
    private String  capital;
    private Integer monOp;
    private String  intPagado;
    private String  intReal;
    private Integer monFi;
    private Integer numGiro;
    private Integer codMon;
    private String  bipersonal;
    private String  sucursal;
    private Integer tasaOp;
    private String  nacionalidad;
    private Integer impto35;
    private String  nombre;
    private String  direccion;
    private String  comuna;
    private String  ciudad;
    private String  fechaApe;
    private String  fechaCon;
    private Integer cuentaCap;
    private Integer cuentaInt;
    private Integer cuentaReaj;
    private String  reajPagado;
    private String  fechaVen;
    private String  reajustabilidad;
    private String  estado;
    private String  p57Bis;
    private String  movInmaduros;
    private String  origen;

    public Integer getRut() {
        return rut;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getProducto() {
        return producto;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public Integer getMoneda() {
        return moneda;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Integer getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }

    public String getFechaInv() {
        return fechaInv;
    }

    public void setFechaInv(String fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public Integer getMonOp() {
        return monOp;
    }

    public void setMonOp(Integer monOp) {
        this.monOp = monOp;
    }

    public String getIntPagado() {
        return intPagado;
    }

    public void setIntPagado(String intPagado) {
        this.intPagado = intPagado;
    }

    public String getIntReal() {
        return intReal;
    }

    public void setIntReal(String intReal) {
        this.intReal = intReal;
    }

    public Integer getMonFi() {
        return monFi;
    }

    public void setMonFi(Integer monFi) {
        this.monFi = monFi;
    }

    public Integer getNumGiro() {
        return numGiro;
    }

    public void setNumGiro(Integer numGiro) {
        this.numGiro = numGiro;
    }

    public Integer getCodMon() {
        return codMon;
    }

    public void setCodMon(Integer codMon) {
        this.codMon = codMon;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public Integer getTasaOp() {
        return tasaOp;
    }

    public void setTasaOp(Integer tasaOp) {
        this.tasaOp = tasaOp;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Integer getImpto35() {
        return impto35;
    }

    public void setImpto35(Integer impto35) {
        this.impto35 = impto35;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getFechaApe() {
        return fechaApe;
    }

    public void setFechaApe(String fechaApe) {
        this.fechaApe = fechaApe;
    }

    public String getFechaCon() {
        return fechaCon;
    }

    public void setFechaCon(String fechaCon) {
        this.fechaCon = fechaCon;
    }

    public Integer getCuentaCap() {
        return cuentaCap;
    }

    public void setCuentaCap(Integer cuentaCap) {
        this.cuentaCap = cuentaCap;
    }

    public Integer getCuentaInt() {
        return cuentaInt;
    }

    public void setCuentaInt(Integer cuentaInt) {
        this.cuentaInt = cuentaInt;
    }

    public Integer getCuentaReaj() {
        return cuentaReaj;
    }

    public void setCuentaReaj(Integer cuentaReaj) {
        this.cuentaReaj = cuentaReaj;
    }

    public String getReajPagado() {
        return reajPagado;
    }

    public void setReajPagado(String reajPagado) {
        this.reajPagado = reajPagado;
    }

    public String getFechaVen() {
        return fechaVen;
    }

    public void setFechaVen(String fechaVen) {
        this.fechaVen = fechaVen;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getP57Bis() {
        return p57Bis;
    }

    public void setP57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
    }

    public String getMovInmaduros() {
        return movInmaduros;
    }

    public void setMovInmaduros(String movInmaduros) {
        this.movInmaduros = movInmaduros;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Override
    public String toString() {
        return "AhorroDto{" + "rut=" + rut + ", dv='" + dv + '\'' + ", producto=" + producto + ", moneda=" + moneda + ", fechaPago='" + fechaPago + '\'' + ", correlativo="
            + correlativo + ", fechaInv='" + fechaInv + '\'' + ", folio=" + folio + ", capital='" + capital + '\'' + ", monOp=" + monOp + ", intPagado='" + intPagado + '\''
            + ", intReal='" + intReal + '\'' + ", monFi=" + monFi + ", numGiro=" + numGiro + ", codMon=" + codMon + ", bipersonal='" + bipersonal + '\'' + ", sucursal='" + sucursal
            + '\'' + ", tasaOp=" + tasaOp + ", nacionalidad='" + nacionalidad + '\'' + ", impto35=" + impto35 + ", nombre='" + nombre + '\'' + ", direccion='" + direccion + '\''
            + ", comuna='" + comuna + '\'' + ", ciudad='" + ciudad + '\'' + ", fechaApe='" + fechaApe + '\'' + ", fechaCon='" + fechaCon + '\'' + ", cuentaCap=" + cuentaCap
            + ", cuentaInt=" + cuentaInt + ", cuentaReaj=" + cuentaReaj + ", reajPagado=" + reajPagado + ", fechaVen='" + fechaVen + '\'' + ", reajustabilidad='" + reajustabilidad
            + '\'' + ", estado='" + estado + '\'' + ", p57Bis='" + p57Bis + '\'' + ", movInmaduros='" + movInmaduros + '\'' + ", origen='" + origen + '\'' + '}';
    }
}
