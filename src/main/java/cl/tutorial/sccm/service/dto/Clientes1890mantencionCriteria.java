package cl.tutorial.sccm.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Clientes1890mantencion entity. This class is used in Clientes1890mantencionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /clientes-1890-mantencions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class Clientes1890mantencionCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter rut;

    private StringFilter nombre;

    private StringFilter nacionalidad;

    private StringFilter impto35;

    private StringFilter direccion;

    private StringFilter comuna;

    private StringFilter ciudad;

    public Clientes1890mantencionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRut() {
        return rut;
    }

    public void setRut(IntegerFilter rut) {
        this.rut = rut;
    }

    public StringFilter getNombre() {
        return nombre;
    }

    public void setNombre(StringFilter nombre) {
        this.nombre = nombre;
    }

    public StringFilter getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(StringFilter nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public StringFilter getImpto35() {
        return impto35;
    }

    public void setImpto35(StringFilter impto35) {
        this.impto35 = impto35;
    }

    public StringFilter getDireccion() {
        return direccion;
    }

    public void setDireccion(StringFilter direccion) {
        this.direccion = direccion;
    }

    public StringFilter getComuna() {
        return comuna;
    }

    public void setComuna(StringFilter comuna) {
        this.comuna = comuna;
    }

    public StringFilter getCiudad() {
        return ciudad;
    }

    public void setCiudad(StringFilter ciudad) {
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return "Clientes1890mantencionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (rut != null ? "rut=" + rut + ", " : "") +
                (nombre != null ? "nombre=" + nombre + ", " : "") +
                (nacionalidad != null ? "nacionalidad=" + nacionalidad + ", " : "") +
                (impto35 != null ? "impto35=" + impto35 + ", " : "") +
                (direccion != null ? "direccion=" + direccion + ", " : "") +
                (comuna != null ? "comuna=" + comuna + ", " : "") +
                (ciudad != null ? "ciudad=" + ciudad + ", " : "") +
            "}";
    }

}
