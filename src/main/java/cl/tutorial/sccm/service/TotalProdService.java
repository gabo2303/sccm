package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.TotalProd;
import java.util.List;

/**
 * Service Interface for managing TotalProd.
 */
public interface TotalProdService {

    /**
     * Save a totalProd.
     *
     * @param totalProd the entity to save
     * @return the persisted entity
     */
    TotalProd save(TotalProd totalProd);

    /**
     *  Get all the totalProds.
     *
     *  @return the list of entities
     */
    List<TotalProd> findAll();

    /**
     *  Get the "id" totalProd.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TotalProd findOne(Long id);

    /**
     *  Delete the "id" totalProd.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
