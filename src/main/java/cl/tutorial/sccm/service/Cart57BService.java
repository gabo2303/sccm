package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Cart57B;
import java.util.List;

/**
 * Service Interface for managing Cart57B.
 */
public interface Cart57BService {

    /**
     * Save a cart57B.
     *
     * @param cart57B the entity to save
     * @return the persisted entity
     */
    Cart57B save(Cart57B cart57B);

    /**
     *  Get all the cart57BS.
     *
     *  @return the list of entities
     */
    List<Cart57B> findAll();

    /**
     *  Get the "id" cart57B.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Cart57B findOne(Long id);

    /**
     *  Delete the "id" cart57B.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<Cart57B> findAllByAnnoTributaId(Long annoId);
}
