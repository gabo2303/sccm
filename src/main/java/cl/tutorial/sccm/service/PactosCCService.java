package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.PactosCC;
import cl.tutorial.sccm.domain.PactosIR;

import java.util.List;

/**
 * Service Interface for managing Pactos.
 */
public interface PactosCCService {

        List<PactosCC> findAllByAnnoTributa(Long annoId);
}
