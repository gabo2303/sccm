package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Uf;
import java.util.List;

/**
 * Service Interface for managing Uf.
 */
public interface UfService {

    /**
     * Save a uf.
     *
     * @param uf the entity to save
     * @return the persisted entity
     */
    Uf save(Uf uf);

    /**
     *  Get all the ufs.
     *
     *  @return the list of entities
     */
    List<Uf> findAll();

    /**
     *  Get the "id" uf.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Uf findOne(Long id);

    /**
     *  Delete the "id" uf.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
