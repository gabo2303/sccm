package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Cert57B;
import java.util.List;

/**
 * Service Interface for managing Cert57B.
 */
public interface Cert57BService {

    /**
     * Save a cert57B.
     *
     * @param cert57B the entity to save
     * @return the persisted entity
     */
    Cert57B save(Cert57B cert57B);

    /**
     *  Get all the cert57BS.
     *
     *  @return the list of entities
     */
    List<Cert57B> findAll();

    /**
     *  Get the "id" cert57B.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Cert57B findOne(Long id);

    /**
     *  Delete the "id" cert57B.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<Cert57B> findAllByAnnoTributaId(Long annoId);
}
