package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Fut;
import java.util.List;

/**
 * Service Interface for managing Fut.
 */
public interface FutService {

    /**
     * Save a fut.
     *
     * @param fut the entity to save
     * @return the persisted entity
     */
    Fut save(Fut fut);

    /**
     *  Get all the futs.
     *
     *  @return the list of entities
     */
    List<Fut> findAll();

    /**
     *  Get the "id" fut.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Fut findOne(Long id);

    /**
     *  Delete the "id" fut.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    List<Fut> findAllByAnnoTributaId(Long annoId);
}
