package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.AhorroMov;
import java.util.List;

/**
 * Service Interface for managing AhorroMov.
 */
public interface AhorroMovService {

    /**
     * Save a ahorroMov.
     *
     * @param ahorroMov the entity to save
     * @return the persisted entity
     */
    AhorroMov save(AhorroMov ahorroMov);

    /**
     *  Get all the ahorroMovs.
     *
     *  @return the list of entities
     */
    List<AhorroMov> findAll();

    /**
     *  Get the "id" ahorroMov.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AhorroMov findOne(Long id);

    /**
     *  Delete the "id" ahorroMov.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
