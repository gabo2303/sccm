package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.SalProxPer;
import java.util.List;

/**
 * Service Interface for managing SalProxPer.
 */
public interface SalProxPerService {

    /**
     * Save a salProxPer.
     *
     * @param salProxPer the entity to save
     * @return the persisted entity
     */
    SalProxPer save(SalProxPer salProxPer);

    /**
     *  Get all the salProxPers.
     *
     *  @return the list of entities
     */
    List<SalProxPer> findAll();

    /**
     *  Get the "id" salProxPer.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SalProxPer findOne(Long id);

    /**
     *  Delete the "id" salProxPer.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
