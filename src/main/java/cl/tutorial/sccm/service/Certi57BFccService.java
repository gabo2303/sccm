package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.Certi57BFcc;
import java.util.List;

/**
 * Service Interface for managing Certi57BFcc.
 */
public interface Certi57BFccService {

    /**
     * Save a certi57BFcc.
     *
     * @param certi57BFcc the entity to save
     * @return the persisted entity
     */
    Certi57BFcc save(Certi57BFcc certi57BFcc);

    /**
     *  Get all the certi57BFccs.
     *
     *  @return the list of entities
     */
    List<Certi57BFcc> findAll();

    /**
     *  Get the "id" certi57BFcc.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Certi57BFcc findOne(Long id);

    /**
     *  Delete the "id" certi57BFcc.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
