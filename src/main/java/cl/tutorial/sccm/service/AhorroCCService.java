package cl.tutorial.sccm.service;

import cl.tutorial.sccm.domain.AhorroCC;
import cl.tutorial.sccm.domain.PactosCC;

import java.util.List;

/**
 * Service Interface for managing Ahorro.
 */
public interface AhorroCCService {

        List<AhorroCC> findAllByAnnoTributa(Long annoId);
}
