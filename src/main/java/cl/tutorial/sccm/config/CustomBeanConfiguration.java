package cl.tutorial.sccm.config;

import cl.tutorial.sccm.repository.CustomDapRepository;
import cl.tutorial.sccm.repository.impl.CustomDapRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

@Configuration
public class CustomBeanConfiguration {

    private final Logger log = LoggerFactory.getLogger(CustomBeanConfiguration.class.getName());

    @PostConstruct
    protected void init() {
        log.info("¡SE INICIALIZÓ LA CLASE CustomBeanConfiguration CORRECTAMENTE MDFK!");
    }

    @Bean
    public CustomDapRepository getCustomDapRepository(EntityManager entityManager) {
        return new CustomDapRepositoryImpl(entityManager);
    }
}
