package cl.tutorial.sccm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author Maikel Eduardo Suarez Torres
 * @author Antonio Marrero Palomino
 */
@Configuration
@ConfigurationProperties(prefix = "sccm.etl")
public class ETLConfiguration 
{
    private static Logger log = LoggerFactory.getLogger(ETLConfiguration.class.getName());

    private String etlmailServerHost;
    private Integer etlmailServerPort;
    private String etlmailServerUser;
    private String etlmailServerPassword;
    private Boolean etlmailServerUseSSL;
    private Boolean etlmailServerUseTLS;
    private String etlmailServerFrom;
    private String etlmailServerFromName;
    private String etlmailServerTo;
    private String etlmailServerSubject;
    private String etlmailServerText;

    private String etldatabaseServerUser;
    private String etldatabaseServerPassword;
    private String etldatabaseServerHost;
    private String etldatabaseServerPort;
    private String etldatabaseServerSid;

    private String etlLdapUrl;
    private String etlLdapBase;
    private String etlLdapBindDN;
    private String etlLdapPassword;
    private String etldatabaseSchema;

    private String etlExecutorPathLoadInterfacesAccountingWin;
    private String etlExecutorPathLoadInterfacesAccountingLin;
    private String etlExecutorPathLoadInterfacesEuroWin;
    private String etlExecutorPathLoadInterfacesEuroLin;
    private String etlExecutorPathLoadInterfacesParityWin;
    private String etlExecutorPathLoadInterfacesParityLin;
    private String etlExecutorPathLoadInterfacesUfWin;
    private String etlExecutorPathLoadInterfacesUfLin;
    private String etlExecutorPathLoadInterfacesUsdWin;
    private String etlExecutorPathLoadInterfacesUsdLin;

    private String etlExecutorPathBonosLetrasLoadBonosWin;
    private String etlExecutorPathBonosLetrasLoadBonosLin;
    private String etlExecutorPathBonosLetrasLoadLhbonosWin;
    private String etlExecutorPathBonosLetrasLoadLhbonosLin;
    private String etlExecutorPathBonosLetrasLoadLhdcvWin;
    private String etlExecutorPathBonosLetrasLoadLhdcvLin;

    private String etlExecutorPathDapWin;
    private String etlExecutorPathDapLin;
    private String etlExecutorPathPactosWin;
    private String etlExecutorPathPactosLin;
    private String etlExecutorPathAhorrosWin;
    private String etlExecutorPathAhorrosLin;
    private String etlExecutorPathAhorrosMovimientosWin;
    private String etlExecutorPathAhorrosMovimientosLin;
	
	private String etlExecutorPathCliente1890Win;
    private String etlExecutorPathCliente1890Lin;
	private String etlExecutorPathRut1890Win;
    private String etlExecutorPathRut1890Lin;

    @PostConstruct
    protected void init() {
        this.cargaAnnosTributarios();
        log.info("SE INICIALIZÓ LA CLASE SCCMConfiguration CORRECTAMENTE!!");
    }

    public String getEtlExecutorPathAhorrosMovimientosWin() {
        return etlExecutorPathAhorrosMovimientosWin;
    }

    public void setEtlExecutorPathAhorrosMovimientosWin(String etlExecutorPathAhorrosMovimientosWin) {
        this.etlExecutorPathAhorrosMovimientosWin = etlExecutorPathAhorrosMovimientosWin;
    }

    public String getEtlExecutorPathAhorrosMovimientosLin() {
        return etlExecutorPathAhorrosMovimientosLin;
    }

    public void setEtlExecutorPathAhorrosMovimientosLin(String etlExecutorPathAhorrosMovimientosLin) {
        this.etlExecutorPathAhorrosMovimientosLin = etlExecutorPathAhorrosMovimientosLin;
    }

    public String getEtlExecutorPathPactosWin() {
        return etlExecutorPathPactosWin;
    }

    public void setEtlExecutorPathPactosWin(String etlExecutorPathPactosWin) {
        this.etlExecutorPathPactosWin = etlExecutorPathPactosWin;
    }

    public String getEtlExecutorPathPactosLin() {
        return etlExecutorPathPactosLin;
    }

    public void setEtlExecutorPathPactosLin(String etlExecutorPathPactosLin) {
        this.etlExecutorPathPactosLin = etlExecutorPathPactosLin;
    }

    public String getEtlExecutorPathDapWin() {
        return etlExecutorPathDapWin;
    }

    public void setEtlExecutorPathDapWin(String etlExecutorPathDapWin) {
        this.etlExecutorPathDapWin = etlExecutorPathDapWin;
    }

    public String getEtlExecutorPathDapLin() {
        return etlExecutorPathDapLin;
    }

    public void setEtlExecutorPathDapLin(String etlExecutorPathDapLin) {
        this.etlExecutorPathDapLin = etlExecutorPathDapLin;
    }

    public String getEtlmailServerHost() {
        return etlmailServerHost;
    }

    public void setEtlmailServerHost(String etlmailServerHost) {
        this.etlmailServerHost = etlmailServerHost;
    }

    public Integer getEtlmailServerPort() {
        return etlmailServerPort;
    }

    public void setEtlmailServerPort(Integer etlmailServerPort) {
        this.etlmailServerPort = etlmailServerPort;
    }

    public String getEtlmailServerUser() {
        return etlmailServerUser;
    }

    public void setEtlmailServerUser(String etlmailServerUser) {
        this.etlmailServerUser = etlmailServerUser;
    }

    public String getEtlmailServerPassword() {
        return etlmailServerPassword;
    }

    public String getEtldatabaseSchema() {
        return etldatabaseSchema;
    }

    public void setEtlmailServerPassword(String etlmailServerPassword) {
        this.etlmailServerPassword = etlmailServerPassword;
    }

    public Boolean getEtlmailServerUseSSL() {
        return etlmailServerUseSSL;
    }

    public void setEtlmailServerUseSSL(Boolean etlmailServerUseSSL) {
        this.etlmailServerUseSSL = etlmailServerUseSSL;
    }

    public Boolean getEtlmailServerUseTLS() {
        return etlmailServerUseTLS;
    }

    public void setEtlmailServerUseTLS(Boolean etlmailServerUseTLS) {
        this.etlmailServerUseTLS = etlmailServerUseTLS;
    }

    public String getEtlmailServerFrom() {
        return etlmailServerFrom;
    }

    public void setEtlmailServerFrom(String etlmailServerFrom) {
        this.etlmailServerFrom = etlmailServerFrom;
    }

    public String getEtlmailServerTo() {
        return etlmailServerTo;
    }

    public void setEtlmailServerTo(String etlmailServerTo) {
        this.etlmailServerTo = etlmailServerTo;
    }

    public String getEtlmailServerSubject() {
        return etlmailServerSubject;
    }

    public void setEtlmailServerSubject(String etlmailServerSubject) {
        this.etlmailServerSubject = etlmailServerSubject;
    }

    public String getEtlmailServerText() {
        return etlmailServerText;
    }

    public void setEtlmailServerText(String etlmailServerText) {
        this.etlmailServerText = etlmailServerText;
    }

    public String getEtldatabaseServerUser() {
        return etldatabaseServerUser;
    }

    public void setEtldatabaseServerUser(String etldatabaseServerUser) {
        this.etldatabaseServerUser = etldatabaseServerUser;
    }

    public String getEtldatabaseServerPassword() {
        return etldatabaseServerPassword;
    }

    public void setEtldatabaseServerPassword(String etldatabaseServerPassword) {
        this.etldatabaseServerPassword = etldatabaseServerPassword;
    }

    public String getEtldatabaseServerHost() {
        return etldatabaseServerHost;
    }

    public void setEtldatabaseServerHost(String etldatabaseServerHost) {
        this.etldatabaseServerHost = etldatabaseServerHost;
    }

    public String getEtldatabaseServerPort() {
        return etldatabaseServerPort;
    }

    public void setEtldatabaseServerPort(String etldatabaseServerPort) {
        this.etldatabaseServerPort = etldatabaseServerPort;
    }

    public String getEtldatabaseServerSid() {
        return etldatabaseServerSid;
    }

    public void setEtldatabaseServerSid(String etldatabaseServerSid) {
        this.etldatabaseServerSid = etldatabaseServerSid;
    }

    public String getEtlmailServerFromName() {
        return etlmailServerFromName;
    }

    public void setEtlmailServerFromName(String etlmailServerFromName) {
        this.etlmailServerFromName = etlmailServerFromName;
    }

    public String getEtlLdapUrl() {
        return etlLdapUrl;
    }

    public void setEtlLdapUrl(String etlLdapUrl) {
        this.etlLdapUrl = etlLdapUrl;
    }

    public String getEtlLdapBase() {
        return etlLdapBase;
    }

    public void setEtlLdapBase(String etlLdapBase) {
        this.etlLdapBase = etlLdapBase;
    }

    public String getEtlLdapBindDN() {
        return etlLdapBindDN;
    }

    public void setEtlLdapBindDN(String etlLdapBindDN) {
        this.etlLdapBindDN = etlLdapBindDN;
    }

    public String getEtlLdapPassword() {
        return etlLdapPassword;
    }

    public void setEtlLdapPassword(String etlLdapPassword) {
        this.etlLdapPassword = etlLdapPassword;
    }

    public void setEtldatabaseSchema(String etldatabaseSchema) {
        this.etldatabaseSchema = etldatabaseSchema;
    }

    public String getEtlExecutorPathLoadInterfacesAccountingWin() {
        return etlExecutorPathLoadInterfacesAccountingWin;
    }

    public void setEtlExecutorPathLoadInterfacesAccountingWin(String etlExecutorPathLoadInterfacesAccountingWin) {
        this.etlExecutorPathLoadInterfacesAccountingWin = etlExecutorPathLoadInterfacesAccountingWin;
    }

    public String getEtlExecutorPathLoadInterfacesAccountingLin() {
        return etlExecutorPathLoadInterfacesAccountingLin;
    }

    public void setEtlExecutorPathLoadInterfacesAccountingLin(String etlExecutorPathLoadInterfacesAccountingLin) {
        this.etlExecutorPathLoadInterfacesAccountingLin = etlExecutorPathLoadInterfacesAccountingLin;
    }

    public String getEtlExecutorPathLoadInterfacesEuroWin() {
        return etlExecutorPathLoadInterfacesEuroWin;
    }

    public void setEtlExecutorPathLoadInterfacesEuroWin(String etlExecutorPathLoadInterfacesEuroWin) {
        this.etlExecutorPathLoadInterfacesEuroWin = etlExecutorPathLoadInterfacesEuroWin;
    }

    public String getEtlExecutorPathLoadInterfacesEuroLin() {
        return etlExecutorPathLoadInterfacesEuroLin;
    }

    public void setEtlExecutorPathLoadInterfacesEuroLin(String etlExecutorPathLoadInterfacesEuroLin) {
        this.etlExecutorPathLoadInterfacesEuroLin = etlExecutorPathLoadInterfacesEuroLin;
    }

    public String getEtlExecutorPathLoadInterfacesParityWin() {
        return etlExecutorPathLoadInterfacesParityWin;
    }

    public void setEtlExecutorPathLoadInterfacesParityWin(String etlExecutorPathLoadInterfacesParityWin) {
        this.etlExecutorPathLoadInterfacesParityWin = etlExecutorPathLoadInterfacesParityWin;
    }

    public String getEtlExecutorPathLoadInterfacesParityLin() {
        return etlExecutorPathLoadInterfacesParityLin;
    }

    public void setEtlExecutorPathLoadInterfacesParityLin(String etlExecutorPathLoadInterfacesParityLin) {
        this.etlExecutorPathLoadInterfacesParityLin = etlExecutorPathLoadInterfacesParityLin;
    }

    public String getEtlExecutorPathLoadInterfacesUfWin() {
        return etlExecutorPathLoadInterfacesUfWin;
    }

    public void setEtlExecutorPathLoadInterfacesUfWin(String etlExecutorPathLoadInterfacesUfWin) {
        this.etlExecutorPathLoadInterfacesUfWin = etlExecutorPathLoadInterfacesUfWin;
    }

    public String getEtlExecutorPathLoadInterfacesUfLin() {
        return etlExecutorPathLoadInterfacesUfLin;
    }

    public void setEtlExecutorPathLoadInterfacesUfLin(String etlExecutorPathLoadInterfacesUfLin) {
        this.etlExecutorPathLoadInterfacesUfLin = etlExecutorPathLoadInterfacesUfLin;
    }

    public String getEtlExecutorPathLoadInterfacesUsdWin() {
        return etlExecutorPathLoadInterfacesUsdWin;
    }

    public void setEtlExecutorPathLoadInterfacesUsdWin(String etlExecutorPathLoadInterfacesUsdWin) {
        this.etlExecutorPathLoadInterfacesUsdWin = etlExecutorPathLoadInterfacesUsdWin;
    }

    public String getEtlExecutorPathLoadInterfacesUsdLin() {
        return etlExecutorPathLoadInterfacesUsdLin;
    }

    public void setEtlExecutorPathLoadInterfacesUsdLin(String etlExecutorPathLoadInterfacesUsdLin) {
        this.etlExecutorPathLoadInterfacesUsdLin = etlExecutorPathLoadInterfacesUsdLin;
    }

    public void cargaAnnosTributarios() {
        log.debug("Se insertaron años tributarios correctamente!");
    }

    public String getEtlExecutorPathBonosLetrasLoadBonosWin() {
        return etlExecutorPathBonosLetrasLoadBonosWin;
    }

    public void setEtlExecutorPathBonosLetrasLoadBonosWin(String etlExecutorPathBonosLetrasLoadBonosWin) {
        this.etlExecutorPathBonosLetrasLoadBonosWin = etlExecutorPathBonosLetrasLoadBonosWin;
    }

    public String getEtlExecutorPathBonosLetrasLoadBonosLin() {
        return etlExecutorPathBonosLetrasLoadBonosLin;
    }

    public void setEtlExecutorPathBonosLetrasLoadBonosLin(String etlExecutorPathBonosLetrasLoadBonosLin) {
        this.etlExecutorPathBonosLetrasLoadBonosLin = etlExecutorPathBonosLetrasLoadBonosLin;
    }

    public String getEtlExecutorPathBonosLetrasLoadLhbonosWin() {
        return etlExecutorPathBonosLetrasLoadLhbonosWin;
    }

    public void setEtlExecutorPathBonosLetrasLoadLhbonosWin(String etlExecutorPathBonosLetrasLoadLhbonosWin) {
        this.etlExecutorPathBonosLetrasLoadLhbonosWin = etlExecutorPathBonosLetrasLoadLhbonosWin;
    }

    public String getEtlExecutorPathBonosLetrasLoadLhbonosLin() {
        return etlExecutorPathBonosLetrasLoadLhbonosLin;
    }

    public void setEtlExecutorPathBonosLetrasLoadLhbonosLin(String etlExecutorPathBonosLetrasLoadLhbonosLin) {
        this.etlExecutorPathBonosLetrasLoadLhbonosLin = etlExecutorPathBonosLetrasLoadLhbonosLin;
    }

    public String getEtlExecutorPathBonosLetrasLoadLhdcvWin() {
        return etlExecutorPathBonosLetrasLoadLhdcvWin;
    }

    public void setEtlExecutorPathBonosLetrasLoadLhdcvWin(String etlExecutorPathBonosLetrasLoadLhdcvWin) {
        this.etlExecutorPathBonosLetrasLoadLhdcvWin = etlExecutorPathBonosLetrasLoadLhdcvWin;
    }

    public String getEtlExecutorPathBonosLetrasLoadLhdcvLin() {
        return etlExecutorPathBonosLetrasLoadLhdcvLin;
    }

    public void setEtlExecutorPathBonosLetrasLoadLhdcvLin(String etlExecutorPathBonosLetrasLoadLhdcvLin) {
        this.etlExecutorPathBonosLetrasLoadLhdcvLin = etlExecutorPathBonosLetrasLoadLhdcvLin;
    }

    public String getEtlExecutorPathAhorrosWin() {
        return etlExecutorPathAhorrosWin;
    }

    public void setEtlExecutorPathAhorrosWin(String etlExecutorPathAhorrosWin) {
        this.etlExecutorPathAhorrosWin = etlExecutorPathAhorrosWin;
    }

    public String getEtlExecutorPathAhorrosLin() {
        return etlExecutorPathAhorrosLin;
    }

    public void setEtlExecutorPathAhorrosLin(String etlExecutorPathAhorrosLin) {
        this.etlExecutorPathAhorrosLin = etlExecutorPathAhorrosLin;
    }
	
	public String getEtlExecutorPathCliente1890Win() {
        return etlExecutorPathCliente1890Win;
    }

    public void setEtlExecutorPathCliente1890Win(String etlExecutorPathCliente1890Win) {
        this.etlExecutorPathCliente1890Win = etlExecutorPathCliente1890Win;
    }

    public String getEtlExecutorPathCliente1890Lin() {
        return etlExecutorPathCliente1890Lin;
    }

    public void setEtlExecutorPathCliente1890Lin(String etlExecutorPathCliente1890Lin) {
        this.etlExecutorPathCliente1890Lin = etlExecutorPathCliente1890Lin;
    }
	
	public String getEtlExecutorPathRut1890Win() {
        return etlExecutorPathRut1890Win;
    }

    public void setEtlExecutorPathRut1890Win(String etlExecutorPathRut1890Win) {
        this.etlExecutorPathRut1890Win = etlExecutorPathRut1890Win;
    }

    public String getEtlExecutorPathRut1890Lin() {
        return etlExecutorPathRut1890Lin;
    }

    public void setEtlExecutorPathRut1890Lin(String etlExecutorPathRut1890Lin) {
        this.etlExecutorPathRut1890Lin = etlExecutorPathRut1890Lin;
    }

    @Override
    public String toString() {
        return "ETLConfiguration{" +
            "etlmailServerHost='" + etlmailServerHost + '\'' +
            ", etlmailServerPort=" + etlmailServerPort +
            ", etlmailServerUser='" + etlmailServerUser + '\'' +
            ", etlmailServerPassword='" + etlmailServerPassword + '\'' +
            ", etlmailServerUseSSL=" + etlmailServerUseSSL +
            ", etlmailServerUseTLS=" + etlmailServerUseTLS +
            ", etlmailServerFrom='" + etlmailServerFrom + '\'' +
            ", etlmailServerFromName='" + etlmailServerFromName + '\'' +
            ", etlmailServerTo='" + etlmailServerTo + '\'' +
            ", etlmailServerSubject='" + etlmailServerSubject + '\'' +
            ", etlmailServerText='" + etlmailServerText + '\'' +
            ", etldatabaseServerUser='" + etldatabaseServerUser + '\'' +
            ", etldatabaseServerPassword='" + etldatabaseServerPassword + '\'' +
            ", etldatabaseServerHost='" + etldatabaseServerHost + '\'' +
            ", etldatabaseServerPort='" + etldatabaseServerPort + '\'' +
            ", etldatabaseServerSid='" + etldatabaseServerSid + '\'' +
            ", etlLdapUrl='" + etlLdapUrl + '\'' +
            ", etlLdapBase='" + etlLdapBase + '\'' +
            ", etlLdapBindDN='" + etlLdapBindDN + '\'' +
            ", etlLdapPassword='" + etlLdapPassword + '\'' +
            ", etldatabaseSchema='" + etldatabaseSchema + '\'' +
            ", etlExecutorPathLoadInterfacesAccountingWin='" + etlExecutorPathLoadInterfacesAccountingWin + '\'' +
            ", etlExecutorPathLoadInterfacesAccountingLin='" + etlExecutorPathLoadInterfacesAccountingLin + '\'' +
            ", etlExecutorPathLoadInterfacesEuroWin='" + etlExecutorPathLoadInterfacesEuroWin + '\'' +
            ", etlExecutorPathLoadInterfacesEuroLin='" + etlExecutorPathLoadInterfacesEuroLin + '\'' +
            ", etlExecutorPathLoadInterfacesParityWin='" + etlExecutorPathLoadInterfacesParityWin + '\'' +
            ", etlExecutorPathLoadInterfacesParityLin='" + etlExecutorPathLoadInterfacesParityLin + '\'' +
            ", etlExecutorPathLoadInterfacesUfWin='" + etlExecutorPathLoadInterfacesUfWin + '\'' +
            ", etlExecutorPathLoadInterfacesUfLin='" + etlExecutorPathLoadInterfacesUfLin + '\'' +
            ", etlExecutorPathLoadInterfacesUsdWin='" + etlExecutorPathLoadInterfacesUsdWin + '\'' +
            ", etlExecutorPathLoadInterfacesUsdLin='" + etlExecutorPathLoadInterfacesUsdLin + '\'' +
            ", etlExecutorPathBonosLetrasLoadBonosWin='" + etlExecutorPathBonosLetrasLoadBonosWin + '\'' +
            ", etlExecutorPathBonosLetrasLoadBonosLin='" + etlExecutorPathBonosLetrasLoadBonosLin + '\'' +
            ", etlExecutorPathBonosLetrasLoadLhbonosWin='" + etlExecutorPathBonosLetrasLoadLhbonosWin + '\'' +
            ", etlExecutorPathBonosLetrasLoadLhbonosLin='" + etlExecutorPathBonosLetrasLoadLhbonosLin + '\'' +
            ", etlExecutorPathBonosLetrasLoadLhdcvWin='" + etlExecutorPathBonosLetrasLoadLhdcvWin + '\'' +
            ", etlExecutorPathBonosLetrasLoadLhdcvLin='" + etlExecutorPathBonosLetrasLoadLhdcvLin + '\'' +
            ", etlExecutorPathDapWin='" + etlExecutorPathDapWin + '\'' +
            ", etlExecutorPathDapLin='" + etlExecutorPathDapLin + '\'' +
            ", etlExecutorPathPactosWin='" + etlExecutorPathPactosWin + '\'' +
            ", etlExecutorPathPactosLin='" + etlExecutorPathPactosLin + '\'' +
            ", etlExecutorPathAhorrosWin='" + etlExecutorPathAhorrosWin + '\'' +
            ", etlExecutorPathAhorrosLin='" + etlExecutorPathAhorrosLin + '\'' +
            ", etlExecutorPathAhorrosMovimientosWin='" + etlExecutorPathAhorrosMovimientosWin + '\'' +
            ", etlExecutorPathAhorrosMovimientosLin='" + etlExecutorPathAhorrosMovimientosLin + '\'' +
            '}';
    }
}
