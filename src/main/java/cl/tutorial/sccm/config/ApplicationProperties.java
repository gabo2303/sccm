package cl.tutorial.sccm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to SCCM.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    String rutBice;

    public String getRutBice() {
        return rutBice;
    }

    public void setRutBice(String rutBice) {
        this.rutBice = rutBice;
    }
}
