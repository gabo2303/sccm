package cl.tutorial.sccm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author Antonio Marrero Palomino
 */
@Configuration
@ConfigurationProperties(prefix = "sccm.jasper")
public class JasperConfiguration 
{
    private static Logger log = LoggerFactory.getLogger(JasperConfiguration.class.getName());

    private String certOperCaptacionJarPath;
    private String certOperCaptacionJarName;
    private String certOperCaptacionExFileNameWin;
    private String certOperCaptacionDestinationPathWin;
	private String certOperCaptacionExFileNameLin;
    private String certOperCaptacionDestinationPathLin;
	
    private String declJurOperCaptacionJarPath;
    private String declJurOperCaptacionJarName;
    private String declJurOperCaptacionExFileNameWin;
    private String declJurOperCaptacionDestinationPathWin;
	private String declJurOperCaptacionExFileNameLin;
    private String declJurOperCaptacionDestinationPathLin;
	
    private String rectifOperCaptacionJarPath;
    private String rectifOperCaptacionJarName;
    private String rectifOperCaptacionExFileNameWin;
    private String rectifOperCaptacionDestinationPathWin;
	private String rectifOperCaptacionExFileNameLin;
    private String rectifOperCaptacionDestinationPathLin;
	
    private String certPagoDvidendosJarPath;
    private String certPagoDvidendosJarName;
    private String certPagoDvidendosExFileNameWin;
    private String certPagoDvidendosDestinationPathWin;
	private String certPagoDvidendosExFileNameLin;
    private String certPagoDvidendosDestinationPathLin;
	
    private String declJurPagoDvidendosJarPath;
    private String declJurPagoDvidendosJarName;
    private String declJurPagoDvidendosExFileNameWin;
    private String declJurPagoDvidendosDestinationPathWin;
	private String declJurPagoDvidendosExFileNameLin;
    private String declJurPagoDvidendosDestinationPathLin;
	
    private String certInversiones57BisJarPath;
    private String certInversiones57BisJarName;
    private String certInversiones57BisExFileNameWin;
	private String certInversiones57BisDestinationPathWin;
	private String certInversiones57BisExFileNameLin;
	private String certInversiones57BisDestinationPathLin;
    
	
    private String declJurInversiones57BisJarPath;
    private String declJurInversiones57BisJarName;
    private String declJurInversiones57BisExFileNameWin;
	private String declJurInversiones57BisExFileNameLin;
    private String declJurInversiones57BisDestinationPathWin;
    private String declJurInversiones57BisDestinationPathLin;

    private String errorMessageJarExecution;
    private String startErrorMessageJarExecution;
    private String connectionErrorMessageJarExecution;

    @PostConstruct
    protected void init() { log.info("SE INICIALIZA LA CLASE JasperConfiguration CORRECTAMENTE!!"); }

    public String getRectifOperCaptacionJarPath() { return rectifOperCaptacionJarPath; }

    public void setRectifOperCaptacionJarPath(String rectifOperCaptacionJarPath) { this.rectifOperCaptacionJarPath = rectifOperCaptacionJarPath; }

    public String getRectifOperCaptacionJarName() { return rectifOperCaptacionJarName; }

    public void setRectifOperCaptacionJarName(String rectifOperCaptacionJarName) { this.rectifOperCaptacionJarName = rectifOperCaptacionJarName; }

    public String getRectifOperCaptacionExFileNameWin() { return rectifOperCaptacionExFileNameWin; }

    public void setRectifOperCaptacionExFileNameWin(String rectifOperCaptacionExFileNameWin) 
	{
		this.rectifOperCaptacionExFileNameWin = rectifOperCaptacionExFileNameWin;
    }
	
	public String getRectifOperCaptacionExFileNameLin() { return rectifOperCaptacionExFileNameLin; }

    public void setRectifOperCaptacionExFileNameLin(String rectifOperCaptacionExFileNameLin) 
	{ 
		this.rectifOperCaptacionExFileNameLin = rectifOperCaptacionExFileNameLin;
    }

    public String getRectifOperCaptacionDestinationPathWin() { return rectifOperCaptacionDestinationPathWin; }

    public void setRectifOperCaptacionDestinationPathWin(String rectifOperCaptacionDestinationPathWin) 
	{
        this.rectifOperCaptacionDestinationPathWin = rectifOperCaptacionDestinationPathWin;
    }
	
	public String getRectifOperCaptacionDestinationPathLin() { return rectifOperCaptacionDestinationPathLin; }

    public void setRectifOperCaptacionDestinationPathLin(String rectifOperCaptacionDestinationPathLin) 
	{
        this.rectifOperCaptacionDestinationPathLin = rectifOperCaptacionDestinationPathLin;
    }

    public String getStartErrorMessageJarExecution() { return startErrorMessageJarExecution; }

    public void setStartErrorMessageJarExecution(String startErrorMessageJarExecution) 
	{
        this.startErrorMessageJarExecution = startErrorMessageJarExecution;
    }

    public String getCertOperCaptacionJarName() { return certOperCaptacionJarName; }

    public void setCertOperCaptacionJarName(String certOperCaptacionJarName) { this.certOperCaptacionJarName = certOperCaptacionJarName; }

    public String getCertOperCaptacionJarPath() { return certOperCaptacionJarPath; }

    public void setCertOperCaptacionJarPath(String certOperCaptacionJarPath) { this.certOperCaptacionJarPath = certOperCaptacionJarPath; }

    public String getDeclJurOperCaptacionJarName() { return declJurOperCaptacionJarName; }

    public void setDeclJurOperCaptacionJarName(String declJurOperCaptacionJarName) { this.declJurOperCaptacionJarName = declJurOperCaptacionJarName; }

    public String getDeclJurOperCaptacionJarPath() { return declJurOperCaptacionJarPath; }

    public void setDeclJurOperCaptacionJarPath(String declJurOperCaptacionJarPath) { this.declJurOperCaptacionJarPath = declJurOperCaptacionJarPath; }

    public String getCertOperCaptacionDestinationPathWin() { return certOperCaptacionDestinationPathWin; }

    public void setCertOperCaptacionDestinationPathWin(String certOperCaptacionDestinationPathWin) 
	{ 
		this.certOperCaptacionDestinationPathWin = certOperCaptacionDestinationPathWin;
    }
	
	public String getCertOperCaptacionDestinationPathLin() { return certOperCaptacionDestinationPathLin; }

    public void setCertOperCaptacionDestinationPathLin(String certOperCaptacionDestinationPathLin) 
	{ 
		this.certOperCaptacionDestinationPathLin = certOperCaptacionDestinationPathLin;
    }

    public String getDeclJurOperCaptacionDestinationPathWin() { return declJurOperCaptacionDestinationPathWin; }

    public void setDeclJurOperCaptacionDestinationPathWin(String declJurOperCaptacionDestinationPathWin) 
	{
        this.declJurOperCaptacionDestinationPathWin = declJurOperCaptacionDestinationPathWin;
    }
	
	public String getDeclJurOperCaptacionDestinationPathLin() { return declJurOperCaptacionDestinationPathLin; }

    public void setDeclJurOperCaptacionDestinationPathLin(String declJurOperCaptacionDestinationPathLin) 
	{
        this.declJurOperCaptacionDestinationPathLin = declJurOperCaptacionDestinationPathLin;
    }

    public String getCertOperCaptacionExFileNameWin() { return certOperCaptacionExFileNameWin; }

    public void setCertOperCaptacionExFileNameWin(String certOperCaptacionExFileNameWin) 
	{ 
		this.certOperCaptacionExFileNameWin = certOperCaptacionExFileNameWin; 
	}

	public String getCertOperCaptacionExFileNameLin() { return certOperCaptacionExFileNameLin; }

    public void setCertOperCaptacionExFileNameLin(String certOperCaptacionExFileNameLin) 
	{ 
		this.certOperCaptacionExFileNameLin = certOperCaptacionExFileNameLin; 
	}

    public String getDeclJurOperCaptacionExFileNameWin() { return declJurOperCaptacionExFileNameWin; }

    public void setDeclJurOperCaptacionExFileNameWin(String declJurOperCaptacionExFileNameWin) 
	{
        this.declJurOperCaptacionExFileNameWin = declJurOperCaptacionExFileNameWin;
    }
	
    public String getDeclJurOperCaptacionExFileNameLin() { return declJurOperCaptacionExFileNameLin; }

    public void setDeclJurOperCaptacionExFileNameLin(String declJurOperCaptacionExFileNameLin) 
	{
        this.declJurOperCaptacionExFileNameLin = declJurOperCaptacionExFileNameLin;
    }

    public String getErrorMessageJarExecution() { return errorMessageJarExecution; }

    public void setErrorMessageJarExecution(String errorMessageJarExecution) { this.errorMessageJarExecution = errorMessageJarExecution; }

    public String getConnectionErrorMessageJarExecution() { return connectionErrorMessageJarExecution; }

    public void setConnectionErrorMessageJarExecution(String connectionErrorMessageJarExecution) 
	{
        this.connectionErrorMessageJarExecution = connectionErrorMessageJarExecution;
    }
	
    public String getCertPagoDvidendosJarPath() { return certPagoDvidendosJarPath; }

    public void setCertPagoDvidendosJarPath(String certPagoDvidendosJarPath) { this.certPagoDvidendosJarPath = certPagoDvidendosJarPath; }

    public String getCertPagoDvidendosJarName() { return certPagoDvidendosJarName; }

    public void setCertPagoDvidendosJarName(String certPagoDvidendosJarName) { this.certPagoDvidendosJarName = certPagoDvidendosJarName; }

    public String getCertPagoDvidendosExFileNameWin() { return certPagoDvidendosExFileNameWin; }

    public void setCertPagoDvidendosExFileNameWin(String certPagoDvidendosExFileNameWin) 
	{ 
		this.certPagoDvidendosExFileNameWin = certPagoDvidendosExFileNameWin; 
	}
	
	public String getCertPagoDvidendosExFileNameLin() { return certPagoDvidendosExFileNameLin; }

    public void setCertPagoDvidendosExFileNameLin(String certPagoDvidendosExFileNameLin) 
	{ 
		this.certPagoDvidendosExFileNameLin = certPagoDvidendosExFileNameLin; 
	}

    public String getCertPagoDvidendosDestinationPathWin() { return certPagoDvidendosDestinationPathWin; }

    public void setCertPagoDvidendosDestinationPathWin(String certPagoDvidendosDestinationPathWin) 
	{
        this.certPagoDvidendosDestinationPathWin = certPagoDvidendosDestinationPathWin;
    }
	
	public String getCertPagoDvidendosDestinationPathLin() { return certPagoDvidendosDestinationPathLin; }

    public void setCertPagoDvidendosDestinationPathLin(String certPagoDvidendosDestinationPathLin) 
	{
        this.certPagoDvidendosDestinationPathLin = certPagoDvidendosDestinationPathLin;
    }

    public String getDeclJurPagoDvidendosJarPath() { return declJurPagoDvidendosJarPath; }

    public void setDeclJurPagoDvidendosJarPath(String declJurPagoDvidendosJarPath) { this.declJurPagoDvidendosJarPath = declJurPagoDvidendosJarPath; }

    public String getDeclJurPagoDvidendosJarName() { return declJurPagoDvidendosJarName; }

    public void setDeclJurPagoDvidendosJarName(String declJurPagoDvidendosJarName) { this.declJurPagoDvidendosJarName = declJurPagoDvidendosJarName; }

    public String getDeclJurPagoDvidendosExFileNameWin() { return declJurPagoDvidendosExFileNameWin; }

    public void setDeclJurPagoDvidendosExFileNameWin(String declJurPagoDvidendosExFileNameWin) 
	{
        this.declJurPagoDvidendosExFileNameWin = declJurPagoDvidendosExFileNameWin;
    }
	
	public String getDeclJurPagoDvidendosExFileNameLin() { return declJurPagoDvidendosExFileNameLin; }

    public void setDeclJurPagoDvidendosExFileNameLin(String declJurPagoDvidendosExFileNameLin) 
	{
        this.declJurPagoDvidendosExFileNameLin = declJurPagoDvidendosExFileNameLin;
    }

    public String getDeclJurPagoDvidendosDestinationPathWin() { return declJurPagoDvidendosDestinationPathWin; }

    public void setDeclJurPagoDvidendosDestinationPathWin(String declJurPagoDvidendosDestinationPathWin) 
	{ 
		this.declJurPagoDvidendosDestinationPathWin = declJurPagoDvidendosDestinationPathWin;
    }
	
	public String getDeclJurPagoDvidendosDestinationPathLin() { return declJurPagoDvidendosDestinationPathLin; }

    public void setDeclJurPagoDvidendosDestinationPathLin(String declJurPagoDvidendosDestinationPathLin) 
	{ 
		this.declJurPagoDvidendosDestinationPathLin = declJurPagoDvidendosDestinationPathLin;
    }

    public String getCertInversiones57BisJarPath() { return certInversiones57BisJarPath; }

    public void setCertInversiones57BisJarPath(String certInversiones57BisJarPath) { this.certInversiones57BisJarPath = certInversiones57BisJarPath; }

    public String getCertInversiones57BisExFileNameWin() { return certInversiones57BisExFileNameWin; }

    public void setCertInversiones57BisExFileNameWin(String certInversiones57BisExFileNameWin) 
	{
        this.certInversiones57BisExFileNameWin = certInversiones57BisExFileNameWin;
    }
	
	public String getCertInversiones57BisExFileNameLin() { return certInversiones57BisExFileNameLin; }

    public void setCertInversiones57BisExFileNameLin(String certInversiones57BisExFileNameLin) 
	{
        this.certInversiones57BisExFileNameLin = certInversiones57BisExFileNameLin;
    }

    public String getCertInversiones57BisJarName() { return certInversiones57BisJarName; }

    public void setCertInversiones57BisJarName(String certInversiones57BisJarName) 
	{ 
		this.certInversiones57BisJarName = certInversiones57BisJarName; 
	}

    public String getCertInversiones57BisDestinationPathWin() { return certInversiones57BisDestinationPathWin; }

    public void setCertInversiones57BisDestinationPathWin(String certInversiones57BisDestinationPathWin) 
	{
        this.certInversiones57BisDestinationPathWin = certInversiones57BisDestinationPathWin;
    }
	
	public String getCertInversiones57BisDestinationPathLin() { return certInversiones57BisDestinationPathLin; }

    public void setCertInversiones57BisDestinationPathLin(String certInversiones57BisDestinationPathLin) 
	{
        this.certInversiones57BisDestinationPathLin = certInversiones57BisDestinationPathLin;
    }

    public String getDeclJurInversiones57BisJarPath() { return declJurInversiones57BisJarPath; }

    public void setDeclJurInversiones57BisJarPath(String declJurInversiones57BisJarPath) 
	{ 
		this.declJurInversiones57BisJarPath = declJurInversiones57BisJarPath;
    }

    public String getDeclJurInversiones57BisExFileNameWin() { return declJurInversiones57BisExFileNameWin; }

    public void setDeclJurInversiones57BisExFileNameWin(String declJurInversiones57BisExFileNameWin) 
	{
        this.declJurInversiones57BisExFileNameWin = declJurInversiones57BisExFileNameWin;
    }
	
	public String getDeclJurInversiones57BisExFileNameLin() { return declJurInversiones57BisExFileNameLin; }

    public void setDeclJurInversiones57BisExFileNameLin(String declJurInversiones57BisExFileNameLin) 
	{
        this.declJurInversiones57BisExFileNameLin = declJurInversiones57BisExFileNameLin;
    }

    public String getDeclJurInversiones57BisJarName() { return declJurInversiones57BisJarName; }

    public void setDeclJurInversiones57BisJarName(String declJurInversiones57BisJarName) 
	{ 
		this.declJurInversiones57BisJarName = declJurInversiones57BisJarName; 
	}

    public String getDeclJurInversiones57BisDestinationPathWin() { return declJurInversiones57BisDestinationPathWin; }

    public void setDeclJurInversiones57BisDestinationPathWin(String declJurInversiones57BisDestinationPathWin) 
	{
        this.declJurInversiones57BisDestinationPathWin = declJurInversiones57BisDestinationPathWin;
    }
	
	public String getDeclJurInversiones57BisDestinationPathLin() { return declJurInversiones57BisDestinationPathLin; }

    public void setDeclJurInversiones57BisDestinationPathLin(String declJurInversiones57BisDestinationPathLin) 
	{
        this.declJurInversiones57BisDestinationPathLin = declJurInversiones57BisDestinationPathLin;
    }
}
