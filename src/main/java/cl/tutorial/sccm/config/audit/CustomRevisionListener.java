package cl.tutorial.sccm.config.audit;

import cl.tutorial.sccm.domain.audit.CustomRevisionEntity;
import cl.tutorial.sccm.security.SecurityUtils;
import org.hibernate.envers.RevisionListener;

public class CustomRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        CustomRevisionEntity entity = (CustomRevisionEntity) revisionEntity;
        entity.setAuditor(SecurityUtils.getCurrentUserLogin());
    }

}
