package cl.tutorial.sccm.config;

import cl.tutorial.sccm.security.AuthoritiesConstants;
import cl.tutorial.sccm.security.jwt.JWTConfigurer;
import cl.tutorial.sccm.security.jwt.TokenProvider;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Import(SecurityProblemSupport.class)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserDetailsService userDetailsService;

    private final TokenProvider tokenProvider;

    private final CorsFilter corsFilter;

    private final SecurityProblemSupport problemSupport;

    // @Autowired
    //private ETLConfiguration etlConfiguration;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder, UserDetailsService userDetailsService, TokenProvider tokenProvider,
        CorsFilter corsFilter, SecurityProblemSupport problemSupport) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userDetailsService = userDetailsService;
        this.tokenProvider = tokenProvider;
        this.corsFilter = corsFilter;
        this.problemSupport = problemSupport;
    }

    @PostConstruct
    public void init() {
        try {
            authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
            .exceptionHandling()
            .authenticationEntryPoint(problemSupport)
            .accessDeniedHandler(problemSupport)
        .and()
            .csrf()
            .disable()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers("/api/register").permitAll()
            .antMatchers("/api/activate").permitAll()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/account/reset-password/init").permitAll()
            .antMatchers("/api/account/reset-password/finish").permitAll()
            .antMatchers("/api/profile-info").permitAll()
            // Permisos para ROLE_ADMIN
            // Datos Banco
            .antMatchers(HttpMethod.POST,"/api/datos-fijos/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/datos-fijos/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/datos-fijos/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Usuarios
            .antMatchers(HttpMethod.POST,"/api/users/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/users/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/users/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Cliente Bice
            .antMatchers(HttpMethod.POST,"/api/cliente-bices/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/cliente-bices/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/cliente-bices/**").hasAuthority(AuthoritiesConstants.ADMIN)
			// Cliente Bice 1890
			.antMatchers(HttpMethod.POST,"/api/cliente-1890-s/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/cliente-1890-s/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/cliente-1890-s/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Instrumento
            .antMatchers(HttpMethod.POST,"/api/instrumento/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/instrumento/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/instrumento/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Auditorías
            .antMatchers("/api/audit-service-access/**", "/api/entity-changes-audit/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Correción Monetaria
            .antMatchers(HttpMethod.POST,"/api/correc-moneta/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/correc-moneta/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/correc-moneta/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Productos
            .antMatchers(HttpMethod.POST,"/api/productos/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/productos/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/productos/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Años tributarios
            .antMatchers(HttpMethod.POST,"/api/anno-tributas/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.PUT,"/api/anno-tributas/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers(HttpMethod.DELETE,"/api/anno-tributas/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Carga de datos -- carga de interfaces (ACCOUNTING, EURO, PARITY, UF, USD) carga datos ETL (BONOS, LHBONOS, LHDCV, DAP, PACTOS, AHORROS, AHORROS_MOVIMIENTOS)
            .antMatchers(HttpMethod.POST,"/api/storage/uploadEtlDataWithOption/**").hasAnyAuthority(AuthoritiesConstants.ADMIN, AuthoritiesConstants.SCCM_1890_USER_AHORRO,
                AuthoritiesConstants.SCCM_1890_USER_BYL, AuthoritiesConstants.SCCM_1890_USER_DAP, AuthoritiesConstants.SCCM_1890_USER_PACTOS)
            .antMatchers(HttpMethod.PUT,"/api/storage/uploadEtlDataWithOption/**").hasAnyAuthority(AuthoritiesConstants.ADMIN, AuthoritiesConstants.SCCM_1890_USER_AHORRO,
                AuthoritiesConstants.SCCM_1890_USER_BYL, AuthoritiesConstants.SCCM_1890_USER_DAP, AuthoritiesConstants.SCCM_1890_USER_PACTOS)
            .antMatchers(HttpMethod.DELETE,"/api/storage/uploadEtlDataWithOption/**").hasAnyAuthority(AuthoritiesConstants.ADMIN, AuthoritiesConstants.SCCM_1890_USER_AHORRO,
                AuthoritiesConstants.SCCM_1890_USER_BYL, AuthoritiesConstants.SCCM_1890_USER_DAP, AuthoritiesConstants.SCCM_1890_USER_PACTOS)
            .antMatchers("/api/audit-service-accesses/**").hasAuthority(AuthoritiesConstants.ADMIN)
            // Permisos para ROLE_SCCM_1941_ADMIN y ROLE_SCCM_1941_USER
            // Certificado 1941
            .antMatchers("/api/ent-pagdivs/**", "/api/cert-pagdivs/**", "/api/storage/uploadInterfaces/**").hasAnyAuthority(
                AuthoritiesConstants.SCCM_1941_ADMIN, AuthoritiesConstants.SCCM_1941_USER)
            .antMatchers("/api/futs/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1941_ADMIN, AuthoritiesConstants.SCCM_1941_USER)
            .antMatchers("/api/storage/emiteCertificados/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1941_ADMIN, AuthoritiesConstants.SCCM_1941_USER)
            .antMatchers("/api/storage/emiteDJ/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1941_ADMIN, AuthoritiesConstants.SCCM_1941_USER)
            // Permisos para ROLE_SCCM_1944_ADMIN y ROLE_SCCM_1944_USER
            // Certificado 1944
            .antMatchers("/api/entrada-57-bs/**", "/api/cart-57-bs/**", "/api/cert-57-bs/**", "/api/storage/uploadInterfaces57Bis/**").hasAnyAuthority(
                AuthoritiesConstants.SCCM_1944_ADMIN, AuthoritiesConstants.SCCM_1944_USER)
            .antMatchers("/api/storage/emiteCertificados57Bis/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1944_ADMIN, AuthoritiesConstants.SCCM_1944_USER)
            .antMatchers("/api/storage/emiteDJ57B/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1944_ADMIN, AuthoritiesConstants.SCCM_1944_USER)
            // Permisos para ROLE_SCCM_1944_ADMIN y ROLE_SCCM_1944_USER
            .antMatchers("/api/representantes/**", "/api/glosas/**").hasAnyAuthority(
                AuthoritiesConstants.SCCM_1944_ADMIN, AuthoritiesConstants.SCCM_1944_USER, AuthoritiesConstants.SCCM_1941_ADMIN, AuthoritiesConstants.SCCM_1941_USER,
                AuthoritiesConstants.SCCM_1890_ADMIN)
            // Permisos para ROLE_SCCM_1890_ADMIN
            // Certificado 1890
            .antMatchers("/api/datos-emi-cert/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1890_ADMIN)
            .antMatchers("/api/storage/emiteCertificadosOperacionesCaptacion/**", "/api/storage/emiteDJOperacionesCaptacion/**", "/api/storage/emiteRectifOperacionesCaptacion/**")
                .hasAnyAuthority(AuthoritiesConstants.SCCM_1890_ADMIN)
            // Permisos para aprobaciones 1890
            .antMatchers("/api/aprobacions/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1890_ADMIN, AuthoritiesConstants.SCCM_1890_ADMIN_AHORRO,
            AuthoritiesConstants.SCCM_1890_ADMIN_BYL, AuthoritiesConstants.SCCM_1890_ADMIN_DAP, AuthoritiesConstants.SCCM_1890_ADMIN_PACTOS)
            // Permisos para SCCM_1890_USER_BYL
            // Bonos y Letras
            .antMatchers("/api/letras-bonos/**", "/api/totales-bonos/**", "/api/letras-bonos-cuadraturas/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1890_USER_BYL)
            // Permisos para SCCM_1890_USER_PACTOS
            // Pactos
            .antMatchers("/api/pactos/**", "/api/pactosIR/**", "/api/pactosCC/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1890_USER_PACTOS)
            // Permisos para SCCM_1890_USER_AHORRO
            // Ahorros
            .antMatchers("/api/ahorros/**", "/api/ahorrosIR/**", "/api/ahorrosCC/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1890_USER_AHORRO)
            // Permisos para SCCM_1890_USER_DAP
            // Dap
            .antMatchers("/api/d-aps/**", "/api/dap-irs/**", "/api/dap-ccs/**").hasAnyAuthority(AuthoritiesConstants.SCCM_1890_USER_DAP)
            // Los permisos generales van de últimos.
            .antMatchers("/api/storage/**").authenticated()
            .antMatchers("/api/**").authenticated()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/v2/api-docs/**").permitAll()
            .antMatchers("/swagger-resources/configuration/ui").permitAll()
            .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN)
        .and()
            .apply(securityConfigurerAdapter());

    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }

    @Inject
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
/*
			LdapContextSource contextSource = new LdapContextSource();
			contextSource.setUrl(etlConfiguration.getEtlLdapUrl()); // LOCAL "ldap://192.168.56.254" TUTORIAL
			contextSource.setBase(etlConfiguration.getEtlLdapBase()); // "CN=Users,DC=tutorial,DC=com"
			contextSource.setUserDn(etlConfiguration.getEtlLdapBindDN()); // "CN=Maikel Eduardo Suarez Torres,CN=Users,DC=tutorial,DC=com"
			contextSource.setPassword(etlConfiguration.getEtlLdapPassword()); // "Tut0r14l"
			contextSource.afterPropertiesSet();
			auth.ldapAuthentication()
					.userSearchFilter("(sAMAccountName={0})")
					.groupSearchFilter("member={0}")
					.contextSource(contextSource);
*/
    }

}
