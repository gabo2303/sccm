package cl.tutorial.sccm.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache("users", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.DatosFijos.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Glosa.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Representante.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Fut.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.SCCMUserRole.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".futs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".glosas", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".reps", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".insts", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".datofijos", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".datosFijos1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Producto.class.getName() + ".prod1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName() + ".prods", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName() + ".glosa1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName() + ".datFijs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName() + ".futs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName() + ".glosas", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Instrumento.class.getName() + ".reps", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.CargaMoneta.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.EntPagdiv.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.CertPagdiv.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".inst2S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".certPagDivs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".corMons", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".rep1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".fut1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".glosas", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".ent1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".sInis", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".mov57S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".emiCarts", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".emiCerts", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".ent57bs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".saldos", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".euros", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".ufs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".usds", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".monedas", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".daps", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".lhBonos", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".lhdcvs", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".bonos", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.AnnoTributa.class.getName() + ".aprobaciones", jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Cliente1890.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Cliente1890.class.getName() + ".cliRut1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Cliente1890.class.getName() + ".rut18901S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Cliente1890.class.getName() + ".rut1890S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.ClienteBice.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.ClienteBice.class.getName() + ".sIni2S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.ClienteBice.class.getName() + ".emiCart1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.ClienteBice.class.getName() + ".emiCert1S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.ClienteBice.class.getName() + ".ent57b2S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.ClienteBice.class.getName() + ".saldos2S", jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.CorrecMoneta.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Entrada57b.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.SalIni57B.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Certi57BFcc.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Cert57B.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.Cart57B.class.getName(), jcacheConfiguration);
            cm.createCache(cl.tutorial.sccm.domain.SalProxPer.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Rut1890.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Ahorro.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.AhorroMov.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Contabilidad.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Euro.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.TotalProd.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.DAP.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.LHBonos.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Bonos.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.LHDCV.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Moneda.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Pactos.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.TablaMoneda.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Uf.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Usd.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.CertVersion.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.UltDiaHabMes.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.DapIR.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.DapCC.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.LetrasBonos.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.LetrasBonosCuadratura.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.TotalesBonos.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.TipoAlerta.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Aprobacion.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.AuditServiceAccess.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.UltDiaHabAnio.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Directorios.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.CertificadosAnteriores.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Clientes1890mantencion.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Temporal1890.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.Rut_temp.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.CabeceraControlCambio.class.getName(), jcacheConfiguration);
           cm.createCache(cl.tutorial.sccm.domain.DetalleControlCambio.class.getName(), jcacheConfiguration);
           // jhipster-needle-ehcache-add-entry
        };
    }
}
