package cl.tutorial.sccm.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String SCCM_1890_ADMIN        = "ROLE_SCCM_1890_ADMIN";
    public static final String SCCM_1890_ADMIN_AHORRO = "ROLE_SCCM_1890_ADMIN_AHORRO";
    public static final String SCCM_1890_USER_AHORRO  = "ROLE_SCCM_1890_USER_AHORRO";
    public static final String SCCM_1890_ADMIN_DAP    = "ROLE_SCCM_1890_ADMIN_DAP";
    public static final String SCCM_1890_USER_DAP     = "ROLE_SCCM_1890_USER_DAP";
    public static final String SCCM_1890_ADMIN_BYL    = "ROLE_SCCM_1890_ADMIN_BYL";
    public static final String SCCM_1890_USER_BYL     = "ROLE_SCCM_1890_USER_BYL";
    public static final String SCCM_1890_ADMIN_PACTOS = "ROLE_SCCM_1890_ADMIN_PACTOS";
    public static final String SCCM_1890_USER_PACTOS  = "ROLE_SCCM_1890_USER_PACTOS";

    public static final String SCCM_1941_ADMIN = "ROLE_SCCM_1941_ADMIN";
    public static final String SCCM_1941_USER  = "ROLE_SCCM_1941_USER";

    public static final String SCCM_1944_ADMIN = "ROLE_SCCM_1944_ADMIN";
    public static final String SCCM_1944_USER  = "ROLE_SCCM_1944_USER";

    // No Utilizados...
//    public static final String SCCM_ADMIN     = "ROLE_SCCM_ADMIN";
//    public static final String SCCM_1890_USER = "ROLE_SCCM_1890_USER";
    public static final String USER           = "ROLE_USER";

    private AuthoritiesConstants() {
    }
}
