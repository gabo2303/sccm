package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Directorios.
 */
@Entity
@Table(name = "directorios")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Directorios implements Serializable 
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "ruta")
    private String ruta;

    @Column(name = "descripcion")
    private String descripcion;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private Producto producto;

    @ManyToOne
    private Instrumento instrumento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getRuta() { return ruta; }

    public Directorios ruta(String ruta) 
	{		
		this.ruta = ruta;
        return this;
    }

    public void setRuta(String ruta) { this.ruta = ruta; }

    public String getDescripcion() { return descripcion; }

    public Directorios descripcion(String descripcion) 
	{
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    public AnnoTributa getAnnoTributa() { return annoTributa; }

    public Directorios annoTributa(AnnoTributa annoTributa) 
	{
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) { this.annoTributa = annoTributa; }

    public Producto getProducto() { return producto; }

    public Directorios producto(Producto producto) 
	{
        this.producto = producto;
        return this;
    }

    public void setProducto(Producto producto) { this.producto = producto; }

    public Instrumento getInstrumento() { return instrumento; }

    public Directorios instrumento(Instrumento instrumento) 
	{
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(Instrumento instrumento) { this.instrumento = instrumento; }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) 
	{
        if (this == o) { return true; }
		
        if (o == null || getClass() != o.getClass()) { return false; }
		
        Directorios directorios = (Directorios) o;
		
        if (directorios.getId() == null || getId() == null) { return false; }
		
        return Objects.equals(getId(), directorios.getId());
    }

    @Override
    public int hashCode() { return Objects.hashCode(getId()); }

    @Override
    public String toString() 
	{
        return "Directorios{" + "id=" + getId() + ", ruta='" + getRuta() + "'" + ", descripcion='" + getDescripcion() + "'" + "}";
    }
}