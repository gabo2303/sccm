package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Cert57B.
 */
@Entity
@Table(name = "cert_57_b")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Cert57B implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Max(value = 10)
    @Column(name = "saldo_neto_per")
    private Integer saldoNetoPer;

    @Max(value = 10)
    @Column(name = "monto_rentabilidad")
    private Integer montoRentabilidad;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private ClienteBice clienteBice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSaldoNetoPer() {
        return saldoNetoPer;
    }

    public Cert57B saldoNetoPer(Integer saldoNetoPer) {
        this.saldoNetoPer = saldoNetoPer;
        return this;
    }

    public void setSaldoNetoPer(Integer saldoNetoPer) {
        this.saldoNetoPer = saldoNetoPer;
    }

    public Integer getMontoRentabilidad() {
        return montoRentabilidad;
    }

    public Cert57B montoRentabilidad(Integer montoRentabilidad) {
        this.montoRentabilidad = montoRentabilidad;
        return this;
    }

    public void setMontoRentabilidad(Integer montoRentabilidad) {
        this.montoRentabilidad = montoRentabilidad;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Cert57B annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public ClienteBice getClienteBice() {
        return clienteBice;
    }

    public Cert57B clienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
        return this;
    }

    public void setClienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cert57B cert57B = (Cert57B) o;
        if (cert57B.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cert57B.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Cert57B{" +
            "id=" + getId() +
            ", saldoNetoPer='" + getSaldoNetoPer() + "'" +
            ", montoRentabilidad='" + getMontoRentabilidad() + "'" +
            "}";
    }
}
