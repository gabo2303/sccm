package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Rut_temp.
 */
@Entity
@Table(name = "rut_temp")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Rut_temp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "rut")
    private Integer rut;

    public Integer getRut() {
        return rut;
    }

    public Rut_temp rut(Integer rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove



    @Override
    public String toString() {
        return "Rut_temp{" +
            ", rut='" + getRut() + "'" +
            "}";
    }
}
