package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Entrada57b.
 */
@Entity
@Table(name = "entrada_57_b")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Entrada57b implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "folio_op")
    private String folioOp;

    @Column(name = "folio_mov")
    private String folioMov;

    @Column(name = "aa_op")
    private String aaOp;

    @Column(name = "mm_op")
    private String mmOp;

    @Column(name = "dd_op")
    private String ddOp;

    @Column(name = "tipo_inst")
    private String tipoInst;

    @Column(name = "tipo_op")
    private String tipoOp;

    @Column(name = "cod_moneda")
    private String codMoneda;

    @Column(name = "monto_hist_nom")
    private String montoHistNom;

    @Column(name = "monto_hist_pesos")
    private String montoHistPesos;

    @Column(name = "aa_vcto")
    private String aaVcto;

    @Column(name = "mm_vcto")
    private String mmVcto;

    @Column(name = "dd_vcto")
    private String ddVcto;

    @Column(name = "flag_remanente")
    private String flagRemanente;

    @Column(name = "flag_modif_ley")
    private String flagModifLey;

    @Column(name = "fecha_dep")
    private Instant fechaDep;

    @Column(name = "monto_int_nom")
    private String montoIntNom;

    @Column(name = "monto_reaj_nom")
    private String montoReajNom;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private ClienteBice clienteBice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFolioOp() {
        return folioOp;
    }

    public Entrada57b folioOp(String folioOp) {
        this.folioOp = folioOp;
        return this;
    }

    public void setFolioOp(String folioOp) {
        this.folioOp = folioOp;
    }

    public String getFolioMov() {
        return folioMov;
    }

    public Entrada57b folioMov(String folioMov) {
        this.folioMov = folioMov;
        return this;
    }

    public void setFolioMov(String folioMov) {
        this.folioMov = folioMov;
    }

    public String getAaOp() {
        return aaOp;
    }

    public Entrada57b aaOp(String aaOp) {
        this.aaOp = aaOp;
        return this;
    }

    public void setAaOp(String aaOp) {
        this.aaOp = aaOp;
    }

    public String getMmOp() {
        return mmOp;
    }

    public Entrada57b mmOp(String mmOp) {
        this.mmOp = mmOp;
        return this;
    }

    public void setMmOp(String mmOp) {
        this.mmOp = mmOp;
    }

    public String getDdOp() {
        return ddOp;
    }

    public Entrada57b ddOp(String ddOp) {
        this.ddOp = ddOp;
        return this;
    }

    public void setDdOp(String ddOp) {
        this.ddOp = ddOp;
    }

    public String getTipoInst() {
        return tipoInst;
    }

    public Entrada57b tipoInst(String tipoInst) {
        this.tipoInst = tipoInst;
        return this;
    }

    public void setTipoInst(String tipoInst) {
        this.tipoInst = tipoInst;
    }

    public String getTipoOp() {
        return tipoOp;
    }

    public Entrada57b tipoOp(String tipoOp) {
        this.tipoOp = tipoOp;
        return this;
    }

    public void setTipoOp(String tipoOp) {
        this.tipoOp = tipoOp;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    public Entrada57b codMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
        return this;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public String getMontoHistNom() {
        return montoHistNom;
    }

    public Entrada57b montoHistNom(String montoHistNom) {
        this.montoHistNom = montoHistNom;
        return this;
    }

    public void setMontoHistNom(String montoHistNom) {
        this.montoHistNom = montoHistNom;
    }

    public String getMontoHistPesos() {
        return montoHistPesos;
    }

    public Entrada57b montoHistPesos(String montoHistPesos) {
        this.montoHistPesos = montoHistPesos;
        return this;
    }

    public void setMontoHistPesos(String montoHistPesos) {
        this.montoHistPesos = montoHistPesos;
    }

    public String getAaVcto() {
        return aaVcto;
    }

    public Entrada57b aaVcto(String aaVcto) {
        this.aaVcto = aaVcto;
        return this;
    }

    public void setAaVcto(String aaVcto) {
        this.aaVcto = aaVcto;
    }

    public String getMmVcto() {
        return mmVcto;
    }

    public Entrada57b mmVcto(String mmVcto) {
        this.mmVcto = mmVcto;
        return this;
    }

    public void setMmVcto(String mmVcto) {
        this.mmVcto = mmVcto;
    }

    public String getDdVcto() {
        return ddVcto;
    }

    public Entrada57b ddVcto(String ddVcto) {
        this.ddVcto = ddVcto;
        return this;
    }

    public void setDdVcto(String ddVcto) {
        this.ddVcto = ddVcto;
    }

    public String getFlagRemanente() {
        return flagRemanente;
    }

    public Entrada57b flagRemanente(String flagRemanente) {
        this.flagRemanente = flagRemanente;
        return this;
    }

    public void setFlagRemanente(String flagRemanente) {
        this.flagRemanente = flagRemanente;
    }

    public String getFlagModifLey() {
        return flagModifLey;
    }

    public Entrada57b flagModifLey(String flagModifLey) {
        this.flagModifLey = flagModifLey;
        return this;
    }

    public void setFlagModifLey(String flagModifLey) {
        this.flagModifLey = flagModifLey;
    }

    public Instant getFechaDep() {
        return fechaDep;
    }

    public Entrada57b fechaDep(Instant fechaDep) {
        this.fechaDep = fechaDep;
        return this;
    }

    public void setFechaDep(Instant fechaDep) {
        this.fechaDep = fechaDep;
    }

    public String getMontoIntNom() {
        return montoIntNom;
    }

    public Entrada57b montoIntNom(String montoIntNom) {
        this.montoIntNom = montoIntNom;
        return this;
    }

    public void setMontoIntNom(String montoIntNom) {
        this.montoIntNom = montoIntNom;
    }

    public String getMontoReajNom() {
        return montoReajNom;
    }

    public Entrada57b montoReajNom(String montoReajNom) {
        this.montoReajNom = montoReajNom;
        return this;
    }

    public void setMontoReajNom(String montoReajNom) {
        this.montoReajNom = montoReajNom;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Entrada57b annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public ClienteBice getClienteBice() {
        return clienteBice;
    }

    public Entrada57b clienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
        return this;
    }

    public void setClienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Entrada57b entrada57b = (Entrada57b) o;
        if (entrada57b.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entrada57b.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Entrada57b{" +
            "id=" + getId() +
            ", folioOp='" + getFolioOp() + "'" +
            ", folioMov='" + getFolioMov() + "'" +
            ", aaOp='" + getAaOp() + "'" +
            ", mmOp='" + getMmOp() + "'" +
            ", ddOp='" + getDdOp() + "'" +
            ", tipoInst='" + getTipoInst() + "'" +
            ", tipoOp='" + getTipoOp() + "'" +
            ", codMoneda='" + getCodMoneda() + "'" +
            ", montoHistNom='" + getMontoHistNom() + "'" +
            ", montoHistPesos='" + getMontoHistPesos() + "'" +
            ", aaVcto='" + getAaVcto() + "'" +
            ", mmVcto='" + getMmVcto() + "'" +
            ", ddVcto='" + getDdVcto() + "'" +
            ", flagRemanente='" + getFlagRemanente() + "'" +
            ", flagModifLey='" + getFlagModifLey() + "'" +
            ", fechaDep='" + getFechaDep() + "'" +
            ", montoIntNom='" + getMontoIntNom() + "'" +
            ", montoReajNom='" + getMontoReajNom() + "'" +
            "}";
    }
}
