package cl.tutorial.sccm.domain.audit;

import cl.tutorial.sccm.config.audit.CustomRevisionListener;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.*;
import java.text.DateFormat;
import java.util.Date;

/**
 * The audit table has the following fields:
 *      id of the original entity (this can be more then one column, if using an embedded or multiple id)
 *      revision number - an integer
 *      revision type - a small integer
 *      audited fields from the original entity
 *
 * The primary key of the audit table is the combination of the original id of the entity and the revision number - there can be at most one historic entry for a given entity
 * instance at a given revision.
 */
@Entity
@RevisionEntity(CustomRevisionListener.class)
@Table(name = "revisions_info")
public class CustomRevisionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @RevisionNumber
    private int id;

    @RevisionTimestamp
    private long timestamp;

    private String auditor;

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Transient
    public Date getRevisionDate() {
        return new Date(timestamp);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomRevisionEntity)) {
            return false;
        }

        final CustomRevisionEntity that = (CustomRevisionEntity) o;
        return id == that.id && timestamp == that.timestamp;
    }

    @Override
    public int hashCode() {
        int result;
        result = id;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "DefaultRevisionEntity(id = " + id + ", revisionDate = " + DateFormat.getDateTimeInstance().format(getRevisionDate()) + ")";
    }
}
