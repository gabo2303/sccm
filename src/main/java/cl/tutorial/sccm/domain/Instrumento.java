package cl.tutorial.sccm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Instrumento.
 */
@Entity
@Table(name = "instrumento")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Instrumento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre_instrumento")
    private String nombreInstrumento;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private Producto producto;

    @OneToMany(mappedBy = "instrumento")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Fut> futs = new HashSet<>();

    @OneToMany(mappedBy = "instrumento")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Glosa> glosa1S = new HashSet<>();

    @OneToMany(mappedBy = "instrumento")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Representante> reps = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreInstrumento() {
        return nombreInstrumento;
    }

    public Instrumento nombreInstrumento(String nombreInstrumento) {
        this.nombreInstrumento = nombreInstrumento;
        return this;
    }

    public void setNombreInstrumento(String nombreInstrumento) {
        this.nombreInstrumento = nombreInstrumento;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Instrumento annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public Set<Fut> getFuts() {
        return futs;
    }

    public Instrumento futs(Set<Fut> futs) {
        this.futs = futs;
        return this;
    }

    public Instrumento addFut(Fut fut) {
        this.futs.add(fut);
        fut.setInstrumento(this);
        return this;
    }

    public Instrumento removeFut(Fut fut) {
        this.futs.remove(fut);
        fut.setInstrumento(null);
        return this;
    }

    public void setFuts(Set<Fut> futs) {
        this.futs = futs;
    }

    public Set<Glosa> getGlosa1S() {
        return glosa1S;
    }

    public Instrumento glosa1S(Set<Glosa> glosas) {
        this.glosa1S = glosas;
        return this;
    }

    public Instrumento addGlosa1(Glosa glosa) {
        this.glosa1S.add(glosa);
        glosa.setInstrumento(this);
        return this;
    }

    public Instrumento removeGlosa1(Glosa glosa) {
        this.glosa1S.remove(glosa);
        glosa.setInstrumento(null);
        return this;
    }

    public void setGlosa1S(Set<Glosa> glosas) {
        this.glosa1S = glosas;
    }

    public Set<Representante> getReps() {
        return reps;
    }

    public Instrumento reps(Set<Representante> representantes) {
        this.reps = representantes;
        return this;
    }

    public Instrumento addRep(Representante representante) {
        this.reps.add(representante);
        representante.setInstrumento(this);
        return this;
    }

    public Instrumento removeRep(Representante representante) {
        this.reps.remove(representante);
        representante.setInstrumento(null);
        return this;
    }

    public void setReps(Set<Representante> representantes) {
        this.reps = representantes;
    }

    public Producto getProducto() {
        return producto;
    }

    public Instrumento producto(Producto producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Instrumento instrumento = (Instrumento) o;
        if (instrumento.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), instrumento.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Instrumento{" +
            "id=" + getId() +
            ", nombreInstrumento='" + getNombreInstrumento() + "'" +
            "}";
    }
}
