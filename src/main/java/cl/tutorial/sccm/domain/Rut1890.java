package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Rut1890.
 */
@Entity
@Table(name = "rut_1890")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Rut1890 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rut")
    private Integer rut;

    @Column(name = "dv")
    private String dv;

    @ManyToOne
    private Cliente1890 cliente1890;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRut() {
        return rut;
    }

    public Rut1890 rut(Integer rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public Rut1890 dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Cliente1890 getCliente1890() {
        return cliente1890;
    }

    public Rut1890 cliente1890(Cliente1890 cliente1890) {
        this.cliente1890 = cliente1890;
        return this;
    }

    public void setCliente1890(Cliente1890 cliente1890) {
        this.cliente1890 = cliente1890;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rut1890 rut1890 = (Rut1890) o;
        if (rut1890.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rut1890.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Rut1890{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", dv='" + getDv() + "'" +
            "}";
    }
}
