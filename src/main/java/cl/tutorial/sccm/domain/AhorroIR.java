package cl.tutorial.sccm.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * A class representing view from VW_COMPARA_INT_REAL cuadratura interés real calc.
 */

@Entity
@Table(name = "VW_COMPARA_INT_REAL")
public class AhorroIR implements Serializable {

    @Id
    @Basic(optional = false)
    private BigInteger id;

    @Column(name = "FOLIO")
    private String folio;

    @Column(name = "INT_REAL")
    private Double intReal;

    @Column(name = "IR_FINAL_CAL")
    private Double irFinalCal;

    @Column(name = "DIFERENCIA")
    private Double diferencia;

    @Column(name = "DIF_ABS")
    private Double difAbs;

    @Column(name = "SI_CALCULADO")
    private Double siCalculado;

    @Column(name = "GIROS")
    private Double giros;

    @Column(name = "ABONOS")
    private Double abonos;

    @Column(name = "SF_CALCULADO")
    private Double sfCalculado;

    @Column(name = "ANNO_TRIBUTA_ID")
    private BigInteger annoTributa;

    public AhorroIR() {
    }

    public AhorroIR(BigInteger id, String folio, Double intReal, Double irFinalCal, Double diferencia, Double difAbs, Double siCalculado, Double giros, Double abonos,
        Double sfCalculado, BigInteger annoTributa) {

        this.id = id;
        this.folio = folio;
        this.intReal = intReal;
        this.irFinalCal = irFinalCal;
        this.diferencia = diferencia;
        this.difAbs = difAbs;
        this.siCalculado = siCalculado;
        this.giros = giros;
        this.abonos = abonos;
        this.sfCalculado = sfCalculado;
        this.annoTributa = annoTributa;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Double getIntReal() {
        return intReal;
    }

    public void setIntReal(Double intReal) {
        this.intReal = intReal;
    }

    public Double getIrFinalCal() {
        return irFinalCal;
    }

    public void setIrFinalCal(Double irFinalCal) {
        this.irFinalCal = irFinalCal;
    }

    public Double getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(Double diferencia) {
        this.diferencia = diferencia;
    }

    public Double getDifAbs() {
        return difAbs;
    }

    public void setDifAbs(Double difAbs) {
        this.difAbs = difAbs;
    }

    public Double getSiCalculado() {
        return siCalculado;
    }

    public void setSiCalculado(Double siCalculado) {
        this.siCalculado = siCalculado;
    }

    public Double getGiros() {
        return giros;
    }

    public void setGiros(Double giros) {
        this.giros = giros;
    }

    public Double getAbonos() {
        return abonos;
    }

    public void setAbonos(Double abonos) {
        this.abonos = abonos;
    }

    public Double getSfCalculado() {
        return sfCalculado;
    }

    public void setSfCalculado(Double sfCalculado) {
        this.sfCalculado = sfCalculado;
    }

    public BigInteger getAnnoTributa() {
        return annoTributa;
    }

    public void setAnnoTributa(BigInteger annoTributa) {
        this.annoTributa = annoTributa;
    }
}
