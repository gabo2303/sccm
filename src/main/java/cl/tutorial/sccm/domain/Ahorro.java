package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Ahorro.
 */
@Entity
@Table(name = "ahorro")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Ahorro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rut")
    private BigInteger rut;

    @Column(name = "dv")
    private String dv;

    @Column(name = "producto")
    private Integer producto;

    @Column(name = "moneda")
    private Integer moneda;

    @Column(name = "fecha_pago")
    private LocalDate fechaPago;

    @Column(name = "correlativo")
    private Integer correlativo;

    @Column(name = "fecha_inv")
    private LocalDate fechaInv;

    @Column(name = "folio")
    private String folio;

    @Column(name = "capital")
    private Double capital;

    @Column(name = "mon_op")
    private Integer monOp;

    @Column(name = "int_pagado")
    private Double intPagado;

    @Column(name = "int_real")
    private Double intReal;

    @Column(name = "mon_fi")
    private Integer monFi;

    @Column(name = "num_giro")
    private Integer numGiro;

    @Column(name = "cod_mon")
    private Integer codMon;

    @Column(name = "bipersonal")
    private String bipersonal;

    @Column(name = "sucursal")
    private String sucursal;

    @Column(name = "tasa_op")
    private Integer tasaOp;

    @Column(name = "nacionalidad")
    private String nacionalidad;

    @Column(name = "impto_35")
    private Integer impto35;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "comuna")
    private String comuna;

    @Column(name = "ciudad")
    private String ciudad;

    @Column(name = "fecha_ape")
    private LocalDate fechaApe;

    @Column(name = "fecha_con")
    private LocalDate fechaCon;

    @Column(name = "cuenta_cap")
    private Integer cuentaCap;

    @Column(name = "cuenta_int")
    private Integer cuentaInt;

    @Column(name = "cuenta_reaj")
    private Integer cuentaReaj;

    @Column(name = "reaj_pagado")
    private Double reajPagado;

    @Column(name = "fecha_ven")
    private LocalDate fechaVen;

    @Column(name = "reajustabilidad")
    private String reajustabilidad;

    @Column(name = "estado")
    private String estado;

    @Column(name = "p_57_bis")
    private String p57Bis;

    @Column(name = "mov_inmaduros")
    private String movInmaduros;

    @Column(name = "origen")
    private String origen;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getRut() {
        return rut;
    }

    public Ahorro rut(BigInteger rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(BigInteger rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public Ahorro dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getProducto() {
        return producto;
    }

    public Ahorro producto(Integer producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public Integer getMoneda() {
        return moneda;
    }

    public Ahorro moneda(Integer moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public Ahorro fechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Integer getCorrelativo() {
        return correlativo;
    }

    public Ahorro correlativo(Integer correlativo) {
        this.correlativo = correlativo;
        return this;
    }

    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }

    public LocalDate getFechaInv() {
        return fechaInv;
    }

    public Ahorro fechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
        return this;
    }

    public void setFechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFolio() {
        return folio;
    }

    public Ahorro folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Double getCapital() {
        return capital;
    }

    public Ahorro capital(Double capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Integer getMonOp() {
        return monOp;
    }

    public Ahorro monOp(Integer monOp) {
        this.monOp = monOp;
        return this;
    }

    public void setMonOp(Integer monOp) {
        this.monOp = monOp;
    }

    public Double getIntPagado() {
        return intPagado;
    }

    public Ahorro intPagado(Double intPagado) {
        this.intPagado = intPagado;
        return this;
    }

    public void setIntPagado(Double intPagado) {
        this.intPagado = intPagado;
    }

    public Double getIntReal() {
        return intReal;
    }

    public Ahorro intReal(Double intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(Double intReal) {
        this.intReal = intReal;
    }

    public Integer getMonFi() {
        return monFi;
    }

    public Ahorro monFi(Integer monFi) {
        this.monFi = monFi;
        return this;
    }

    public void setMonFi(Integer monFi) {
        this.monFi = monFi;
    }

    public Integer getNumGiro() {
        return numGiro;
    }

    public Ahorro numGiro(Integer numGiro) {
        this.numGiro = numGiro;
        return this;
    }

    public void setNumGiro(Integer numGiro) {
        this.numGiro = numGiro;
    }

    public Integer getCodMon() {
        return codMon;
    }

    public Ahorro codMon(Integer codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Integer codMon) {
        this.codMon = codMon;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public Ahorro bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public Ahorro sucursal(String sucursal) {
        this.sucursal = sucursal;
        return this;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public Integer getTasaOp() {
        return tasaOp;
    }

    public Ahorro tasaOp(Integer tasaOp) {
        this.tasaOp = tasaOp;
        return this;
    }

    public void setTasaOp(Integer tasaOp) {
        this.tasaOp = tasaOp;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public Ahorro nacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
        return this;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Integer getImpto35() {
        return impto35;
    }

    public Ahorro impto35(Integer impto35) {
        this.impto35 = impto35;
        return this;
    }

    public void setImpto35(Integer impto35) {
        this.impto35 = impto35;
    }

    public String getNombre() {
        return nombre;
    }

    public Ahorro nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public Ahorro direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public Ahorro comuna(String comuna) {
        this.comuna = comuna;
        return this;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCiudad() {
        return ciudad;
    }

    public Ahorro ciudad(String ciudad) {
        this.ciudad = ciudad;
        return this;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public LocalDate getFechaApe() {
        return fechaApe;
    }

    public Ahorro fechaApe(LocalDate fechaApe) {
        this.fechaApe = fechaApe;
        return this;
    }

    public void setFechaApe(LocalDate fechaApe) {
        this.fechaApe = fechaApe;
    }

    public LocalDate getFechaCon() {
        return fechaCon;
    }

    public Ahorro fechaCon(LocalDate fechaCon) {
        this.fechaCon = fechaCon;
        return this;
    }

    public void setFechaCon(LocalDate fechaCon) {
        this.fechaCon = fechaCon;
    }

    public Integer getCuentaCap() {
        return cuentaCap;
    }

    public Ahorro cuentaCap(Integer cuentaCap) {
        this.cuentaCap = cuentaCap;
        return this;
    }

    public void setCuentaCap(Integer cuentaCap) {
        this.cuentaCap = cuentaCap;
    }

    public Integer getCuentaInt() {
        return cuentaInt;
    }

    public Ahorro cuentaInt(Integer cuentaInt) {
        this.cuentaInt = cuentaInt;
        return this;
    }

    public void setCuentaInt(Integer cuentaInt) {
        this.cuentaInt = cuentaInt;
    }

    public Integer getCuentaReaj() {
        return cuentaReaj;
    }

    public Ahorro cuentaReaj(Integer cuentaReaj) {
        this.cuentaReaj = cuentaReaj;
        return this;
    }

    public void setCuentaReaj(Integer cuentaReaj) {
        this.cuentaReaj = cuentaReaj;
    }

    public Double getReajPagado() {
        return reajPagado;
    }

    public Ahorro reajPagado(Double reajPagado) {
        this.reajPagado = reajPagado;
        return this;
    }

    public void setReajPagado(Double reajPagado) {
        this.reajPagado = reajPagado;
    }

    public LocalDate getFechaVen() {
        return fechaVen;
    }

    public Ahorro fechaVen(LocalDate fechaVen) {
        this.fechaVen = fechaVen;
        return this;
    }

    public void setFechaVen(LocalDate fechaVen) {
        this.fechaVen = fechaVen;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public Ahorro reajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
        return this;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public String getEstado() {
        return estado;
    }

    public Ahorro estado(String estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getp57Bis() {
        return p57Bis;
    }

    public Ahorro p57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
        return this;
    }

    public void setp57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
    }

    public String getMovInmaduros() {
        return movInmaduros;
    }

    public Ahorro movInmaduros(String movInmaduros) {
        this.movInmaduros = movInmaduros;
        return this;
    }

    public void setMovInmaduros(String movInmaduros) {
        this.movInmaduros = movInmaduros;
    }

    public String getOrigen() {
        return origen;
    }

    public Ahorro origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Ahorro annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ahorro ahorro = (Ahorro) o;
        if (ahorro.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ahorro.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Ahorro{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", dv='" + getDv() + "'" +
            ", producto='" + getProducto() + "'" +
            ", moneda='" + getMoneda() + "'" +
            ", fechaPago='" + getFechaPago() + "'" +
            ", correlativo='" + getCorrelativo() + "'" +
            ", fechaInv='" + getFechaInv() + "'" +
            ", folio='" + getFolio() + "'" +
            ", capital='" + getCapital() + "'" +
            ", monOp='" + getMonOp() + "'" +
            ", intPagado='" + getIntPagado() + "'" +
            ", intReal='" + getIntReal() + "'" +
            ", monFi='" + getMonFi() + "'" +
            ", numGiro='" + getNumGiro() + "'" +
            ", codMon='" + getCodMon() + "'" +
            ", bipersonal='" + getBipersonal() + "'" +
            ", sucursal='" + getSucursal() + "'" +
            ", tasaOp='" + getTasaOp() + "'" +
            ", nacionalidad='" + getNacionalidad() + "'" +
            ", impto35='" + getImpto35() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", comuna='" + getComuna() + "'" +
            ", ciudad='" + getCiudad() + "'" +
            ", fechaApe='" + getFechaApe() + "'" +
            ", fechaCon='" + getFechaCon() + "'" +
            ", cuentaCap='" + getCuentaCap() + "'" +
            ", cuentaInt='" + getCuentaInt() + "'" +
            ", cuentaReaj='" + getCuentaReaj() + "'" +
            ", reajPagado='" + getReajPagado() + "'" +
            ", fechaVen='" + getFechaVen() + "'" +
            ", reajustabilidad='" + getReajustabilidad() + "'" +
            ", estado='" + getEstado() + "'" +
            ", p57Bis='" + getp57Bis() + "'" +
            ", movInmaduros='" + getMovInmaduros() + "'" +
            ", origen='" + getOrigen() + "'" +
            "}";
    }
}
