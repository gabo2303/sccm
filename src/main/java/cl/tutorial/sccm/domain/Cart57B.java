package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Cart57B.
 */
@Entity
@Table(name = "cart_57_b")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Cart57B implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "marca")
    private String marca;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "monto")
    private Float monto;

    @Column(name = "factor_act")
    private Float factorAct;

    @Max(value = 32)
    @Column(name = "monto_actualizado")
    private Integer montoActualizado;

    @Max(value = 3)
    @Column(name = "meses")
    private Integer meses;

    @Max(value = 32)
    @Column(name = "monto_proporcional")
    private Integer montoProporcional;

    @Max(value = 32)
    @Column(name = "monto_proporcional_sig")
    private Integer montoProporcionalSig;

    @Max(value = 32)
    @Column(name = "interes")
    private Integer interes;

    @Max(value = 32)
    @Column(name = "interes_actualizado")
    private Integer interesActualizado;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private ClienteBice clienteBice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public Cart57B marca(String marca) {
        this.marca = marca;
        return this;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public Cart57B tipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public Cart57B fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Float getMonto() {
        return monto;
    }

    public Cart57B monto(Float monto) {
        this.monto = monto;
        return this;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public Float getFactorAct() {
        return factorAct;
    }

    public Cart57B factorAct(Float factorAct) {
        this.factorAct = factorAct;
        return this;
    }

    public void setFactorAct(Float factorAct) {
        this.factorAct = factorAct;
    }

    public Integer getMontoActualizado() {
        return montoActualizado;
    }

    public Cart57B montoActualizado(Integer montoActualizado) {
        this.montoActualizado = montoActualizado;
        return this;
    }

    public void setMontoActualizado(Integer montoActualizado) {
        this.montoActualizado = montoActualizado;
    }

    public Integer getMeses() {
        return meses;
    }

    public Cart57B meses(Integer meses) {
        this.meses = meses;
        return this;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public Integer getMontoProporcional() {
        return montoProporcional;
    }

    public Cart57B montoProporcional(Integer montoProporcional) {
        this.montoProporcional = montoProporcional;
        return this;
    }

    public void setMontoProporcional(Integer montoProporcional) {
        this.montoProporcional = montoProporcional;
    }

    public Integer getMontoProporcionalSig() {
        return montoProporcionalSig;
    }

    public Cart57B montoProporcionalSig(Integer montoProporcionalSig) {
        this.montoProporcionalSig = montoProporcionalSig;
        return this;
    }

    public void setMontoProporcionalSig(Integer montoProporcionalSig) {
        this.montoProporcionalSig = montoProporcionalSig;
    }

    public Integer getInteres() {
        return interes;
    }

    public Cart57B interes(Integer interes) {
        this.interes = interes;
        return this;
    }

    public void setInteres(Integer interes) {
        this.interes = interes;
    }

    public Integer getInteresActualizado() {
        return interesActualizado;
    }

    public Cart57B interesActualizado(Integer interesActualizado) {
        this.interesActualizado = interesActualizado;
        return this;
    }

    public void setInteresActualizado(Integer interesActualizado) {
        this.interesActualizado = interesActualizado;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Cart57B annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public ClienteBice getClienteBice() {
        return clienteBice;
    }

    public Cart57B clienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
        return this;
    }

    public void setClienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cart57B cart57B = (Cart57B) o;
        if (cart57B.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cart57B.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Cart57B{" +
            "id=" + getId() +
            ", marca='" + getMarca() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", monto='" + getMonto() + "'" +
            ", factorAct='" + getFactorAct() + "'" +
            ", montoActualizado='" + getMontoActualizado() + "'" +
            ", meses='" + getMeses() + "'" +
            ", montoProporcional='" + getMontoProporcional() + "'" +
            ", montoProporcionalSig='" + getMontoProporcionalSig() + "'" +
            ", interes='" + getInteres() + "'" +
            ", interesActualizado='" + getInteresActualizado() + "'" +
            "}";
    }
}
