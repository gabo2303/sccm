package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Usd.
 */
@Entity
@Table(name = "usd")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Usd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "valor")
    private Double valor;

    @Column(name = "cod_mon")
    private Integer codMon;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public Usd fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Double getValor() {
        return valor;
    }

    public Usd valor(Double valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getCodMon() {
        return codMon;
    }

    public Usd codMon(Integer codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Integer codMon) {
        this.codMon = codMon;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Usd annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Usd usd = (Usd) o;
        if (usd.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usd.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Usd{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", valor='" + getValor() + "'" +
            ", codMon='" + getCodMon() + "'" +
            "}";
    }
}
