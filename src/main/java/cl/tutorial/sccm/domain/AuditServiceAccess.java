package cl.tutorial.sccm.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Representa los logs de auditoría a los servicios que se guardan en el sistema.
 *
 * @author Antonio Marrero Palomino.
 */
@ApiModel(description = "Representa los logs de auditoría a los servicios que se guardan en el sistema. @author Antonio Marrero Palomino.")
@Entity
@Table(name = "audit_service_access")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditServiceAccess implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Fecha y hora en la que se realiza la petición.
     */
    @ApiModelProperty(value = "Fecha y hora en la que se realiza la petición.")
    @Column(name = "jhi_timestamp")
    private ZonedDateTime timestamp;

    /**
     * Método HTTP de la petición.
     */
    @ApiModelProperty(value = "Método HTTP de la petición.")
    @Column(name = "method")
    private String method;

    /**
     * URL relativa del servicio invocado.
     */
    @ApiModelProperty(value = "URL relativa del servicio invocado.")
    @Column(name = "path")
    private String path;

    /**
     * Dirección IP desde donde se realiza la petición.
     */
    @ApiModelProperty(value = "Dirección IP desde donde se realiza la petición.")
    @Column(name = "remote_address")
    private String remoteAddress;

    /**
     * Tiempo en milisegundos que demora el servicioen dar respuesta.
     */
    @ApiModelProperty(value = "Tiempo en milisegundos que demora el servicioen dar respuesta.")
    @Column(name = "time_taken")
    private Long timeTaken;

    /**
     * Usuario que realiza la petición desde el servidor.
     */
    @ApiModelProperty(value = "Usuario que realiza la petición desde el servidor.")
    @Column(name = "user_principal")
    private String userPrincipal;

    /**
     * Usuario que realiza la petición desde el navegador.
     */
    @ApiModelProperty(value = "Usuario que realiza la petición desde el navegador.")
    @Column(name = "remote_user")
    private String remoteUser;

    /**
     * Representa los parámetros del servicio Ej: currency=13&page=0&size=100
     */
    @ApiModelProperty(value = "Representa los parámetros del servicio Ej: currency=13&page=0&size=100")
    @Column(name = "query")
    private String query;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public AuditServiceAccess timestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMethod() {
        return method;
    }

    public AuditServiceAccess method(String method) {
        this.method = method;
        return this;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public AuditServiceAccess path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public AuditServiceAccess remoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public Long getTimeTaken() {
        return timeTaken;
    }

    public AuditServiceAccess timeTaken(Long timeTaken) {
        this.timeTaken = timeTaken;
        return this;
    }

    public void setTimeTaken(Long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getUserPrincipal() {
        return userPrincipal;
    }

    public AuditServiceAccess userPrincipal(String userPrincipal) {
        this.userPrincipal = userPrincipal;
        return this;
    }

    public void setUserPrincipal(String userPrincipal) {
        this.userPrincipal = userPrincipal;
    }

    public String getRemoteUser() {
        return remoteUser;
    }

    public AuditServiceAccess remoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
        return this;
    }

    public void setRemoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
    }

    public String getQuery() {
        return query;
    }

    public AuditServiceAccess query(String query) {
        this.query = query;
        return this;
    }

    public void setQuery(String query) {
        this.query = query;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuditServiceAccess auditServiceAccess = (AuditServiceAccess) o;
        if (auditServiceAccess.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), auditServiceAccess.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuditServiceAccess{" + "id=" + getId() + ", timestamp='" + getTimestamp() + "'" + ", method='" + getMethod() + "'" + ", path='" + getPath() + "'"
            + ", remoteAddress='" + getRemoteAddress() + "'" + ", timeTaken='" + getTimeTaken() + "'" + ", userPrincipal='" + getUserPrincipal() + "'" + ", remoteUser='"
            + getRemoteUser() + "'" + ", query='" + getQuery() + "'" + "}";
    }
}
