package cl.tutorial.sccm.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A class representing view from VW_C_CUADRATURA_CONTABLE.
 */
@Entity
@Table(name = "VW_C_CUADRATURA_CONTABLE")
public class AhorroCC implements Serializable {

    @Id
    @Basic(optional = true)
    private Long    id;

    @Column(name = "sucursal")
    private Integer sucursal;

    @Column(name = "mes")
    private String  mes;

    @Column(name = "contabilidad")
    private Double  contabilidad;

    @Column(name = "interfaz_ahorro")
    private Double  interfazAhorro;

    @Column(name = "diferencia")
    private Double  diferencia;

    @Column(name = "dia1")
    private String  dia1;

    @Column(name = "dia2")
    private Integer dia2;

    @Column(name = "dia3")
    private String  dia3;

    @Column(name = "anno_tributa_id")
    private Long    annoTributa;

    public AhorroCC() {
    }

    public AhorroCC(Long id, Integer sucursal, String mes, Double contabilidad, Double interfazAhorro, Double diferencia, String dia1, Integer dia2, String dia3,
        Long annoTributa) {
        this.id = id;
        this.sucursal = sucursal;
        this.mes = mes;
        this.contabilidad = contabilidad;
        this.interfazAhorro = interfazAhorro;
        this.diferencia = diferencia;
        this.dia1 = dia1;
        this.dia2 = dia2;
        this.dia3 = dia3;
        this.annoTributa = annoTributa;
    }

    public AhorroCC(Integer sucursal, String mes, Double contabilidad, Double interfazAhorro, Double diferencia, String dia1, Integer dia2, String dia3, Long annoTributa) {
        this.sucursal = sucursal;
        this.mes = mes;
        this.contabilidad = contabilidad;
        this.interfazAhorro = interfazAhorro;
        this.diferencia = diferencia;
        this.dia1 = dia1;
        this.dia2 = dia2;
        this.dia3 = dia3;
        this.annoTributa = annoTributa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Double getContabilidad() {
        return contabilidad;
    }

    public void setContabilidad(Double contabilidad) {
        this.contabilidad = contabilidad;
    }

    public Double getInterfazAhorro() {
        return interfazAhorro;
    }

    public void setInterfazAhorro(Double interfazAhorro) {
        this.interfazAhorro = interfazAhorro;
    }

    public Double getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(Double diferencia) {
        this.diferencia = diferencia;
    }

    public String getDia1() {
        return dia1;
    }

    public void setDia1(String dia1) {
        this.dia1 = dia1;
    }

    public Integer getDia2() {
        return dia2;
    }

    public void setDia2(Integer dia2) {
        this.dia2 = dia2;
    }

    public String getDia3() {
        return dia3;
    }

    public void setDia3(String dia3) {
        this.dia3 = dia3;
    }

    public Long getAnnoTributa() {
        return annoTributa;
    }

    public void setAnnoTributa(Long annoTributa) {
        this.annoTributa = annoTributa;
    }
}
