package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import cl.tutorial.sccm.domain.enumeration.SCCMRole;

/**
 * A SCCMUserRole.
 */
@Entity
@Table(name = "sccm_user_role")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SCCMUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "sccm_system_user")
    private Integer sccmSystemUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "sccm_role")
    private SCCMRole sccmRole;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSccmSystemUser() {
        return sccmSystemUser;
    }

    public SCCMUserRole sccmSystemUser(Integer sccmSystemUser) {
        this.sccmSystemUser = sccmSystemUser;
        return this;
    }

    public void setSccmSystemUser(Integer sccmSystemUser) {
        this.sccmSystemUser = sccmSystemUser;
    }

    public SCCMRole getSccmRole() {
        return sccmRole;
    }

    public SCCMUserRole sccmRole(SCCMRole sccmRole) {
        this.sccmRole = sccmRole;
        return this;
    }

    public void setSccmRole(SCCMRole sccmRole) {
        this.sccmRole = sccmRole;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SCCMUserRole sCCMUserRole = (SCCMUserRole) o;
        if (sCCMUserRole.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sCCMUserRole.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SCCMUserRole{" +
            "id=" + getId() +
            ", sccmSystemUser='" + getSccmSystemUser() + "'" +
            ", sccmRole='" + getSccmRole() + "'" +
            "}";
    }
}
