package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CabeceraControlCambio.
 */
@Entity
@Table(name = "cabecera_control_cambio")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CabeceraControlCambio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "estado")
    private String estado;

    @Column(name = "nombre_tabla")
    private String nombreTabla;

    @Column(name = "accion")
    private String accion;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "fecha")
    private LocalDate fecha = LocalDate.now();

    @Column(name = "id_registro")
    private Long idRegistro;
	
	/*@OneToMany(mappedBy = "cabeceraControlCambio")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<DetalleControlCambio> detCont = new HashSet<>();*/

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public CabeceraControlCambio estado(String estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }

    public CabeceraControlCambio nombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
        return this;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public String getAccion() {
        return accion;
    }

    public CabeceraControlCambio accion(String accion) {
        this.accion = accion;
        return this;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getUsuario() {
        return usuario;
    }

    public CabeceraControlCambio usuario(String usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public CabeceraControlCambio fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Long getIdRegistro() {
        return idRegistro;
    }

    public CabeceraControlCambio idRegistro(Long idRegistro) {
        this.idRegistro = idRegistro;
        return this;
    }

    public void setIdRegistro(Long idRegistro) {
        this.idRegistro = idRegistro;
    }
	
	
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

	/*public Set<DetalleControlCambio> getDetCont() {
        return detCont;
    }

    public CabeceraControlCambio detCont(Set<DetalleControlCambio> detalles) {
        this.detCont = detalles;
        return this;
    }

    public CabeceraControlCambio removeDetCont(DetalleControlCambio detalles) {
        this.detCont.remove(detalles);
        detalles.setDetcabcontrol(null);
        return this;
    }

    public void setDetCont(Set<DetalleControlCambio> detalles) {
        this.detCont = detalles;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CabeceraControlCambio cabeceraControlCambio = (CabeceraControlCambio) o;
        if (cabeceraControlCambio.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cabeceraControlCambio.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CabeceraControlCambio{" +
            "id=" + getId() +
            ", estado='" + getEstado() + "'" +
            ", nombreTabla='" + getNombreTabla() + "'" +
            ", accion='" + getAccion() + "'" +
            ", usuario='" + getUsuario() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", idRegistro='" + getIdRegistro() + "'" +
            "}";
    }
}
