package cl.tutorial.sccm.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Representa la vista VW_L_B_C (Cuadratura de Bonos y Letras).
 *
 * @author Antonio Marrero Palomino.
 */
@ApiModel(description = "Representa la vista VW_L_B_C (Cuadratura de Bonos y Letras). @author Antonio Marrero Palomino.")
@Entity
@Table(name = "VW_L_B_C")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LetrasBonosCuadratura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    private Long id;

    @Column(name = "anno_tributa_id")
    private Long annoTributaId;

    @Column(name = "anno")
    private String anno;

    @Column(name = "instrumento")
    private String instrumento;

    @Column(name = "mes")
    private String mes;

    @Column(name = "moneda")
    private Long moneda;

    @Column(name = "monto_pagado", precision = 10, scale = 2)
    private BigDecimal montoPagado;

    @Column(name = "origen")
    private String origen;

    @Column(name = "valor_nominal", precision = 10, scale = 2)
    private BigDecimal valorNominal;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnnoTributaId() {
        return annoTributaId;
    }

    public LetrasBonosCuadratura annoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
        return this;
    }

    public void setAnnoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
    }

    public String getAnno() {
        return anno;
    }

    public LetrasBonosCuadratura anno(String anno) {
        this.anno = anno;
        return this;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public LetrasBonosCuadratura instrumento(String instrumento) {
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public String getMes() {
        return mes;
    }

    public LetrasBonosCuadratura mes(String mes) {
        this.mes = mes;
        return this;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Long getMoneda() {
        return moneda;
    }

    public LetrasBonosCuadratura moneda(Long moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Long moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoPagado() {
        return montoPagado;
    }

    public LetrasBonosCuadratura montoPagado(BigDecimal montoPagado) {
        this.montoPagado = montoPagado;
        return this;
    }

    public void setMontoPagado(BigDecimal montoPagado) {
        this.montoPagado = montoPagado;
    }

    public String getOrigen() {
        return origen;
    }

    public LetrasBonosCuadratura origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public BigDecimal getValorNominal() {
        return valorNominal;
    }

    public LetrasBonosCuadratura valorNominal(BigDecimal valorNominal) {
        this.valorNominal = valorNominal;
        return this;
    }

    public void setValorNominal(BigDecimal valorNominal) {
        this.valorNominal = valorNominal;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LetrasBonosCuadratura letrasBonosCuadratura = (LetrasBonosCuadratura) o;
        if (letrasBonosCuadratura.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), letrasBonosCuadratura.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LetrasBonosCuadratura{" + "id=" + getId() + ", anno='" + getAnno() + "'" + ", instrumento='" + getInstrumento() + "'" + ", mes='" + getMes() + "'" + ", moneda='"
            + getMoneda() + "'" + ", montoPagado='" + getMontoPagado() + "'" + ", origen='" + getOrigen() + "'" + ", valorNominal='" + getValorNominal() + "'" + "}";
    }
}
