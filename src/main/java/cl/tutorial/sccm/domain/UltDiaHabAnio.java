package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UltDiaHabAnio.
 */
@Entity
@Table(name = "ult_dia_anno")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UltDiaHabAnio implements Serializable 
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "mes")
    private Integer mes;

    @Column(name = "dia")
    private Integer dia;

    @Column(name = "fecha")
    private LocalDate fecha;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Integer getMes() { return mes; }

    public UltDiaHabAnio mes(Integer mes) 
	{
        this.mes = mes;
        
		return this;
    }

    public void setMes(Integer mes) { this.mes = mes; }

    public Integer getDia() { return dia; }

    public UltDiaHabAnio dia(Integer dia) 
	{
        this.dia = dia;
        
		return this;
    }

    public void setDia(Integer dia) { this.dia = dia; }

    public LocalDate getFecha() { return fecha; }

    public UltDiaHabAnio fecha(LocalDate fecha) 
	{
        this.fecha = fecha;
        
		return this;
    }

    public void setFecha(LocalDate fecha) { this.fecha = fecha; }

    public AnnoTributa getAnnoTributario() { return annoTributa; }

    public UltDiaHabAnio annoTributario(AnnoTributa annoTributa) 
	{
        this.annoTributa = annoTributa;
        
		return this;
    }

    public void setAnnoTributario(AnnoTributa annoTributa) { this.annoTributa = annoTributa; }
	
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove
    @Override
    public boolean equals(Object o) 
	{
        if (this == o) { return true; }
		
        if (o == null || getClass() != o.getClass()) { return false; }
		
        UltDiaHabAnio ultDiaHabAnio = (UltDiaHabAnio) o;
        
		if (ultDiaHabAnio.getId() == null || getId() == null) { return false; }
		
        return Objects.equals(getId(), ultDiaHabAnio.getId());
    }

    @Override
    public int hashCode() { return Objects.hashCode(getId()); }

    @Override
    public String toString() 
	{
        return "UltDiaHabAnio{" + "id=" + getId() + ", mes='" + getMes() + "'" + ", dia='" + getDia() + "'" + ", fecha='" + getFecha() + "'" + "}";
    }
}