package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SalIni57B.
 */
@Entity
@Table(name = "sal_ini_57_b")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class SalIni57B implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "moneda_nominal")
    private String monedaNominal;

    @Column(name = "saldo_neto_per")
    private Long saldoNetoPer;

    @Column(name = "saldo_prox_per")
    private Long saldoProxPer;

    @Column(name = "num_certificado")
    private Long numCertificado;

    @Column(name = "ano_act")
    private Long anoAct;

    @Column(name = "mes_act")
    private Long mesAct;

    @Column(name = "giro_rentab_nom")
    private Long giroRentabNom;

    @Column(name = "giro_rentab_pesos")
    private Long giroRentabPesos;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private ClienteBice clienteBice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public SalIni57B tipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMonedaNominal() {
        return monedaNominal;
    }

    public SalIni57B monedaNominal(String monedaNominal) {
        this.monedaNominal = monedaNominal;
        return this;
    }

    public void setMonedaNominal(String monedaNominal) {
        this.monedaNominal = monedaNominal;
    }

    public Long getSaldoNetoPer() {
        return saldoNetoPer;
    }

    public SalIni57B saldoNetoPer(Long saldoNetoPer) {
        this.saldoNetoPer = saldoNetoPer;
        return this;
    }

    public void setSaldoNetoPer(Long saldoNetoPer) {
        this.saldoNetoPer = saldoNetoPer;
    }

    public Long getSaldoProxPer() {
        return saldoProxPer;
    }

    public SalIni57B saldoProxPer(Long saldoProxPer) {
        this.saldoProxPer = saldoProxPer;
        return this;
    }

    public void setSaldoProxPer(Long saldoProxPer) {
        this.saldoProxPer = saldoProxPer;
    }

    public Long getNumCertificado() {
        return numCertificado;
    }

    public SalIni57B numCertificado(Long numCertificado) {
        this.numCertificado = numCertificado;
        return this;
    }

    public void setNumCertificado(Long numCertificado) {
        this.numCertificado = numCertificado;
    }

    public Long getAnoAct() {
        return anoAct;
    }

    public SalIni57B anoAct(Long anoAct) {
        this.anoAct = anoAct;
        return this;
    }

    public void setAnoAct(Long anoAct) {
        this.anoAct = anoAct;
    }

    public Long getMesAct() {
        return mesAct;
    }

    public SalIni57B mesAct(Long mesAct) {
        this.mesAct = mesAct;
        return this;
    }

    public void setMesAct(Long mesAct) {
        this.mesAct = mesAct;
    }

    public Long getGiroRentabNom() {
        return giroRentabNom;
    }

    public SalIni57B giroRentabNom(Long giroRentabNom) {
        this.giroRentabNom = giroRentabNom;
        return this;
    }

    public void setGiroRentabNom(Long giroRentabNom) {
        this.giroRentabNom = giroRentabNom;
    }

    public Long getGiroRentabPesos() {
        return giroRentabPesos;
    }

    public SalIni57B giroRentabPesos(Long giroRentabPesos) {
        this.giroRentabPesos = giroRentabPesos;
        return this;
    }

    public void setGiroRentabPesos(Long giroRentabPesos) {
        this.giroRentabPesos = giroRentabPesos;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public SalIni57B annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public ClienteBice getClienteBice() {
        return clienteBice;
    }

    public SalIni57B clienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
        return this;
    }

    public void setClienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SalIni57B salIni57B = (SalIni57B) o;
        if (salIni57B.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salIni57B.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SalIni57B{" +
            "id=" + getId() +
            ", tipo='" + getTipo() + "'" +
            ", monedaNominal='" + getMonedaNominal() + "'" +
            ", saldoNetoPer='" + getSaldoNetoPer() + "'" +
            ", saldoProxPer='" + getSaldoProxPer() + "'" +
            ", numCertificado='" + getNumCertificado() + "'" +
            ", anoAct='" + getAnoAct() + "'" +
            ", mesAct='" + getMesAct() + "'" +
            ", giroRentabNom='" + getGiroRentabNom() + "'" +
            ", giroRentabPesos='" + getGiroRentabPesos() + "'" +
            "}";
    }
}
