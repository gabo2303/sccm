package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SalProxPer.
 */
@Entity
@Table(name = "sal_prox_per")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class SalProxPer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tipo_sal")
    private String tipoSal;

    @Column(name = "saldo_neto_per")
    private Float saldoNetoPer;

    @Column(name = "saldo_prox_per")
    private Float saldoProxPer;

    @Column(name = "giro_rentab")
    private Float giroRentab;

    @Column(name = "anno_act")
    private Integer annoAct;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private ClienteBice clienteBice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoSal() {
        return tipoSal;
    }

    public SalProxPer tipoSal(String tipoSal) {
        this.tipoSal = tipoSal;
        return this;
    }

    public void setTipoSal(String tipoSal) {
        this.tipoSal = tipoSal;
    }

    public Float getSaldoNetoPer() {
        return saldoNetoPer;
    }

    public SalProxPer saldoNetoPer(Float saldoNetoPer) {
        this.saldoNetoPer = saldoNetoPer;
        return this;
    }

    public void setSaldoNetoPer(Float saldoNetoPer) {
        this.saldoNetoPer = saldoNetoPer;
    }

    public Float getSaldoProxPer() {
        return saldoProxPer;
    }

    public SalProxPer saldoProxPer(Float saldoProxPer) {
        this.saldoProxPer = saldoProxPer;
        return this;
    }

    public void setSaldoProxPer(Float saldoProxPer) {
        this.saldoProxPer = saldoProxPer;
    }

    public Float getGiroRentab() {
        return giroRentab;
    }

    public SalProxPer giroRentab(Float giroRentab) {
        this.giroRentab = giroRentab;
        return this;
    }

    public void setGiroRentab(Float giroRentab) {
        this.giroRentab = giroRentab;
    }

    public Integer getAnnoAct() {
        return annoAct;
    }

    public SalProxPer annoAct(Integer annoAct) {
        this.annoAct = annoAct;
        return this;
    }

    public void setAnnoAct(Integer annoAct) {
        this.annoAct = annoAct;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public SalProxPer annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public ClienteBice getClienteBice() {
        return clienteBice;
    }

    public SalProxPer clienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
        return this;
    }

    public void setClienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SalProxPer salProxPer = (SalProxPer) o;
        if (salProxPer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salProxPer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SalProxPer{" +
            "id=" + getId() +
            ", tipoSal='" + getTipoSal() + "'" +
            ", saldoNetoPer='" + getSaldoNetoPer() + "'" +
            ", saldoProxPer='" + getSaldoProxPer() + "'" +
            ", giroRentab='" + getGiroRentab() + "'" +
            ", annoAct='" + getAnnoAct() + "'" +
            "}";
    }
}
