package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CertVersion.
 */
@Entity
@Table(name = "cert_version")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class CertVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nro_certificado")
    private Integer nroCertificado;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private ClienteBice clienteBice;

    @ManyToOne
    private Instrumento instrumento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNroCertificado() {
        return nroCertificado;
    }

    public CertVersion nroCertificado(Integer nroCertificado) {
        this.nroCertificado = nroCertificado;
        return this;
    }

    public void setNroCertificado(Integer nroCertificado) {
        this.nroCertificado = nroCertificado;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public CertVersion annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public ClienteBice getClienteBice() {
        return clienteBice;
    }

    public CertVersion clienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
        return this;
    }

    public void setClienteBice(ClienteBice clienteBice) {
        this.clienteBice = clienteBice;
    }

    public Instrumento getInstrumento() {
        return instrumento;
    }

    public CertVersion instrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CertVersion certVersion = (CertVersion) o;
        if (certVersion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), certVersion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CertVersion{" +
            "id=" + getId() +
            ", nroCertificado='" + getNroCertificado() + "'" +
            "}";
    }
}
