package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DapCC.
 */
@Entity
@Table(name = "VW_DAP_CC_I_CUADRATURA_ALL")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DapCC implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    private Long id;

    @Column(name = "SUCURSAL")
    private Long sucursal;

    @Column(name = "CUENTA_INT")
    private Long cuentaInt;

    @Column(name = "COD_MONEDA")
    private Long codMoneda;

    @Column(name = "MONEDA_CONTABLE")
    private Integer monedaContable;

    @Column(name = "TIPO_PLAZO")
    private String tipoPlazo;

    @Column(name = "FECHA_CONTABLE")
    private LocalDate fechaContable;

    @Column(name = "FCC")
    private BigDecimal fcc;

    @Column(name = "CONTABILIDAD")
    private BigDecimal contabilidad;

    @Column(name = "DIFERENCIA")
    private BigDecimal diferencia;

    @Column(name = "DIFERENCIA_ABS")
    private BigDecimal diferenciaAbs;

    @Column(name = "ANNO_TRIBUTA_ID")
    private BigInteger annoTributaId;

    public DapCC() {
    }

    public DapCC(Long id, Long sucursal, Long cuentaInt, Long codMoneda, Integer monedaContable, String tipoPlazo, LocalDate fechaContable, BigDecimal fcc, BigDecimal contabilidad,
        BigDecimal diferencia, BigDecimal diferenciaAbs, BigInteger annoTributaId) {

        this.id = id;
        this.sucursal = sucursal;
        this.cuentaInt = cuentaInt;
        this.codMoneda = codMoneda;
        this.monedaContable = monedaContable;
        this.tipoPlazo = tipoPlazo;
        this.fechaContable = fechaContable;
        this.fcc = fcc;
        this.contabilidad = contabilidad;
        this.diferencia = diferencia;
        this.diferenciaAbs = diferenciaAbs;
        this.annoTributaId = annoTributaId;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSucursal() {
        return sucursal;
    }

    public DapCC sucursal(Long sucursal) {
        this.sucursal = sucursal;
        return this;
    }

    public void setSucursal(Long sucursal) {
        this.sucursal = sucursal;
    }

    public Long getCuentaInt() {
        return cuentaInt;
    }

    public DapCC cuentaInt(Long cuentaInt) {
        this.cuentaInt = cuentaInt;
        return this;
    }

    public void setCuentaInt(Long cuentaInt) {
        this.cuentaInt = cuentaInt;
    }

    public Long getCodMoneda() {
        return codMoneda;
    }

    public DapCC codMoneda(Long codMoneda) {
        this.codMoneda = codMoneda;
        return this;
    }

    public void setCodMoneda(Long codMoneda) {
        this.codMoneda = codMoneda;
    }

    public Integer getMonedaContable() {
        return monedaContable;
    }

    public DapCC monedaContable(Integer monedaContable) {
        this.monedaContable = monedaContable;
        return this;
    }

    public void setMonedaContable(Integer monedaContable) {
        this.monedaContable = monedaContable;
    }

    public String getTipoPlazo() {
        return tipoPlazo;
    }

    public DapCC tipoPlazo(String tipoPlazo) {
        this.tipoPlazo = tipoPlazo;
        return this;
    }

    public void setTipoPlazo(String tipoPlazo) {
        this.tipoPlazo = tipoPlazo;
    }

    public LocalDate getFechaContable() {
        return fechaContable;
    }

    public DapCC fechaContable(LocalDate fechaContable) {
        this.fechaContable = fechaContable;
        return this;
    }

    public void setFechaContable(LocalDate fechaContable) {
        this.fechaContable = fechaContable;
    }

    public BigDecimal getFcc() {
        return fcc;
    }

    public DapCC fcc(BigDecimal fcc) {
        this.fcc = fcc;
        return this;
    }

    public void setFcc(BigDecimal fcc) {
        this.fcc = fcc;
    }

    public BigDecimal getContabilidad() {
        return contabilidad;
    }

    public DapCC contabilidad(BigDecimal contabilidad) {
        this.contabilidad = contabilidad;
        return this;
    }

    public void setContabilidad(BigDecimal contabilidad) {
        this.contabilidad = contabilidad;
    }

    public BigDecimal getDiferencia() {
        return diferencia;
    }

    public DapCC diferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
        return this;
    }

    public void setDiferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
    }

    public BigDecimal getDiferenciaAbs() {
        return diferenciaAbs;
    }

    public DapCC diferenciaAbs(BigDecimal diferenciaAbs) {
        this.diferenciaAbs = diferenciaAbs;
        return this;
    }

    public void setDiferenciaAbs(BigDecimal diferenciaAbs) {
        this.diferenciaAbs = diferenciaAbs;
    }

    public BigInteger getAnnoTributaId() {
        return annoTributaId;
    }

    public DapCC annoTributaId(BigInteger annoTributaId) {
        this.annoTributaId = annoTributaId;
        return this;
    }

    public void setAnnoTributaId(BigInteger annoTributaId) {
        this.annoTributaId = annoTributaId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DapCC dapCC = (DapCC) o;
        if (dapCC.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dapCC.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DapCC{" + "id=" + getId() + ", sucursal='" + getSucursal() + "'" + ", cuentaInt='" + getCuentaInt() + "'" + ", codMoneda='" + getCodMoneda() + "'"
            + ", monedaContable='" + getMonedaContable() + "'" + ", tipoPlazo='" + getTipoPlazo() + "'" + ", fechaContable='" + getFechaContable() + "'" + ", fcc='" + getFcc()
            + "'" + ", contabilidad='" + getContabilidad() + "'" + ", diferencia='" + getDiferencia() + "'" + ", diferenciaAbs='" + getDiferenciaAbs() + "'" + ", annoTributaId='"
            + getAnnoTributaId() + "'" + "}";
    }
}
