package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A EntPagdiv.
 */
@Entity
@Table(name = "ent_pagdiv")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class EntPagdiv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_registro")
    private String idRegistro;

    @Column(name = "fecha_carga")
    private LocalDate fechaCarga;

    @Column(name = "usuario_carga")
    private String usuarioCarga;

    @Column(name = "reg_dividendos")
    private Integer regDividendos;

    @Column(name = "rut_tmp")
    private String rutTmp;

    @Column(name = "nombre_tmp")
    private String nombreTmp;

    @Column(name = "num_div_tmp")
    private Integer numDivTmp;

    @Column(name = "fe_pag_tmp")
    private LocalDate fePagTmp;

    @Column(name = "acciones_tmp")
    private Integer accionesTmp;

    @Column(name = "div_pesos_tmp")
    private Long divPesosTmp;

    @Column(name = "or_sec_tmp")
    private Integer orSecTmp;

    @Column(name = "pag_div_tmp")
    private Integer pagDivTmp;

    @Column(name = "filler")
    private String filler;

    @Column(name = "ok_rut")
    private Boolean okRut;

    @Column(name = "ok_fe_pag")
    private Boolean okFePag;

    @Column(name = "ok_nu_div")
    private Boolean okNuDiv;

    @Column(name = "ok_div_pesos")
    private Boolean okDivPesos;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdRegistro() {
        return idRegistro;
    }

    public EntPagdiv idRegistro(String idRegistro) {
        this.idRegistro = idRegistro;
        return this;
    }

    public void setIdRegistro(String idRegistro) {
        this.idRegistro = idRegistro;
    }

    public LocalDate getFechaCarga() {
        return fechaCarga;
    }

    public EntPagdiv fechaCarga(LocalDate fechaCarga) {
        this.fechaCarga = fechaCarga;
        return this;
    }

    public void setFechaCarga(LocalDate fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getUsuarioCarga() {
        return usuarioCarga;
    }

    public EntPagdiv usuarioCarga(String usuarioCarga) {
        this.usuarioCarga = usuarioCarga;
        return this;
    }

    public void setUsuarioCarga(String usuarioCarga) {
        this.usuarioCarga = usuarioCarga;
    }

    public Integer getRegDividendos() {
        return regDividendos;
    }

    public EntPagdiv regDividendos(Integer regDividendos) {
        this.regDividendos = regDividendos;
        return this;
    }

    public void setRegDividendos(Integer regDividendos) {
        this.regDividendos = regDividendos;
    }

    public String getRutTmp() {
        return rutTmp;
    }

    public EntPagdiv rutTmp(String rutTmp) {
        this.rutTmp = rutTmp;
        return this;
    }

    public void setRutTmp(String rutTmp) {
        this.rutTmp = rutTmp;
    }

    public String getNombreTmp() {
        return nombreTmp;
    }

    public EntPagdiv nombreTmp(String nombreTmp) {
        this.nombreTmp = nombreTmp;
        return this;
    }

    public void setNombreTmp(String nombreTmp) {
        this.nombreTmp = nombreTmp;
    }

    public Integer getNumDivTmp() {
        return numDivTmp;
    }

    public EntPagdiv numDivTmp(Integer numDivTmp) {
        this.numDivTmp = numDivTmp;
        return this;
    }

    public void setNumDivTmp(Integer numDivTmp) {
        this.numDivTmp = numDivTmp;
    }

    public LocalDate getFePagTmp() {
        return fePagTmp;
    }

    public EntPagdiv fePagTmp(LocalDate fePagTmp) {
        this.fePagTmp = fePagTmp;
        return this;
    }

    public void setFePagTmp(LocalDate fePagTmp) {
        this.fePagTmp = fePagTmp;
    }

    public Integer getAccionesTmp() {
        return accionesTmp;
    }

    public EntPagdiv accionesTmp(Integer accionesTmp) {
        this.accionesTmp = accionesTmp;
        return this;
    }

    public void setAccionesTmp(Integer accionesTmp) {
        this.accionesTmp = accionesTmp;
    }

    public Long getDivPesosTmp() {
        return divPesosTmp;
    }

    public EntPagdiv divPesosTmp(Long divPesosTmp) {
        this.divPesosTmp = divPesosTmp;
        return this;
    }

    public void setDivPesosTmp(Long divPesosTmp) {
        this.divPesosTmp = divPesosTmp;
    }

    public Integer getOrSecTmp() {
        return orSecTmp;
    }

    public EntPagdiv orSecTmp(Integer orSecTmp) {
        this.orSecTmp = orSecTmp;
        return this;
    }

    public void setOrSecTmp(Integer orSecTmp) {
        this.orSecTmp = orSecTmp;
    }

    public Integer getPagDivTmp() {
        return pagDivTmp;
    }

    public EntPagdiv pagDivTmp(Integer pagDivTmp) {
        this.pagDivTmp = pagDivTmp;
        return this;
    }

    public void setPagDivTmp(Integer pagDivTmp) {
        this.pagDivTmp = pagDivTmp;
    }

    public String getFiller() {
        return filler;
    }

    public EntPagdiv filler(String filler) {
        this.filler = filler;
        return this;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public Boolean isOkRut() {
        return okRut;
    }

    public EntPagdiv okRut(Boolean okRut) {
        this.okRut = okRut;
        return this;
    }

    public void setOkRut(Boolean okRut) {
        this.okRut = okRut;
    }

    public Boolean isOkFePag() {
        return okFePag;
    }

    public EntPagdiv okFePag(Boolean okFePag) {
        this.okFePag = okFePag;
        return this;
    }

    public void setOkFePag(Boolean okFePag) {
        this.okFePag = okFePag;
    }

    public Boolean isOkNuDiv() {
        return okNuDiv;
    }

    public EntPagdiv okNuDiv(Boolean okNuDiv) {
        this.okNuDiv = okNuDiv;
        return this;
    }

    public void setOkNuDiv(Boolean okNuDiv) {
        this.okNuDiv = okNuDiv;
    }

    public Boolean isOkDivPesos() {
        return okDivPesos;
    }

    public EntPagdiv okDivPesos(Boolean okDivPesos) {
        this.okDivPesos = okDivPesos;
        return this;
    }

    public void setOkDivPesos(Boolean okDivPesos) {
        this.okDivPesos = okDivPesos;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public EntPagdiv annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EntPagdiv entPagdiv = (EntPagdiv) o;
        if (entPagdiv.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entPagdiv.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EntPagdiv{" +
            "id=" + getId() +
            ", idRegistro='" + getIdRegistro() + "'" +
            ", fechaCarga='" + getFechaCarga() + "'" +
            ", usuarioCarga='" + getUsuarioCarga() + "'" +
            ", regDividendos='" + getRegDividendos() + "'" +
            ", rutTmp='" + getRutTmp() + "'" +
            ", nombreTmp='" + getNombreTmp() + "'" +
            ", numDivTmp='" + getNumDivTmp() + "'" +
            ", fePagTmp='" + getFePagTmp() + "'" +
            ", accionesTmp='" + getAccionesTmp() + "'" +
            ", divPesosTmp='" + getDivPesosTmp() + "'" +
            ", orSecTmp='" + getOrSecTmp() + "'" +
            ", pagDivTmp='" + getPagDivTmp() + "'" +
            ", filler='" + getFiller() + "'" +
            ", okRut='" + isOkRut() + "'" +
            ", okFePag='" + isOkFePag() + "'" +
            ", okNuDiv='" + isOkNuDiv() + "'" +
            ", okDivPesos='" + isOkDivPesos() + "'" +
            "}";
    }
}
