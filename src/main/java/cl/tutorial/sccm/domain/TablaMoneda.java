package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TablaMoneda.
 */
@Entity
@Table(name = "tabla_moneda")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class TablaMoneda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cod_mon")
    private Integer codMon;

    @Column(name = "glosa")
    private String glosa;

    @Column(name = "moneda")
    private Integer moneda;

    @Column(name = "simbolo")
    private String simbolo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCodMon() {
        return codMon;
    }

    public TablaMoneda codMon(Integer codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Integer codMon) {
        this.codMon = codMon;
    }

    public String getGlosa() {
        return glosa;
    }

    public TablaMoneda glosa(String glosa) {
        this.glosa = glosa;
        return this;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public Integer getMoneda() {
        return moneda;
    }

    public TablaMoneda moneda(Integer moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public TablaMoneda simbolo(String simbolo) {
        this.simbolo = simbolo;
        return this;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TablaMoneda tablaMoneda = (TablaMoneda) o;
        if (tablaMoneda.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tablaMoneda.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TablaMoneda{" +
            "id=" + getId() +
            ", codMon='" + getCodMon() + "'" +
            ", glosa='" + getGlosa() + "'" +
            ", moneda='" + getMoneda() + "'" +
            ", simbolo='" + getSimbolo() + "'" +
            "}";
    }
}
