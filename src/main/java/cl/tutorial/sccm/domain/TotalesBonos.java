package cl.tutorial.sccm.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Representa la vista VW_TOTALES_BONOS.
 *
 * @author Antonio Marrero Palomino.
 */
@ApiModel(description = "Representa la vista VW_TOTALES_BONOS. @author Antonio Marrero Palomino.")
@Entity
@Table(name = "VW_TOTALES_BONOS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TotalesBonos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    private Long id;

    @Column(name = "anno_tributa_id")
    private Long annoTributaId;

    @Column(name = "fecha_evento")
    private ZonedDateTime fechaEvento;

    @Column(name = "fecha_inversion")
    private ZonedDateTime fechaInversion;

    @Column(name = "fecha_pago")
    private ZonedDateTime fechaPago;

    @Column(name = "instrumento")
    private String instrumento;

    @Column(name = "moneda_pago")
    private Long monedaPago;

    @Column(name = "monto_pagado_mo", precision = 10, scale = 2)
    private BigDecimal montoPagadoMo;

    @Column(name = "monto_pagado_valor", precision = 10, scale = 2)
    private BigDecimal montoPagadoValor;

    @Column(name = "nombre_beneficiario")
    private String nombreBeneficiario;

    @Column(name = "num_cupon")
    private Long numCupon;

    @Column(name = "origen")
    private String origen;

    @Column(name = "rut_beneficiario")
    private String rutBeneficiario;

    @Column(name = "serie")
    private Long serie;

    @Column(name = "tipo_irf")
    private Long tipoIrf;

    @Column(name = "valor_nominal", precision = 10, scale = 2)
    private BigDecimal valorNominal;

    public TotalesBonos() {
    }

    public TotalesBonos(Long id, Long annoTributaId, ZonedDateTime fechaEvento, ZonedDateTime fechaInversion, ZonedDateTime fechaPago, String instrumento, Long monedaPago,
        BigDecimal montoPagadoMo, BigDecimal montoPagadoValor, String nombreBeneficiario, Long numCupon, String origen, String rutBeneficiario, Long serie, Long tipoIrf,
        BigDecimal valorNominal) {

        this.id = id;
        this.annoTributaId = annoTributaId;
        this.fechaEvento = fechaEvento;
        this.fechaInversion = fechaInversion;
        this.fechaPago = fechaPago;
        this.instrumento = instrumento;
        this.monedaPago = monedaPago;
        this.montoPagadoMo = montoPagadoMo;
        this.montoPagadoValor = montoPagadoValor;
        this.nombreBeneficiario = nombreBeneficiario;
        this.numCupon = numCupon;
        this.origen = origen;
        this.rutBeneficiario = rutBeneficiario;
        this.serie = serie;
        this.tipoIrf = tipoIrf;
        this.valorNominal = valorNominal;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnnoTributaId() {
        return annoTributaId;
    }

    public TotalesBonos annoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
        return this;
    }

    public void setAnnoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
    }

    public ZonedDateTime getFechaEvento() {
        return fechaEvento;
    }

    public TotalesBonos fechaEvento(ZonedDateTime fechaEvento) {
        this.fechaEvento = fechaEvento;
        return this;
    }

    public void setFechaEvento(ZonedDateTime fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public ZonedDateTime getFechaInversion() {
        return fechaInversion;
    }

    public TotalesBonos fechaInversion(ZonedDateTime fechaInversion) {
        this.fechaInversion = fechaInversion;
        return this;
    }

    public void setFechaInversion(ZonedDateTime fechaInversion) {
        this.fechaInversion = fechaInversion;
    }

    public ZonedDateTime getFechaPago() {
        return fechaPago;
    }

    public TotalesBonos fechaPago(ZonedDateTime fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(ZonedDateTime fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public TotalesBonos instrumento(String instrumento) {
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public Long getMonedaPago() {
        return monedaPago;
    }

    public TotalesBonos monedaPago(Long monedaPago) {
        this.monedaPago = monedaPago;
        return this;
    }

    public void setMonedaPago(Long monedaPago) {
        this.monedaPago = monedaPago;
    }

    public BigDecimal getMontoPagadoMo() {
        return montoPagadoMo;
    }

    public TotalesBonos montoPagadoMo(BigDecimal montoPagadoMo) {
        this.montoPagadoMo = montoPagadoMo;
        return this;
    }

    public void setMontoPagadoMo(BigDecimal montoPagadoMo) {
        this.montoPagadoMo = montoPagadoMo;
    }

    public BigDecimal getMontoPagadoValor() {
        return montoPagadoValor;
    }

    public TotalesBonos montoPagadoValor(BigDecimal montoPagadoValor) {
        this.montoPagadoValor = montoPagadoValor;
        return this;
    }

    public void setMontoPagadoValor(BigDecimal montoPagadoValor) {
        this.montoPagadoValor = montoPagadoValor;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public TotalesBonos nombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
        return this;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public Long getNumCupon() {
        return numCupon;
    }

    public TotalesBonos numCupon(Long numCupon) {
        this.numCupon = numCupon;
        return this;
    }

    public void setNumCupon(Long numCupon) {
        this.numCupon = numCupon;
    }

    public String getOrigen() {
        return origen;
    }

    public TotalesBonos origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getRutBeneficiario() {
        return rutBeneficiario;
    }

    public TotalesBonos rutBeneficiario(String rutBeneficiario) {
        this.rutBeneficiario = rutBeneficiario;
        return this;
    }

    public void setRutBeneficiario(String rutBeneficiario) {
        this.rutBeneficiario = rutBeneficiario;
    }

    public Long getSerie() {
        return serie;
    }

    public TotalesBonos serie(Long serie) {
        this.serie = serie;
        return this;
    }

    public void setSerie(Long serie) {
        this.serie = serie;
    }

    public Long getTipoIrf() {
        return tipoIrf;
    }

    public TotalesBonos tipoIrf(Long tipoIrf) {
        this.tipoIrf = tipoIrf;
        return this;
    }

    public void setTipoIrf(Long tipoIrf) {
        this.tipoIrf = tipoIrf;
    }

    public BigDecimal getValorNominal() {
        return valorNominal;
    }

    public TotalesBonos valorNominal(BigDecimal valorNominal) {
        this.valorNominal = valorNominal;
        return this;
    }

    public void setValorNominal(BigDecimal valorNominal) {
        this.valorNominal = valorNominal;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TotalesBonos totalesBonos = (TotalesBonos) o;
        if (totalesBonos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), totalesBonos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TotalesBonos{" + "id=" + getId() + ", fechaEvento='" + getFechaEvento() + "'" + ", fechaInversion='" + getFechaInversion() + "'" + ", fechaPago='" + getFechaPago()
            + "'" + ", instrumento='" + getInstrumento() + "'" + ", monedaPago='" + getMonedaPago() + "'" + ", montoPagadoMo='" + getMontoPagadoMo() + "'" + ", montoPagadoValor='"
            + getMontoPagadoValor() + "'" + ", nombreBeneficiario='" + getNombreBeneficiario() + "'" + ", numCupon='" + getNumCupon() + "'" + ", origen='" + getOrigen() + "'"
            + ", rutBeneficiario='" + getRutBeneficiario() + "'" + ", serie='" + getSerie() + "'" + ", tipoIrf='" + getTipoIrf() + "'" + ", valorNominal='" + getValorNominal()
            + "'" + "}";
    }
}
