package cl.tutorial.sccm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A AnnoTributa.
 */
@Entity
@Table(name = "anno_tributa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class AnnoTributa implements Serializable 
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id = Long.parseLong(String.valueOf(-1));

    @Column(name = "anno_t")
    private Integer annoT;

    @Column(name = "anno_c")
    private Integer annoC;
	
	@Column(name = "anno_activo")
    private String annoActivo;

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Instrumento> inst2S = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<CertPagdiv> certPagDivs = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<CorrecMoneta> corMons = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Representante> rep1S = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Fut> fut1S = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Glosa> glosas = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<EntPagdiv> ent1S = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<SalIni57B> sInis = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Cart57B> emiCarts = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Cert57B> emiCerts = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Entrada57b> ent57bs = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<SalProxPer> saldos = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Euro> euros = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Uf> ufs = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Usd> usds = new HashSet<>();

    @OneToMany(mappedBy = "annoTributa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotAudited
    private Set<Moneda> monedas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
    public Integer getAnnoT() {
        return annoT;
    }

    public AnnoTributa annoT(Integer annoT) {
        this.annoT = annoT;
        return this;
    }

    public void setAnnoT(Integer annoT) {
        this.annoT = annoT;
    }

    public Integer getAnnoC() {
        return annoC;
    }

    public AnnoTributa annoC(Integer annoC) {
        this.annoC = annoC;
        return this;
    }

    public void setAnnoC(Integer annoC) {
        this.annoC = annoC;
    }
	
	public String getAnnoActivo() {
        return annoActivo;
    }

    public AnnoTributa annoActivo(String annoActivo) {
        this.annoActivo = annoActivo;
        return this;
    }

    public void setAnnoActivo(String annoActivo) {
        this.annoActivo = annoActivo;
    }

    public Set<Instrumento> getInst2S() {
        return inst2S;
    }

    public AnnoTributa inst2S(Set<Instrumento> instrumentos) {
        this.inst2S = instrumentos;
        return this;
    }

    public AnnoTributa addInst2(Instrumento instrumento) {
        this.inst2S.add(instrumento);
        instrumento.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeInst2(Instrumento instrumento) {
        this.inst2S.remove(instrumento);
        instrumento.setAnnoTributa(null);
        return this;
    }

    public void setInst2S(Set<Instrumento> instrumentos) {
        this.inst2S = instrumentos;
    }

    public Set<CertPagdiv> getCertPagDivs() {
        return certPagDivs;
    }

    public AnnoTributa certPagDivs(Set<CertPagdiv> certPagdivs) {
        this.certPagDivs = certPagdivs;
        return this;
    }

    public AnnoTributa addCertPagDiv(CertPagdiv certPagdiv) {
        this.certPagDivs.add(certPagdiv);
        certPagdiv.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeCertPagDiv(CertPagdiv certPagdiv) {
        this.certPagDivs.remove(certPagdiv);
        certPagdiv.setAnnoTributa(null);
        return this;
    }

    public void setCertPagDivs(Set<CertPagdiv> certPagdivs) {
        this.certPagDivs = certPagdivs;
    }

    public Set<CorrecMoneta> getCorMons() {
        return corMons;
    }

    public AnnoTributa corMons(Set<CorrecMoneta> correcMonetas) {
        this.corMons = correcMonetas;
        return this;
    }

    public AnnoTributa addCorMon(CorrecMoneta correcMoneta) {
        this.corMons.add(correcMoneta);
        correcMoneta.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeCorMon(CorrecMoneta correcMoneta) {
        this.corMons.remove(correcMoneta);
        correcMoneta.setAnnoTributa(null);
        return this;
    }

    public void setCorMons(Set<CorrecMoneta> correcMonetas) {
        this.corMons = correcMonetas;
    }

    public Set<Representante> getRep1S() {
        return rep1S;
    }

    public AnnoTributa rep1S(Set<Representante> representantes) {
        this.rep1S = representantes;
        return this;
    }

    public AnnoTributa addRep1(Representante representante) {
        this.rep1S.add(representante);
        representante.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeRep1(Representante representante) {
        this.rep1S.remove(representante);
        representante.setAnnoTributa(null);
        return this;
    }

    public void setRep1S(Set<Representante> representantes) {
        this.rep1S = representantes;
    }

    public Set<Fut> getFut1S() {
        return fut1S;
    }

    public AnnoTributa fut1S(Set<Fut> futs) {
        this.fut1S = futs;
        return this;
    }

    public AnnoTributa addFut1(Fut fut) {
        this.fut1S.add(fut);
        fut.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeFut1(Fut fut) {
        this.fut1S.remove(fut);
        fut.setAnnoTributa(null);
        return this;
    }

    public void setFut1S(Set<Fut> futs) {
        this.fut1S = futs;
    }

    public Set<Glosa> getGlosas() {
        return glosas;
    }

    public AnnoTributa glosas(Set<Glosa> glosas) {
        this.glosas = glosas;
        return this;
    }

    public AnnoTributa addGlosa(Glosa glosa) {
        this.glosas.add(glosa);
        glosa.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeGlosa(Glosa glosa) {
        this.glosas.remove(glosa);
        glosa.setAnnoTributa(null);
        return this;
    }

    public void setGlosas(Set<Glosa> glosas) {
        this.glosas = glosas;
    }

    public Set<EntPagdiv> getEnt1S() {
        return ent1S;
    }

    public AnnoTributa ent1S(Set<EntPagdiv> entPagdivs) {
        this.ent1S = entPagdivs;
        return this;
    }

    public AnnoTributa addEnt1(EntPagdiv entPagdiv) {
        this.ent1S.add(entPagdiv);
        entPagdiv.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeEnt1(EntPagdiv entPagdiv) {
        this.ent1S.remove(entPagdiv);
        entPagdiv.setAnnoTributa(null);
        return this;
    }

    public void setEnt1S(Set<EntPagdiv> entPagdivs) {
        this.ent1S = entPagdivs;
    }

    public Set<SalIni57B> getSInis() {
        return sInis;
    }

    public AnnoTributa sInis(Set<SalIni57B> salIni57BS) {
        this.sInis = salIni57BS;
        return this;
    }

    public AnnoTributa addSIni(SalIni57B salIni57B) {
        this.sInis.add(salIni57B);
        salIni57B.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeSIni(SalIni57B salIni57B) {
        this.sInis.remove(salIni57B);
        salIni57B.setAnnoTributa(null);
        return this;
    }

    public void setSInis(Set<SalIni57B> salIni57BS) {
        this.sInis = salIni57BS;
    }

    public Set<Cart57B> getEmiCarts() {
        return emiCarts;
    }

    public AnnoTributa emiCarts(Set<Cart57B> cart57BS) {
        this.emiCarts = cart57BS;
        return this;
    }

    public AnnoTributa addEmiCart(Cart57B cart57B) {
        this.emiCarts.add(cart57B);
        cart57B.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeEmiCart(Cart57B cart57B) {
        this.emiCarts.remove(cart57B);
        cart57B.setAnnoTributa(null);
        return this;
    }

    public void setEmiCarts(Set<Cart57B> cart57BS) {
        this.emiCarts = cart57BS;
    }

    public Set<Cert57B> getEmiCerts() {
        return emiCerts;
    }

    public AnnoTributa emiCerts(Set<Cert57B> cert57BS) {
        this.emiCerts = cert57BS;
        return this;
    }

    public AnnoTributa addEmiCert(Cert57B cert57B) {
        this.emiCerts.add(cert57B);
        cert57B.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeEmiCert(Cert57B cert57B) {
        this.emiCerts.remove(cert57B);
        cert57B.setAnnoTributa(null);
        return this;
    }

    public void setEmiCerts(Set<Cert57B> cert57BS) {
        this.emiCerts = cert57BS;
    }

    public Set<Entrada57b> getEnt57bs() {
        return ent57bs;
    }

    public AnnoTributa ent57bs(Set<Entrada57b> entrada57bs) {
        this.ent57bs = entrada57bs;
        return this;
    }

    public AnnoTributa addEnt57b(Entrada57b entrada57b) {
        this.ent57bs.add(entrada57b);
        entrada57b.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeEnt57b(Entrada57b entrada57b) {
        this.ent57bs.remove(entrada57b);
        entrada57b.setAnnoTributa(null);
        return this;
    }

    public void setEnt57bs(Set<Entrada57b> entrada57bs) {
        this.ent57bs = entrada57bs;
    }

    public Set<SalProxPer> getSaldos() {
        return saldos;
    }

    public AnnoTributa saldos(Set<SalProxPer> salProxPers) {
        this.saldos = salProxPers;
        return this;
    }

    public AnnoTributa addSaldos(SalProxPer salProxPer) {
        this.saldos.add(salProxPer);
        salProxPer.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeSaldos(SalProxPer salProxPer) {
        this.saldos.remove(salProxPer);
        salProxPer.setAnnoTributa(null);
        return this;
    }

    public void setSaldos(Set<SalProxPer> salProxPers) {
        this.saldos = salProxPers;
    }

    public Set<Euro> getEuros() {
        return euros;
    }

    public AnnoTributa euros(Set<Euro> euros) {
        this.euros = euros;
        return this;
    }

    public AnnoTributa addEuro(Euro euro) {
        this.euros.add(euro);
        euro.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeEuro(Euro euro) {
        this.euros.remove(euro);
        euro.setAnnoTributa(null);
        return this;
    }

    public void setEuros(Set<Euro> euros) {
        this.euros = euros;
    }

    public Set<Uf> getUfs() {
        return ufs;
    }

    public AnnoTributa ufs(Set<Uf> ufs) {
        this.ufs = ufs;
        return this;
    }

    public AnnoTributa addUf(Uf uf) {
        this.ufs.add(uf);
        uf.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeUf(Uf uf) {
        this.ufs.remove(uf);
        uf.setAnnoTributa(null);
        return this;
    }

    public void setUfs(Set<Uf> ufs) {
        this.ufs = ufs;
    }

    public Set<Usd> getUsds() {
        return usds;
    }

    public AnnoTributa usds(Set<Usd> usds) {
        this.usds = usds;
        return this;
    }

    public AnnoTributa addUsd(Usd usd) {
        this.usds.add(usd);
        usd.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeUsd(Usd usd) {
        this.usds.remove(usd);
        usd.setAnnoTributa(null);
        return this;
    }

    public void setUsds(Set<Usd> usds) {
        this.usds = usds;
    }

    public Set<Moneda> getMonedas() {
        return monedas;
    }

    public AnnoTributa monedas(Set<Moneda> monedas) {
        this.monedas = monedas;
        return this;
    }

    public AnnoTributa addMoneda(Moneda moneda) {
        this.monedas.add(moneda);
        moneda.setAnnoTributa(this);
        return this;
    }

    public AnnoTributa removeMoneda(Moneda moneda) {
        this.monedas.remove(moneda);
        moneda.setAnnoTributa(null);
        return this;
    }

    public void setMonedas(Set<Moneda> monedas) {
        this.monedas = monedas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnnoTributa annoTributa = (AnnoTributa) o;
        if (annoTributa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), annoTributa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AnnoTributa{" +
            "id=" + getId() +
            ", annoT='" + getAnnoT() + "'" +			
            ", annoC='" + getAnnoC() + "'" +
			", annoActivo='" + getAnnoActivo() + "'" +
            "}";
    }
}
