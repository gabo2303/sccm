package cl.tutorial.sccm.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CompositeAudKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "REV", nullable = false, updatable = false)
    protected Integer revision;

    @Column(name = "ID", nullable = false, updatable = false)
    protected Long id;

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CompositeAudKey))
            return false;
        CompositeAudKey that = (CompositeAudKey) o;
        return Objects.equals(revision, that.revision) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(revision, id);
    }
}
