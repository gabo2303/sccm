package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DetalleControlCambio.
 */
@Entity
@Table(name = "detalle_control_cambio")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DetalleControlCambio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre_campo")
    private String nombreCampo;

    @Column(name = "valor_campo")
    private String valorCampo;

    @Column(name = "tipo_campo")
    private String tipoCampo;

	@Lob
    @Column(name = "imagen")
    private byte[] imagen;

    @ManyToOne
    private CabeceraControlCambio detcabcontrol;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCampo() {
        return nombreCampo;
    }

    public DetalleControlCambio nombreCampo(String nombreCampo) {
        this.nombreCampo = nombreCampo;
        return this;
    }

    public void setNombreCampo(String nombreCampo) {
        this.nombreCampo = nombreCampo;
    }

    public String getValorCampo() {
        return valorCampo;
    }

    public DetalleControlCambio valorCampo(String valorCampo) {
        this.valorCampo = valorCampo;
        return this;
    }

    public void setValorCampo(String valorCampo) {
        this.valorCampo = valorCampo;
    }

    public String getTipoCampo() {
        return tipoCampo;
    }

    public DetalleControlCambio tipoCampo(String tipoCampo) {
        this.tipoCampo = tipoCampo;
        return this;
    }

    public void setTipoCampo(String tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

	public byte[] getImagen() {
        return imagen;
    }

    public DetalleControlCambio imagen(byte[] imagen) {
        this.imagen = imagen;
        return this;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public CabeceraControlCambio getDetcabcontrol() {
        return detcabcontrol;
    }

    public DetalleControlCambio detcabcontrol(CabeceraControlCambio cabeceraControlCambio) {
        this.detcabcontrol = cabeceraControlCambio;
        return this;
    }

    public void setDetcabcontrol(CabeceraControlCambio cabeceraControlCambio) {
        this.detcabcontrol = cabeceraControlCambio;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DetalleControlCambio detalleControlCambio = (DetalleControlCambio) o;
        if (detalleControlCambio.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), detalleControlCambio.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DetalleControlCambio{" +
            "id=" + getId() +
            ", nombreCampo='" + getNombreCampo() + "'" +
            ", valorCampo='" + getValorCampo() + "'" +
            ", tipoCampo='" + getTipoCampo() + "'" +
            "}";
    }
}
