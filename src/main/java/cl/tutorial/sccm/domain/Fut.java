package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Fut.
 */
@Entity
@Table(name = "fut")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Fut implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "valor")
    private Float valor;

    @Column(name = "cred_imp_categoria")
    private Float credImpCategoria;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private Instrumento instrumento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getValor() {
        return valor;
    }

    public Fut valor(Float valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Float getCredImpCategoria() {
        return credImpCategoria;
    }

    public Fut credImpCategoria(Float credImpCategoria) {
        this.credImpCategoria = credImpCategoria;
        return this;
    }

    public void setCredImpCategoria(Float credImpCategoria) {
        this.credImpCategoria = credImpCategoria;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Fut annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public Instrumento getInstrumento() {
        return instrumento;
    }

    public Fut instrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fut fut = (Fut) o;
        if (fut.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fut.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Fut{" +
            "id=" + getId() +
            ", valor='" + getValor() + "'" +
            ", credImpCategoria='" + getCredImpCategoria() + "'" +
            "}";
    }
}
