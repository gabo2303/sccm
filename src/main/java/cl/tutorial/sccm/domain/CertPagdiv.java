package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A CertPagdiv.
 */
@Entity
@Table(name = "cert_pagdiv")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class CertPagdiv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_certificado")
    private Long idCertificado;

    @Column(name = "rut_accionista")
    private String rutAccionista;

    @Column(name = "nombre_accionista")
    private String nombreAccionista;

    @Column(name = "fecha_pago")
    private LocalDate fechaPago;

    @Column(name = "dividendo_nro")
    private Long dividendoNro;

    @Column(name = "monto_historico")
    private Long montoHistorico;

    @Column(name = "fct_act")
    private String fctAct;

    @Column(name = "monto_act")
    private Long montoAct;

    @Column(name = "ccred_idpc_generados")
    private Long ccredIdpcGenerados;

    @Column(name = "ccred_idpc_acumulados")
    private Long ccredIdpcAcumulados;

    @Column(name = "ccred_idpc_voluntario")
    private Long ccredIdpcVoluntario;

    @Column(name = "sdcredito")
    private Long sdcredito;

    @Column(name = "mon_exento_igcomp")
    private Long monExentoIgcomp;

    @Column(name = "monto_noconst_renta")
    private Long montoNoconstRenta;

    @Column(name = "rentas_trib_cumplida")
    private Long rentasTribCumplida;

    @Column(name = "rentas_generadas")
    private Long rentasGeneradas;

    @Column(name = "acum_ac_norest_sinder")
    private Long acumAcNorestSinder;

    @Column(name = "acum_ac_norest_conder")
    private Long acumAcNorestConder;

    @Column(name = "acum_ac_restric_sinder")
    private Long acumAcRestricSinder;

    @Column(name = "acum_ac_restric_conder")
    private Long acumAcRestricConder;

    @Column(name = "acum_ac_cred_total")
    private Long acumAcCredTotal;

    @Column(name = "acum_h_conder")
    private Long acumHConder;

    @Column(name = "acum_h_sinder")
    private Long acumHSinder;

    @Column(name = "acum_h_cred_total")
    private Long acumHCredTotal;

    @Column(name = "cred_imp_tasa_adic")
    private Long credImpTasaAdic;

    @Column(name = "tasa_efectiva_fut")
    private Float tasaEfectivaFut;

    @Column(name = "devolucion_capital")
    private Long devolucionCapital;

    @Column(name = "acciones")
    private Long acciones;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCertificado() {
        return idCertificado;
    }

    public CertPagdiv idCertificado(Long idCertificado) {
        this.idCertificado = idCertificado;
        return this;
    }

    public void setIdCertificado(Long idCertificado) {
        this.idCertificado = idCertificado;
    }

    public String getRutAccionista() {
        return rutAccionista;
    }

    public CertPagdiv rutAccionista(String rutAccionista) {
        this.rutAccionista = rutAccionista;
        return this;
    }

    public void setRutAccionista(String rutAccionista) {
        this.rutAccionista = rutAccionista;
    }

    public String getNombreAccionista() {
        return nombreAccionista;
    }

    public CertPagdiv nombreAccionista(String nombreAccionista) {
        this.nombreAccionista = nombreAccionista;
        return this;
    }

    public void setNombreAccionista(String nombreAccionista) {
        this.nombreAccionista = nombreAccionista;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public CertPagdiv fechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Long getDividendoNro() {
        return dividendoNro;
    }

    public CertPagdiv dividendoNro(Long dividendoNro) {
        this.dividendoNro = dividendoNro;
        return this;
    }

    public void setDividendoNro(Long dividendoNro) {
        this.dividendoNro = dividendoNro;
    }

    public Long getMontoHistorico() {
        return montoHistorico;
    }

    public CertPagdiv montoHistorico(Long montoHistorico) {
        this.montoHistorico = montoHistorico;
        return this;
    }

    public void setMontoHistorico(Long montoHistorico) {
        this.montoHistorico = montoHistorico;
    }

    public String getFctAct() {
        return fctAct;
    }

    public CertPagdiv fctAct(String fctAct) {
        this.fctAct = fctAct;
        return this;
    }

    public void setFctAct(String fctAct) {
        this.fctAct = fctAct;
    }

    public Long getMontoAct() {
        return montoAct;
    }

    public CertPagdiv montoAct(Long montoAct) {
        this.montoAct = montoAct;
        return this;
    }

    public void setMontoAct(Long montoAct) {
        this.montoAct = montoAct;
    }

    public Long getCcredIdpcGenerados() {
        return ccredIdpcGenerados;
    }

    public CertPagdiv ccredIdpcGenerados(Long ccredIdpcGenerados) {
        this.ccredIdpcGenerados = ccredIdpcGenerados;
        return this;
    }

    public void setCcredIdpcGenerados(Long ccredIdpcGenerados) {
        this.ccredIdpcGenerados = ccredIdpcGenerados;
    }

    public Long getCcredIdpcAcumulados() {
        return ccredIdpcAcumulados;
    }

    public CertPagdiv ccredIdpcAcumulados(Long ccredIdpcAcumulados) {
        this.ccredIdpcAcumulados = ccredIdpcAcumulados;
        return this;
    }

    public void setCcredIdpcAcumulados(Long ccredIdpcAcumulados) {
        this.ccredIdpcAcumulados = ccredIdpcAcumulados;
    }

    public Long getCcredIdpcVoluntario() {
        return ccredIdpcVoluntario;
    }

    public CertPagdiv ccredIdpcVoluntario(Long ccredIdpcVoluntario) {
        this.ccredIdpcVoluntario = ccredIdpcVoluntario;
        return this;
    }

    public void setCcredIdpcVoluntario(Long ccredIdpcVoluntario) {
        this.ccredIdpcVoluntario = ccredIdpcVoluntario;
    }

    public Long getSdcredito() {
        return sdcredito;
    }

    public CertPagdiv sdcredito(Long sdcredito) {
        this.sdcredito = sdcredito;
        return this;
    }

    public void setSdcredito(Long sdcredito) {
        this.sdcredito = sdcredito;
    }

    public Long getMonExentoIgcomp() {
        return monExentoIgcomp;
    }

    public CertPagdiv monExentoIgcomp(Long monExentoIgcomp) {
        this.monExentoIgcomp = monExentoIgcomp;
        return this;
    }

    public void setMonExentoIgcomp(Long monExentoIgcomp) {
        this.monExentoIgcomp = monExentoIgcomp;
    }

    public Long getMontoNoconstRenta() {
        return montoNoconstRenta;
    }

    public CertPagdiv montoNoconstRenta(Long montoNoconstRenta) {
        this.montoNoconstRenta = montoNoconstRenta;
        return this;
    }

    public void setMontoNoconstRenta(Long montoNoconstRenta) {
        this.montoNoconstRenta = montoNoconstRenta;
    }

    public Long getRentasTribCumplida() {
        return rentasTribCumplida;
    }

    public CertPagdiv rentasTribCumplida(Long rentasTribCumplida) {
        this.rentasTribCumplida = rentasTribCumplida;
        return this;
    }

    public void setRentasTribCumplida(Long rentasTribCumplida) {
        this.rentasTribCumplida = rentasTribCumplida;
    }

    public Long getRentasGeneradas() {
        return rentasGeneradas;
    }

    public CertPagdiv rentasGeneradas(Long rentasGeneradas) {
        this.rentasGeneradas = rentasGeneradas;
        return this;
    }

    public void setRentasGeneradas(Long rentasGeneradas) {
        this.rentasGeneradas = rentasGeneradas;
    }

    public Long getAcumAcNorestSinder() {
        return acumAcNorestSinder;
    }

    public CertPagdiv acumAcNorestSinder(Long acumAcNorestSinder) {
        this.acumAcNorestSinder = acumAcNorestSinder;
        return this;
    }

    public void setAcumAcNorestSinder(Long acumAcNorestSinder) {
        this.acumAcNorestSinder = acumAcNorestSinder;
    }

    public Long getAcumAcNorestConder() {
        return acumAcNorestConder;
    }

    public CertPagdiv acumAcNorestConder(Long acumAcNorestConder) {
        this.acumAcNorestConder = acumAcNorestConder;
        return this;
    }

    public void setAcumAcNorestConder(Long acumAcNorestConder) {
        this.acumAcNorestConder = acumAcNorestConder;
    }

    public Long getAcumAcRestricSinder() {
        return acumAcRestricSinder;
    }

    public CertPagdiv acumAcRestricSinder(Long acumAcRestricSinder) {
        this.acumAcRestricSinder = acumAcRestricSinder;
        return this;
    }

    public void setAcumAcRestricSinder(Long acumAcRestricSinder) {
        this.acumAcRestricSinder = acumAcRestricSinder;
    }

    public Long getAcumAcRestricConder() {
        return acumAcRestricConder;
    }

    public CertPagdiv acumAcRestricConder(Long acumAcRestricConder) {
        this.acumAcRestricConder = acumAcRestricConder;
        return this;
    }

    public void setAcumAcRestricConder(Long acumAcRestricConder) {
        this.acumAcRestricConder = acumAcRestricConder;
    }

    public Long getAcumAcCredTotal() {
        return acumAcCredTotal;
    }

    public CertPagdiv acumAcCredTotal(Long acumAcCredTotal) {
        this.acumAcCredTotal = acumAcCredTotal;
        return this;
    }

    public void setAcumAcCredTotal(Long acumAcCredTotal) {
        this.acumAcCredTotal = acumAcCredTotal;
    }

    public Long getAcumHConder() {
        return acumHConder;
    }

    public CertPagdiv acumHConder(Long acumHConder) {
        this.acumHConder = acumHConder;
        return this;
    }

    public void setAcumHConder(Long acumHConder) {
        this.acumHConder = acumHConder;
    }

    public Long getAcumHSinder() {
        return acumHSinder;
    }

    public CertPagdiv acumHSinder(Long acumHSinder) {
        this.acumHSinder = acumHSinder;
        return this;
    }

    public void setAcumHSinder(Long acumHSinder) {
        this.acumHSinder = acumHSinder;
    }

    public Long getAcumHCredTotal() {
        return acumHCredTotal;
    }

    public CertPagdiv acumHCredTotal(Long acumHCredTotal) {
        this.acumHCredTotal = acumHCredTotal;
        return this;
    }

    public void setAcumHCredTotal(Long acumHCredTotal) {
        this.acumHCredTotal = acumHCredTotal;
    }

    public Long getCredImpTasaAdic() {
        return credImpTasaAdic;
    }

    public CertPagdiv credImpTasaAdic(Long credImpTasaAdic) {
        this.credImpTasaAdic = credImpTasaAdic;
        return this;
    }

    public void setCredImpTasaAdic(Long credImpTasaAdic) {
        this.credImpTasaAdic = credImpTasaAdic;
    }

    public Float getTasaEfectivaFut() {
        return tasaEfectivaFut;
    }

    public CertPagdiv tasaEfectivaFut(Float tasaEfectivaFut) {
        this.tasaEfectivaFut = tasaEfectivaFut;
        return this;
    }

    public void setTasaEfectivaFut(Float tasaEfectivaFut) {
        this.tasaEfectivaFut = tasaEfectivaFut;
    }

    public Long getDevolucionCapital() {
        return devolucionCapital;
    }

    public CertPagdiv devolucionCapital(Long devolucionCapital) {
        this.devolucionCapital = devolucionCapital;
        return this;
    }

    public void setDevolucionCapital(Long devolucionCapital) {
        this.devolucionCapital = devolucionCapital;
    }

    public Long getAcciones() {
        return acciones;
    }

    public CertPagdiv acciones(Long acciones) {
        this.acciones = acciones;
        return this;
    }

    public void setAcciones(Long acciones) {
        this.acciones = acciones;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public CertPagdiv annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CertPagdiv certPagdiv = (CertPagdiv) o;
        if (certPagdiv.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), certPagdiv.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CertPagdiv{" +
            "id=" + getId() +
            ", idCertificado='" + getIdCertificado() + "'" +
            ", rutAccionista='" + getRutAccionista() + "'" +
            ", nombreAccionista='" + getNombreAccionista() + "'" +
            ", fechaPago='" + getFechaPago() + "'" +
            ", dividendoNro='" + getDividendoNro() + "'" +
            ", montoHistorico='" + getMontoHistorico() + "'" +
            ", fctAct='" + getFctAct() + "'" +
            ", montoAct='" + getMontoAct() + "'" +
            ", ccredIdpcGenerados='" + getCcredIdpcGenerados() + "'" +
            ", ccredIdpcAcumulados='" + getCcredIdpcAcumulados() + "'" +
            ", ccredIdpcVoluntario='" + getCcredIdpcVoluntario() + "'" +
            ", sdcredito='" + getSdcredito() + "'" +
            ", monExentoIgcomp='" + getMonExentoIgcomp() + "'" +
            ", montoNoconstRenta='" + getMontoNoconstRenta() + "'" +
            ", rentasTribCumplida='" + getRentasTribCumplida() + "'" +
            ", rentasGeneradas='" + getRentasGeneradas() + "'" +
            ", acumAcNorestSinder='" + getAcumAcNorestSinder() + "'" +
            ", acumAcNorestConder='" + getAcumAcNorestConder() + "'" +
            ", acumAcRestricSinder='" + getAcumAcRestricSinder() + "'" +
            ", acumAcRestricConder='" + getAcumAcRestricConder() + "'" +
            ", acumAcCredTotal='" + getAcumAcCredTotal() + "'" +
            ", acumHConder='" + getAcumHConder() + "'" +
            ", acumHSinder='" + getAcumHSinder() + "'" +
            ", acumHCredTotal='" + getAcumHCredTotal() + "'" +
            ", credImpTasaAdic='" + getCredImpTasaAdic() + "'" +
            ", tasaEfectivaFut='" + getTasaEfectivaFut() + "'" +
            ", devolucionCapital='" + getDevolucionCapital() + "'" +
            ", acciones='" + getAcciones() + "'" +
            "}";
    }
}
