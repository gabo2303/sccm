package cl.tutorial.sccm.domain.exception;

public class EtlExecutionException extends Exception {

    public EtlExecutionException() {
    }

    public EtlExecutionException(String message) {
        super(message);
    }

    public EtlExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public EtlExecutionException(Throwable cause) {
        super(cause);
    }
}
