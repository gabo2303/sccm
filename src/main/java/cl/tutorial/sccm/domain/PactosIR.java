package cl.tutorial.sccm.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A class representing view from pactos interes real calc.
 */

@Entity
@Table(name = "vw_pactos_int_real")
public class PactosIR implements Serializable {

    @Id
    @Basic(optional = false)
    private Long id;

    @Column(name = "rut")
    private Integer rut;

    @Column(name = "dv")
    private String dv;

    @Column(name = "folio")
    private String folio;

    @Column(name = "mon_pac")
    private String monPac;

    @Column(name = "fecha_pago")
    private LocalDate fechaPago;

    @Column(name = "fecha_inv")
    private LocalDate fechaInv;

    @Column(name = "int_real")
    private Long intReal;

    @Column(name = "int_real_cal")
    private Long intRealCal;

    @Column(name = "diferencia")
    private Long diferencia;

    @Column(name = "fecha_cont")
    private LocalDate fechaCont;

    @Column(name = "anno_tributa_id")
    private Long annoTributa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRut() {
        return rut;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getMonPac() {
        return monPac;
    }

    public void setMonPac(String monPac) {
        this.monPac = monPac;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public LocalDate getFechaInv() {
        return fechaInv;
    }

    public void setFechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
    }

    public Long getIntReal() {
        return intReal;
    }

    public void setIntReal(Long intReal) {
        this.intReal = intReal;
    }

    public Long getIntRealCal() {
        return intRealCal;
    }

    public void setIntRealCal(Long intRealCal) {
        this.intRealCal = intRealCal;
    }

    public Long getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(Long diferencia) {
        this.diferencia = diferencia;
    }

    public LocalDate getFechaCont() {
        return fechaCont;
    }

    public void setFechaCont(LocalDate fechaCont) {
        this.fechaCont = fechaCont;
    }

    public Long getAnnoTributa() {
        return annoTributa;
    }

    public void setAnnoTributa(Long annoTributa) {
        this.annoTributa = annoTributa;
    }

    public PactosIR() {

    }

    public PactosIR(Long id, Integer rut, String dv, String folio, String monPac, LocalDate fechaPago, LocalDate fechaInv, Long intReal, Long intRealCal, Long diferencia,
        LocalDate fechaCont, Long annoTributa) {
        this.id = id;
        this.rut = rut;
        this.dv = dv;
        this.folio = folio;
        this.monPac = monPac;
        this.fechaPago = fechaPago;
        this.fechaInv = fechaInv;
        this.intReal = intReal;
        this.intRealCal = intRealCal;
        this.diferencia = diferencia;
        this.fechaCont = fechaCont;
        this.annoTributa = annoTributa;
    }

    @Override
    public String toString() {
        return "PactosIR{" + "id=" + id + ", rut=" + rut + ", dv='" + dv + '\'' + ", folio=" + folio + ", monPac=" + monPac + ", fechaPago=" + fechaPago + ", fechaInv=" + fechaInv
            + ", intReal=" + intReal + ", intRealCal=" + intRealCal + ", diferencia=" + diferencia + ", fechaCont=" + fechaCont + ", annoTributa=" + annoTributa + '}';
    }
}
