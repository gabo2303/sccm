package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 * A Glosa.
 */
@Entity
@Table(name = "glosa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Glosa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Size(max = 1000)
    @Column(name = "valor", length = 1000)
    private String valor;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private Instrumento instrumento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Glosa nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public Glosa valor(String valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Glosa annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public Instrumento getInstrumento() {
        return instrumento;
    }

    public Glosa instrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Glosa glosa = (Glosa) o;
        if (glosa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), glosa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Glosa{" + "id=" + getId() + ", nombre='" + getNombre() + "'" + ", valor='" + getValor() + "'" + "}";
    }
}
