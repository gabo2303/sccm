package cl.tutorial.sccm.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Representa la vista VW_L_B (Bonos y Letras) que une las tablas BONOS, LHBONOS y LHDCV.
 *
 * @author Antonio Marrero Palomino.
 */
@ApiModel(description = "Representa la vista VW_L_B (Bonos y Letras) que une las tablas BONOS, LHBONOS y LHDCV. @author Antonio Marrero Palomino.")
@Entity
@Table(name = "VW_L_B")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LetrasBonos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    private Long id;

    @Column(name = "anno_tributa_id")
    private Long annoTributaId;

    @Column(name = "bipersonal")
    private String bipersonal;

    @Column(name = "capital", precision = 10, scale = 2)
    private BigDecimal capital;

    @Column(name = "cap_mon", precision = 10, scale = 2)
    private BigDecimal capMon;

    @Column(name = "cod_inst")
    private Long codInst;

    @Column(name = "cod_mon")
    private Long codMon;

    @Column(name = "cod_tipo_int")
    private Long codTipoInt;

    @Column(name = "correlativo")
    private Long correlativo;

    @Column(name = "corr_moneda")
    private Long corrMoneda;

    @Column(name = "cta_cap")
    private Long ctaCap;

    @Column(name = "cta_int")
    private Long ctaInt;

    @Column(name = "cta_reaj")
    private Long ctaReaj;

    @Column(name = "dv")
    private String dv;

    @Column(name = "est_cta")
    private String estCta;

    @Column(name = "extranjero")
    private String extranjero;

    @Column(name = "fecha_inv")
    private ZonedDateTime fechaInv;

    @Column(name = "fecha_pago")
    private ZonedDateTime fechaPago;

    @Column(name = "fec_cont")
    private ZonedDateTime fecCont;

    @Column(name = "fec_vcto")
    private ZonedDateTime fecVcto;

    @Column(name = "folio")
    private String folio;

    @Column(name = "id_table")
    private Long idTable;

    @Column(name = "imp_ext", precision = 10, scale = 2)
    private BigDecimal impExt;

    @Column(name = "ind_pago")
    private String indPago;

    @Column(name = "int_nom", precision = 10, scale = 2)
    private BigDecimal intNom;

    @Column(name = "int_real", precision = 10, scale = 2)
    private BigDecimal intReal;

    @Column(name = "mon_fi")
    private Long monFi;

    @Column(name = "mon_pac")
    private Long monPac;

    @Column(name = "mon_tasa", precision = 10, scale = 2)
    private BigDecimal monTasa;

    @Column(name = "nemotecnico")
    private String nemotecnico;

    @Column(name = "nom_cli")
    private String nomCli;

    @Column(name = "num_cupones")
    private Long numCupones;

    @Column(name = "num_cupon_pago")
    private Long numCuponPago;

    @Column(name = "num_giro")
    private Long numGiro;

    @Column(name = "onp")
    private String onp;

    @Column(name = "origen")
    private String origen;

    @Column(name = "per_tasa")
    private Long perTasa;

    @Column(name = "plazo")
    private Long plazo;

    @Column(name = "producto")
    private Long producto;

    @Column(name = "reajustabilidad")
    private String reajustabilidad;

    @Column(name = "rut")
    private Long rut;

    @Column(name = "serie_inst")
    private String serieInst;

    @Column(name = "tasa_emi", precision = 10, scale = 2)
    private BigDecimal tasaEmi;

    @Column(name = "tipo_int")
    private String tipoInt;

    @Column(name = "valor_nominal", precision = 10, scale = 2)
    private BigDecimal valorNominal;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnnoTributaId() {
        return annoTributaId;
    }

    public LetrasBonos annoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
        return this;
    }

    public void setAnnoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public LetrasBonos bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public BigDecimal getCapital() {
        return capital;
    }

    public LetrasBonos capital(BigDecimal capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(BigDecimal capital) {
        this.capital = capital;
    }

    public BigDecimal getCapMon() {
        return capMon;
    }

    public LetrasBonos capMon(BigDecimal capMon) {
        this.capMon = capMon;
        return this;
    }

    public void setCapMon(BigDecimal capMon) {
        this.capMon = capMon;
    }

    public Long getCodInst() {
        return codInst;
    }

    public LetrasBonos codInst(Long codInst) {
        this.codInst = codInst;
        return this;
    }

    public void setCodInst(Long codInst) {
        this.codInst = codInst;
    }

    public Long getCodMon() {
        return codMon;
    }

    public LetrasBonos codMon(Long codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Long codMon) {
        this.codMon = codMon;
    }

    public Long getCodTipoInt() {
        return codTipoInt;
    }

    public LetrasBonos codTipoInt(Long codTipoInt) {
        this.codTipoInt = codTipoInt;
        return this;
    }

    public void setCodTipoInt(Long codTipoInt) {
        this.codTipoInt = codTipoInt;
    }

    public Long getCorrelativo() {
        return correlativo;
    }

    public LetrasBonos correlativo(Long correlativo) {
        this.correlativo = correlativo;
        return this;
    }

    public void setCorrelativo(Long correlativo) {
        this.correlativo = correlativo;
    }

    public Long getCorrMoneda() {
        return corrMoneda;
    }

    public LetrasBonos corrMoneda(Long corrMoneda) {
        this.corrMoneda = corrMoneda;
        return this;
    }

    public void setCorrMoneda(Long corrMoneda) {
        this.corrMoneda = corrMoneda;
    }

    public Long getCtaCap() {
        return ctaCap;
    }

    public LetrasBonos ctaCap(Long ctaCap) {
        this.ctaCap = ctaCap;
        return this;
    }

    public void setCtaCap(Long ctaCap) {
        this.ctaCap = ctaCap;
    }

    public Long getCtaInt() {
        return ctaInt;
    }

    public LetrasBonos ctaInt(Long ctaInt) {
        this.ctaInt = ctaInt;
        return this;
    }

    public void setCtaInt(Long ctaInt) {
        this.ctaInt = ctaInt;
    }

    public Long getCtaReaj() {
        return ctaReaj;
    }

    public LetrasBonos ctaReaj(Long ctaReaj) {
        this.ctaReaj = ctaReaj;
        return this;
    }

    public void setCtaReaj(Long ctaReaj) {
        this.ctaReaj = ctaReaj;
    }

    public String getDv() {
        return dv;
    }

    public LetrasBonos dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public String getEstCta() {
        return estCta;
    }

    public LetrasBonos estCta(String estCta) {
        this.estCta = estCta;
        return this;
    }

    public void setEstCta(String estCta) {
        this.estCta = estCta;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public LetrasBonos extranjero(String extranjero) {
        this.extranjero = extranjero;
        return this;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }

    public ZonedDateTime getFechaInv() {
        return fechaInv;
    }

    public LetrasBonos fechaInv(ZonedDateTime fechaInv) {
        this.fechaInv = fechaInv;
        return this;
    }

    public void setFechaInv(ZonedDateTime fechaInv) {
        this.fechaInv = fechaInv;
    }

    public ZonedDateTime getFechaPago() {
        return fechaPago;
    }

    public LetrasBonos fechaPago(ZonedDateTime fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(ZonedDateTime fechaPago) {
        this.fechaPago = fechaPago;
    }

    public ZonedDateTime getFecCont() {
        return fecCont;
    }

    public LetrasBonos fecCont(ZonedDateTime fecCont) {
        this.fecCont = fecCont;
        return this;
    }

    public void setFecCont(ZonedDateTime fecCont) {
        this.fecCont = fecCont;
    }

    public ZonedDateTime getFecVcto() {
        return fecVcto;
    }

    public LetrasBonos fecVcto(ZonedDateTime fecVcto) {
        this.fecVcto = fecVcto;
        return this;
    }

    public void setFecVcto(ZonedDateTime fecVcto) {
        this.fecVcto = fecVcto;
    }

    public String getFolio() {
        return folio;
    }

    public LetrasBonos folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Long getIdTable() {
        return idTable;
    }

    public LetrasBonos idTable(Long idTable) {
        this.idTable = idTable;
        return this;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    public BigDecimal getImpExt() {
        return impExt;
    }

    public LetrasBonos impExt(BigDecimal impExt) {
        this.impExt = impExt;
        return this;
    }

    public void setImpExt(BigDecimal impExt) {
        this.impExt = impExt;
    }

    public String getIndPago() {
        return indPago;
    }

    public LetrasBonos indPago(String indPago) {
        this.indPago = indPago;
        return this;
    }

    public void setIndPago(String indPago) {
        this.indPago = indPago;
    }

    public BigDecimal getIntNom() {
        return intNom;
    }

    public LetrasBonos intNom(BigDecimal intNom) {
        this.intNom = intNom;
        return this;
    }

    public void setIntNom(BigDecimal intNom) {
        this.intNom = intNom;
    }

    public BigDecimal getIntReal() {
        return intReal;
    }

    public LetrasBonos intReal(BigDecimal intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(BigDecimal intReal) {
        this.intReal = intReal;
    }

    public Long getMonFi() {
        return monFi;
    }

    public LetrasBonos monFi(Long monFi) {
        this.monFi = monFi;
        return this;
    }

    public void setMonFi(Long monFi) {
        this.monFi = monFi;
    }

    public Long getMonPac() {
        return monPac;
    }

    public LetrasBonos monPac(Long monPac) {
        this.monPac = monPac;
        return this;
    }

    public void setMonPac(Long monPac) {
        this.monPac = monPac;
    }

    public BigDecimal getMonTasa() {
        return monTasa;
    }

    public LetrasBonos monTasa(BigDecimal monTasa) {
        this.monTasa = monTasa;
        return this;
    }

    public void setMonTasa(BigDecimal monTasa) {
        this.monTasa = monTasa;
    }

    public String getNemotecnico() {
        return nemotecnico;
    }

    public LetrasBonos nemotecnico(String nemotecnico) {
        this.nemotecnico = nemotecnico;
        return this;
    }

    public void setNemotecnico(String nemotecnico) {
        this.nemotecnico = nemotecnico;
    }

    public String getNomCli() {
        return nomCli;
    }

    public LetrasBonos nomCli(String nomCli) {
        this.nomCli = nomCli;
        return this;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public Long getNumCupones() {
        return numCupones;
    }

    public LetrasBonos numCupones(Long numCupones) {
        this.numCupones = numCupones;
        return this;
    }

    public void setNumCupones(Long numCupones) {
        this.numCupones = numCupones;
    }

    public Long getNumCuponPago() {
        return numCuponPago;
    }

    public LetrasBonos numCuponPago(Long numCuponPago) {
        this.numCuponPago = numCuponPago;
        return this;
    }

    public void setNumCuponPago(Long numCuponPago) {
        this.numCuponPago = numCuponPago;
    }

    public Long getNumGiro() {
        return numGiro;
    }

    public LetrasBonos numGiro(Long numGiro) {
        this.numGiro = numGiro;
        return this;
    }

    public void setNumGiro(Long numGiro) {
        this.numGiro = numGiro;
    }

    public String getOnp() {
        return onp;
    }

    public LetrasBonos onp(String onp) {
        this.onp = onp;
        return this;
    }

    public void setOnp(String onp) {
        this.onp = onp;
    }

    public String getOrigen() {
        return origen;
    }

    public LetrasBonos origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Long getPerTasa() {
        return perTasa;
    }

    public LetrasBonos perTasa(Long perTasa) {
        this.perTasa = perTasa;
        return this;
    }

    public void setPerTasa(Long perTasa) {
        this.perTasa = perTasa;
    }

    public Long getPlazo() {
        return plazo;
    }

    public LetrasBonos plazo(Long plazo) {
        this.plazo = plazo;
        return this;
    }

    public void setPlazo(Long plazo) {
        this.plazo = plazo;
    }

    public Long getProducto() {
        return producto;
    }

    public LetrasBonos producto(Long producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Long producto) {
        this.producto = producto;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public LetrasBonos reajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
        return this;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public Long getRut() {
        return rut;
    }

    public LetrasBonos rut(Long rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(Long rut) {
        this.rut = rut;
    }

    public String getSerieInst() {
        return serieInst;
    }

    public LetrasBonos serieInst(String serieInst) {
        this.serieInst = serieInst;
        return this;
    }

    public void setSerieInst(String serieInst) {
        this.serieInst = serieInst;
    }

    public BigDecimal getTasaEmi() {
        return tasaEmi;
    }

    public LetrasBonos tasaEmi(BigDecimal tasaEmi) {
        this.tasaEmi = tasaEmi;
        return this;
    }

    public void setTasaEmi(BigDecimal tasaEmi) {
        this.tasaEmi = tasaEmi;
    }

    public String getTipoInt() {
        return tipoInt;
    }

    public LetrasBonos tipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
        return this;
    }

    public void setTipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
    }

    public BigDecimal getValorNominal() {
        return valorNominal;
    }

    public LetrasBonos valorNominal(BigDecimal valorNominal) {
        this.valorNominal = valorNominal;
        return this;
    }

    public void setValorNominal(BigDecimal valorNominal) {
        this.valorNominal = valorNominal;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LetrasBonos letrasBonos = (LetrasBonos) o;
        if (letrasBonos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), letrasBonos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LetrasBonos{" + "id=" + getId() + ", annoTributaId='" + getAnnoTributaId() + "'" + ", bipersonal='" + getBipersonal() + "'" + ", capital='" + getCapital() + "'"
            + ", capMon='" + getCapMon() + "'" + ", codInst='" + getCodInst() + "'" + ", codMon='" + getCodMon() + "'" + ", codTipoInt='" + getCodTipoInt() + "'"
            + ", correlativo='" + getCorrelativo() + "'" + ", corrMoneda='" + getCorrMoneda() + "'" + ", ctaCap='" + getCtaCap() + "'" + ", ctaInt='" + getCtaInt() + "'"
            + ", ctaReaj='" + getCtaReaj() + "'" + ", dv='" + getDv() + "'" + ", estCta='" + getEstCta() + "'" + ", extranjero='" + getExtranjero() + "'" + ", fechaInv='"
            + getFechaInv() + "'" + ", fechaPago='" + getFechaPago() + "'" + ", fecCont='" + getFecCont() + "'" + ", fecVcto='" + getFecVcto() + "'" + ", folio='" + getFolio()
            + "'" + ", idTable='" + getIdTable() + "'" + ", impExt='" + getImpExt() + "'" + ", indPago='" + getIndPago() + "'" + ", intNom='" + getIntNom() + "'" + ", intReal='"
            + getIntReal() + "'" + ", monFi='" + getMonFi() + "'" + ", monPac='" + getMonPac() + "'" + ", monTasa='" + getMonTasa() + "'" + ", nemotecnico='" + getNemotecnico()
            + "'" + ", nomCli='" + getNomCli() + "'" + ", numCupones='" + getNumCupones() + "'" + ", numCuponPago='" + getNumCuponPago() + "'" + ", numGiro='" + getNumGiro() + "'"
            + ", onp='" + getOnp() + "'" + ", origen='" + getOrigen() + "'" + ", perTasa='" + getPerTasa() + "'" + ", plazo='" + getPlazo() + "'" + ", producto='" + getProducto()
            + "'" + ", reajustabilidad='" + getReajustabilidad() + "'" + ", rut='" + getRut() + "'" + ", serieInst='" + getSerieInst() + "'" + ", tasaEmi='" + getTasaEmi() + "'"
            + ", tipoInt='" + getTipoInt() + "'" + ", valorNominal='" + getValorNominal() + "'" + "}";
    }
}
