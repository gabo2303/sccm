package cl.tutorial.sccm.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Representa los tipos de alertas para DAP y Ahorro.
 *
 * @author Antonio Marrero Palomino.
 */
@ApiModel(description = "Representa los tipos de alertas para DAP y Ahorro. @author Antonio Marrero Palomino.")
@Entity
@Table(name = "tipo_alerta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class TipoAlerta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "codigo_unico")
    private String codigoUnico;

    @Column(name = "es_dap")
    private Boolean esDap;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public TipoAlerta nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoUnico() {
        return codigoUnico;
    }

    public TipoAlerta descripcion(String descripcion) {
        this.codigoUnico = descripcion;
        return this;
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public Boolean isEsDap() {
        return esDap;
    }

    public TipoAlerta esDap(Boolean esDap) {
        this.esDap = esDap;
        return this;
    }

    public void setEsDap(Boolean esDap) {
        this.esDap = esDap;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoAlerta tipoAlerta = (TipoAlerta) o;
        if (tipoAlerta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoAlerta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoAlerta{" + "id=" + getId() + ", nombre='" + getNombre() + "'" + ", codigoUnico='" + getCodigoUnico() + "'" + ", esDap='" + isEsDap() + "'" + "}";
    }
}
