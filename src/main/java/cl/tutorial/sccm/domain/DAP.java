package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DAP.
 */
@Entity
@Table(name = "dap")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class DAP implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "fec_vcto")
    private LocalDate fecVcto;

    @Column(name = "fecha_inv")
    private LocalDate fechaInv;

    @Column(name = "fec_cont")
    private LocalDate fecCont;

    @Column(name = "fecha_pago")
    private LocalDate fechaPago;

    @Column(name = "imp_ext")
    private Double impExt;

    @Column(name = "capital")
    private Double capital;

    @Column(name = "cap_mon")
    private Double capMon;

    @Column(name = "int_nom")
    private Double intNom;

    @Column(name = "mon_tasa")
    private Double monTasa;

    @Column(name = "tasa_op")
    private Double tasaOp;

    @Column(name = "int_pagado")
    private Double intPagado;

    @Column(name = "plazo")
    private BigInteger plazo;

    @Column(name = "num_cupones")
    private BigInteger numCupones;

    @Column(name = "num_cupon_pago")
    private BigInteger numCuponPago;

    @Column(name = "reaj_pagado")
    private BigInteger reajPagado;

    @Column(name = "sucursal")
    private BigInteger sucursal;

    @Column(name = "rut")
    private BigInteger rut;

    @Column(name = "producto")
    private BigInteger producto;

    @Column(name = "moneda")
    private BigInteger moneda;

    @Column(name = "correlativo")
    private BigInteger correlativo;

    @Column(name = "folio")
    private String folio;

    @Column(name = "int_real")
    private BigDecimal intReal;

    @Column(name = "mon_fi")
    private Double monFi;

    @Column(name = "num_giro")
    private BigInteger numGiro;

    @Column(name = "cod_mon")
    private BigInteger codMon;

    @Column(name = "cta_cap")
    private BigInteger ctaCap;

    @Column(name = "cod_tipo_int")
    private BigInteger codTipoInt;

    @Column(name = "per_tasa")
    private BigInteger perTasa;

    @Column(name = "cta_reaj")
    private BigInteger ctaReaj;

    @Column(name = "cta_int")
    private BigInteger ctaInt;

    @Column(name = "dv")
    private String dv;

    @Column(name = "tip_plazo")
    private String tipPlazo;

    @Column(name = "tipo_int")
    private String tipoInt;

    @Column(name = "onp")
    private String onp;

    @Column(name = "est_cta")
    private String estCta;

    @Column(name = "bipersonal")
    private String bipersonal;

    @Column(name = "reajustabilidad")
    private String reajustabilidad;

    @Column(name = "ind_pago")
    private String indPago;

    @Column(name = "p_57_bis")
    private String p57Bis;

    @Column(name = "extranjero")
    private String extranjero;

    @Column(name = "origen")
    private String origen;

    // [INT_PAGADO]/(1+[REAJ_PAGADO]/[CAPITAL]) AS [INT_NOM$]
    @Formula("INT_PAGADO / (1 + REAJ_PAGADO / CAPITAL)")
    @NotAudited
    private Double interesNominalPesos;

    // Round([INT_PAGADO]-[INT_NOM$],0)
    @Formula("INT_PAGADO - (INT_PAGADO / (1 + REAJ_PAGADO / CAPITAL))")
    @NotAudited
    private Double reajusteNegativo;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public DAP(LocalDate fecVcto, LocalDate fechaInv, LocalDate fecCont, LocalDate fechaPago, Double impExt, Double capital, Double capMon, Double intNom, Double monTasa,
        Double tasaOp, Double intPagado, BigInteger plazo, BigInteger numCupones, BigInteger numCuponPago, BigInteger reajPagado, BigInteger sucursal, BigInteger rut,
        BigInteger producto, BigInteger moneda, BigInteger correlativo, String folio, BigDecimal intReal, Double monFi, BigInteger numGiro, BigInteger codMon, BigInteger ctaCap,
        BigInteger codTipoInt, BigInteger perTasa, BigInteger ctaReaj, BigInteger ctaInt, String dv, String tipPlazo, String tipoInt, String onp, String estCta, String bipersonal,
        String reajustabilidad, String indPago, String p57Bis, String extranjero, String origen, AnnoTributa annoTributa) {

        this.fecVcto = fecVcto;
        this.fechaInv = fechaInv;
        this.fecCont = fecCont;
        this.fechaPago = fechaPago;
        this.impExt = impExt;
        this.capital = capital;
        this.capMon = capMon;
        this.intNom = intNom;
        this.monTasa = monTasa;
        this.tasaOp = tasaOp;
        this.intPagado = intPagado;
        this.plazo = plazo;
        this.numCupones = numCupones;
        this.numCuponPago = numCuponPago;
        this.reajPagado = reajPagado;
        this.sucursal = sucursal;
        this.rut = rut;
        this.producto = producto;
        this.moneda = moneda;
        this.correlativo = correlativo;
        this.folio = folio;
        this.intReal = intReal;
        this.monFi = monFi;
        this.numGiro = numGiro;
        this.codMon = codMon;
        this.ctaCap = ctaCap;
        this.codTipoInt = codTipoInt;
        this.perTasa = perTasa;
        this.ctaReaj = ctaReaj;
        this.ctaInt = ctaInt;
        this.dv = dv;
        this.tipPlazo = tipPlazo;
        this.tipoInt = tipoInt;
        this.onp = onp;
        this.estCta = estCta;
        this.bipersonal = bipersonal;
        this.reajustabilidad = reajustabilidad;
        this.indPago = indPago;
        this.p57Bis = p57Bis;
        this.extranjero = extranjero;
        this.origen = origen;
        this.annoTributa = annoTributa;
    }

    public DAP() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getRut() {
        return rut;
    }

    public DAP rut(BigInteger rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(BigInteger rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public DAP dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public BigInteger getProducto() {
        return producto;
    }

    public DAP producto(BigInteger producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(BigInteger producto) {
        this.producto = producto;
    }

    public BigInteger getMoneda() {
        return moneda;
    }

    public DAP moneda(BigInteger moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(BigInteger moneda) {
        this.moneda = moneda;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public DAP fechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public BigInteger getCorrelativo() {
        return correlativo;
    }

    public DAP correlativo(BigInteger correlativo) {
        this.correlativo = correlativo;
        return this;
    }

    public void setCorrelativo(BigInteger correlativo) {
        this.correlativo = correlativo;
    }

    public LocalDate getFechaInv() {
        return fechaInv;
    }

    public DAP fechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
        return this;
    }

    public void setFechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFolio() {
        return folio;
    }

    public DAP folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Double getCapital() {
        return capital;
    }

    public DAP capital(Double capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Double getCapMon() {
        return capMon;
    }

    public DAP capMon(Double capMon) {
        this.capMon = capMon;
        return this;
    }

    public void setCapMon(Double capMon) {
        this.capMon = capMon;
    }

    public Double getIntNom() {
        return intNom;
    }

    public DAP intNom(Double intNom) {
        this.intNom = intNom;
        return this;
    }

    public void setIntNom(Double intNom) {
        this.intNom = intNom;
    }

    public BigDecimal getIntReal() {
        return intReal;
    }

    public DAP intReal(BigDecimal intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(BigDecimal intReal) {
        this.intReal = intReal;
    }

    public Double getMonFi() {
        return monFi;
    }

    public DAP monFi(Double monFi) {
        this.monFi = monFi;
        return this;
    }

    public void setMonFi(Double monFi) {
        this.monFi = monFi;
    }

    public BigInteger getNumGiro() {
        return numGiro;
    }

    public DAP numGiro(BigInteger numGiro) {
        this.numGiro = numGiro;
        return this;
    }

    public void setNumGiro(BigInteger numGiro) {
        this.numGiro = numGiro;
    }

    public BigInteger getCodMon() {
        return codMon;
    }

    public DAP codMon(BigInteger codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(BigInteger codMon) {
        this.codMon = codMon;
    }

    public String getTipPlazo() {
        return tipPlazo;
    }

    public DAP tipPlazo(String tipPlazo) {
        this.tipPlazo = tipPlazo;
        return this;
    }

    public void setTipPlazo(String tipPlazo) {
        this.tipPlazo = tipPlazo;
    }

    public Double getMonTasa() {
        return monTasa;
    }

    public DAP monTasa(Double monTasa) {
        this.monTasa = monTasa;
        return this;
    }

    public void setMonTasa(Double monTasa) {
        this.monTasa = monTasa;
    }

    public Double getTasaOp() {
        return tasaOp;
    }

    public DAP tasaOp(Double tasaOp) {
        this.tasaOp = tasaOp;
        return this;
    }

    public void setTasaOp(Double tasaOp) {
        this.tasaOp = tasaOp;
    }

    public BigInteger getCtaCap() {
        return ctaCap;
    }

    public DAP ctaCap(BigInteger ctaCap) {
        this.ctaCap = ctaCap;
        return this;
    }

    public void setCtaCap(BigInteger ctaCap) {
        this.ctaCap = ctaCap;
    }

    public BigInteger getCtaInt() {
        return ctaInt;
    }

    public DAP ctaInt(BigInteger ctaInt) {
        this.ctaInt = ctaInt;
        return this;
    }

    public void setCtaInt(BigInteger ctaInt) {
        this.ctaInt = ctaInt;
    }

    public BigInteger getCtaReaj() {
        return ctaReaj;
    }

    public DAP ctaReaj(BigInteger ctaReaj) {
        this.ctaReaj = ctaReaj;
        return this;
    }

    public void setCtaReaj(BigInteger ctaReaj) {
        this.ctaReaj = ctaReaj;
    }

    public BigInteger getPerTasa() {
        return perTasa;
    }

    public DAP perTasa(BigInteger perTasa) {
        this.perTasa = perTasa;
        return this;
    }

    public void setPerTasa(BigInteger perTasa) {
        this.perTasa = perTasa;
    }

    public String getTipoInt() {
        return tipoInt;
    }

    public DAP tipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
        return this;
    }

    public void setTipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
    }

    public BigInteger getCodTipoInt() {
        return codTipoInt;
    }

    public DAP codTipoInt(BigInteger codTipoInt) {
        this.codTipoInt = codTipoInt;
        return this;
    }

    public void setCodTipoInt(BigInteger codTipoInt) {
        this.codTipoInt = codTipoInt;
    }

    public LocalDate getFecVcto() {
        return fecVcto;
    }

    public DAP fecVcto(LocalDate fecVcto) {
        this.fecVcto = fecVcto;
        return this;
    }

    public void setFecVcto(LocalDate fecVcto) {
        this.fecVcto = fecVcto;
    }

    public String getOnp() {
        return onp;
    }

    public DAP onp(String onp) {
        this.onp = onp;
        return this;
    }

    public void setOnp(String onp) {
        this.onp = onp;
    }

    public String getEstCta() {
        return estCta;
    }

    public DAP estCta(String estCta) {
        this.estCta = estCta;
        return this;
    }

    public void setEstCta(String estCta) {
        this.estCta = estCta;
    }

    public LocalDate getFecCont() {
        return fecCont;
    }

    public DAP fecCont(LocalDate fecCont) {
        this.fecCont = fecCont;
        return this;
    }

    public void setFecCont(LocalDate fecCont) {
        this.fecCont = fecCont;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public DAP bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public DAP reajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
        return this;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public BigInteger getPlazo() {
        return plazo;
    }

    public DAP plazo(BigInteger plazo) {
        this.plazo = plazo;
        return this;
    }

    public void setPlazo(BigInteger plazo) {
        this.plazo = plazo;
    }

    public String getIndPago() {
        return indPago;
    }

    public DAP indPago(String indPago) {
        this.indPago = indPago;
        return this;
    }

    public void setIndPago(String indPago) {
        this.indPago = indPago;
    }

    public String getp57Bis() {
        return p57Bis;
    }

    public DAP p57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
        return this;
    }

    public void setp57Bis(String p57Bis) {
        this.p57Bis = p57Bis;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public DAP extranjero(String extranjero) {
        this.extranjero = extranjero;
        return this;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }

    public Double getImpExt() {
        return impExt;
    }

    public DAP impExt(Double impExt) {
        this.impExt = impExt;
        return this;
    }

    public void setImpExt(Double impExt) {
        this.impExt = impExt;
    }

    public BigInteger getNumCupones() {
        return numCupones;
    }

    public DAP numCupones(BigInteger numCupones) {
        this.numCupones = numCupones;
        return this;
    }

    public void setNumCupones(BigInteger numCupones) {
        this.numCupones = numCupones;
    }

    public BigInteger getNumCuponPago() {
        return numCuponPago;
    }

    public DAP numCuponPago(BigInteger numCuponPago) {
        this.numCuponPago = numCuponPago;
        return this;
    }

    public void setNumCuponPago(BigInteger numCuponPago) {
        this.numCuponPago = numCuponPago;
    }

    public String getOrigen() {
        return origen;
    }

    public DAP origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public BigInteger getReajPagado() {
        return reajPagado;
    }

    public DAP reajPagado(BigInteger reajPagado) {
        this.reajPagado = reajPagado;
        return this;
    }

    public void setReajPagado(BigInteger reajPagado) {
        this.reajPagado = reajPagado;
    }

    public Double getIntPagado() {
        return intPagado;
    }

    public DAP intPagado(Double intPagado) {
        this.intPagado = intPagado;
        return this;
    }

    public void setIntPagado(Double intPagado) {
        this.intPagado = intPagado;
    }

    public BigInteger getSucursal() {
        return sucursal;
    }

    public DAP sucursal(BigInteger sucursal) {
        this.sucursal = sucursal;
        return this;
    }

    public void setSucursal(BigInteger sucursal) {
        this.sucursal = sucursal;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public DAP annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public Double getInteresNominalPesos() {
        return interesNominalPesos;
    }

    public void setInteresNominalPesos(Double interesNominalPesos) {
        this.interesNominalPesos = interesNominalPesos;
    }

    public Double getReajusteNegativo() {
        return reajusteNegativo;
    }

    public void setReajusteNegativo(Double reajusteNegativo) {
        this.reajusteNegativo = reajusteNegativo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DAP dAP = (DAP) o;
        if (dAP.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dAP.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DAP{" + "id=" + id + ", fecVcto=" + fecVcto + ", fechaInv=" + fechaInv + ", fecCont=" + fecCont + ", fechaPago=" + fechaPago + ", impExt=" + impExt + ", capital="
            + capital + ", capMon=" + capMon + ", intNom=" + intNom + ", monTasa=" + monTasa + ", tasaOp=" + tasaOp + ", intPagado=" + intPagado + ", plazo=" + plazo
            + ", numCupones=" + numCupones + ", numCuponPago=" + numCuponPago + ", reajPagado=" + reajPagado + ", sucursal=" + sucursal + ", rut=" + rut + ", producto=" + producto
            + ", moneda=" + moneda + ", correlativo=" + correlativo + ", folio=" + folio + ", intReal=" + intReal + ", monFi=" + monFi + ", numGiro=" + numGiro + ", codMon="
            + codMon + ", ctaCap=" + ctaCap + ", codTipoInt=" + codTipoInt + ", perTasa=" + perTasa + ", ctaReaj=" + ctaReaj + ", ctaInt=" + ctaInt + ", dv='" + dv + '\''
            + ", tipPlazo='" + tipPlazo + '\'' + ", tipoInt='" + tipoInt + '\'' + ", onp='" + onp + '\'' + ", estCta='" + estCta + '\'' + ", bipersonal='" + bipersonal + '\''
            + ", reajustabilidad='" + reajustabilidad + '\'' + ", indPago='" + indPago + '\'' + ", p57Bis='" + p57Bis + '\'' + ", extranjero='" + extranjero + '\'' + ", origen='"
            + origen + '\'' + ", interesNominalPesos='" + interesNominalPesos + '\'' + ", reajusteNegativo='" + reajusteNegativo + '\'' + ", annoTributa=" + annoTributa + '}';
    }
}
