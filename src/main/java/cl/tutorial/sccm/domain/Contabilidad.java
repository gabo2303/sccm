package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Contabilidad.
 */
@Entity
@Table(name = "contabilidad")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Contabilidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "sucursal")
    private Integer sucursal;

    @Column(name = "moneda")
    private Integer moneda;

    @Column(name = "cta_cont_gl")
    private Double ctaContGl;

    @Column(name = "cta_cont_alp")
    private Double ctaContAlp;

    @Column(name = "fecha")
    private Instant fecha;

    @Column(name = "monto_dr")
    private Double montoDr;

    @Column(name = "monto_cr")
    private Double montoCr;

    @Column(name = "suc_origen")
    private Integer sucOrigen;

    @Column(name = "modulo")
    private String modulo;

    @Column(name = "num_docto")
    private String numDocto;

    @Column(name = "cta_sbif")
    private String ctaSbif;

    @Column(name = "glosa")
    private String glosa;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public Contabilidad sucursal(Integer sucursal) {
        this.sucursal = sucursal;
        return this;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public Integer getMoneda() {
        return moneda;
    }

    public Contabilidad moneda(Integer moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }

    public Double getCtaContGl() {
        return ctaContGl;
    }

    public Contabilidad ctaContGl(Double ctaContGl) {
        this.ctaContGl = ctaContGl;
        return this;
    }

    public void setCtaContGl(Double ctaContGl) {
        this.ctaContGl = ctaContGl;
    }

    public Double getCtaContAlp() {
        return ctaContAlp;
    }

    public Contabilidad ctaContAlp(Double ctaContAlp) {
        this.ctaContAlp = ctaContAlp;
        return this;
    }

    public void setCtaContAlp(Double ctaContAlp) {
        this.ctaContAlp = ctaContAlp;
    }

    public Instant getFecha() {
        return fecha;
    }

    public Contabilidad fecha(Instant fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public Double getMontoDr() {
        return montoDr;
    }

    public Contabilidad montoDr(Double montoDr) {
        this.montoDr = montoDr;
        return this;
    }

    public void setMontoDr(Double montoDr) {
        this.montoDr = montoDr;
    }

    public Double getMontoCr() {
        return montoCr;
    }

    public Contabilidad montoCr(Double montoCr) {
        this.montoCr = montoCr;
        return this;
    }

    public void setMontoCr(Double montoCr) {
        this.montoCr = montoCr;
    }

    public Integer getSucOrigen() {
        return sucOrigen;
    }

    public Contabilidad sucOrigen(Integer sucOrigen) {
        this.sucOrigen = sucOrigen;
        return this;
    }

    public void setSucOrigen(Integer sucOrigen) {
        this.sucOrigen = sucOrigen;
    }

    public String getModulo() {
        return modulo;
    }

    public Contabilidad modulo(String modulo) {
        this.modulo = modulo;
        return this;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getNumDocto() {
        return numDocto;
    }

    public Contabilidad numDocto(String numDocto) {
        this.numDocto = numDocto;
        return this;
    }

    public void setNumDocto(String numDocto) {
        this.numDocto = numDocto;
    }

    public String getCtaSbif() {
        return ctaSbif;
    }

    public Contabilidad ctaSbif(String ctaSbif) {
        this.ctaSbif = ctaSbif;
        return this;
    }

    public void setCtaSbif(String ctaSbif) {
        this.ctaSbif = ctaSbif;
    }

    public String getGlosa() {
        return glosa;
    }

    public Contabilidad glosa(String glosa) {
        this.glosa = glosa;
        return this;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Contabilidad annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contabilidad contabilidad = (Contabilidad) o;
        if (contabilidad.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contabilidad.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Contabilidad{" +
            "id=" + getId() +
            ", sucursal='" + getSucursal() + "'" +
            ", moneda='" + getMoneda() + "'" +
            ", ctaContGl='" + getCtaContGl() + "'" +
            ", ctaContAlp='" + getCtaContAlp() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", montoDr='" + getMontoDr() + "'" +
            ", montoCr='" + getMontoCr() + "'" +
            ", sucOrigen='" + getSucOrigen() + "'" +
            ", modulo='" + getModulo() + "'" +
            ", numDocto='" + getNumDocto() + "'" +
            ", ctaSbif='" + getCtaSbif() + "'" +
            ", glosa='" + getGlosa() + "'" +
            "}";
    }
}
