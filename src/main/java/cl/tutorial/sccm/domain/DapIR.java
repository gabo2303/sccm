package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Entidad que representa Depósito a Plazo Cálculo de Intereses Reales.
 */
@Entity
@Table(name = "VW_DAP_CALC_INT_REALES_ALL")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DapIR implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    private BigInteger id;

    @Column(name = "FOLIO")
    private String folio;

    @Column(name = "COD_MON")
    private Long codMon;

    @Column(name = "FECHA_PAGO")
    private LocalDate fechaPago;

    @Column(name = "INT_REAL")
    private BigDecimal intReal;

    @Column(name = "INT_REAL_CAL")
    private BigDecimal intRealCal;

    @Column(name = "DIFERENCIA")
    private BigDecimal diferencia;

    @Column(name = "ANNO_TRIBUTA_ID")
    private BigInteger annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public DapIR folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Long getCodMon() {
        return codMon;
    }

    public DapIR codMon(Long codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Long codMon) {
        this.codMon = codMon;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public DapIR fechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public BigDecimal getIntReal() {
        return intReal;
    }

    public DapIR intReal(BigDecimal intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(BigDecimal intReal) {
        this.intReal = intReal;
    }

    public BigDecimal getIntRealCal() {
        return intRealCal;
    }

    public DapIR intRealCal(BigDecimal intRealCal) {
        this.intRealCal = intRealCal;
        return this;
    }

    public void setIntRealCal(BigDecimal intRealCal) {
        this.intRealCal = intRealCal;
    }

    public BigDecimal getDiferencia() {
        return diferencia;
    }

    public DapIR diferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
        return this;
    }

    public void setDiferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
    }

    public BigInteger getAnnoTributa() {
        return annoTributa;
    }

    public DapIR annoTributa(BigInteger annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(BigInteger annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DapIR dapIR = (DapIR) o;
        if (dapIR.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dapIR.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DapIR{" + "id=" + getId() + ", folio='" + getFolio() + "'" + ", codMon='" + getCodMon() + "'" + ", fechaPago='" + getFechaPago() + "'" + ", intReal='"
            + getIntReal() + "'" + ", intRealCal='" + getIntRealCal() + "'" + ", diferencia='" + getDiferencia() + "'" + ", annoTributa='" + getAnnoTributa() + "'" + "}";
    }
}
