package cl.tutorial.sccm.domain;

import cl.tutorial.sccm.domain.AnnoTributa;
import cl.tutorial.sccm.domain.Instrumento;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A Glosa_aud.
 */
@Entity
@Table(name = "glosa_aud")
public class GlosaAud extends AbstractAudEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nombre;

    private String valor;

    @Column(name = "ANNO_TRIBUTA_ID")
    private Long annoTributaId;

    @Column(name = "INSTRUMENTO_ID")
    private Long instrumentoId;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Long getAnnoTributaId() {
        return annoTributaId;
    }

    public void setAnnoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
    }

    public Long getInstrumentoId() {
        return instrumentoId;
    }

    public void setInstrumentoId(Long instrumentoId) {
        this.instrumentoId = instrumentoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof GlosaAud))
            return false;
        if (!super.equals(o))
            return false;
        GlosaAud glosaAud = (GlosaAud) o;
        return Objects.equals(nombre, glosaAud.nombre) && Objects.equals(valor, glosaAud.valor) && Objects.equals(annoTributaId, glosaAud.annoTributaId) && Objects
            .equals(instrumentoId, glosaAud.instrumentoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nombre, valor, annoTributaId, instrumentoId);
    }
}
