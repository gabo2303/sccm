package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A LHDCV.
 */
@Entity
@Table(name = "LHDCV")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class LHDCV implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rut")
    private BigInteger rut;

    @Column(name = "dv")
    private String dv;

    @Column(name = "producto")
    private Integer producto;

    @Column(name = "fecha_pago")
    private LocalDate fechaPago;

    @Column(name = "correlativo")
    private Integer correlativo;

    @Column(name = "fecha_inv")
    private LocalDate fechaInv;

    @Column(name = "folio")
    private String folio;

    @Column(name = "capital")
    private Double capital;

    @Column(name = "cap_mon")
    private Double capMon;

    @Column(name = "int_nom")
    private Double intNom;

    @Column(name = "int_real")
    private Integer intReal;

    @Column(name = "mon_fi")
    private Integer monFi;

    @Column(name = "num_giro")
    private Integer numGiro;

    @Column(name = "cod_mon")
    private Integer codMon;

    @Column(name = "mon_tasa")
    private Double monTasa;

    @Column(name = "tasa_emi")
    private Double tasaEmi;

    @Column(name = "nom_cli")
    private String nomCli;

    @Column(name = "cod_inst")
    private Integer codInst;

    @Column(name = "serie_inst")
    private String serieInst;

    @Column(name = "per_tasa")
    private Integer perTasa;

    @Column(name = "tipo_int")
    private String tipoInt;

    @Column(name = "cod_tipo_int")
    private Integer codTipoInt;

    @Column(name = "fec_vcto")
    private LocalDate fecVcto;

    @Column(name = "onp")
    private String onp;

    @Column(name = "est_cta")
    private String estCta;

    @Column(name = "fec_cont")
    private LocalDate fecCont;

    @Column(name = "bipersonal")
    private String bipersonal;

    @Column(name = "reajustabilidad")
    private String reajustabilidad;

    @Column(name = "plazo")
    private Integer plazo;

    @Column(name = "ind_pago")
    private String indPago;

    @Column(name = "extranjero")
    private String extranjero;

    @Column(name = "imp_ext")
    private Double impExt;

    @Column(name = "num_cupones")
    private Integer numCupones;

    @Column(name = "num_cupon_pago")
    private Integer numCuponPago;

    @Column(name = "cta_cap")
    private Integer ctaCap;

    @Column(name = "cta_int")
    private Integer ctaInt;

    @Column(name = "cta_reaj")
    private Integer ctaReaj;

    @Column(name = "origen")
    private String origen;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getRut() {
        return rut;
    }

    public LHDCV rut(BigInteger rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(BigInteger rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public LHDCV dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getProducto() {
        return producto;
    }

    public LHDCV producto(Integer producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public LHDCV fechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Integer getCorrelativo() {
        return correlativo;
    }

    public LHDCV correlativo(Integer correlativo) {
        this.correlativo = correlativo;
        return this;
    }

    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }

    public LocalDate getFechaInv() {
        return fechaInv;
    }

    public LHDCV fechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
        return this;
    }

    public void setFechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFolio() {
        return folio;
    }

    public LHDCV folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Double getCapital() {
        return capital;
    }

    public LHDCV capital(Double capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Double getCapMon() {
        return capMon;
    }

    public LHDCV capMon(Double capMon) {
        this.capMon = capMon;
        return this;
    }

    public void setCapMon(Double capMon) {
        this.capMon = capMon;
    }

    public Double getIntNom() {
        return intNom;
    }

    public LHDCV intNom(Double intNom) {
        this.intNom = intNom;
        return this;
    }

    public void setIntNom(Double intNom) {
        this.intNom = intNom;
    }

    public Integer getIntReal() {
        return intReal;
    }

    public LHDCV intReal(Integer intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(Integer intReal) {
        this.intReal = intReal;
    }

    public Integer getMonFi() {
        return monFi;
    }

    public LHDCV monFi(Integer monFi) {
        this.monFi = monFi;
        return this;
    }

    public void setMonFi(Integer monFi) {
        this.monFi = monFi;
    }

    public Integer getNumGiro() {
        return numGiro;
    }

    public LHDCV numGiro(Integer numGiro) {
        this.numGiro = numGiro;
        return this;
    }

    public void setNumGiro(Integer numGiro) {
        this.numGiro = numGiro;
    }

    public Integer getCodMon() {
        return codMon;
    }

    public LHDCV codMon(Integer codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Integer codMon) {
        this.codMon = codMon;
    }

    public Double getMonTasa() {
        return monTasa;
    }

    public LHDCV monTasa(Double monTasa) {
        this.monTasa = monTasa;
        return this;
    }

    public void setMonTasa(Double monTasa) {
        this.monTasa = monTasa;
    }

    public Double getTasaEmi() {
        return tasaEmi;
    }

    public LHDCV tasaEmi(Double tasaEmi) {
        this.tasaEmi = tasaEmi;
        return this;
    }

    public void setTasaEmi(Double tasaEmi) {
        this.tasaEmi = tasaEmi;
    }

    public String getNomCli() {
        return nomCli;
    }

    public LHDCV nomCli(String nomCli) {
        this.nomCli = nomCli;
        return this;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public Integer getCodInst() {
        return codInst;
    }

    public LHDCV codInst(Integer codInst) {
        this.codInst = codInst;
        return this;
    }

    public void setCodInst(Integer codInst) {
        this.codInst = codInst;
    }

    public String getSerieInst() {
        return serieInst;
    }

    public LHDCV serieInst(String serieInst) {
        this.serieInst = serieInst;
        return this;
    }

    public void setSerieInst(String serieInst) {
        this.serieInst = serieInst;
    }

    public Integer getPerTasa() {
        return perTasa;
    }

    public LHDCV perTasa(Integer perTasa) {
        this.perTasa = perTasa;
        return this;
    }

    public void setPerTasa(Integer perTasa) {
        this.perTasa = perTasa;
    }

    public String getTipoInt() {
        return tipoInt;
    }

    public LHDCV tipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
        return this;
    }

    public void setTipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
    }

    public Integer getCodTipoInt() {
        return codTipoInt;
    }

    public LHDCV codTipoInt(Integer codTipoInt) {
        this.codTipoInt = codTipoInt;
        return this;
    }

    public void setCodTipoInt(Integer codTipoInt) {
        this.codTipoInt = codTipoInt;
    }

    public LocalDate getFecVcto() {
        return fecVcto;
    }

    public LHDCV fecVcto(LocalDate fecVcto) {
        this.fecVcto = fecVcto;
        return this;
    }

    public void setFecVcto(LocalDate fecVcto) {
        this.fecVcto = fecVcto;
    }

    public String getOnp() {
        return onp;
    }

    public LHDCV onp(String onp) {
        this.onp = onp;
        return this;
    }

    public void setOnp(String onp) {
        this.onp = onp;
    }

    public String getEstCta() {
        return estCta;
    }

    public LHDCV estCta(String estCta) {
        this.estCta = estCta;
        return this;
    }

    public void setEstCta(String estCta) {
        this.estCta = estCta;
    }

    public LocalDate getFecCont() {
        return fecCont;
    }

    public LHDCV fecCont(LocalDate fecCont) {
        this.fecCont = fecCont;
        return this;
    }

    public void setFecCont(LocalDate fecCont) {
        this.fecCont = fecCont;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public LHDCV bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public LHDCV reajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
        return this;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public Integer getPlazo() {
        return plazo;
    }

    public LHDCV plazo(Integer plazo) {
        this.plazo = plazo;
        return this;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }

    public String getIndPago() {
        return indPago;
    }

    public LHDCV indPago(String indPago) {
        this.indPago = indPago;
        return this;
    }

    public void setIndPago(String indPago) {
        this.indPago = indPago;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public LHDCV extranjero(String extranjero) {
        this.extranjero = extranjero;
        return this;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }

    public Double getImpExt() {
        return impExt;
    }

    public LHDCV impExt(Double impExt) {
        this.impExt = impExt;
        return this;
    }

    public void setImpExt(Double impExt) {
        this.impExt = impExt;
    }

    public Integer getNumCupones() {
        return numCupones;
    }

    public LHDCV numCupones(Integer numCupones) {
        this.numCupones = numCupones;
        return this;
    }

    public void setNumCupones(Integer numCupones) {
        this.numCupones = numCupones;
    }

    public Integer getNumCuponPago() {
        return numCuponPago;
    }

    public LHDCV numCuponPago(Integer numCuponPago) {
        this.numCuponPago = numCuponPago;
        return this;
    }

    public void setNumCuponPago(Integer numCuponPago) {
        this.numCuponPago = numCuponPago;
    }

    public Integer getCtaCap() {
        return ctaCap;
    }

    public LHDCV ctaCap(Integer ctaCap) {
        this.ctaCap = ctaCap;
        return this;
    }

    public void setCtaCap(Integer ctaCap) {
        this.ctaCap = ctaCap;
    }

    public Integer getCtaInt() {
        return ctaInt;
    }

    public LHDCV ctaInt(Integer ctaInt) {
        this.ctaInt = ctaInt;
        return this;
    }

    public void setCtaInt(Integer ctaInt) {
        this.ctaInt = ctaInt;
    }

    public Integer getCtaReaj() {
        return ctaReaj;
    }

    public LHDCV ctaReaj(Integer ctaReaj) {
        this.ctaReaj = ctaReaj;
        return this;
    }

    public void setCtaReaj(Integer ctaReaj) {
        this.ctaReaj = ctaReaj;
    }

    public String getOrigen() {
        return origen;
    }

    public LHDCV origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public LHDCV annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bonos lHBonos = (Bonos) o;
        if (lHBonos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lHBonos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LHBonos{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", dv='" + getDv() + "'" +
            ", producto='" + getProducto() + "'" +
            ", fechaPago='" + getFechaPago() + "'" +
            ", correlativo='" + getCorrelativo() + "'" +
            ", fechaInv='" + getFechaInv() + "'" +
            ", folio='" + getFolio() + "'" +
            ", capital='" + getCapital() + "'" +
            ", capMon='" + getCapMon() + "'" +
            ", intNom='" + getIntNom() + "'" +
            ", intReal='" + getIntReal() + "'" +
            ", monFi='" + getMonFi() + "'" +
            ", numGiro='" + getNumGiro() + "'" +
            ", codMon='" + getCodMon() + "'" +
            ", monTasa='" + getMonTasa() + "'" +
            ", tasaEmi='" + getTasaEmi() + "'" +
            ", nomCli='" + getNomCli() + "'" +
            ", codInst='" + getCodInst() + "'" +
            ", serieInst='" + getSerieInst() + "'" +
            ", perTasa='" + getPerTasa() + "'" +
            ", tipoInt='" + getTipoInt() + "'" +
            ", codTipoInt='" + getCodTipoInt() + "'" +
            ", fecVcto='" + getFecVcto() + "'" +
            ", onp='" + getOnp() + "'" +
            ", estCta='" + getEstCta() + "'" +
            ", fecCont='" + getFecCont() + "'" +
            ", bipersonal='" + getBipersonal() + "'" +
            ", reajustabilidad='" + getReajustabilidad() + "'" +
            ", plazo='" + getPlazo() + "'" +
            ", indPago='" + getIndPago() + "'" +
            ", extranjero='" + getExtranjero() + "'" +
            ", impExt='" + getImpExt() + "'" +
            ", numCupones='" + getNumCupones() + "'" +
            ", numCuponPago='" + getNumCuponPago() + "'" +
            ", ctaCap='" + getCtaCap() + "'" +
            ", ctaInt='" + getCtaInt() + "'" +
            ", ctaReaj='" + getCtaReaj() + "'" +
            ", origen='" + getOrigen() + "'" +
            "}";
    }
}
