package cl.tutorial.sccm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Producto.
 */
@Entity
@Table(name = "producto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre_producto")
    private String nombreProducto;

    @Column(name = "usuario_producto")
    private String usuarioProducto;

    @Column(name = "admin_producto")
    private String adminProducto;

    @Column(name = "titulo_certificado")
    private String tituloCertificado;

    @OneToMany(mappedBy = "producto")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Instrumento> insts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public Producto nombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
        return this;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getUsuarioProducto() {
        return usuarioProducto;
    }

    public Producto usuarioProducto(String usuarioProducto) {
        this.usuarioProducto = usuarioProducto;
        return this;
    }

    public void setUsuarioProducto(String usuarioProducto) {
        this.usuarioProducto = usuarioProducto;
    }

    public String getAdminProducto() {
        return adminProducto;
    }

    public Producto adminProducto(String adminProducto) {
        this.adminProducto = adminProducto;
        return this;
    }

    public void setAdminProducto(String adminProducto) {
        this.adminProducto = adminProducto;
    }

    public String getTituloCertificado() {
        return tituloCertificado;
    }

    public Producto tituloCertificado(String tituloCertificado) {
        this.tituloCertificado = tituloCertificado;
        return this;
    }

    public void setTituloCertificado(String tituloCertificado) {
        this.tituloCertificado = tituloCertificado;
    }

    public Set<Instrumento> getInsts() {
        return insts;
    }

    public Producto insts(Set<Instrumento> instrumentos) {
        this.insts = instrumentos;
        return this;
    }

    public Producto addInst(Instrumento instrumento) {
        this.insts.add(instrumento);
        instrumento.setProducto(this);
        return this;
    }

    public Producto removeInst(Instrumento instrumento) {
        this.insts.remove(instrumento);
        instrumento.setProducto(null);
        return this;
    }

    public void setInsts(Set<Instrumento> instrumentos) {
        this.insts = instrumentos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Producto producto = (Producto) o;
        if (producto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), producto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Producto{" +
            "id=" + getId() +
            ", nombreProducto='" + getNombreProducto() + "'" +
            ", usuarioProducto='" + getUsuarioProducto() + "'" +
            ", adminProducto='" + getAdminProducto() + "'" +
            ", tituloCertificado='" + getTituloCertificado() + "'" +
            "}";
    }
}
