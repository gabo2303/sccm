package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DatosFijos.
 */
@Entity
@Table(name = "datos_fijos")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class DatosFijos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre_banco")
    private String nombreBanco;

    @Column(name = "rut_banco")
    private String rutBanco;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "giro")
    private String giro;

    @Column(name = "tipo_soc_anonima")
    private String tipoSocAnonima;

    @Column(name = "transaccion_bolsa")
    private Boolean transaccionBolsa;

    @Lob
    @Column(name = "imagen")
    private byte[] imagen;

    @Column(name = "imagen_content_type")
    private String imagenContentType;

    @Column(name = "ruta_exportacion")
    private String rutaExportacion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public DatosFijos nombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
        return this;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getRutBanco() {
        return rutBanco;
    }

    public DatosFijos rutBanco(String rutBanco) {
        this.rutBanco = rutBanco;
        return this;
    }

    public void setRutBanco(String rutBanco) {
        this.rutBanco = rutBanco;
    }

    public String getDireccion() {
        return direccion;
    }

    public DatosFijos direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getGiro() {
        return giro;
    }

    public DatosFijos giro(String giro) {
        this.giro = giro;
        return this;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public String getTipoSocAnonima() {
        return tipoSocAnonima;
    }

    public DatosFijos tipoSocAnonima(String tipoSocAnonima) {
        this.tipoSocAnonima = tipoSocAnonima;
        return this;
    }

    public void setTipoSocAnonima(String tipoSocAnonima) {
        this.tipoSocAnonima = tipoSocAnonima;
    }

    public Boolean isTransaccionBolsa() {
        return transaccionBolsa;
    }

    public DatosFijos transaccionBolsa(Boolean transaccionBolsa) {
        this.transaccionBolsa = transaccionBolsa;
        return this;
    }

    public void setTransaccionBolsa(Boolean transaccionBolsa) {
        this.transaccionBolsa = transaccionBolsa;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public DatosFijos imagen(byte[] imagen) {
        this.imagen = imagen;
        return this;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public DatosFijos imagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
        return this;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public String getRutaExportacion() {
        return rutaExportacion;
    }

    public DatosFijos rutaExportacion(String rutaExportacion) {
        this.rutaExportacion = rutaExportacion;
        return this;
    }

    public void setRutaExportacion(String rutaExportacion) {
        this.rutaExportacion = rutaExportacion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatosFijos datosFijos = (DatosFijos) o;
        if (datosFijos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datosFijos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatosFijos{" +
            "id=" + getId() +
            ", nombreBanco='" + getNombreBanco() + "'" +
            ", rutBanco='" + getRutBanco() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", giro='" + getGiro() + "'" +
            ", tipoSocAnonima='" + getTipoSocAnonima() + "'" +
            ", transaccionBolsa='" + isTransaccionBolsa() + "'" +
            ", imagen='" + getImagen() + "'" +
            ", imagenContentType='" + imagenContentType + "'" +
            ", rutaExportacion='" + getRutaExportacion() + "'" +
            "}";
    }
}
