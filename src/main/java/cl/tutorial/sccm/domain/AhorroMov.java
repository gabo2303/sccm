package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A AhorroMov.
 */
@Entity
@Table(name = "ahorro_mov")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class AhorroMov implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rut")
    private String rut;

    @Column(name = "cuenta")
    private Integer cuenta;

    @Column(name = "producto")
    private Integer producto;

    @Column(name = "fecha_mov")
    private LocalDate fechaMov;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "glosa")
    private String glosa;

    @Column(name = "capital")
    private Double capital;

    @Column(name = "valor_uf")
    private Double valorUf;

    @Column(name = "capital_uf")
    private Double capitalUf;

    @Column(name = "cod_moneda")
    private Integer codMoneda;

    @Column(name = "origen")
    private String origen;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public AhorroMov rut(String rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public Integer getCuenta() {
        return cuenta;
    }

    public AhorroMov cuenta(Integer cuenta) {
        this.cuenta = cuenta;
        return this;
    }

    public void setCuenta(Integer cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getProducto() {
        return producto;
    }

    public AhorroMov producto(Integer producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public LocalDate getFechaMov() {
        return fechaMov;
    }

    public AhorroMov fechaMov(LocalDate fechaMov) {
        this.fechaMov = fechaMov;
        return this;
    }

    public void setFechaMov(LocalDate fechaMov) {
        this.fechaMov = fechaMov;
    }

    public String getTipo() {
        return tipo;
    }

    public AhorroMov tipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public AhorroMov codigo(String codigo) {
        this.codigo = codigo;
        return this;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getGlosa() {
        return glosa;
    }

    public AhorroMov glosa(String glosa) {
        this.glosa = glosa;
        return this;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public Double getCapital() {
        return capital;
    }

    public AhorroMov capital(Double capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Double getValorUf() {
        return valorUf;
    }

    public AhorroMov valorUf(Double valorUf) {
        this.valorUf = valorUf;
        return this;
    }

    public void setValorUf(Double valorUf) {
        this.valorUf = valorUf;
    }

    public Double getCapitalUf() {
        return capitalUf;
    }

    public AhorroMov capitalUf(Double capitalUf) {
        this.capitalUf = capitalUf;
        return this;
    }

    public void setCapitalUf(Double capitalUf) {
        this.capitalUf = capitalUf;
    }

    public Integer getCodMoneda() {
        return codMoneda;
    }

    public AhorroMov codMoneda(Integer codMoneda) {
        this.codMoneda = codMoneda;
        return this;
    }

    public void setCodMoneda(Integer codMoneda) {
        this.codMoneda = codMoneda;
    }

    public String getOrigen() {
        return origen;
    }

    public AhorroMov origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AhorroMov ahorroMov = (AhorroMov) o;
        if (ahorroMov.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ahorroMov.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AhorroMov{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", cuenta='" + getCuenta() + "'" +
            ", producto='" + getProducto() + "'" +
            ", fechaMov='" + getFechaMov() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", codigo='" + getCodigo() + "'" +
            ", glosa='" + getGlosa() + "'" +
            ", capital='" + getCapital() + "'" +
            ", valorUf='" + getValorUf() + "'" +
            ", capitalUf='" + getCapitalUf() + "'" +
            ", codMoneda='" + getCodMoneda() + "'" +
            ", origen='" + getOrigen() + "'" +
            "}";
    }
}
