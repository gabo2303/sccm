package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Pactos.
 */
@Entity
@Table(name = "pactos")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Pactos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rut")
    private BigInteger rut;

    @Column(name = "dv")
    private String dv;

    @Column(name = "producto")
    private Integer producto;

    @Column(name = "moneda")
    private Integer moneda;

    @Column(name = "fecha_pago")
    private LocalDate fechaPago;

    @Column(name = "correlativo")
    private Integer correlativo;

    @Column(name = "fecha_inv")
    private LocalDate fechaInv;

    @Column(name = "folio")
    private String folio;

    @Column(name = "filler")
    private String filler;

    @Column(name = "capital")
    private BigDecimal capital;

    @Column(name = "cap_mon_ori")
    private BigDecimal capMonOri;

    @Column(name = "int_nom")
    private Float intNom;

    @Column(name = "int_real")
    private Float intReal;

    @Column(name = "mon_fi")
    private BigDecimal monFi;

    @Column(name = "num_giro")
    private Long numGiro;

    @Column(name = "cod_mon")
    private Long codMon;

    @Column(name = "mon_pac")
    private String monPac;

    @Column(name = "tas_mon")
    private String tasMon;

    @Column(name = "tasa")
    private Float tasa;

    @Column(name = "per_tasa")
    private Float perTasa;

    @Column(name = "reajuste")
    private Float reajuste;

    @Column(name = "tipo_int")
    private String tipoInt;

    @Column(name = "cod_tip_int")
    private String codTipInt;

    @Column(name = "fec_conta")
    private LocalDate fecConta;

    @Column(name = "bipersonal")
    private String bipersonal;

    @Column(name = "reajustabilidad")
    private String reajustabilidad;

    @Column(name = "plazo")
    private Integer plazo;

    @Column(name = "tipo_pago")
    private String tipoPago;

    @Column(name = "origen")
    private String origen;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getRut() {
        return rut;
    }

    public Pactos rut(BigInteger rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(BigInteger rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public Pactos dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Integer getProducto() {
        return producto;
    }

    public Pactos producto(Integer producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public Integer getMoneda() {
        return moneda;
    }

    public Pactos moneda(Integer moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public Pactos fechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
        return this;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Integer getCorrelativo() {
        return correlativo;
    }

    public Pactos correlativo(Integer correlativo) {
        this.correlativo = correlativo;
        return this;
    }

    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }

    public LocalDate getFechaInv() {
        return fechaInv;
    }

    public Pactos fechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
        return this;
    }

    public void setFechaInv(LocalDate fechaInv) {
        this.fechaInv = fechaInv;
    }

    public String getFolio() {
        return folio;
    }

    public Pactos folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFiller() {
        return filler;
    }

    public Pactos filler(String filler) {
        this.filler = filler;
        return this;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public BigDecimal getCapital() {
        return capital;
    }

    public Pactos capital(BigDecimal capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(BigDecimal capital) {
        this.capital = capital;
    }

    public BigDecimal getCapMonOri() {
        return capMonOri;
    }

    public Pactos capMonOri(BigDecimal capMonOri) {
        this.capMonOri = capMonOri;
        return this;
    }

    public void setCapMonOri(BigDecimal capMonOri) {
        this.capMonOri = capMonOri;
    }

    public Float getIntNom() {
        return intNom;
    }

    public Pactos intNom(Float intNom) {
        this.intNom = intNom;
        return this;
    }

    public void setIntNom(Float intNom) {
        this.intNom = intNom;
    }

    public Float getIntReal() {
        return intReal;
    }

    public Pactos intReal(Float intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(Float intReal) {
        this.intReal = intReal;
    }

    public BigDecimal getMonFi() {
        return monFi;
    }

    public Pactos monFi(BigDecimal monFi) {
        this.monFi = monFi;
        return this;
    }

    public void setMonFi(BigDecimal monFi) {
        this.monFi = monFi;
    }

    public Long getNumGiro() {
        return numGiro;
    }

    public Pactos numGiro(Long numGiro) {
        this.numGiro = numGiro;
        return this;
    }

    public void setNumGiro(Long numGiro) {
        this.numGiro = numGiro;
    }

    public Long getCodMon() {
        return codMon;
    }

    public Pactos codMon(Long codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(Long codMon) {
        this.codMon = codMon;
    }

    public String getMonPac() {
        return monPac;
    }

    public Pactos monPac(String monPac) {
        this.monPac = monPac;
        return this;
    }

    public void setMonPac(String monPac) {
        this.monPac = monPac;
    }

    public String getTasMon() {
        return tasMon;
    }

    public Pactos tasMon(String tasMon) {
        this.tasMon = tasMon;
        return this;
    }

    public void setTasMon(String tasMon) {
        this.tasMon = tasMon;
    }

    public Float getTasa() {
        return tasa;
    }

    public Pactos tasa(Float tasa) {
        this.tasa = tasa;
        return this;
    }

    public void setTasa(Float tasa) {
        this.tasa = tasa;
    }

    public Float getPerTasa() {
        return perTasa;
    }

    public Pactos perTasa(Float perTasa) {
        this.perTasa = perTasa;
        return this;
    }

    public void setPerTasa(Float perTasa) {
        this.perTasa = perTasa;
    }

    public Float getReajuste() {
        return reajuste;
    }

    public Pactos reajuste(Float reajuste) {
        this.reajuste = reajuste;
        return this;
    }

    public void setReajuste(Float reajuste) {
        this.reajuste = reajuste;
    }

    public String getTipoInt() {
        return tipoInt;
    }

    public Pactos tipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
        return this;
    }

    public void setTipoInt(String tipoInt) {
        this.tipoInt = tipoInt;
    }

    public String getCodTipInt() {
        return codTipInt;
    }

    public Pactos codTipInt(String codTipInt) {
        this.codTipInt = codTipInt;
        return this;
    }

    public void setCodTipInt(String codTipInt) {
        this.codTipInt = codTipInt;
    }

    public LocalDate getFecConta() {
        return fecConta;
    }

    public Pactos fecConta(LocalDate fecConta) {
        this.fecConta = fecConta;
        return this;
    }

    public void setFecConta(LocalDate fecConta) {
        this.fecConta = fecConta;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public Pactos bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public String getReajustabilidad() {
        return reajustabilidad;
    }

    public Pactos reajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
        return this;
    }

    public void setReajustabilidad(String reajustabilidad) {
        this.reajustabilidad = reajustabilidad;
    }

    public Integer getPlazo() {
        return plazo;
    }

    public Pactos plazo(Integer plazo) {
        this.plazo = plazo;
        return this;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public Pactos tipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
        return this;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getOrigen() {
        return origen;
    }

    public Pactos origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Pactos annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pactos pactos = (Pactos) o;
        if (pactos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pactos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pactos{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", dv='" + getDv() + "'" +
            ", producto='" + getProducto() + "'" +
            ", moneda='" + getMoneda() + "'" +
            ", fechaPago='" + getFechaPago() + "'" +
            ", correlativo='" + getCorrelativo() + "'" +
            ", fechaInv='" + getFechaInv() + "'" +
            ", folio='" + getFolio() + "'" +
            ", filler='" + getFiller() + "'" +
            ", capital='" + getCapital() + "'" +
            ", capMonOri='" + getCapMonOri() + "'" +
            ", intNom='" + getIntNom() + "'" +
            ", intReal='" + getIntReal() + "'" +
            ", monFi='" + getMonFi() + "'" +
            ", numGiro='" + getNumGiro() + "'" +
            ", codMon='" + getCodMon() + "'" +
            ", monPac='" + getMonPac() + "'" +
            ", tasMon='" + getTasMon() + "'" +
            ", tasa='" + getTasa() + "'" +
            ", perTasa='" + getPerTasa() + "'" +
            ", reajuste='" + getReajuste() + "'" +
            ", tipoInt='" + getTipoInt() + "'" +
            ", codTipInt='" + getCodTipInt() + "'" +
            ", fecConta='" + getFecConta() + "'" +
            ", bipersonal='" + getBipersonal() + "'" +
            ", reajustabilidad='" + getReajustabilidad() + "'" +
            ", plazo='" + getPlazo() + "'" +
            ", tipoPago='" + getTipoPago() + "'" +
            ", origen='" + getOrigen() + "'" +
            "}";
    }
}
