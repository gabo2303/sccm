package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TotalProd.
 */
@Entity
@Table(name = "total_prod")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class TotalProd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "origen")
    private String origen;

    @Column(name = "producto")
    private Integer producto;

    @Column(name = "cod_mon")
    private String codMon;

    @Column(name = "int_nom")
    private Double intNom;

    @Column(name = "int_real")
    private Double intReal;

    @Column(name = "mont_fin")
    private Double montFin;

    @Column(name = "bipersonal")
    private String bipersonal;

    @Column(name = "imp_ext")
    private Integer impExt;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrigen() {
        return origen;
    }

    public TotalProd origen(String origen) {
        this.origen = origen;
        return this;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Integer getProducto() {
        return producto;
    }

    public TotalProd producto(Integer producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public String getCodMon() {
        return codMon;
    }

    public TotalProd codMon(String codMon) {
        this.codMon = codMon;
        return this;
    }

    public void setCodMon(String codMon) {
        this.codMon = codMon;
    }

    public Double getIntNom() {
        return intNom;
    }

    public TotalProd intNom(Double intNom) {
        this.intNom = intNom;
        return this;
    }

    public void setIntNom(Double intNom) {
        this.intNom = intNom;
    }

    public Double getIntReal() {
        return intReal;
    }

    public TotalProd intReal(Double intReal) {
        this.intReal = intReal;
        return this;
    }

    public void setIntReal(Double intReal) {
        this.intReal = intReal;
    }

    public Double getMontFin() {
        return montFin;
    }

    public TotalProd montFin(Double montFin) {
        this.montFin = montFin;
        return this;
    }

    public void setMontFin(Double montFin) {
        this.montFin = montFin;
    }

    public String getBipersonal() {
        return bipersonal;
    }

    public TotalProd bipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
        return this;
    }

    public void setBipersonal(String bipersonal) {
        this.bipersonal = bipersonal;
    }

    public Integer getImpExt() {
        return impExt;
    }

    public TotalProd impExt(Integer impExt) {
        this.impExt = impExt;
        return this;
    }

    public void setImpExt(Integer impExt) {
        this.impExt = impExt;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public TotalProd annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TotalProd totalProd = (TotalProd) o;
        if (totalProd.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), totalProd.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TotalProd{" +
            "id=" + getId() +
            ", origen='" + getOrigen() + "'" +
            ", producto='" + getProducto() + "'" +
            ", codMon='" + getCodMon() + "'" +
            ", intNom='" + getIntNom() + "'" +
            ", intReal='" + getIntReal() + "'" +
            ", montFin='" + getMontFin() + "'" +
            ", bipersonal='" + getBipersonal() + "'" +
            ", impExt='" + getImpExt() + "'" +
            "}";
    }
}
