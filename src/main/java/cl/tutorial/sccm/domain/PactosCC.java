package cl.tutorial.sccm.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A class representing view from pactos cuadratura contable calc.
 */

@Entity
@Table(name = "vw_pactos_cua_cont")
public class PactosCC implements Serializable {
    @Column(name = "origen")
    private String origen;
    @Column(name = "producto")
    private Integer producto;
    @Id
    @Basic(optional = false)
    @Column(name = "simbolo")
    private String simbolo;
    @Column(name = "int_nom")
    private Double intNom;
    @Column(name = "int_real")
    private Double intReal;
    @Column(name = "mon_fi")
    private Double monFi;
    @Column(name = "anno_tributa_id")
    private Long annoTributa;

    public PactosCC() {
    }

    public PactosCC(String origen, Integer producto, String simbolo, Double intNom, Double intReal, Double monFi, Long annoTributa) {
        this.origen = origen;
        this.producto = producto;
        this.simbolo = simbolo;
        this.intNom = intNom;
        this.intReal = intReal;
        this.monFi = monFi;
        this.annoTributa = annoTributa;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Integer getProducto() {
        return producto;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public Double getIntNom() {
        return intNom;
    }

    public void setIntNom(Double intNom) {
        this.intNom = intNom;
    }

    public Double getIntReal() {
        return intReal;
    }

    public void setIntReal(Double intReal) {
        this.intReal = intReal;
    }

    public Double getMonFi() {
        return monFi;
    }

    public void setMonFi(Double monFi) {
        this.monFi = monFi;
    }

    public Long getAnnoTributa() {
        return annoTributa;
    }

    public void setAnnoTributa(Long annoTributa) {
        this.annoTributa = annoTributa;
    }
}
