package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Temporal1890.
 */
@Entity
@Table(name = "temporal_1890")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Temporal1890 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "rut")
    private Integer rut;

    @Column(name = "dv")
    private String dv;

    @Column(name = "totalcoluno")
    private Long totalcoluno;

    @Column(name = "totalcolunoneg")
    private Long totalcolunoneg;

    @Column(name = "totalcoldos")
    private Long totalcoldos;

    @Column(name = "totalcoldosneg")
    private Long totalcoldosneg;

    @Column(name = "totalcoltres")
    private Long totalcoltres;

    @Column(name = "totalcoltresneg")
    private Long totalcoltresneg;

    @Column(name = "totalcolcuatro")
    private Long totalcolcuatro;

    @Column(name = "totalcolcuatroneg")
    private Long totalcolcuatroneg;

    @Column(name = "totalcolcinco")
    private Long totalcolcinco;

    @Column(name = "totalcolcinconeg")
    private Long totalcolcinconeg;

    @Column(name = "totalcolseis")
    private Long totalcolseis;

    @Column(name = "totalcolseisneg")
    private Long totalcolseisneg;

    @Column(name = "tip_16")
    private Integer tip_16;

    @Column(name = "tip_17")
    private Integer tip_17;

    @Column(name = "nro_cer")
    private Integer nro_cer;

    //@ManyToOne
    //private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    public Integer getRut() {
        return rut;
    }

    public Temporal1890 rut(Integer rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public Temporal1890 dv(String dv) {
        this.dv = dv;
        return this;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Long getTotalcoluno() {
        return totalcoluno;
    }

    public Temporal1890 totalcoluno(Long totalcoluno) {
        this.totalcoluno = totalcoluno;
        return this;
    }

    public void setTotalcoluno(Long totalcoluno) {
        this.totalcoluno = totalcoluno;
    }

    public Long getTotalcolunoneg() {
        return totalcolunoneg;
    }

    public Temporal1890 totalcolunoneg(Long totalcolunoneg) {
        this.totalcolunoneg = totalcolunoneg;
        return this;
    }

    public void setTotalcolunoneg(Long totalcolunoneg) {
        this.totalcolunoneg = totalcolunoneg;
    }

    public Long getTotalcoldos() {
        return totalcoldos;
    }

    public Temporal1890 totalcoldos(Long totalcoldos) {
        this.totalcoldos = totalcoldos;
        return this;
    }

    public void setTotalcoldos(Long totalcoldos) {
        this.totalcoldos = totalcoldos;
    }

    public Long getTotalcoldosneg() {
        return totalcoldosneg;
    }

    public Temporal1890 totalcoldosneg(Long totalcoldosneg) {
        this.totalcoldosneg = totalcoldosneg;
        return this;
    }

    public void setTotalcoldosneg(Long totalcoldosneg) {
        this.totalcoldosneg = totalcoldosneg;
    }

    public Long getTotalcoltres() {
        return totalcoltres;
    }

    public Temporal1890 totalcoltres(Long totalcoltres) {
        this.totalcoltres = totalcoltres;
        return this;
    }

    public void setTotalcoltres(Long totalcoltres) {
        this.totalcoltres = totalcoltres;
    }

    public Long getTotalcoltresneg() {
        return totalcoltresneg;
    }

    public Temporal1890 totalcoltresneg(Long totalcoltresneg) {
        this.totalcoltresneg = totalcoltresneg;
        return this;
    }

    public void setTotalcoltresneg(Long totalcoltresneg) {
        this.totalcoltresneg = totalcoltresneg;
    }

    public Long getTotalcolcuatro() {
        return totalcolcuatro;
    }

    public Temporal1890 totalcolcuatro(Long totalcolcuatro) {
        this.totalcolcuatro = totalcolcuatro;
        return this;
    }

    public void setTotalcolcuatro(Long totalcolcuatro) {
        this.totalcolcuatro = totalcolcuatro;
    }

    public Long getTotalcolcuatroneg() {
        return totalcolcuatroneg;
    }

    public Temporal1890 totalcolcuatroneg(Long totalcolcuatroneg) {
        this.totalcolcuatroneg = totalcolcuatroneg;
        return this;
    }

    public void setTotalcolcuatroneg(Long totalcolcuatroneg) {
        this.totalcolcuatroneg = totalcolcuatroneg;
    }

    public Long getTotalcolcinco() {
        return totalcolcinco;
    }

    public Temporal1890 totalcolcinco(Long totalcolcinco) {
        this.totalcolcinco = totalcolcinco;
        return this;
    }

    public void setTotalcolcinco(Long totalcolcinco) {
        this.totalcolcinco = totalcolcinco;
    }

    public Long getTotalcolcinconeg() {
        return totalcolcinconeg;
    }

    public Temporal1890 totalcolcinconeg(Long totalcolcinconeg) {
        this.totalcolcinconeg = totalcolcinconeg;
        return this;
    }

    public void setTotalcolcinconeg(Long totalcolcinconeg) {
        this.totalcolcinconeg = totalcolcinconeg;
    }

    public Long getTotalcolseis() {
        return totalcolseis;
    }

    public Temporal1890 totalcolseis(Long totalcolseis) {
        this.totalcolseis = totalcolseis;
        return this;
    }

    public void setTotalcolseis(Long totalcolseis) {
        this.totalcolseis = totalcolseis;
    }

    public Long getTotalcolseisneg() {
        return totalcolseisneg;
    }

    public Temporal1890 totalcolseisneg(Long totalcolseisneg) {
        this.totalcolseisneg = totalcolseisneg;
        return this;
    }

    public void setTotalcolseisneg(Long totalcolseisneg) {
        this.totalcolseisneg = totalcolseisneg;
    }

    public Integer getTip_16() {
        return tip_16;
    }

    public Temporal1890 tip_16(Integer tip_16) {
        this.tip_16 = tip_16;
        return this;
    }

    public void setTip_16(Integer tip_16) {
        this.tip_16 = tip_16;
    }

    public Integer getTip_17() {
        return tip_17;
    }

    public Temporal1890 tip_17(Integer tip_17) {
        this.tip_17 = tip_17;
        return this;
    }

    public void setTip_17(Integer tip_17) {
        this.tip_17 = tip_17;
    }

    public Integer getNro_cer() {
        return nro_cer;
    }

    public Temporal1890 nro_cer(Integer nro_cer) {
        this.nro_cer = nro_cer;
        return this;
    }

    public void setNro_cer(Integer nro_cer) {
        this.nro_cer = nro_cer;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    @Override
    public String toString() {
        return "Temporal1890{" +
            ", rut='" + getRut() + "'" +
            ", dv='" + getDv() + "'" +
            ", totalcoluno='" + getTotalcoluno() + "'" +
            ", totalcolunoneg='" + getTotalcolunoneg() + "'" +
            ", totalcoldos='" + getTotalcoldos() + "'" +
            ", totalcoldosneg='" + getTotalcoldosneg() + "'" +
            ", totalcoltres='" + getTotalcoltres() + "'" +
            ", totalcoltresneg='" + getTotalcoltresneg() + "'" +
            ", totalcolcuatro='" + getTotalcolcuatro() + "'" +
            ", totalcolcuatroneg='" + getTotalcolcuatroneg() + "'" +
            ", totalcolcinco='" + getTotalcolcinco() + "'" +
            ", totalcolcinconeg='" + getTotalcolcinconeg() + "'" +
            ", totalcolseis='" + getTotalcolseis() + "'" +
            ", totalcolseisneg='" + getTotalcolseisneg() + "'" +
            ", tip_16='" + getTip_16() + "'" +
            ", tip_17='" + getTip_17() + "'" +
            ", nro_cer='" + getNro_cer() + "'" +
            "}";
    }
}
