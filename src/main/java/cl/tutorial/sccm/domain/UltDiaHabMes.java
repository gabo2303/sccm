package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A UltDiaHabMes.
 */
@Entity
@Table(name = "ult_dia_mes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class UltDiaHabMes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "mes")
    private Integer mes;

    @Column(name = "dia")
    private Integer dia;

    @ManyToOne
    private AnnoTributa annoTributa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Integer getMes() { return mes; }

    public UltDiaHabMes mes(Integer mes) 
	{ 
		this.mes = mes;
        
		return this;
    }

    public void setMes(Integer mes) { this.mes = mes; }

    public Integer getDia() { return dia; }

    public UltDiaHabMes dia(Integer dia) 
	{
        this.dia = dia;
        
		return this;
    }

    public void setDia(Integer dia) { this.dia = dia; }

    public AnnoTributa getAnnoTributa() { return annoTributa; }

    public UltDiaHabMes annoTributa(AnnoTributa annoTributa) 
	{
        this.annoTributa = annoTributa;
        
		return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) { this.annoTributa = annoTributa; }
    
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove
    @Override
    public boolean equals(Object o) 
	{
        if (this == o) { return true; }
		
        if (o == null || getClass() != o.getClass()) { return false; }
		
        UltDiaHabMes ultDiaHabMes = (UltDiaHabMes) o;
        
		if (ultDiaHabMes.getId() == null || getId() == null) { return false; }
		
        return Objects.equals(getId(), ultDiaHabMes.getId());
    }

    @Override
    public int hashCode() { return Objects.hashCode(getId()); }

    @Override
    public String toString() 
	{
        return "UltDiaHabMes{" +
            "id=" + getId() +
            ", mes='" + getMes() + "'" +
            ", dia='" + getDia() + "'" +
            "}";
    }
}