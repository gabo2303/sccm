package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Representante.
 */
@Entity
@Table(name = "representante")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Representante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "cargo")
    private String cargo;

    @Column(name = "rut")
    private String rut;

    @Lob
    @Column(name = "firma")
    private byte[] firma;

    @Column(name = "firma_content_type")
    private String firmaContentType;

    @ManyToOne
    private AnnoTributa annoTributa;

    @ManyToOne
    private Instrumento instrumento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Representante nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public Representante cargo(String cargo) {
        this.cargo = cargo;
        return this;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getRut() {
        return rut;
    }

    public Representante rut(String rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public byte[] getFirma() {
        return firma;
    }

    public Representante firma(byte[] firma) {
        this.firma = firma;
        return this;
    }

    public void setFirma(byte[] firma) {
        this.firma = firma;
    }

    public String getFirmaContentType() {
        return firmaContentType;
    }

    public Representante firmaContentType(String firmaContentType) {
        this.firmaContentType = firmaContentType;
        return this;
    }

    public void setFirmaContentType(String firmaContentType) {
        this.firmaContentType = firmaContentType;
    }

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Representante annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public Instrumento getInstrumento() {
        return instrumento;
    }

    public Representante instrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
        return this;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Representante representante = (Representante) o;
        if (representante.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), representante.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Representante{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", cargo='" + getCargo() + "'" +
            ", rut='" + getRut() + "'" +
            ", firma='" + getFirma() + "'" +
            ", firmaContentType='" + firmaContentType + "'" +
            "}";
    }
}
