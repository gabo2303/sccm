package cl.tutorial.sccm.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public abstract class AbstractAudEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected CompositeAudKey compositeAudKey;

    @Column(name = "REVTYPE", nullable = false, updatable = false)
    protected Integer revisionType;

    protected String auditor;

    @Column(name = "TIMESTAMP")
    protected Long fecha;

    public CompositeAudKey getCompositeAudKey() {
        return compositeAudKey;
    }

    public void setCompositeAudKey(CompositeAudKey compositeAudKey) {
        this.compositeAudKey = compositeAudKey;
    }

    public Integer getRevisionType() {
        return revisionType;
    }

    public void setRevisionType(Integer revisionType) {
        this.revisionType = revisionType;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof AbstractAudEntity))
            return false;
        AbstractAudEntity that = (AbstractAudEntity) o;
        return Objects.equals(compositeAudKey, that.compositeAudKey) && Objects.equals(revisionType, that.revisionType) && Objects.equals(auditor, that.auditor) && Objects
            .equals(fecha, that.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(compositeAudKey, revisionType, auditor, fecha);
    }
}
