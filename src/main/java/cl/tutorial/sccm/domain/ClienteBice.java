package cl.tutorial.sccm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ClienteBice.
 */
@Entity
@Table(name = "cliente_bice")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class ClienteBice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rut")
    private String rut;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "direccion")
    private String direccion;

    @OneToMany(mappedBy = "clienteBice")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Cart57B> emiCart1S = new HashSet<>();

    @OneToMany(mappedBy = "clienteBice")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Cert57B> emiCert1S = new HashSet<>();

    @OneToMany(mappedBy = "clienteBice")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Entrada57b> ent57b2S = new HashSet<>();

    @OneToMany(mappedBy = "clienteBice")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<SalIni57B> sIni2S = new HashSet<>();

    @OneToMany(mappedBy = "clienteBice")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<SalProxPer> saldos2S = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public ClienteBice rut(String rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public ClienteBice nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public ClienteBice direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Set<Cart57B> getEmiCart1S() {
        return emiCart1S;
    }

    public ClienteBice emiCart1S(Set<Cart57B> cart57BS) {
        this.emiCart1S = cart57BS;
        return this;
    }

    public ClienteBice addEmiCart1(Cart57B cart57B) {
        this.emiCart1S.add(cart57B);
        cart57B.setClienteBice(this);
        return this;
    }

    public ClienteBice removeEmiCart1(Cart57B cart57B) {
        this.emiCart1S.remove(cart57B);
        cart57B.setClienteBice(null);
        return this;
    }

    public void setEmiCart1S(Set<Cart57B> cart57BS) {
        this.emiCart1S = cart57BS;
    }

    public Set<Cert57B> getEmiCert1S() {
        return emiCert1S;
    }

    public ClienteBice emiCert1S(Set<Cert57B> cert57BS) {
        this.emiCert1S = cert57BS;
        return this;
    }

    public ClienteBice addEmiCert1(Cert57B cert57B) {
        this.emiCert1S.add(cert57B);
        cert57B.setClienteBice(this);
        return this;
    }

    public ClienteBice removeEmiCert1(Cert57B cert57B) {
        this.emiCert1S.remove(cert57B);
        cert57B.setClienteBice(null);
        return this;
    }

    public void setEmiCert1S(Set<Cert57B> cert57BS) {
        this.emiCert1S = cert57BS;
    }

    public Set<Entrada57b> getEnt57b2S() {
        return ent57b2S;
    }

    public ClienteBice ent57b2S(Set<Entrada57b> entrada57bs) {
        this.ent57b2S = entrada57bs;
        return this;
    }

    public ClienteBice addEnt57b2(Entrada57b entrada57b) {
        this.ent57b2S.add(entrada57b);
        entrada57b.setClienteBice(this);
        return this;
    }

    public ClienteBice removeEnt57b2(Entrada57b entrada57b) {
        this.ent57b2S.remove(entrada57b);
        entrada57b.setClienteBice(null);
        return this;
    }

    public void setEnt57b2S(Set<Entrada57b> entrada57bs) {
        this.ent57b2S = entrada57bs;
    }

    public Set<SalIni57B> getSIni2S() {
        return sIni2S;
    }

    public ClienteBice sIni2S(Set<SalIni57B> salIni57BS) {
        this.sIni2S = salIni57BS;
        return this;
    }

    public ClienteBice addSIni2(SalIni57B salIni57B) {
        this.sIni2S.add(salIni57B);
        salIni57B.setClienteBice(this);
        return this;
    }

    public ClienteBice removeSIni2(SalIni57B salIni57B) {
        this.sIni2S.remove(salIni57B);
        salIni57B.setClienteBice(null);
        return this;
    }

    public void setSIni2S(Set<SalIni57B> salIni57BS) {
        this.sIni2S = salIni57BS;
    }

    public Set<SalProxPer> getSaldos2S() {
        return saldos2S;
    }

    public ClienteBice saldos2S(Set<SalProxPer> salProxPers) {
        this.saldos2S = salProxPers;
        return this;
    }

    public ClienteBice addSaldos2(SalProxPer salProxPer) {
        this.saldos2S.add(salProxPer);
        salProxPer.setClienteBice(this);
        return this;
    }

    public ClienteBice removeSaldos2(SalProxPer salProxPer) {
        this.saldos2S.remove(salProxPer);
        salProxPer.setClienteBice(null);
        return this;
    }

    public void setSaldos2S(Set<SalProxPer> salProxPers) {
        this.saldos2S = salProxPers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClienteBice clienteBice = (ClienteBice) o;
        if (clienteBice.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clienteBice.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClienteBice{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", direccion='" + getDireccion() + "'" +
            "}";
    }
}
