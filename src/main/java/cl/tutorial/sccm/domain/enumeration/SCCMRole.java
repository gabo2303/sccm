package cl.tutorial.sccm.domain.enumeration;

/**
 * The SCCMRole enumeration.
 */
public enum SCCMRole {
    ROLE_SCCM_1941_ADMIN, ROLE_SCCM_1941_USER, ROLE_SCCM_1944_ADMIN, ROLE_SCCM_1944_USER, ROLE_SCCM_1890_ADMIN, ROLE_SCCM_1890_USER, ROLE_SCCM_ADMIN
}
