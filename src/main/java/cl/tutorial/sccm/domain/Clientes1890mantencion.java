package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Clientes1890mantencion.
 */
@Entity
@Table(name = "clientes_1890_mantencion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Clientes1890mantencion implements Serializable {

    private static final long serialVersionUID = 1L;
	
	@Id    
    @Column(name = "rut")
    private Integer rut;

    @Column(name = "razon")
    private String razon;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    public Integer getRut() {
        return rut;
    }

    public Clientes1890mantencion rut(Integer rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(Integer rut) {
        this.rut = rut;
    }

    public String getRazon() {
        return razon;
    }

    public Clientes1890mantencion razon(String razon) {
        this.razon = razon;
        return this;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public String toString() {
        return "Clientes1890mantencion{" +
            ", rut='" + getRut() + "'" +
            ", nombre='" + getRazon() + "'" +
            "}";
    }
}