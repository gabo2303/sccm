package cl.tutorial.sccm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Cliente1890.
 */
@Entity
@Table(name = "cliente_1890")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Cliente1890 implements Serializable 
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "nacionalidad")
    private String nacionalidad;

    @Column(name = "impto_35")
    private String impto35;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "comuna")
    private String comuna;

    @Column(name = "ciudad")
    private String ciudad;

    @OneToMany(mappedBy = "cliente1890")
    //@JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotAudited
    private Set<Rut1890> rut1890S = new HashSet<>();

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }

    public Cliente1890 nombre(String nombre) 
	{
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getNacionalidad() { return nacionalidad; }

    public Cliente1890 nacionalidad(String nacionalidad) 
	{
        this.nacionalidad = nacionalidad;
        return this;
    }

    public void setNacionalidad(String nacionalidad) { this.nacionalidad = nacionalidad; }

    public String getImpto35() { return impto35; }

    public Cliente1890 impto35(String impto35) 
	{
        this.impto35 = impto35;
        return this;
    }

    public void setImpto35(String impto35) { this.impto35 = impto35; }

    public String getDireccion() { return direccion; }

    public Cliente1890 direccion(String direccion) 
	{
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) { this.direccion = direccion; }

    public String getComuna() { return comuna; }

    public Cliente1890 comuna(String comuna) 
	{
        this.comuna = comuna;
        return this;
    }

    public void setComuna(String comuna) { this.comuna = comuna; }

    public String getCiudad() { return ciudad; }

    public Cliente1890 ciudad(String ciudad) 
	{
        this.ciudad = ciudad;
        return this;
    }

    public void setCiudad(String ciudad) { this.ciudad = ciudad; }

    public Set<Rut1890> getRut1890S() { return rut1890S; }

    public Cliente1890 rut1890S(Set<Rut1890> rut1890S) 
	{
        this.rut1890S = rut1890S;
        return this;
    }

    public Cliente1890 addRut1890(Rut1890 rut1890) 
	{
        this.rut1890S.add(rut1890);
        rut1890.setCliente1890(this);
        return this;
    }

    public Cliente1890 removeRut1890(Rut1890 rut1890) 
	{
        this.rut1890S.remove(rut1890);
        rut1890.setCliente1890(null);
        return this;
    }

    public void setRut1890S(Set<Rut1890> rut1890S) { this.rut1890S = rut1890S; }

    @Override
    public boolean equals(Object o) 
	{
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cliente1890 cliente1890 = (Cliente1890) o;
        if (cliente1890.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cliente1890.getId());
    }

    @Override
    public int hashCode() { return Objects.hashCode(getId()); }

    @Override
    public String toString() 
	{
        return "Cliente1890{" + "id=" + getId() + ", nombre='" + getNombre() + "'" + ", nacionalidad='" + getNacionalidad() + "'" 
			   + ", impto35='" + getImpto35() + "'" + ", direccion='" + getDireccion() + "'" + ", comuna='" + getComuna() + "'" + ", ciudad='" 
			   + getCiudad() + "'" + "}";
    }
}