package cl.tutorial.sccm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Certi57BFcc.
 */
@Entity
@Table(name = "certi_57_b_fcc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Certi57BFcc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Max(value = 3)
    @Column(name = "suc")
    private Integer suc;

    @Max(value = 3)
    @Column(name = "tip")
    private Integer tip;

    @Max(value = 3)
    @Column(name = "sucur")
    private Integer sucur;

    @Max(value = 3)
    @Column(name = "tipde")
    private Integer tipde;

    @Max(value = 7)
    @Column(name = "folio")
    private String folio;

    @Max(value = 1)
    @Column(name = "tipre")
    private Integer tipre;

    @Size(max = 40)
    @Column(name = "nombre", length = 40)
    private String nombre;

    @Size(max = 60)
    @Column(name = "direccion", length = 60)
    private String direccion;

    @Size(max = 20)
    @Column(name = "comuna", length = 20)
    private String comuna;

    @Size(max = 20)
    @Column(name = "ciudad", length = 20)
    private String ciudad;

    @Max(value = 8)
    @Column(name = "telefono")
    private Integer telefono;

    @Column(name = "tasad")
    private Float tasad;

    @Column(name = "fedep")
    private LocalDate fedep;

    @Column(name = "feven")
    private LocalDate feven;

    @Max(value = 4)
    @Column(name = "nudia")
    private Integer nudia;

    @Max(value = 4)
    @Column(name = "orsec")
    private Integer orsec;

    @Max(value = 1)
    @Column(name = "ticli")
    private Integer ticli;

    @Max(value = 4)
    @Column(name = "diaven")
    private Integer diaven;

    @Max(value = 10)
    @Column(name = "nurut")
    private Integer nurut;

    @Size(max = 1)
    @Column(name = "dvrut", length = 1)
    private String dvrut;

    @Column(name = "capital")
    private Float capital;

    @Column(name = "interes")
    private Float interes;

    @Column(name = "capmoneda")
    private Float capmoneda;

    @Column(name = "reajuste")
    private Float reajuste;

    @Column(name = "intmoneda")
    private Float intmoneda;

    @Max(value = 1)
    @Column(name = "codco")
    private Integer codco;

    @Column(name = "newfo")
    private Float newfo;

    @Max(value = 3)
    @Column(name = "comon")
    private Integer comon;

    @Size(max = 1)
    @Column(name = "segur", length = 1)
    private String segur;

    @Column(name = "monfi")
    private Float monfi;

    @Size(max = 6)
    @Column(name = "capta", length = 6)
    private String capta;

    @Size(max = 1)
    @Column(name = "autor", length = 1)
    private String autor;

    @Max(value = 2)
    @Column(name = "sucur_1")
    private Integer sucur1;

    @Max(value = 3)
    @Column(name = "tipde_1")
    private Integer tipde1;

    @Max(value = 7)
    @Column(name = "folio_1")
    private Integer folio1;

    @Column(name = "tasad_1")
    private Float tasad1;

    @Column(name = "fedep_1")
    private LocalDate fedep1;

    @Column(name = "feven_1")
    private LocalDate feven1;

    @Max(value = 4)
    @Column(name = "nudia_1")
    private Integer nudia1;

    @Column(name = "capit_1")
    private Float capit1;

    @Column(name = "capmo_1")
    private Float capmo1;

    @Size(max = 44)
    @Column(name = "filler", length = 44)
    private String filler;

    @Column(name = "fedia")
    private LocalDate fedia;

    @Max(value = 10)
    @Column(name = "ccapit")
    private Integer ccapit;

    @Max(value = 10)
    @Column(name = "ccinte")
    private Integer ccinte;

    @Max(value = 10)
    @Column(name = "ccreaj")
    private Integer ccreaj;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSuc() {
        return suc;
    }

    public Certi57BFcc suc(Integer suc) {
        this.suc = suc;
        return this;
    }

    public void setSuc(Integer suc) {
        this.suc = suc;
    }

    public Integer getTip() {
        return tip;
    }

    public Certi57BFcc tip(Integer tip) {
        this.tip = tip;
        return this;
    }

    public void setTip(Integer tip) {
        this.tip = tip;
    }

    public Integer getSucur() {
        return sucur;
    }

    public Certi57BFcc sucur(Integer sucur) {
        this.sucur = sucur;
        return this;
    }

    public void setSucur(Integer sucur) {
        this.sucur = sucur;
    }

    public Integer getTipde() {
        return tipde;
    }

    public Certi57BFcc tipde(Integer tipde) {
        this.tipde = tipde;
        return this;
    }

    public void setTipde(Integer tipde) {
        this.tipde = tipde;
    }

    public String getFolio() {
        return folio;
    }

    public Certi57BFcc folio(String folio) {
        this.folio = folio;
        return this;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Integer getTipre() {
        return tipre;
    }

    public Certi57BFcc tipre(Integer tipre) {
        this.tipre = tipre;
        return this;
    }

    public void setTipre(Integer tipre) {
        this.tipre = tipre;
    }

    public String getNombre() {
        return nombre;
    }

    public Certi57BFcc nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public Certi57BFcc direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public Certi57BFcc comuna(String comuna) {
        this.comuna = comuna;
        return this;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCiudad() {
        return ciudad;
    }

    public Certi57BFcc ciudad(String ciudad) {
        this.ciudad = ciudad;
        return this;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public Certi57BFcc telefono(Integer telefono) {
        this.telefono = telefono;
        return this;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public Float getTasad() {
        return tasad;
    }

    public Certi57BFcc tasad(Float tasad) {
        this.tasad = tasad;
        return this;
    }

    public void setTasad(Float tasad) {
        this.tasad = tasad;
    }

    public LocalDate getFedep() {
        return fedep;
    }

    public Certi57BFcc fedep(LocalDate fedep) {
        this.fedep = fedep;
        return this;
    }

    public void setFedep(LocalDate fedep) {
        this.fedep = fedep;
    }

    public LocalDate getFeven() {
        return feven;
    }

    public Certi57BFcc feven(LocalDate feven) {
        this.feven = feven;
        return this;
    }

    public void setFeven(LocalDate feven) {
        this.feven = feven;
    }

    public Integer getNudia() {
        return nudia;
    }

    public Certi57BFcc nudia(Integer nudia) {
        this.nudia = nudia;
        return this;
    }

    public void setNudia(Integer nudia) {
        this.nudia = nudia;
    }

    public Integer getOrsec() {
        return orsec;
    }

    public Certi57BFcc orsec(Integer orsec) {
        this.orsec = orsec;
        return this;
    }

    public void setOrsec(Integer orsec) {
        this.orsec = orsec;
    }

    public Integer getTicli() {
        return ticli;
    }

    public Certi57BFcc ticli(Integer ticli) {
        this.ticli = ticli;
        return this;
    }

    public void setTicli(Integer ticli) {
        this.ticli = ticli;
    }

    public Integer getDiaven() {
        return diaven;
    }

    public Certi57BFcc diaven(Integer diaven) {
        this.diaven = diaven;
        return this;
    }

    public void setDiaven(Integer diaven) {
        this.diaven = diaven;
    }

    public Integer getNurut() {
        return nurut;
    }

    public Certi57BFcc nurut(Integer nurut) {
        this.nurut = nurut;
        return this;
    }

    public void setNurut(Integer nurut) {
        this.nurut = nurut;
    }

    public String getDvrut() {
        return dvrut;
    }

    public Certi57BFcc dvrut(String dvrut) {
        this.dvrut = dvrut;
        return this;
    }

    public void setDvrut(String dvrut) {
        this.dvrut = dvrut;
    }

    public Float getCapital() {
        return capital;
    }

    public Certi57BFcc capital(Float capital) {
        this.capital = capital;
        return this;
    }

    public void setCapital(Float capital) {
        this.capital = capital;
    }

    public Float getInteres() {
        return interes;
    }

    public Certi57BFcc interes(Float interes) {
        this.interes = interes;
        return this;
    }

    public void setInteres(Float interes) {
        this.interes = interes;
    }

    public Float getCapmoneda() {
        return capmoneda;
    }

    public Certi57BFcc capmoneda(Float capmoneda) {
        this.capmoneda = capmoneda;
        return this;
    }

    public void setCapmoneda(Float capmoneda) {
        this.capmoneda = capmoneda;
    }

    public Float getReajuste() {
        return reajuste;
    }

    public Certi57BFcc reajuste(Float reajuste) {
        this.reajuste = reajuste;
        return this;
    }

    public void setReajuste(Float reajuste) {
        this.reajuste = reajuste;
    }

    public Float getIntmoneda() {
        return intmoneda;
    }

    public Certi57BFcc intmoneda(Float intmoneda) {
        this.intmoneda = intmoneda;
        return this;
    }

    public void setIntmoneda(Float intmoneda) {
        this.intmoneda = intmoneda;
    }

    public Integer getCodco() {
        return codco;
    }

    public Certi57BFcc codco(Integer codco) {
        this.codco = codco;
        return this;
    }

    public void setCodco(Integer codco) {
        this.codco = codco;
    }

    public Float getNewfo() {
        return newfo;
    }

    public Certi57BFcc newfo(Float newfo) {
        this.newfo = newfo;
        return this;
    }

    public void setNewfo(Float newfo) {
        this.newfo = newfo;
    }

    public Integer getComon() {
        return comon;
    }

    public Certi57BFcc comon(Integer comon) {
        this.comon = comon;
        return this;
    }

    public void setComon(Integer comon) {
        this.comon = comon;
    }

    public String getSegur() {
        return segur;
    }

    public Certi57BFcc segur(String segur) {
        this.segur = segur;
        return this;
    }

    public void setSegur(String segur) {
        this.segur = segur;
    }

    public Float getMonfi() {
        return monfi;
    }

    public Certi57BFcc monfi(Float monfi) {
        this.monfi = monfi;
        return this;
    }

    public void setMonfi(Float monfi) {
        this.monfi = monfi;
    }

    public String getCapta() {
        return capta;
    }

    public Certi57BFcc capta(String capta) {
        this.capta = capta;
        return this;
    }

    public void setCapta(String capta) {
        this.capta = capta;
    }

    public String getAutor() {
        return autor;
    }

    public Certi57BFcc autor(String autor) {
        this.autor = autor;
        return this;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Integer getSucur1() {
        return sucur1;
    }

    public Certi57BFcc sucur1(Integer sucur1) {
        this.sucur1 = sucur1;
        return this;
    }

    public void setSucur1(Integer sucur1) {
        this.sucur1 = sucur1;
    }

    public Integer getTipde1() {
        return tipde1;
    }

    public Certi57BFcc tipde1(Integer tipde1) {
        this.tipde1 = tipde1;
        return this;
    }

    public void setTipde1(Integer tipde1) {
        this.tipde1 = tipde1;
    }

    public Integer getFolio1() {
        return folio1;
    }

    public Certi57BFcc folio1(Integer folio1) {
        this.folio1 = folio1;
        return this;
    }

    public void setFolio1(Integer folio1) {
        this.folio1 = folio1;
    }

    public Float getTasad1() {
        return tasad1;
    }

    public Certi57BFcc tasad1(Float tasad1) {
        this.tasad1 = tasad1;
        return this;
    }

    public void setTasad1(Float tasad1) {
        this.tasad1 = tasad1;
    }

    public LocalDate getFedep1() {
        return fedep1;
    }

    public Certi57BFcc fedep1(LocalDate fedep1) {
        this.fedep1 = fedep1;
        return this;
    }

    public void setFedep1(LocalDate fedep1) {
        this.fedep1 = fedep1;
    }

    public LocalDate getFeven1() {
        return feven1;
    }

    public Certi57BFcc feven1(LocalDate feven1) {
        this.feven1 = feven1;
        return this;
    }

    public void setFeven1(LocalDate feven1) {
        this.feven1 = feven1;
    }

    public Integer getNudia1() {
        return nudia1;
    }

    public Certi57BFcc nudia1(Integer nudia1) {
        this.nudia1 = nudia1;
        return this;
    }

    public void setNudia1(Integer nudia1) {
        this.nudia1 = nudia1;
    }

    public Float getCapit1() {
        return capit1;
    }

    public Certi57BFcc capit1(Float capit1) {
        this.capit1 = capit1;
        return this;
    }

    public void setCapit1(Float capit1) {
        this.capit1 = capit1;
    }

    public Float getCapmo1() {
        return capmo1;
    }

    public Certi57BFcc capmo1(Float capmo1) {
        this.capmo1 = capmo1;
        return this;
    }

    public void setCapmo1(Float capmo1) {
        this.capmo1 = capmo1;
    }

    public String getFiller() {
        return filler;
    }

    public Certi57BFcc filler(String filler) {
        this.filler = filler;
        return this;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public LocalDate getFedia() {
        return fedia;
    }

    public Certi57BFcc fedia(LocalDate fedia) {
        this.fedia = fedia;
        return this;
    }

    public void setFedia(LocalDate fedia) {
        this.fedia = fedia;
    }

    public Integer getCcapit() {
        return ccapit;
    }

    public Certi57BFcc ccapit(Integer ccapit) {
        this.ccapit = ccapit;
        return this;
    }

    public void setCcapit(Integer ccapit) {
        this.ccapit = ccapit;
    }

    public Integer getCcinte() {
        return ccinte;
    }

    public Certi57BFcc ccinte(Integer ccinte) {
        this.ccinte = ccinte;
        return this;
    }

    public void setCcinte(Integer ccinte) {
        this.ccinte = ccinte;
    }

    public Integer getCcreaj() {
        return ccreaj;
    }

    public Certi57BFcc ccreaj(Integer ccreaj) {
        this.ccreaj = ccreaj;
        return this;
    }

    public void setCcreaj(Integer ccreaj) {
        this.ccreaj = ccreaj;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Certi57BFcc certi57BFcc = (Certi57BFcc) o;
        if (certi57BFcc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), certi57BFcc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Certi57BFcc{" +
            "id=" + getId() +
            ", suc='" + getSuc() + "'" +
            ", tip='" + getTip() + "'" +
            ", sucur='" + getSucur() + "'" +
            ", tipde='" + getTipde() + "'" +
            ", folio='" + getFolio() + "'" +
            ", tipre='" + getTipre() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", comuna='" + getComuna() + "'" +
            ", ciudad='" + getCiudad() + "'" +
            ", telefono='" + getTelefono() + "'" +
            ", tasad='" + getTasad() + "'" +
            ", fedep='" + getFedep() + "'" +
            ", feven='" + getFeven() + "'" +
            ", nudia='" + getNudia() + "'" +
            ", orsec='" + getOrsec() + "'" +
            ", ticli='" + getTicli() + "'" +
            ", diaven='" + getDiaven() + "'" +
            ", nurut='" + getNurut() + "'" +
            ", dvrut='" + getDvrut() + "'" +
            ", capital='" + getCapital() + "'" +
            ", interes='" + getInteres() + "'" +
            ", capmoneda='" + getCapmoneda() + "'" +
            ", reajuste='" + getReajuste() + "'" +
            ", intmoneda='" + getIntmoneda() + "'" +
            ", codco='" + getCodco() + "'" +
            ", newfo='" + getNewfo() + "'" +
            ", comon='" + getComon() + "'" +
            ", segur='" + getSegur() + "'" +
            ", monfi='" + getMonfi() + "'" +
            ", capta='" + getCapta() + "'" +
            ", autor='" + getAutor() + "'" +
            ", sucur1='" + getSucur1() + "'" +
            ", tipde1='" + getTipde1() + "'" +
            ", folio1='" + getFolio1() + "'" +
            ", tasad1='" + getTasad1() + "'" +
            ", fedep1='" + getFedep1() + "'" +
            ", feven1='" + getFeven1() + "'" +
            ", nudia1='" + getNudia1() + "'" +
            ", capit1='" + getCapit1() + "'" +
            ", capmo1='" + getCapmo1() + "'" +
            ", filler='" + getFiller() + "'" +
            ", fedia='" + getFedia() + "'" +
            ", ccapit='" + getCcapit() + "'" +
            ", ccinte='" + getCcinte() + "'" +
            ", ccreaj='" + getCcreaj() + "'" +
            "}";
    }
}
