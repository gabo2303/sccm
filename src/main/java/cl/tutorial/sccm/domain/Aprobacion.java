package cl.tutorial.sccm.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Representa una aprobación de un producto (Pactos, Ahorro, Depósito a Plazo y Letras y Bonos).
 *
 * @author Antonio Marrero Palomino.
 */
@ApiModel(description = "Representa una aprobación de un producto (Pactos, Ahorro, Depósito a Plazo y Letras y Bonos). @author Antonio Marrero Palomino.")
@Entity
@Table(name = "aprobacion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Audited
public class Aprobacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "producto")
    private String producto;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "cuadratura_contable")
    private Boolean cuadraturaContable;

    @Column(name = "intereses_pagados")
    private Boolean interesesPagados;

    @Column(name = "aprobacion_producto")
    private Boolean aprobacionProducto;

    @Column(name = "comentarios")
    private String comentarios;

    @ManyToOne
    private AnnoTributa annoTributa;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDate fechaUltimaModificacion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProducto() {
        return producto;
    }

    public Aprobacion producto(String producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getUsuario() {
        return usuario;
    }

    public Aprobacion usuario(String usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Boolean isCuadraturaContable() {
        return cuadraturaContable;
    }

    public Aprobacion cuadraturaContable(Boolean cuadraturaContable) {
        this.cuadraturaContable = cuadraturaContable;
        return this;
    }

    public void setCuadraturaContable(Boolean cuadraturaContable) {
        this.cuadraturaContable = cuadraturaContable;
    }

    public Boolean isInteresesPagados() {
        return interesesPagados;
    }

    public Aprobacion interesesPagados(Boolean interesesPagados) {
        this.interesesPagados = interesesPagados;
        return this;
    }

    public void setInteresesPagados(Boolean interesesPagados) {
        this.interesesPagados = interesesPagados;
    }

    public Boolean isAprobacionProducto() {
        return aprobacionProducto;
    }

    public Aprobacion aprobacionProducto(Boolean aprobacionProducto) {
        this.aprobacionProducto = aprobacionProducto;
        return this;
    }

    public void setAprobacionProducto(Boolean aprobacionProducto) {
        this.aprobacionProducto = aprobacionProducto;
    }

    public String getComentarios() {
        return comentarios;
    }

    public Aprobacion comentarios(String comentarios) {
        this.comentarios = comentarios;
        return this;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    /*public Long getAnnoTributaId() {
        return annoTributaId;
    }

    public Aprobacion annoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
        return this;
    }

    public void setAnnoTributaId(Long annoTributaId) {
        this.annoTributaId = annoTributaId;
    }*/

    public AnnoTributa getAnnoTributa() {
        return annoTributa;
    }

    public Aprobacion annoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
        return this;
    }

    public void setAnnoTributa(AnnoTributa annoTributa) {
        this.annoTributa = annoTributa;
    }

    public LocalDate getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public Aprobacion fechaUltimaModificacion(LocalDate fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
        return this;
    }

    public void setFechaUltimaModificacion(LocalDate fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Aprobacion aprobacion = (Aprobacion) o;
        if (aprobacion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aprobacion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Aprobacion{" + "id=" + id + ", producto='" + producto + '\'' + ", usuario='" + usuario + '\'' + ", cuadraturaContable=" + cuadraturaContable + ", interesesPagados="
            + interesesPagados + ", aprobacionProducto=" + aprobacionProducto + ", comentarios='" + comentarios + '\'' + ", annoTributa=" + annoTributa
            + ", fechaUltimaModificacion=" + fechaUltimaModificacion + '}';
    }
}
