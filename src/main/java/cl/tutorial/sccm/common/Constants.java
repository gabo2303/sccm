package cl.tutorial.sccm.common;

/**
 * Application constants.
 */
public final class Constants 
{
    //Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT                	= "system";
    public static final String ANONYMOUS_USER                	= "anonymoususer";
    public static final String DEFAULT_LANGUAGE              	= "es";
    public static final String INTERFACES_LOADING_UF         	= "uf";
    public static final String INTERFACES_LOADING_PARITY     	= "paridad";
    public static final String INTERFACES_LOADING_EURO      	= "euro";
    public static final String INTERFACES_LOADING_USD        	= "usd";
    public static final String INTERFACES_LOADING_ACCOUNTING 	= "contabilidad";

    public static final String BONOS_LETRAS_LOAD_BONOS   		= "bonos";
    public static final String BONOS_LETRAS_LOAD_LHBONOS 		= "lhbonos";
    public static final String BONOS_LETRAS_LOAD_LHDCV   		= "lhdcv";

    public static final String DATA_LOAD_ETL_DAP                 = "dap";
    public static final String DATA_LOAD_ETL_PACTOS              = "pactos";
    public static final String DATA_LOAD_ETL_AHORROS             = "ahorros";
    public static final String DATA_LOAD_ETL_AHORROS_MOVIMIENTOS = "ahorros-movimientos";
	public static final String DATA_LOAD_ETL_RUT_1890 			 = "rut-1890";
	public static final String DATA_LOAD_ETL_CLIENTE_1890 		 = "cliente-1890";

    public static final String OPTION_TODOS           			= "TODOS";
    public static final String COMMAND_JAVA_MINUS_JAR 			= "java -jar ";
    public static final String COMMAND_CD             			= "cd ";
    public static final String COMMAND_EXIT           			= "exit";
    public static final String COMMAND_CMD_SLASH_C   		 	= "cmd /c ";

    public static final String EXCEL_OPTION_BICE    			= "EXCEL_OPTION_BICE";
    public static final String EXCEL_OPTION_NO_BICE 			= "EXCEL_OPTION_NO_BICE";
    public static final String EXCEL_OPTION_ALL     			= "EXCEL_OPTION_ALL";

    public static final String LINE_SEPARATOR          			= System.getProperty("line.separator");
    public static final String PATTERN_DATE_SHORT      			= "dd/MM/yyyy";
    public static final String PATTERN_DATE_TIME_SHORT 			= "dd/MM/yyyy HH:mm";
    public static final String PATTERN_DATE_TIME_LONG  			= "dd/MM/yyyy HH:mm:ss";
    public static final String PATTERN_NUMBER_FORMAT   			= "";
    public static final String STRING_DOUBLE_FORMAT    			= "%.4f";

    public static final String ALARMA_MAS_DE_6_GIROS_SIN_REAJUSTES_NI_INTERESES = "MAS_DE_6_GIROS_SIN_REAJUSTES_NI_INTERESES";
    public static final String ALARMA_CUENTA_CERRADA                            = "CUENTA_CERRADA";
    public static final String ALARMA_CLIENTE_EXTRANJERO                        = "CLIENTE_EXTRANJERO";
    public static final String ALARMA_INTERES_REAL_MAYOR_QUE_INTERES_PAGADO     = "INTERES_REAL_MAYOR_QUE_INTERES_PAGADO";
    public static final String ALARMA_CAPITAL_E_INTERES_PAGADO_NEGATIVO         = "CAPITAL_E_INTERES_PAGADO_NEGATIVO";
    public static final String ALARMA_MAS_DE_6_GIROS                            = "MAS_DE_6_GIROS";
    public static final String ALARMA_OPCION_NULL                               = "ALARMA_OPCION_NULL";

    public static final String ALARMA_DAP_RENOVACIONES                             = "RENOVACIONES";
    public static final String ALARMA_DAP_PAGOS_ANTICIPADOS                        = "PAGOS_ANTICIPADOS";
    public static final String ALARMA_DAP_REAJUSTES_NEGATIVOS                      = "REAJUSTES_NEGATIVOS";
    public static final String ALARMA_DAP_EXCLUIR_CLIENTES_57_BIS                  = "EXCLUIR_CLIENTES_57_BIS";
    public static final String ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS = "EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS";

    public static final String  ESTADO_CUENTA_CERRADA             = "C";
    public static final String  NACIONALIDAD_EXTRANJERO           = "S";
    public static final Double  DOUBLE_VALUE_ZERO                 = new Double("0");
    public static final Integer NUMERO_DE_GIROS_6                 = 6;
    public static final String  TIPO_PLAZO_INDEFINIDO             = "I";
    public static final String  INDICADOR_PAGO_ANTICIPADO         = "A ";
    public static final String  INDICADOR_EXCLUIR_CLIENTES_57_BIS = "S";
    public static final Integer INDICADOR_DIAS_DIFERENCIA         = 7;
    public static final Long    INDICADOR_DIAS_DIFERENCIA_LONG    = new Long("7");

    public static final int SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS     = 1; // Pactos
    public static final int SELECTED_OPTION_OPER_CAPT_EMI_CERT_AHORRO     = 2; // Ahorro
    public static final int SELECTED_OPTION_OPER_CAPT_EMI_CERT_DAP        = 3; // Depósito a Plazo
    public static final int SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS      = 4; // Bonos
    public static final int SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI        = 5; // GPI
    public static final int SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM = 6; // DCV ASICOM
    public static final int TOTAL_PRODUCTS_TO_BE_APPROVED                 = 4;

    public static final String ENVERS_CHANGE_TYPE_CREATE = "0";
    public static final String ENVERS_CHANGE_TYPE_UPDATE = "1";
    public static final String ENVERS_CHANGE_TYPE_DELETE = "2";

    public static final String ENVERS_AUDIT_ZERO_HOUR = " 00:00:00";

    public static final String ENVERS_AUDIT_COLUMN_NAME_TIMESTAMP = "timestamp";
    public static final String ENVERS_AUDIT_COLUMN_NAME_PASSWORD  = "PASSWORD";
    public static final String ENVERS_AUDIT_COLUMN_NAME_DATE      = "FECHA";
    public static final String ENVERS_AUDIT_COLUMN_NAME_REVTYPE   = "revtype";

    private Constants() { }
}