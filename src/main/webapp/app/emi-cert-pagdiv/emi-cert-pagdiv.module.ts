import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EMISION_CERTIFICADOS_ROUTE, EmisionCertificado1941Component } from './';
import { EmisionCertificadosService } from './emi-cert-pagdiv.service';
import { NavbarComponent } from '../layouts/navbar/navbar.component';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmisionCertificado1941Component,
    ],
    entryComponents: [],
    providers: [
        EmisionCertificadosService, NavbarComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppEmisionCertificado1941Module {
}
