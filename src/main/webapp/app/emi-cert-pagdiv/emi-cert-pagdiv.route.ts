import { Route } from '@angular/router';
import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmisionCertificado1941Component } from './';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emi-cert-pagdiv',
    component: EmisionCertificado1941Component,
    data: {
        authorities: [ 'ROLE_SCCM_ADMIN', 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER', 'ROLE_ADMIN' ],
        pageTitle: 'sccmApp.emision-certificados.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
