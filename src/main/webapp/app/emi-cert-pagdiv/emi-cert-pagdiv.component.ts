import { Component, OnInit } from '@angular/core';
import { EmisionCertificadosService } from './emi-cert-pagdiv.service';
import { JhiAlertService } from 'ng-jhipster';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { ResponseWrapper } from '../shared/model/response-wrapper.model';
import { AnnoTributaService } from '../entities/anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../entities/anno-tributa/anno-tributa.model';
import { CertPagdivService } from '../entities/cert-pagdiv/cert-pagdiv.service';
import { CertPagdiv } from '../entities/cert-pagdiv/cert-pagdiv.model';
import { isNullOrUndefined } from 'util';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { OPTION_TODOS } from '../shared';

import { Temporal1890Service } from '../entities/temporal-1890/temporal-1890.service';

import { NavbarComponent } from '../layouts/navbar/navbar.component';

@Component({
    selector: 'jhi-emision-certificados',
    templateUrl: './emi-cert-pagdiv.component.html',
    styleUrls: [
        'emi-cert-pagdiv.css'
    ]
})
export class EmisionCertificado1941Component implements OnInit 
{	
    message: string;
    tipo = 'T';
    rut: string;
    lista: string;
    annosTributarios: AnnoTributa[];
    isJustOne = false;
    certpagdivs: CertPagdiv[];
    annoSelected: number;
    entPagdivsFlag = false;
    entPagDivSinRut = false;
    flagResult = false;
    resultMsg = '';
    resultType = '';
    classType = '';
    OPTION_TODOS = OPTION_TODOS;
	
	rutSelected: string;
	
    constructor(private emisionCertificadosService: EmisionCertificadosService, private alertService: JhiAlertService, 
				private annoTributaService: AnnoTributaService, private certPagdivService: CertPagdivService, 
				private spinnerService: Ng4LoadingSpinnerService) { }
		
    ngOnInit() { this.loadAnnosTributarios(); }
	
    certificados(idAnnoSelected) 
	{
        //this.spinnerService.show();
        this.flagResult = false;
		
		if (!isNullOrUndefined(this.rutSelected)) 
		{
            if (parseInt(this.rutSelected, 10) === 0) 
			{
                this.spinnerService.hide();
                this.resultMsg = 'Debe seleccionar cliente';
                this.resultType = 'Error: ';
                this.classType = 'alert alert-danger alert-dismissible';
                this.flagResult = true;
				
                return false;
            }
        }
		
        this.emitir(idAnnoSelected);
    }
	
    async emitir(idAnnoSelected) 
	{
        this.flagResult = false;
        
		this.resultMsg = 'El proceso se continuará ejecutando en segundo plano. Revise log al momento de terminar.';
		this.resultType = 'Info: ';
		this.classType = 'alert alert-info alert-dismissible';
		this.flagResult = true;
		
		if (this.rutSelected === '' || this.rutSelected === null || this.rutSelected === undefined) { this.rutSelected = this.OPTION_TODOS; }
		
        idAnnoSelected = idAnnoSelected.value;
        
		await this.emisionCertificadosService.emiteCertificados(this.rutSelected, idAnnoSelected).subscribe
		(
			(event: any) => 
			{
				if (event.type === HttpEventType.UploadProgress) 
				{
					const percentDone = Math.round(100 * event.loaded / event.total);
					
					console.log(`File is ${percentDone}% loaded.`);
				} 
				else if (event instanceof HttpResponse) 
				{
					if (event.status === 200)
					{
						this.resultMsg = 'Certificados generados exitosamente.';
						this.resultType = 'Éxito: ';
						this.classType = 'alert alert-success alert-dismissible';
						this.spinnerService.hide();
					}
					else 
					{
						this.resultMsg = 'No se han podido generar certificados';
						this.resultType = 'Error: ';
						this.classType = 'alert alert-danger alert-dismissible';
						this.spinnerService.hide();
					}
					
					this.flagResult = true;
				}
				else if (event.type && event.type === 3) 
				{
					this.flagResult = true;
					this.resultMsg = event.partialText;
					this.resultType = '';
					this.classType = 'alert alert-danger alert-dismissible';
					this.spinnerService.hide();
				}
			},
			
			(error: any) => 
			{ 
				console.log("HUBO UN ERROR PARA ACCEDER AL SERVICIO"); 
				console.log(error); 
			},			
			() => 
			{ 
				console.log("TERMINO LA EMISION"); 					
			}
		);
    }

    async listar() 
	{
        await this.emisionCertificadosService.listaCertificados().subscribe((event) => 
		{
			if (event.type === HttpEventType.UploadProgress) 
			{
				
			} 
			else if (event instanceof HttpResponse) 
			{
				console.log('event: ' + event.status);
				
				if (event.status === 200) 
				{
					this.alertService.success('Certificados emitidos exitósamente!.');
					console.log('Listado emitido exitósamente!');
					return event.status;
				} 
				else 
				{
					this.alertService.error('Error emitiendo certificados!.');
					console.log('Error emitiendo listado!');
					return event.status;
				}
			}
        });
    }

    loadAnnosTributarios() 
	{
        this.spinnerService.show();
        
		this.annoTributaService.query().subscribe((res: ResponseWrapper) => 
		{
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    validateJustOne() { this.isJustOne = true; }

    validateAllMdfk() { this.isJustOne = false; }

    filterByAnooTributario(annoTributaId: any) 
	{
        this.spinnerService.show();
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId();
    }

    loadAllByAnnoTributaId() 
	{
        this.entPagdivsFlag = false;
        this.flagResult = false;
        
		this.certPagdivService.loadAllByAnnoTributaId(this.annoSelected).subscribe
		(
            (res: ResponseWrapper) => 
			{
                this.certpagdivs = res.json;
                
				if (this.certpagdivs.length > 0) 
				{
                    this.entPagdivsFlag = true;
                    this.spinnerService.hide();
                } 
				else 
				{
                    this.resultMsg = 'No se han encontrado datos para año tributario seleccionado.';
                    this.resultType = 'Error: ';
                    this.classType = 'alert alert-danger alert-dismissible';
                    this.flagResult = true;
                    this.spinnerService.hide();
                }
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }

    private onError(error) 
	{
        this.spinnerService.hide();
		
        this.alertService.error(error.message, null, null);
    }
}