import { Component, OnInit } from '@angular/core';
import { EmisionRectificatoriaService } from './emi-rectif-oper-capt.service';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { ResponseWrapper } from '../shared/model/response-wrapper.model';
import { AnnoTributa } from '../entities/anno-tributa/anno-tributa.model';
import { AnnoTributaService } from '../entities/anno-tributa/anno-tributa.service';
import { Rut_tempService } from '../entities/rut-temp/rut-temp.service';
import { Subscription } from 'rxjs/Subscription';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-emi-dj-oper-capt',
    templateUrl: './emi-rectif-oper-capt.component.html',
    styleUrls: [
        'emi-rectif-oper-capt.css'
    ]
})
export class EmiRectifOperCaptComponent implements OnInit {
    message: string;
    tipo = 'T';
    rut: string;
    annosTributarios: AnnoTributa[];
    eventSubscriber: Subscription;
    flagResult = false;
    resultMsg = '';
    resultType = '';
    classType = '';
	canUse: boolean;
	
    constructor(private emisionRectificatoriaService: EmisionRectificatoriaService, private alertService: JhiAlertService, 
				private annoTributaService: AnnoTributaService, private eventManager: JhiEventManager, 
				private spinnerService: Ng4LoadingSpinnerService, private rutTempService: Rut_tempService) {  }

    ngOnInit()
	{
		this.rutTempService.query().subscribe
		(
			(res: ResponseWrapper) => 
			{
				if(res.json.length != 0) 
				{ 
					this.canUse = false;
					
					this.flagResult = true;
					this.resultMsg = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
					this.resultType = 'Info: ';
					this.classType = 'alert alert-info alert-dismissible';
				}				
				else { this.canUse = true; }
			},
			(res: ResponseWrapper) => {  }
		);
		
        this.loadAllAnnosTributarios();
        this.registerChangeInEntrada57bs();
    }

    certificados(idAnnoSelected) {
        this.spinnerService.show();
        this.emitir(idAnnoSelected);
    }

    async emitir(idAnnoSelected) {
        this.flagResult = false;
        this.resultMsg = '';
        this.resultType = '';
        let resp: any;
        this.spinnerService.show();
        idAnnoSelected = idAnnoSelected.value;
        await this.emisionRectificatoriaService.emiteRectifOperacionesCaptacion(idAnnoSelected).subscribe((event: any) => {
                if (event instanceof HttpResponse) {
                    if (event.status === 200) {
                        resp = event.body.toString().split(',');
                        if (resp[ 0 ] === 'OK') {
                            this.flagResult = true;
                            this.resultMsg = resp[ 1 ];
                            this.resultType = 'Éxito: ';
                            this.classType = 'alert alert-success alert-dismissible';
                        } else {
                            this.flagResult = true;
                            this.resultMsg = resp[ 1 ];
                            this.resultType = 'Error: ';
                            this.classType = 'alert alert-danger alert-dismissible';
                        }
                        this.spinnerService.hide();
                    } else {
                        this.flagResult = true;
                        this.resultMsg = 'Ha ocurrido un problema tratando de generar certificados';
                        this.resultType = 'Error: ';
                        this.classType = 'alert alert-danger alert-dismissible';
                        this.spinnerService.hide();
                    }
                } else if (event.type && event.type === 3) {
                    this.flagResult = true;
                    this.resultMsg = event.partialText;
                    this.resultType = '';
                    this.classType = 'alert alert-danger alert-dismissible';
                    this.spinnerService.hide();
                }
            }
        );
    }

    loadAllAnnosTributarios() {
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    registerChangeInEntrada57bs() {
        this.eventSubscriber = this.eventManager.subscribe('entrada57bListModification', (response) => this.loadAllAnnosTributarios());
    }

    private onError(error) {
        this.spinnerService.hide();
        this.alertService.error(error.message, null, null);
    }

    onAlertClose() {
        this.flagResult = false;
    }
}
