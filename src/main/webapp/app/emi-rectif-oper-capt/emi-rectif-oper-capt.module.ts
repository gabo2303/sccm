import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EmiRectifOperCaptComponent, EMISION_CERTIFICADOS_ROUTE } from './';
import { EmisionRectificatoriaService } from './emi-rectif-oper-capt.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmiRectifOperCaptComponent,
    ],
    entryComponents: [],
    providers: [
        EmisionRectificatoriaService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class EmiRectifOperCaptModule {
}
