import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmiRectifOperCaptComponent } from './';
import { SCCM_1890_ADMIN } from '../app.constants';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emi-rectif-oper-capt',
    component: EmiRectifOperCaptComponent,
    data: {
        authorities: [ SCCM_1890_ADMIN ],
        pageTitle: 'sccmApp.emision-rectificatoria.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
