import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { SERVER_API_URL } from '../app.constants';
import { CommonServices } from '../shared';

@Injectable()
export class EmisionCertificadosService 
{
	private resourceUrl = SERVER_API_URL + 'api/storage';
	
    constructor(private http: HttpClient, private commonServices: CommonServices) {  }

    emiteCertificados(rut: string, anno: string): Observable<HttpEvent<{}>> {
        
		const formdata: FormData = new FormData();
        formdata.append('rut', rut);
        formdata.append('anno', anno);
        
		let headers = this.commonServices.generateAuthHeader();
        
		const req = new HttpRequest('POST', this.resourceUrl + '/emiteCertificadosOperacionesCaptacion', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }
	
    listaCertificados(): Observable<HttpEvent<{}>> {
        
		const formdata: FormData = new FormData();
        let headers = this.commonServices.generateAuthHeader();
        
		const req = new HttpRequest('POST', this.resourceUrl + '/listaCertificados', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }
}