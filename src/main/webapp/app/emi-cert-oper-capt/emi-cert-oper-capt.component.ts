import { Component, OnInit } from '@angular/core';
import { EmisionCertificadosService } from './emi-cert-oper-capt.service';
import { JhiAlertService } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { ResponseWrapper } from '../shared/model/response-wrapper.model';
import { AnnoTributaService } from '../entities/anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../entities/anno-tributa/anno-tributa.model';
import { CertPagdivService } from '../entities/cert-pagdiv/cert-pagdiv.service';
import { Rut_tempService } from '../entities/rut-temp/rut-temp.service';
import { CertPagdiv } from '../entities/cert-pagdiv/cert-pagdiv.model';
import { Temporal1890Service } from '../entities/temporal-1890/temporal-1890.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Cert57BService } from '../entities/cert-57-b/cert-57-b.service';
import { isNullOrUndefined } from 'util';
import { OPTION_TODOS, CommonServices } from '../shared';

@Component({
    selector: 'jhi-emision-certificados',
    templateUrl: './emi-cert-oper-capt.component.html',
    styleUrls: [
        'emi-cert-oper-capt.css'
    ]
})
export class EmiCertOperCaptComponent implements OnInit {
    message: string;
    tipo = 'T';
    rut: string;
    annosTributarios: AnnoTributa[];
    isJustOne = false;
    isRutValid = false;
    rutFromCertPagDiv: CertPagdiv[];
    annoSelected: number;
    entPagdivsFlag = false;
    flagResult = false;
    resultMsg = '';
    resultType = '';
    classType = '';
    rutFilterValue: string = null;
    OPTION_TODOS = OPTION_TODOS;
	canUse: boolean;
	
    constructor(private emisionCertificadosService: EmisionCertificadosService, private alertService: JhiAlertService, 
				private annoTributaService: AnnoTributaService, private certPagdivService: CertPagdivService, 
				private spinnerService: Ng4LoadingSpinnerService, private cert57BService: Cert57BService, 
				private rutTempService: Rut_tempService, private clientes1890TemporalService: Temporal1890Service,
				private commonServices: CommonServices) {  }

    ngOnInit() 
	{
		this.rutTempService.query().subscribe
		(
			(res: ResponseWrapper) => 
			{
				if(res.json.length != 0) 
				{ 
					this.canUse = false;
					
					this.flagResult = true;
					this.resultMsg = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
					this.resultType = 'Info: ';
					this.classType = 'alert alert-info alert-dismissible';
				}				
				else { this.canUse = true; }
			},
			(res: ResponseWrapper) => {  }
		);
		
        this.loadAnnosTributarios();
        this.loadAllRutsFromSerPagDiv();
    }

    certificados() { this.emitir(); }

    async emitir()
	{
        this.flagResult = false;
        this.resultMsg = '';
        this.resultType = '';
        this.classType = '';
        //this.spinnerService.show();
        const rutSelected = this.isJustOne ? this.rutFilterValue : this.OPTION_TODOS;
        let annoSelected = '' + this.annoSelected;
        
		this.flagResult = true;
		this.resultMsg = 'El proceso se continuará ejecutando en segundo plano. Revise log al momento de terminar.';
		this.resultType = 'Info: ';
		this.classType = 'alert alert-info alert-dismissible';
		
		if (isNullOrUndefined(annoSelected)) 
		{
            this.flagResult = true;
            this.resultMsg = 'Debe seleccionar un año tributario.';
            this.resultType = 'Error: ';
            this.classType = 'alert alert-danger alert-dismissible';
            this.spinnerService.hide();
        } 
		else if (this.isJustOne && !this.isRutValid) 
		{
            this.flagResult = true;
            this.resultMsg = 'Debe ingtesar un valor del rut válido. Más de 4 díjitos.';
            this.resultType = 'Error: ';
            this.classType = 'alert alert-danger alert-dismissible';
            this.spinnerService.hide();
        } 
		else 
		{
            let resp: any;
            
			await this.emisionCertificadosService.emiteCertificados(rutSelected, annoSelected).subscribe
			(
				(event: any) => 
				{
					if (event instanceof HttpResponse) {
						if (event.status === 200) {
							resp = event.body.toString().split(',');
							if (resp[ 0 ] === 'OK') {
								this.flagResult = true;
								this.resultMsg = resp[ 1 ];
								this.resultType = 'Éxito: ';
								this.classType = 'alert alert-success alert-dismissible';
							} else {
								this.flagResult = true;
								this.resultMsg = resp[ 1 ];
								this.resultType = 'Error: ';
								this.classType = 'alert alert-danger alert-dismissible';
							}
							this.spinnerService.hide();
						} else {
							this.flagResult = true;
							this.resultMsg = 'Ha ocurrido un problema tratando de generar certificados';
							this.resultType = 'Error: ';
							this.classType = 'alert alert-danger alert-dismissible';
							this.spinnerService.hide();
						}
					} else if (event.type && event.type === 3) {
						this.flagResult = true;
						this.resultMsg = event.partialText;
						this.resultType = '';
						this.classType = 'alert alert-danger alert-dismissible';
						this.spinnerService.hide();
					}
				},
				(error: any) => { },
				
				() => 
				{
					console.log("TERMINO LÑA WEA");
				}
			);
        }
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.annoSelected = this.annosTributarios[ 0 ].id;
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    validateJustOne() {
        this.isJustOne = true;
    }

    validateAllMdfk() {
        this.isJustOne = false;
    }

    loadAllRutsFromSerPagDiv() {
        this.certPagdivService.query().subscribe(
            (res: ResponseWrapper) => {
                this.rutFromCertPagDiv = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) {
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.certificados();
    }

    valueChange($event) {
        this.isRutValid = !isNullOrUndefined(this.rutFilterValue) && this.rutFilterValue.length > 4 && !isNaN(Number(this.rutFilterValue));
    }

    private onError(error) {
        this.spinnerService.hide();
        this.alertService.error(error.message, null, null);
    }

    onAlertClose() {
        this.flagResult = false;
    }
	
	generateExcel() 
	{
        this.spinnerService.show();
        
		this.clientes1890TemporalService.query().subscribe
		(
            (res: ResponseWrapper) => 
			{
				this.commonServices.download(res.json, 'Log Certificados Emitidos 1890');
			},
            (res: ResponseWrapper) => this.onError(res.json)
        );
		
		this.spinnerService.hide();
    }
}
