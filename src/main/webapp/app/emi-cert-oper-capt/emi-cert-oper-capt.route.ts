import { Route } from '@angular/router';
import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmiCertOperCaptComponent } from './emi-cert-oper-capt.component';
import { SCCM_1890_ADMIN } from '../app.constants';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emi-cert-oper-capt',
    component: EmiCertOperCaptComponent,
    data: {
        authorities: [ SCCM_1890_ADMIN ],
        pageTitle: 'sccmApp.emision-certificados.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
