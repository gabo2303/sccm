import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EmiCertOperCaptComponent, EMISION_CERTIFICADOS_ROUTE } from './';
import { EmisionCertificadosService } from './emi-cert-oper-capt.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmiCertOperCaptComponent,
    ],
    entryComponents: [],
    providers: [
        EmisionCertificadosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class EmiCertOperCaptModule {
}
