// DO NOT EDIT THIS FILE, EDIT THE WEBPACK COMMON CONFIG INSTEAD, WHICH WILL MODIFY THIS FILE
/* tslint:disable */
let _VERSION = '0.0.1-SNAPSHOT'; // This value will be overwritten by Webpack
let _DEBUG_INFO_ENABLED = true; // This value will be overwritten by Webpack
let _SERVER_API_URL = ''; // This value will be overwritten by Webpack
/* @toreplace VERSION */
/* @toreplace DEBUG_INFO_ENABLED */
/* @toreplace SERVER_API_URL */
/* tslint:enable */
export const VERSION = _VERSION;
export const DEBUG_INFO_ENABLED = _DEBUG_INFO_ENABLED;
export const SERVER_API_URL = _SERVER_API_URL;

export const INTERFACES_LOADING_UF = 'uf';
export const INTERFACES_LOADING_PARITY = 'paridad';
export const INTERFACES_LOADING_EURO = 'euro';
export const INTERFACES_LOADING_USD = 'usd';
export const INTERFACES_LOADING_ACCOUNTING = 'contabilidad';
export const BONOS_LETRAS_LOAD_BONOS = 'bonos';
export const BONOS_LETRAS_LOAD_LHBONOS = 'lhbonos';
export const BONOS_LETRAS_LOAD_LHDCV = 'lhdcv';
export const DATA_LOAD_ETL_DAP = 'dap';
export const DATA_LOAD_ETL_PACTOS = 'pactos';
export const DATA_LOAD_ETL_AHORROS = 'ahorros';
export const DATA_LOAD_ETL_AHORROS_MOVIMIENTOS = 'ahorros-movimientos';
export const APPROVAL_PRODUCT_TYPE_DAP = 'depósito-a-plazo';
export const APPROVAL_PRODUCT_TYPE_PACTOS = 'pactos';
export const APPROVAL_PRODUCT_TYPE_AHORROS = 'ahorros';
export const APPROVAL_PRODUCT_TYPE_BONOS_LETRAS = 'bonos-y-letras';

export const ADMIN = 'ROLE_ADMIN';
export const USER = 'ROLE_USER';
export const SCCM_1890_ADMIN = 'ROLE_SCCM_1890_ADMIN';
export const SCCM_1890_ADMIN_AHORRO = 'ROLE_SCCM_1890_ADMIN_AHORRO';
export const SCCM_1890_USER_AHORRO = 'ROLE_SCCM_1890_USER_AHORRO';
export const SCCM_1890_ADMIN_DAP = 'ROLE_SCCM_1890_ADMIN_DAP';
export const SCCM_1890_USER_DAP = 'ROLE_SCCM_1890_USER_DAP';
export const SCCM_1890_ADMIN_BYL = 'ROLE_SCCM_1890_ADMIN_BYL';
export const SCCM_1890_USER_BYL = 'ROLE_SCCM_1890_USER_BYL';
export const SCCM_1890_ADMIN_PACTOS = 'ROLE_SCCM_1890_ADMIN_PACTOS';
export const SCCM_1890_USER_PACTOS = 'ROLE_SCCM_1890_USER_PACTOS';
export const SCCM_1941_ADMIN = 'ROLE_SCCM_1941_ADMIN';
export const SCCM_1941_USER = 'ROLE_SCCM_1941_USER';
export const SCCM_1944_ADMIN = 'ROLE_SCCM_1944_ADMIN';
export const SCCM_1944_USER = 'ROLE_SCCM_1944_USER';

export const CSV_SEPARATOR = ';';
