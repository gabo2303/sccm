import {Component, OnInit} from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {Account, LoginModalService, Principal} from '../shared';
import {Router} from '@angular/router';
import {LoginService} from "../shared/login/login.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
    selector: 'jhi-home',
    templateUrl: './dispatch.component.html',
    styleUrls: [
        'dispatch.css'
    ]

})
export class DispatchComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    userNameConnected: string;

    constructor(private principal: Principal,
                private loginModalService: LoginModalService,
                private eventManager: JhiEventManager,
                private router: Router,
                private loginService: LoginService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.router.navigate(['home']);
    }

}
