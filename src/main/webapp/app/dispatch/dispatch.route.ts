import { Route } from '@angular/router';

import { DispatchComponent } from './';

export const HOME_ROUTE: Route = {
    path: 'dispatch',
    component: DispatchComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
