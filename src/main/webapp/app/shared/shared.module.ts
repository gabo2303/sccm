import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    AccountService,
    AuthServerProvider,
    CSRFService,
    HasAnyAuthorityDirective,
    JhiLoginModalComponent,
    LoginModalService,
    LoginService,
    Principal,
    SccmSharedCommonModule,
    SccmSharedLibsModule,
    StateStorageService,
    UserService,
} from './';
import { CargainterfaceService } from './cargainterface/cargainterface.service';
import { CargainterfaceService57Bis } from './cargainterface57Bis/cargainterface57Bis.service';
import { ExcelService } from './excel/ExcelService';
import { CargaAhorroService } from './cargaAhorro/cargaAhorro.service';
import { LoadDataEtlComponent } from './load-data-etl/load-data-etl.component';

@NgModule({
    imports: [
        SccmSharedLibsModule,
        SccmSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        CargainterfaceService,
        CargainterfaceService57Bis,
        ExcelService,
        CargaAhorroService
    ],
    entryComponents: [ JhiLoginModalComponent ],
    exports: [
        SccmSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class SccmSharedModule {
}
