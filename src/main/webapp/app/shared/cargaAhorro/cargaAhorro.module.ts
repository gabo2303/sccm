import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../';
import { CARGAAHORRO_ROUTE, CargaAhorroComponent } from './';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ CARGAAHORRO_ROUTE ], { useHash: true })
    ],
    declarations: [
        CargaAhorroComponent,
    ],
    entryComponents: [],
    providers: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppCargaAhorroModule {
}
