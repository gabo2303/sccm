import { Route } from '@angular/router';
import { UserRouteAccessService } from '../auth/user-route-access-service';
import { CargaAhorroComponent } from './cargaAhorro.component';

export const CARGAAHORRO_ROUTE: Route = {
    path: 'cargaAhorro',
    component: CargaAhorroComponent,
    data: {
        authorities: [ 'ROLE_SCCM_ADMIN', 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER', 'ROLE_USER', 'ROLE_ADMIN' ],
        pageTitle: 'cargaAhorro.title'
    },
    canActivate: [ UserRouteAccessService ]
};
