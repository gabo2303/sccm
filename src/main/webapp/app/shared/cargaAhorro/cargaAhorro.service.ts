import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { CommonServices } from '../common.service';

@Injectable()
export class CargaAhorroService {

    constructor(private http: HttpClient, private commonServices: CommonServices) {
    }

    pushFileToStorageOpeCapAhorro(file: File): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/uplIntOpeCapAhorro', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
        return this.http.request(req);
    }

    pushFileToStorageOpeCapAhorroMov(file: File): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/uplIntOpeCapAhorroMov', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
        return this.http.request(req);
    }
}
