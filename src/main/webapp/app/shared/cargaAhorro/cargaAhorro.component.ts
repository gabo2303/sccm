import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { CargaAhorroService } from './cargaAhorro.service';
import { Router } from '@angular/router';
import { JhiAlertService } from 'ng-jhipster';
import { Entrada57bService } from '../../entities/entrada-57-b/entrada-57-b.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-cargaAhorro',
    templateUrl: './cargaAhorro.component.html',
    styleUrls: [
        'cargaAhorro.css'
    ]
})
export class CargaAhorroComponent implements OnInit {

    filesToUpload: Array<File> = [];
    message: string;
    selectedFiles: FileList;
    currentFileUpload: File;
    progress: { percentage: number } = { percentage: 0 };
    cargainterface57BFlagResponse = false;
    classType = '';
    bodyMessage = '';
    headMessage = '';
    allowMov = false;

    constructor(private cargaInterfaceService: CargaAhorroService, private router: Router, private alertService: JhiAlertService, private entrada57bService: Entrada57bService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
    }

    fileChangeEvent(fileInput: any) {
        this.filesToUpload = [];
        this.filesToUpload = <Array<File>>fileInput.target.files;
        // this.product.photo = fileInput.target.files[0]['name'];
    }

    selectFile(event) {
        this.selectedFiles = event.target.files;
    }

    async uploader() {
        this.spinnerService.show();
        this.cargainterface57BFlagResponse = false;
        const value = (<HTMLInputElement>document.getElementById('cin')).value;
        if (value.trim() === '') {
            this.classType = 'alert alert-danger alert-dismissible';
            this.headMessage = 'Error:';
            this.bodyMessage = ' Debe seleccionar archivo.';
            this.cargainterface57BFlagResponse = true;
            this.spinnerService.hide();
            return false;
        } else {
            const files: Array<File> = this.filesToUpload;
            let i = 0;
            for (i; i < files.length; i++) {
                await this.upload(i);
            }
        }
    }

    async upload(index: number) {
        this.spinnerService.show();
        this.progress.percentage = 0;
        this.currentFileUpload = this.filesToUpload[ index ];
        await this.cargaInterfaceService.pushFileToStorageOpeCapAhorro(this.currentFileUpload)
        .subscribe(
            result => {
                if (result.type > 1) {
                    if (result instanceof HttpResponse) {
                        if (result.statusText.toString() === 'OK') {
                            this.allowMov = true;
                            this.classType = 'alert alert-success alert-dismissible';
                            this.headMessage = 'Éxito:';
                            this.bodyMessage = ' Archivo subido correctamente.';
                            this.cargainterface57BFlagResponse = true;
                            this.spinnerService.hide();
                        } else {
                            this.classType = 'alert alert-danger alert-dismissible';
                            this.headMessage = 'Error:';
                            this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo. 4';
                            this.cargainterface57BFlagResponse = true;
                            this.spinnerService.hide();
                        }
                    }
                    else {
                        this.classType = 'alert alert-danger alert-dismissible';
                        this.headMessage = 'Error:';
                        this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo. 3';
                        this.cargainterface57BFlagResponse = true;
                        this.spinnerService.hide();
                    }
                }
            },
            error => {
                this.classType = 'alert alert-danger alert-dismissible';
                this.headMessage = 'Error:';
                this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo. 1';
                this.cargainterface57BFlagResponse = true;
                this.spinnerService.hide();
            }
        );
        this.selectedFiles = undefined

    }

    async uploaderMov() {
        this.spinnerService.show();
        this.cargainterface57BFlagResponse = false;
        const value = (<HTMLInputElement>document.getElementById('cinMov')).value;
        if (value.trim() === '') {
            this.classType = 'alert alert-danger alert-dismissible';
            this.headMessage = 'Error:';
            this.bodyMessage = ' Debe seleccionar archivo.';
            this.cargainterface57BFlagResponse = true;
            this.spinnerService.hide();
            return false;
        } else {
            const files: Array<File> = this.filesToUpload;
            let i = 0;
            for (i; i < files.length; i++) {
                await this.uploadMov(i);
            }
        }
    }

    async uploadMov(index: number) {
        this.spinnerService.show();
        this.progress.percentage = 0;
        this.currentFileUpload = this.filesToUpload[ index ];
        await this.cargaInterfaceService.pushFileToStorageOpeCapAhorroMov(this.currentFileUpload)
        .subscribe(
            result => {
                if (result.type > 1) {
                    if (result instanceof HttpResponse) {
                        if (result.statusText.toString() === 'OK') {
                            this.allowMov = false;
                            this.classType = 'alert alert-success alert-dismissible';
                            this.headMessage = 'Éxito:';
                            this.bodyMessage = ' Archivo subido correctamente.';
                            this.cargainterface57BFlagResponse = true;
                            this.spinnerService.hide();
                        } else {
                            this.classType = 'alert alert-danger alert-dismissible';
                            this.headMessage = 'Error:';
                            this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo. 4';
                            this.cargainterface57BFlagResponse = true;
                            this.spinnerService.hide();
                        }
                    }
                    else {
                        this.classType = 'alert alert-danger alert-dismissible';
                        this.headMessage = 'Error:';
                        this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo. 3';
                        this.cargainterface57BFlagResponse = true;
                        this.spinnerService.hide();
                    }
                }
            },
            error => {
                this.classType = 'alert alert-danger alert-dismissible';
                this.headMessage = 'Error:';
                this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo. 1';
                this.cargainterface57BFlagResponse = true;
                this.spinnerService.hide();
            }
        );
        this.selectedFiles = undefined

    }
}
