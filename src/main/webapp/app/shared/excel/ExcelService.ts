import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { ExportToCSV } from '@molteni/export-csv/dist';

const EXCEL_TYPE = 'application/octet-stream';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

    constructor() {
    }

    public exportAsExcelFile(json: any[], excelFileName: string, excelWorkSheetName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, excelWorkSheetName)
        // { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'binary' });		

        function s2ab(s) {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i !== s.length; ++i) {
                view[ i ] = s.charCodeAt(i) & 0xFF;
            }
            return buf;
        }

        FileSaver.saveAs(new Blob([ s2ab(excelBuffer) ],
			{ type: EXCEL_TYPE }),
            excelFileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);

        // this.saveAsExcelFile(excelBuffer, excelFileName);
    }
	
	public exportAsCSVFile(json: any[], excelFileName: string, excelWorkSheetName: string): void 
	{
		var exportToCSV = new ExportToCSV();
		
		console.log(json);
		
		exportToCSV.exportAllToCSV(json, excelFileName + '_export_' + new Date().getTime() + '.csv');        
    }
}
