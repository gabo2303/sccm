import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { Http, Response, ResponseContentType } from '@angular/http';

import { createRequestOption, ResponseWrapper } from '../shared';
import { ExcelService } from './excel/ExcelService';
import { CSV_SEPARATOR, SERVER_API_URL } from '../app.constants';

@Injectable()
export class CommonServices 
{
    private CSV_SEPARATOR = CSV_SEPARATOR;
    private resourceUrl = SERVER_API_URL + 'api/storage';

    constructor(private http: HttpClient, private localStorage: LocalStorageService, 
				private sessionStorage: SessionStorageService, private httpNormal: Http) { }

    public pushFileToServerAndProcessWithEtl(file: File, option: string): Observable<HttpEvent<{}>> 
	{
        const formData: FormData = new FormData();
        formData.append('file', file);
        formData.append('option', option);
        let headers = this.generateAuthHeader();
        
		console.log("OPCION SELECCIONADA: " + option);
		
		const req = new HttpRequest('POST', `${this.resourceUrl}/uploadEtlDataWithOption`, formData, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
        
		return this.http.request(req);
    }

    public generateAuthHeader() 
	{
        let headers = new HttpHeaders();
        const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
        
		if (!!token) { headers = headers.append('Authorization', 'Bearer ' + token); }
		
        return headers;
    }

    public generateExcel(document: Document, tableId: string, excelService: ExcelService, eventManager: JhiEventManager, excelFileName: string, 
						 eventName: string, eventContent: string, jhiAlertService: JhiAlertService, noDataErrorMessage: string) 
	{
        const table: HTMLTableElement = <HTMLTableElement> document.getElementById(tableId);
        const rowCount = !isNullOrUndefined(table) ? table.rows.length : 0;
        
		if (rowCount > 1) 
		{
            let contentData = '[';
            let dataElement = '';
            const headers: any [] = [];
            const tableHeader: HTMLTableSectionElement = table.tHead;
            
			for (let i = 0; i < table.rows.length; i++) 
			{
                if (i > 0) { dataElement = '{'; }
				
                const headersLength: number = tableHeader.rows[ 0 ].cells.length;
                
				for (let j = 0; j < headersLength; j++) 
				{
                    if (i === 0) 
					{
                        // Obtenemos la primera fila de la tabla o cabecera y limpiamos tag html y espacios vacíos
                        headers[ j ] = table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').replace('$', '').replace('.', '').replace('.', '').replace('.', '').trim();
                    }
					else
					{
                        if (!isNullOrUndefined(table.rows[ i ].cells[ j ])) 
						{
                            if (j < headersLength - 1) {
                                dataElement += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('&nbsp;', '').trim() + '",';
                            } else {
                                dataElement += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('&nbsp;', '').trim() + '"';
                            }
                        }
                    }
                }
				
                if (i > 0) { dataElement += '},'; }
				
                if (dataElement !== '{},') { contentData += dataElement; }
            }
			
            contentData += ']';
            contentData = contentData.replace('},]', '}]');

            //excelService.exportAsExcelFile(JSON.parse(contentData.toString()), excelFileName, excelFileName);
			excelService.exportAsCSVFile(JSON.parse(contentData.toString()), excelFileName, excelFileName);
        } 
		else { jhiAlertService.error(noDataErrorMessage); }
    }

    /**
     * Convert Json to CSV data.
     *
     * @param objArray
     * @constructor
     */
    ConvertToCSV(objArray: any, filename: string) 
	{
		const array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
		let str = '';
		let row = '';

		for (let index in objArray[ 0 ]) 
		{
			if(filename == 'Log Certificados Emitidos 1890')
			{	
				if (index == 'nro_cer')
				{
					row += 'cant_emisiones' + this.CSV_SEPARATOR;
				}	
				else if(index == 'totalcoluno')
				{
					row += 'tot_ir_dapaho_pos' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolunoneg')
				{
					row += 'tot_ir_dapaho_neg' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcoldos')
				{
					row += 'tot_ir_cc_pos' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcoldosneg')
				{
					row += 'tot_ir_cc_neg' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcoltres')
				{
					row += 'tot_ir_inv_pos' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcoltresneg')
				{
					row += 'tot_ir_inv_neg' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolcuatro')
				{
					row += 'tot_ir_dapaho_pos_act' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolcuatroneg')
				{
					row += 'tot_ir_dapaho_neg_act' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolcinco')
				{
					row += 'tot_ir_cc_pos_act' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolcinconeg')
				{
					row += 'tot_ir_cc_neg_act' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolseis')
				{
					row += 'tot_ir_inv_pos_act' + this.CSV_SEPARATOR;
				}
				else if(index == 'totalcolseisneg')
				{
					row += 'tot_ir_inv_neg_act' + this.CSV_SEPARATOR;
				}
				else if(index == 'tip_16')
				{
					row += 'tot_intnom_otras_rtas' + this.CSV_SEPARATOR;
				}
				else if(index == 'tip_17')
				{
					row += 'impto_adic_valnom_pag' + this.CSV_SEPARATOR;
				}
				else if(index == 'annoTributa')
				{
					row += '' + this.CSV_SEPARATOR;
				}
				else { row += index + this.CSV_SEPARATOR; }
			}
			else { row += index + this.CSV_SEPARATOR; }
		}
		
		row = row.slice(0, -1);
		// append Label row with line break
		str += row + '\r\n';
		
		console.log('Nombre Archivo: ' + filename);
		
		let cont = 0;
		
		for (let i = 0; i < array.length; i++) 
		{
			let line = '';
			cont++;
			
			for (let index in array[ i ]) 
			{				
				if (line != '') { line += this.CSV_SEPARATOR }
				
				if (filename == 'Log Certificados Emitidos 1890' && index == 'id')
				{
					line += '"' + cont + '"';
				}
				else
				{					
					console.log(array[ i ][ index ]);
					
					if (array[ i ][ index ] != undefined)
					{	
						line += '"' + array[ i ][ index ] + '"';
					}
					else { line += '""'; }
				}
			}

			str += line + '\r\n';
		}

        return str;
    }

    /**
     * Download CSV
     *
     * @param data
     * @param filename
     */
    download(data: any, filename: string) 
	{
		console.log("LLEGA AL METODO QUE DESCARGAR CSV");
		console.log(data);
		
        const csvData = this.ConvertToCSV(data, filename);
        
		console.log("EXITO EN LA CONVERSION DE DATA A CSV");
		
		let a: any = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        
		const blob = new Blob([ csvData ], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);
        
		a.href = url;

        a.download = filename + '.csv';
		
        a.click();
    }
	
	/**
     * Download Word
     *
     * @param filename
     */
    downloadWord(archivo: String, req?: any) : Observable<ResponseWrapper> 
	{
		console.log("LLAMA AL SERVICIO PARA EL WORD");
		
		console.log(archivo);
        const options = createRequestOption(req);
		options.responseType = ResponseContentType.ArrayBuffer;       	
		
		return this.httpNormal.get(`${this.resourceUrl}` + "/getDocxFile?archivo=" + archivo, options).map(data => data);    
    }
	
	downloadLog(archivo: String, req?: any) : Observable<ResponseWrapper> 
	{
		console.log("LLAMA AL SERVICIO PARA EL LOG");
		
		console.log(archivo);
        const options = createRequestOption(req);
		options.responseType = ResponseContentType.ArrayBuffer;       	
		
		return this.httpNormal.get(`${this.resourceUrl}` + "/getLogsEtl?archivo=" + archivo, options).map(data => data);    
    }
}