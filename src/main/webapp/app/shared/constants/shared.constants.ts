export const HTTP_ERROR_CODE_404 = 404;
export const NO_VALID_DATES = 'NO_VALID_DATES';
export const TASKS_LIMIT_NUMBER_MENU = 10;
export const PROCESS_LIMIT_NUMBER_MENU = 5;
export const NO_MESSAGE_TEXT = 'No error message available';
export const UNEXPECTED_ERROR_MESSAGE_TEXT = 'An unexpected error has occurred. Please contact the system administrator.';
export const ERROR_404_MESSAGE_TEXT = 'An error has occurred connecting with the server. Please contact the system administrator.';
export const GENERIC_ERROR_MESSAGE_TEXT = 'An error has occurred on the server. Please try again';
export const PAGE_NOT_FOUND = 'Dear user, the page you are trying to access does not exist.';
export const NOT_FOUND_TASK_WITH_ID = 'Dear user, the task you are trying to access does no exist.';
export const USER_ALREADY_EXISTS_WITH_ID = 'The user you are trying to add already exists.';
export const USER_NOT_EXISTS_WITH_ID = 'The user you are trying to update not exists.';
export const PAGE_CONNECTION_TIMEOUT_ERROR = 'Connection timed out trying to connect to';
export const EMPTY_JSON_ARRAY = '[]';
export const EMPTY_STRING_FOR_CANDIDATES_VALUE = ' ';
export const SELECT_BLANK_OPTION = '--SELECCIONE--';
export const DATA_TYPE_OPTIONS_OPER_CAPT_EMI_CERT = [ { code: 1, name: 'Pactos', routerLinkValue: 'pactos' }, { code: 2, name: 'Ahorro', routerLinkValue: 'ahorro' },
    { code: 3, name: 'Depósito a Plazo', routerLinkValue: 'dap' }, { code: 4, name: 'Bonos', routerLinkValue: 'bonos' }, { code: 5, name: 'GPI', routerLinkValue: 'lh-bonos' },
    { code: 6, name: 'DCV ASICOM', routerLinkValue: 'lhdcv' } ];
export const SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS = 1; // Pactos
export const SELECTED_OPTION_OPER_CAPT_EMI_CERT_AHORRO = 2; // Ahorro
export const SELECTED_OPTION_OPER_CAPT_EMI_CERT_DAP = 3; // Depósito a Plazo
export const SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS = 4; // Bonos -> Tabla - Bonos
export const SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI = 5; // GPI -> Tabla - LHBonos
export const SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM = 6; // DCV ASICOM -> Tabla - LHDCV
export const ALARMA_DAP_REAJUSTES_NEGATIVOS = 'REAJUSTES_NEGATIVOS';
export const ALARMA_DAP_RENOVACIONES = 'RENOVACIONES';
export const ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS = 'EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS';
export const OPTION_TODOS = 'TODOS';
// Formato de las fechas en el sistema
export const PATTERN_DECIMAL_NUMBER = '1.4-4';
export const PATTERN_DATE_SHORT = 'dd/MM/yyyy';
export const PATTERN_DATE_ENG_SHORT = 'yyyy-MM-dd';
export const PATTERN_DATE_SHORT_DATE_PICKER = 'dd/mm/yyyy';
export const PATTERN_DATE_TIME_SHORT = 'dd/MM/yyyy HH:mm';
export const PATTERN_DATE_TIME_EXTENDED = 'dd/MM/yyyy HH:mm:ss';
export const DATE_PICKER_LOCALE = 'es';
export const DATE_FORMAT_TABLES = 'EEE dd/MM/yy HH:mm';
export const PATTERN_DATE_SHORT_DATE_PIPE = 'dd/MM/yyyy';
export const DATE_T_VALUE = 'T';
export const DATE_ZERO_TIME_VALUE = '00:00:00';
export const PATTERN_TIME_ENG_SHORT = 'HH:mm';
export const DATE_Z_VALUE = 'Z';
export const EXCEL_OPTION_WITH_NEMOTECNICO = 'EXCEL_OPTION_WITH_NEMOTECNICO';
export const EXCEL_OPTION_WITHOUT_NEMOTECNICO = 'EXCEL_OPTION_WITHOUT_NEMOTECNICO';
export const EXCEL_OPTION_BICE = 'EXCEL_OPTION_BICE';
export const EXCEL_OPTION_NO_BICE = 'EXCEL_OPTION_NO_BICE';
export const EXCEL_OPTION_ALL = 'EXCEL_OPTION_ALL';
export const TABLE_NAMES_AUDIT = [ { displayName: 'Ahorro', value: 'AHORRO' },
    { displayName: 'Ahorro Movimiento', value: 'AHORRO_MOV' },
    { displayName: 'Año Tributario', value: 'ANNO_TRIBUTA' },
    { displayName: 'Aprobacion', value: 'APROBACION' },
    { displayName: 'Bonos', value: 'BONOS' },
    { displayName: 'Carga Monetaria', value: 'CARGA_MONETA' },
    { displayName: 'Cart 57 B', value: 'CART_57_B' },
    { displayName: 'Certi 57 B Fcc', value: 'CERTI_57_B_FCC' },
    { displayName: 'Cert 57 B', value: 'CERT_57_B' },
    { displayName: 'Cert Pagdiv', value: 'CERT_PAGDIV' },
    { displayName: 'Cert Version', value: 'CERT_VERSION' },
    { displayName: 'Cliente 1890', value: 'CLIENTE_1890' },
    { displayName: 'Cliente Bice', value: 'CLIENTE_BICE' },
    { displayName: 'Contabilidad', value: 'CONTABILIDAD' },
    { displayName: 'Correción Monetaria', value: 'CORREC_MONETA' },
    { displayName: 'Dap', value: 'DAP' },
    { displayName: 'Datos Fijos', value: 'DATOS_FIJOS' },
    { displayName: 'Entrada 57 B', value: 'ENTRADA_57_B' },
    { displayName: 'Ent Pagdiv', value: 'ENT_PAGDIV' },
    { displayName: 'Euro', value: 'EURO' },
    { displayName: 'Fut', value: 'FUT' },
    { displayName: 'Glosa', value: 'GLOSA' },
    { displayName: 'Instrumento', value: 'INSTRUMENTO' },
    { displayName: 'Usuarios', value: 'JHI_USER' },
    { displayName: 'Usuarios y Roles', value: 'JHI_USER_AUTHORITY' },
    { displayName: 'Lhbonos', value: 'LHBONOS' },
    { displayName: 'Lhdcv', value: 'LHDCV' },
    { displayName: 'Moneda', value: 'MONEDA' },
    { displayName: 'Pactos', value: 'PACTOS' },
    { displayName: 'Producto', value: 'PRODUCTO' },
    { displayName: 'Representante', value: 'REPRESENTANTE' },
    { displayName: 'Rut 1890', value: 'RUT_1890' },
    { displayName: 'Sal Ini 57 B', value: 'SAL_INI_57_B' },
    { displayName: 'Sal Prox Per', value: 'SAL_PROX_PER' },
    { displayName: 'Tabla Moneda', value: 'TABLA_MONEDA' },
    { displayName: 'Tipo Alerta', value: 'TIPO_ALERTA' },
    { displayName: 'Total Prod', value: 'TOTAL_PROD' },
    { displayName: 'UF', value: 'UF' },
    { displayName: 'Ult Dia Mes', value: 'ULT_DIA_MES' },
    { displayName: 'USD', value: 'USD' } ];

// Constantes relacionadas con las tablas
export const TASKS_TABLE_ITEMS_PER_PAGE = 10;
export const GENERAL_AVAILABILITY_TABLE_ITEMS_PER_PAGE = 30;
