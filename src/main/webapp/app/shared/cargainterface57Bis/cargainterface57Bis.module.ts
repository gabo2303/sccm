import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../';

import { CARGAINTERFACE57BIS_ROUTE, Cargainterface57BisComponent } from './';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ CARGAINTERFACE57BIS_ROUTE ], { useHash: true })
    ],
    declarations: [
        Cargainterface57BisComponent,
    ],
    entryComponents: [],
    providers: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppCargainterface57BisModule {
}
