import { Component, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { CargainterfaceService57Bis } from './cargainterface57Bis.service';
import { Router } from '@angular/router';
import { JhiAlertService } from 'ng-jhipster';
import { Entrada57bService } from '../../entities/entrada-57-b/entrada-57-b.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-cargainterface57-bis',
    templateUrl: './cargainterface57Bis.component.html',
    styleUrls: [
        'cargainterface57Bis.css'
    ]
})
export class Cargainterface57BisComponent implements OnInit {

    filesToUpload: Array<File> = [];
    message: string;
    selectedFiles: FileList;
    currentFileUpload: File;
    progress: { percentage: number } = { percentage: 0 };
    canValidate: boolean;
    canMerge: boolean;
    canLoad: boolean;
    canUpload: boolean;
    cargainterface57BFlagResponse = false;
    classType = '';
    bodyMessage = '';
    headMessage = '';

    constructor(private cargaInterfaceService: CargainterfaceService57Bis, private router: Router, private alertService: JhiAlertService,
                private entrada57bService: Entrada57bService, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
    }

    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        // this.product.photo = fileInput.target.files[0]['name'];
    }

    selectFile(event) {
        this.selectedFiles = event.target.files;
    }

    async uploader() {
        this.spinnerService.show();
        this.cargainterface57BFlagResponse = false;
        const value = (<HTMLInputElement>document.getElementById('cin')).value;
        if (value.trim() === '') {
            this.classType = 'alert alert-danger alert-dismissible';
            this.headMessage = 'Error:';
            this.bodyMessage = ' Debe seleccionar archivo.';
            this.cargainterface57BFlagResponse = true;
            this.spinnerService.hide();
            return false;
        } else {
            const files: Array<File> = this.filesToUpload;
            let i = 0;
            for (i; i < files.length; i++) {
                await this.upload(i);
            }
            if (i === files.length) {
                this.allowLoad();
                this.classType = 'alert alert-success alert-dismissible';
                this.headMessage = 'Éxito:';
                this.bodyMessage = ' Archivo subido correctamente.';
                this.cargainterface57BFlagResponse = true;
                this.spinnerService.hide();

            } else {
                this.classType = 'alert alert-danger alert-dismissible';
                this.headMessage = 'Error:';
                this.bodyMessage = ' Ha ocurrido un problema intentando subir archivo.';
                this.cargainterface57BFlagResponse = true;
                this.spinnerService.hide();
            }
        }
    }

    // -----------> SUBE UN FICHERO A LA BASE DE DATOS
    async upload(index: number) {
        this.progress.percentage = 0;
        this.currentFileUpload = this.filesToUpload[ index ];
        await this.cargaInterfaceService.pushFileToStorage57Bis(this.currentFileUpload).subscribe((event) => {
            if (event.type === HttpEventType.UploadProgress) {
                this.progress.percentage = Math.round(100 * event.loaded / event.total);
            } else if (event instanceof HttpResponse) {
                if (event.status === 200) {
                    this.progress.percentage = 0;
                    return event.status;
                } else {
                    this.progress.percentage = 0;
                    return event.status;
                }
            }
        });
        this.selectedFiles = undefined

    }

    // -----------> EJECUTA ETL PARA CARGAR INTERFACE EN BASE DE DATOS
    async executeSp1InsertaEntrada57B() {
        this.spinnerService.show();
        this.cargainterface57BFlagResponse = false;
        await this.entrada57bService.executeSp1InsertaEntrada57B().subscribe((event) => {
            if (event.status === 200) {
                this.allowLoad();
                this.classType = 'alert alert-success alert-dismissible';
                this.headMessage = 'Éxito:';
                this.bodyMessage = ' Datos cargados correctamente.';
                this.cargainterface57BFlagResponse = true;
                this.spinnerService.hide();
            } else {
                this.classType = 'alert alert-danger alert-dismissible';
                this.headMessage = 'Error:';
                this.bodyMessage = ' Ha ocurrido un problema intentando cargar datos.';
                this.cargainterface57BFlagResponse = true;
                this.spinnerService.hide();
            }
        });
        this.canLoad = false;
    }

    allowValidate() {
        this.canValidate = true;
    }

    allowUpload() {
        this.canUpload = true;
    }

    allowMerge() {
        this.canMerge = true;
    }

    allowLoad() {
        this.canLoad = true;
    }
}
