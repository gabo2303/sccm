import { Route } from '@angular/router';
import { UserRouteAccessService } from '../auth/user-route-access-service';
import { Cargainterface57BisComponent } from './cargainterface57Bis.component';

export const CARGAINTERFACE57BIS_ROUTE: Route = {
    path: 'cargainterfaceInversiones',
    component: Cargainterface57BisComponent,
    data: {
        authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
        pageTitle: 'cargainterface.title'
    },
    canActivate: [ UserRouteAccessService ]
};
