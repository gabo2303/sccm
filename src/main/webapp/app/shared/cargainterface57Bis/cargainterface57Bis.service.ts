import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CommonServices } from '../common.service';

@Injectable()
export class CargainterfaceService57Bis {

    constructor(private http: HttpClient, private commonServices: CommonServices) {
    }

    pushFileToStorage57Bis(file: File): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/uploadInterfaces57Bis', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
        return this.http.request(req);
    }

    executeAllSps(): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/executeAllSps57Bis', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }
}
