import { Pipe, PipeTransform } from '@angular/core';

const PADDING = '0000';

@Pipe({ name: 'replaceDotByComma' })
export class ReplaceDotByCommaPipe implements PipeTransform {

    private DECIMAL_OLD_SEPARATOR: string;
    private DECIMAL_NEW_SEPARATOR: string;

    constructor() {
        this.DECIMAL_OLD_SEPARATOR = '.';
        this.DECIMAL_NEW_SEPARATOR = ',';
    }

    transform(value: number | string, fractionSize = 4): string {
        let integer: string, fraction: string;
        [ integer, fraction = '' ] = (value || '').toString().split(this.DECIMAL_OLD_SEPARATOR);
        fraction = fractionSize > 0 ? this.DECIMAL_NEW_SEPARATOR + (fraction + PADDING).substring(0, fractionSize) : '';
        return integer + fraction;
    }

}
