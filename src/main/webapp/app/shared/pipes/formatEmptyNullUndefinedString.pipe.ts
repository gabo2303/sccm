import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({ name: 'formatEmptyNullUndefinedString' })
export class FormatEmptyNullUndefinedString implements PipeTransform {

    /**
     * Pipe para dar fomrato a los strings si son vacíos, null o undefined.
     *
     * @param {string} value
     * @returns {string}
     */
    transform(value: string): string {
        let transformedValue: string = value;
        if (isNullOrUndefined(value) || value === '') {
            transformedValue = '─';
        }
        return transformedValue;
    }

}
