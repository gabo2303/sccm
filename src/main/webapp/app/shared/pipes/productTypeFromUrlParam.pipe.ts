import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'productTypeFromUrlParam' })
export class ProductTypeFromUrlParamPipe implements PipeTransform {

    transform(input: string): string {
        let transform = input.replace(/-/g, ' ');
        return transform.length > 0 ? transform.replace(/\w\S*/g, (txt => txt[ 0 ].toUpperCase() + txt.substr(1).toLowerCase())) : '';
    }

}
