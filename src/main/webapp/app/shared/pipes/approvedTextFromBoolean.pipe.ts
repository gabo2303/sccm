import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({ name: 'approvedTextFromBoolean' })
export class ApprovedTextFromBooleanPipe implements PipeTransform {

    transform(value: boolean): string {
        return 'sccmApp.aprobacion.' + (isNullOrUndefined(value) || !value ? 'notApproved' : 'approved');
    }

}
