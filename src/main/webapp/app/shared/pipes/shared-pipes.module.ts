import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormatEmptyNullUndefinedString } from './formatEmptyNullUndefinedString.pipe';
import { GroupByPipe } from './groupBy.pipe';
import { AbsoluteValuePipe } from './absoluteValue.pipe';
import { ReplaceDotByCommaPipe } from './replaceDotByComma.pipe';
import { ApprovedTextFromBooleanPipe } from './approvedTextFromBoolean.pipe';
import { ProductTypeFromUrlParamPipe } from './productTypeFromUrlParam.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        FormatEmptyNullUndefinedString, GroupByPipe, AbsoluteValuePipe, ReplaceDotByCommaPipe, ApprovedTextFromBooleanPipe, ProductTypeFromUrlParamPipe
    ],
    providers: [
        ProductTypeFromUrlParamPipe
    ],
    exports: [
        DatePipe, FormatEmptyNullUndefinedString, GroupByPipe, AbsoluteValuePipe, ReplaceDotByCommaPipe, ApprovedTextFromBooleanPipe, ProductTypeFromUrlParamPipe
    ]
})
export class SharedPipesModule {
}
