import { Component, OnInit, ViewChild } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
import { DEBUG_INFO_ENABLED } from '../../app.constants';
import { CommonServices } from '../common.service';
import { Rut_tempService } from '../../entities/rut-temp/rut-temp.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-load-data-etl',
    templateUrl: './load-data-etl.component.html',
    styles: []
})
export class LoadDataEtlComponent implements OnInit 
{
    filesToUpload: Array<File> = [];
    message: string;
    currentFileUpload: File;
    progress: { percentage: number } = { percentage: 0 };
    loadDataEtlFlagResponse = false;
    classType = '';
    bodyMessage = '';
    headMessage = '';
	canUse: boolean;
    @ViewChild('fileInput') fileInput: any;
    selectedOption: string;
	flagTerminoCarga: boolean;
	
    constructor(private commonServices: CommonServices, private spinnerService: Ng4LoadingSpinnerService, 
				private route: ActivatedRoute, private rutTempService: Rut_tempService) { }

    ngOnInit() 
	{
		this.flagTerminoCarga = false;
		
        this.route.params.subscribe((data: Params) => 
		{			
            this.selectedOption = data[ 'option' ];
			console.log(this.selectedOption);
			
			if(this.selectedOption == 'bonos' || this.selectedOption == 'lhbonos' || this.selectedOption == 'lhdcv'
			|| this.selectedOption == 'dap' || this.selectedOption == 'ahorros' || this.selectedOption == 'ahorros-movimientos'
			|| this.selectedOption == 'pactos' || this.selectedOption == 'rut-1890' || this.selectedOption == 'cliente-1890')
			{
				this.rutTempService.query().subscribe
				(
					(res: ResponseWrapper) => 
					{
						if(res.json.length != 0) 
						{
							this.canUse = false;
							
							this.loadDataEtlFlagResponse = true;
							this.bodyMessage = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
							this.headMessage = 'Info: ';
							this.classType = 'alert alert-info alert-dismissible';
						}
						else { this.canUse = true; }
					},
					(res: ResponseWrapper) => {  }
				);
			}
			else { this.canUse = true; }
			
            this.clear();
        });
    }

    fileChangeEvent(fileInput: any) { this.filesToUpload = <Array<File>>fileInput.target.files; }

    uploadFile() 
	{
        this.spinnerService.show();
        const files: Array<File> = this.filesToUpload;
        this.loadDataEtlFlagResponse = false;
        const value = this.fileInput.nativeElement.value;
        
		if (files.length === 0 || isNullOrUndefined(value) || value.trim() === '') 
		{
            if (DEBUG_INFO_ENABLED) { console.log('No file selected!') }
			
            this.classType = 'alert alert-danger alert-dismissible fade show';
            this.headMessage = 'Error: ';
            this.bodyMessage = 'Debe seleccionar archivo.';
            this.loadDataEtlFlagResponse = true;
            this.spinnerService.hide();
			
            return;
        }
        this.currentFileUpload = files[ 0 ];

        this.commonServices.pushFileToServerAndProcessWithEtl(this.currentFileUpload, this.selectedOption).subscribe
		(
            (event) => 
			{
                if (event.type === HttpEventType.UploadProgress) 
				{
                    const percentDone = Math.round(100 * event.loaded / event.total);
                    
					if (DEBUG_INFO_ENABLED) { console.log(`File is ${percentDone}% loaded.`); }
					
                    this.classType = 'alert alert-info alert-dismissible fade show';
                    this.headMessage = 'Progreso: ';
                    this.bodyMessage = 'Archivo subido al servidor en un ' + percentDone + '%. ' + (percentDone === 100 ? 'Por favor espere, se están procesando los datos' +
                        ' en el ETL...' : '');
                    this.loadDataEtlFlagResponse = true;
                } 
				else if (event instanceof HttpResponse) 
				{
                    if (DEBUG_INFO_ENABLED) { console.log('File is completely loaded!'); }
					
                    /*this.classType = 'alert alert-success alert-dismissible fade show';
                    this.headMessage = 'Éxito: ';
                    this.bodyMessage = 'El archivo fue procesado correctamente.';
                    this.loadDataEtlFlagResponse = true;*/
                }
            },
            (err) => 
			{
                if (DEBUG_INFO_ENABLED) { console.log('Upload Error:', err); }
				
                let error: any = err.error;
                
				if (err.error instanceof ProgressEvent) { this.bodyMessage = 'No se ha podido completar la carga del archivo.'; } 
				
				else 
				{
                    this.bodyMessage = error;
                    
					try 
					{
                        error = JSON.parse(error);
                        this.bodyMessage = error.title + ' - ' + error.detail;
                    } 
					catch (e) {  }
                }
				
                this.classType = 'alert alert-danger alert-dismissible fade show';
                this.headMessage = 'Error: ';
                this.loadDataEtlFlagResponse = true;
                this.spinnerService.hide();
				
				if(this.selectedOption == 'pactos' || this.selectedOption == 'bonos' || this.selectedOption == 'lhbonos' ||
				this.selectedOption == 'lhdcv' || this.selectedOption == 'dap' || this.selectedOption == 'ahorros' ||
				this.selectedOption == 'ahorros-movimientos' || this.selectedOption == 'contabilidad' ||
				this.selectedOption == 'rut-1890' || this.selectedOption == 'cliente-1890')
				{
					this.flagTerminoCarga = true;
				}
            }, 
			() => 
			{
                if (DEBUG_INFO_ENABLED) { console.log('Upload done'); }
				
				this.classType = 'alert alert-success alert-dismissible fade show';
				this.headMessage = 'Finalizado: ';
				this.bodyMessage = 'El proceso de carga ha finalizado.';
				this.loadDataEtlFlagResponse = true;				
				
				if(this.selectedOption == 'pactos' || this.selectedOption == 'bonos' || this.selectedOption == 'lhbonos' ||
				this.selectedOption == 'lhdcv' || this.selectedOption == 'dap' || this.selectedOption == 'ahorros' ||
				this.selectedOption == 'ahorros-movimientos' || this.selectedOption == 'contabilidad' || 
				this.selectedOption == 'rut-1890' || this.selectedOption == 'cliente-1890')
				{
					this.flagTerminoCarga = true;
				}
				
                this.spinnerService.hide();
            }
        )
    }

    removeAlert() 
	{
        this.loadDataEtlFlagResponse = false;
        this.classType = 'alert alert-danger alert-dismissible fade hide';
        this.bodyMessage = '';
        this.headMessage = '';
    }

    clear() 
	{
        this.removeAlert();
        this.filesToUpload = [];
        this.loadDataEtlFlagResponse = true;
		this.flagTerminoCarga = false;
		
		try 
		{
            this.fileInput.value = '';
            this.fileInput.nativeElement.value = '';
        } 
		catch (e) 
		{
            if (DEBUG_INFO_ENABLED) { console.error(e); }
        }
    }
	
	descargarLog(tipo)
	{		
		var nombreLog = '';
		
		if(this.selectedOption == 'pactos') { nombreLog = tipo + 'Pactos'; }
		
		else if(this.selectedOption == 'bonos') { nombreLog = tipo + 'Bonos'; }
		
		else if(this.selectedOption == 'lhbonos') { nombreLog = tipo + 'LhBonos'; }	 
		
		else if(this.selectedOption == 'lhdcv') { nombreLog = tipo + 'Lhdcv'; }
		
		else if(this.selectedOption == 'dap') { nombreLog = tipo + 'Dap'; }
		
		else if(this.selectedOption == 'ahorros') { nombreLog = tipo + 'Ahorro'; }
		
		else if(this.selectedOption == 'ahorros-movimientos') { nombreLog = tipo + 'AhorroMovimentos';	}
		
		else if(this.selectedOption == 'contabilidad') { nombreLog = tipo + 'Contabilidad';	}
		
		else if(this.selectedOption == 'cliente-1890') { nombreLog = tipo + 'Cliente1890'; }
		
		else if(this.selectedOption == 'rut-1890') { nombreLog = tipo + 'Rut1890'; }
		
		this.commonServices.downloadLog(nombreLog).subscribe
		(
			(res) => 
			{ 				
				let file = new Blob([res['_body']], { type: 'text/csv'});            
				var fileURL = URL.createObjectURL(file);
				
				var descargar = document.createElement("a");
				descargar.download = nombreLog + ".csv";
				descargar.href = fileURL;
				descargar.click();
				
				this.classType = 'alert alert-success alert-dismissible fade show';
				this.headMessage = 'Éxito: ';
				this.bodyMessage = 'El log ha sido descargado a su computador.';
				this.loadDataEtlFlagResponse = true;
			},
			(res: ResponseWrapper) => 
			{
				this.classType = 'alert alert-info alert-dismissible fade show';
				this.bodyMessage = 'No existe log.';
				this.headMessage = 'Info: ';
				this.loadDataEtlFlagResponse = true;
			}
		);
	}
}