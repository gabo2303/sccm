import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../auth/user-route-access-service';
import { LoadDataEtlComponent } from './load-data-etl.component';
import { SCCM_1890_USER_AHORRO, SCCM_1890_USER_BYL, SCCM_1890_USER_DAP, SCCM_1890_USER_PACTOS } from '../../app.constants';

export const LOAD_DATA_ETL_ROUTE: Routes = [
    {
        path: 'load-data-etl-il/:option', // Carga de interfaces
        component: LoadDataEtlComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-pactos/:option', // Carga de pactos
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-ahorro/:option', // Carga de ahorro
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-ahorro-mov/:option', // Carga de ahorro movimiento
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-dap/:option', // Carga de dap
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-byl/:option', // Carga de letras y bonos
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-rut-1890/:option', // Carga de Rut 1890
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'load-data-etl-cliente-1890/:option', // Carga de Cliente 1890
        component: LoadDataEtlComponent,
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'loadDataEtl.title'
        },
        canActivate: [ UserRouteAccessService ]
    } 
];
