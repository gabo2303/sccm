import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SccmSharedModule } from '../index';
import { RouterModule } from '@angular/router';
import { LoadDataEtlComponent } from './load-data-etl.component';
import { LOAD_DATA_ETL_ROUTE } from './load-data-etl.route';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(LOAD_DATA_ETL_ROUTE, { useHash: true })
    ],
    declarations: [ LoadDataEtlComponent ],
    entryComponents: [],
    providers: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class LoadDataEtlModule {
}
