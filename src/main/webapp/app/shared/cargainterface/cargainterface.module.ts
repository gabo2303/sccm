import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../';
import { CARGAINTERFACE_ROUTE, CargainterfaceComponent } from './';

@NgModule({
    imports: 
	[
        SccmSharedModule,
        RouterModule.forRoot([ CARGAINTERFACE_ROUTE ], { useHash: true })
    ],
    declarations: [ CargainterfaceComponent, ],
    entryComponents: [],
    providers: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class SccmAppCargainterfaceModule { }