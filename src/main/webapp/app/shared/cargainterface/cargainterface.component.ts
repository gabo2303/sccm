import { Component, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { CargainterfaceService } from './cargainterface.service';
import { Router } from '@angular/router';
import { JhiAlertService } from 'ng-jhipster';
import { CertPagdivService } from '../../entities/cert-pagdiv/cert-pagdiv.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-cargainterface',
    templateUrl: './cargainterface.component.html'
})
export class CargainterfaceComponent implements OnInit 
{
    filesToUpload: Array<File> = [];
    selectedFiles: FileList;
    currentFileUpload: File;
    progress: { percentage: number } = { percentage: 0 };
    canLoad: boolean;

    constructor(private cargaInterfaceService: CargainterfaceService, private router: Router, private alertService: JhiAlertService, private certPagDivService: CertPagdivService,
                private spinnerService: Ng4LoadingSpinnerService) { }

    ngOnInit() { }

    fileChangeEvent(fileInput: any) { this.filesToUpload = <Array<File>>fileInput.target.files; }

    selectFile(event) { this.selectedFiles = event.target.files; }
	
	// SUBE EL ARCHIVO AL FICHERO CORRESPONDIENTE
    async uploader() 
	{
        this.spinnerService.show();
        const value = (<HTMLInputElement>document.getElementById('cin')).value;
        
		if (value.trim() === '') 
		{
			this.alertService.error("cargainterface.seleccionarArchivo");
            this.spinnerService.hide();
            
			return false;
        } 
		else 
		{
            const files: Array<File> = this.filesToUpload;
            let i = 0;
            
			for (i; i < files.length; i++) { await this.upload(i); }
			
            if (i === files.length) 
			{
                this.allowLoad();
				
				this.alertService.success("cargainterface.subidaExitosa");
                
				this.spinnerService.hide();
            } 
			else 
			{
				this.alertService.error("cargainterface.subidaFallida");
                
				this.spinnerService.hide();
            }
        }
    }

    // EJECUTA ETL PARA CARGAR INTERFACE EN BASE DE DATOS
    async load1941() 
	{
        this.spinnerService.show();
        this.progress.percentage = 0;
        
		await this.cargaInterfaceService.loadIface1941(this.currentFileUpload.name).subscribe((event) => 
		{                
			if (event.type === HttpEventType.UploadProgress) 
			{
				this.progress.percentage = Math.round(100 * event.loaded / event.total);
			} 
			else if (event instanceof HttpResponse) 
			{
				if (event.status === 200) 
				{						
					this.spinnerService.hide();
					this.alertService.success("cargainterface.interfazExitosa");
					
					return event.status;
				} 
				else 
				{
					this.spinnerService.hide();
					this.alertService.error("cargainterface.interfazFallida");
					
					return event.status;
				}
			}
		});
		
        this.canLoad = false;
    }

    // SUBE UN FICHERO A LA BASE DE DATOS
    async upload(index: number) 
	{
        this.progress.percentage = 0;
        this.currentFileUpload = this.filesToUpload[ index ];
        
		await this.cargaInterfaceService.pushFileToStorage(this.currentFileUpload).subscribe((event) => 
		{           
			if (event.type === HttpEventType.UploadProgress) 
			{
                this.progress.percentage = Math.round(100 * event.loaded / event.total);
            } 
			else if (event instanceof HttpResponse) 
			{
                if (event.status === 200) 
				{
                    this.progress.percentage = 0;
                    return event.status;
                } 
				else 
				{
                    this.progress.percentage = 0;
                    return event.status;
                }
            }
        });
		
        this.selectedFiles = undefined;
    }

    allowLoad() { this.canLoad = true; }
}