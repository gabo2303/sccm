import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { CommonServices } from '../common.service';

@Injectable()
export class CargainterfaceService 
{
	private resourceUrl = SERVER_API_URL + 'api/storage';

    constructor(private http: HttpClient, private commonServices: CommonServices) {  }

    pushFileToStorage(file: File): Observable<HttpEvent<{}>> 
	{		
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        let headers = this.commonServices.generateAuthHeader();
        
		const req = new HttpRequest('POST', this.resourceUrl + '/uploadInterfaces', formdata, 
		{
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
		
        return this.http.request(req);
    }

    loadIface1941(filename: string): Observable<HttpEvent<{}>> 
	{		
        const formdata: FormData = new FormData();
        let headers = this.commonServices.generateAuthHeader();
        headers = headers.append('Content-Type', 'undefined');
        
		const req = new HttpRequest('POST', this.resourceUrl + '/load1941/' + filename, formdata, 
		{
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }
}