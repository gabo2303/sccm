import { Route } from '@angular/router';
import { UserRouteAccessService } from '../auth/user-route-access-service';
import { CargainterfaceComponent } from './cargainterface.component';

export const CARGAINTERFACE_ROUTE: Route = 
{
    path: 'cargainterface',
    component: CargainterfaceComponent,
    data: 
	{
        authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
        pageTitle: 'cargainterface.title'
    },
    canActivate: [ UserRouteAccessService ]
};