import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Account, LoginModalService, Principal, CommonServices, ResponseWrapper } from '../shared';
import { Router } from '@angular/router';
import { LoginService } from '../shared/login/login.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]

})
export class HomeComponent implements OnInit 
{
    account: Account;
    modalRef: NgbModalRef;
    userNameConnected: string;
	
    constructor(private principal: Principal, private loginModalService: LoginModalService, private eventManager: JhiEventManager,
                private router: Router, private loginService: LoginService, private spinnerService: Ng4LoadingSpinnerService, 
				private commonServices: CommonServices, private jhiAlertService: JhiAlertService) { }
	
    ngOnInit() 
	{
        this.isAdmin();
        
		this.router.events.subscribe((path) => { this.spinnerService.hide(); });
		
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() 
	{
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }
	
    isAuthenticated() { return this.principal.isAuthenticated(); }

    login() { this.modalRef = this.loginModalService.open(); }

    logout() 
	{
        this.loginService.logout();
        this.router.navigate([ 'dispatch' ]);
    }
	
    isAdmin(): void 
	{
        if (this.isAuthenticated() && this.principal.hasAnyAuthority([ 'ROLE_ADMIN' ])) 
		{
            this.principal.identity().then((account) => {
                this.account = account;
                this.userNameConnected = this.account.firstName + ' ' + this.account.lastName;
            });
        } 
		else { this.account = null; }
    }
	
	downloadDoc(doc)
	{
		if(doc != "57bis")
		{
			this.commonServices.downloadWord(doc).subscribe
			(
				(res) => 
				{ 
					let file = new Blob([res['_body']], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });            
					var fileURL = URL.createObjectURL(file);
					//window.open(fileURL);
					
					var descargar = document.createElement("a");
					descargar.download = doc + ".docx";
					descargar.href = fileURL;
					descargar.click();
				},
				(res: ResponseWrapper) => { }
			);
		}
		else
		{
			this.jhiAlertService.error("home.57bis");
		}
	}
}