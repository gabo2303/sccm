import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { CommonServices, SccmSharedModule, UserRouteAccessService } from './shared';
import { SccmHomeModule } from './home/home.module';
import { SccmAdminModule } from './admin/admin.module';
import { SccmAccountModule } from './account/account.module';
import { SccmEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import { SccmAppCargainterfaceModule } from './shared/cargainterface/cargainterface.module';
import { SccmAppCargainterface57BisModule } from './shared/cargainterface57Bis/cargainterface57Bis.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ActiveMenuDirective, ErrorComponent, FooterComponent, JhiMainComponent, LayoutRoutingModule, NavbarComponent, PageRibbonComponent, ProfileService } from './layouts';
import { SccmAppEmisionCertificado1941Module } from './emi-cert-pagdiv/index';
import { SccmAppEmisionDj1941Module } from './emi-dj-pagdiv/index';
import { SccmAppEmisionCertificado57BisModule } from './emi-cert-57bis/emi-cert-57bis.module';
import { SccmAppEmisionDj57BModule } from './emi-dj-57b/emi-dj-57b.module';
import { SccmDispatchModule } from './dispatch/dispatch.module';
import { SccmAppCargaAhorroModule } from './shared/cargaAhorro/cargaAhorro.module';
import { LoadDataEtlModule } from './shared/load-data-etl/load-data-etl.module';
import { EmiCertOperCaptModule } from './emi-cert-oper-capt';
import { EmiDjOperCaptModule } from './emi-dj-oper-capt';
import { EmiRectifOperCaptModule } from './emi-rectif-oper-capt';
import { SccmAppEntityChangesAuditModule } from './entity-changes-audit';

// jhipster-needle-angular-add-module-import JHipster will add new module here

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        SccmSharedModule,
        SccmHomeModule,
        SccmAdminModule,
        SccmAccountModule,
        SccmEntityModule,
        SccmAppCargainterfaceModule,
        SccmAppCargainterface57BisModule,
        HttpClientModule,
        SccmAppEmisionCertificado1941Module,
        SccmAppEmisionDj1941Module,
        SccmAppEmisionCertificado57BisModule,
        SccmAppEntityChangesAuditModule,
        Ng4LoadingSpinnerModule,
        SccmAppEmisionDj57BModule,
        EmiCertOperCaptModule,
        EmiDjOperCaptModule,
        EmiRectifOperCaptModule,
        SccmDispatchModule,
        SccmAppCargaAhorroModule,
        LoadDataEtlModule

        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        CommonServices,
        UserRouteAccessService,
        HttpClient
    ],
    bootstrap: [ JhiMainComponent ]
})
export class SccmAppModule {
}
