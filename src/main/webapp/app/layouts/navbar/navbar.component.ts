import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { ProfileService } from '../profiles/profile.service';
import { JhiLanguageHelper, LoginModalService, LoginService, Principal } from '../../shared';

import { 
    ADMIN,
    APPROVAL_PRODUCT_TYPE_AHORROS,
    APPROVAL_PRODUCT_TYPE_BONOS_LETRAS,
    APPROVAL_PRODUCT_TYPE_DAP,
    APPROVAL_PRODUCT_TYPE_PACTOS,
    BONOS_LETRAS_LOAD_BONOS,
    BONOS_LETRAS_LOAD_LHBONOS,
    BONOS_LETRAS_LOAD_LHDCV,
    DATA_LOAD_ETL_AHORROS,
    DATA_LOAD_ETL_AHORROS_MOVIMIENTOS,
    DATA_LOAD_ETL_DAP,
    DATA_LOAD_ETL_PACTOS,
    INTERFACES_LOADING_ACCOUNTING,
    INTERFACES_LOADING_EURO,
    INTERFACES_LOADING_PARITY,
    INTERFACES_LOADING_UF,
    INTERFACES_LOADING_USD,
    SCCM_1890_ADMIN,
    SCCM_1890_ADMIN_AHORRO,
    SCCM_1890_ADMIN_BYL,
    SCCM_1890_ADMIN_DAP,
    SCCM_1890_ADMIN_PACTOS,
    SCCM_1890_USER_AHORRO,
    SCCM_1890_USER_BYL,
    SCCM_1890_USER_DAP, SCCM_1890_USER_PACTOS, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1944_ADMIN, SCCM_1944_USER,
    VERSION
} from '../../app.constants';
import { ProductoService } from '../../entities/producto/producto.service';
import { Producto } from '../../entities/producto/producto.model';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { InstrumentoService } from '../../entities/instrumento/instrumento.service';
import { Instrumento } from '../../entities/instrumento/instrumento.model';
import { Temporal1890Service } from '../../entities/temporal-1890/temporal-1890.service';

import * as myGlobals from '../../globals'; 

declare var $: any;

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        'navbar.css'
    ]
})
export class NavbarComponent implements OnInit {
	
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    products: Producto[] = [];
    instruments: Instrumento[] = [];
    isCustomSubSubMenuInit = false;
	is1890Corriendo : boolean;

	muestra = false;
    classType = '';
    bodyMessage = '';
    headMessage = '';

    INTERFACES_LOADING_UF = INTERFACES_LOADING_UF;
    INTERFACES_LOADING_PARITY = INTERFACES_LOADING_PARITY;
    INTERFACES_LOADING_EURO = INTERFACES_LOADING_EURO;
    INTERFACES_LOADING_USD = INTERFACES_LOADING_USD;
    INTERFACES_LOADING_ACCOUNTING = INTERFACES_LOADING_ACCOUNTING;

    BONOS_LETRAS_LOAD_BONOS = BONOS_LETRAS_LOAD_BONOS;
    BONOS_LETRAS_LOAD_LHBONOS = BONOS_LETRAS_LOAD_LHBONOS;
    BONOS_LETRAS_LOAD_LHDCV = BONOS_LETRAS_LOAD_LHDCV;

    DATA_LOAD_ETL_DAP = DATA_LOAD_ETL_DAP;
    DATA_LOAD_ETL_PACTOS = DATA_LOAD_ETL_PACTOS;
    DATA_LOAD_ETL_AHORROS = DATA_LOAD_ETL_AHORROS;
    DATA_LOAD_ETL_AHORROS_MOVIMIENTOS = DATA_LOAD_ETL_AHORROS_MOVIMIENTOS;

    APPROVAL_PRODUCT_TYPE_DAP = APPROVAL_PRODUCT_TYPE_DAP;
    APPROVAL_PRODUCT_TYPE_PACTOS = APPROVAL_PRODUCT_TYPE_PACTOS;
    APPROVAL_PRODUCT_TYPE_AHORROS = APPROVAL_PRODUCT_TYPE_AHORROS;
    APPROVAL_PRODUCT_TYPE_BONOS_LETRAS = APPROVAL_PRODUCT_TYPE_BONOS_LETRAS;


    ROLE_ADMIN = ADMIN;
    ROLE_SCCM_1890_ADMIN = SCCM_1890_ADMIN;
    ROLE_SCCM_1890_ADMIN_AHORRO = SCCM_1890_ADMIN_AHORRO;
    ROLE_SCCM_1890_USER_AHORRO = SCCM_1890_USER_AHORRO;
    ROLE_SCCM_1890_ADMIN_DAP = SCCM_1890_ADMIN_DAP;
    ROLE_SCCM_1890_USER_DAP = SCCM_1890_USER_DAP;
    ROLE_SCCM_1890_ADMIN_BYL = SCCM_1890_ADMIN_BYL;
    ROLE_SCCM_1890_USER_BYL = SCCM_1890_USER_BYL;
    ROLE_SCCM_1890_ADMIN_PACTOS = SCCM_1890_ADMIN_PACTOS;
    ROLE_SCCM_1890_USER_PACTOS = SCCM_1890_USER_PACTOS;
    ROLE_SCCM_1941_ADMIN = SCCM_1941_ADMIN;
    ROLE_SCCM_1941_USER = SCCM_1941_USER;
    ROLE_SCCM_1944_ADMIN = SCCM_1944_ADMIN;
    ROLE_SCCM_1944_USER = SCCM_1944_USER;

    constructor(private loginService: LoginService, private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper, private principal: Principal,
                private loginModalService: LoginModalService, private profileService: ProfileService, private router: Router, private productoService: ProductoService,
                private jhiAlertService: JhiAlertService, private spinnerService: Ng4LoadingSpinnerService, 
				private instrumentoService: InstrumentoService, private temporal1890Service: Temporal1890Service) 
	{
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
		this.is1890Corriendo = true;
    }

    ngOnInit() 
	{
        this.isAdmin();
		
        this.router.events.subscribe((path) => {
            this.isAdmin();
            this.spinnerService.hide();
        });

        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        this.profileService.getProfileInfo().subscribe((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    trackId(index: number, item: Producto) {
        return item.id;
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate([ 'dispatch' ]);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    getAllProducts() {
        this.spinnerService.show();
        this.productoService.loadAllOrderByIdDesc().subscribe(
            (res: ResponseWrapper) => {
                this.products = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => {
                this.spinnerService.hide();
            }
        );
    }

    getAllInstruments() {
        this.spinnerService.show();
        this.instrumentoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.instruments = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => {
                this.spinnerService.hide();
            }
        );
    }

    isAdmin() {
        if (this.isAuthenticated() && this.principal.hasAnyAuthority([ 'ROLE_ADMIN' ])) {
            this.getAllProducts();
            this.getAllInstruments();
            return true;
        }
        return false;
    }

    wdf(event, instId) 
	{
		$('.dropdown' + instId).next('ul').toggle('drop');
						
		event.stopPropagation();
		event.preventDefault();
    }

    wdfs(event, instId, className) {
        event.preventDefault();
        const $this = $('.dropdown' + className + instId).parent().find('ul');
        $('.dropdownSubmenu ul').not($this).hide();
        $('.dropdown' + className + instId).next('ul').toggle('drop');
        event.stopPropagation();
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
	
	haceAlgo()
	{		
		this.is1890Corriendo = !this.is1890Corriendo;
				
		console.log(this.is1890Corriendo);
	}
	
	removeAlert() 
	{
        this.muestra = false;
        this.classType = 'alert alert-danger alert-dismissible fade hide';
        this.bodyMessage = '';
        this.headMessage = '';
    }
}
