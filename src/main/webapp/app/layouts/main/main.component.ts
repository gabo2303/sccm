import { Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { JhiLanguageHelper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

// declare var $: any;

@Component({
    selector: 'jhi-main',
    templateUrl: './main.component.html',

})
export class JhiMainComponent implements OnInit {
    template = `<img src="loading.gif">`;

    constructor(private jhiLanguageHelper: JhiLanguageHelper, private router: Router, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.router.events.subscribe((event) => {
            this.spinnerService.show();
            if (event instanceof NavigationEnd) {
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
                this.spinnerService.hide();
            }
        });
    }

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = (routeSnapshot.data && routeSnapshot.data[ 'pageTitle' ]) ? routeSnapshot.data[ 'pageTitle' ] : 'sccmApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }
}
