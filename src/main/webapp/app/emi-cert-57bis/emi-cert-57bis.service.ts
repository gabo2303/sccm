import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { CommonServices } from '../shared';

@Injectable()
export class EmisionCertificadosService {

    constructor(private http: HttpClient, private commonServices: CommonServices) {
    }

    emiteCertificados(rut: string, anno: string): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        formdata.append('rut', rut);
        formdata.append('anno', anno);
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/emiteCertificados57Bis', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }

    listaCertificados(): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/listaCertificados', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }

}
