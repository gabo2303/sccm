import { Route } from '@angular/router';
import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmisionCertificado57BisComponent } from './emi-cert-57bis.component';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emi-cert-57bis',
    component: EmisionCertificado57BisComponent,
    data: {
        authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
        pageTitle: 'sccmApp.emision-certificados.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
