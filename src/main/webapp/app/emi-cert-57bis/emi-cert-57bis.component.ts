import { Component, OnInit } from '@angular/core';
import { EmisionCertificadosService } from './emi-cert-57bis.service';
import { JhiAlertService } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { ResponseWrapper } from '../shared/model/response-wrapper.model';
import { AnnoTributaService } from '../entities/anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../entities/anno-tributa/anno-tributa.model';
import { CertPagdivService } from '../entities/cert-pagdiv/cert-pagdiv.service';
import { CertPagdiv } from '../entities/cert-pagdiv/cert-pagdiv.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Cert57BService } from '../entities/cert-57-b/cert-57-b.service';
import { OPTION_TODOS } from '../shared';

@Component({
    selector: 'jhi-emision-certificados',
    templateUrl: './emi-cert-57bis.component.html',
    styleUrls: [
        'emi-cert-57bis.css'
    ]
})
export class EmisionCertificado57BisComponent implements OnInit {
    message: string;
    tipo = 'T';
    rut: string;
    annosTributarios: AnnoTributa[];
    rutFromCertPagDiv: CertPagdiv[];
    annoSelected: number;
    flagResult = false;
    resultMsg = '';
    resultType = '';
    classType = '';
    OPTION_TODOS = OPTION_TODOS;

    constructor(private emisionCertificadosService: EmisionCertificadosService, private alertService: JhiAlertService, private annoTributaService: AnnoTributaService,
                private certPagdivService: CertPagdivService, private spinnerService: Ng4LoadingSpinnerService, private cert57BService: Cert57BService) {
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadAllRutsFromSerPagDiv();
    }

    certificados() {
        this.emitir();
    }

    async emitir() {
        this.flagResult = false;
        this.resultMsg = '';
        this.resultType = '';
        this.classType = '';
        this.spinnerService.show();
        const rutSelected: string = this.OPTION_TODOS;
        const annoSelected = '' + this.annoSelected;
        let resp: string [];
        await this.emisionCertificadosService.emiteCertificados(rutSelected, annoSelected).subscribe((event: any) => {
                if (event instanceof HttpResponse) {
                    if (event.status === 200) {
                        resp = event.body.toString().split(',');
                        if (resp[ 0 ] === 'OK') {
                            this.flagResult = true;
                            this.resultMsg = resp[ 1 ];
                            this.resultType = 'Éxito: ';
                            this.classType = 'alert alert-success alert-dismissible';
                        } else {
                            this.flagResult = true;
                            this.resultMsg = resp[ 1 ];
                            this.resultType = 'Error: ';
                            this.classType = 'alert alert-danger alert-dismissible';
                        }
                        this.spinnerService.hide();
                    } else {
                        this.flagResult = true;
                        this.resultMsg = 'Ha ocurrido un problema tratando de generar certificados';
                        this.resultType = 'Error: ';
                        this.classType = 'alert alert-danger alert-dismissible';
                        this.spinnerService.hide();
                    }
                } else if (event.type && event.type === 3) {
                    this.flagResult = true;
                    this.resultMsg = event.partialText;
                    this.resultType = '';
                    this.classType = 'alert alert-danger alert-dismissible';
                    this.spinnerService.hide();
                }
            }
        );
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadAllRutsFromSerPagDiv() {
        this.certPagdivService.query().subscribe(
            (res: ResponseWrapper) => {
                this.rutFromCertPagDiv = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) {
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.certificados();
    }

    private onError(error) {
        this.spinnerService.hide();
        this.alertService.error(error.message, null, null);
    }

}
