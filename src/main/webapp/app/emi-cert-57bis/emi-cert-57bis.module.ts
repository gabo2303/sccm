import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EMISION_CERTIFICADOS_ROUTE, EmisionCertificado57BisComponent } from './';
import { EmisionCertificadosService } from './emi-cert-57bis.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmisionCertificado57BisComponent,
    ],
    entryComponents: [],
    providers: [
        EmisionCertificadosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppEmisionCertificado57BisModule {
}
