import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmisionDj57BComponent } from './';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emision-dj-57b',
    component: EmisionDj57BComponent,
    data: {
        authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
        pageTitle: 'sccmApp.emision-certificados.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
