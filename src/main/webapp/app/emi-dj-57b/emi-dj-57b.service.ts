import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CommonServices } from '../shared';

@Injectable()
export class EmisionCertificadosService {

    constructor(private http: HttpClient, private commonServices: CommonServices) {
    }

    emiteDJ(idAnnoSelected: string): Observable<HttpEvent<{}>> {
        const formData = new FormData();
        formData.append('anno', idAnnoSelected);
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/emiteDJ57B', formData, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
        return this.http.request(req);
    }

    listaDJs(): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();
        let headers = this.commonServices.generateAuthHeader();
        const req = new HttpRequest('POST', '/api/storage/listaCertificados', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
        return this.http.request(req);
    }

}
