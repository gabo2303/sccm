import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EMISION_CERTIFICADOS_ROUTE, EmisionDj57BComponent } from './';
import { EmisionCertificadosService } from './emi-dj-57b.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmisionDj57BComponent,
    ],
    entryComponents: [],
    providers: [
        EmisionCertificadosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppEmisionDj57BModule {
}
