import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EntPagdiv } from './ent-pagdiv.model';
import { EntPagdivService } from './ent-pagdiv.service';

@Injectable()
export class EntPagdivPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private entPagdivService: EntPagdivService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.entPagdivService.find(id).subscribe((entPagdiv) => {
                    if (entPagdiv.fechaCarga) {
                        entPagdiv.fechaCarga = {
                            year: entPagdiv.fechaCarga.getFullYear(),
                            month: entPagdiv.fechaCarga.getMonth() + 1,
                            day: entPagdiv.fechaCarga.getDate()
                        };
                    }
                    if (entPagdiv.fePagTmp) {
                        entPagdiv.fePagTmp = {
                            year: entPagdiv.fePagTmp.getFullYear(),
                            month: entPagdiv.fePagTmp.getMonth() + 1,
                            day: entPagdiv.fePagTmp.getDate()
                        };
                    }
                    this.ngbModalRef = this.entPagdivModalRef(component, entPagdiv);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.entPagdivModalRef(component, new EntPagdiv());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    entPagdivModalRef(component: Component, entPagdiv: EntPagdiv): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.entPagdiv = entPagdiv;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
