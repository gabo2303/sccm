import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { EntPagdivComponent } from './ent-pagdiv.component';
import { EntPagdivDetailComponent } from './ent-pagdiv-detail.component';
import { EntPagdivPopupComponent } from './ent-pagdiv-dialog.component';
import { EntPagdivDeletePopupComponent } from './ent-pagdiv-delete-dialog.component';

export const entPagdivRoute: Routes = [
    {
        path: 'ent-pagdiv',
        component: EntPagdivComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.entPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'ent-pagdiv/:id',
        component: EntPagdivDetailComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.entPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const entPagdivPopupRoute: Routes = [
    {
        path: 'ent-pagdiv-new',
        component: EntPagdivPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.entPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'ent-pagdiv/:id/edit',
        component: EntPagdivPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.entPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'ent-pagdiv/:id/delete',
        component: EntPagdivDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.entPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
