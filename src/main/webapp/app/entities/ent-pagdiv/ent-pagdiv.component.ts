import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { EntPagdiv } from './ent-pagdiv.model';
import { EntPagdivService } from './ent-pagdiv.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { ExcelService } from '../../shared/excel/ExcelService';
import { HttpErrorResponse } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { isNullOrUndefined } from 'util';

declare var $: any;

@Component({
    selector: 'jhi-ent-pagdiv',
    templateUrl: './ent-pagdiv.component.html'
})
export class EntPagdivComponent implements OnInit, OnDestroy {
    entPagdivs: EntPagdiv[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    entPagDivsFlag = false;
    entPagDivsFlagSinData = false;

    constructor(private entPagdivService: EntPagdivService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private excelService: ExcelService, private spinnerService: Ng4LoadingSpinnerService) {
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.entPagDivsFlag = false;
        this.entPagDivsFlagSinData = false;
        this.entPagdivService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.entPagdivs = res.json;
                if (this.entPagdivs.length > 0) {
                    this.entPagDivsFlag = true;
                } else {
                    this.entPagDivsFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadAll() {
        this.entPagdivService.query().subscribe(
            (res: ResponseWrapper) => {
                this.entPagdivs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEntPagdivs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: EntPagdiv) {
        return item.id;
    }

    registerChangeInEntPagdivs() {
        this.eventSubscriber = this.eventManager.subscribe('entPagdivListModification', (response) => this.loadAll());
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    filterByAnooTributario(annoTributaId: any) {
        this.spinnerService.show();
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    generateExcel() {
        const table: HTMLTableElement = <HTMLTableElement> document.getElementById('table');
        const rowCount = !isNullOrUndefined(table) ? table.rows.length : 0;
        if (rowCount > 1) {
            let textos = '[';
            const headers: any [] = [];
            for (let i = 0; i < table.rows.length; i++) {
                if (i > 0) {
                    textos += '{';
                }
                for (let j = 0; j < 6; j++) {
                    if (i === 0) {
                        // Obtenemos la primera fila de la tabla o cabecera y limpiamos tag html y espacios vacíos
                        headers[ j ] = table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim();
                    } else {
                        if (j < 5) {
                            textos += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '",';
                        } else {
                            textos += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '"';
                        }
                    }
                }
                if (i > 0 && i < table.rows.length - 1) {
                    textos += '},';
                }
                if (i === table.rows.length - 1) {
                    textos += '}]';
                }
            }
            this.excelService.exportAsExcelFile(
                JSON.parse(textos.toString()),
                'Datos Pago Dividendo',
                'Datos Pago Dividendo');
        } else {
            this.jhiAlertService.error('No existen registros para exportar');
        }
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

}
