import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { EntPagdiv } from './ent-pagdiv.model';
import { EntPagdivPopupService } from './ent-pagdiv-popup.service';
import { EntPagdivService } from './ent-pagdiv.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ent-pagdiv-dialog',
    templateUrl: './ent-pagdiv-dialog.component.html'
})
export class EntPagdivDialogComponent implements OnInit {

    entPagdiv: EntPagdiv;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    fechaCargaDp: any;
    fePagTmpDp: any;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private entPagdivService: EntPagdivService, private eventManager: JhiEventManager,
                private annoTributaService: AnnoTributaService) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
        .subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.entPagdiv.id !== undefined) {
            this.subscribeToSaveResponse(
                this.entPagdivService.update(this.entPagdiv));
        } else {
            this.subscribeToSaveResponse(
                this.entPagdivService.create(this.entPagdiv));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<EntPagdiv>) {
        result.subscribe((res: EntPagdiv) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: EntPagdiv) {
        this.eventManager.broadcast({ name: 'entPagdivListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-ent-pagdiv-popup',
    template: ''
})
export class EntPagdivPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private entPagdivPopupService: EntPagdivPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.entPagdivPopupService
                .open(EntPagdivDialogComponent as Component, params[ 'id' ]);
            } else {
                this.entPagdivPopupService
                .open(EntPagdivDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
