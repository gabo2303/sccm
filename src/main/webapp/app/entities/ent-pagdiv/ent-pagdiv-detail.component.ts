import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { EntPagdiv } from './ent-pagdiv.model';
import { EntPagdivService } from './ent-pagdiv.service';

@Component({
    selector: 'jhi-ent-pagdiv-detail',
    templateUrl: './ent-pagdiv-detail.component.html'
})
export class EntPagdivDetailComponent implements OnInit, OnDestroy {

    entPagdiv: EntPagdiv;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private entPagdivService: EntPagdivService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInEntPagdivs();
    }

    load(id) {
        this.entPagdivService.find(id).subscribe((entPagdiv) => {
            this.entPagdiv = entPagdiv;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEntPagdivs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'entPagdivListModification',
            (response) => this.load(this.entPagdiv.id)
        );
    }
}
