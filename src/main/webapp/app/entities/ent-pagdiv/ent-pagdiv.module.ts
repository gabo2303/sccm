import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    EntPagdivComponent,
    EntPagdivDeleteDialogComponent,
    EntPagdivDeletePopupComponent,
    EntPagdivDetailComponent,
    EntPagdivDialogComponent,
    EntPagdivPopupComponent,
    entPagdivPopupRoute,
    EntPagdivPopupService,
    entPagdivRoute,
    EntPagdivService,
} from './';

const ENTITY_STATES = [
    ...entPagdivRoute,
    ...entPagdivPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EntPagdivComponent,
        EntPagdivDetailComponent,
        EntPagdivDialogComponent,
        EntPagdivDeleteDialogComponent,
        EntPagdivPopupComponent,
        EntPagdivDeletePopupComponent,
    ],
    entryComponents: [
        EntPagdivComponent,
        EntPagdivDialogComponent,
        EntPagdivPopupComponent,
        EntPagdivDeleteDialogComponent,
        EntPagdivDeletePopupComponent,
    ],
    providers: [
        EntPagdivService,
        EntPagdivPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmEntPagdivModule {
}
