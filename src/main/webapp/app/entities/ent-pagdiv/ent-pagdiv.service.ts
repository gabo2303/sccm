import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { EntPagdiv } from './ent-pagdiv.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class EntPagdivService {

    private resourceUrl = SERVER_API_URL + 'api/ent-pagdivs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    create(entPagdiv: EntPagdiv): Observable<EntPagdiv> {
        const copy = this.convert(entPagdiv);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(entPagdiv: EntPagdiv): Observable<EntPagdiv> {
        const copy = this.convert(entPagdiv);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<EntPagdiv> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map(
            (res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`).map(
            (res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to EntPagdiv.
     */
    private convertItemFromServer(json: any): EntPagdiv {
        const entity: EntPagdiv = Object.assign(new EntPagdiv(), json);
        entity.fechaCarga = this.dateUtils.convertLocalDateFromServer(json.fechaCarga);
        entity.fePagTmp = this.dateUtils.convertLocalDateFromServer(json.fePagTmp);
        return entity;
    }

    /**
     * Convert a EntPagdiv to a JSON which can be sent to the server.
     */
    private convert(entPagdiv: EntPagdiv): EntPagdiv {
        const copy: EntPagdiv = Object.assign({}, entPagdiv);
        copy.fechaCarga = this.dateUtils.convertLocalDateToServer(entPagdiv.fechaCarga);
        copy.fePagTmp = this.dateUtils.convertLocalDateToServer(entPagdiv.fePagTmp);
        return copy;
    }
}
