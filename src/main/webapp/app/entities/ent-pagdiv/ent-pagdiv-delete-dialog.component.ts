import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EntPagdiv } from './ent-pagdiv.model';
import { EntPagdivPopupService } from './ent-pagdiv-popup.service';
import { EntPagdivService } from './ent-pagdiv.service';

@Component({
    selector: 'jhi-ent-pagdiv-delete-dialog',
    templateUrl: './ent-pagdiv-delete-dialog.component.html'
})
export class EntPagdivDeleteDialogComponent {

    entPagdiv: EntPagdiv;

    constructor(private entPagdivService: EntPagdivService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.entPagdivService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'entPagdivListModification',
                content: 'Deleted an entPagdiv'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ent-pagdiv-delete-popup',
    template: ''
})
export class EntPagdivDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private entPagdivPopupService: EntPagdivPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.entPagdivPopupService
            .open(EntPagdivDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
