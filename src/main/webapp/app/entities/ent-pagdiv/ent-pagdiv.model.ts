import { BaseEntity } from './../../shared';

export class EntPagdiv implements BaseEntity {
    constructor(
        public id?: number,
        public idRegistro?: string,
        public fechaCarga?: any,
        public usuarioCarga?: string,
        public regDividendos?: number,
        public rutTmp?: string,
        public nombreTmp?: string,
        public numDivTmp?: number,
        public fePagTmp?: any,
        public accionesTmp?: number,
        public divPesosTmp?: number,
        public orSecTmp?: number,
        public pagDivTmp?: number,
        public filler?: string,
        public okRut?: boolean,
        public okFePag?: boolean,
        public okNuDiv?: boolean,
        public okDivPesos?: boolean,
        public annoTributa?: BaseEntity,
    ) {
        this.okRut = false;
        this.okFePag = false;
        this.okNuDiv = false;
        this.okDivPesos = false;
    }
}
