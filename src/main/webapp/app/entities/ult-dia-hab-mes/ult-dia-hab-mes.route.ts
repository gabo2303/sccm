import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UltDiaHabMesComponent } from './ult-dia-hab-mes.component';
import { UltDiaHabMesDetailComponent } from './ult-dia-hab-mes-detail.component';
import { UltDiaHabMesPopupComponent } from './ult-dia-hab-mes-dialog.component';
import { UltDiaHabMesDeletePopupComponent } from './ult-dia-hab-mes-delete-dialog.component';

export const ultDiaHabMesRoute: Routes = [
    {
        path: 'ult-dia-hab-mes',
        component: UltDiaHabMesComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'], 
            pageTitle: 'sccmApp.ultDiaHabMes.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ult-dia-hab-mes/:id',
        component: UltDiaHabMesDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.ultDiaHabMes.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ultDiaHabMesPopupRoute: Routes = [
    {
        path: 'ult-dia-hab-mes-new',
        component: UltDiaHabMesPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.ultDiaHabMes.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ult-dia-hab-mes/:id/edit',
        component: UltDiaHabMesPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.ultDiaHabMes.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ult-dia-hab-mes/:id/delete',
        component: UltDiaHabMesDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.ultDiaHabMes.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
