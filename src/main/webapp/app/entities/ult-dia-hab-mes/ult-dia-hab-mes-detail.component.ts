import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { UltDiaHabMes } from './ult-dia-hab-mes.model';
import { UltDiaHabMesService } from './ult-dia-hab-mes.service';

@Component({
    selector: 'jhi-ult-dia-hab-mes-detail',
    templateUrl: './ult-dia-hab-mes-detail.component.html'
})
export class UltDiaHabMesDetailComponent implements OnInit, OnDestroy {

    ultDiaHabMes: UltDiaHabMes;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ultDiaHabMesService: UltDiaHabMesService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUltDiaHabMes();
    }

    load(id) {
        this.ultDiaHabMesService.find(id).subscribe((ultDiaHabMes) => {
            this.ultDiaHabMes = ultDiaHabMes;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUltDiaHabMes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ultDiaHabMesListModification',
            (response) => this.load(this.ultDiaHabMes.id)
        );
    }
}
