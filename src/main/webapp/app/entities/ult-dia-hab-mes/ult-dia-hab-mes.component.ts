import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { UltDiaHabMes } from './ult-dia-hab-mes.model';
import { UltDiaHabMesService } from './ult-dia-hab-mes.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ult-dia-hab-mes',
    templateUrl: './ult-dia-hab-mes.component.html'
})
export class UltDiaHabMesComponent implements OnInit, OnDestroy 
{
	ultDiaHabMes: UltDiaHabMes[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private ultDiaHabMesService: UltDiaHabMesService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager,
				private principal: Principal ) { }

    loadAll() 
	{
        this.ultDiaHabMesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.ultDiaHabMes = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
    ngOnInit() 
	{
        this.loadAll();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInUltDiaHabMes();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: UltDiaHabMes) {
        return item.id;
    }
    registerChangeInUltDiaHabMes() {
        this.eventSubscriber = this.eventManager.subscribe('ultDiaHabMesListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
