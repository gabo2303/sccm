import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UltDiaHabMes } from './ult-dia-hab-mes.model';
import { UltDiaHabMesPopupService } from './ult-dia-hab-mes-popup.service';
import { UltDiaHabMesService } from './ult-dia-hab-mes.service';
import { AnnoTributaService } from '../anno-tributa';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ult-dia-hab-mes-dialog',
    templateUrl: './ult-dia-hab-mes-dialog.component.html'
})
export class UltDiaHabMesDialogComponent implements OnInit {

    ultDiaHabMes: UltDiaHabMes;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor( public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private ultDiaHabMesService: UltDiaHabMesService,
				 private annoTributaService: AnnoTributaService, private eventManager: JhiEventManager ) {  }

    ngOnInit() 
	{
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.isSaving = true;
        
		if (this.ultDiaHabMes.id !== undefined) 
		{
            this.subscribeToSaveResponse(this.ultDiaHabMesService.update(this.ultDiaHabMes));
        } 
		else 
		{
            this.subscribeToSaveResponse(this.ultDiaHabMesService.create(this.ultDiaHabMes));
        }
    }

    private subscribeToSaveResponse(result: Observable<UltDiaHabMes>) 
	{
        result.subscribe((res: UltDiaHabMes) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: UltDiaHabMes) 
	{
        this.eventManager.broadcast({ name: 'ultDiaHabMesListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }

    trackAnnoTributaById(index: number, item: AnnoTributa) { return item.id; }
}

@Component({
    selector: 'jhi-ult-dia-hab-mes-popup',
    template: ''
})
export class UltDiaHabMesPopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;
    annosTributarios: AnnoTributa[];
	
    constructor( private route: ActivatedRoute, private ultDiaHabMesPopupService: UltDiaHabMesPopupService, 
				 private annoTributaService: AnnoTributaService, private jhiAlertService: JhiAlertService) {  }

    ngOnInit() 
	{
		console.log("ENTRA ACA");
		this.loadAnnosTributarios();
		
		
		console.log("ANIOS TRIBUTA");
		console.log(this.annosTributarios);
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) { this.ultDiaHabMesPopupService.open(UltDiaHabMesDialogComponent as Component, params['id']); } 
			
			else { this.ultDiaHabMesPopupService.open(UltDiaHabMesDialogComponent as Component); }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
	
	loadAnnosTributarios() 
	{
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => { this.annosTributarios = res.json; console.log(res.json); },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
	private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }
}
