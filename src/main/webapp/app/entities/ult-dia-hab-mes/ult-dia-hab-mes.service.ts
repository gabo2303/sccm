import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { UltDiaHabMes } from './ult-dia-hab-mes.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UltDiaHabMesService 
{
    private resourceUrl = SERVER_API_URL + 'api/ult-dia-hab-mes';

    constructor(private http: Http) { }

    create(ultDiaHabMes: UltDiaHabMes): Observable<UltDiaHabMes> {
        const copy = this.convert(ultDiaHabMes);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ultDiaHabMes: UltDiaHabMes): Observable<UltDiaHabMes> {
        const copy = this.convert(ultDiaHabMes);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<UltDiaHabMes> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UltDiaHabMes.
     */
    private convertItemFromServer(json: any): UltDiaHabMes 
	{
        const entity: UltDiaHabMes = Object.assign(new UltDiaHabMes(), json);
        
		return entity;
    }

    /**
     * Convert a UltDiaHabMes to a JSON which can be sent to the server.
     */
    private convert(ultDiaHabMes: UltDiaHabMes): UltDiaHabMes 
	{
        const copy: UltDiaHabMes = Object.assign({}, ultDiaHabMes);
        
		return copy;
    }
}