import { BaseEntity } from './../../shared';

export class UltDiaHabMes implements BaseEntity {
    constructor(
        public id?: number,
        public mes?: number,
        public dia?: number,
        public annoTributa?: BaseEntity,
    ) {
    }
}
