import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UltDiaHabMes } from './ult-dia-hab-mes.model';
import { UltDiaHabMesService } from './ult-dia-hab-mes.service';

@Injectable()
export class UltDiaHabMesPopupService 
{
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private ultDiaHabMesService: UltDiaHabMesService) 
	{
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> 
	{
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            
			if (isOpen) { resolve(this.ngbModalRef); }

            if (id) 
			{
                this.ultDiaHabMesService.find(id).subscribe((ultDiaHabMes) => {
                    this.ngbModalRef = this.ultDiaHabMesModalRef(component, ultDiaHabMes);
                    resolve(this.ngbModalRef);
                });
            } 
			else 
			{
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.ultDiaHabMesModalRef(component, new UltDiaHabMes());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ultDiaHabMesModalRef(component: Component, ultDiaHabMes: UltDiaHabMes): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ultDiaHabMes = ultDiaHabMes;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
