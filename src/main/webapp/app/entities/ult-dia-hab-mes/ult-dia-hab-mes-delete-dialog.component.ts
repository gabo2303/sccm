import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UltDiaHabMes } from './ult-dia-hab-mes.model';
import { UltDiaHabMesPopupService } from './ult-dia-hab-mes-popup.service';
import { UltDiaHabMesService } from './ult-dia-hab-mes.service';

@Component({
    selector: 'jhi-ult-dia-hab-mes-delete-dialog',
    templateUrl: './ult-dia-hab-mes-delete-dialog.component.html'
})
export class UltDiaHabMesDeleteDialogComponent {

    ultDiaHabMes: UltDiaHabMes;

    constructor(
        private ultDiaHabMesService: UltDiaHabMesService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ultDiaHabMesService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ultDiaHabMesListModification',
                content: 'Deleted an ultDiaHabMes'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ult-dia-hab-mes-delete-popup',
    template: ''
})
export class UltDiaHabMesDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ultDiaHabMesPopupService: UltDiaHabMesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ultDiaHabMesPopupService
                .open(UltDiaHabMesDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
