import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    UltDiaHabMesService,
    UltDiaHabMesPopupService,
    UltDiaHabMesComponent,
    UltDiaHabMesDetailComponent,
    UltDiaHabMesDialogComponent,
    UltDiaHabMesPopupComponent,
    UltDiaHabMesDeletePopupComponent,
    UltDiaHabMesDeleteDialogComponent,
    ultDiaHabMesRoute,
    ultDiaHabMesPopupRoute,
} from './';

const ENTITY_STATES = [
    ...ultDiaHabMesRoute,
    ...ultDiaHabMesPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UltDiaHabMesComponent,
        UltDiaHabMesDetailComponent,
        UltDiaHabMesDialogComponent,
        UltDiaHabMesDeleteDialogComponent,
        UltDiaHabMesPopupComponent,
        UltDiaHabMesDeletePopupComponent,
    ],
    entryComponents: [
        UltDiaHabMesComponent,
        UltDiaHabMesDialogComponent,
        UltDiaHabMesPopupComponent,
        UltDiaHabMesDeleteDialogComponent,
        UltDiaHabMesDeletePopupComponent,
    ],
    providers: [
        UltDiaHabMesService,
        UltDiaHabMesPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmUltDiaHabMesModule {}
