import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PactosCuaCont } from './cuaContPactos.model';
import { ResponseWrapper } from '../../shared';

@Injectable()
export class CuaContPactosService {

    private resourceUrl = SERVER_API_URL + 'api/pactosCC';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`)
        .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Pactos.
     */
    private convertItemFromServer(json: any): PactosCuaCont {
        const entity: PactosCuaCont = Object.assign(new PactosCuaCont(), json);
        return entity;
    }
}
