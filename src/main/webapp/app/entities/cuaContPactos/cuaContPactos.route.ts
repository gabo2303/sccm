import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { CuaContPactosComponent } from './cuaContPactos.component';
import { SCCM_1890_USER_PACTOS } from '../../app.constants';

export const cuaConPactosRoute: Routes = [
    {
        path: 'cuaContPac',
        component: CuaContPactosComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];
