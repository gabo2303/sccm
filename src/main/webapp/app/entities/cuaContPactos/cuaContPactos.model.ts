export class PactosCuaCont {
    constructor(
        public origen?: string,
        public producto?: number,
        public simbolo?: string,
        public intNom?: number,
        public intReal?: number,
        public monFi?: number,
        public annoTributa?: number,
    ) {
    }
}
