import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import { cuaConPactosRoute, CuaContPactosComponent, CuaContPactosService } from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...cuaConPactosRoute
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        CuaContPactosComponent,
    ],
    entryComponents: [
        CuaContPactosComponent,
    ],
    providers: [
        CuaContPactosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmCuaContPactosModule {
}
