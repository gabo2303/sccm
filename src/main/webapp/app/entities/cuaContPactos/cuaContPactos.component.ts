import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { PactosCuaCont } from './cuaContPactos.model';
import { CuaContPactosService } from './cuaContPactos.service';
import { Principal, ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-pactosCC',
    templateUrl: './cuaContPactos.component.html'
})
export class CuaContPactosComponent implements OnInit, OnDestroy {
    pactosCC: PactosCuaCont[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    pactosFlag = false;
    pactosFlagSinData = false;
    rutSelected: number;

    constructor(private pactosCCService: CuaContPactosService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService) {
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPactos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPactos() {
        this.eventSubscriber = this.eventManager.subscribe('pactosCCListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) {
        this.rutSelected = null;
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) {
        console.log('annoId: ' + annoId);
        this.pactosFlag = false;
        this.pactosFlagSinData = false;
        this.spinnerService.show();
        this.pactosCCService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.pactosCC = res.json;
                if (this.pactosCC.length > 0) {
                    this.pactosFlag = true;
                } else {
                    this.pactosFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    filterByRut(rutSelected: any) {
        this.spinnerService.show();
        this.rutSelected = parseInt(rutSelected.value, 10);
        this.spinnerService.hide();
    }

    verifyIsDate(dateToEval: any) {
        return dateToEval.getDate() > 0 || dateToEval.getDate() < 0 || dateToEval.getDate() === 0 ?
            dateToEval.getDate() + '/' + dateToEval.getMonth() + '/' + dateToEval.getFullYear() :
            'Sin fecha válida';
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

}
