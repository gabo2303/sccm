import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Entrada57b } from './entrada-57-b.model';
import { Entrada57bService } from './entrada-57-b.service';

@Injectable()
export class Entrada57bPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private datePipe: DatePipe, private modalService: NgbModal, private router: Router, private entrada57bService: Entrada57bService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.entrada57bService.find(id)
                .subscribe((entrada57bResponse: Entrada57b) => {
                    const entrada57b: Entrada57b = entrada57bResponse;
                    this.ngbModalRef = this.entrada57bModalRef(component, entrada57b);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.entrada57bModalRef(component, new Entrada57b());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    entrada57bModalRef(component: Component, entrada57b: Entrada57b): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.entrada57b = entrada57b;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
