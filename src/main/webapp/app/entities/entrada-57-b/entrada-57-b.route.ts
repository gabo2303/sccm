import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { Entrada57bComponent } from './entrada-57-b.component';
import { Entrada57bDetailComponent } from './entrada-57-b-detail.component';
import { Entrada57bPopupComponent } from './entrada-57-b-dialog.component';
import { Entrada57bDeletePopupComponent } from './entrada-57-b-delete-dialog.component';

export const entrada57bRoute: Routes = [
    {
        path: 'entrada-57-b',
        component: Entrada57bComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.entrada57b.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'entrada-57-b/:id',
        component: Entrada57bDetailComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.entrada57b.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const entrada57bPopupRoute: Routes = [
    {
        path: 'entrada-57-b-new',
        component: Entrada57bPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.entrada57b.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'entrada-57-b/:id/edit',
        component: Entrada57bPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.entrada57b.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'entrada-57-b/:id/delete',
        component: Entrada57bDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.entrada57b.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
