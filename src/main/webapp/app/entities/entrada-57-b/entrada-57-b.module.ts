import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Entrada57bComponent,
    Entrada57bDeleteDialogComponent,
    Entrada57bDeletePopupComponent,
    Entrada57bDetailComponent,
    Entrada57bDialogComponent,
    Entrada57bPopupComponent,
    entrada57bPopupRoute,
    Entrada57bPopupService,
    entrada57bRoute,
    Entrada57bService,
} from './';

const ENTITY_STATES = [
    ...entrada57bRoute,
    ...entrada57bPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Entrada57bComponent,
        Entrada57bDetailComponent,
        Entrada57bDialogComponent,
        Entrada57bDeleteDialogComponent,
        Entrada57bPopupComponent,
        Entrada57bDeletePopupComponent,
    ],
    entryComponents: [
        Entrada57bComponent,
        Entrada57bDialogComponent,
        Entrada57bPopupComponent,
        Entrada57bDeleteDialogComponent,
        Entrada57bDeletePopupComponent,
    ],
    providers: [
        Entrada57bService,
        Entrada57bPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmEntrada57bModule {
}
