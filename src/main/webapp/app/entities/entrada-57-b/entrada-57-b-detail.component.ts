import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Entrada57b } from './entrada-57-b.model';
import { Entrada57bService } from './entrada-57-b.service';
import { ClienteBice } from '../cliente-bice/cliente-bice.model';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';
import { ClienteBiceService } from '../cliente-bice/cliente-bice.service';

@Component({
    selector: 'jhi-entrada-57-b-detail',
    templateUrl: './entrada-57-b-detail.component.html'
})
export class Entrada57bDetailComponent implements OnInit, OnDestroy {

    entrada57b: Entrada57b;
    clientesList: ClienteBice [];
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private entrada57bService: Entrada57bService, private route: ActivatedRoute, private jhiAlertService: JhiAlertService,
                private clientesService: ClienteBiceService) {
    }

    ngOnInit() {
        this.loadAllClientes();
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInEntrada57bs();
    }

    load(id) {
        this.entrada57bService.find(id).subscribe((entrada57bResponse) => {
            this.entrada57b = entrada57bResponse;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEntrada57bs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'entrada57bListModification',
            (response) => this.load(this.entrada57b.id)
        );
    }

    loadAllClientes() {
        this.clientesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientesList = res.json;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

}
