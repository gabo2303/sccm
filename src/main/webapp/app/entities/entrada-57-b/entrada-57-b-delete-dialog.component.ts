import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Entrada57b } from './entrada-57-b.model';
import { Entrada57bPopupService } from './entrada-57-b-popup.service';
import { Entrada57bService } from './entrada-57-b.service';

@Component({
    selector: 'jhi-entrada-57-b-delete-dialog',
    templateUrl: './entrada-57-b-delete-dialog.component.html'
})
export class Entrada57bDeleteDialogComponent {

    entrada57b: Entrada57b;

    constructor(private entrada57bService: Entrada57bService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.entrada57bService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'entrada57bListModification',
                content: 'Deleted an entrada57b'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-entrada-57-b-delete-popup',
    template: ''
})
export class Entrada57bDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private entrada57bPopupService: Entrada57bPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.entrada57bPopupService.open(Entrada57bDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
