import { BaseEntity } from './../../shared';

export class Entrada57b implements BaseEntity {
    constructor(
        public id?: number,
        public folioOp?: string,
        public folioMov?: string,
        public aaOp?: string,
        public mmOp?: string,
        public ddOp?: string,
        public tipoInst?: string,
        public tipoOp?: string,
        public codMoneda?: string,
        public montoHistNom?: string,
        public montoHistPesos?: string,
        public aaVcto?: string,
        public mmVcto?: string,
        public ddVcto?: string,
        public flagRemanente?: string,
        public flagModifLey?: string,
        public fechaDep?: any,
        public montoIntNom?: string,
        public montoReajNom?: string,
        public annoTributa?: BaseEntity,
        public clienteBice?: BaseEntity,
    ) {
    }
}
