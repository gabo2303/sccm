import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Entrada57b } from './entrada-57-b.model';
import { Entrada57bService } from './entrada-57-b.service';
import { Principal } from '../../shared';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { ClienteBiceService } from '../cliente-bice/cliente-bice.service';
import { ClienteBice } from '../cliente-bice/cliente-bice.model';
import { ExcelService } from '../../shared/excel/ExcelService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { isNullOrUndefined } from 'util';

declare const $;

@Component({
    selector: 'jhi-entrada-57-b',
    templateUrl: './entrada-57-b.component.html'
})
export class Entrada57bComponent implements OnInit, OnDestroy {
    entrada57bs: Entrada57b[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    ent57BFlag = false;
    ent57BFlagSinData = false;
    clientesList: ClienteBice [];

    constructor(private entrada57bService: Entrada57bService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private clientesService: ClienteBiceService, private excelService: ExcelService,
                private spinnerService: Ng4LoadingSpinnerService) {

    }

    loadAllByAnnoTributaId(annoId: number) {
        this.ent57BFlag = false;
        this.ent57BFlagSinData = false;
        this.spinnerService.show();
        this.entrada57bService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.entrada57bs = res.json;
                if (this.entrada57bs.length > 0) {
                    this.ent57BFlag = true;
                } else {
                    this.ent57BFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadAllClientes();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEntrada57bs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Entrada57b) {
        return item.id;
    }

    registerChangeInEntrada57bs() {
        this.eventSubscriber = this.eventManager.subscribe('entrada57bListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    filterByAnooTributario(annoTributaId: any) {
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllClientes() {
        this.clientesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientesList = res.json;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    generateExcel() {
        const table: HTMLTableElement = <HTMLTableElement> document.getElementById('table');
        const rowCount = !isNullOrUndefined(table) ? table.rows.length : 0;
        if (rowCount > 1) {
            let textos = '[';
            const headers: any [] = [];
            for (let i = 0; i < table.rows.length; i++) {
                if (i > 0) {
                    textos += '{';
                }
                for (let j = 0; j < 7; j++) {
                    if (i === 0) {
                        // Obtenemos la primera fila de la tabla o cabecera y limpiamos tag html y espacios vacíos
                        headers[ j ] = table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim();
                    } else {
                        if (j < 6) {
                            textos += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '",';
                        } else {
                            textos += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '"';
                        }
                    }
                }
                if (i > 0 && i < table.rows.length - 1) {
                    textos += '},';
                }
                if (i === table.rows.length - 1) {
                    textos += '}]';
                }
            }
            this.excelService.exportAsExcelFile(
                JSON.parse(textos.toString()),
                'Datos Inversiones 57 Bis',
                'Datos Inversiones 57 Bis');
            this.eventManager.broadcast({
                name: 'entrada57bListModification',
                content: 'Deleted an entrada57b'
            });

        } else {
            this.jhiAlertService.error('No existen registros para exportar');
        }
    }

    async reprocessE57B() {
        await this.entrada57bService.reprocessE57B(this.annoSelected).subscribe((event) => {
            if (event.status === 200) {
                return event.status;
            } else {
                this.jhiAlertService.error('Error ejecutando reproceso !.');
                return event.status;
            }
        });

    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}
