import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Entrada57b } from './entrada-57-b.model';
import { Entrada57bPopupService } from './entrada-57-b-popup.service';
import { Entrada57bService } from './entrada-57-b.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ClienteBice, ClienteBiceService } from '../cliente-bice';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-entrada-57-b-dialog',
    templateUrl: './entrada-57-b-dialog.component.html'
})
export class Entrada57bDialogComponent implements OnInit {

    entrada57b: Entrada57b;
    isSaving: boolean;
    pedro = undefined;
    annotributas: AnnoTributa[];

    clientebices: ClienteBice[];

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private entrada57bService: Entrada57bService,
                private annoTributaService: AnnoTributaService, private clienteBiceService: ClienteBiceService, private eventManager: JhiEventManager,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
        .subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
        this.clienteBiceService.query()
        .subscribe((res: ResponseWrapper) => {
            this.clientebices = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save(fechaOperacion, fechaVencimiento, fechaOperacion2, fechaVencimiento2) {
        this.spinnerService.show();
        this.isSaving = true;
        if (this.entrada57b.id !== undefined) {
            // this.entrada57b.clienteBice = clienteBice2.value;
            if (fechaOperacion.value !== undefined) {
                const fechaOperacionSplit = fechaOperacion.value.split('-');
                this.entrada57b.ddOp = (fechaOperacionSplit[ 2 ]);
                this.entrada57b.mmOp = (fechaOperacionSplit[ 1 ]);
                this.entrada57b.aaOp = (fechaOperacionSplit[ 0 ]);
            }
            if (fechaVencimiento.value !== undefined) {
                console.log(fechaVencimiento.value);
                const fechaVencimientoSplit = fechaVencimiento.value.split('-');
                this.entrada57b.ddVcto = fechaVencimientoSplit[ 2 ];
                this.entrada57b.mmVcto = fechaVencimientoSplit[ 1 ];
                this.entrada57b.aaVcto = fechaVencimientoSplit[ 0 ];
            }
            this.subscribeToSaveResponse(
                this.entrada57bService.update(this.entrada57b));
            this.spinnerService.hide();
        } else {
            this.entrada57b.folioMov = '1';
            this.entrada57b.tipoOp = '2';
            this.entrada57b.flagModifLey = 'B';
            this.entrada57b.tipoInst = null;
            this.entrada57b.flagRemanente = null;
            if (fechaOperacion2.value !== '') {
                const fechaOperacion2Split = fechaOperacion2.value.split('-');
                this.entrada57b.ddOp = (fechaOperacion2Split[ 2 ]);
                this.entrada57b.mmOp = (fechaOperacion2Split[ 1 ]);
                this.entrada57b.aaOp = (fechaOperacion2Split[ 0 ]);
            } else {
                this.spinnerService.hide();
                return false;
            }
            if (fechaVencimiento2.value !== '') {
                const fechaVencimientoSplit2 = fechaVencimiento2.value.split('-');
                this.entrada57b.ddVcto = fechaVencimientoSplit2[ 2 ];
                this.entrada57b.mmVcto = fechaVencimientoSplit2[ 1 ];
                this.entrada57b.aaVcto = fechaVencimientoSplit2[ 0 ];
            } else {
                this.spinnerService.hide();
                return false;
            }
            this.subscribeToSaveResponse(this.entrada57bService.create(this.entrada57b));
            this.spinnerService.hide();
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackClienteBiceById(index: number, item: ClienteBice) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<Entrada57b>) {
        result.subscribe((res: Entrada57b) =>
            this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Entrada57b) {
        this.eventManager.broadcast({ name: 'entrada57bListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-entrada-57-b-popup',
    template: ''
})
export class Entrada57bPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private entrada57bPopupService: Entrada57bPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.entrada57bPopupService
                .open(Entrada57bDialogComponent as Component, params[ 'id' ]);
            } else {
                this.entrada57bPopupService
                .open(Entrada57bDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
