import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Entrada57b } from './entrada-57-b.model';
import { createRequestOption } from '../../shared';
import { Http, Response } from '@angular/http';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';

@Injectable()
export class Entrada57bService {

    private resourceUrl = SERVER_API_URL + 'api/entrada-57-bs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    create(entPagdiv: Entrada57b): Observable<Entrada57b> {
        const copy = this.convert(entPagdiv);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(entPagdiv: Entrada57b): Observable<Entrada57b> {
        // const copy = this.convert(entPagdiv);
        return this.http.put(this.resourceUrl, entPagdiv).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Entrada57b> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeSp1InsertaEntrada57B(): Observable<Response> {
        return this.http.get(`${this.resourceUrl}` + '/executeSp1InsertaEntrada57B');
    }

    reprocessE57B(idAnno: number): Observable<Response> {
        return this.http.get(`${this.resourceUrl}/${idAnno}/reprocessE57B`);
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`)
        .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Entrada57b.
     */
    private convertItemFromServer(json: any): Entrada57b {
        const entity: Entrada57b = Object.assign(new Entrada57b(), json);
        entity.fechaDep = this.dateUtils.convertLocalDateFromServer(json.fechaDep);
        return entity;
    }

    /**
     * Convert a Entrada57b to a JSON which can be sent to the server.
     */
    private convert(entrada57b: Entrada57b): Entrada57b {
        const copy: Entrada57b = Object.assign({}, entrada57b);
        copy.fechaDep = this.dateUtils.toDate(entrada57b.fechaDep);
        return copy;
    }

}
