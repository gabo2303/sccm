import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatosEmisionCertificadoService } from './datos-emision-certificado.service';
import { Ahorro, AhorroService } from '../ahorro';

@Injectable()
export class DatosEmiCertPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private ahorroService: AhorroService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.ahorroService.find(id).subscribe((ahorro) => {
                    if (ahorro.fechaPago) {
                        ahorro.fechaPago = {
                            year: ahorro.fechaPago.getFullYear(),
                            month: ahorro.fechaPago.getMonth() + 1,
                            day: ahorro.fechaPago.getDate()
                        };
                    }
                    if (ahorro.fechaInv) {
                        ahorro.fechaInv = {
                            year: ahorro.fechaInv.getFullYear(),
                            month: ahorro.fechaInv.getMonth() + 1,
                            day: ahorro.fechaInv.getDate()
                        };
                    }
                    if (ahorro.fechaApe) {
                        ahorro.fechaApe = {
                            year: ahorro.fechaApe.getFullYear(),
                            month: ahorro.fechaApe.getMonth() + 1,
                            day: ahorro.fechaApe.getDate()
                        };
                    }
                    if (ahorro.fechaCon) {
                        ahorro.fechaCon = {
                            year: ahorro.fechaCon.getFullYear(),
                            month: ahorro.fechaCon.getMonth() + 1,
                            day: ahorro.fechaCon.getDate()
                        };
                    }
                    if (ahorro.fechaVen) {
                        ahorro.fechaVen = {
                            year: ahorro.fechaVen.getFullYear(),
                            month: ahorro.fechaVen.getMonth() + 1,
                            day: ahorro.fechaVen.getDate()
                        };
                    }
                    this.ngbModalRef = this.ahorroModalRef(component, ahorro);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.ahorroModalRef(component, new Ahorro());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ahorroModalRef(component: Component, ahorro: Ahorro): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.ahorro = ahorro;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
