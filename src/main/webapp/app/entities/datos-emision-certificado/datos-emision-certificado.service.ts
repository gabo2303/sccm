import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { createRequestOption, ResponseWrapper } from '../../shared';
import { isNullOrUndefined } from 'util';

@Injectable()
export class DatosEmisionCertificadoService {

    private resourceUrl = SERVER_API_URL + 'api/datos-emi-cert';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findAllByTaxYearIdRutDataTypePaginated(taxYearId: number, dataType: number, rut: number, folio: string, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const rutAndFolio: string = (!isNullOrUndefined(rut) ? `&rut=${rut}` : '') + (!isNullOrUndefined(folio) ? `&folio=${folio}` : '');
        return this.http.get(`${this.resourceUrl}/findAllByTaxYearIdRutDataTypePaginated?taxYearId=${taxYearId}&dataType=${dataType}${rutAndFolio}`, options).map(
            (res: Response) => this.convertResponse(res)
        );
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }
}
