import { Component, OnDestroy, OnInit } from '@angular/core';
import { Ahorro } from '../ahorro/ahorro.model';
import { Subscription } from 'rxjs';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { TipoAlertaService } from '../tipo-alerta';
import { TablaMoneda } from '../tabla-moneda/tabla-moneda.model';
import { Rut_tempService } from '../rut-temp/rut-temp.service';
import {
    CommonServices,
    DATA_TYPE_OPTIONS_OPER_CAPT_EMI_CERT,
    ITEMS_PER_PAGE,
    PATTERN_DATE_SHORT,
    Principal,
    ResponseWrapper,
    SELECT_BLANK_OPTION,
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS,
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM,
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI,
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS
} from '../../shared';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { ExcelService } from '../../shared/excel/ExcelService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosEmisionCertificadoService } from './datos-emision-certificado.service';
import { isNullOrUndefined } from 'util';
import { TablaMonedaService } from '../tabla-moneda/tabla-moneda.service';

@Component({
    selector: 'jhi-datos-emision-certificado',
    templateUrl: './datos-emision-certificado.component.html',
    styles: []
})
export class DatosEmisionCertificadoComponent implements OnInit, OnDestroy 
{
    data: any[];
    paginatedData: any[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    dataFlag = false;
    dataFlagSinData = false;
    rutFilterValue: string = null;
    folioFilterValue: string = null;
    selectedDataType: { code: number, name: string, routerLinkValue: string };
    selectedYear: number;

    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tablaMonedas: TablaMoneda[];
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    SELECT_BLANK_OPTION = SELECT_BLANK_OPTION;
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS = SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS;
    // SELECTED_OPTION_OPER_CAPT_EMI_CERT_AHORRO = SELECTED_OPTION_OPER_CAPT_EMI_CERT_AHORRO;
    // SELECTED_OPTION_OPER_CAPT_EMI_CERT_DAP = SELECTED_OPTION_OPER_CAPT_EMI_CERT_DAP;
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS = SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS;
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI = SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI;
    SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM = SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM;
    DATA_TYPE_OPTIONS_OPER_CAPT_EMI_CERT: { code: number, name: string, routerLinkValue: string }[] = DATA_TYPE_OPTIONS_OPER_CAPT_EMI_CERT;
    otherGroupInteresPagado = false;
    routerLinkVaue: string;
	canUse: boolean;
	flagResult = false;
	resultMsg = '';
    resultType = '';
    classType = '';
	
    constructor(private datosEmisionCertificadoService: DatosEmisionCertificadoService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager,
                private principal: Principal, private excelService: ExcelService, private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService,
                private commonServices: CommonServices, private activatedRoute: ActivatedRoute, private parseLinks: JhiParseLinks, private router: Router,
                private tipoAlertaService: TipoAlertaService, private tablaMonedaService: TablaMonedaService,
				private rutTempService: Rut_tempService) 
	{
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
        this.selectedDataType = this.DATA_TYPE_OPTIONS_OPER_CAPT_EMI_CERT[ 0 ]; // Seleccionando por defecto Pactos como tipo de datos
        this.routerLinkVaue = this.selectedDataType.routerLinkValue;
		console.log(this.routerLinkVaue);
    }

    ngOnInit() 
	{
		this.rutTempService.query().subscribe
		(
			(res: ResponseWrapper) => 
			{
				if(res.json.length != 0) 
				{ 
					this.canUse = false;
					
					this.flagResult = true;
					this.resultMsg = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
					this.resultType = 'Info: ';
					this.classType = 'alert alert-info alert-dismissible';
				}				
				else { this.canUse = true; }
			},
			(res: ResponseWrapper) => {  }
		);
		
        this.loadAnnosTributarios();
        this.loadAllTablaMoneda();
        this.principal.identity().then((account) => { this.currentAccount = account; });
        this.registerChangeInAhorros();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    loadAllTablaMoneda() 
	{
        this.tablaMonedaService.query().subscribe
		(
            (res: ResponseWrapper) => { this.tablaMonedas = res.json; },
			
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAnnosTributarios() 
	{
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.selectedYear = this.annosTributarios[ 0 ].id;
                // this.selectedYear = 5; // TODO: Temporal, borrar esta línea.
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    trackId(index: number, item: Ahorro) {
        return item.id;
    }

    registerChangeInAhorros() {
        this.eventSubscriber = this.eventManager.subscribe('operCapDataEmiCertListModification', (response) => this.filterData());
    }

    filterData() 
	{
        this.spinnerService.show();
        this.page = 1;
        this.dataFlag = false;
        this.dataFlagSinData = false;
        this.loadData();
    }

    loadData() 
	{
        this.spinnerService.show();
        const isRutNullOrEmpty = isNullOrUndefined(this.rutFilterValue) || this.rutFilterValue.length <= 0;
        const isFolioNullOrEmpty = isNullOrUndefined(this.folioFilterValue) || this.folioFilterValue.length <= 0;
		
		if (isRutNullOrEmpty && isFolioNullOrEmpty) 
		{
            const errorMsg = { message: 'error.rutOrFilterEmpty' };
            this.onError(errorMsg);
        } 
		else if (!isRutNullOrEmpty && isNaN(+this.rutFilterValue)) 
		{
            const errorMsg = { message: 'error.rutNotANumber' };
            this.onError(errorMsg);
        } 
		else 
		{
            const rutValue: number = isRutNullOrEmpty ? null : + this.rutFilterValue;
            const folioValue: string = isFolioNullOrEmpty ? null : this.folioFilterValue;
            
			this.datosEmisionCertificadoService.findAllByTaxYearIdRutDataTypePaginated(this.selectedYear, this.selectedDataType.code, rutValue, folioValue, {
                page: this.page - 1,
                size: this.itemsPerPage
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    loadPage(page: number) {
        this.spinnerService.show();
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate([ 'datos-emi-cert' ], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                }
        });
        this.loadPageStatic();
    }

    clear() 
	{
        this.page = 0;
        
		this.router.navigate([ 'datos-emi-cert', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        } ]);
        
		this.loadData();
    }

    convertCodMonToStr(codMon): string 
	{
        codMon = parseInt(codMon, 10);
        
		const find = this.tablaMonedas.find((elem) => { return elem.codMon === codMon; });
		
        if (isNullOrUndefined(find)) 
		{
            console.log('ERROR ' + codMon);
            return '---';
        }
		
        return find.glosa;
    }

    private loadPageStatic() 
	{
        const start = (this.page - 1) * this.itemsPerPage;
        this.data = this.paginatedData.slice(start, start + this.itemsPerPage);
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

    private onSuccess(data, headers) 
	{
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.paginatedData = data;
        
		if (this.paginatedData.length > 0) 
		{
            if (this.SELECTED_OPTION_OPER_CAPT_EMI_CERT_PACTOS === this.selectedDataType.code || this.SELECTED_OPTION_OPER_CAPT_EMI_CERT_BONOS === this.selectedDataType.code ||
                this.SELECTED_OPTION_OPER_CAPT_EMI_CERT_GPI === this.selectedDataType.code || this.SELECTED_OPTION_OPER_CAPT_EMI_CERT_DCV_ASICOM === this.selectedDataType.code) 
			{
                this.otherGroupInteresPagado = true;
            } 
			else { this.otherGroupInteresPagado = false; }
			
            this.routerLinkVaue = this.selectedDataType.routerLinkValue;
            this.data = this.paginatedData.slice(0, this.itemsPerPage);
            this.page = 1;
            this.dataFlag = true;
        } 
		else { this.dataFlagSinData = true; }
		
        this.spinnerService.hide();
    }
}