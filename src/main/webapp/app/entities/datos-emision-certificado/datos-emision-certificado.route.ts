import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { Injectable } from '@angular/core';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';

import { DapResolvePagingParams } from '../d-ap/dap.route';
import { DatosEmisionCertificadoComponent } from './datos-emision-certificado.component';
import { AhorroDeletePopupComponent, AhorroDetailComponent, AhorroPopupComponent } from '../ahorro';
import { SCCM_1890_ADMIN } from '../../app.constants';

@Injectable()
export class DatosEmisionCertificadoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const datosEmisionCertificadoRoute: Routes = [
    {
        path: 'datos-emi-cert',
        component: DatosEmisionCertificadoComponent,
        resolve: {
            'pagingParams': DapResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.datosEmisionCertificado.page.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'datos-emi-cert/:id',
        component: AhorroDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.datosEmisionCertificado.page.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const datosEmisionCertificadoPopupRoute: Routes = [
    {
        path: 'datos-emi-cert-new',
        component: AhorroPopupComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.datosEmisionCertificado.page.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'datos-emi-cert/:id/edit',
        component: AhorroPopupComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.datosEmisionCertificado.page.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'datos-emi-cert/:id/delete',
        component: AhorroDeletePopupComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.datosEmisionCertificado.page.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
