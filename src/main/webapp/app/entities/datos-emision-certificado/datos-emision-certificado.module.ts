import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { DatosEmisionCertificadoComponent } from './datos-emision-certificado.component';
import { DatosEmiCertDetailComponent } from './datos-emi-cert-detail.component';
import { DatosEmiCertDialogComponent } from './datos-emi-cert-dialog.component';
import { DatosEmiCertDeleteDialogComponent } from './datos-emi-cert-delete-dialog.component';
import { SccmSharedModule } from '../../shared';
import { RouterModule } from '@angular/router';
import { AhorroComponent, AhorroDeleteDialogComponent, AhorroDeletePopupComponent, AhorroDialogComponent, AhorroPopupComponent, AhorroPopupService, 
		 AhorroResolvePagingParams, AhorroService } from '../ahorro';
import { PactosComponent, PactosDeleteDialogComponent, PactosDeletePopupComponent, PactosDialogComponent, PactosPopupComponent, PactosPopupService, 
		 PactosService } from '../pactos';		
import { BonosComponent, BonosDeleteDialogComponent, BonosDeletePopupComponent, BonosDialogComponent, BonosPopupComponent, BonosPopupService, 
		 BonosService } from '../bonos';			 
import { datosEmisionCertificadoPopupRoute, datosEmisionCertificadoRoute } from './datos-emision-certificado.route';
import { DatosEmisionCertificadoService } from './datos-emision-certificado.service';
import { DatosEmiCertPopupService } from './datos-emi-cert-popup.service';

const ENTITY_STATES = [
    ...datosEmisionCertificadoRoute,
    ...datosEmisionCertificadoPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DatosEmisionCertificadoComponent,
        DatosEmiCertDetailComponent,
        DatosEmiCertDialogComponent,
        DatosEmiCertDeleteDialogComponent
    ],
    entryComponents: [
        AhorroComponent,
        AhorroDialogComponent,
        AhorroPopupComponent,
        AhorroDeleteDialogComponent,
        AhorroDeletePopupComponent,
		PactosComponent,
        PactosDialogComponent,
        PactosPopupComponent,
        PactosDeleteDialogComponent,
        PactosDeletePopupComponent,
		BonosComponent,
        BonosDialogComponent,
        BonosPopupComponent,
        BonosDeleteDialogComponent,
        BonosDeletePopupComponent
    ],
    providers: [
        DatosEmisionCertificadoService,
        DatosEmiCertPopupService,
        AhorroService,
        AhorroPopupService,
        AhorroResolvePagingParams,
		PactosService,
        PactosPopupService,
		BonosService,
        BonosPopupService
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class DatosEmisionCertificadoModule {
}
