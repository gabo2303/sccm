import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { CabeceraControlCambio } from './cabecera-control-cambio.model';
import { CabeceraControlCambioService } from './cabecera-control-cambio.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({ selector: 'jhi-cabecera-control-cambio', templateUrl: './cabecera-control-cambio.component.html' })
export class CabeceraControlCambioComponent implements OnInit, OnDestroy 
{
	cabeceraControlCambios: CabeceraControlCambio[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor( private cabeceraControlCambioService: CabeceraControlCambioService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager,
				 private principal: Principal ) {  }

    loadAll() 
	{
        this.cabeceraControlCambioService.query([{'estado.equals':'RECHAZADO'}]).subscribe(
            
			(res: ResponseWrapper) =>  
			{
                this.cabeceraControlCambios = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
    ngOnInit() 
	{
        this.loadAll();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInCabeceraControlCambios();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: CabeceraControlCambio) { return item.id; }
	
    registerChangeInCabeceraControlCambios() 
	{ 
		this.eventSubscriber = this.eventManager.subscribe('cabeceraControlCambioListModification', (response) => this.loadAll()); 
	}

    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
}