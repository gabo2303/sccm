import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ResponseWrapper } from '../../shared';

import { CabeceraControlCambio } from './cabecera-control-cambio.model';
import { CabeceraControlCambioPopupService } from './cabecera-control-cambio-popup.service';
import { CabeceraControlCambioService } from './cabecera-control-cambio.service';
import { DetalleControlCambio } from '../detalle-control-cambio/detalle-control-cambio.model';
import { DetalleControlCambioService } from '../detalle-control-cambio/detalle-control-cambio.service';
import { Glosa } from '../glosa/glosa.model';
import { GlosaService } from '../glosa/glosa.service';
import { Representante } from '../representante/representante.model';
import { RepresentanteService } from '../representante/representante.service';
import { Ahorro } from '../ahorro/ahorro.model';
import { AhorroService } from '../ahorro/ahorro.service';
import { Pactos } from '../pactos/pactos.model';
import { PactosService } from '../pactos/pactos.service';
import { DAP } from '../d-ap/dap.model';
import { DAPService } from '../d-ap/dap.service';
import { Bonos } from '../bonos/bonos.model';
import { BonosService } from '../bonos/bonos.service';
import { LHBonos } from '../l-h-bonos/lh-bonos.model';
import { LHBonosService } from '../l-h-bonos/lh-bonos.service';
import { LHDCV } from '../l-hdcv/lhdcv.model';
import { LHDCVService } from '../l-hdcv/lhdcv.service';
import { InstrumentoService } from '../instrumento/instrumento.service';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';

@Component({ selector: 'jhi-cabecera-control-cambio-dialog', templateUrl: './cabecera-control-cambio-dialog.component.html' })
export class CabeceraControlCambioDialogComponent implements OnInit 
{
    cabeceraControlCambio: CabeceraControlCambio;
	detalleControlCambios: DetalleControlCambio[];
	glosa: Glosa;
	representante: Representante;
	ahorro: Ahorro;
	pactos: Pactos;
	dap: DAP;
	bonos: Bonos;
	lhbonos: LHBonos;
	lhdcv: LHDCV;
    isSaving: boolean;
    fechaDp: any;
	glosaInstrumentoLista: boolean;
	glosaAnnoLista: boolean;
	
    constructor( public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private cabeceraControlCambioService: CabeceraControlCambioService,
				 private eventManager: JhiEventManager, private detalleControlCambioService: DetalleControlCambioService, private glosaService: GlosaService,
				 private representanteService: RepresentanteService, private ahorroService: AhorroService, private pactosService: PactosService, 
				 private dapService: DAPService, private instrumentoService: InstrumentoService, private annoTributaService: AnnoTributaService,
				 private bonosService: BonosService, private lhbonosService: LHBonosService, private lhdcvService: LHDCVService ) {  }

    ngOnInit() { this.isSaving = false; }

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.isSaving = true;
		
		if(this.cabeceraControlCambio.nombreTabla == "GLOSA")
		{
			this.glosa = new Glosa();
			
			if(this.cabeceraControlCambio.accion == "CREAR" || this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{		
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "instrumento")
							{
								this.instrumentoService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((instrumento) => 
								{
									this.glosa.instrumento = instrumento;
								});
							}
							else if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.glosa.annoTributa = annoTributa;
								});
							}
							else 
							{
								this.glosa[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);		
				
				if (this.cabeceraControlCambio.accion == "CREAR")
				{
					setTimeout( () => 
					{						
						this.subscribeToSaveResponse( this.glosaService.createGlosa(this.glosa) );
						
						this.activeModal.dismiss(true);
					}, 
					5000);
				}
				else
				{
					this.glosa.id = this.cabeceraControlCambio.idRegistro;
					
					setTimeout( () => 
					{						
						this.subscribeToSaveResponse( this.glosaService.updateGlosa(this.glosa) );
						
						this.activeModal.dismiss(true);
					}, 
					5000);
				}				
			}
			else
			{			
				this.glosaService.deleteGlosa(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an glosa' });
					
					this.activeModal.dismiss(true);
				});
			}
		}
		else if(this.cabeceraControlCambio.nombreTabla == "REPRESENTANTE_LEGAL")
		{
			this.representante = new Representante();
			
			if(this.cabeceraControlCambio.accion == "CREAR" || this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{		
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "instrumento")
							{
								this.instrumentoService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((instrumento) => 
								{
									this.representante.instrumento = instrumento;
								});
							}
							else if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.representante.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].nombreCampo == "firma")
							{
								this.representante.firma = this.detalleControlCambios[i].imagen;
							}
							else 
							{
								this.representante[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);		
				
				if (this.cabeceraControlCambio.accion == "CREAR")
				{
					console.log(this.representante)
					
					setTimeout( () => 
					{						
						this.subscribeToSaveResponse( this.representanteService.createRepresentante(this.representante) );
						
						this.activeModal.dismiss(true);
					}, 
					10000);
				}
				else
				{
					this.representante.id = this.cabeceraControlCambio.idRegistro;
					
					setTimeout( () => 
					{						
						this.subscribeToSaveResponse( this.representanteService.updateRepresentante(this.representante) );
						
						this.activeModal.dismiss(true);
					}, 
					10000);
				}				
			}
			else
			{			
				this.representanteService.deleteRepresentante(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an glosa' });
					
					this.activeModal.dismiss(true);
				});
			}
		}	
		else if( this.cabeceraControlCambio.nombreTabla == "AHORRO" )
		{
			this.ahorro = new Ahorro();
			
			if(this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{	
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.ahorro.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].tipoCampo == "F")
							{
								var fecha = this.detalleControlCambios[i].valorCampo;
								
								this.ahorro[this.detalleControlCambios[i].nombreCampo] = { year: fecha.substring(0, 4), month: fecha.substring(5, 7), day: fecha.substring(8, 10) };
							}
							else 
							{
								this.ahorro[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);	
				
				this.ahorro.id = this.cabeceraControlCambio.idRegistro;
					
				setTimeout( () => 
				{			
					console.log(this.ahorro);
					this.subscribeToSaveResponse( this.ahorroService.updateAhorro(this.ahorro) );
					
					this.activeModal.dismiss(true);
				}, 
				10000);
			}
			else
			{
				this.ahorroService.deleteAhorro(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an ahorro' });
					
					this.activeModal.dismiss(true);
				});
			}				
		}	
		else if( this.cabeceraControlCambio.nombreTabla == "PACTOS" )
		{
			this.pactos = new Pactos();
			
			if(this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{	
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.pactos.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].tipoCampo == "F")
							{
								var fecha = this.detalleControlCambios[i].valorCampo;
								
								this.pactos[this.detalleControlCambios[i].nombreCampo] = { year: fecha.substring(0, 4), month: fecha.substring(5, 7), day: fecha.substring(8, 10) };
							}
							else 
							{
								this.pactos[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);	
				
				this.pactos.id = this.cabeceraControlCambio.idRegistro;
					
				setTimeout( () => 
				{			
					console.log(this.pactos);
					this.subscribeToSaveResponse( this.pactosService.updatePactos(this.pactos) );
					
					this.activeModal.dismiss(true);
				}, 
				10000);
			}
			else
			{
				this.pactosService.deletePactos(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an pacto' });
					
					this.activeModal.dismiss(true);
				});
			}				
		}	
		else if( this.cabeceraControlCambio.nombreTabla == "DAP" )
		{
			this.dap = new DAP();
			
			if(this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{	
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.dap.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].tipoCampo == "F")
							{
								var fecha = this.detalleControlCambios[i].valorCampo;
								
								this.dap[this.detalleControlCambios[i].nombreCampo] = { year: fecha.substring(0, 4), month: fecha.substring(5, 7), day: fecha.substring(8, 10) };
							}
							else 
							{
								this.dap[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);	
				
				//this.dap.interesNominalPesos = 0;
				//this.dap.reajusteNegativo = 0;
				this.dap.id = this.cabeceraControlCambio.idRegistro;
					
				setTimeout( () => 
				{			
					console.log(this.dap);
					this.subscribeToSaveResponse( this.dapService.updateDap(this.dap) );
					
					this.activeModal.dismiss(true);
				}, 
				10000);
			}
			else
			{
				this.dapService.deleteDap(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an pacto' });
					
					this.activeModal.dismiss(true);
				});
			}				
		}		
		else if( this.cabeceraControlCambio.nombreTabla == "BONOS" )
		{
			this.bonos = new Bonos();
			
			if(this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{	
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.bonos.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].tipoCampo == "F")
							{
								var fecha = this.detalleControlCambios[i].valorCampo;
								
								this.bonos[this.detalleControlCambios[i].nombreCampo] = { year: fecha.substring(0, 4), month: fecha.substring(5, 7), day: fecha.substring(8, 10) };
							}
							else 
							{
								this.bonos[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);	
				
				this.bonos.id = this.cabeceraControlCambio.idRegistro;
					
				setTimeout( () => 
				{			
					console.log(this.bonos);
					this.subscribeToSaveResponse( this.bonosService.updateBonos(this.bonos) );
					
					this.activeModal.dismiss(true);
				}, 
				10000);
			}
			else
			{
				this.bonosService.deleteBonos(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an bono' });
					
					this.activeModal.dismiss(true);
				});
			}				
		}	
		else if( this.cabeceraControlCambio.nombreTabla == "LHBONOS" )
		{
			this.lhbonos = new LHBonos();
			
			if(this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{	
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.lhbonos.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].tipoCampo == "F")
							{
								var fecha = this.detalleControlCambios[i].valorCampo;
								
								this.lhbonos[this.detalleControlCambios[i].nombreCampo] = { year: fecha.substring(0, 4), month: fecha.substring(5, 7), day: fecha.substring(8, 10) };
							}
							else 
							{
								this.lhbonos[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);	
				
				this.lhbonos.id = this.cabeceraControlCambio.idRegistro;
					
				setTimeout( () => 
				{			
					console.log(this.lhbonos);
					this.subscribeToSaveResponse( this.lhbonosService.updateLHBonos(this.lhbonos) );
					
					this.activeModal.dismiss(true);
				}, 
				10000);
			}
			else
			{
				this.lhbonosService.deleteLHBonos(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an bono' });
					
					this.activeModal.dismiss(true);
				});
			}				
		}	
		else if( this.cabeceraControlCambio.nombreTabla == "LHDCV" )
		{
			this.lhdcv = new LHDCV();
			
			if(this.cabeceraControlCambio.accion == "ACTUALIZAR")
			{	
				this.detalleControlCambioService.query(this.cabeceraControlCambio.id).subscribe
				(
					(res: ResponseWrapper) => 
					{
						this.detalleControlCambios = res.json;
						
						for ( let i = 0; i < this.detalleControlCambios.length; i++)
						{
							if(this.detalleControlCambios[i].nombreCampo == "annoTributa")
							{
								this.annoTributaService.find(parseInt(this.detalleControlCambios[i].valorCampo)).subscribe((annoTributa) => 
								{
									this.lhdcv.annoTributa = annoTributa;
								});
							}
							else if(this.detalleControlCambios[i].tipoCampo == "F")
							{
								var fecha = this.detalleControlCambios[i].valorCampo;
								
								this.lhdcv[this.detalleControlCambios[i].nombreCampo] = { year: fecha.substring(0, 4), month: fecha.substring(5, 7), day: fecha.substring(8, 10) };
							}
							else 
							{
								this.lhdcv[this.detalleControlCambios[i].nombreCampo] = this.detalleControlCambios[i].valorCampo;
							}
						}			
					},
					(res: ResponseWrapper) => this.onError(res.json)
				);	
				
				this.lhdcv.id = this.cabeceraControlCambio.idRegistro;
					
				setTimeout( () => 
				{			
					console.log(this.lhdcv);
					this.subscribeToSaveResponse( this.lhdcvService.updateLHDCV(this.lhdcv) );
					
					this.activeModal.dismiss(true);
				}, 
				10000);
			}
			else
			{
				this.lhdcvService.deleteLHDCV(this.cabeceraControlCambio.idRegistro).subscribe( (response) => 
				{
					this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'Deleted an bono' });
					
					this.activeModal.dismiss(true);
				});
			}				
		}			
			
		this.cabeceraControlCambio.estado = 'APROBADO';
	
		this.subscribeToSaveResponse(this.cabeceraControlCambioService.update(this.cabeceraControlCambio));
    }

    private subscribeToSaveResponse(result: Observable<CabeceraControlCambio>) 
	{
        result.subscribe((res: CabeceraControlCambio) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CabeceraControlCambio) 
	{
        this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }
}

@Component({ selector: 'jhi-cabecera-control-cambio-popup', template: '' })
export class CabeceraControlCambioPopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;

    constructor( private route: ActivatedRoute, private cabeceraControlCambioPopupService: CabeceraControlCambioPopupService ) {}

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => 
		{
            if ( params['id'] ) 
			{
                this.cabeceraControlCambioPopupService.open(CabeceraControlCambioDialogComponent as Component, params['id']);
            } 
			else { this.cabeceraControlCambioPopupService.open(CabeceraControlCambioDialogComponent as Component); }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}