import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CabeceraControlCambio } from './cabecera-control-cambio.model';
import { CabeceraControlCambioPopupService } from './cabecera-control-cambio-popup.service';
import { CabeceraControlCambioService } from './cabecera-control-cambio.service';

@Component({ selector: 'jhi-cabecera-control-cambio-delete-dialog', templateUrl: './cabecera-control-cambio-delete-dialog.component.html' })
export class CabeceraControlCambioDeleteDialogComponent 
{
    cabeceraControlCambio: CabeceraControlCambio;

    constructor( private cabeceraControlCambioService: CabeceraControlCambioService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager ) { }

    clear() { this.activeModal.dismiss('cancel'); }

    confirmDelete(id: number) 
	{
        /*this.cabeceraControlCambioService.delete(id).subscribe((response) => 
		{
            this.eventManager.broadcast({
                name: 'cabeceraControlCambioListModification',
                content: 'Deleted an cabeceraControlCambio'
            });
            this.activeModal.dismiss(true);
        });*/
		
		this.cabeceraControlCambio.estado = 'RECHAZADO';
		
		this.subscribeToSaveResponse(this.cabeceraControlCambioService.update(this.cabeceraControlCambio));		
		
		this.activeModal.dismiss(true);
    }
	
	private subscribeToSaveResponse(result: Observable<CabeceraControlCambio>) 
	{
        result.subscribe(
			(res: CabeceraControlCambio) => this.onSaveSuccess(res), 
			(res: Response) => this.onSaveError()
		);
    }
	
	private onSaveSuccess(result: CabeceraControlCambio) 
	{
        this.eventManager.broadcast({ name: 'cabeceraControlCambioListModification', content: 'OK'});    
    }
	
	private onSaveError() { }
}

@Component({
    selector: 'jhi-cabecera-control-cambio-delete-popup',
    template: ''
})
export class CabeceraControlCambioDeletePopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;

    constructor( private route: ActivatedRoute, private cabeceraControlCambioPopupService: CabeceraControlCambioPopupService ) { }

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => 
		{
            this.cabeceraControlCambioPopupService.open(CabeceraControlCambioDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}