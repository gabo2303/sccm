import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CabeceraControlCambio } from './cabecera-control-cambio.model';
import { CabeceraControlCambioService } from './cabecera-control-cambio.service';

@Injectable()
export class CabeceraControlCambioPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private cabeceraControlCambioService: CabeceraControlCambioService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cabeceraControlCambioService.find(id).subscribe((cabeceraControlCambio) => {
                    if (cabeceraControlCambio.fecha) {
                        cabeceraControlCambio.fecha = {
                            year: cabeceraControlCambio.fecha.getFullYear(),
                            month: cabeceraControlCambio.fecha.getMonth() + 1,
                            day: cabeceraControlCambio.fecha.getDate()
                        };
                    }
                    this.ngbModalRef = this.cabeceraControlCambioModalRef(component, cabeceraControlCambio);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cabeceraControlCambioModalRef(component, new CabeceraControlCambio());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cabeceraControlCambioModalRef(component: Component, cabeceraControlCambio: CabeceraControlCambio): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.cabeceraControlCambio = cabeceraControlCambio;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
