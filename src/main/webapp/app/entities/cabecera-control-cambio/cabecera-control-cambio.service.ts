import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CabeceraControlCambio } from './cabecera-control-cambio.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CabeceraControlCambioService {

    private resourceUrl = SERVER_API_URL + 'api/cabecera-control-cambios';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(cabeceraControlCambio: CabeceraControlCambio): Observable<CabeceraControlCambio> {
        const copy = this.convert(cabeceraControlCambio);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(cabeceraControlCambio: CabeceraControlCambio): Observable<CabeceraControlCambio> {
        const copy = this.convert(cabeceraControlCambio);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CabeceraControlCambio> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
				
        return this.http.get(this.resourceUrl, { params: { "estado.equals": "PENDIENTE" } }).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CabeceraControlCambio.
     */
    private convertItemFromServer(json: any): CabeceraControlCambio {
        const entity: CabeceraControlCambio = Object.assign(new CabeceraControlCambio(), json);
        entity.fecha = this.dateUtils
            .convertLocalDateFromServer(json.fecha);
        return entity;
    }

    /**
     * Convert a CabeceraControlCambio to a JSON which can be sent to the server.
     */
    private convert(cabeceraControlCambio: CabeceraControlCambio): CabeceraControlCambio {
        const copy: CabeceraControlCambio = Object.assign({}, cabeceraControlCambio);
        copy.fecha = this.dateUtils
            .convertLocalDateToServer(cabeceraControlCambio.fecha);
        return copy;
    }
}
