import { BaseEntity } from './../../shared';

export class CabeceraControlCambio implements BaseEntity {
    constructor(
        public id?: number,
        public estado?: string,
        public nombreTabla?: string,
        public accion?: string,
        public usuario?: string,
        public fecha?: any,
        public idRegistro?: number,
    ) {
    }
}
