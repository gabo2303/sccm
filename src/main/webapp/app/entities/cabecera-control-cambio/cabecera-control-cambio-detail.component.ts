import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CabeceraControlCambio } from './cabecera-control-cambio.model';
import { CabeceraControlCambioService } from './cabecera-control-cambio.service';
import { DetalleControlCambio } from '../detalle-control-cambio/detalle-control-cambio.model';
import { DetalleControlCambioService } from '../detalle-control-cambio/detalle-control-cambio.service';

import { ResponseWrapper } from '../../shared';

@Component({ selector: 'jhi-cabecera-control-cambio-detail', templateUrl: './cabecera-control-cambio-detail.component.html' })
export class CabeceraControlCambioDetailComponent implements OnInit, OnDestroy 
{
    cabeceraControlCambio: CabeceraControlCambio;
	detalleControlCambios: DetalleControlCambio[];
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor( private eventManager: JhiEventManager, private cabeceraControlCambioService: CabeceraControlCambioService, private jhiAlertService: JhiAlertService,
				 private detalleControlCambioService: DetalleControlCambioService, private route: ActivatedRoute ) { }

    ngOnInit() 
	{
        this.subscription = this.route.params.subscribe((params) => { this.load(params['id']); });
		
        this.registerChangeInCabeceraControlCambios();
    }

    load(id) 
	{
        this.cabeceraControlCambioService.find(id).subscribe((cabeceraControlCambio) => { this.cabeceraControlCambio = cabeceraControlCambio; });
		
		this.detalleControlCambioService.query(id).subscribe(
            (res: ResponseWrapper) => 
			{
                this.detalleControlCambios = res.json;
				console.log('json: ', res.json);
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
    previousState() { window.history.back(); }

    ngOnDestroy() 
	{		
        this.subscription.unsubscribe();
        
		this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCabeceraControlCambios() 
	{
        this.eventSubscriber = this.eventManager.subscribe( 'cabeceraControlCambioListModification', (response) => this.load(this.cabeceraControlCambio.id) );
    }
	
	private onError(error) { this.jhiAlertService.error(error.message, null, null); }
}