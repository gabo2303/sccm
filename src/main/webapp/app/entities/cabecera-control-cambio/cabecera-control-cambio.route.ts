import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CabeceraControlCambioComponent } from './cabecera-control-cambio.component';
import { CabeceraControlCambioDetailComponent } from './cabecera-control-cambio-detail.component';
import { CabeceraControlCambioPopupComponent } from './cabecera-control-cambio-dialog.component';
import { CabeceraControlCambioDeletePopupComponent } from './cabecera-control-cambio-delete-dialog.component';

export const cabeceraControlCambioRoute: Routes = 
[
    {
        path: 'cabecera-control-cambio',
        component: CabeceraControlCambioComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cabeceraControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, 
	{
        path: 'cabecera-control-cambio/:id',
        component: CabeceraControlCambioDetailComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cabeceraControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cabeceraControlCambioPopupRoute: Routes = 
[
    {
        path: 'cabecera-control-cambio-new',
        component: CabeceraControlCambioPopupComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cabeceraControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cabecera-control-cambio/:id/edit',
        component: CabeceraControlCambioPopupComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cabeceraControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cabecera-control-cambio/:id/delete',
        component: CabeceraControlCambioDeletePopupComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cabeceraControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];