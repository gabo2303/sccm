import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    CabeceraControlCambioService,
    CabeceraControlCambioPopupService,
    CabeceraControlCambioComponent,
    CabeceraControlCambioDetailComponent,
    CabeceraControlCambioDialogComponent,
    CabeceraControlCambioPopupComponent,
    CabeceraControlCambioDeletePopupComponent,
    CabeceraControlCambioDeleteDialogComponent,
    cabeceraControlCambioRoute,
    cabeceraControlCambioPopupRoute,
} from './';

const ENTITY_STATES = [
    ...cabeceraControlCambioRoute,
    ...cabeceraControlCambioPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CabeceraControlCambioComponent,
        CabeceraControlCambioDetailComponent,
        CabeceraControlCambioDialogComponent,
        CabeceraControlCambioDeleteDialogComponent,
        CabeceraControlCambioPopupComponent,
        CabeceraControlCambioDeletePopupComponent,
    ],
    entryComponents: [
        CabeceraControlCambioComponent,
        CabeceraControlCambioDialogComponent,
        CabeceraControlCambioPopupComponent,
        CabeceraControlCambioDeleteDialogComponent,
        CabeceraControlCambioDeletePopupComponent,
    ],
    providers: [
        CabeceraControlCambioService,
        CabeceraControlCambioPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmCabeceraControlCambioModule {}
