import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import { CertificadosAnterioresService, CertificadosAnterioresComponent, certificadosAnterioresRoute, } from './';

const ENTITY_STATES = [ ...certificadosAnterioresRoute, ];

@NgModule({
    imports: 
	[
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [ CertificadosAnterioresComponent, ],
    entryComponents: [ CertificadosAnterioresComponent, ],
    providers: [ CertificadosAnterioresService, ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class SccmCertificadosAnterioresModule { }