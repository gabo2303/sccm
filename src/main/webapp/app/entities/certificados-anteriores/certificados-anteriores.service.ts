import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { CertificadosAnteriores } from './certificados-anteriores.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CertificadosAnterioresService 
{
    private resourceUrl = SERVER_API_URL + 'api/certificados-anteriores';

    constructor(private http: Http) { }

    query(ruta: String, archivo: String, req?: any): Observable<ResponseWrapper> 
	{
		console.log(ruta);
		console.log(archivo);
        const options = createRequestOption(req);
		options.responseType = ResponseContentType.ArrayBuffer;       	
		
		return this.http.get(`${this.resourceUrl}` + "?ruta=" + ruta + "&archivo=" + archivo, options).map(data => data);
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();	
		
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(jsonResponse[i]); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CertificadosAnteriores.
     */
    private convertItemFromServer(json: any): CertificadosAnteriores {
        const entity: CertificadosAnteriores = Object.assign(new CertificadosAnteriores(), json);
        return entity;
    }

    /**
     * Convert a CertificadosAnteriores to a JSON which can be sent to the server.
     */
    private convert(certificadosAnteriores: CertificadosAnteriores): CertificadosAnteriores {
        const copy: CertificadosAnteriores = Object.assign({}, certificadosAnteriores);
        return copy;
    }
}
