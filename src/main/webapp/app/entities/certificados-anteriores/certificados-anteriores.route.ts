import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CertificadosAnterioresComponent } from './certificados-anteriores.component';

export const certificadosAnterioresRoute: Routes =
[
    {
        path: 'certificados-anteriores',
        component: CertificadosAnterioresComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.certificadosAnteriores.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];