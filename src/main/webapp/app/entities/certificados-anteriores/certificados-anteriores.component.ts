import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiDataUtils, JhiAlertService } from 'ng-jhipster';

import { CertificadosAnteriores } from './certificados-anteriores.model';
import { CertificadosAnterioresService } from './certificados-anteriores.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ProductoService } from '../producto/producto.service';
import { Producto } from '../producto/producto.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { isNullOrUndefined } from 'util';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({ selector: 'jhi-certificados-anteriores', templateUrl: './certificados-anteriores.component.html' })

export class CertificadosAnterioresComponent implements OnInit
{
	productos: Producto[];
	annoTributas: AnnoTributa[];
	certificadosAnteriores: CertificadosAnteriores[];
    currentAccount: any;
    eventSubscriber: Subscription;
	isRutValid = false;
	rutFilterValue: string = null;
	annoSeleccionado: string;
	productoSeleccionado: string;
	certificado: any;
	
    constructor( private certificadosAnterioresService: CertificadosAnterioresService, private jhiAlertService: JhiAlertService,
				 private eventManager: JhiEventManager, private principal: Principal, private annoTributaService: AnnoTributaService,
				 private productoService: ProductoService, private spinnerService: Ng4LoadingSpinnerService, 
				 private dataUtils: JhiDataUtils ) { }

    loadAll() 
	{
		this.annoTributaService.query2().subscribe(
            (res: ResponseWrapper) => 
			{ 
				console.log('SE CARGA LA WEA'); 
				this.annoTributas = res.json; 
				console.log(res); 
				console.log(res.json); 
			},
            (res: ResponseWrapper) => this.onError(res.json)
        );
		
		this.productoService.query().subscribe(
            (res: ResponseWrapper) => { this.productos = res.json; },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
    ngOnInit()
	{
        this.loadAll();
		
        this.principal.identity().then((account) => { this.currentAccount = account; });		
    }
	
	trackId(index: number, item: CertificadosAnteriores) { return item.id; }
    
	buscar()
	{
		//this.spinnerService.show();
		
		/*if (!this.isRutValid) 
		{
            this.jhiAlertService.error("sccmApp.certificadosAnteriores.rutInvalido");
			this.spinnerService.hide();
        } 
		else */if (isNullOrUndefined(this.annoSeleccionado))
		{
			this.jhiAlertService.error("sccmApp.certificadosAnteriores.seleccioneAnno");
			this.spinnerService.hide();
		}
		else if (isNullOrUndefined(this.productoSeleccionado))
		{
			this.jhiAlertService.error("sccmApp.certificadosAnteriores.seleccioneInstrumento");
			this.spinnerService.hide();
		}
		else 
		{
			var carpeta = null;
			
			if(this.productoSeleccionado == "Dividendos") { carpeta = "Pago_Dividendos" } 
			
			else if (this.productoSeleccionado == "Captaciones") { carpeta = "Operaciones_Captacion" } 
			
			else if (this.productoSeleccionado == "57 Bis") { carpeta = "57Bis" }
			
			let ruta = "/paso/" + carpeta + "/AT_" + this.annoSeleccionado + "/Certificados";
			let archivo = this.rutFilterValue; //+ "_F" + instrumento + "_" + this.annoSeleccionado;
			
			this.certificadosAnterioresService.query(ruta, archivo).subscribe
			(
				(res) => 
				{ 
					let file = new Blob([res['_body']], { type: 'application/pdf' });     
						
					var fileURL = URL.createObjectURL(file);
					window.open(fileURL);
					
					/*var descargar = document.createElement("a");
					descargar.download = "certificado.pdf";
					descargar.href = fileURL;
					descargar.click();*/
				},
				(res: ResponseWrapper) => this.onError(res)
			);
        }
	}
	
	valueChange($event) 
	{
        this.isRutValid = !isNullOrUndefined(this.rutFilterValue) && this.rutFilterValue.length > 7 && !isNaN(Number(this.rutFilterValue));
    }
	
    private onError(error) { this.jhiAlertService.error("sccmApp.certificadosAnteriores.sinCertificado", null, null); }
}
