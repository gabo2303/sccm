import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClienteBice } from './cliente-bice.model';
import { ClienteBicePopupService } from './cliente-bice-popup.service';
import { ClienteBiceService } from './cliente-bice.service';

@Component({
    selector: 'jhi-cliente-bice-dialog',
    templateUrl: './cliente-bice-dialog.component.html'
})
export class ClienteBiceDialogComponent implements OnInit {

    clienteBice: ClienteBice;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clienteBiceService: ClienteBiceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clienteBice.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clienteBiceService.update(this.clienteBice));
        } else {
            this.subscribeToSaveResponse(
                this.clienteBiceService.create(this.clienteBice));
        }
    }

    private subscribeToSaveResponse(result: Observable<ClienteBice>) {
        result.subscribe((res: ClienteBice) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ClienteBice) {
        this.eventManager.broadcast({ name: 'clienteBiceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-cliente-bice-popup',
    template: ''
})
export class ClienteBicePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clienteBicePopupService: ClienteBicePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clienteBicePopupService
                    .open(ClienteBiceDialogComponent as Component, params['id']);
            } else {
                this.clienteBicePopupService
                    .open(ClienteBiceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
