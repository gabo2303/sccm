export * from './cliente-bice.model';
export * from './cliente-bice-popup.service';
export * from './cliente-bice.service';
export * from './cliente-bice-dialog.component';
export * from './cliente-bice-delete-dialog.component';
export * from './cliente-bice-detail.component';
export * from './cliente-bice.component';
export * from './cliente-bice.route';
