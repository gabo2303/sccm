import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ClienteBice } from './cliente-bice.model';
import { ClienteBiceService } from './cliente-bice.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cliente-bice',
    templateUrl: './cliente-bice.component.html'
})
export class ClienteBiceComponent implements OnInit, OnDestroy {
clienteBices: ClienteBice[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private clienteBiceService: ClienteBiceService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.clienteBiceService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clienteBices = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClienteBices();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ClienteBice) {
        return item.id;
    }
    registerChangeInClienteBices() {
        this.eventSubscriber = this.eventManager.subscribe('clienteBiceListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
