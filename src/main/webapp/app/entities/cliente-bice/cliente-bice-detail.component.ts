import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ClienteBice } from './cliente-bice.model';
import { ClienteBiceService } from './cliente-bice.service';

@Component({
    selector: 'jhi-cliente-bice-detail',
    templateUrl: './cliente-bice-detail.component.html'
})
export class ClienteBiceDetailComponent implements OnInit, OnDestroy {

    clienteBice: ClienteBice;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clienteBiceService: ClienteBiceService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClienteBices();
    }

    load(id) {
        this.clienteBiceService.find(id).subscribe((clienteBice) => {
            this.clienteBice = clienteBice;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClienteBices() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clienteBiceListModification',
            (response) => this.load(this.clienteBice.id)
        );
    }
}
