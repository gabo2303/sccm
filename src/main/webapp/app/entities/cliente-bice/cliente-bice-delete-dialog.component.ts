import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClienteBice } from './cliente-bice.model';
import { ClienteBicePopupService } from './cliente-bice-popup.service';
import { ClienteBiceService } from './cliente-bice.service';

@Component({
    selector: 'jhi-cliente-bice-delete-dialog',
    templateUrl: './cliente-bice-delete-dialog.component.html'
})
export class ClienteBiceDeleteDialogComponent {

    clienteBice: ClienteBice;

    constructor(
        private clienteBiceService: ClienteBiceService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clienteBiceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clienteBiceListModification',
                content: 'Deleted an clienteBice'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cliente-bice-delete-popup',
    template: ''
})
export class ClienteBiceDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clienteBicePopupService: ClienteBicePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clienteBicePopupService
                .open(ClienteBiceDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
