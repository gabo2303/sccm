import { BaseEntity } from './../../shared';

export class ClienteBice implements BaseEntity {
    constructor(
        public id?: number,
        public rut?: string,
        public nombre?: string,
        public direccion?: string,
        public emiCart1S?: BaseEntity[],
        public emiCert1S?: BaseEntity[],
        public ent57b2S?: BaseEntity[],
        public sIni2S?: BaseEntity[],
        public saldos2S?: BaseEntity[],
    ) {
    }
}
