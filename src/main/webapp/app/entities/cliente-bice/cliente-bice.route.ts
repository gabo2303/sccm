import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { ClienteBiceComponent } from './cliente-bice.component';
import { ClienteBiceDetailComponent } from './cliente-bice-detail.component';
import { ClienteBicePopupComponent } from './cliente-bice-dialog.component';
import { ClienteBiceDeletePopupComponent } from './cliente-bice-delete-dialog.component';

export const clienteBiceRoute: Routes = [
    {
        path: 'cliente-bice',
        component: ClienteBiceComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.clienteBice.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'cliente-bice/:id',
        component: ClienteBiceDetailComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.clienteBice.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const clienteBicePopupRoute: Routes = [
    {
        path: 'cliente-bice-new',
        component: ClienteBicePopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.clienteBice.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cliente-bice/:id/edit',
        component: ClienteBicePopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.clienteBice.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cliente-bice/:id/delete',
        component: ClienteBiceDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.clienteBice.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
