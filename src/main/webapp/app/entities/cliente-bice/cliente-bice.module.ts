import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    ClienteBiceService,
    ClienteBicePopupService,
    ClienteBiceComponent,
    ClienteBiceDetailComponent,
    ClienteBiceDialogComponent,
    ClienteBicePopupComponent,
    ClienteBiceDeletePopupComponent,
    ClienteBiceDeleteDialogComponent,
    clienteBiceRoute,
    clienteBicePopupRoute,
} from './';

const ENTITY_STATES = [
    ...clienteBiceRoute,
    ...clienteBicePopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ClienteBiceComponent,
        ClienteBiceDetailComponent,
        ClienteBiceDialogComponent,
        ClienteBiceDeleteDialogComponent,
        ClienteBicePopupComponent,
        ClienteBiceDeletePopupComponent,
    ],
    entryComponents: [
        ClienteBiceComponent,
        ClienteBiceDialogComponent,
        ClienteBicePopupComponent,
        ClienteBiceDeleteDialogComponent,
        ClienteBiceDeletePopupComponent,
    ],
    providers: [
        ClienteBiceService,
        ClienteBicePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmClienteBiceModule {}
