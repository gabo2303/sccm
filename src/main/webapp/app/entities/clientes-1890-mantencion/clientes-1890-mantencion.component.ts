import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Clientes1890mantencion } from './clientes-1890-mantencion.model';
import { Clientes1890mantencionService } from './clientes-1890-mantencion.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-clientes-1890-mantencion',
    templateUrl: './clientes-1890-mantencion.component.html'
})
export class Clientes1890mantencionComponent implements OnInit, OnDestroy {
clientes1890mantencions: Clientes1890mantencion[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private clientes1890mantencionService: Clientes1890mantencionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.clientes1890mantencionService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientes1890mantencions = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClientes1890mantencions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientes1890mantencions() {
        this.eventSubscriber = this.eventManager.subscribe('clientes1890mantencionListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
