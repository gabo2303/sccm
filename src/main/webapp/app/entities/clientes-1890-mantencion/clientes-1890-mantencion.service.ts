import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Clientes1890mantencion } from './clientes-1890-mantencion.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class Clientes1890mantencionService {

    private resourceUrl = SERVER_API_URL + 'api/clientes-1890-mantencions';

    constructor(private http: Http) { }

    create(clientes1890mantencion: Clientes1890mantencion): Observable<Clientes1890mantencion> {
        const copy = this.convert(clientes1890mantencion);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(clientes1890mantencion: Clientes1890mantencion): Observable<Clientes1890mantencion> {
        const copy = this.convert(clientes1890mantencion);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Clientes1890mantencion> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Clientes1890mantencion.
     */
    private convertItemFromServer(json: any): Clientes1890mantencion {
        const entity: Clientes1890mantencion = Object.assign(new Clientes1890mantencion(), json);
        return entity;
    }

    /**
     * Convert a Clientes1890mantencion to a JSON which can be sent to the server.
     */
    private convert(clientes1890mantencion: Clientes1890mantencion): Clientes1890mantencion {
        const copy: Clientes1890mantencion = Object.assign({}, clientes1890mantencion);
        return copy;
    }
}
