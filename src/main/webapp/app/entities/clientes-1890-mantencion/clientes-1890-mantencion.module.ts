import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Clientes1890mantencionService,
    Clientes1890mantencionComponent,
    clientes1890mantencionRoute,
} from './';

const ENTITY_STATES = [
    ...clientes1890mantencionRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Clientes1890mantencionComponent,
    ],
    entryComponents: [
        Clientes1890mantencionComponent,
    ],
    providers: [
        Clientes1890mantencionService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmClientes1890mantencionModule {}
