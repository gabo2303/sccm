import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Clientes1890mantencionComponent } from './clientes-1890-mantencion.component';

export const clientes1890mantencionRoute: Routes = [
    {
        path: 'clientes-1890-mantencion',
        component: Clientes1890mantencionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.clientes1890mantencion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
