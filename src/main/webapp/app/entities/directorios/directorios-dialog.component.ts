import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Directorios } from './directorios.model';
import { DirectoriosPopupService } from './directorios-popup.service';
import { DirectoriosService } from './directorios.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { Producto, ProductoService } from '../producto';
import { Instrumento, InstrumentoService } from '../instrumento';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-directorios-dialog',
    templateUrl: './directorios-dialog.component.html'
})
export class DirectoriosDialogComponent implements OnInit 
{
    directorios: Directorios;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    productos: Producto[];

    instrumentos: Instrumento[];
	
	descripciones : String[];

    constructor( public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private directoriosService: DirectoriosService,
				 private annoTributaService: AnnoTributaService, private productoService: ProductoService, private instrumentoService: InstrumentoService,
				 private eventManager: JhiEventManager ) {  }

    ngOnInit() 
	{
        this.isSaving = false;
        
		this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        
		this.productoService.query()
            .subscribe((res: ResponseWrapper) => { this.productos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        
		this.instrumentoService.query()
            .subscribe((res: ResponseWrapper) => { this.instrumentos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
			
		this.descripciones = ['Certificados', 'DJ', 'Rectificatoria'];
    }

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.isSaving = true;
        
		if (this.directorios.id !== undefined) 
		{
            this.subscribeToSaveResponse(this.directoriosService.update(this.directorios));
        } 
		else { this.subscribeToSaveResponse(this.directoriosService.create(this.directorios)); }
    }

    private subscribeToSaveResponse(result: Observable<Directorios>) 
	{
        result.subscribe((res: Directorios) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Directorios) 
	{
        this.eventManager.broadcast({ name: 'directoriosListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }

    trackAnnoTributaById(index: number, item: AnnoTributa) { return item.id; }

    trackProductoById(index: number, item: Producto) { return item.id; }

    trackInstrumentoById(index: number, item: Instrumento) { return item.id; }
}

@Component({
    selector: 'jhi-directorios-popup',
    template: ''
})
export class DirectoriosPopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;

    constructor( private route: ActivatedRoute, private directoriosPopupService: DirectoriosPopupService ) {}

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => 
		{
            if ( params['id'] ) 
			{
                this.directoriosPopupService.open(DirectoriosDialogComponent as Component, params['id']);
            } 
			else { this.directoriosPopupService.open(DirectoriosDialogComponent as Component); }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}