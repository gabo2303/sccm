import { BaseEntity } from './../../shared';

export class Directorios implements BaseEntity {
    constructor(
        public id?: number,
        public ruta?: string,
        public descripcion?: string,
        public annoTributa?: BaseEntity,
        public producto?: BaseEntity,
        public instrumento?: BaseEntity,
    ) {
    }
}
