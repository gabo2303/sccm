import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Directorios } from './directorios.model';
import { DirectoriosService } from './directorios.service';

@Component({
    selector: 'jhi-directorios-detail',
    templateUrl: './directorios-detail.component.html'
})
export class DirectoriosDetailComponent implements OnInit, OnDestroy {

    directorios: Directorios;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private directoriosService: DirectoriosService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDirectorios();
    }

    load(id) {
        this.directoriosService.find(id).subscribe((directorios) => {
            this.directorios = directorios;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDirectorios() {
        this.eventSubscriber = this.eventManager.subscribe(
            'directoriosListModification',
            (response) => this.load(this.directorios.id)
        );
    }
}
