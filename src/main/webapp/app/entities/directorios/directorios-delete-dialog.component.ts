import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Directorios } from './directorios.model';
import { DirectoriosPopupService } from './directorios-popup.service';
import { DirectoriosService } from './directorios.service';

@Component({
    selector: 'jhi-directorios-delete-dialog',
    templateUrl: './directorios-delete-dialog.component.html'
})
export class DirectoriosDeleteDialogComponent {

    directorios: Directorios;

    constructor(
        private directoriosService: DirectoriosService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.directoriosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'directoriosListModification',
                content: 'Deleted an directorios'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-directorios-delete-popup',
    template: ''
})
export class DirectoriosDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private directoriosPopupService: DirectoriosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.directoriosPopupService
                .open(DirectoriosDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
