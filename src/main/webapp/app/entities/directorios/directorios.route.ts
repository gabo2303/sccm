import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DirectoriosComponent } from './directorios.component';
import { DirectoriosDetailComponent } from './directorios-detail.component';
import { DirectoriosPopupComponent } from './directorios-dialog.component';
import { DirectoriosDeletePopupComponent } from './directorios-delete-dialog.component';

@Injectable()
export class DirectoriosResolvePagingParams implements Resolve<any> 
{
    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) 
	{
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        
		return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
		};
    }
}

export const directoriosRoute: Routes = 
[
    {
        path: 'directorios',
        component: DirectoriosComponent,
        resolve: {
            'pagingParams': DirectoriosResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.directorios.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, 
	{
        path: 'directorios/:id',
        component: DirectoriosDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.directorios.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const directoriosPopupRoute: Routes = 
[
    {
        path: 'directorios-new',
        component: DirectoriosPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.directorios.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'directorios/:id/edit',
        component: DirectoriosPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.directorios.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'directorios/:id/delete',
        component: DirectoriosDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.directorios.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];