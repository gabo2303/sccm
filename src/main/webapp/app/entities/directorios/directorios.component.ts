import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { Directorios } from './directorios.model';
import { DirectoriosService } from './directorios.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-directorios',
    templateUrl: './directorios.component.html'
})
export class DirectoriosComponent implements OnInit, OnDestroy 
{
	currentAccount: any;
    directorios: Directorios[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor( private directoriosService: DirectoriosService, private parseLinks: JhiParseLinks, private jhiAlertService: JhiAlertService,
				 private principal: Principal, private activatedRoute: ActivatedRoute, private router: Router, private eventManager: JhiEventManager,
				 private spinnerService: Ng4LoadingSpinnerService) 
	{
        this.itemsPerPage = ITEMS_PER_PAGE;
        
		this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    loadAll() 
	{
        this.directoriosService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
    loadPage(page: number) 
	{
        if (page !== this.previousPage) 
		{
            this.previousPage = page;
            this.transition();
        }
    }
	
    transition() 
	{
        this.router.navigate(['/directorios'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
		
        this.loadAll();
    }

    clear() 
	{
        this.page = 0;
        
		this.router.navigate(['/directorios', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        
		this.loadAll();
    }
	
    ngOnInit() 
	{
		this.spinnerService.show();
		
        this.loadAll();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInDirectorios();
		
		this.spinnerService.hide();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: Directorios) { return item.id; }
    
	registerChangeInDirectorios() 
	{
        this.eventSubscriber = this.eventManager.subscribe('directoriosListModification', (response) => this.loadAll());
    }

    sort() 
	{
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        
		if (this.predicate !== 'id') { result.push('id'); }
		
        return result;
    }

    private onSuccess(data, headers) 
	{
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        
        this.directorios = data;
    }
	
    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
}