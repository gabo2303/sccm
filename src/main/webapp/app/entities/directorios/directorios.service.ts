import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Directorios } from './directorios.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DirectoriosService {

    private resourceUrl = SERVER_API_URL + 'api/directorios';

    constructor(private http: Http) { }

    create(directorios: Directorios): Observable<Directorios> {
        const copy = this.convert(directorios);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(directorios: Directorios): Observable<Directorios> {
        const copy = this.convert(directorios);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Directorios> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Directorios.
     */
    private convertItemFromServer(json: any): Directorios {
        const entity: Directorios = Object.assign(new Directorios(), json);
        return entity;
    }

    /**
     * Convert a Directorios to a JSON which can be sent to the server.
     */
    private convert(directorios: Directorios): Directorios {
        const copy: Directorios = Object.assign({}, directorios);
        return copy;
    }
}
