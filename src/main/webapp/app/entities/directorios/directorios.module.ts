import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    DirectoriosService,
    DirectoriosPopupService,
    DirectoriosComponent,
    DirectoriosDetailComponent,
    DirectoriosDialogComponent,
    DirectoriosPopupComponent,
    DirectoriosDeletePopupComponent,
    DirectoriosDeleteDialogComponent,
    directoriosRoute,
    directoriosPopupRoute,
    DirectoriosResolvePagingParams
} from './';

const ENTITY_STATES = [
    ...directoriosRoute,
    ...directoriosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DirectoriosComponent,
        DirectoriosDetailComponent,
        DirectoriosDialogComponent,
        DirectoriosDeleteDialogComponent,
        DirectoriosPopupComponent,
        DirectoriosDeletePopupComponent
    ],
    entryComponents: [
        DirectoriosComponent,
        DirectoriosDialogComponent,
        DirectoriosPopupComponent,
        DirectoriosDeleteDialogComponent,
        DirectoriosDeletePopupComponent
    ],
    providers: [
        DirectoriosService,
        DirectoriosPopupService,
        DirectoriosResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmDirectoriosModule {}
