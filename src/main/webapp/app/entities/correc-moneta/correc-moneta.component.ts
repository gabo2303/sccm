import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { CorrecMoneta } from './correc-moneta.model';
import { CorrecMonetaService } from './correc-moneta.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-correc-moneta',
    templateUrl: './correc-moneta.component.html'
})
export class CorrecMonetaComponent implements OnInit, OnDestroy {

    correcMonetas: CorrecMoneta[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];

    constructor(private correcMonetaService: CorrecMonetaService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private spinnerService: Ng4LoadingSpinnerService) {
    }

    loadAll() {
        this.loadAnnosTributarios();
        this.correcMonetaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.correcMonetas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCorrecMonetas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CorrecMoneta) {
        return item.id;
    }

    registerChangeInCorrecMonetas() {
        this.eventSubscriber = this.eventManager.subscribe('correcMonetaListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }
}
