import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CorrecMoneta } from './correc-moneta.model';
import { CorrecMonetaService } from './correc-moneta.service';

@Component({
    selector: 'jhi-correc-moneta-detail',
    templateUrl: './correc-moneta-detail.component.html'
})
export class CorrecMonetaDetailComponent implements OnInit, OnDestroy {

    correcMoneta: CorrecMoneta;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private correcMonetaService: CorrecMonetaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInCorrecMonetas();
    }

    load(id) {
        this.correcMonetaService.find(id).subscribe((correcMoneta) => {
            this.correcMoneta = correcMoneta;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCorrecMonetas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'correcMonetaListModification',
            (response) => this.load(this.correcMoneta.id)
        );
    }
}
