import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { CorrecMonetaComponent } from './correc-moneta.component';
import { CorrecMonetaDetailComponent } from './correc-moneta-detail.component';
import { CorrecMonetaPopupComponent } from './correc-moneta-dialog.component';
import { CorrecMonetaDeletePopupComponent } from './correc-moneta-delete-dialog.component';

export const correcMonetaRoute: Routes = [
    {
        path: 'correc-moneta',
        component: CorrecMonetaComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.correcMoneta.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'correc-moneta/:id',
        component: CorrecMonetaDetailComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.correcMoneta.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const correcMonetaPopupRoute: Routes = [
    {
        path: 'correc-moneta-new',
        component: CorrecMonetaPopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.correcMoneta.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'correc-moneta/:id/edit',
        component: CorrecMonetaPopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.correcMoneta.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'correc-moneta/:id/delete',
        component: CorrecMonetaDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.correcMoneta.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
