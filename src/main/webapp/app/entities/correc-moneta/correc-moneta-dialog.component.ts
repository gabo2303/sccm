import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CorrecMoneta } from './correc-moneta.model';
import { CorrecMonetaPopupService } from './correc-moneta-popup.service';
import { CorrecMonetaService } from './correc-moneta.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-correc-moneta-dialog',
    templateUrl: './correc-moneta-dialog.component.html'
})
export class CorrecMonetaDialogComponent implements OnInit {

    correcMoneta: CorrecMoneta;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private correcMonetaService: CorrecMonetaService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.correcMoneta.id !== undefined) {
            this.subscribeToSaveResponse(
                this.correcMonetaService.update(this.correcMoneta));
        } else {
            this.subscribeToSaveResponse(
                this.correcMonetaService.create(this.correcMoneta));
        }
    }

    private subscribeToSaveResponse(result: Observable<CorrecMoneta>) {
        result.subscribe((res: CorrecMoneta) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CorrecMoneta) {
        this.eventManager.broadcast({ name: 'correcMonetaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-correc-moneta-popup',
    template: ''
})
export class CorrecMonetaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private correcMonetaPopupService: CorrecMonetaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.correcMonetaPopupService
                    .open(CorrecMonetaDialogComponent as Component, params['id']);
            } else {
                this.correcMonetaPopupService
                    .open(CorrecMonetaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
