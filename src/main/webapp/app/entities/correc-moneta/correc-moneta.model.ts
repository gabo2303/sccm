import { BaseEntity } from './../../shared';

export class CorrecMoneta implements BaseEntity {
    constructor(
        public id?: number,
        public mes?: number,
        public valor?: number,
        public annoTributa?: BaseEntity,
    ) {
    }
}
