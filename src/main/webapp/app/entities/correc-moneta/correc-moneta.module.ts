import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    CorrecMonetaService,
    CorrecMonetaPopupService,
    CorrecMonetaComponent,
    CorrecMonetaDetailComponent,
    CorrecMonetaDialogComponent,
    CorrecMonetaPopupComponent,
    CorrecMonetaDeletePopupComponent,
    CorrecMonetaDeleteDialogComponent,
    correcMonetaRoute,
    correcMonetaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...correcMonetaRoute,
    ...correcMonetaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CorrecMonetaComponent,
        CorrecMonetaDetailComponent,
        CorrecMonetaDialogComponent,
        CorrecMonetaDeleteDialogComponent,
        CorrecMonetaPopupComponent,
        CorrecMonetaDeletePopupComponent,
    ],
    entryComponents: [
        CorrecMonetaComponent,
        CorrecMonetaDialogComponent,
        CorrecMonetaPopupComponent,
        CorrecMonetaDeleteDialogComponent,
        CorrecMonetaDeletePopupComponent,
    ],
    providers: [
        CorrecMonetaService,
        CorrecMonetaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmCorrecMonetaModule {}
