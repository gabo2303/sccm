import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CorrecMoneta } from './correc-moneta.model';
import { CorrecMonetaPopupService } from './correc-moneta-popup.service';
import { CorrecMonetaService } from './correc-moneta.service';

@Component({
    selector: 'jhi-correc-moneta-delete-dialog',
    templateUrl: './correc-moneta-delete-dialog.component.html'
})
export class CorrecMonetaDeleteDialogComponent {

    correcMoneta: CorrecMoneta;

    constructor(private correcMonetaService: CorrecMonetaService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.correcMonetaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'correcMonetaListModification',
                content: 'Deleted an correcMoneta'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-correc-moneta-delete-popup',
    template: ''
})
export class CorrecMonetaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private correcMonetaPopupService: CorrecMonetaPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.correcMonetaPopupService
            .open(CorrecMonetaDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
