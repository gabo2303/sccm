import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SalProxPer } from './sal-prox-per.model';
import { SalProxPerPopupService } from './sal-prox-per-popup.service';
import { SalProxPerService } from './sal-prox-per.service';

@Component({
    selector: 'jhi-sal-prox-per-delete-dialog',
    templateUrl: './sal-prox-per-delete-dialog.component.html'
})
export class SalProxPerDeleteDialogComponent {

    salProxPer: SalProxPer;

    constructor(
        private salProxPerService: SalProxPerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.salProxPerService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'salProxPerListModification',
                content: 'Deleted an salProxPer'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sal-prox-per-delete-popup',
    template: ''
})
export class SalProxPerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private salProxPerPopupService: SalProxPerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.salProxPerPopupService
                .open(SalProxPerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
