import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SalProxPer } from './sal-prox-per.model';
import { SalProxPerService } from './sal-prox-per.service';

@Component({
    selector: 'jhi-sal-prox-per-detail',
    templateUrl: './sal-prox-per-detail.component.html'
})
export class SalProxPerDetailComponent implements OnInit, OnDestroy {

    salProxPer: SalProxPer;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private salProxPerService: SalProxPerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSalProxPers();
    }

    load(id) {
        this.salProxPerService.find(id).subscribe((salProxPer) => {
            this.salProxPer = salProxPer;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSalProxPers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'salProxPerListModification',
            (response) => this.load(this.salProxPer.id)
        );
    }
}
