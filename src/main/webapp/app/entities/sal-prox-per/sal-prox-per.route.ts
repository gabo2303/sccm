import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalProxPerComponent } from './sal-prox-per.component';
import { SalProxPerDetailComponent } from './sal-prox-per-detail.component';
import { SalProxPerPopupComponent } from './sal-prox-per-dialog.component';
import { SalProxPerDeletePopupComponent } from './sal-prox-per-delete-dialog.component';

export const salProxPerRoute: Routes = [
    {
        path: 'sal-prox-per',
        component: SalProxPerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salProxPer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sal-prox-per/:id',
        component: SalProxPerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salProxPer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salProxPerPopupRoute: Routes = [
    {
        path: 'sal-prox-per-new',
        component: SalProxPerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salProxPer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sal-prox-per/:id/edit',
        component: SalProxPerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salProxPer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sal-prox-per/:id/delete',
        component: SalProxPerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salProxPer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
