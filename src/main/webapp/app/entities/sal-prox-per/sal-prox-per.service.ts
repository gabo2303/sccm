import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { SalProxPer } from './sal-prox-per.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SalProxPerService {

    private resourceUrl = SERVER_API_URL + 'api/sal-prox-pers';

    constructor(private http: Http) { }

    create(salProxPer: SalProxPer): Observable<SalProxPer> {
        const copy = this.convert(salProxPer);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(salProxPer: SalProxPer): Observable<SalProxPer> {
        const copy = this.convert(salProxPer);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<SalProxPer> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SalProxPer.
     */
    private convertItemFromServer(json: any): SalProxPer {
        const entity: SalProxPer = Object.assign(new SalProxPer(), json);
        return entity;
    }

    /**
     * Convert a SalProxPer to a JSON which can be sent to the server.
     */
    private convert(salProxPer: SalProxPer): SalProxPer {
        const copy: SalProxPer = Object.assign({}, salProxPer);
        return copy;
    }
}
