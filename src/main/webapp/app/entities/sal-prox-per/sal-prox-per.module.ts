import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    SalProxPerService,
    SalProxPerPopupService,
    SalProxPerComponent,
    SalProxPerDetailComponent,
    SalProxPerDialogComponent,
    SalProxPerPopupComponent,
    SalProxPerDeletePopupComponent,
    SalProxPerDeleteDialogComponent,
    salProxPerRoute,
    salProxPerPopupRoute,
} from './';

const ENTITY_STATES = [
    ...salProxPerRoute,
    ...salProxPerPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SalProxPerComponent,
        SalProxPerDetailComponent,
        SalProxPerDialogComponent,
        SalProxPerDeleteDialogComponent,
        SalProxPerPopupComponent,
        SalProxPerDeletePopupComponent,
    ],
    entryComponents: [
        SalProxPerComponent,
        SalProxPerDialogComponent,
        SalProxPerPopupComponent,
        SalProxPerDeleteDialogComponent,
        SalProxPerDeletePopupComponent,
    ],
    providers: [
        SalProxPerService,
        SalProxPerPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmSalProxPerModule {}
