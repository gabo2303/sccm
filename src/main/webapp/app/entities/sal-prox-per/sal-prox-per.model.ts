import { BaseEntity } from './../../shared';

export class SalProxPer implements BaseEntity {
    constructor(
        public id?: number,
        public tipoSal?: string,
        public saldoNetoPer?: number,
        public saldoProxPer?: number,
        public giroRentab?: number,
        public annoAct?: number,
        public annoTributa?: BaseEntity,
        public clienteBice?: BaseEntity,
    ) {
    }
}
