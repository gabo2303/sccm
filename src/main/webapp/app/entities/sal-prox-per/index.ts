export * from './sal-prox-per.model';
export * from './sal-prox-per-popup.service';
export * from './sal-prox-per.service';
export * from './sal-prox-per-dialog.component';
export * from './sal-prox-per-delete-dialog.component';
export * from './sal-prox-per-detail.component';
export * from './sal-prox-per.component';
export * from './sal-prox-per.route';
