import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { SalProxPer } from './sal-prox-per.model';
import { SalProxPerService } from './sal-prox-per.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sal-prox-per',
    templateUrl: './sal-prox-per.component.html'
})
export class SalProxPerComponent implements OnInit, OnDestroy {
salProxPers: SalProxPer[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private salProxPerService: SalProxPerService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.salProxPerService.query().subscribe(
            (res: ResponseWrapper) => {
                this.salProxPers = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSalProxPers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SalProxPer) {
        return item.id;
    }
    registerChangeInSalProxPers() {
        this.eventSubscriber = this.eventManager.subscribe('salProxPerListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
