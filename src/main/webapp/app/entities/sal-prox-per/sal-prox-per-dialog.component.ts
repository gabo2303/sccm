import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SalProxPer } from './sal-prox-per.model';
import { SalProxPerPopupService } from './sal-prox-per-popup.service';
import { SalProxPerService } from './sal-prox-per.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ClienteBice, ClienteBiceService } from '../cliente-bice';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sal-prox-per-dialog',
    templateUrl: './sal-prox-per-dialog.component.html'
})
export class SalProxPerDialogComponent implements OnInit {

    salProxPer: SalProxPer;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    clientebices: ClienteBice[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private salProxPerService: SalProxPerService,
        private annoTributaService: AnnoTributaService,
        private clienteBiceService: ClienteBiceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clienteBiceService.query()
            .subscribe((res: ResponseWrapper) => { this.clientebices = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.salProxPer.id !== undefined) {
            this.subscribeToSaveResponse(
                this.salProxPerService.update(this.salProxPer));
        } else {
            this.subscribeToSaveResponse(
                this.salProxPerService.create(this.salProxPer));
        }
    }

    private subscribeToSaveResponse(result: Observable<SalProxPer>) {
        result.subscribe((res: SalProxPer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SalProxPer) {
        this.eventManager.broadcast({ name: 'salProxPerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackClienteBiceById(index: number, item: ClienteBice) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-sal-prox-per-popup',
    template: ''
})
export class SalProxPerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private salProxPerPopupService: SalProxPerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salProxPerPopupService
                    .open(SalProxPerDialogComponent as Component, params['id']);
            } else {
                this.salProxPerPopupService
                    .open(SalProxPerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
