import { BaseEntity } from './../../shared';

export class Contabilidad implements BaseEntity {
    constructor(
        public id?: number,
        public sucursal?: number,
        public moneda?: number,
        public ctaContGl?: number,
        public ctaContAlp?: number,
        public fecha?: any,
        public montoDr?: number,
        public montoCr?: number,
        public sucOrigen?: number,
        public modulo?: string,
        public numDocto?: string,
        public ctaSbif?: string,
        public glosa?: string,
        public annoTributa?: BaseEntity,
    ) {
    }
}
