import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    ContabilidadService,
    ContabilidadPopupService,
    ContabilidadComponent,
    ContabilidadDetailComponent,
    ContabilidadDialogComponent,
    ContabilidadPopupComponent,
    ContabilidadDeletePopupComponent,
    ContabilidadDeleteDialogComponent,
    contabilidadRoute,
    contabilidadPopupRoute,
} from './';

const ENTITY_STATES = [
    ...contabilidadRoute,
    ...contabilidadPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ContabilidadComponent,
        ContabilidadDetailComponent,
        ContabilidadDialogComponent,
        ContabilidadDeleteDialogComponent,
        ContabilidadPopupComponent,
        ContabilidadDeletePopupComponent,
    ],
    entryComponents: [
        ContabilidadComponent,
        ContabilidadDialogComponent,
        ContabilidadPopupComponent,
        ContabilidadDeleteDialogComponent,
        ContabilidadDeletePopupComponent,
    ],
    providers: [
        ContabilidadService,
        ContabilidadPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmContabilidadModule {}
