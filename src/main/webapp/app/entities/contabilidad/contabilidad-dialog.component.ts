import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Contabilidad } from './contabilidad.model';
import { ContabilidadPopupService } from './contabilidad-popup.service';
import { ContabilidadService } from './contabilidad.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-contabilidad-dialog',
    templateUrl: './contabilidad-dialog.component.html'
})
export class ContabilidadDialogComponent implements OnInit {

    contabilidad: Contabilidad;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private contabilidadService: ContabilidadService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.contabilidad.id !== undefined) {
            this.subscribeToSaveResponse(
                this.contabilidadService.update(this.contabilidad));
        } else {
            this.subscribeToSaveResponse(
                this.contabilidadService.create(this.contabilidad));
        }
    }

    private subscribeToSaveResponse(result: Observable<Contabilidad>) {
        result.subscribe((res: Contabilidad) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Contabilidad) {
        this.eventManager.broadcast({ name: 'contabilidadListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-contabilidad-popup',
    template: ''
})
export class ContabilidadPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private contabilidadPopupService: ContabilidadPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.contabilidadPopupService
                    .open(ContabilidadDialogComponent as Component, params['id']);
            } else {
                this.contabilidadPopupService
                    .open(ContabilidadDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
