import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ContabilidadComponent } from './contabilidad.component';
import { ContabilidadDetailComponent } from './contabilidad-detail.component';
import { ContabilidadPopupComponent } from './contabilidad-dialog.component';
import { ContabilidadDeletePopupComponent } from './contabilidad-delete-dialog.component';

export const contabilidadRoute: Routes = [
    {
        path: 'contabilidad',
        component: ContabilidadComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.contabilidad.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'contabilidad/:id',
        component: ContabilidadDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.contabilidad.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const contabilidadPopupRoute: Routes = [
    {
        path: 'contabilidad-new',
        component: ContabilidadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.contabilidad.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'contabilidad/:id/edit',
        component: ContabilidadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.contabilidad.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'contabilidad/:id/delete',
        component: ContabilidadDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.contabilidad.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
