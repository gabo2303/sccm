import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Contabilidad } from './contabilidad.model';
import { ContabilidadPopupService } from './contabilidad-popup.service';
import { ContabilidadService } from './contabilidad.service';

@Component({
    selector: 'jhi-contabilidad-delete-dialog',
    templateUrl: './contabilidad-delete-dialog.component.html'
})
export class ContabilidadDeleteDialogComponent {

    contabilidad: Contabilidad;

    constructor(
        private contabilidadService: ContabilidadService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.contabilidadService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'contabilidadListModification',
                content: 'Deleted an contabilidad'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-contabilidad-delete-popup',
    template: ''
})
export class ContabilidadDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private contabilidadPopupService: ContabilidadPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.contabilidadPopupService
                .open(ContabilidadDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
