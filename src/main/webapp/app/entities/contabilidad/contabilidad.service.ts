import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Contabilidad } from './contabilidad.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ContabilidadService {

    private resourceUrl = SERVER_API_URL + 'api/contabilidads';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(contabilidad: Contabilidad): Observable<Contabilidad> {
        const copy = this.convert(contabilidad);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(contabilidad: Contabilidad): Observable<Contabilidad> {
        const copy = this.convert(contabilidad);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Contabilidad> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Contabilidad.
     */
    private convertItemFromServer(json: any): Contabilidad {
        const entity: Contabilidad = Object.assign(new Contabilidad(), json);
        entity.fecha = this.dateUtils
            .convertDateTimeFromServer(json.fecha);
        return entity;
    }

    /**
     * Convert a Contabilidad to a JSON which can be sent to the server.
     */
    private convert(contabilidad: Contabilidad): Contabilidad {
        const copy: Contabilidad = Object.assign({}, contabilidad);

        copy.fecha = this.dateUtils.toDate(contabilidad.fecha);
        return copy;
    }
}
