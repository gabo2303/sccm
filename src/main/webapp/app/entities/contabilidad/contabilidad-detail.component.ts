import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Contabilidad } from './contabilidad.model';
import { ContabilidadService } from './contabilidad.service';

@Component({
    selector: 'jhi-contabilidad-detail',
    templateUrl: './contabilidad-detail.component.html'
})
export class ContabilidadDetailComponent implements OnInit, OnDestroy {

    contabilidad: Contabilidad;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private contabilidadService: ContabilidadService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInContabilidads();
    }

    load(id) {
        this.contabilidadService.find(id).subscribe((contabilidad) => {
            this.contabilidad = contabilidad;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInContabilidads() {
        this.eventSubscriber = this.eventManager.subscribe(
            'contabilidadListModification',
            (response) => this.load(this.contabilidad.id)
        );
    }
}
