import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Contabilidad } from './contabilidad.model';
import { ContabilidadService } from './contabilidad.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-contabilidad',
    templateUrl: './contabilidad.component.html'
})
export class ContabilidadComponent implements OnInit, OnDestroy {
contabilidads: Contabilidad[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private contabilidadService: ContabilidadService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.contabilidadService.query().subscribe(
            (res: ResponseWrapper) => {
                this.contabilidads = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInContabilidads();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Contabilidad) {
        return item.id;
    }
    registerChangeInContabilidads() {
        this.eventSubscriber = this.eventManager.subscribe('contabilidadListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
