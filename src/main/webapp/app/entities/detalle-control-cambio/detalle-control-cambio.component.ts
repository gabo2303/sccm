import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { DetalleControlCambio } from './detalle-control-cambio.model';
import { DetalleControlCambioService } from './detalle-control-cambio.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-detalle-control-cambio',
    templateUrl: './detalle-control-cambio.component.html'
})
export class DetalleControlCambioComponent implements OnInit, OnDestroy {
detalleControlCambios: DetalleControlCambio[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private detalleControlCambioService: DetalleControlCambioService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.detalleControlCambioService.query().subscribe(
            (res: ResponseWrapper) => {
                this.detalleControlCambios = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDetalleControlCambios();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DetalleControlCambio) {
        return item.id;
    }
    registerChangeInDetalleControlCambios() {
        this.eventSubscriber = this.eventManager.subscribe('detalleControlCambioListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
