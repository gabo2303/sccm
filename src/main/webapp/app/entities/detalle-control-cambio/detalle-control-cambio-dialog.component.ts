import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DetalleControlCambio } from './detalle-control-cambio.model';
import { DetalleControlCambioPopupService } from './detalle-control-cambio-popup.service';
import { DetalleControlCambioService } from './detalle-control-cambio.service';
import { CabeceraControlCambio, CabeceraControlCambioService } from '../cabecera-control-cambio';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-detalle-control-cambio-dialog',
    templateUrl: './detalle-control-cambio-dialog.component.html'
})
export class DetalleControlCambioDialogComponent implements OnInit {

    detalleControlCambio: DetalleControlCambio;
    isSaving: boolean;

    cabeceracontrolcambios: CabeceraControlCambio[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private detalleControlCambioService: DetalleControlCambioService,
        private cabeceraControlCambioService: CabeceraControlCambioService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.cabeceraControlCambioService.query()
            .subscribe((res: ResponseWrapper) => { this.cabeceracontrolcambios = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.detalleControlCambio.id !== undefined) {
            this.subscribeToSaveResponse(
                this.detalleControlCambioService.update(this.detalleControlCambio));
        } else {
            this.subscribeToSaveResponse(
                this.detalleControlCambioService.create(this.detalleControlCambio));
        }
    }

    private subscribeToSaveResponse(result: Observable<DetalleControlCambio>) {
        result.subscribe((res: DetalleControlCambio) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DetalleControlCambio) {
        this.eventManager.broadcast({ name: 'detalleControlCambioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCabeceraControlCambioById(index: number, item: CabeceraControlCambio) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-detalle-control-cambio-popup',
    template: ''
})
export class DetalleControlCambioPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detalleControlCambioPopupService: DetalleControlCambioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.detalleControlCambioPopupService
                    .open(DetalleControlCambioDialogComponent as Component, params['id']);
            } else {
                this.detalleControlCambioPopupService
                    .open(DetalleControlCambioDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
