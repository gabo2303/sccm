import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    DetalleControlCambioService,
    DetalleControlCambioPopupService,
    DetalleControlCambioComponent,
    DetalleControlCambioDetailComponent,
    DetalleControlCambioDialogComponent,
    DetalleControlCambioPopupComponent,
    DetalleControlCambioDeletePopupComponent,
    DetalleControlCambioDeleteDialogComponent,
    detalleControlCambioRoute,
    detalleControlCambioPopupRoute,
} from './';

const ENTITY_STATES = [
    ...detalleControlCambioRoute,
    ...detalleControlCambioPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DetalleControlCambioComponent,
        DetalleControlCambioDetailComponent,
        DetalleControlCambioDialogComponent,
        DetalleControlCambioDeleteDialogComponent,
        DetalleControlCambioPopupComponent,
        DetalleControlCambioDeletePopupComponent,
    ],
    entryComponents: [
        DetalleControlCambioComponent,
        DetalleControlCambioDialogComponent,
        DetalleControlCambioPopupComponent,
        DetalleControlCambioDeleteDialogComponent,
        DetalleControlCambioDeletePopupComponent,
    ],
    providers: [
        DetalleControlCambioService,
        DetalleControlCambioPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmDetalleControlCambioModule {}
