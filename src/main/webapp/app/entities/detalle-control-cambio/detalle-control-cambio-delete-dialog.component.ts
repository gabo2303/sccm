import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DetalleControlCambio } from './detalle-control-cambio.model';
import { DetalleControlCambioPopupService } from './detalle-control-cambio-popup.service';
import { DetalleControlCambioService } from './detalle-control-cambio.service';

@Component({
    selector: 'jhi-detalle-control-cambio-delete-dialog',
    templateUrl: './detalle-control-cambio-delete-dialog.component.html'
})
export class DetalleControlCambioDeleteDialogComponent {

    detalleControlCambio: DetalleControlCambio;

    constructor(
        private detalleControlCambioService: DetalleControlCambioService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.detalleControlCambioService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'detalleControlCambioListModification',
                content: 'Deleted an detalleControlCambio'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-detalle-control-cambio-delete-popup',
    template: ''
})
export class DetalleControlCambioDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detalleControlCambioPopupService: DetalleControlCambioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.detalleControlCambioPopupService
                .open(DetalleControlCambioDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
