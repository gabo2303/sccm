import { BaseEntity } from './../../shared';

export class DetalleControlCambio implements BaseEntity {
    constructor(
        public id?: number,
        public nombreCampo?: string,
        public valorCampo?: string,
        public tipoCampo?: string,
		public imagen?: any,
        public detcabcontrol?: BaseEntity,
    ) {
    }
}
