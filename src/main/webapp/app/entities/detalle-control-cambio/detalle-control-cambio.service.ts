import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { DetalleControlCambio } from './detalle-control-cambio.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DetalleControlCambioService 
{
    private resourceUrl = SERVER_API_URL + 'api/detalle-control-cambios';

    constructor(private http: Http) { }

    create(detalleControlCambio: DetalleControlCambio): Observable<DetalleControlCambio> 
	{
        const copy = this.convert(detalleControlCambio);
        
		return this.http.post(this.resourceUrl, copy).map( (res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(detalleControlCambio: DetalleControlCambio): Observable<DetalleControlCambio> 
	{
        const copy = this.convert(detalleControlCambio);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<DetalleControlCambio> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{		
        const options = createRequestOption(req);
		
		console.log('ENTRA A QUERY', req);
		
        return this.http.get(this.resourceUrl, { params: { 'detcabcontrolId.equals':req } } ).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to DetalleControlCambio.
     */
    private convertItemFromServer(json: any): DetalleControlCambio {
        const entity: DetalleControlCambio = Object.assign(new DetalleControlCambio(), json);
        return entity;
    }

    /**
     * Convert a DetalleControlCambio to a JSON which can be sent to the server.
     */
    private convert(detalleControlCambio: DetalleControlCambio): DetalleControlCambio 
	{
        const copy: DetalleControlCambio = Object.assign({}, detalleControlCambio);
        
		return copy;
    }
}