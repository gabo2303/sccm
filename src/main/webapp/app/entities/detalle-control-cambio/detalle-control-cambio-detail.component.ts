import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DetalleControlCambio } from './detalle-control-cambio.model';
import { DetalleControlCambioService } from './detalle-control-cambio.service';

@Component({
    selector: 'jhi-detalle-control-cambio-detail',
    templateUrl: './detalle-control-cambio-detail.component.html'
})
export class DetalleControlCambioDetailComponent implements OnInit, OnDestroy {

    detalleControlCambio: DetalleControlCambio;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private detalleControlCambioService: DetalleControlCambioService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDetalleControlCambios();
    }

    load(id) {
        this.detalleControlCambioService.find(id).subscribe((detalleControlCambio) => {
            this.detalleControlCambio = detalleControlCambio;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDetalleControlCambios() {
        this.eventSubscriber = this.eventManager.subscribe(
            'detalleControlCambioListModification',
            (response) => this.load(this.detalleControlCambio.id)
        );
    }
}
