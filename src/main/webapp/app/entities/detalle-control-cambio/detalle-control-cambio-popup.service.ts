import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DetalleControlCambio } from './detalle-control-cambio.model';
import { DetalleControlCambioService } from './detalle-control-cambio.service';

@Injectable()
export class DetalleControlCambioPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private detalleControlCambioService: DetalleControlCambioService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.detalleControlCambioService.find(id).subscribe((detalleControlCambio) => {
                    this.ngbModalRef = this.detalleControlCambioModalRef(component, detalleControlCambio);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.detalleControlCambioModalRef(component, new DetalleControlCambio());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    detalleControlCambioModalRef(component: Component, detalleControlCambio: DetalleControlCambio): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.detalleControlCambio = detalleControlCambio;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
