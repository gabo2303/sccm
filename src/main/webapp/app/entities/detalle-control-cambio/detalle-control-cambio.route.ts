import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DetalleControlCambioComponent } from './detalle-control-cambio.component';
import { DetalleControlCambioDetailComponent } from './detalle-control-cambio-detail.component';
import { DetalleControlCambioPopupComponent } from './detalle-control-cambio-dialog.component';
import { DetalleControlCambioDeletePopupComponent } from './detalle-control-cambio-delete-dialog.component';

export const detalleControlCambioRoute: Routes = [
    {
        path: 'detalle-control-cambio',
        component: DetalleControlCambioComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.detalleControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'detalle-control-cambio/:id',
        component: DetalleControlCambioDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.detalleControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const detalleControlCambioPopupRoute: Routes = [
    {
        path: 'detalle-control-cambio-new',
        component: DetalleControlCambioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.detalleControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detalle-control-cambio/:id/edit',
        component: DetalleControlCambioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.detalleControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detalle-control-cambio/:id/delete',
        component: DetalleControlCambioDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.detalleControlCambio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
