import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DapIR } from './dap-ir.model';
import { DapIRService } from './dap-ir.service';

@Injectable()
export class DapIRPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private dapIRService: DapIRService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dapIRService.find(id).subscribe((dapIR) => {
                    if (dapIR.fechaPago) {
                        dapIR.fechaPago = {
                            year: dapIR.fechaPago.getFullYear(),
                            month: dapIR.fechaPago.getMonth() + 1,
                            day: dapIR.fechaPago.getDate()
                        };
                    }
                    this.ngbModalRef = this.dapIRModalRef(component, dapIR);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dapIRModalRef(component, new DapIR());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dapIRModalRef(component: Component, dapIR: DapIR): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dapIR = dapIR;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
