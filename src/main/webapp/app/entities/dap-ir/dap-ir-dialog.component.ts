import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DapIR } from './dap-ir.model';
import { DapIRPopupService } from './dap-ir-popup.service';
import { DapIRService } from './dap-ir.service';

@Component({
    selector: 'jhi-dap-ir-dialog',
    templateUrl: './dap-ir-dialog.component.html'
})
export class DapIRDialogComponent implements OnInit {

    dapIR: DapIR;
    isSaving: boolean;
    fechaPagoDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private dapIRService: DapIRService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dapIR.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dapIRService.update(this.dapIR));
        } else {
            this.subscribeToSaveResponse(
                this.dapIRService.create(this.dapIR));
        }
    }

    private subscribeToSaveResponse(result: Observable<DapIR>) {
        result.subscribe((res: DapIR) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DapIR) {
        this.eventManager.broadcast({ name: 'dapIRListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-dap-ir-popup',
    template: ''
})
export class DapIRPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dapIRPopupService: DapIRPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dapIRPopupService
                    .open(DapIRDialogComponent as Component, params['id']);
            } else {
                this.dapIRPopupService
                    .open(DapIRDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
