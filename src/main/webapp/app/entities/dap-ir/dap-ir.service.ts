import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DapIR } from './dap-ir.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { isNullOrUndefined } from 'util';
import { DapIrDto } from './dap-ir-dto.model';

@Injectable()
export class DapIRService {

    private resourceUrl = SERVER_API_URL + 'api/dap-irs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findByAllByTaxYearId(idYear: number): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearId`).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    findByAllByTaxYearIdCurrencyPaginated(idYear: number, currency: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const requestUrl = this.resourceUrl + '/findByAllByTaxYearIdCurrencyPaginated?idAnno=' + idYear + (isNullOrUndefined(currency) ? '' : '&currency=' + currency);
        return this.http.get(requestUrl, options).map(
            (res: Response) => this.convertResponse(res));
    }

    create(dapIR: DapIR): Observable<DapIR> {
        const copy = this.convert(dapIR);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(dapIR: DapIR): Observable<DapIR> {
        const copy = this.convert(dapIR);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<DapIR> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse2(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer2(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to DapIrDto.
     */
    private convertItemFromServer2(json: any): DapIrDto {
        const entity: DapIrDto = Object.assign(new DapIrDto(), json);
        return entity;
    }

    /**
     * Convert a returned JSON object to DapIR.
     */
    private convertItemFromServer(json: any): DapIR {
        const entity: DapIR = Object.assign(new DapIR(), json);
        entity.fechaPago = this.dateUtils.convertLocalDateFromServer(json.fechaPago);
        return entity;
    }

    /**
     * Convert a DapIR to a JSON which can be sent to the server.
     */
    private convert(dapIR: DapIR): DapIR {
        const copy: DapIR = Object.assign({}, dapIR);
        copy.fechaPago = this.dateUtils.convertLocalDateToServer(dapIR.fechaPago);
        return copy;
    }
}
