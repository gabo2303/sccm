import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DapIRComponent } from './dap-ir.component';
import { SCCM_1890_USER_DAP } from '../../app.constants';

@Injectable()
export class DapIRResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const dapIRRoute: Routes = [
    /*{
        path: 'dap-ir',
        component: DapIRComponent,
        resolve: {
            'pagingParams': DapIRResolvePagingParams
        },
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapIR.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'dap-ir/:id',
        component: DapIRDetailComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapIR.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    },*/
    {
        path: 'intRealDap',
        component: DapIRComponent,
        resolve: {
            'pagingParams': DapIRResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dapIR.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const dapIRPopupRoute: Routes = [
    /*{
        path: 'dap-ir-new',
        component: DapIRPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapIR.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'dap-ir/:id/edit',
        component: DapIRPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapIR.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'dap-ir/:id/delete',
        component: DapIRDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapIR.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }*/
];
