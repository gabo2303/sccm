import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DapIR } from './dap-ir.model';
import { DapIRService } from './dap-ir.service';

@Component({
    selector: 'jhi-dap-ir-detail',
    templateUrl: './dap-ir-detail.component.html'
})
export class DapIRDetailComponent implements OnInit, OnDestroy {

    dapIR: DapIR;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dapIRService: DapIRService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDapIRS();
    }

    load(id) {
        this.dapIRService.find(id).subscribe((dapIR) => {
            this.dapIR = dapIR;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDapIRS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dapIRListModification',
            (response) => this.load(this.dapIR.id)
        );
    }
}
