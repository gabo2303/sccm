export * from './dap-ir.model';
export * from './dap-ir-popup.service';
export * from './dap-ir.service';
export * from './dap-ir-dialog.component';
export * from './dap-ir-delete-dialog.component';
export * from './dap-ir-detail.component';
export * from './dap-ir.component';
export * from './dap-ir.route';
