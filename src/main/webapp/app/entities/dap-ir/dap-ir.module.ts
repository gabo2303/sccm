import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    DapIRComponent,
    DapIRDeleteDialogComponent,
    DapIRDeletePopupComponent,
    DapIRDetailComponent,
    DapIRDialogComponent,
    DapIRPopupComponent,
    dapIRPopupRoute,
    DapIRPopupService,
    DapIRResolvePagingParams,
    dapIRRoute,
    DapIRService,
} from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...dapIRRoute,
    ...dapIRPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        DapIRComponent,
        DapIRDetailComponent,
        DapIRDialogComponent,
        DapIRDeleteDialogComponent,
        DapIRPopupComponent,
        DapIRDeletePopupComponent,
    ],
    entryComponents: [
        DapIRComponent,
        DapIRDialogComponent,
        DapIRPopupComponent,
        DapIRDeleteDialogComponent,
        DapIRDeletePopupComponent,
    ],
    providers: [
        DapIRService,
        DapIRPopupService,
        DapIRResolvePagingParams,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmDapIRModule {
}
