
export class DapIrDto {
    constructor(
        public folio?: string,
        public codMon?: number,
        public fechaPago?: any,
        public intReal?: any,
        public intRealCal?: any,
        public diferencia?: any,
    ) {
    }
}
