import { BaseEntity } from './../../shared';

export class DapIR implements BaseEntity {
    constructor(
        public id?: number,
        public folio?: string,
        public codMon?: number,
        public fechaPago?: any,
        public intReal?: number,
        public intRealCal?: number,
        public diferencia?: number,
        public annoTributa?: number,
    ) {
    }
}
