import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { DapIR } from './dap-ir.model';
import { DapIRService } from './dap-ir.service';
import { CommonServices, ITEMS_PER_PAGE, PATTERN_DATE_SHORT, Principal, ResponseWrapper, SELECT_BLANK_OPTION } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ExcelService } from '../../shared/excel/ExcelService';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { TablaMoneda } from '../tabla-moneda/tabla-moneda.model';
import { TablaMonedaService } from '../tabla-moneda/tabla-moneda.service';
import { isNullOrUndefined } from 'util';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-dap-ir',
    templateUrl: './dap-ir.component.html',
    styleUrls: [
        'dap-ir.css'
    ]
})
export class DapIRComponent implements OnInit, OnDestroy {

    dapIRS: DapIR[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    tablaMonedas: TablaMoneda[];

    annoSelected: number;
    dapIrsFlag = false;
    dapIrsFlagSinData = false;
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    selectedCurrencyType: number = null;
    SELECT_BLANK_OPTION = SELECT_BLANK_OPTION;

    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(private dapIRService: DapIRService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService, private commonServices: CommonServices, private router: Router,
                private excelService: ExcelService, private tablaMonedaService: TablaMonedaService, private parseLinks: JhiParseLinks, private activatedRoute: ActivatedRoute) {

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
    }

    loadAll() {
        this.spinnerService.show();
        this.dapIRService.findByAllByTaxYearIdCurrencyPaginated(this.annoSelected, this.selectedCurrencyType, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        this.spinnerService.show();
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate([ 'intRealDap' ], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage
                }
        });
        this.loadAll();
    }

    ngOnInit() {
        // this.loadAll();
        this.loadAllTablaMoneda();
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDapIRS();
    }

    loadAllTablaMoneda() {
        this.tablaMonedaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tablaMonedas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DapIR) {
        return item.id;
    }

    registerChangeInDapIRS() {
        this.eventSubscriber = this.eventManager.subscribe('dapIRListModification', (response) => this.loadAll());
    }

    filterByAnnoTributarioAndCurrency(annoTributaId: HTMLSelectElement, selectedCurrencyType: HTMLSelectElement) {
        this.selectedCurrencyType = null;
        if (selectedCurrencyType.value !== this.SELECT_BLANK_OPTION) {
            this.selectedCurrencyType = parseInt(selectedCurrencyType.value, 10);
        } else {
            this.selectedCurrencyType = null;
        }
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected, this.selectedCurrencyType);
    }

    loadAllByAnnoTributaId(yearId: number, selectedCurrencyType?: number) {
        this.dapIrsFlag = false;
        this.dapIrsFlagSinData = false;
        this.spinnerService.show();
        this.dapIRService.findByAllByTaxYearIdCurrencyPaginated(yearId, selectedCurrencyType, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    generateExcel2() 
	{
		console.log("ENTRA AL METODO PARA EXTRAER DATOS");
        this.spinnerService.show();
        
		this.dapIRService.findByAllByTaxYearId(this.annoSelected).subscribe
		(
            (res: ResponseWrapper) => 
			{
				console.log("EXITO EN LA EXTRACCION DE DATOS");
				this.onSuccess(res.json, res.headers, true);				
			},

            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    generateExcel() {
        this.commonServices.generateExcel(document, 'main-data-table', this.excelService, this.eventManager, 'Cálculo de Intereses Reales DAP',
            'dapIRListModification', 'Deleted an DapIrs', this.jhiAlertService, 'No existen registros para exportar');
    }

    filterByCurrencyType(selectedCurrencyType: HTMLSelectElement) {
        this.spinnerService.show();
        if (selectedCurrencyType.value !== this.SELECT_BLANK_OPTION) {
            this.selectedCurrencyType = parseInt(selectedCurrencyType.value, 10);
        } else {
            this.selectedCurrencyType = null;
        }
        this.spinnerService.hide();
    }

    convertCodMonToStr(codMon): string {
        codMon = parseInt(codMon, 10);
        const find = this.tablaMonedas.find((elem) => {
            return elem.codMon === codMon;
        });
        if (isNullOrUndefined(find)) {
            console.log('ERROR ' + codMon);
            return '---';
        }
        return find.glosa;
    }

    sort() {
        const result = [ this.predicate + ',' + (this.reverse ? 'asc' : 'desc') ];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    clear() {
        this.page = 0;
        this.router.navigate([ 'intRealDap', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        } ]);
        this.loadAll();
    }

    private onSuccess(data, headers, downloadFile?) 
	{
        if (downloadFile) { this.commonServices.download(data, 'Cuadratura Interés Real - Depósito a Plazo'); } 
		
		else 
		{
            this.links = this.parseLinks.parse(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            // this.page = pagingParams.page;
            this.dapIRS = data;
        
			if (this.dapIRS.length > 0) { this.dapIrsFlag = true; } else { this.dapIrsFlagSinData = true; }
        }
		
        this.spinnerService.hide();
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        
		this.jhiAlertService.error(error.message, null, null);
    }
}
