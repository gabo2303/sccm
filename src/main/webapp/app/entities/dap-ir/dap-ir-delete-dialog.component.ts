import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DapIR } from './dap-ir.model';
import { DapIRPopupService } from './dap-ir-popup.service';
import { DapIRService } from './dap-ir.service';

@Component({
    selector: 'jhi-dap-ir-delete-dialog',
    templateUrl: './dap-ir-delete-dialog.component.html'
})
export class DapIRDeleteDialogComponent {

    dapIR: DapIR;

    constructor(
        private dapIRService: DapIRService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dapIRService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dapIRListModification',
                content: 'Deleted an dapIR'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dap-ir-delete-popup',
    template: ''
})
export class DapIRDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dapIRPopupService: DapIRPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dapIRPopupService
                .open(DapIRDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
