export class AhorroCuaCont {
    constructor(
        public id?: number,
        public sucursal?: number,
        public mes?: string,
        public contabilidad?: number,
        public interfazAhorro?: number,
        public diferencia?: number,
        public dia1?: string,
        public dia2?: number,
        public dia3?: string,
        public annoTributa?: number,
    ) {
    }
}
