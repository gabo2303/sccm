import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import { cuaConAhorroRoute, CuaContAhorrosComponent, CuaContAhorrosService } from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...cuaConAhorroRoute
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        CuaContAhorrosComponent,
    ],
    entryComponents: [
        CuaContAhorrosComponent,
    ],
    providers: [
        CuaContAhorrosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmCuaContAhorrosModule {
}
