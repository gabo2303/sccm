import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { CuaContAhorrosComponent } from './cuaContAhorros.component';
import { SCCM_1890_USER_AHORRO } from '../../app.constants';

export const cuaConAhorroRoute: Routes = [
    {
        path: 'cuaContAho',
        component: CuaContAhorrosComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];
