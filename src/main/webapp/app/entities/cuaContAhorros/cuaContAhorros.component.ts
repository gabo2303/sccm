import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { AhorroCuaCont } from './cuaContAhorros.model';
import { CuaContAhorrosService } from './cuaContAhorros.service';
import { CommonServices, Principal, ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ExcelService } from '../../shared/excel/ExcelService';

@Component({
    selector: 'jhi-cuaContAhorros',
    templateUrl: './cuaContAhorros.component.html',
    styleUrls: [
        'cuaContAhorros.css'
    ]
})
export class CuaContAhorrosComponent implements OnInit, OnDestroy {
    ahorrosCC: AhorroCuaCont[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    pactosFlag = false;
    pactosFlagSinData = false;
    rutSelected: number;

    constructor(private ahorrosCCService: CuaContAhorrosService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService, private commonServices: CommonServices,
                private excelService: ExcelService) {
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPactos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPactos() {
        this.eventSubscriber = this.eventManager.subscribe('pactosCCListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) {
        this.rutSelected = null;
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.pactosFlag = false;
        this.pactosFlagSinData = false;
        this.spinnerService.show();
        this.ahorrosCCService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.ahorrosCC = res.json;
                if (this.ahorrosCC.length > 0) {
                    this.pactosFlag = true;
                } else {
                    this.pactosFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    filterByRut(rutSelected: any) {
        this.spinnerService.show();
        this.rutSelected = parseInt(rutSelected.value, 10);
        this.spinnerService.hide();
    }

    verifyIsDate(dateToEval: any) {
        return dateToEval.getDate() > 0 || dateToEval.getDate() < 0 || dateToEval.getDate() === 0 ?
            dateToEval.getDate() + '/' + dateToEval.getMonth() + '/' + dateToEval.getFullYear() :
            'Sin fecha válida';
    }

    generateExcel() {
        this.commonServices.generateExcel(document, 'main-data-table', this.excelService, this.eventManager, 'Cuadratura Contable',
            'ahorroListModification', 'Deleted an ahorro', this.jhiAlertService, 'No existen registros para exportar');
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

}
