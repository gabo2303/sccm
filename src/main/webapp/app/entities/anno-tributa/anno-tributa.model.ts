import { BaseEntity } from './../../shared';

export class AnnoTributa implements BaseEntity 
{
    constructor( public id?: number, public annoT?: number, public annoC?: number, public annoActivo?: number,
        public inst2S?: BaseEntity[],
        public certPagDivs?: BaseEntity[],
        public corMons?: BaseEntity[],
        public rep1S?: BaseEntity[],
        public fut1S?: BaseEntity[],
        public glosas?: BaseEntity[],
        public ent1S?: BaseEntity[],
        public sInis?: BaseEntity[],
        public emiCarts?: BaseEntity[],
        public emiCerts?: BaseEntity[],
        public ent57bs?: BaseEntity[],
        public saldos?: BaseEntity[],
        public euros?: BaseEntity[],
        public ufs?: BaseEntity[],
        public usds?: BaseEntity[],
        public monedas?: BaseEntity[],
    ) {
    }
}
