import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { AnnoTributa } from './anno-tributa.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class AnnoTributaService 
{
    private resourceUrl = SERVER_API_URL + 'api/anno-tributas';

    constructor(private http: Http) { }

    create(annoTributa: AnnoTributa): Observable<AnnoTributa> 
	{
        const copy = this.convert(annoTributa);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(annoTributa: AnnoTributa): Observable<AnnoTributa> 
	{
        const copy = this.convert(annoTributa);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AnnoTributa> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }
	
	query2(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl + '/todos-los-annios', options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[ i ])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }
	
    /**
     * Convert a returned JSON object to AnnoTributa.
     */
    private convertItemFromServer(json: any): AnnoTributa 
	{
		const entity: AnnoTributa = Object.assign(new AnnoTributa(), json);
        
		return entity;
    }

    /**
     * Convert a AnnoTributa to a JSON which can be sent to the server.
     */
    private convert(annoTributa: AnnoTributa): AnnoTributa 
	{
        const copy: AnnoTributa = Object.assign({}, annoTributa);
        
		return copy;
    }
}