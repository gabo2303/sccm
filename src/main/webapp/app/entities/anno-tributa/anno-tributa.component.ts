import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { AnnoTributa } from './anno-tributa.model';
import { AnnoTributaService } from './anno-tributa.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({ selector: 'jhi-anno-tributa', templateUrl: './anno-tributa.component.html' })
export class AnnoTributaComponent implements OnInit
{
    annoTributas: AnnoTributa[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private annoTributaService: AnnoTributaService, private jhiAlertService: JhiAlertService, 
				private eventManager: JhiEventManager, private principal: Principal) { }

    loadAll() 
	{
        this.annoTributaService.query2().subscribe(
            (res: ResponseWrapper) => 
			{ 
				this.annoTributas = res.json; 
				console.log(res.json);
			},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() 
	{
        this.loadAll();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInAnnoTributas();
    }
	
    trackId(index: number, item: AnnoTributa) { return item.id; }
	
    registerChangeInAnnoTributas() 
	{
        this.eventSubscriber = this.eventManager.subscribe('annoTributaListModification', (response) => this.loadAll());
    }
	
    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
}