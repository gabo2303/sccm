import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { AnnoTributa } from './anno-tributa.model';
import { AnnoTributaPopupService } from './anno-tributa-popup.service';
import { AnnoTributaService } from './anno-tributa.service';

@Component({
    selector: 'jhi-anno-tributa-dialog',
    templateUrl: './anno-tributa-dialog.component.html'
})
export class AnnoTributaDialogComponent implements OnInit 
{
	annosActivos: String[];
    annoTributa: AnnoTributa;
    isSaving: boolean;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private annoTributaService: AnnoTributaService,
                private eventManager: JhiEventManager) { }

    ngOnInit() 
	{
        this.isSaving = false;
		this.annosActivos = ['S', 'N'];
    }

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.isSaving = true;
		
		console.log(this.annoTributa);
		
		if (this.annoTributa.id !== undefined) 
		{
			console.log('entra update');
            this.subscribeToSaveResponse(this.annoTributaService.update(this.annoTributa));
        } 
		else 
		{
			console.log('entra create');
            this.subscribeToSaveResponse(this.annoTributaService.create(this.annoTributa));
        }
    }

    private subscribeToSaveResponse(result: Observable<AnnoTributa>) 
	{
        result.subscribe((res: AnnoTributa) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AnnoTributa) 
	{
        this.eventManager.broadcast({ name: 'annoTributaListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }
}

@Component({ selector: 'jhi-anno-tributa-popup', template: '' })
export class AnnoTributaPopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;

    constructor( private route: ActivatedRoute, private annoTributaPopupService: AnnoTributaPopupService ) { }

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => 
		{
            if (params[ 'id' ]) { this.annoTributaPopupService.open(AnnoTributaDialogComponent as Component, params[ 'id' ]); } 
			
			else { this.annoTributaPopupService.open(AnnoTributaDialogComponent as Component); }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}