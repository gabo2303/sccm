import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AnnoTributa } from './anno-tributa.model';
import { AnnoTributaPopupService } from './anno-tributa-popup.service';
import { AnnoTributaService } from './anno-tributa.service';

@Component({
    selector: 'jhi-anno-tributa-delete-dialog',
    templateUrl: './anno-tributa-delete-dialog.component.html'
})
export class AnnoTributaDeleteDialogComponent {

    annoTributa: AnnoTributa;

    constructor(private annoTributaService: AnnoTributaService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.annoTributaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'annoTributaListModification',
                content: 'Deleted an annoTributa'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-anno-tributa-delete-popup',
    template: ''
})
export class AnnoTributaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private annoTributaPopupService: AnnoTributaPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.annoTributaPopupService
            .open(AnnoTributaDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
