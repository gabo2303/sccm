import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AnnoTributa } from './anno-tributa.model';
import { AnnoTributaService } from './anno-tributa.service';

@Component({
    selector: 'jhi-anno-tributa-detail',
    templateUrl: './anno-tributa-detail.component.html'
})
export class AnnoTributaDetailComponent implements OnInit, OnDestroy {

    annoTributa: AnnoTributa;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private annoTributaService: AnnoTributaService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInAnnoTributas();
    }

    load(id) {
        this.annoTributaService.find(id).subscribe((annoTributa) => {
            this.annoTributa = annoTributa;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAnnoTributas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'annoTributaListModification',
            (response) => this.load(this.annoTributa.id)
        );
    }
}
