import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AnnoTributaComponent } from './anno-tributa.component';
import { AnnoTributaDetailComponent } from './anno-tributa-detail.component';
import { AnnoTributaPopupComponent } from './anno-tributa-dialog.component';
import { AnnoTributaDeletePopupComponent } from './anno-tributa-delete-dialog.component';

export const annoTributaRoute: Routes =
[
    {
        path: 'anno-tributa',
        component: AnnoTributaComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.annoTributa.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, 
	{
        path: 'anno-tributa/:id',
        component: AnnoTributaDetailComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.annoTributa.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const annoTributaPopupRoute: Routes =
[
    {
        path: 'anno-tributa-new',
        component: AnnoTributaPopupComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.annoTributa.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'anno-tributa/:id/edit',
        component: AnnoTributaPopupComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.annoTributa.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'anno-tributa/:id/delete',
        component: AnnoTributaDeletePopupComponent,
        data: 
		{
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.annoTributa.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];