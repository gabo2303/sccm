import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AnnoTributa } from './anno-tributa.model';
import { AnnoTributaService } from './anno-tributa.service';

@Injectable()
export class AnnoTributaPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private annoTributaService: AnnoTributaService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.annoTributaService.find(id).subscribe((annoTributa) => {
                    this.ngbModalRef = this.annoTributaModalRef(component, annoTributa);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.annoTributaModalRef(component, new AnnoTributa());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    annoTributaModalRef(component: Component, annoTributa: AnnoTributa): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.annoTributa = annoTributa;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
