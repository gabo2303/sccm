import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    AnnoTributaComponent,
    AnnoTributaDeleteDialogComponent,
    AnnoTributaDeletePopupComponent,
    AnnoTributaDetailComponent,
    AnnoTributaDialogComponent,
    AnnoTributaPopupComponent,
    annoTributaPopupRoute,
    AnnoTributaPopupService,
    annoTributaRoute,
    AnnoTributaService,
} from './';

const ENTITY_STATES = [
    ...annoTributaRoute,
    ...annoTributaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AnnoTributaComponent,
        AnnoTributaDetailComponent,
        AnnoTributaDialogComponent,
        AnnoTributaDeleteDialogComponent,
        AnnoTributaPopupComponent,
        AnnoTributaDeletePopupComponent,
    ],
    entryComponents: [
        AnnoTributaComponent,
        AnnoTributaDialogComponent,
        AnnoTributaPopupComponent,
        AnnoTributaDeleteDialogComponent,
        AnnoTributaDeletePopupComponent,
    ],
    providers: [
        AnnoTributaService,
        AnnoTributaPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAnnoTributaModule {
}
