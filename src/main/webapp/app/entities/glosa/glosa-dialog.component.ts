import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Glosa } from './glosa.model';
import { GlosaPopupService } from './glosa-popup.service';
import { GlosaService } from './glosa.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { Instrumento, InstrumentoService } from '../instrumento';
import { ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-glosa-dialog',
    templateUrl: './glosa-dialog.component.html'
})
export class GlosaDialogComponent implements OnInit {

    glosa: Glosa;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    instrumentos: Instrumento[];

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private glosaService: GlosaService, private annoTributaService: AnnoTributaService,
                private instrumentoService: InstrumentoService, private eventManager: JhiEventManager, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
        .subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
        this.instrumentoService.query()
        .subscribe((res: ResponseWrapper) => {
            this.instrumentos = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.spinnerService.show();
        this.isSaving = true;
        if (this.glosa.id !== undefined) {
            this.subscribeToSaveResponse(
                this.glosaService.update(this.glosa));
        } else {
            this.subscribeToSaveResponse(
                this.glosaService.create(this.glosa));
        }
        this.spinnerService.hide();
    }

    private subscribeToSaveResponse(result: Observable<Glosa>) {
        result.subscribe((res: Glosa) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Glosa) {
        this.eventManager.broadcast({ name: 'glosaListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackInstrumentoById(index: number, item: Instrumento) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-glosa-popup',
    template: ''
})
export class GlosaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private glosaPopupService: GlosaPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.glosaPopupService
                .open(GlosaDialogComponent as Component, params[ 'id' ]);
            } else {
                this.glosaPopupService
                .open(GlosaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
