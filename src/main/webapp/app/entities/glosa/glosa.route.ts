import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { GlosaComponent } from './glosa.component';
import { GlosaDetailComponent } from './glosa-detail.component';
import { GlosaPopupComponent } from './glosa-dialog.component';
import { GlosaDeletePopupComponent } from './glosa-delete-dialog.component';
import { SCCM_1890_ADMIN, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1944_ADMIN, SCCM_1944_USER } from '../../app.constants';

export const glosaRoute: Routes = [
    {
        path: 'glosa-57-b/:idCertificate/show',
        component: GlosaComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'glosa-pagdiv/:idCertificate/show',
        component: GlosaComponent,
        data: {
            authorities: [ SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'glosa-oper-capt/:idCertificate/show',
        component: GlosaComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'glosa-57-b/:idCertificate/show/:id',
        component: GlosaDetailComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'glosa-pagdiv/:idCertificate/show/:id',
        component: GlosaDetailComponent,
        data: {
            authorities: [ SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'glosa-oper-capt/:idCertificate/show/:id',
        component: GlosaDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const glosaPopupRoute: Routes = [
    {
        path: 'glosa-new',
        component: GlosaPopupComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER, SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }, {
        path: 'glosa/:id/edit',
        component: GlosaPopupComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER, SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }, {
        path: 'glosa/:id/delete',
        component: GlosaDeletePopupComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER, SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.glosa.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
