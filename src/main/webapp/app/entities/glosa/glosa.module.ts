import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    GlosaService,
    GlosaPopupService,
    GlosaComponent,
    GlosaDetailComponent,
    GlosaDialogComponent,
    GlosaPopupComponent,
    GlosaDeletePopupComponent,
    GlosaDeleteDialogComponent,
    glosaRoute,
    glosaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...glosaRoute,
    ...glosaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        GlosaComponent,
        GlosaDetailComponent,
        GlosaDialogComponent,
        GlosaDeleteDialogComponent,
        GlosaPopupComponent,
        GlosaDeletePopupComponent,
    ],
    entryComponents: [
        GlosaComponent,
        GlosaDialogComponent,
        GlosaPopupComponent,
        GlosaDeleteDialogComponent,
        GlosaDeletePopupComponent,
    ],
    providers: [
        GlosaService,
        GlosaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmGlosaModule {}
