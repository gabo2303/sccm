import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Glosa } from './glosa.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class GlosaService 
{
    private resourceUrl = SERVER_API_URL + 'api/glosas';

    constructor(private http: Http) { }

    create(glosa: Glosa): Observable<Glosa> 
	{
        const copy = this.convert(glosa);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	createGlosa(glosa: Glosa): Observable<Glosa> 
	{
        const copy = this.convert(glosa);
        
		return this.http.post(this.resourceUrl + "/crearGlosa", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(glosa: Glosa): Observable<Glosa> 
	{
        const copy = this.convert(glosa);
        
		return this.http.put(this.resourceUrl, copy).map( (res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updateGlosa(glosa: Glosa): Observable<Glosa> 
	{
        const copy = this.convert(glosa);
        
		return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Glosa> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }
	
	deleteGlosa(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Glosa.
     */
    private convertItemFromServer(json: any): Glosa 
	{
        const entity: Glosa = Object.assign(new Glosa(), json);
        
		return entity;
    }

    /**
     * Convert a Glosa to a JSON which can be sent to the server.
     */
    private convert(glosa: Glosa): Glosa 
	{
        const copy: Glosa = Object.assign({}, glosa);
        
		return copy;
    }

    loadAllByAnnoTributaIdAndInsttumentoId(idAnno: number, idInst: number): Observable<ResponseWrapper> 
	{
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/${idInst}`).map((res: Response) => this.convertResponse(res));
    }
}