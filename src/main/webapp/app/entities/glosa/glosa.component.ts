import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Glosa } from './glosa.model';
import { GlosaService } from './glosa.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { InstrumentoService } from '../instrumento/instrumento.service';
import { Rut_tempService } from '../rut-temp/rut-temp.service';
import { Instrumento } from '../instrumento/instrumento.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { HttpErrorResponse } from '@angular/common/http';

declare const $;

@Component({
    selector: 'jhi-glosa',
    templateUrl: './glosa.component.html'
})
export class GlosaComponent implements OnInit, OnDestroy {
    glosas: Glosa[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    glosasFlag = false;
    productId: number;
    instrumentsList: Instrumento [];
    glosasFlagSinData = false;
	canUse: boolean;
	flagResult = false;
	resultMsg = '';
    resultType = '';
    classType = '';
	
    constructor(private glosaService: GlosaService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, 
				private principal: Principal, private annoTributaService: AnnoTributaService, private route: ActivatedRoute, 
				private instrumentoService: InstrumentoService, private spinnerService: Ng4LoadingSpinnerService, 
				private rutTempService: Rut_tempService) { }

    loadAll() 
	{	
        this.glosaService.query().subscribe
		(
            (res: ResponseWrapper) => { this.glosas = res.json; },
			
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }

    ngOnInit() 
	{
		this.rutTempService.query().subscribe
		(
			(res: ResponseWrapper) => 
			{
				if(res.json.length != 0) 
				{
					this.canUse = false;
					
					this.flagResult = true;
					this.resultMsg = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
					this.resultType = 'Info: ';
					this.classType = 'alert alert-info alert-dismissible';
				}
				else { this.canUse = true; }
			},
			(res: ResponseWrapper) => {  }
		);
		
        this.loadAnnosTributarios();
        this.loadAllInstrumentos();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInGlosas();
        this.route.params.subscribe((params) => { this.productId = parseInt(params[ 'idCertificate' ], 10); });
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: Glosa) { return item.id; }     

    registerChangeInGlosas() 
	{
        this.glosasFlag = false;
        this.glosasFlagSinData = false;
        this.eventSubscriber = this.eventManager.subscribe('glosaListModification', (response) => this.loadAllGlosasByAnnoTributaIdAndInstrumentoId());
    }

    loadAnnosTributarios() 
	{
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    filterByAnooTributario(annoTributaId: any) 
	{
        this.glosasFlag = false;
        this.glosasFlagSinData = false;
        this.spinnerService.show();
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllGlosasByAnnoTributaIdAndInstrumentoId();
    }

    loadAllInstrumentos() 
	{
        this.instrumentoService.query().subscribe
		(
            (res: ResponseWrapper) => { this.instrumentsList = res.json; },
			
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAllGlosasByAnnoTributaIdAndInstrumentoId() 
	{
        this.glosaService.loadAllByAnnoTributaIdAndInsttumentoId(this.annoSelected, this.productId).subscribe
		(
            (res: ResponseWrapper) => 
			{
                this.glosas = res.json;
                console.log('this.cart57B.length: ' + this.glosas.length);
                
				if (this.glosas.length > 0) { this.glosasFlag = true; } else { this.glosasFlagSinData = true; }
				
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}