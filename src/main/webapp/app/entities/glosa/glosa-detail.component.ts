import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Glosa } from './glosa.model';
import { GlosaService } from './glosa.service';

@Component({
    selector: 'jhi-glosa-detail',
    templateUrl: './glosa-detail.component.html'
})
export class GlosaDetailComponent implements OnInit, OnDestroy {

    glosa: Glosa;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private glosaService: GlosaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGlosas();
    }

    load(id) {
        this.glosaService.find(id).subscribe((glosa) => {
            this.glosa = glosa;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGlosas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'glosaListModification',
            (response) => this.load(this.glosa.id)
        );
    }
}
