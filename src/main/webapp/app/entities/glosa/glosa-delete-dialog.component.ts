import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Glosa } from './glosa.model';
import { GlosaPopupService } from './glosa-popup.service';
import { GlosaService } from './glosa.service';

@Component({
    selector: 'jhi-glosa-delete-dialog',
    templateUrl: './glosa-delete-dialog.component.html'
})
export class GlosaDeleteDialogComponent {

    glosa: Glosa;

    constructor(
        private glosaService: GlosaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.glosaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'glosaListModification',
                content: 'Deleted an glosa'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-glosa-delete-popup',
    template: ''
})
export class GlosaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private glosaPopupService: GlosaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.glosaPopupService
                .open(GlosaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
