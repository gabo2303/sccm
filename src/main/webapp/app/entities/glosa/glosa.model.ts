import { BaseEntity } from './../../shared';

export class Glosa implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public valor?: string,
        public annoTributa?: BaseEntity,
        public instrumento?: BaseEntity,
    ) {
    }
}
