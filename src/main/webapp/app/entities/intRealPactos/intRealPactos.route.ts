import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { IntRealPactosComponent } from './intRealPactos.component';
import { SCCM_1890_USER_PACTOS } from '../../app.constants';

export const intRealPactosRoute: Routes = [
    {
        path: 'intRealPac',
        component: IntRealPactosComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];
