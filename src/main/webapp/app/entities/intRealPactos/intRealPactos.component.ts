import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { PactosIR } from './intRealPactos.model';
import { IntRealPactosService } from './intRealPactos.service';
import { CommonServices, Principal, ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ExcelService } from '../../shared/excel/ExcelService';

@Component({
    selector: 'jhi-pactosIR',
    templateUrl: './intRealPactos.component.html',
    styleUrls: [
        'intRealPactos.css'
    ]
})
export class IntRealPactosComponent implements OnInit, OnDestroy {
    pactosIR: PactosIR[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    pactosFlag = false;
    pactosFlagSinData = false;
    rutSelected: number;

    @ViewChild('selectRutFilter') selectRutFilter: HTMLSelectElement;

    constructor(private pactosIRService: IntRealPactosService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService, private commonServices: CommonServices,
                private excelService: ExcelService) {
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPactos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PactosIR) {
        return item.id;
    }

    registerChangeInPactos() {
        this.eventSubscriber = this.eventManager.subscribe('pactosIRListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) {
        this.rutSelected = null;
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) 
	{
        this.pactosFlag = false;
        this.pactosFlagSinData = false;
        this.spinnerService.show();
        this.pactosIRService.loadAllByAnnoTributaId(annoId).subscribe
		(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    filterByRut() {
        this.spinnerService.show();
        this.rutSelected = parseInt(this.selectRutFilter.nativeElement.value, 10);
        this.spinnerService.hide();
    }

    verifyIsDate(dateToEval: any) {
        return dateToEval.getDate() > 0 || dateToEval.getDate() < 0 || dateToEval.getDate() === 0 ?
            dateToEval.getDate() + '/' + dateToEval.getMonth() + '/' + dateToEval.getFullYear() :
            'Sin fecha válida';
    }

    generateExcel() {
        this.commonServices.generateExcel(document, 'main-data-table', this.excelService, this.eventManager, 'Cuadratura Interés Real',
            'ahorroListModification', 'Deleted an ahorro', this.jhiAlertService, 'No existen registros para exportar');
    }

	private onSuccess(data, headers) 
	{		
		console.log('this.pactosIR.length: ' + data.length);
		
		if (data.length > 0) 
		{
			this.pactosFlag = true;
						   
			for(let i = 0; i < data.length; i++)
			{
				var fechaPago = new Date(data[i].fechaPago);
				var fechaInv = new Date(data[i].fechaInv);
				var fechaCont = new Date(data[i].fechaCont);
				
				data[i].fechaPago = fechaPago.setDate(fechaPago.getDate() + 1);
				data[i].fechaInv = fechaInv.setDate(fechaInv.getDate() + 1);
				data[i].fechaCont = fechaCont.setDate(fechaCont.getDate() + 1);
			}
		}
		else { this.pactosFlagSinData = true; }
		
		this.spinnerService.hide();
        
        this.pactosIR = data;
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

}
