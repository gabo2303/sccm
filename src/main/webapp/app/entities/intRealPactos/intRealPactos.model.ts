export class PactosIR {
    constructor(
        public id?: number,
        public rut?: number,
        public dv?: string,
        public folio?: string,
        public monPac?: string,
        public fechaPago?: any,
        public fechaInv?: any,
        public intReal?: number,
        public intRealCal?: number,
        public diferencia?: number,
        public fechaCont?: any,
        public annoTributa?: number,
    ) {
    }
}
