import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TipoAlerta } from './tipo-alerta.model';
import { TipoAlertaService } from './tipo-alerta.service';

@Component({
    selector: 'jhi-tipo-alerta-detail',
    templateUrl: './tipo-alerta-detail.component.html'
})
export class TipoAlertaDetailComponent implements OnInit, OnDestroy {

    tipoAlerta: TipoAlerta;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tipoAlertaService: TipoAlertaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTipoAlertas();
    }

    load(id) {
        this.tipoAlertaService.find(id).subscribe((tipoAlerta) => {
            this.tipoAlerta = tipoAlerta;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTipoAlertas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tipoAlertaListModification',
            (response) => this.load(this.tipoAlerta.id)
        );
    }
}
