import { BaseEntity } from './../../shared';

export class TipoAlerta implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public codigoUnico?: string,
        public esDap?: boolean,
    ) {
        this.esDap = false;
    }
}
