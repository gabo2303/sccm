import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { TipoAlertaComponent } from './tipo-alerta.component';
import { TipoAlertaDetailComponent } from './tipo-alerta-detail.component';
import { TipoAlertaPopupComponent } from './tipo-alerta-dialog.component';
import { TipoAlertaDeletePopupComponent } from './tipo-alerta-delete-dialog.component';

export const tipoAlertaRoute: Routes = [
    {
        path: 'tipo-alerta',
        component: TipoAlertaComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.tipoAlerta.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'tipo-alerta/:id',
        component: TipoAlertaDetailComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.tipoAlerta.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const tipoAlertaPopupRoute: Routes = [
    {
        path: 'tipo-alerta-new',
        component: TipoAlertaPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.tipoAlerta.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'tipo-alerta/:id/edit',
        component: TipoAlertaPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.tipoAlerta.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'tipo-alerta/:id/delete',
        component: TipoAlertaDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.tipoAlerta.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
