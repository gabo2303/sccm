export * from './tipo-alerta.model';
export * from './tipo-alerta-popup.service';
export * from './tipo-alerta.service';
export * from './tipo-alerta-dialog.component';
export * from './tipo-alerta-delete-dialog.component';
export * from './tipo-alerta-detail.component';
export * from './tipo-alerta.component';
export * from './tipo-alerta.route';
