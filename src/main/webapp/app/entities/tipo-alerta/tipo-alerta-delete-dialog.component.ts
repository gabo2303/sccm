import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TipoAlerta } from './tipo-alerta.model';
import { TipoAlertaPopupService } from './tipo-alerta-popup.service';
import { TipoAlertaService } from './tipo-alerta.service';

@Component({
    selector: 'jhi-tipo-alerta-delete-dialog',
    templateUrl: './tipo-alerta-delete-dialog.component.html'
})
export class TipoAlertaDeleteDialogComponent {

    tipoAlerta: TipoAlerta;

    constructor(
        private tipoAlertaService: TipoAlertaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tipoAlertaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tipoAlertaListModification',
                content: 'Deleted an tipoAlerta'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tipo-alerta-delete-popup',
    template: ''
})
export class TipoAlertaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tipoAlertaPopupService: TipoAlertaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tipoAlertaPopupService
                .open(TipoAlertaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
