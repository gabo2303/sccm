import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    TipoAlertaService,
    TipoAlertaPopupService,
    TipoAlertaComponent,
    TipoAlertaDetailComponent,
    TipoAlertaDialogComponent,
    TipoAlertaPopupComponent,
    TipoAlertaDeletePopupComponent,
    TipoAlertaDeleteDialogComponent,
    tipoAlertaRoute,
    tipoAlertaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tipoAlertaRoute,
    ...tipoAlertaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TipoAlertaComponent,
        TipoAlertaDetailComponent,
        TipoAlertaDialogComponent,
        TipoAlertaDeleteDialogComponent,
        TipoAlertaPopupComponent,
        TipoAlertaDeletePopupComponent,
    ],
    entryComponents: [
        TipoAlertaComponent,
        TipoAlertaDialogComponent,
        TipoAlertaPopupComponent,
        TipoAlertaDeleteDialogComponent,
        TipoAlertaDeletePopupComponent,
    ],
    providers: [
        TipoAlertaService,
        TipoAlertaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmTipoAlertaModule {}
