import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TipoAlerta } from './tipo-alerta.model';
import { TipoAlertaPopupService } from './tipo-alerta-popup.service';
import { TipoAlertaService } from './tipo-alerta.service';

@Component({
    selector: 'jhi-tipo-alerta-dialog',
    templateUrl: './tipo-alerta-dialog.component.html'
})
export class TipoAlertaDialogComponent implements OnInit {

    tipoAlerta: TipoAlerta;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tipoAlertaService: TipoAlertaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tipoAlerta.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tipoAlertaService.update(this.tipoAlerta));
        } else {
            this.subscribeToSaveResponse(
                this.tipoAlertaService.create(this.tipoAlerta));
        }
    }

    private subscribeToSaveResponse(result: Observable<TipoAlerta>) {
        result.subscribe((res: TipoAlerta) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TipoAlerta) {
        this.eventManager.broadcast({ name: 'tipoAlertaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-tipo-alerta-popup',
    template: ''
})
export class TipoAlertaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tipoAlertaPopupService: TipoAlertaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tipoAlertaPopupService
                    .open(TipoAlertaDialogComponent as Component, params['id']);
            } else {
                this.tipoAlertaPopupService
                    .open(TipoAlertaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
