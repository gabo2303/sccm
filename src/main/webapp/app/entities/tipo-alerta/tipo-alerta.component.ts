import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { TipoAlerta } from './tipo-alerta.model';
import { TipoAlertaService } from './tipo-alerta.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-tipo-alerta',
    templateUrl: './tipo-alerta.component.html'
})
export class TipoAlertaComponent implements OnInit, OnDestroy {
tipoAlertas: TipoAlerta[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private tipoAlertaService: TipoAlertaService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.tipoAlertaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tipoAlertas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTipoAlertas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TipoAlerta) {
        return item.id;
    }
    registerChangeInTipoAlertas() {
        this.eventSubscriber = this.eventManager.subscribe('tipoAlertaListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
