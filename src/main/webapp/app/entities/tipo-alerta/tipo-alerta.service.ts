import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TipoAlerta } from './tipo-alerta.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { isNullOrUndefined } from 'util';

@Injectable()
export class TipoAlertaService {

    private resourceUrl = SERVER_API_URL + 'api/tipo-alertas';

    constructor(private http: Http) {
    }

    create(tipoAlerta: TipoAlerta): Observable<TipoAlerta> {
        const copy = this.convert(tipoAlerta);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(tipoAlerta: TipoAlerta): Observable<TipoAlerta> {
        const copy = this.convert(tipoAlerta);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TipoAlerta> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(isDap?: boolean, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + (isNullOrUndefined(isDap) ? '' : '?isDap=' + isDap), options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TipoAlerta.
     */
    private convertItemFromServer(json: any): TipoAlerta {
        const entity: TipoAlerta = Object.assign(new TipoAlerta(), json);
        return entity;
    }

    /**
     * Convert a TipoAlerta to a JSON which can be sent to the server.
     */
    private convert(tipoAlerta: TipoAlerta): TipoAlerta {
        const copy: TipoAlerta = Object.assign({}, tipoAlerta);
        return copy;
    }
}
