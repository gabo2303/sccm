import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Euro } from './euro.model';
import { EuroPopupService } from './euro-popup.service';
import { EuroService } from './euro.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-euro-dialog',
    templateUrl: './euro-dialog.component.html'
})
export class EuroDialogComponent implements OnInit {

    euro: Euro;
    isSaving: boolean;

    annotributas: AnnoTributa[];
    fechaDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private euroService: EuroService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.euro.id !== undefined) {
            this.subscribeToSaveResponse(
                this.euroService.update(this.euro));
        } else {
            this.subscribeToSaveResponse(
                this.euroService.create(this.euro));
        }
    }

    private subscribeToSaveResponse(result: Observable<Euro>) {
        result.subscribe((res: Euro) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Euro) {
        this.eventManager.broadcast({ name: 'euroListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-euro-popup',
    template: ''
})
export class EuroPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private euroPopupService: EuroPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.euroPopupService
                    .open(EuroDialogComponent as Component, params['id']);
            } else {
                this.euroPopupService
                    .open(EuroDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
