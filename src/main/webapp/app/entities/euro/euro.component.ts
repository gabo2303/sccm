import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Euro } from './euro.model';
import { EuroService } from './euro.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-euro',
    templateUrl: './euro.component.html'
})
export class EuroComponent implements OnInit, OnDestroy {
    euros: Euro[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private euroService: EuroService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal) {
    }

    loadAll() {
        this.euroService.query().subscribe(
            (res: ResponseWrapper) => {
                this.euros = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEuros();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Euro) {
        return item.id;
    }

    registerChangeInEuros() {
        this.eventSubscriber = this.eventManager.subscribe('euroListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
