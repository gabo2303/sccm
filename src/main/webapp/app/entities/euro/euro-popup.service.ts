import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Euro } from './euro.model';
import { EuroService } from './euro.service';

@Injectable()
export class EuroPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private euroService: EuroService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.euroService.find(id).subscribe((euro) => {
                    if (euro.fecha) {
                        euro.fecha = {
                            year: euro.fecha.getFullYear(),
                            month: euro.fecha.getMonth() + 1,
                            day: euro.fecha.getDate()
                        };
                    }
                    this.ngbModalRef = this.euroModalRef(component, euro);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.euroModalRef(component, new Euro());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    euroModalRef(component: Component, euro: Euro): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.euro = euro;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
