import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Euro } from './euro.model';
import { EuroPopupService } from './euro-popup.service';
import { EuroService } from './euro.service';

@Component({
    selector: 'jhi-euro-delete-dialog',
    templateUrl: './euro-delete-dialog.component.html'
})
export class EuroDeleteDialogComponent {

    euro: Euro;

    constructor(
        private euroService: EuroService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.euroService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'euroListModification',
                content: 'Deleted an euro'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-euro-delete-popup',
    template: ''
})
export class EuroDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private euroPopupService: EuroPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.euroPopupService
                .open(EuroDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
