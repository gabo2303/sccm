import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EuroComponent } from './euro.component';
import { EuroDetailComponent } from './euro-detail.component';
import { EuroPopupComponent } from './euro-dialog.component';
import { EuroDeletePopupComponent } from './euro-delete-dialog.component';

export const euroRoute: Routes = [
    {
        path: 'euro',
        component: EuroComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.euro.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'euro/:id',
        component: EuroDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.euro.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const euroPopupRoute: Routes = [
    {
        path: 'euro-new',
        component: EuroPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.euro.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'euro/:id/edit',
        component: EuroPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.euro.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'euro/:id/delete',
        component: EuroDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.euro.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
