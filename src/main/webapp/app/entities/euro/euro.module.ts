import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    EuroService,
    EuroPopupService,
    EuroComponent,
    EuroDetailComponent,
    EuroDialogComponent,
    EuroPopupComponent,
    EuroDeletePopupComponent,
    EuroDeleteDialogComponent,
    euroRoute,
    euroPopupRoute,
} from './';

const ENTITY_STATES = [
    ...euroRoute,
    ...euroPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EuroComponent,
        EuroDetailComponent,
        EuroDialogComponent,
        EuroDeleteDialogComponent,
        EuroPopupComponent,
        EuroDeletePopupComponent,
    ],
    entryComponents: [
        EuroComponent,
        EuroDialogComponent,
        EuroPopupComponent,
        EuroDeleteDialogComponent,
        EuroDeletePopupComponent,
    ],
    providers: [
        EuroService,
        EuroPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmEuroModule {}
