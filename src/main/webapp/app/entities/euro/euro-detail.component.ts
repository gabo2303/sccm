import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Euro } from './euro.model';
import { EuroService } from './euro.service';

@Component({
    selector: 'jhi-euro-detail',
    templateUrl: './euro-detail.component.html'
})
export class EuroDetailComponent implements OnInit, OnDestroy {

    euro: Euro;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private euroService: EuroService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEuros();
    }

    load(id) {
        this.euroService.find(id).subscribe((euro) => {
            this.euro = euro;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEuros() {
        this.eventSubscriber = this.eventManager.subscribe(
            'euroListModification',
            (response) => this.load(this.euro.id)
        );
    }
}
