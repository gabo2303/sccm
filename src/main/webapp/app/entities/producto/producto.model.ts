import { BaseEntity } from './../../shared';

export class Producto implements BaseEntity {
    constructor(
        public id?: number,
        public nombreProducto?: string,
        public usuarioProducto?: string,
        public adminProducto?: string,
        public tituloCertificado?: string,
        public insts?: BaseEntity[],
    ) {
    }
}
