import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CertVersion } from './cert-version.model';
import { CertVersionPopupService } from './cert-version-popup.service';
import { CertVersionService } from './cert-version.service';

@Component({
    selector: 'jhi-cert-version-delete-dialog',
    templateUrl: './cert-version-delete-dialog.component.html'
})
export class CertVersionDeleteDialogComponent {

    certVersion: CertVersion;

    constructor(
        private certVersionService: CertVersionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.certVersionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'certVersionListModification',
                content: 'Deleted an certVersion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cert-version-delete-popup',
    template: ''
})
export class CertVersionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private certVersionPopupService: CertVersionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.certVersionPopupService
                .open(CertVersionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
