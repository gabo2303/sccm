import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CertVersion } from './cert-version.model';
import { CertVersionPopupService } from './cert-version-popup.service';
import { CertVersionService } from './cert-version.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ClienteBice, ClienteBiceService } from '../cliente-bice';
import { Instrumento, InstrumentoService } from '../instrumento';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cert-version-dialog',
    templateUrl: './cert-version-dialog.component.html'
})
export class CertVersionDialogComponent implements OnInit {

    certVersion: CertVersion;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    clientebices: ClienteBice[];

    instrumentos: Instrumento[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private certVersionService: CertVersionService,
        private annoTributaService: AnnoTributaService,
        private clienteBiceService: ClienteBiceService,
        private instrumentoService: InstrumentoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clienteBiceService.query()
            .subscribe((res: ResponseWrapper) => { this.clientebices = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.instrumentoService.query()
            .subscribe((res: ResponseWrapper) => { this.instrumentos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.certVersion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.certVersionService.update(this.certVersion));
        } else {
            this.subscribeToSaveResponse(
                this.certVersionService.create(this.certVersion));
        }
    }

    private subscribeToSaveResponse(result: Observable<CertVersion>) {
        result.subscribe((res: CertVersion) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CertVersion) {
        this.eventManager.broadcast({ name: 'certVersionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackClienteBiceById(index: number, item: ClienteBice) {
        return item.id;
    }

    trackInstrumentoById(index: number, item: Instrumento) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-cert-version-popup',
    template: ''
})
export class CertVersionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private certVersionPopupService: CertVersionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.certVersionPopupService
                    .open(CertVersionDialogComponent as Component, params['id']);
            } else {
                this.certVersionPopupService
                    .open(CertVersionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
