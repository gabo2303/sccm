import { BaseEntity } from './../../shared';

export class CertVersion implements BaseEntity {
    constructor(
        public id?: number,
        public nroCertificado?: number,
        public annoTributa?: BaseEntity,
        public clienteBice?: BaseEntity,
        public instrumento?: BaseEntity,
    ) {
    }
}
