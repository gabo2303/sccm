import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CertVersion } from './cert-version.model';
import { CertVersionService } from './cert-version.service';

@Component({
    selector: 'jhi-cert-version-detail',
    templateUrl: './cert-version-detail.component.html'
})
export class CertVersionDetailComponent implements OnInit, OnDestroy {

    certVersion: CertVersion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private certVersionService: CertVersionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCertVersions();
    }

    load(id) {
        this.certVersionService.find(id).subscribe((certVersion) => {
            this.certVersion = certVersion;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCertVersions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'certVersionListModification',
            (response) => this.load(this.certVersion.id)
        );
    }
}
