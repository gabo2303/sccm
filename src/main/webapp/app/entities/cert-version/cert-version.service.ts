import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { CertVersion } from './cert-version.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CertVersionService {

    private resourceUrl = SERVER_API_URL + 'api/cert-versions';

    constructor(private http: Http) { }

    create(certVersion: CertVersion): Observable<CertVersion> {
        const copy = this.convert(certVersion);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(certVersion: CertVersion): Observable<CertVersion> {
        const copy = this.convert(certVersion);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CertVersion> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CertVersion.
     */
    private convertItemFromServer(json: any): CertVersion {
        const entity: CertVersion = Object.assign(new CertVersion(), json);
        return entity;
    }

    /**
     * Convert a CertVersion to a JSON which can be sent to the server.
     */
    private convert(certVersion: CertVersion): CertVersion {
        const copy: CertVersion = Object.assign({}, certVersion);
        return copy;
    }
}
