import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CertVersionComponent } from './cert-version.component';
import { CertVersionDetailComponent } from './cert-version-detail.component';
import { CertVersionPopupComponent } from './cert-version-dialog.component';
import { CertVersionDeletePopupComponent } from './cert-version-delete-dialog.component';

export const certVersionRoute: Routes = [
    {
        path: 'cert-version',
        component: CertVersionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certVersion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cert-version/:id',
        component: CertVersionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certVersion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const certVersionPopupRoute: Routes = [
    {
        path: 'cert-version-new',
        component: CertVersionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cert-version/:id/edit',
        component: CertVersionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cert-version/:id/delete',
        component: CertVersionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
