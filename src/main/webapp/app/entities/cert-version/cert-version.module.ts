import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    CertVersionService,
    CertVersionPopupService,
    CertVersionComponent,
    CertVersionDetailComponent,
    CertVersionDialogComponent,
    CertVersionPopupComponent,
    CertVersionDeletePopupComponent,
    CertVersionDeleteDialogComponent,
    certVersionRoute,
    certVersionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...certVersionRoute,
    ...certVersionPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CertVersionComponent,
        CertVersionDetailComponent,
        CertVersionDialogComponent,
        CertVersionDeleteDialogComponent,
        CertVersionPopupComponent,
        CertVersionDeletePopupComponent,
    ],
    entryComponents: [
        CertVersionComponent,
        CertVersionDialogComponent,
        CertVersionPopupComponent,
        CertVersionDeleteDialogComponent,
        CertVersionDeletePopupComponent,
    ],
    providers: [
        CertVersionService,
        CertVersionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmCertVersionModule {}
