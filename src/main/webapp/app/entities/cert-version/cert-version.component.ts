import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { CertVersion } from './cert-version.model';
import { CertVersionService } from './cert-version.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cert-version',
    templateUrl: './cert-version.component.html'
})
export class CertVersionComponent implements OnInit, OnDestroy {
certVersions: CertVersion[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private certVersionService: CertVersionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.certVersionService.query().subscribe(
            (res: ResponseWrapper) => {
                this.certVersions = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCertVersions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CertVersion) {
        return item.id;
    }
    registerChangeInCertVersions() {
        this.eventSubscriber = this.eventManager.subscribe('certVersionListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
