import { BaseEntity } from './../../shared';

export class Cart57B implements BaseEntity {
    constructor(
        public id?: number,
        public marca?: string,
        public tipo?: string,
        public fecha?: any,
        public monto?: number,
        public factorAct?: number,
        public montoActualizado?: number,
        public meses?: number,
        public montoProporcional?: number,
        public montoProporcionalSig?: number,
        public interes?: number,
        public interesActualizado?: number,
        public annoTributa?: BaseEntity,
        public clienteBice?: BaseEntity,
    ) {
    }
}
