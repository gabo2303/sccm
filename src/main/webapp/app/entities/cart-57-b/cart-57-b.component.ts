import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Cart57B } from './cart-57-b.model';
import { Cart57BService } from './cart-57-b.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { ClienteBice } from '../cliente-bice/cliente-bice.model';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ClienteBiceService } from '../cliente-bice/cliente-bice.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ExcelService } from '../../shared/excel/ExcelService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-cart-57-b',
    templateUrl: './cart-57-b.component.html'
})
export class Cart57BComponent implements OnInit, OnDestroy {
    cart57BS: Cart57B[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    cart57BFlag = false;
    clientesList: ClienteBice [];
    rutFromCart57B: Cart57B [];
    rutSelected: string;
    flagTable = false;
    cart57BFlagSinData = false;

    constructor(private cart57BService: Cart57BService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private clientesService: ClienteBiceService, private excelService: ExcelService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    loadAll() {
        this.cart57BService.query().subscribe(
            (res: ResponseWrapper) => {
                this.cart57BS = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadAllClientes();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCart57BS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Cart57B) {
        return item.id;
    }

    registerChangeInCart57BS() {
        this.eventSubscriber = this.eventManager.subscribe('cart57BListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    filterByAnooTributario(annoTributaId: any) {
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);

    }

    loadAllClientes() {
        this.clientesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientesList = res.json;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    generateExcel() {
        const rowCount = (<HTMLTableElement> document.getElementById('table')).rows.length;
        if (rowCount > 1) {
            let textos = '[';
            const headers: any [] = [];

            const table = (<HTMLTableElement> document.getElementById('table'));
            for (let i = 0; i < (<HTMLTableElement> document.getElementById('table')).rows.length; i++) {
                if (i > 0) {
                    textos += '{';
                }
                for (let j = 0; j < 13; j++) {
                    if (i === 0) {
                        // Obtenemos la primera fila de la tabla o cabecera y limpiamos tag html y espacios vacíos
                        headers[ j ] =
                            (<HTMLTableElement> document.getElementById('table')).rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim();
                    } else {
                        if (j < 12) {
                            textos += '"' + headers[ j ] + '" : "' +
                                (<HTMLTableElement> document.getElementById('table')).rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim()
                                + '",';
                        } else {
                            textos += '"' + headers[ j ] + '" : "' +
                                (<HTMLTableElement> document.getElementById('table')).rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim()
                                + '"';
                        }
                    }
                }
                if (i > 0 && i < (<HTMLTableElement> document.getElementById('table')).rows.length - 1) {
                    textos += '},';
                }
                if (i === (<HTMLTableElement> document.getElementById('table')).rows.length - 1) {
                    textos += '}]';
                }
            }
            this.excelService.exportAsExcelFile(
                JSON.parse(textos.toString()), 'Datos Cartola 57 Bis', 'Datos Cartola 57 Bis');
        } else {
            this.jhiAlertService.error('No existen registros para exportar');
        }
    }

    loadAllRutsFromCart57B() {
        this.cart57BService.query().subscribe(
            (res: ResponseWrapper) => {
                this.rutFromCart57B = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filtered(idAnnoSelected, rutSelected) {
        this.annoSelected = idAnnoSelected;
        this.rutSelected = rutSelected;
        this.flagTable = true;
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.cart57BFlag = false;
        this.cart57BFlagSinData = false;
        this.spinnerService.show();
        this.cart57BService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.cart57BS = res.json;
                console.log('this.cart57B.length: ' + this.cart57BS.length);
                if (this.cart57BS.length > 0) {
                    this.cart57BFlag = true;
                } else {
                    this.cart57BFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

}
