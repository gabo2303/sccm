import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Cart57BComponent,
    Cart57BDeleteDialogComponent,
    Cart57BDeletePopupComponent,
    Cart57BDetailComponent,
    Cart57BDialogComponent,
    Cart57BPopupComponent,
    cart57BPopupRoute,
    Cart57BPopupService,
    cart57BRoute,
    Cart57BService,
} from './';

const ENTITY_STATES = [
    ...cart57BRoute,
    ...cart57BPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Cart57BComponent,
        Cart57BDetailComponent,
        Cart57BDialogComponent,
        Cart57BDeleteDialogComponent,
        Cart57BPopupComponent,
        Cart57BDeletePopupComponent,
    ],
    entryComponents: [
        Cart57BComponent,
        Cart57BDialogComponent,
        Cart57BPopupComponent,
        Cart57BDeleteDialogComponent,
        Cart57BDeletePopupComponent,
    ],
    providers: [
        Cart57BService,
        Cart57BPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmCart57BModule {
}
