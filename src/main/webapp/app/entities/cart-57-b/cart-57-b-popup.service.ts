import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Cart57B } from './cart-57-b.model';
import { Cart57BService } from './cart-57-b.service';

@Injectable()
export class Cart57BPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private cart57BService: Cart57BService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cart57BService.find(id).subscribe((cart57B) => {
                    if (cart57B.fecha) {
                        cart57B.fecha = {
                            year: cart57B.fecha.getFullYear(),
                            month: cart57B.fecha.getMonth() + 1,
                            day: cart57B.fecha.getDate()
                        };
                    }
                    this.ngbModalRef = this.cart57BModalRef(component, cart57B);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cart57BModalRef(component, new Cart57B());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cart57BModalRef(component: Component, cart57B: Cart57B): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.cart57B = cart57B;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
