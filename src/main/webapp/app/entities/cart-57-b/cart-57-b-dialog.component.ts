import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Cart57B } from './cart-57-b.model';
import { Cart57BPopupService } from './cart-57-b-popup.service';
import { Cart57BService } from './cart-57-b.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ClienteBice, ClienteBiceService } from '../cliente-bice';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cart-57-b-dialog',
    templateUrl: './cart-57-b-dialog.component.html'
})
export class Cart57BDialogComponent implements OnInit {

    cart57B: Cart57B;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    clientebices: ClienteBice[];
    fechaDp: any;
    pedro = undefined;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private cart57BService: Cart57BService,
                private annoTributaService: AnnoTributaService, private clienteBiceService: ClienteBiceService, private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
        .subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
        this.clienteBiceService.query()
        .subscribe((res: ResponseWrapper) => {
            this.clientebices = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cart57B.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cart57BService.update(this.cart57B));
        } else {
            this.subscribeToSaveResponse(
                this.cart57BService.create(this.cart57B));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackClienteBiceById(index: number, item: ClienteBice) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<Cart57B>) {
        result.subscribe((res: Cart57B) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Cart57B) {
        this.eventManager.broadcast({ name: 'cart57BListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-cart-57-b-popup',
    template: ''
})
export class Cart57BPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private cart57BPopupService: Cart57BPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.cart57BPopupService
                .open(Cart57BDialogComponent as Component, params[ 'id' ]);
            } else {
                this.cart57BPopupService
                .open(Cart57BDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
