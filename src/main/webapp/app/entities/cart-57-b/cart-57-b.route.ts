import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { Cart57BComponent } from './cart-57-b.component';
import { Cart57BDetailComponent } from './cart-57-b-detail.component';
import { Cart57BPopupComponent } from './cart-57-b-dialog.component';
import { Cart57BDeletePopupComponent } from './cart-57-b-delete-dialog.component';

export const cart57BRoute: Routes = [
    {
        path: 'cart-57-b',
        component: Cart57BComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cart57B.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'cart-57-b/:id',
        component: Cart57BDetailComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cart57B.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const cart57BPopupRoute: Routes = [
    {
        path: 'cart-57-b-new',
        component: Cart57BPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cart57B.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cart-57-b/:id/edit',
        component: Cart57BPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cart57B.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cart-57-b/:id/delete',
        component: Cart57BDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cart57B.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
