import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Cart57B } from './cart-57-b.model';
import { Cart57BPopupService } from './cart-57-b-popup.service';
import { Cart57BService } from './cart-57-b.service';

@Component({
    selector: 'jhi-cart-57-b-delete-dialog',
    templateUrl: './cart-57-b-delete-dialog.component.html'
})
export class Cart57BDeleteDialogComponent {

    cart57B: Cart57B;

    constructor(private cart57BService: Cart57BService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cart57BService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cart57BListModification',
                content: 'Deleted an cart57B'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cart-57-b-delete-popup',
    template: ''
})
export class Cart57BDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private cart57BPopupService: Cart57BPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cart57BPopupService
            .open(Cart57BDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
