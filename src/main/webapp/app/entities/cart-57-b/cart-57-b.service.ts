import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Cart57B } from './cart-57-b.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class Cart57BService {

    private resourceUrl = SERVER_API_URL + 'api/cart-57-bs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    create(cart57B: Cart57B): Observable<Cart57B> {
        const copy = this.convert(cart57B);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(cart57B: Cart57B): Observable<Cart57B> {
        const copy = this.convert(cart57B);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Cart57B> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`).map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Cart57B.
     */
    private convertItemFromServer(json: any): Cart57B {
        const entity: Cart57B = Object.assign(new Cart57B(), json);
        entity.fecha = this.dateUtils.convertLocalDateFromServer(json.fecha);
        return entity;
    }

    /**
     * Convert a Cart57B to a JSON which can be sent to the server.
     */
    private convert(cart57B: Cart57B): Cart57B {
        const copy: Cart57B = Object.assign({}, cart57B);
        copy.fecha = this.dateUtils.convertLocalDateToServer(cart57B.fecha);
        return copy;
    }
}
