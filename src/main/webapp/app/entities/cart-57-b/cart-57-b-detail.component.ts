import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Cart57B } from './cart-57-b.model';
import { Cart57BService } from './cart-57-b.service';
import { ClienteBice } from '../cliente-bice/cliente-bice.model';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ClienteBiceService } from '../cliente-bice/cliente-bice.service';

@Component({
    selector: 'jhi-cart-57-b-detail',
    templateUrl: './cart-57-b-detail.component.html'
})
export class Cart57BDetailComponent implements OnInit, OnDestroy {

    cart57B: Cart57B;
    clientesList: ClienteBice [];
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private cart57BService: Cart57BService, private route: ActivatedRoute, private jhiAlertService: JhiAlertService,
                private clientesService: ClienteBiceService) {
    }

    ngOnInit() {
        this.loadAllClientes();
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInCart57BS();
    }

    load(id) {
        this.cart57BService.find(id).subscribe((cart57B) => {
            this.cart57B = cart57B;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCart57BS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cart57BListModification',
            (response) => this.load(this.cart57B.id)
        );
    }

    loadAllClientes() {
        this.clientesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientesList = res.json;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

}
