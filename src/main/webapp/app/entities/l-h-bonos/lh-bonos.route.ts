import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LHBonosComponent } from './lh-bonos.component';
import { LHBonosDetailComponent } from './lh-bonos-detail.component';
import { LHBonosPopupComponent } from './lh-bonos-dialog.component';
import { LHBonosDeletePopupComponent } from './lh-bonos-delete-dialog.component';

export const lHBonosRoute: Routes = [
    {
        path: 'lh-bonos',
        component: LHBonosComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN' ],
            pageTitle: 'sccmApp.lHBonos.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'lh-bonos/:id',
        component: LHBonosDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.lHBonos.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lHBonosPopupRoute: Routes = [
    {
        path: 'lh-bonos-new',
        component: LHBonosPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.lHBonos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lh-bonos/:id/edit',
        component: LHBonosPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.lHBonos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lh-bonos/:id/delete',
        component: LHBonosDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.lHBonos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
