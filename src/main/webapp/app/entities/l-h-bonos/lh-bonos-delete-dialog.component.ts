import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LHBonos } from './lh-bonos.model';
import { LHBonosPopupService } from './lh-bonos-popup.service';
import { LHBonosService } from './lh-bonos.service';

@Component({
    selector: 'jhi-lh-bonos-delete-dialog',
    templateUrl: './lh-bonos-delete-dialog.component.html'
})
export class LHBonosDeleteDialogComponent {

    lHBonos: LHBonos;

    constructor(private lHBonosService: LHBonosService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lHBonosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'lHBonosListModification', content: 'Deleted an lHBonos' });
            this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK' });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-lh-bonos-delete-popup',
    template: ''
})
export class LHBonosDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private lHBonosPopupService: LHBonosPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.lHBonosPopupService.open(LHBonosDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
