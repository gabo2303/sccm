import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { LHBonos } from './lh-bonos.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class LHBonosService 
{
    private resourceUrl = SERVER_API_URL + 'api/l-h-bonos';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(lHBonos: LHBonos): Observable<LHBonos> 
	{
        const copy = this.convert(lHBonos);
		
        return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(lHBonos: LHBonos): Observable<LHBonos> 
	{
        const copy = this.convert(lHBonos);
		
        return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updateLHBonos(lHBonos: LHBonos): Observable<LHBonos> 
	{
        const copy = this.convert(lHBonos);
		
        return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<LHBonos> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }
	
	deleteLHBonos(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LHBonos.
     */
    private convertItemFromServer(json: any): LHBonos 
	{
        const entity: LHBonos = Object.assign(new LHBonos(), json);
        
		entity.fechaPago = this.dateUtils.convertLocalDateFromServer(json.fechaPago);
        entity.fechaInv = this.dateUtils.convertLocalDateFromServer(json.fechaInv);
        entity.fecVcto = this.dateUtils.convertLocalDateFromServer(json.fecVcto);
        entity.fecCont = this.dateUtils.convertLocalDateFromServer(json.fecCont);
		
        return entity;
    }

    /**
     * Convert a LHBonos to a JSON which can be sent to the server.
     */
    private convert(lHBonos: LHBonos): LHBonos 
	{
        const copy: LHBonos = Object.assign({}, lHBonos);
        
		copy.fechaPago = this.dateUtils.convertLocalDateToServer(lHBonos.fechaPago);
        copy.fechaInv = this.dateUtils.convertLocalDateToServer(lHBonos.fechaInv);
        copy.fecVcto = this.dateUtils.convertLocalDateToServer(lHBonos.fecVcto);
        copy.fecCont = this.dateUtils.convertLocalDateToServer(lHBonos.fecCont);
		
        return copy;
    }
}