import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { LHBonos } from './lh-bonos.model';
import { LHBonosService } from './lh-bonos.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lh-bonos',
    templateUrl: './lh-bonos.component.html'
})
export class LHBonosComponent implements OnInit, OnDestroy {
lHBonos: LHBonos[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private lHBonosService: LHBonosService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.lHBonosService.query().subscribe(
            (res: ResponseWrapper) => {
                this.lHBonos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLHBonos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: LHBonos) {
        return item.id;
    }
    registerChangeInLHBonos() {
        this.eventSubscriber = this.eventManager.subscribe('lHBonosListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
