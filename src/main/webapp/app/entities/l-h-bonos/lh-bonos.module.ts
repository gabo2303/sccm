import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    LHBonosService,
    LHBonosPopupService,
    LHBonosComponent,
    LHBonosDetailComponent,
    LHBonosDialogComponent,
    LHBonosPopupComponent,
    LHBonosDeletePopupComponent,
    LHBonosDeleteDialogComponent,
    lHBonosRoute,
    lHBonosPopupRoute,
} from './';

const ENTITY_STATES = [
    ...lHBonosRoute,
    ...lHBonosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LHBonosComponent,
        LHBonosDetailComponent,
        LHBonosDialogComponent,
        LHBonosDeleteDialogComponent,
        LHBonosPopupComponent,
        LHBonosDeletePopupComponent,
    ],
    entryComponents: [
        LHBonosComponent,
        LHBonosDialogComponent,
        LHBonosPopupComponent,
        LHBonosDeleteDialogComponent,
        LHBonosDeletePopupComponent,
    ],
    providers: [
        LHBonosService,
        LHBonosPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmLHBonosModule {}
