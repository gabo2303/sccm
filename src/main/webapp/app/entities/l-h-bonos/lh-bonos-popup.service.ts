import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LHBonos } from './lh-bonos.model';
import { LHBonosService } from './lh-bonos.service';

@Injectable()
export class LHBonosPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private lHBonosService: LHBonosService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.lHBonosService.find(id).subscribe((lHBonos) => {
                    if (lHBonos.fechaPago) {
                        lHBonos.fechaPago = {
                            year: lHBonos.fechaPago.getFullYear(),
                            month: lHBonos.fechaPago.getMonth() + 1,
                            day: lHBonos.fechaPago.getDate()
                        };
                    }
                    if (lHBonos.fechaInv) {
                        lHBonos.fechaInv = {
                            year: lHBonos.fechaInv.getFullYear(),
                            month: lHBonos.fechaInv.getMonth() + 1,
                            day: lHBonos.fechaInv.getDate()
                        };
                    }
                    if (lHBonos.fecVcto) {
                        lHBonos.fecVcto = {
                            year: lHBonos.fecVcto.getFullYear(),
                            month: lHBonos.fecVcto.getMonth() + 1,
                            day: lHBonos.fecVcto.getDate()
                        };
                    }
                    if (lHBonos.fecCont) {
                        lHBonos.fecCont = {
                            year: lHBonos.fecCont.getFullYear(),
                            month: lHBonos.fecCont.getMonth() + 1,
                            day: lHBonos.fecCont.getDate()
                        };
                    }
                    this.ngbModalRef = this.lHBonosModalRef(component, lHBonos);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.lHBonosModalRef(component, new LHBonos());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    lHBonosModalRef(component: Component, lHBonos: LHBonos): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.lHBonos = lHBonos;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
