import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LHBonos } from './lh-bonos.model';
import { LHBonosPopupService } from './lh-bonos-popup.service';
import { LHBonosService } from './lh-bonos.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lh-bonos-dialog',
    templateUrl: './lh-bonos-dialog.component.html'
})
export class LHBonosDialogComponent implements OnInit {

    lHBonos: LHBonos;
    isSaving: boolean;

    annotributas: AnnoTributa[];
    fechaPagoDp: any;
    fechaInvDp: any;
    fecVctoDp: any;
    fecContDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private lHBonosService: LHBonosService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lHBonos.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lHBonosService.update(this.lHBonos));
        } else {
            this.subscribeToSaveResponse(
                this.lHBonosService.create(this.lHBonos));
        }
    }

    private subscribeToSaveResponse(result: Observable<LHBonos>) {
        result.subscribe((res: LHBonos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LHBonos) {
        this.eventManager.broadcast({ name: 'lHBonosListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-lh-bonos-popup',
    template: ''
})
export class LHBonosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lHBonosPopupService: LHBonosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lHBonosPopupService
                    .open(LHBonosDialogComponent as Component, params['id']);
            } else {
                this.lHBonosPopupService
                    .open(LHBonosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
