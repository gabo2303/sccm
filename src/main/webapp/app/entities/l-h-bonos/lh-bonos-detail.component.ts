import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LHBonos } from './lh-bonos.model';
import { LHBonosService } from './lh-bonos.service';

@Component({
    selector: 'jhi-lh-bonos-detail',
    templateUrl: './lh-bonos-detail.component.html'
})
export class LHBonosDetailComponent implements OnInit, OnDestroy {

    lHBonos: LHBonos;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private lHBonosService: LHBonosService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLHBonos();
    }

    load(id) {
        this.lHBonosService.find(id).subscribe((lHBonos) => {
			console.log(lHBonos);
            this.lHBonos = lHBonos;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLHBonos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'lHBonosListModification',
            (response) => this.load(this.lHBonos.id)
        );
    }
}
