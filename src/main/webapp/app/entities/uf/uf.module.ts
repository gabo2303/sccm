import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    UfService,
    UfPopupService,
    UfComponent,
    UfDetailComponent,
    UfDialogComponent,
    UfPopupComponent,
    UfDeletePopupComponent,
    UfDeleteDialogComponent,
    ufRoute,
    ufPopupRoute,
} from './';

const ENTITY_STATES = [
    ...ufRoute,
    ...ufPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UfComponent,
        UfDetailComponent,
        UfDialogComponent,
        UfDeleteDialogComponent,
        UfPopupComponent,
        UfDeletePopupComponent,
    ],
    entryComponents: [
        UfComponent,
        UfDialogComponent,
        UfPopupComponent,
        UfDeleteDialogComponent,
        UfDeletePopupComponent,
    ],
    providers: [
        UfService,
        UfPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmUfModule {}
