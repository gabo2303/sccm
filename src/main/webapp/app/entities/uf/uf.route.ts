import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UfComponent } from './uf.component';
import { UfDetailComponent } from './uf-detail.component';
import { UfPopupComponent } from './uf-dialog.component';
import { UfDeletePopupComponent } from './uf-delete-dialog.component';

export const ufRoute: Routes = [
    {
        path: 'uf',
        component: UfComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.uf.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'uf/:id',
        component: UfDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.uf.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ufPopupRoute: Routes = [
    {
        path: 'uf-new',
        component: UfPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.uf.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'uf/:id/edit',
        component: UfPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.uf.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'uf/:id/delete',
        component: UfDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.uf.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
