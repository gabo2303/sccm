import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Uf } from './uf.model';
import { UfPopupService } from './uf-popup.service';
import { UfService } from './uf.service';

@Component({
    selector: 'jhi-uf-delete-dialog',
    templateUrl: './uf-delete-dialog.component.html'
})
export class UfDeleteDialogComponent {

    uf: Uf;

    constructor(
        private ufService: UfService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ufService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ufListModification',
                content: 'Deleted an uf'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-uf-delete-popup',
    template: ''
})
export class UfDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ufPopupService: UfPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ufPopupService
                .open(UfDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
