import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Uf } from './uf.model';
import { UfService } from './uf.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-uf',
    templateUrl: './uf.component.html'
})
export class UfComponent implements OnInit, OnDestroy {
ufs: Uf[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ufService: UfService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.ufService.query().subscribe(
            (res: ResponseWrapper) => {
                this.ufs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUfs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Uf) {
        return item.id;
    }
    registerChangeInUfs() {
        this.eventSubscriber = this.eventManager.subscribe('ufListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
