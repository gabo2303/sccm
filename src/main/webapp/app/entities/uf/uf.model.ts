import { BaseEntity } from './../../shared';

export class Uf implements BaseEntity {
    constructor(
        public id?: number,
        public fecha?: any,
        public valor?: number,
        public codMon?: number,
        public annoTributa?: BaseEntity,
    ) {
    }
}
