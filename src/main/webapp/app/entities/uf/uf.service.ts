import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Uf } from './uf.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UfService {

    private resourceUrl = SERVER_API_URL + 'api/ufs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(uf: Uf): Observable<Uf> {
        const copy = this.convert(uf);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(uf: Uf): Observable<Uf> {
        const copy = this.convert(uf);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Uf> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Uf.
     */
    private convertItemFromServer(json: any): Uf {
        const entity: Uf = Object.assign(new Uf(), json);
        entity.fecha = this.dateUtils
            .convertLocalDateFromServer(json.fecha);
        return entity;
    }

    /**
     * Convert a Uf to a JSON which can be sent to the server.
     */
    private convert(uf: Uf): Uf {
        const copy: Uf = Object.assign({}, uf);
        copy.fecha = this.dateUtils
            .convertLocalDateToServer(uf.fecha);
        return copy;
    }
}
