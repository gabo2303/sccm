import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Uf } from './uf.model';
import { UfService } from './uf.service';

@Component({
    selector: 'jhi-uf-detail',
    templateUrl: './uf-detail.component.html'
})
export class UfDetailComponent implements OnInit, OnDestroy {

    uf: Uf;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ufService: UfService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUfs();
    }

    load(id) {
        this.ufService.find(id).subscribe((uf) => {
            this.uf = uf;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUfs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ufListModification',
            (response) => this.load(this.uf.id)
        );
    }
}
