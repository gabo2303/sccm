import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { LHDCV } from './lhdcv.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class LHDCVService 
{
    private resourceUrl = SERVER_API_URL + 'api/l-hdcvs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(lHDCV: LHDCV): Observable<LHDCV> 
	{
        const copy = this.convert(lHDCV);
		
        return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(lHDCV: LHDCV): Observable<LHDCV> 
	{
        const copy = this.convert(lHDCV);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updateLHDCV(lHDCV: LHDCV): Observable<LHDCV> 
	{
        const copy = this.convert(lHDCV);
        
		return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<LHDCV> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }
	
	deleteLHDCV(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LHDCV.
     */
    private convertItemFromServer(json: any): LHDCV 
	{
        const entity: LHDCV = Object.assign(new LHDCV(), json);
        
		entity.fechaPago = this.dateUtils.convertDateTimeFromServer(json.fechaPago);
        entity.fechaInv = this.dateUtils.convertDateTimeFromServer(json.fechaInv);
        entity.fecVcto = this.dateUtils.convertDateTimeFromServer(json.fecVcto);
        entity.fecCont = this.dateUtils.convertDateTimeFromServer(json.fecCont);
		
        return entity;
    }

    /**
     * Convert a LHDCV to a JSON which can be sent to the server.
     */
    private convert(lHDCV: LHDCV): LHDCV 
	{
        const copy: LHDCV = Object.assign({}, lHDCV);
		
		try
		{
			copy.fechaPago = this.dateUtils.toDate(lHDCV.fechaPago);
			copy.fechaInv = this.dateUtils.toDate(lHDCV.fechaInv);
			copy.fecVcto = this.dateUtils.toDate(lHDCV.fecVcto);
			copy.fecCont = this.dateUtils.toDate(lHDCV.fecCont);
        }
		catch
		{			
			copy.fechaPago = this.dateUtils.convertLocalDateToServer(lHDCV.fechaPago);
			copy.fechaInv = this.dateUtils.convertLocalDateToServer(lHDCV.fechaInv);
			copy.fecVcto = this.dateUtils.convertLocalDateToServer(lHDCV.fecVcto);
			copy.fecCont = this.dateUtils.convertLocalDateToServer(lHDCV.fecCont);
		}
		
        
		
        return copy;
    }
}