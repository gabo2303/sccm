import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LHDCV } from './lhdcv.model';
import { LHDCVPopupService } from './lhdcv-popup.service';
import { LHDCVService } from './lhdcv.service';

@Component({
    selector: 'jhi-lhdcv-delete-dialog',
    templateUrl: './lhdcv-delete-dialog.component.html'
})
export class LHDCVDeleteDialogComponent {

    lHDCV: LHDCV;

    constructor(private lHDCVService: LHDCVService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lHDCVService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'lHDCVListModification', content: 'Deleted an lHDCV' });
            this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK' });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-lhdcv-delete-popup',
    template: ''
})
export class LHDCVDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private lHDCVPopupService: LHDCVPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.lHDCVPopupService.open(LHDCVDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
