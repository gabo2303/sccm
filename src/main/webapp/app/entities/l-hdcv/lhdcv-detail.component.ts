import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LHDCV } from './lhdcv.model';
import { LHDCVService } from './lhdcv.service';

@Component({
    selector: 'jhi-lhdcv-detail',
    templateUrl: './lhdcv-detail.component.html'
})
export class LHDCVDetailComponent implements OnInit, OnDestroy {

    lHDCV: LHDCV;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private lHDCVService: LHDCVService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLHDCVS();
    }

    load(id) {
        this.lHDCVService.find(id).subscribe((lHDCV) => {
            this.lHDCV = lHDCV;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLHDCVS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'lHDCVListModification',
            (response) => this.load(this.lHDCV.id)
        );
    }
}
