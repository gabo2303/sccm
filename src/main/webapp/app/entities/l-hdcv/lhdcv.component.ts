import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { LHDCV } from './lhdcv.model';
import { LHDCVService } from './lhdcv.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lhdcv',
    templateUrl: './lhdcv.component.html'
})
export class LHDCVComponent implements OnInit, OnDestroy {
lHDCVS: LHDCV[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private lHDCVService: LHDCVService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.lHDCVService.query().subscribe(
            (res: ResponseWrapper) => {
                this.lHDCVS = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLHDCVS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: LHDCV) {
        return item.id;
    }
    registerChangeInLHDCVS() {
        this.eventSubscriber = this.eventManager.subscribe('lHDCVListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
