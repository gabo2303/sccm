import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { LHDCVComponent } from './lhdcv.component';
import { LHDCVDetailComponent } from './lhdcv-detail.component';
import { LHDCVPopupComponent } from './lhdcv-dialog.component';
import { LHDCVDeletePopupComponent } from './lhdcv-delete-dialog.component';

export const lHDCVRoute: Routes = [
    {
        path: 'lhdcv',
        component: LHDCVComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.lHDCV.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'lhdcv/:id',
        component: LHDCVDetailComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.lHDCV.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const lHDCVPopupRoute: Routes = [
    {
        path: 'lhdcv-new',
        component: LHDCVPopupComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.lHDCV.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'lhdcv/:id/edit',
        component: LHDCVPopupComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.lHDCV.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'lhdcv/:id/delete',
        component: LHDCVDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.lHDCV.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
