import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LHDCV } from './lhdcv.model';
import { LHDCVPopupService } from './lhdcv-popup.service';
import { LHDCVService } from './lhdcv.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lhdcv-dialog',
    templateUrl: './lhdcv-dialog.component.html'
})
export class LHDCVDialogComponent implements OnInit {

    lHDCV: LHDCV;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private lHDCVService: LHDCVService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lHDCV.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lHDCVService.update(this.lHDCV));
        } else {
            this.subscribeToSaveResponse(
                this.lHDCVService.create(this.lHDCV));
        }
    }

    private subscribeToSaveResponse(result: Observable<LHDCV>) {
        result.subscribe((res: LHDCV) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LHDCV) {
        this.eventManager.broadcast({ name: 'lHDCVListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-lhdcv-popup',
    template: ''
})
export class LHDCVPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lHDCVPopupService: LHDCVPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lHDCVPopupService
                    .open(LHDCVDialogComponent as Component, params['id']);
            } else {
                this.lHDCVPopupService
                    .open(LHDCVDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
