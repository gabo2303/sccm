import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { LHDCV } from './lhdcv.model';
import { LHDCVService } from './lhdcv.service';

@Injectable()
export class LHDCVPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private lHDCVService: LHDCVService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.lHDCVService.find(id).subscribe((lHDCV) => {
                    lHDCV.fechaPago = this.datePipe
                        .transform(lHDCV.fechaPago, 'yyyy-MM-ddTHH:mm:ss');
                    lHDCV.fechaInv = this.datePipe
                        .transform(lHDCV.fechaInv, 'yyyy-MM-ddTHH:mm:ss');
                    lHDCV.fecVcto = this.datePipe
                        .transform(lHDCV.fecVcto, 'yyyy-MM-ddTHH:mm:ss');
                    lHDCV.fecCont = this.datePipe
                        .transform(lHDCV.fecCont, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.lHDCVModalRef(component, lHDCV);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.lHDCVModalRef(component, new LHDCV());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    lHDCVModalRef(component: Component, lHDCV: LHDCV): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.lHDCV = lHDCV;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
