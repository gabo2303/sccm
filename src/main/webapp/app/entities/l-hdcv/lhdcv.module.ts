import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    LHDCVService,
    LHDCVPopupService,
    LHDCVComponent,
    LHDCVDetailComponent,
    LHDCVDialogComponent,
    LHDCVPopupComponent,
    LHDCVDeletePopupComponent,
    LHDCVDeleteDialogComponent,
    lHDCVRoute,
    lHDCVPopupRoute,
} from './';

const ENTITY_STATES = [
    ...lHDCVRoute,
    ...lHDCVPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LHDCVComponent,
        LHDCVDetailComponent,
        LHDCVDialogComponent,
        LHDCVDeleteDialogComponent,
        LHDCVPopupComponent,
        LHDCVDeletePopupComponent,
    ],
    entryComponents: [
        LHDCVComponent,
        LHDCVDialogComponent,
        LHDCVPopupComponent,
        LHDCVDeleteDialogComponent,
        LHDCVDeletePopupComponent,
    ],
    providers: [
        LHDCVService,
        LHDCVPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmLHDCVModule {}
