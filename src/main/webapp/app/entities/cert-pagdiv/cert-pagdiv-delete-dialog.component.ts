import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CertPagdiv } from './cert-pagdiv.model';
import { CertPagdivPopupService } from './cert-pagdiv-popup.service';
import { CertPagdivService } from './cert-pagdiv.service';

@Component({
    selector: 'jhi-cert-pagdiv-delete-dialog',
    templateUrl: './cert-pagdiv-delete-dialog.component.html'
})
export class CertPagdivDeleteDialogComponent {

    certPagdiv: CertPagdiv;

    constructor(
        private certPagdivService: CertPagdivService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.certPagdivService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'certPagdivListModification',
                content: 'Deleted an certPagdiv'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cert-pagdiv-delete-popup',
    template: ''
})
export class CertPagdivDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private certPagdivPopupService: CertPagdivPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.certPagdivPopupService
                .open(CertPagdivDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
