import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { CertPagdiv } from './cert-pagdiv.model';
import { CertPagdivService } from './cert-pagdiv.service';

@Component({
    selector: 'jhi-cert-pagdiv-detail',
    templateUrl: './cert-pagdiv-detail.component.html'
})
export class CertPagdivDetailComponent implements OnInit, OnDestroy {

    certPagdiv: CertPagdiv;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private certPagdivService: CertPagdivService,
        private jhiAlertService: JhiAlertService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInCertPagdivs();
    }

    load(id) {
        this.certPagdivService.find(id).subscribe((certPagdiv) => {
            this.certPagdiv = certPagdiv;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCertPagdivs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'certPagdivListModification',
            (response) => this.load(this.certPagdiv.id)
        );
    }

}
