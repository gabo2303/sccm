import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CertPagdiv } from './cert-pagdiv.model';
import { CertPagdivService } from './cert-pagdiv.service';

@Injectable()
export class CertPagdivPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private certPagdivService: CertPagdivService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.certPagdivService.find(id).subscribe((certPagdiv) => {
                    if (certPagdiv.fechaPago) {
                        certPagdiv.fechaPago = {
                            year: certPagdiv.fechaPago.getFullYear(),
                            month: certPagdiv.fechaPago.getMonth() + 1,
                            day: certPagdiv.fechaPago.getDate()
                        };
                    }
                    this.ngbModalRef = this.certPagdivModalRef(component, certPagdiv);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.certPagdivModalRef(component, new CertPagdiv());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    certPagdivModalRef(component: Component, certPagdiv: CertPagdiv): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.certPagdiv = certPagdiv;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
