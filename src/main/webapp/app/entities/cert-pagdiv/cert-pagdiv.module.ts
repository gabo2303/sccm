import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    CertPagdivService,
    CertPagdivPopupService,
    CertPagdivComponent,
    CertPagdivDetailComponent,
    CertPagdivDialogComponent,
    CertPagdivPopupComponent,
    CertPagdivDeletePopupComponent,
    CertPagdivDeleteDialogComponent,
    certPagdivRoute,
    certPagdivPopupRoute,
} from './';

const ENTITY_STATES = [
    ...certPagdivRoute,
    ...certPagdivPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CertPagdivComponent,
        CertPagdivDetailComponent,
        CertPagdivDialogComponent,
        CertPagdivDeleteDialogComponent,
        CertPagdivPopupComponent,
        CertPagdivDeletePopupComponent,
    ],
    entryComponents: [
        CertPagdivComponent,
        CertPagdivDialogComponent,
        CertPagdivPopupComponent,
        CertPagdivDeleteDialogComponent,
        CertPagdivDeletePopupComponent,
    ],
    providers: [
        CertPagdivService,
        CertPagdivPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmCertPagdivModule {}
