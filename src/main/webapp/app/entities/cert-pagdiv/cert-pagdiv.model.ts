import { BaseEntity } from './../../shared';

export class CertPagdiv implements BaseEntity {
    constructor(
        public id?: number,
        public idCertificado?: number,
        public rutAccionista?: string,
        public nombreAccionista?: string,
        public fechaPago?: any,
        public dividendoNro?: number,
        public montoHistorico?: number,
        public fctAct?: number,
        public montoAct?: number,
        public ccredIdpcGenerados?: number,
        public ccredIdpcAcumulados?: number,
        public ccredIdpcVoluntario?: number,
        public sdcredito?: number,
        public monExentoIgcomp?: number,
        public montoNoconstRenta?: number,
        public rentasTribCumplida?: number,
        public rentasGeneradas?: number,
        public acumAcNorestSinder?: number,
        public acumAcNorestConder?: number,
        public acumAcRestricSinder?: number,
        public acumAcRestricConder?: number,
        public acumAcCredTotal?: number,
        public acumHConder?: number,
        public acumHSinder?: number,
        public acumHCredTotal?: number,
        public credImpTasaAdic?: number,
        public tasaEfectivaFut?: number,
        public devolucionCapital?: number,
        public acciones?: number,
        public annoTributa?: BaseEntity,
    ) {
    }
}
