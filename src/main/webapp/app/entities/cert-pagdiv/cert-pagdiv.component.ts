import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { CertPagdiv } from './cert-pagdiv.model';
import { CertPagdivService } from './cert-pagdiv.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ClienteBiceService } from '../cliente-bice/cliente-bice.service';
import { ExcelService } from '../../shared/excel/ExcelService';
import { HttpErrorResponse } from '@angular/common/http';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { ClienteBice } from '../cliente-bice/cliente-bice.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'jhi-cert-pagdiv',
    templateUrl: './cert-pagdiv.component.html'
})
export class CertPagdivComponent implements OnInit, OnDestroy {
    certPagdivs: CertPagdiv[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    clientesList: ClienteBice [];
    certPagDivFlag = false;
    certPagDivFlagSinData = false;
    ccredIdpcGeneradosFlag = false;
    ccredIdpcAcumuladosFlag = false;
    ccredIdpcVoluntarioFlag = false;
    sdcreditoFlag = false;
    monExentoIgcompFlag = false;
    montoNoconstRentaFlag = false;
    rentasTribCumplidaFlag = false;
    rentasGeneradasFlag = false;
    acumAcNorestSinderFlag = false;
    acumAcNorestConderFlag = false;
    acumAcRestricSinderFlag = false;
    acumAcRestricConderFlag = false;
    acumAcCredTotalFlag = false;
    acumHConderFlag = false;
    acumHSinderFlag = false;
    acumHCredTotalFlag = false;
    credImpTasaAdicFlag = false;
    devolucionCapitalFlag = false;

    constructor(private certPagdivService: CertPagdivService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private clientesService: ClienteBiceService, private excelService: ExcelService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    loadAll() {
        this.certPagdivService.query().subscribe(
            (res: ResponseWrapper) => {
                this.certPagdivs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadAllClientes();
        // this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCertPagdivs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CertPagdiv) {
        return item.id;
    }

    registerChangeInCertPagdivs() {
        this.eventSubscriber = this.eventManager.subscribe('certPagdivListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    filterByAnooTributario(annoTributaId: any) {
        this.spinnerService.show();
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.certPagDivFlag = false;
        this.certPagDivFlagSinData = false;
        this.certPagdivService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.certPagdivs = res.json;
                if (this.certPagdivs.length > 0) {
                    this.certPagDivFlag = true;
                    if (this.certPagdivs[ 0 ].ccredIdpcGenerados !== 0) {
                        this.ccredIdpcGeneradosFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].ccredIdpcAcumulados !== 0) {
                        this.ccredIdpcAcumuladosFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].ccredIdpcVoluntario !== 0) {
                        this.ccredIdpcVoluntarioFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].sdcredito !== 0) {
                        this.sdcreditoFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].monExentoIgcomp !== 0) {
                        this.monExentoIgcompFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].montoNoconstRenta !== 0) {
                        this.montoNoconstRentaFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].rentasTribCumplida !== 0) {
                        this.rentasTribCumplidaFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].rentasGeneradas !== 0) {
                        this.rentasGeneradasFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumAcNorestSinder !== 0) {
                        this.acumAcNorestSinderFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumAcNorestConder !== 0) {
                        this.acumAcNorestConderFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumAcRestricSinder !== 0) {
                        this.acumAcRestricSinderFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumAcRestricConder !== 0) {
                        this.acumAcRestricConderFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumAcCredTotal !== 0) {
                        this.acumAcCredTotalFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumHConder !== 0) {
                        this.acumHConderFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumHSinder !== 0) {
                        this.acumHSinderFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].acumHCredTotal !== 0) {
                        this.acumHCredTotalFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].credImpTasaAdic !== 0) {
                        this.credImpTasaAdicFlag = true;
                    }
                    if (this.certPagdivs[ 0 ].devolucionCapital !== 0) {
                        this.devolucionCapitalFlag = true;
                    }
                } else {
                    this.certPagDivFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadAllClientes() {
        this.clientesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientesList = res.json;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    generateExcel() {
        const table: HTMLTableElement = <HTMLTableElement> document.getElementById('table');
        const rowCount = !isNullOrUndefined(table) ? table.rows.length : 0;
        if (rowCount > 1) {
            let textos = '[';
            const headers: any [] = [];
            for (let i = 0; i < table.rows.length; i++) {
                if (i > 0) {
                    textos += '{';
                }
                for (let j = 0; j < 12; j++) {
                    if (i === 0) {
                        // Obtenemos la primera fila de la tabla o cabecera y limpiamos tag html y espacios vacíos
                        headers[ j ] = table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim();
                    } else {
                        if (j < 11) {
                            textos += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '",';
                        } else {
                            textos += '"' + headers[ j ] + '" : "' + table.rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '"';
                        }
                    }
                }
                if (i > 0 && i < table.rows.length - 1) {
                    textos += '},';
                }
                if (i === table.rows.length - 1) {
                    textos += '}]';
                }
            }
            this.excelService.exportAsExcelFile(
                JSON.parse(textos.toString()),
                'Emisión Pago Dividendo',
                'Emisión Pago Dividendo');
        } else {
            this.jhiAlertService.error('No existen registros para exportar');
        }
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}

