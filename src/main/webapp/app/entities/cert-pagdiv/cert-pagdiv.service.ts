import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CertPagdiv } from './cert-pagdiv.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class CertPagdivService {

    private resourceUrl = SERVER_API_URL + 'api/cert-pagdivs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    create(certPagdiv: CertPagdiv): Observable<CertPagdiv> {
        const copy = this.convert(certPagdiv);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(certPagdiv: CertPagdiv): Observable<CertPagdiv> {
        const copy = this.convert(certPagdiv);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CertPagdiv> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CertPagdiv.
     */
    private convertItemFromServer(json: any): CertPagdiv {
        const entity: CertPagdiv = Object.assign(new CertPagdiv(), json);
        entity.fechaPago = this.dateUtils
        .convertLocalDateFromServer(json.fechaPago);
        return entity;
    }

    /**
     * Convert a CertPagdiv to a JSON which can be sent to the server.
     */
    private convert(certPagdiv: CertPagdiv): CertPagdiv {
        const copy: CertPagdiv = Object.assign({}, certPagdiv);
        copy.fechaPago = this.dateUtils
        .convertLocalDateToServer(certPagdiv.fechaPago);
        return copy;
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`)
        .map((res: Response) => this.convertResponse(res));
    }

    executeSpLoadCertPagDiv(): Observable<Response> {
        return this.http.get(`${this.resourceUrl}` + '/executeSpLoadCertPagDiv');
    }

}
