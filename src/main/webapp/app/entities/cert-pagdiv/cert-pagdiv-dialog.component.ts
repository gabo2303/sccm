import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CertPagdiv } from './cert-pagdiv.model';
import { CertPagdivPopupService } from './cert-pagdiv-popup.service';
import { CertPagdivService } from './cert-pagdiv.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cert-pagdiv-dialog',
    templateUrl: './cert-pagdiv-dialog.component.html'
})
export class CertPagdivDialogComponent implements OnInit {

    certPagdiv: CertPagdiv;
    isSaving: boolean;

    annotributas: AnnoTributa[];
    fechaPagoDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private certPagdivService: CertPagdivService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.certPagdiv.id !== undefined) {
            this.subscribeToSaveResponse(
                this.certPagdivService.update(this.certPagdiv));
        } else {
            this.subscribeToSaveResponse(
                this.certPagdivService.create(this.certPagdiv));
        }
    }

    private subscribeToSaveResponse(result: Observable<CertPagdiv>) {
        result.subscribe((res: CertPagdiv) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CertPagdiv) {
        this.eventManager.broadcast({ name: 'certPagdivListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-cert-pagdiv-popup',
    template: ''
})
export class CertPagdivPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private certPagdivPopupService: CertPagdivPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.certPagdivPopupService
                    .open(CertPagdivDialogComponent as Component, params['id']);
            } else {
                this.certPagdivPopupService
                    .open(CertPagdivDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
