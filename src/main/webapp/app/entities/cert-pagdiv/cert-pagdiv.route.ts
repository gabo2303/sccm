import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { CertPagdivComponent } from './cert-pagdiv.component';
import { CertPagdivDetailComponent } from './cert-pagdiv-detail.component';
import { CertPagdivPopupComponent } from './cert-pagdiv-dialog.component';
import { CertPagdivDeletePopupComponent } from './cert-pagdiv-delete-dialog.component';

export const certPagdivRoute: Routes = [
    {
        path: 'cert-pagdiv',
        component: CertPagdivComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.certPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'cert-pagdiv/:id',
        component: CertPagdivDetailComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.certPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const certPagdivPopupRoute: Routes = [
    {
        path: 'cert-pagdiv-new',
        component: CertPagdivPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.certPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cert-pagdiv/:id/edit',
        component: CertPagdivPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.certPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cert-pagdiv/:id/delete',
        component: CertPagdivDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.certPagdiv.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
