import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { TablaMoneda } from './tabla-moneda.model';
import { TablaMonedaService } from './tabla-moneda.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-tabla-moneda',
    templateUrl: './tabla-moneda.component.html'
})
export class TablaMonedaComponent implements OnInit, OnDestroy {
tablaMonedas: TablaMoneda[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private tablaMonedaService: TablaMonedaService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.tablaMonedaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tablaMonedas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTablaMonedas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TablaMoneda) {
        return item.id;
    }
    registerChangeInTablaMonedas() {
        this.eventSubscriber = this.eventManager.subscribe('tablaMonedaListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
