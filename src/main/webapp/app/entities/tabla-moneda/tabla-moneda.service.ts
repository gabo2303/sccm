import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TablaMoneda } from './tabla-moneda.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TablaMonedaService {

    private resourceUrl = SERVER_API_URL + 'api/tabla-monedas';

    constructor(private http: Http) { }

    create(tablaMoneda: TablaMoneda): Observable<TablaMoneda> {
        const copy = this.convert(tablaMoneda);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(tablaMoneda: TablaMoneda): Observable<TablaMoneda> {
        const copy = this.convert(tablaMoneda);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TablaMoneda> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TablaMoneda.
     */
    private convertItemFromServer(json: any): TablaMoneda {
        const entity: TablaMoneda = Object.assign(new TablaMoneda(), json);
        return entity;
    }

    /**
     * Convert a TablaMoneda to a JSON which can be sent to the server.
     */
    private convert(tablaMoneda: TablaMoneda): TablaMoneda {
        const copy: TablaMoneda = Object.assign({}, tablaMoneda);
        return copy;
    }
}
