import { BaseEntity } from './../../shared';

export class TablaMoneda implements BaseEntity {
    constructor(
        public id?: number,
        public codMon?: number,
        public glosa?: string,
        public moneda?: number,
        public simbolo?: string,
    ) {
    }
}
