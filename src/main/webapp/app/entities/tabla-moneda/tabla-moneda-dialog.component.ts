import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TablaMoneda } from './tabla-moneda.model';
import { TablaMonedaPopupService } from './tabla-moneda-popup.service';
import { TablaMonedaService } from './tabla-moneda.service';

@Component({
    selector: 'jhi-tabla-moneda-dialog',
    templateUrl: './tabla-moneda-dialog.component.html'
})
export class TablaMonedaDialogComponent implements OnInit {

    tablaMoneda: TablaMoneda;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tablaMonedaService: TablaMonedaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tablaMoneda.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tablaMonedaService.update(this.tablaMoneda));
        } else {
            this.subscribeToSaveResponse(
                this.tablaMonedaService.create(this.tablaMoneda));
        }
    }

    private subscribeToSaveResponse(result: Observable<TablaMoneda>) {
        result.subscribe((res: TablaMoneda) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TablaMoneda) {
        this.eventManager.broadcast({ name: 'tablaMonedaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-tabla-moneda-popup',
    template: ''
})
export class TablaMonedaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tablaMonedaPopupService: TablaMonedaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tablaMonedaPopupService
                    .open(TablaMonedaDialogComponent as Component, params['id']);
            } else {
                this.tablaMonedaPopupService
                    .open(TablaMonedaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
