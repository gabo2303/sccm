import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TablaMoneda } from './tabla-moneda.model';
import { TablaMonedaService } from './tabla-moneda.service';

@Component({
    selector: 'jhi-tabla-moneda-detail',
    templateUrl: './tabla-moneda-detail.component.html'
})
export class TablaMonedaDetailComponent implements OnInit, OnDestroy {

    tablaMoneda: TablaMoneda;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tablaMonedaService: TablaMonedaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTablaMonedas();
    }

    load(id) {
        this.tablaMonedaService.find(id).subscribe((tablaMoneda) => {
            this.tablaMoneda = tablaMoneda;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTablaMonedas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tablaMonedaListModification',
            (response) => this.load(this.tablaMoneda.id)
        );
    }
}
