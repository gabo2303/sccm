import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TablaMonedaComponent } from './tabla-moneda.component';
import { TablaMonedaDetailComponent } from './tabla-moneda-detail.component';
import { TablaMonedaPopupComponent } from './tabla-moneda-dialog.component';
import { TablaMonedaDeletePopupComponent } from './tabla-moneda-delete-dialog.component';

export const tablaMonedaRoute: Routes = [
    {
        path: 'tabla-moneda',
        component: TablaMonedaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.tablaMoneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tabla-moneda/:id',
        component: TablaMonedaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.tablaMoneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tablaMonedaPopupRoute: Routes = [
    {
        path: 'tabla-moneda-new',
        component: TablaMonedaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.tablaMoneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tabla-moneda/:id/edit',
        component: TablaMonedaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.tablaMoneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tabla-moneda/:id/delete',
        component: TablaMonedaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.tablaMoneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
