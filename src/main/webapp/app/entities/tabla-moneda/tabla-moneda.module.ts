import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    TablaMonedaService,
    TablaMonedaPopupService,
    TablaMonedaComponent,
    TablaMonedaDetailComponent,
    TablaMonedaDialogComponent,
    TablaMonedaPopupComponent,
    TablaMonedaDeletePopupComponent,
    TablaMonedaDeleteDialogComponent,
    tablaMonedaRoute,
    tablaMonedaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tablaMonedaRoute,
    ...tablaMonedaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TablaMonedaComponent,
        TablaMonedaDetailComponent,
        TablaMonedaDialogComponent,
        TablaMonedaDeleteDialogComponent,
        TablaMonedaPopupComponent,
        TablaMonedaDeletePopupComponent,
    ],
    entryComponents: [
        TablaMonedaComponent,
        TablaMonedaDialogComponent,
        TablaMonedaPopupComponent,
        TablaMonedaDeleteDialogComponent,
        TablaMonedaDeletePopupComponent,
    ],
    providers: [
        TablaMonedaService,
        TablaMonedaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmTablaMonedaModule {}
