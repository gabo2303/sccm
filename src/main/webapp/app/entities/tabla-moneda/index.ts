export * from './tabla-moneda.model';
export * from './tabla-moneda-popup.service';
export * from './tabla-moneda.service';
export * from './tabla-moneda-dialog.component';
export * from './tabla-moneda-delete-dialog.component';
export * from './tabla-moneda-detail.component';
export * from './tabla-moneda.component';
export * from './tabla-moneda.route';
