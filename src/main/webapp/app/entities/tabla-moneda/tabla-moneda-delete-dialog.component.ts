import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TablaMoneda } from './tabla-moneda.model';
import { TablaMonedaPopupService } from './tabla-moneda-popup.service';
import { TablaMonedaService } from './tabla-moneda.service';

@Component({
    selector: 'jhi-tabla-moneda-delete-dialog',
    templateUrl: './tabla-moneda-delete-dialog.component.html'
})
export class TablaMonedaDeleteDialogComponent {

    tablaMoneda: TablaMoneda;

    constructor(
        private tablaMonedaService: TablaMonedaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tablaMonedaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tablaMonedaListModification',
                content: 'Deleted an tablaMoneda'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tabla-moneda-delete-popup',
    template: ''
})
export class TablaMonedaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tablaMonedaPopupService: TablaMonedaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tablaMonedaPopupService
                .open(TablaMonedaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
