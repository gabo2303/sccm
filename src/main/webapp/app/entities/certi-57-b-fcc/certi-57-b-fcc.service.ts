import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Certi57BFcc } from './certi-57-b-fcc.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class Certi57BFccService {

    private resourceUrl = SERVER_API_URL + 'api/certi-57-b-fccs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(certi57BFcc: Certi57BFcc): Observable<Certi57BFcc> {
        const copy = this.convert(certi57BFcc);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(certi57BFcc: Certi57BFcc): Observable<Certi57BFcc> {
        const copy = this.convert(certi57BFcc);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Certi57BFcc> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Certi57BFcc.
     */
    private convertItemFromServer(json: any): Certi57BFcc {
        const entity: Certi57BFcc = Object.assign(new Certi57BFcc(), json);
        entity.fedep = this.dateUtils
            .convertLocalDateFromServer(json.fedep);
        entity.feven = this.dateUtils
            .convertLocalDateFromServer(json.feven);
        entity.fedep1 = this.dateUtils
            .convertLocalDateFromServer(json.fedep1);
        entity.feven1 = this.dateUtils
            .convertLocalDateFromServer(json.feven1);
        entity.fedia = this.dateUtils
            .convertLocalDateFromServer(json.fedia);
        return entity;
    }

    /**
     * Convert a Certi57BFcc to a JSON which can be sent to the server.
     */
    private convert(certi57BFcc: Certi57BFcc): Certi57BFcc {
        const copy: Certi57BFcc = Object.assign({}, certi57BFcc);
        copy.fedep = this.dateUtils
            .convertLocalDateToServer(certi57BFcc.fedep);
        copy.feven = this.dateUtils
            .convertLocalDateToServer(certi57BFcc.feven);
        copy.fedep1 = this.dateUtils
            .convertLocalDateToServer(certi57BFcc.fedep1);
        copy.feven1 = this.dateUtils
            .convertLocalDateToServer(certi57BFcc.feven1);
        copy.fedia = this.dateUtils
            .convertLocalDateToServer(certi57BFcc.fedia);
        return copy;
    }
}
