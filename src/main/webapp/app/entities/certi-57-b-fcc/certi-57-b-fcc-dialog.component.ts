import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Certi57BFcc } from './certi-57-b-fcc.model';
import { Certi57BFccPopupService } from './certi-57-b-fcc-popup.service';
import { Certi57BFccService } from './certi-57-b-fcc.service';

@Component({
    selector: 'jhi-certi-57-b-fcc-dialog',
    templateUrl: './certi-57-b-fcc-dialog.component.html'
})
export class Certi57BFccDialogComponent implements OnInit {

    certi57BFcc: Certi57BFcc;
    isSaving: boolean;
    fedepDp: any;
    fevenDp: any;
    fedep1Dp: any;
    feven1Dp: any;
    fediaDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private certi57BFccService: Certi57BFccService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.certi57BFcc.id !== undefined) {
            this.subscribeToSaveResponse(
                this.certi57BFccService.update(this.certi57BFcc));
        } else {
            this.subscribeToSaveResponse(
                this.certi57BFccService.create(this.certi57BFcc));
        }
    }

    private subscribeToSaveResponse(result: Observable<Certi57BFcc>) {
        result.subscribe((res: Certi57BFcc) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Certi57BFcc) {
        this.eventManager.broadcast({ name: 'certi57BFccListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-certi-57-b-fcc-popup',
    template: ''
})
export class Certi57BFccPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private certi57BFccPopupService: Certi57BFccPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.certi57BFccPopupService
                    .open(Certi57BFccDialogComponent as Component, params['id']);
            } else {
                this.certi57BFccPopupService
                    .open(Certi57BFccDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
