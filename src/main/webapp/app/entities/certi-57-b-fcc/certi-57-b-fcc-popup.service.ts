import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Certi57BFcc } from './certi-57-b-fcc.model';
import { Certi57BFccService } from './certi-57-b-fcc.service';

@Injectable()
export class Certi57BFccPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private certi57BFccService: Certi57BFccService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.certi57BFccService.find(id).subscribe((certi57BFcc) => {
                    if (certi57BFcc.fedep) {
                        certi57BFcc.fedep = {
                            year: certi57BFcc.fedep.getFullYear(),
                            month: certi57BFcc.fedep.getMonth() + 1,
                            day: certi57BFcc.fedep.getDate()
                        };
                    }
                    if (certi57BFcc.feven) {
                        certi57BFcc.feven = {
                            year: certi57BFcc.feven.getFullYear(),
                            month: certi57BFcc.feven.getMonth() + 1,
                            day: certi57BFcc.feven.getDate()
                        };
                    }
                    if (certi57BFcc.fedep1) {
                        certi57BFcc.fedep1 = {
                            year: certi57BFcc.fedep1.getFullYear(),
                            month: certi57BFcc.fedep1.getMonth() + 1,
                            day: certi57BFcc.fedep1.getDate()
                        };
                    }
                    if (certi57BFcc.feven1) {
                        certi57BFcc.feven1 = {
                            year: certi57BFcc.feven1.getFullYear(),
                            month: certi57BFcc.feven1.getMonth() + 1,
                            day: certi57BFcc.feven1.getDate()
                        };
                    }
                    if (certi57BFcc.fedia) {
                        certi57BFcc.fedia = {
                            year: certi57BFcc.fedia.getFullYear(),
                            month: certi57BFcc.fedia.getMonth() + 1,
                            day: certi57BFcc.fedia.getDate()
                        };
                    }
                    this.ngbModalRef = this.certi57BFccModalRef(component, certi57BFcc);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.certi57BFccModalRef(component, new Certi57BFcc());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    certi57BFccModalRef(component: Component, certi57BFcc: Certi57BFcc): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.certi57BFcc = certi57BFcc;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
