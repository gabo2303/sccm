import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Certi57BFcc } from './certi-57-b-fcc.model';
import { Certi57BFccPopupService } from './certi-57-b-fcc-popup.service';
import { Certi57BFccService } from './certi-57-b-fcc.service';

@Component({
    selector: 'jhi-certi-57-b-fcc-delete-dialog',
    templateUrl: './certi-57-b-fcc-delete-dialog.component.html'
})
export class Certi57BFccDeleteDialogComponent {

    certi57BFcc: Certi57BFcc;

    constructor(
        private certi57BFccService: Certi57BFccService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.certi57BFccService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'certi57BFccListModification',
                content: 'Deleted an certi57BFcc'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-certi-57-b-fcc-delete-popup',
    template: ''
})
export class Certi57BFccDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private certi57BFccPopupService: Certi57BFccPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.certi57BFccPopupService
                .open(Certi57BFccDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
