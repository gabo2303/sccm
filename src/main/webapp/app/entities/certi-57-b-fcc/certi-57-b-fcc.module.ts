import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Certi57BFccService,
    Certi57BFccPopupService,
    Certi57BFccComponent,
    Certi57BFccDetailComponent,
    Certi57BFccDialogComponent,
    Certi57BFccPopupComponent,
    Certi57BFccDeletePopupComponent,
    Certi57BFccDeleteDialogComponent,
    certi57BFccRoute,
    certi57BFccPopupRoute,
} from './';

const ENTITY_STATES = [
    ...certi57BFccRoute,
    ...certi57BFccPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Certi57BFccComponent,
        Certi57BFccDetailComponent,
        Certi57BFccDialogComponent,
        Certi57BFccDeleteDialogComponent,
        Certi57BFccPopupComponent,
        Certi57BFccDeletePopupComponent,
    ],
    entryComponents: [
        Certi57BFccComponent,
        Certi57BFccDialogComponent,
        Certi57BFccPopupComponent,
        Certi57BFccDeleteDialogComponent,
        Certi57BFccDeletePopupComponent,
    ],
    providers: [
        Certi57BFccService,
        Certi57BFccPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmCerti57BFccModule {}
