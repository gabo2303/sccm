import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Certi57BFcc } from './certi-57-b-fcc.model';
import { Certi57BFccService } from './certi-57-b-fcc.service';

@Component({
    selector: 'jhi-certi-57-b-fcc-detail',
    templateUrl: './certi-57-b-fcc-detail.component.html'
})
export class Certi57BFccDetailComponent implements OnInit, OnDestroy {

    certi57BFcc: Certi57BFcc;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private certi57BFccService: Certi57BFccService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInCerti57BFccs();
    }

    load(id) {
        this.certi57BFccService.find(id).subscribe((certi57BFcc) => {
            this.certi57BFcc = certi57BFcc;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCerti57BFccs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'certi57BFccListModification',
            (response) => this.load(this.certi57BFcc.id)
        );
    }
}
