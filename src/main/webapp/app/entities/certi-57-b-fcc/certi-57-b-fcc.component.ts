import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Certi57BFcc } from './certi-57-b-fcc.model';
import { Certi57BFccService } from './certi-57-b-fcc.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-certi-57-b-fcc',
    templateUrl: './certi-57-b-fcc.component.html'
})
export class Certi57BFccComponent implements OnInit, OnDestroy {
certi57BFccs: Certi57BFcc[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private certi57BFccService: Certi57BFccService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.certi57BFccService.query().subscribe(
            (res: ResponseWrapper) => {
                this.certi57BFccs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCerti57BFccs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Certi57BFcc) {
        return item.id;
    }
    registerChangeInCerti57BFccs() {
        this.eventSubscriber = this.eventManager.subscribe('certi57BFccListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
