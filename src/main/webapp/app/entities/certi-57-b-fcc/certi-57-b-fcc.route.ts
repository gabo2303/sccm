import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Certi57BFccComponent } from './certi-57-b-fcc.component';
import { Certi57BFccDetailComponent } from './certi-57-b-fcc-detail.component';
import { Certi57BFccPopupComponent } from './certi-57-b-fcc-dialog.component';
import { Certi57BFccDeletePopupComponent } from './certi-57-b-fcc-delete-dialog.component';

export const certi57BFccRoute: Routes = [
    {
        path: 'certi-57-b-fcc',
        component: Certi57BFccComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certi57BFcc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'certi-57-b-fcc/:id',
        component: Certi57BFccDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certi57BFcc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const certi57BFccPopupRoute: Routes = [
    {
        path: 'certi-57-b-fcc-new',
        component: Certi57BFccPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certi57BFcc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'certi-57-b-fcc/:id/edit',
        component: Certi57BFccPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certi57BFcc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'certi-57-b-fcc/:id/delete',
        component: Certi57BFccDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.certi57BFcc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
