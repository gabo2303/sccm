import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Aprobacion } from './aprobacion.model';
import { AprobacionService } from './aprobacion.service';
import { PATTERN_DATE_SHORT } from '../../shared';
import { APPROVAL_PRODUCT_TYPE_BONOS_LETRAS } from '../../app.constants';

@Component({
    selector: 'jhi-aprobacion-detail',
    templateUrl: './aprobacion-detail.component.html'
})
export class AprobacionDetailComponent implements OnInit, OnDestroy {

    aprobacion: Aprobacion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    product: string = null;
    APPROVAL_PRODUCT_TYPE_BONOS_LETRAS = APPROVAL_PRODUCT_TYPE_BONOS_LETRAS;

    constructor(private eventManager: JhiEventManager, private aprobacionService: AprobacionService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.product = params[ 'product' ];
            this.load(params[ 'id' ]);
        });
        this.registerChangeInAprobacions();
    }

    load(id) {
        this.aprobacionService.find(id).subscribe((aprobacion) => {
            this.aprobacion = aprobacion;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAprobacions() {
        this.eventSubscriber = this.eventManager.subscribe('aprobacionListModification', (response) => this.load(this.aprobacion.id)
        );
    }
}
