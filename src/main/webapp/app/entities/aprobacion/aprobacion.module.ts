import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    AprobacionComponent,
    AprobacionDeleteDialogComponent,
    AprobacionDeletePopupComponent,
    AprobacionDetailComponent,
    AprobacionDialogComponent,
    AprobacionPopupComponent,
    aprobacionPopupRoute,
    AprobacionPopupService,
    aprobacionRoute,
    AprobacionService,
} from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...aprobacionRoute,
    ...aprobacionPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        AprobacionComponent,
        AprobacionDetailComponent,
        AprobacionDialogComponent,
        AprobacionDeleteDialogComponent,
        AprobacionPopupComponent,
        AprobacionDeletePopupComponent,
    ],
    entryComponents: [
        AprobacionComponent,
        AprobacionDialogComponent,
        AprobacionPopupComponent,
        AprobacionDeleteDialogComponent,
        AprobacionDeletePopupComponent,
    ],
    providers: [
        AprobacionService,
        AprobacionPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAprobacionModule {
}
