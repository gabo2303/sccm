import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Aprobacion } from './aprobacion.model';
import { AprobacionService } from './aprobacion.service';
import { APPROVAL_PRODUCT_TYPE_BONOS_LETRAS } from '../../app.constants';

@Injectable()
export class AprobacionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private aprobacionService: AprobacionService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, product?: string, currentAccount?: any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }
            if (id) {
                this.aprobacionService.find(id).subscribe((aprobacion) => {
                    if (aprobacion.fechaUltimaModificacion) {
                        aprobacion.fechaUltimaModificacion = {
                            year: aprobacion.fechaUltimaModificacion.getFullYear(),
                            month: aprobacion.fechaUltimaModificacion.getMonth() + 1,
                            day: aprobacion.fechaUltimaModificacion.getDate()
                        };
                    }
                    this.ngbModalRef = this.aprobacionModalRef(component, aprobacion);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    let aprobacion = new Aprobacion();
                    aprobacion.producto = product;
                    if (currentAccount) {
                        aprobacion.usuario = currentAccount.firstName + (currentAccount.lastName ? ' ' + currentAccount.lastName : '');
                    }
                    this.ngbModalRef = this.aprobacionModalRef(component, aprobacion);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    aprobacionModalRef(component: Component, aprobacion: Aprobacion): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.aprobacion = aprobacion;
        if (aprobacion && aprobacion.producto && aprobacion.producto === 'Bonos Y Letras') {
            modalRef.componentInstance.product = APPROVAL_PRODUCT_TYPE_BONOS_LETRAS;
        }
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
