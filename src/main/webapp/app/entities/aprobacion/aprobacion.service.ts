import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Aprobacion } from './aprobacion.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class AprobacionService {

    private resourceUrl = SERVER_API_URL + 'api/aprobacions';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    aprobacionsByProduct(product: string, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/findAllByProduct?product=' + product, options).map((res: Response) => this.convertResponse(res));
    }

    create(aprobacion: Aprobacion): Observable<Aprobacion> {
        const copy = this.convert(aprobacion);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(aprobacion: Aprobacion): Observable<Aprobacion> {
        const copy = this.convert(aprobacion);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Aprobacion> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Aprobacion.
     */
    private convertItemFromServer(json: any): Aprobacion {
        const entity: Aprobacion = Object.assign(new Aprobacion(), json);
        entity.fechaUltimaModificacion = this.dateUtils.convertLocalDateFromServer(json.fechaUltimaModificacion);
        return entity;
    }

    /**
     * Convert a Aprobacion to a JSON which can be sent to the server.
     */
    private convert(aprobacion: Aprobacion): Aprobacion {
        const copy: Aprobacion = Object.assign({}, aprobacion);
        copy.fechaUltimaModificacion = this.dateUtils.convertLocalDateToServer(aprobacion.fechaUltimaModificacion);
        return copy;
    }
}
