import { BaseEntity } from './../../shared';

export class Aprobacion implements BaseEntity {
    constructor(
        public id?: number,
        public producto?: string,
        public usuario?: string,
        public cuadraturaContable?: boolean,
        public interesesPagados?: boolean,
        public aprobacionProducto?: boolean,
        public comentarios?: string,
        public annoTributa?: BaseEntity,
        public fechaUltimaModificacion?: any,
    ) {
        this.cuadraturaContable = false;
        this.interesesPagados = false;
        this.aprobacionProducto = false;
    }
}
