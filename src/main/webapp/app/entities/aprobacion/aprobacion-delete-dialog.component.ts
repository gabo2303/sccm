import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Aprobacion } from './aprobacion.model';
import { AprobacionPopupService } from './aprobacion-popup.service';
import { AprobacionService } from './aprobacion.service';

@Component({
    selector: 'jhi-aprobacion-delete-dialog',
    templateUrl: './aprobacion-delete-dialog.component.html'
})
export class AprobacionDeleteDialogComponent {

    aprobacion: Aprobacion;

    constructor(private aprobacionService: AprobacionService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.aprobacionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'aprobacionListModification',
                content: 'Deleted an aprobacion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-aprobacion-delete-popup',
    template: ''
})
export class AprobacionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private aprobacionPopupService: AprobacionPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.aprobacionPopupService.open(AprobacionDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
