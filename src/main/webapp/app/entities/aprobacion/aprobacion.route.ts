import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { AprobacionComponent } from './aprobacion.component';
import { AprobacionPopupComponent } from './aprobacion-dialog.component';
import { AprobacionDeletePopupComponent } from './aprobacion-delete-dialog.component';
import { SCCM_1890_ADMIN, SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN_BYL, SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN_PACTOS } from '../../app.constants';
import { AprobacionDetailComponent } from './aprobacion-detail.component';

export const aprobacionRoute: Routes = [
    {
        path: 'aprobacion-pactos/:product',
        component: AprobacionComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_PACTOS, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-ahorro/:product',
        component: AprobacionComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-dap/:product',
        component: AprobacionComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-byl/:product',
        component: AprobacionComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_BYL, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-pactos/:product/:id',
        component: AprobacionDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_PACTOS, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-ahorro/:product/:id',
        component: AprobacionDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-dap/:product/:id',
        component: AprobacionDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'aprobacion-byl/:product/:id',
        component: AprobacionDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_BYL, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const aprobacionPopupRoute: Routes = [
    {
        path: 'aprobacion-new/:product',
        component: AprobacionPopupComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_PACTOS, SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN_BYL ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'aprobacion/:id/edit',
        component: AprobacionPopupComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_PACTOS, SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN_BYL, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'aprobacion/:id/delete',
        component: AprobacionDeletePopupComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN_PACTOS, SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN_BYL, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.aprobacion.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
