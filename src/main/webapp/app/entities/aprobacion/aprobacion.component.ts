import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Aprobacion } from './aprobacion.model';
import { AprobacionService } from './aprobacion.service';
import { Rut_tempService } from '../rut-temp/rut-temp.service';
import { PATTERN_DATE_SHORT, Principal, ResponseWrapper } from '../../shared';
import { ActivatedRoute, Params } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ProductTypeFromUrlParamPipe } from '../../shared/pipes/productTypeFromUrlParam.pipe';
import { APPROVAL_PRODUCT_TYPE_BONOS_LETRAS, SCCM_1890_ADMIN_AHORRO, SCCM_1890_ADMIN_BYL, SCCM_1890_ADMIN_DAP, SCCM_1890_ADMIN_PACTOS } from '../../app.constants';

@Component({
    selector: 'jhi-aprobacion',
    templateUrl: './aprobacion.component.html'
})
export class AprobacionComponent implements OnInit, OnDestroy 
{
    aprobacions: Aprobacion[];
    currentAccount: any;
    eventSubscriber: Subscription;
    product: string = null;
    routeData: any;
    aprobacionsFlag = false;
    aprobacionsFlagSinData = false;
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    ROLE_SCCM_1890_ADMIN_PACTOS = SCCM_1890_ADMIN_PACTOS;
    ROLE_SCCM_1890_ADMIN_AHORRO = SCCM_1890_ADMIN_AHORRO;
    ROLE_SCCM_1890_ADMIN_DAP = SCCM_1890_ADMIN_DAP;
    ROLE_SCCM_1890_ADMIN_BYL = SCCM_1890_ADMIN_BYL;
    APPROVAL_PRODUCT_TYPE_BONOS_LETRAS = APPROVAL_PRODUCT_TYPE_BONOS_LETRAS;
	canUse: boolean;
	flagResult = false;
	resultMsg = '';
    resultType = '';
    classType = '';
	
    constructor(private aprobacionService: AprobacionService, private jhiAlertService: JhiAlertService, 
				private eventManager: JhiEventManager, private principal: Principal, private activatedRoute: ActivatedRoute, 
				private spinnerService: Ng4LoadingSpinnerService, private productTypeFromUrlParamPipe: ProductTypeFromUrlParamPipe,
				private rutTempService: Rut_tempService) {  }

    ngOnInit() 
	{
		this.rutTempService.query().subscribe
		(
			(res: ResponseWrapper) => 
			{
				if(res.json.length != 0) 
				{
					this.canUse = false;
					
					this.flagResult = true;
					this.resultMsg = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
					this.resultType = 'Info: ';
					this.classType = 'alert alert-info alert-dismissible';
				}
				else { this.canUse = true; }
			},
			(res: ResponseWrapper) => {  }
		);
		
		this.activatedRoute.params.subscribe((data: Params) => 
		{
			this.product = data[ 'product' ];
			this.loadAll();
			
			this.principal.identity().then((account) => { this.currentAccount = account; });
			
			this.registerChangeInAprobacions();
		});
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    loadAll() 
	{
        this.spinnerService.show();
        
		this.aprobacionService.aprobacionsByProduct(this.productTypeFromUrlParamPipe.transform(this.product)).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    trackId(index: number, item: Aprobacion) { return item.id; }

    registerChangeInAprobacions() 
	{ 
		this.eventSubscriber = this.eventManager.subscribe('aprobacionListModification', (response) => this.loadAll());
    }

    private onSuccess(data, headers) 
	{
        this.aprobacions = data;
        
		if (this.aprobacions.length > 0) { this.aprobacionsFlag = true; } else { this.aprobacionsFlagSinData = true; }

        this.spinnerService.hide();
    }

    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
}