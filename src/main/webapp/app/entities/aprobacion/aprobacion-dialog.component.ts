import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Aprobacion } from './aprobacion.model';
import { AprobacionPopupService } from './aprobacion-popup.service';
import { AprobacionService } from './aprobacion.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { Principal, ResponseWrapper } from '../../shared';
import { ProductTypeFromUrlParamPipe } from '../../shared/pipes/productTypeFromUrlParam.pipe';
import { APPROVAL_PRODUCT_TYPE_BONOS_LETRAS } from '../../app.constants';
import { Subscription } from 'rxjs';

@Component({
    selector: 'jhi-aprobacion-dialog',
    templateUrl: './aprobacion-dialog.component.html'
})
export class AprobacionDialogComponent implements OnInit {

    aprobacion: Aprobacion;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    product: string = null;
    APPROVAL_PRODUCT_TYPE_BONOS_LETRAS = APPROVAL_PRODUCT_TYPE_BONOS_LETRAS;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private aprobacionService: AprobacionService, private eventManager: JhiEventManager,
                private annoTributaService: AnnoTributaService) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annotributas = res.json;
                // Se elimina el año tributario 0
                // this.annotributas.splice(0, 1);
            }, (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.aprobacion.id !== undefined) {
            this.subscribeToSaveResponse(this.aprobacionService.update(this.aprobacion));
        } else {
            this.subscribeToSaveResponse(this.aprobacionService.create(this.aprobacion));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<Aprobacion>) {
        result.subscribe((res: Aprobacion) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Aprobacion) {
        this.eventManager.broadcast({ name: 'aprobacionListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-aprobacion-popup',
    template: ''
})
export class AprobacionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    currentAccount: any;

    constructor(private route: ActivatedRoute, private aprobacionPopupService: AprobacionPopupService, private productTypeFromUrlParamPipe: ProductTypeFromUrlParamPipe,
                private principal: Principal) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.aprobacionPopupService.open(AprobacionDialogComponent as Component, params[ 'id' ]);
            } else {
                this.principal.identity().then((account) => {
                    this.currentAccount = account;
                    this.aprobacionPopupService.open(
                        AprobacionDialogComponent as Component, null, this.productTypeFromUrlParamPipe.transform(params[ 'product' ]), this.currentAccount);
                });
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
