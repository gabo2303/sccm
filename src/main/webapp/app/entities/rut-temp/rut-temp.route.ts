import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Rut_tempComponent } from './rut-temp.component';

export const rut_tempRoute: Routes = [
    {
        path: 'rut-temp',
        component: Rut_tempComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.rut_temp.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
