import { BaseEntity } from './../../shared';

export class Rut_temp implements BaseEntity {
    constructor(
        public id?: number,
        public rut?: number,
    ) {
    }
}
