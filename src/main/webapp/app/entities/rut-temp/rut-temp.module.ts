import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Rut_tempService,
    Rut_tempComponent,
    rut_tempRoute,
} from './';

const ENTITY_STATES = [
    ...rut_tempRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Rut_tempComponent,
    ],
    entryComponents: [
        Rut_tempComponent,
    ],
    providers: [
        Rut_tempService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmRut_tempModule {}
