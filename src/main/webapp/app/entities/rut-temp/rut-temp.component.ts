import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Rut_temp } from './rut-temp.model';
import { Rut_tempService } from './rut-temp.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-rut-temp',
    templateUrl: './rut-temp.component.html'
})
export class Rut_tempComponent implements OnInit, OnDestroy {
rut_temps: Rut_temp[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private rut_tempService: Rut_tempService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.rut_tempService.query().subscribe(
            (res: ResponseWrapper) => {
                this.rut_temps = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInRut_temps();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Rut_temp) {
        return item.id;
    }
    registerChangeInRut_temps() {
        this.eventSubscriber = this.eventManager.subscribe('rut_tempListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
