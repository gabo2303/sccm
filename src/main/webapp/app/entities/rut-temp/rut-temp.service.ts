import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Rut_temp } from './rut-temp.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class Rut_tempService {

    private resourceUrl = SERVER_API_URL + 'api/rut-temps';

    constructor(private http: Http) { }

    create(rut_temp: Rut_temp): Observable<Rut_temp> {
        const copy = this.convert(rut_temp);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(rut_temp: Rut_temp): Observable<Rut_temp> {
        const copy = this.convert(rut_temp);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Rut_temp> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Rut_temp.
     */
    private convertItemFromServer(json: any): Rut_temp {
        const entity: Rut_temp = Object.assign(new Rut_temp(), json);
        return entity;
    }

    /**
     * Convert a Rut_temp to a JSON which can be sent to the server.
     */
    private convert(rut_temp: Rut_temp): Rut_temp {
        const copy: Rut_temp = Object.assign({}, rut_temp);
        return copy;
    }
}
