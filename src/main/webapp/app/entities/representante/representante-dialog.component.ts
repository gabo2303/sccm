import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { Representante } from './representante.model';
import { RepresentantePopupService } from './representante-popup.service';
import { RepresentanteService } from './representante.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { Instrumento, InstrumentoService } from '../instrumento';
import { ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({ selector: 'jhi-representante-dialog', templateUrl: './representante-dialog.component.html' })
export class RepresentanteDialogComponent implements OnInit 
{
    representante: Representante;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    instrumentos: Instrumento[];

    constructor(public activeModal: NgbActiveModal, private dataUtils: JhiDataUtils, private jhiAlertService: JhiAlertService, private representanteService: RepresentanteService,
                private annoTributaService: AnnoTributaService, private instrumentoService: InstrumentoService, private eventManager: JhiEventManager,
                private spinnerService: Ng4LoadingSpinnerService) { }

    ngOnInit() 
	{
        this.isSaving = false;
        
		this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
		
        this.instrumentoService.query().subscribe((res: ResponseWrapper) => {
            this.instrumentos = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) { return this.dataUtils.byteSize(field); }

    openFile(contentType, field) { return this.dataUtils.openFile(contentType, field); }     

    setFileData(event, entity, field, isImage) { this.dataUtils.setFileData(event, entity, field, isImage); }

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.spinnerService.show();
        this.isSaving = true;
		
		if (this.representante.id !== undefined) 
		{
            this.subscribeToSaveResponse(this.representanteService.update(this.representante));
        } 
		else 
		{
            this.subscribeToSaveResponse(this.representanteService.create(this.representante));
        }
        
		this.spinnerService.hide();
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) { return item.id; }

    trackInstrumentoById(index: number, item: Instrumento) { return item.id; }

    private subscribeToSaveResponse(result: Observable<Representante>) 
	{
        result.subscribe((res: Representante) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Representante) 
	{
        this.eventManager.broadcast({ name: 'representanteListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }
}

@Component({ selector: 'jhi-representante-popup', template: '' })
export class RepresentantePopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;

    constructor(private route: ActivatedRoute, private representantePopupService: RepresentantePopupService) { }

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.representantePopupService.open(RepresentanteDialogComponent as Component, params[ 'id' ]);
            } else {
                this.representantePopupService.open(RepresentanteDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}