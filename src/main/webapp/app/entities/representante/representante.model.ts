import { BaseEntity } from './../../shared';

export class Representante implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public cargo?: string,
        public rut?: string,
        public firmaContentType?: string,
        public firma?: any,
        public annoTributa?: BaseEntity,
        public instrumento?: BaseEntity,
    ) {
    }
}
