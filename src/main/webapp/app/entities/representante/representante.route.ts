import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { RepresentanteComponent } from './representante.component';
import { RepresentanteDetailComponent } from './representante-detail.component';
import { RepresentantePopupComponent } from './representante-dialog.component';
import { RepresentanteDeletePopupComponent } from './representante-delete-dialog.component';
import { SCCM_1890_ADMIN, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1944_ADMIN, SCCM_1944_USER } from '../../app.constants';

export const representanteRoute: Routes = [
    {
        path: 'representante-57-b/:productId/show',
        component: RepresentanteComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'representante-pagdiv/:productId/show',
        component: RepresentanteComponent,
        data: {
            authorities: [ SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'representante-oper-capt/:productId/show',
        component: RepresentanteComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'representante-57-b/:productId/show/:id',
        component: RepresentanteDetailComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'representante-pagdiv/:productId/show/:id',
        component: RepresentanteDetailComponent,
        data: {
            authorities: [ SCCM_1941_ADMIN, SCCM_1941_USER ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'representante-oper-capt/:productId/show/:id',
        component: RepresentanteDetailComponent,
        data: {
            authorities: [ SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const representantePopupRoute: Routes = [
    {
        path: 'representante-new',
        component: RepresentantePopupComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }, {
        path: 'representante/:id/edit',
        component: RepresentantePopupComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }, {
        path: 'representante/:id/delete',
        component: RepresentanteDeletePopupComponent,
        data: {
            authorities: [ SCCM_1944_ADMIN, SCCM_1944_USER, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1890_ADMIN ],
            pageTitle: 'sccmApp.representante.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
