import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { Representante } from './representante.model';
import { RepresentanteService } from './representante.service';

@Component({
    selector: 'jhi-representante-detail',
    templateUrl: './representante-detail.component.html'
})
export class RepresentanteDetailComponent implements OnInit, OnDestroy {

    representante: Representante;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private representanteService: RepresentanteService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInRepresentantes();
    }

    load(id) {
        this.representanteService.find(id).subscribe((representante) => {
            this.representante = representante;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRepresentantes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'representanteListModification',
            (response) => this.load(this.representante.id)
        );
    }
}
