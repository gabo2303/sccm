import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { Representante } from './representante.model';
import { RepresentanteService } from './representante.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { Instrumento } from '../instrumento/instrumento.model';
import { InstrumentoService } from '../instrumento/instrumento.service';
import { Rut_tempService } from '../rut-temp/rut-temp.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-representante',
    templateUrl: './representante.component.html'
})
export class RepresentanteComponent implements OnInit, OnDestroy 
{
    representantes: Representante[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    representantesFlag = false;
    productId: number;
    instrumentsList: Instrumento [];
    representantesFlagSinData = false;
	canUse: boolean;
	flagResult = false;
	resultMsg = '';
    resultType = '';
    classType = '';

    constructor(private representanteService: RepresentanteService, private jhiAlertService: JhiAlertService, 
				private dataUtils: JhiDataUtils, private route: ActivatedRoute, private eventManager: JhiEventManager, 
				private principal: Principal, private annoTributaService: AnnoTributaService, 
				private instrumentoService: InstrumentoService, private spinnerService: Ng4LoadingSpinnerService,
				private rutTempService: Rut_tempService) {  }

    loadAll() 
	{
        this.representanteService.query().subscribe
		(
            (res: ResponseWrapper) => { this.representantes = res.json; },
			
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() 
	{
		this.rutTempService.query().subscribe
		(
			(res: ResponseWrapper) => 
			{
				if(res.json.length != 0) 
				{
					this.canUse = false;
					
					this.flagResult = true;
					this.resultMsg = "Proceso emision masiva de certificados esta en ejecucion. No permite realizar cambios.";
					this.resultType = 'Info: ';
					this.classType = 'alert alert-info alert-dismissible';
				}
				else { this.canUse = true; }
			},
			(res: ResponseWrapper) => {  }
		);
		
        this.loadAnnosTributarios();
        this.loadAllInstrumentos();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInRepresentantes();
        
		this.route.params.subscribe((params) => { this.productId = parseInt(params[ 'productId' ], 10); });
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: Representante) { return item.id; }

    byteSize(field) { return this.dataUtils.byteSize(field); }     

    openFile(contentType, field) { return this.dataUtils.openFile(contentType, field); }

    registerChangeInRepresentantes() 
	{
        this.eventSubscriber = this.eventManager.subscribe('representanteListModification', (response) => this.loadRepresentantesFiltered());
    }

    loadAnnosTributarios() 
	{
        this.spinnerService.show();
		
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => 
		{
            this.annosTributarios = res.json;
            this.spinnerService.hide();
        }, 
		(res: ResponseWrapper) => this.onError(res.json));
    }

    filterByAnooTributario(annoTributaId: any) 
	{
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadRepresentantesFiltered();
    }

    private loadRepresentantesFiltered() 
	{
        this.spinnerService.show();
        this.representantesFlag = false;
        this.representantesFlagSinData = false;
        
		this.representanteService.loadAllByAnnoTributaIdAndInsttumentoId(this.annoSelected, this.productId).subscribe
		(
            (res: ResponseWrapper) => 
			{
                this.representantes = res.json;
                console.log('this.representantes.length: ' + this.representantes.length);
                
				if (this.representantes.length > 0) { this.representantesFlag = true; } 
				
				else { this.representantesFlagSinData = true; }
				
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadAllInstrumentos() 
	{
        this.instrumentoService.query().subscribe
		(
            (res: ResponseWrapper) => { this.instrumentsList = res.json; },
			
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}