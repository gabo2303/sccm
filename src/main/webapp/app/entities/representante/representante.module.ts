import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    RepresentanteComponent,
    RepresentanteDeleteDialogComponent,
    RepresentanteDeletePopupComponent,
    RepresentanteDetailComponent,
    RepresentanteDialogComponent,
    RepresentantePopupComponent,
    representantePopupRoute,
    RepresentantePopupService,
    representanteRoute,
    RepresentanteService,
} from './';

const ENTITY_STATES = [
    ...representanteRoute,
    ...representantePopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RepresentanteComponent,
        RepresentanteDetailComponent,
        RepresentanteDialogComponent,
        RepresentanteDeleteDialogComponent,
        RepresentantePopupComponent,
        RepresentanteDeletePopupComponent,
    ],
    entryComponents: [
        RepresentanteComponent,
        RepresentanteDialogComponent,
        RepresentantePopupComponent,
        RepresentanteDeleteDialogComponent,
        RepresentanteDeletePopupComponent,
    ],
    providers: [
        RepresentanteService,
        RepresentantePopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmRepresentanteModule {
}
