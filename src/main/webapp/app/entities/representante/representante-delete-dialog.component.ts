import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Representante } from './representante.model';
import { RepresentantePopupService } from './representante-popup.service';
import { RepresentanteService } from './representante.service';

@Component({
    selector: 'jhi-representante-delete-dialog',
    templateUrl: './representante-delete-dialog.component.html'
})
export class RepresentanteDeleteDialogComponent {

    representante: Representante;

    constructor(private representanteService: RepresentanteService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.representanteService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'representanteListModification',
                content: 'Deleted an representante'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-representante-delete-popup',
    template: ''
})
export class RepresentanteDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private representantePopupService: RepresentantePopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.representantePopupService.open(RepresentanteDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
