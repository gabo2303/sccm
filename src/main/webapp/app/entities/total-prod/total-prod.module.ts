import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    TotalProdService,
    TotalProdPopupService,
    TotalProdComponent,
    TotalProdDetailComponent,
    TotalProdDialogComponent,
    TotalProdPopupComponent,
    TotalProdDeletePopupComponent,
    TotalProdDeleteDialogComponent,
    totalProdRoute,
    totalProdPopupRoute,
} from './';

const ENTITY_STATES = [
    ...totalProdRoute,
    ...totalProdPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TotalProdComponent,
        TotalProdDetailComponent,
        TotalProdDialogComponent,
        TotalProdDeleteDialogComponent,
        TotalProdPopupComponent,
        TotalProdDeletePopupComponent,
    ],
    entryComponents: [
        TotalProdComponent,
        TotalProdDialogComponent,
        TotalProdPopupComponent,
        TotalProdDeleteDialogComponent,
        TotalProdDeletePopupComponent,
    ],
    providers: [
        TotalProdService,
        TotalProdPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmTotalProdModule {}
