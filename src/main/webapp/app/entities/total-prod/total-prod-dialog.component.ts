import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TotalProd } from './total-prod.model';
import { TotalProdPopupService } from './total-prod-popup.service';
import { TotalProdService } from './total-prod.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-total-prod-dialog',
    templateUrl: './total-prod-dialog.component.html'
})
export class TotalProdDialogComponent implements OnInit {

    totalProd: TotalProd;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private totalProdService: TotalProdService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.totalProd.id !== undefined) {
            this.subscribeToSaveResponse(
                this.totalProdService.update(this.totalProd));
        } else {
            this.subscribeToSaveResponse(
                this.totalProdService.create(this.totalProd));
        }
    }

    private subscribeToSaveResponse(result: Observable<TotalProd>) {
        result.subscribe((res: TotalProd) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TotalProd) {
        this.eventManager.broadcast({ name: 'totalProdListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-total-prod-popup',
    template: ''
})
export class TotalProdPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private totalProdPopupService: TotalProdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.totalProdPopupService
                    .open(TotalProdDialogComponent as Component, params['id']);
            } else {
                this.totalProdPopupService
                    .open(TotalProdDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
