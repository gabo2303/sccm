import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { TotalProd } from './total-prod.model';
import { TotalProdService } from './total-prod.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-total-prod',
    templateUrl: './total-prod.component.html'
})
export class TotalProdComponent implements OnInit, OnDestroy {
totalProds: TotalProd[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private totalProdService: TotalProdService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.totalProdService.query().subscribe(
            (res: ResponseWrapper) => {
                this.totalProds = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTotalProds();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TotalProd) {
        return item.id;
    }
    registerChangeInTotalProds() {
        this.eventSubscriber = this.eventManager.subscribe('totalProdListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
