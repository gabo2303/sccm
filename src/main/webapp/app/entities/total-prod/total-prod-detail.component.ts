import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TotalProd } from './total-prod.model';
import { TotalProdService } from './total-prod.service';

@Component({
    selector: 'jhi-total-prod-detail',
    templateUrl: './total-prod-detail.component.html'
})
export class TotalProdDetailComponent implements OnInit, OnDestroy {

    totalProd: TotalProd;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private totalProdService: TotalProdService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTotalProds();
    }

    load(id) {
        this.totalProdService.find(id).subscribe((totalProd) => {
            this.totalProd = totalProd;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTotalProds() {
        this.eventSubscriber = this.eventManager.subscribe(
            'totalProdListModification',
            (response) => this.load(this.totalProd.id)
        );
    }
}
