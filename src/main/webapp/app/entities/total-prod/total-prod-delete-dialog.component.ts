import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TotalProd } from './total-prod.model';
import { TotalProdPopupService } from './total-prod-popup.service';
import { TotalProdService } from './total-prod.service';

@Component({
    selector: 'jhi-total-prod-delete-dialog',
    templateUrl: './total-prod-delete-dialog.component.html'
})
export class TotalProdDeleteDialogComponent {

    totalProd: TotalProd;

    constructor(
        private totalProdService: TotalProdService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.totalProdService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'totalProdListModification',
                content: 'Deleted an totalProd'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-total-prod-delete-popup',
    template: ''
})
export class TotalProdDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private totalProdPopupService: TotalProdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.totalProdPopupService
                .open(TotalProdDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
