import { BaseEntity } from './../../shared';

export class TotalProd implements BaseEntity {
    constructor(
        public id?: number,
        public origen?: string,
        public producto?: number,
        public codMon?: string,
        public intNom?: number,
        public intReal?: number,
        public montFin?: number,
        public bipersonal?: string,
        public impExt?: number,
        public annoTributa?: BaseEntity,
    ) {
    }
}
