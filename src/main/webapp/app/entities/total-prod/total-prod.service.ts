import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TotalProd } from './total-prod.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TotalProdService {

    private resourceUrl = SERVER_API_URL + 'api/total-prods';

    constructor(private http: Http) { }

    create(totalProd: TotalProd): Observable<TotalProd> {
        const copy = this.convert(totalProd);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(totalProd: TotalProd): Observable<TotalProd> {
        const copy = this.convert(totalProd);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TotalProd> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TotalProd.
     */
    private convertItemFromServer(json: any): TotalProd {
        const entity: TotalProd = Object.assign(new TotalProd(), json);
        return entity;
    }

    /**
     * Convert a TotalProd to a JSON which can be sent to the server.
     */
    private convert(totalProd: TotalProd): TotalProd {
        const copy: TotalProd = Object.assign({}, totalProd);
        return copy;
    }
}
