export * from './total-prod.model';
export * from './total-prod-popup.service';
export * from './total-prod.service';
export * from './total-prod-dialog.component';
export * from './total-prod-delete-dialog.component';
export * from './total-prod-detail.component';
export * from './total-prod.component';
export * from './total-prod.route';
