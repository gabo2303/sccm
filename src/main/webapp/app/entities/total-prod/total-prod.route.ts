import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TotalProdComponent } from './total-prod.component';
import { TotalProdDetailComponent } from './total-prod-detail.component';
import { TotalProdPopupComponent } from './total-prod-dialog.component';
import { TotalProdDeletePopupComponent } from './total-prod-delete-dialog.component';

export const totalProdRoute: Routes = [
    {
        path: 'total-prod',
        component: TotalProdComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.totalProd.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'total-prod/:id',
        component: TotalProdDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.totalProd.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const totalProdPopupRoute: Routes = [
    {
        path: 'total-prod-new',
        component: TotalProdPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.totalProd.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'total-prod/:id/edit',
        component: TotalProdPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.totalProd.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'total-prod/:id/delete',
        component: TotalProdDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.totalProd.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
