import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Cert57B } from './cert-57-b.model';
import { Cert57BService } from './cert-57-b.service';

@Injectable()
export class Cert57BPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private cert57BService: Cert57BService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cert57BService.find(id).subscribe((cert57B) => {
                    this.ngbModalRef = this.cert57BModalRef(component, cert57B);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cert57BModalRef(component, new Cert57B());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cert57BModalRef(component: Component, cert57B: Cert57B): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.cert57B = cert57B;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
