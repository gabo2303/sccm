import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { Cert57BComponent } from './cert-57-b.component';
import { Cert57BDetailComponent } from './cert-57-b-detail.component';
import { Cert57BPopupComponent } from './cert-57-b-dialog.component';
import { Cert57BDeletePopupComponent } from './cert-57-b-delete-dialog.component';

export const cert57BRoute: Routes = [
    {
        path: 'cert-57-b',
        component: Cert57BComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cert57B.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'cert-57-b/:id',
        component: Cert57BDetailComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cert57B.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const cert57BPopupRoute: Routes = [
    {
        path: 'cert-57-b-new',
        component: Cert57BPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cert57B.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cert-57-b/:id/edit',
        component: Cert57BPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cert57B.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'cert-57-b/:id/delete',
        component: Cert57BDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1944_ADMIN', 'ROLE_SCCM_1944_USER' ],
            pageTitle: 'sccmApp.cert57B.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
