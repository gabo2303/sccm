import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../../shared';
import {
    Cert57BComponent,
    Cert57BDeleteDialogComponent,
    Cert57BDeletePopupComponent,
    Cert57BDetailComponent,
    Cert57BDialogComponent,
    Cert57BPopupComponent,
    cert57BPopupRoute,
    Cert57BPopupService,
    cert57BRoute,
    Cert57BService,
} from './';

const ENTITY_STATES = [
    ...cert57BRoute,
    ...cert57BPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Cert57BComponent,
        Cert57BDetailComponent,
        Cert57BDialogComponent,
        Cert57BDeleteDialogComponent,
        Cert57BPopupComponent,
        Cert57BDeletePopupComponent,
    ],
    entryComponents: [
        Cert57BComponent,
        Cert57BDialogComponent,
        Cert57BPopupComponent,
        Cert57BDeleteDialogComponent,
        Cert57BDeletePopupComponent,
    ],
    providers: [
        Cert57BService,
        Cert57BPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmCert57BModule {
}
