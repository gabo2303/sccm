import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Cert57B } from './cert-57-b.model';
import { Cert57BService } from './cert-57-b.service';

@Component({
    selector: 'jhi-cert-57-b-detail',
    templateUrl: './cert-57-b-detail.component.html'
})
export class Cert57BDetailComponent implements OnInit, OnDestroy {

    cert57B: Cert57B;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private cert57BService: Cert57BService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInCert57BS();
    }

    load(id) {
        this.cert57BService.find(id).subscribe((cert57B) => {
            this.cert57B = cert57B;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCert57BS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cert57BListModification',
            (response) => this.load(this.cert57B.id)
        );
    }
}
