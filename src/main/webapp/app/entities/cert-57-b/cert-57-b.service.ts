import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { Cert57B } from './cert-57-b.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class Cert57BService {

    private resourceUrl = SERVER_API_URL + 'api/cert-57-bs';

    constructor(private http: Http) {
    }

    create(cert57B: Cert57B): Observable<Cert57B> {
        const copy = this.convert(cert57B);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(cert57B: Cert57B): Observable<Cert57B> {
        const copy = this.convert(cert57B);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Cert57B> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Cert57B.
     */
    private convertItemFromServer(json: any): Cert57B {
        const entity: Cert57B = Object.assign(new Cert57B(), json);
        return entity;
    }

    /**
     * Convert a Cert57B to a JSON which can be sent to the server.
     */
    private convert(cert57B: Cert57B): Cert57B {
        const copy: Cert57B = Object.assign({}, cert57B);
        return copy;
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`)
        .map((res: Response) => this.convertResponse(res));
    }
}
