import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Cert57B } from './cert-57-b.model';
import { Cert57BPopupService } from './cert-57-b-popup.service';
import { Cert57BService } from './cert-57-b.service';

@Component({
    selector: 'jhi-cert-57-b-delete-dialog',
    templateUrl: './cert-57-b-delete-dialog.component.html'
})
export class Cert57BDeleteDialogComponent {

    cert57B: Cert57B;

    constructor(
        private cert57BService: Cert57BService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cert57BService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cert57BListModification',
                content: 'Deleted an cert57B'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cert-57-b-delete-popup',
    template: ''
})
export class Cert57BDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cert57BPopupService: Cert57BPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cert57BPopupService
            .open(Cert57BDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
