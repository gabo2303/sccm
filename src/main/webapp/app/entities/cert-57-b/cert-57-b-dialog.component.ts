import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Cert57B } from './cert-57-b.model';
import { Cert57BPopupService } from './cert-57-b-popup.service';
import { Cert57BService } from './cert-57-b.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ClienteBice, ClienteBiceService } from '../cliente-bice';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cert-57-b-dialog',
    templateUrl: './cert-57-b-dialog.component.html'
})
export class Cert57BDialogComponent implements OnInit {

    cert57B: Cert57B;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    clientebices: ClienteBice[];

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private cert57BService: Cert57BService,
                private annoTributaService: AnnoTributaService, private clienteBiceService: ClienteBiceService, private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
        .subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
        this.clienteBiceService.query()
        .subscribe((res: ResponseWrapper) => {
            this.clientebices = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cert57B.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cert57BService.update(this.cert57B));
        } else {
            this.subscribeToSaveResponse(
                this.cert57BService.create(this.cert57B));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackClienteBiceById(index: number, item: ClienteBice) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<Cert57B>) {
        result.subscribe((res: Cert57B) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Cert57B) {
        this.eventManager.broadcast({ name: 'cert57BListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-cert-57-b-popup',
    template: ''
})
export class Cert57BPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private cert57BPopupService: Cert57BPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.cert57BPopupService
                .open(Cert57BDialogComponent as Component, params[ 'id' ]);
            } else {
                this.cert57BPopupService
                .open(Cert57BDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
