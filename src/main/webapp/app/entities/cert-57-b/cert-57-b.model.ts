import { BaseEntity } from './../../shared';

export class Cert57B implements BaseEntity {
    constructor(
        public id?: number,
        public saldoNetoPer?: number,
        public montoRentabilidad?: number,
        public annoTributa?: BaseEntity,
        public clienteBice?: BaseEntity,
    ) {
    }
}
