import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Cert57B } from './cert-57-b.model';
import { Cert57BService } from './cert-57-b.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { ClienteBiceService } from '../cliente-bice/cliente-bice.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ClienteBice } from '../cliente-bice/cliente-bice.model';
import { ExcelService } from '../../shared/excel/ExcelService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-cert-57-b',
    templateUrl: './cert-57-b.component.html'
})
export class Cert57BComponent implements OnInit, OnDestroy {
    cert57BS: Cert57B[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annoSelected: number;
    cert57Flag = false;
    cert57FlagSinData = false;
    annosTributarios: AnnoTributa[];
    clientesList: ClienteBice [];

    constructor(private cert57BService: Cert57BService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private clientesService: ClienteBiceService, private excelService: ExcelService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadAllClientes();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCert57BS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Cert57B) {
        return item.id;
    }

    registerChangeInCert57BS() {
        this.eventSubscriber = this.eventManager.subscribe('cert57BListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    filterByAnooTributario(annoTributaId: any) {
        this.spinnerService.show();
        console.log('annoTributaId: ' + annoTributaId.value);
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.cert57Flag = false;
        this.cert57FlagSinData = false;
        this.cert57BService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.cert57BS = res.json;
                console.log('this.cert57BS.length: ' + this.cert57BS.length);
                if (this.cert57BS.length > 0) {
                    this.cert57Flag = true;
                } else {
                    this.cert57FlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadAllClientes() {
        this.clientesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.clientesList = res.json;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    generateExcel() {
        const rowCount = (<HTMLTableElement> document.getElementById('table')).rows.length;
        if (rowCount > 1) {
            let textos = '[';
            const headers: any [] = [];
            const table = (<HTMLTableElement> document.getElementById('table'));
            for (let i = 0; i < (<HTMLTableElement> document.getElementById('table')).rows.length; i++) {
                if (i > 0) {
                    textos += '{';
                }
                for (let j = 0; j < 4; j++) {
                    if (i === 0) {
                        // Obtenemos la primera fila de la tabla o cabecera y limpiamos tag html y espacios vacíos
                        headers[ j ] =
                            (<HTMLTableElement> document.getElementById('table')).rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim();
                    } else {
                        if (j < 3) {
                            textos += '"' + headers[ j ] + '" : "' +
                                (<HTMLTableElement> document.getElementById('table'))
                                    .rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '",';
                        } else {
                            textos += '"' + headers[ j ] + '" : "' + (<HTMLTableElement> document.getElementById('table'))
                                .rows[ i ].cells[ j ].innerHTML.replace(/<[^>]*>/g, '').replace('&nbsp;', '').trim() + '"';
                        }
                    }
                }
                if (i > 0 && i < (<HTMLTableElement> document.getElementById('table')).rows.length - 1) {
                    textos += '},';
                }
                if (i === (<HTMLTableElement> document.getElementById('table')).rows.length - 1) {
                    textos += '}]';
                }
            }
            this.excelService.exportAsExcelFile(
                JSON.parse(textos.toString()),
                'Datos Certificado 57 Bis',
                'Datos Certificado 57 Bis');
        } else {
            this.jhiAlertService.error('No existen registros para exportar');
        }
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

}
