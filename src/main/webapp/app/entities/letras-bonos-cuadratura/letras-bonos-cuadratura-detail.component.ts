import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LetrasBonosCuadratura } from './letras-bonos-cuadratura.model';
import { LetrasBonosCuadraturaService } from './letras-bonos-cuadratura.service';

@Component({
    selector: 'jhi-letras-bonos-cuadratura-detail',
    templateUrl: './letras-bonos-cuadratura-detail.component.html'
})
export class LetrasBonosCuadraturaDetailComponent implements OnInit, OnDestroy {

    letrasBonosCuadratura: LetrasBonosCuadratura;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private letrasBonosCuadraturaService: LetrasBonosCuadraturaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLetrasBonosCuadraturas();
    }

    load(id) {
        this.letrasBonosCuadraturaService.find(id).subscribe((letrasBonosCuadratura) => {
            this.letrasBonosCuadratura = letrasBonosCuadratura;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLetrasBonosCuadraturas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'letrasBonosCuadraturaListModification',
            (response) => this.load(this.letrasBonosCuadratura.id)
        );
    }
}
