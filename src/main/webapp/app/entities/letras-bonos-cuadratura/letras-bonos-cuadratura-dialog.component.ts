import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LetrasBonosCuadratura } from './letras-bonos-cuadratura.model';
import { LetrasBonosCuadraturaPopupService } from './letras-bonos-cuadratura-popup.service';
import { LetrasBonosCuadraturaService } from './letras-bonos-cuadratura.service';

@Component({
    selector: 'jhi-letras-bonos-cuadratura-dialog',
    templateUrl: './letras-bonos-cuadratura-dialog.component.html'
})
export class LetrasBonosCuadraturaDialogComponent implements OnInit {

    letrasBonosCuadratura: LetrasBonosCuadratura;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private letrasBonosCuadraturaService: LetrasBonosCuadraturaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.letrasBonosCuadratura.id !== undefined) {
            this.subscribeToSaveResponse(
                this.letrasBonosCuadraturaService.update(this.letrasBonosCuadratura));
        } else {
            this.subscribeToSaveResponse(
                this.letrasBonosCuadraturaService.create(this.letrasBonosCuadratura));
        }
    }

    private subscribeToSaveResponse(result: Observable<LetrasBonosCuadratura>) {
        result.subscribe((res: LetrasBonosCuadratura) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LetrasBonosCuadratura) {
        this.eventManager.broadcast({ name: 'letrasBonosCuadraturaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-letras-bonos-cuadratura-popup',
    template: ''
})
export class LetrasBonosCuadraturaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private letrasBonosCuadraturaPopupService: LetrasBonosCuadraturaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.letrasBonosCuadraturaPopupService
                    .open(LetrasBonosCuadraturaDialogComponent as Component, params['id']);
            } else {
                this.letrasBonosCuadraturaPopupService
                    .open(LetrasBonosCuadraturaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
