import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LetrasBonosCuadratura } from './letras-bonos-cuadratura.model';
import { LetrasBonosCuadraturaService } from './letras-bonos-cuadratura.service';

@Injectable()
export class LetrasBonosCuadraturaPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private letrasBonosCuadraturaService: LetrasBonosCuadraturaService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.letrasBonosCuadraturaService.find(id).subscribe((letrasBonosCuadratura) => {
                    this.ngbModalRef = this.letrasBonosCuadraturaModalRef(component, letrasBonosCuadratura);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.letrasBonosCuadraturaModalRef(component, new LetrasBonosCuadratura());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    letrasBonosCuadraturaModalRef(component: Component, letrasBonosCuadratura: LetrasBonosCuadratura): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.letrasBonosCuadratura = letrasBonosCuadratura;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
