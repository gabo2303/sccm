export class LetrasBonosCNoNemoDto {
    constructor(
        public anno?: string,
        public mes?: string,
        public moneda?: number,
        public montoPagado?: any,
        public origen?: string,
        public valorNominal?: any,
    ) {
    }
}
