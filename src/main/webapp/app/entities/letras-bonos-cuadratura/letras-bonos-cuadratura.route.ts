import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LetrasBonosCuadraturaComponent } from './letras-bonos-cuadratura.component';
import { LetrasBonosCuadraturaDetailComponent } from './letras-bonos-cuadratura-detail.component';
import { SCCM_1890_USER_BYL } from '../../app.constants';

@Injectable()
export class LetrasBonosCuadraturaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const letrasBonosCuadraturaRoute: Routes = [
    {
        path: 'letras-bonos-cuadratura',
        component: LetrasBonosCuadraturaComponent,
        resolve: {
            'pagingParams': LetrasBonosCuadraturaResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'sccmApp.letrasBonosCuadratura.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'letras-bonos-cuadratura/:id',
        component: LetrasBonosCuadraturaDetailComponent,
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'sccmApp.letrasBonosCuadratura.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const letrasBonosCuadraturaPopupRoute: Routes = [
    /*{
        path: 'letras-bonos-cuadratura-new',
        component: LetrasBonosCuadraturaPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.letrasBonosCuadratura.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'letras-bonos-cuadratura/:id/edit',
        component: LetrasBonosCuadraturaPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.letrasBonosCuadratura.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'letras-bonos-cuadratura/:id/delete',
        component: LetrasBonosCuadraturaDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.letrasBonosCuadratura.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }*/
];
