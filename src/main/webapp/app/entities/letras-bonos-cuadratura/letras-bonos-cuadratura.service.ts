import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { LetrasBonosCuadratura } from './letras-bonos-cuadratura.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { LetrasBonosCuadraturaDto } from './letras-bonos-cuadratura-dto.model';
import { LetrasBonosCNoNemoDto } from './letras-bonos-c-no-nemo-dto.model';

@Injectable()
export class LetrasBonosCuadraturaService {

    private resourceUrl = SERVER_API_URL + 'api/letras-bonos-cuadraturas';

    constructor(private http: Http) {
    }

    findByAllByTaxYearId(idYear: number, withNemotecnico: boolean, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearId`, options).map((res: Response) => this.convertResponse2(res, withNemotecnico));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    create(letrasBonosCuadratura: LetrasBonosCuadratura): Observable<LetrasBonosCuadratura> {
        const copy = this.convert(letrasBonosCuadratura);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(letrasBonosCuadratura: LetrasBonosCuadratura): Observable<LetrasBonosCuadratura> {
        const copy = this.convert(letrasBonosCuadratura);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<LetrasBonosCuadratura> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse2(res: Response, withNemotecnico: boolean): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        if (withNemotecnico) {
            for (let i = 0; i < jsonResponse.length; i++) {
                result.push(this.convertItemFromServerNemo(jsonResponse[ i ]));
            }
        } else {
            for (let i = 0; i < jsonResponse.length; i++) {
                result.push(this.convertItemFromServerNoNemo(jsonResponse[ i ]));
            }
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LetrasBonosCuadraturaDto.
     */
    private convertItemFromServerNemo(json: any): LetrasBonosCuadraturaDto {
        const entity: LetrasBonosCuadraturaDto = Object.assign(new LetrasBonosCuadraturaDto(), json);
        return entity;
    }

    /**
     * Convert a returned JSON object to LetrasBonosCNoNemoDto.
     */
    private convertItemFromServerNoNemo(json: any): LetrasBonosCNoNemoDto {
        const entity: LetrasBonosCNoNemoDto = new LetrasBonosCNoNemoDto(json.anno, json.mes, json.moneda, json.montoPagado, json.origen, json.valorNominal);
        return entity;
    }

    /**
     * Convert a returned JSON object to LetrasBonosCuadratura.
     */
    private convertItemFromServer(json: any): LetrasBonosCuadratura {
        const entity: LetrasBonosCuadratura = Object.assign(new LetrasBonosCuadratura(), json);
        return entity;
    }

    /**
     * Convert a LetrasBonosCuadratura to a JSON which can be sent to the server.
     */
    private convert(letrasBonosCuadratura: LetrasBonosCuadratura): LetrasBonosCuadratura {
        const copy: LetrasBonosCuadratura = Object.assign({}, letrasBonosCuadratura);
        return copy;
    }
}
