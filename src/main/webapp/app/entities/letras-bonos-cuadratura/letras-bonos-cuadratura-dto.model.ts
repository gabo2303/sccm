export class LetrasBonosCuadraturaDto {
    constructor(
        public anno?: string,
        public instrumento?: string,
        public mes?: string,
        public moneda?: number,
        public montoPagado?: any,
        public origen?: string,
        public valorNominal?: any,
    ) {
    }
}
