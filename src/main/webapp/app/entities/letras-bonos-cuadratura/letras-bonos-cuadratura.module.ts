import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    LetrasBonosCuadraturaService,
    LetrasBonosCuadraturaPopupService,
    LetrasBonosCuadraturaComponent,
    LetrasBonosCuadraturaDetailComponent,
    LetrasBonosCuadraturaDialogComponent,
    LetrasBonosCuadraturaPopupComponent,
    LetrasBonosCuadraturaDeletePopupComponent,
    LetrasBonosCuadraturaDeleteDialogComponent,
    letrasBonosCuadraturaRoute,
    letrasBonosCuadraturaPopupRoute,
    LetrasBonosCuadraturaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...letrasBonosCuadraturaRoute,
    ...letrasBonosCuadraturaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LetrasBonosCuadraturaComponent,
        LetrasBonosCuadraturaDetailComponent,
        LetrasBonosCuadraturaDialogComponent,
        LetrasBonosCuadraturaDeleteDialogComponent,
        LetrasBonosCuadraturaPopupComponent,
        LetrasBonosCuadraturaDeletePopupComponent,
    ],
    entryComponents: [
        LetrasBonosCuadraturaComponent,
        LetrasBonosCuadraturaDialogComponent,
        LetrasBonosCuadraturaPopupComponent,
        LetrasBonosCuadraturaDeleteDialogComponent,
        LetrasBonosCuadraturaDeletePopupComponent,
    ],
    providers: [
        LetrasBonosCuadraturaService,
        LetrasBonosCuadraturaPopupService,
        LetrasBonosCuadraturaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmLetrasBonosCuadraturaModule {}
