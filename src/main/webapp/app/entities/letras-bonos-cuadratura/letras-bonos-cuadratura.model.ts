import { BaseEntity } from './../../shared';

export class LetrasBonosCuadratura implements BaseEntity {
    constructor(
        public id?: number,
        public annoTributaId?: number,
        public anno?: string,
        public instrumento?: string,
        public mes?: string,
        public moneda?: number,
        public montoPagado?: number,
        public origen?: string,
        public valorNominal?: number,
    ) {
    }
}
