import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LetrasBonosCuadratura } from './letras-bonos-cuadratura.model';
import { LetrasBonosCuadraturaPopupService } from './letras-bonos-cuadratura-popup.service';
import { LetrasBonosCuadraturaService } from './letras-bonos-cuadratura.service';

@Component({
    selector: 'jhi-letras-bonos-cuadratura-delete-dialog',
    templateUrl: './letras-bonos-cuadratura-delete-dialog.component.html'
})
export class LetrasBonosCuadraturaDeleteDialogComponent {

    letrasBonosCuadratura: LetrasBonosCuadratura;

    constructor(
        private letrasBonosCuadraturaService: LetrasBonosCuadraturaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.letrasBonosCuadraturaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'letrasBonosCuadraturaListModification',
                content: 'Deleted an letrasBonosCuadratura'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-letras-bonos-cuadratura-delete-popup',
    template: ''
})
export class LetrasBonosCuadraturaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private letrasBonosCuadraturaPopupService: LetrasBonosCuadraturaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.letrasBonosCuadraturaPopupService
                .open(LetrasBonosCuadraturaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
