import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Temporal1890Service,
    Temporal1890PopupService,
    Temporal1890Component,
    Temporal1890DetailComponent,
    Temporal1890DialogComponent,
    Temporal1890PopupComponent,
    Temporal1890DeletePopupComponent,
    Temporal1890DeleteDialogComponent,
    temporal1890Route,
    temporal1890PopupRoute,
} from './';

const ENTITY_STATES = [
    ...temporal1890Route,
    ...temporal1890PopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Temporal1890Component,
        Temporal1890DetailComponent,
        Temporal1890DialogComponent,
        Temporal1890DeleteDialogComponent,
        Temporal1890PopupComponent,
        Temporal1890DeletePopupComponent,
    ],
    entryComponents: [
        Temporal1890Component,
        Temporal1890DialogComponent,
        Temporal1890PopupComponent,
        Temporal1890DeleteDialogComponent,
        Temporal1890DeletePopupComponent,
    ],
    providers: [
        Temporal1890Service,
        Temporal1890PopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmTemporal1890Module {}
