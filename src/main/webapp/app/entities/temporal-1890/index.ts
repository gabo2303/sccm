export * from './temporal-1890.model';
export * from './temporal-1890-popup.service';
export * from './temporal-1890.service';
export * from './temporal-1890-dialog.component';
export * from './temporal-1890-delete-dialog.component';
export * from './temporal-1890-detail.component';
export * from './temporal-1890.component';
export * from './temporal-1890.route';
