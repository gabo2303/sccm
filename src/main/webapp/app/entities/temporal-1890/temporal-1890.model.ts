import { BaseEntity } from './../../shared';

export class Temporal1890 implements BaseEntity {
    constructor(
        public id?: number,
        public rut?: number,
        public dv?: string,
        public totalcoluno?: number,
        public totalcolunoneg?: number,
        public totalcoldos?: number,
        public totalcoldosneg?: number,
        public totalcoltres?: number,
        public totalcoltresneg?: number,
        public totalcolcuatro?: number,
        public totalcolcuatroneg?: number,
        public totalcolcinco?: number,
        public totalcolcinconeg?: number,
        public totalcolseis?: number,
        public totalcolseisneg?: number,
        public tip_16?: number,
        public tip_17?: number,
        public nro_cer?: number,
        public annoTributa?: BaseEntity,
    ) {
    }
}
