import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Temporal1890 } from './temporal-1890.model';
import { Temporal1890Service } from './temporal-1890.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-temporal-1890',
    templateUrl: './temporal-1890.component.html'
})
export class Temporal1890Component implements OnInit, OnDestroy {
temporal1890S: Temporal1890[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private temporal1890Service: Temporal1890Service,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.temporal1890Service.query().subscribe(
            (res: ResponseWrapper) => {
                this.temporal1890S = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTemporal1890S();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Temporal1890) {
        return item.id;
    }
    registerChangeInTemporal1890S() {
        this.eventSubscriber = this.eventManager.subscribe('temporal1890ListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
