import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Temporal1890 } from './temporal-1890.model';
import { Temporal1890Service } from './temporal-1890.service';

@Component({
    selector: 'jhi-temporal-1890-detail',
    templateUrl: './temporal-1890-detail.component.html'
})
export class Temporal1890DetailComponent implements OnInit, OnDestroy {

    temporal1890: Temporal1890;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private temporal1890Service: Temporal1890Service,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTemporal1890S();
    }

    load(id) {
        this.temporal1890Service.find(id).subscribe((temporal1890) => {
            this.temporal1890 = temporal1890;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTemporal1890S() {
        this.eventSubscriber = this.eventManager.subscribe(
            'temporal1890ListModification',
            (response) => this.load(this.temporal1890.id)
        );
    }
}
