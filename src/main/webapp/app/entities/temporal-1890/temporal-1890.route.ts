import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Temporal1890Component } from './temporal-1890.component';
import { Temporal1890DetailComponent } from './temporal-1890-detail.component';
import { Temporal1890PopupComponent } from './temporal-1890-dialog.component';
import { Temporal1890DeletePopupComponent } from './temporal-1890-delete-dialog.component';

export const temporal1890Route: Routes = [
    {
        path: 'temporal-1890',
        component: Temporal1890Component,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.temporal1890.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'temporal-1890/:id',
        component: Temporal1890DetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.temporal1890.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const temporal1890PopupRoute: Routes = [
    {
        path: 'temporal-1890-new',
        component: Temporal1890PopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.temporal1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'temporal-1890/:id/edit',
        component: Temporal1890PopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.temporal1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'temporal-1890/:id/delete',
        component: Temporal1890DeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.temporal1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
