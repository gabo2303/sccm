import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Temporal1890 } from './temporal-1890.model';
import { Temporal1890PopupService } from './temporal-1890-popup.service';
import { Temporal1890Service } from './temporal-1890.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-temporal-1890-dialog',
    templateUrl: './temporal-1890-dialog.component.html'
})
export class Temporal1890DialogComponent implements OnInit {

    temporal1890: Temporal1890;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private temporal1890Service: Temporal1890Service,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.temporal1890.id !== undefined) {
            this.subscribeToSaveResponse(
                this.temporal1890Service.update(this.temporal1890));
        } else {
            this.subscribeToSaveResponse(
                this.temporal1890Service.create(this.temporal1890));
        }
    }

    private subscribeToSaveResponse(result: Observable<Temporal1890>) {
        result.subscribe((res: Temporal1890) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Temporal1890) {
        this.eventManager.broadcast({ name: 'temporal1890ListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-temporal-1890-popup',
    template: ''
})
export class Temporal1890PopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private temporal1890PopupService: Temporal1890PopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.temporal1890PopupService
                    .open(Temporal1890DialogComponent as Component, params['id']);
            } else {
                this.temporal1890PopupService
                    .open(Temporal1890DialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
