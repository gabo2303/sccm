import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Temporal1890 } from './temporal-1890.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class Temporal1890Service {

    private resourceUrl = SERVER_API_URL + 'api/temporal-1890-s';

    constructor(private http: Http) { }

    create(temporal1890: Temporal1890): Observable<Temporal1890> {
        const copy = this.convert(temporal1890);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(temporal1890: Temporal1890): Observable<Temporal1890> {
        const copy = this.convert(temporal1890);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Temporal1890> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Temporal1890.
     */
    private convertItemFromServer(json: any): Temporal1890 {
        const entity: Temporal1890 = Object.assign(new Temporal1890(), json);
        return entity;
    }

    /**
     * Convert a Temporal1890 to a JSON which can be sent to the server.
     */
    private convert(temporal1890: Temporal1890): Temporal1890 {
        const copy: Temporal1890 = Object.assign({}, temporal1890);
        return copy;
    }
}
