import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Temporal1890 } from './temporal-1890.model';
import { Temporal1890PopupService } from './temporal-1890-popup.service';
import { Temporal1890Service } from './temporal-1890.service';

@Component({
    selector: 'jhi-temporal-1890-delete-dialog',
    templateUrl: './temporal-1890-delete-dialog.component.html'
})
export class Temporal1890DeleteDialogComponent {

    temporal1890: Temporal1890;

    constructor(
        private temporal1890Service: Temporal1890Service,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.temporal1890Service.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'temporal1890ListModification',
                content: 'Deleted an temporal1890'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-temporal-1890-delete-popup',
    template: ''
})
export class Temporal1890DeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private temporal1890PopupService: Temporal1890PopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.temporal1890PopupService
                .open(Temporal1890DeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
