import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { LetrasBonos } from './letras-bonos.model';
import { LetrasBonosService } from './letras-bonos.service';
import { CommonServices, ITEMS_PER_PAGE, PATTERN_DATE_SHORT, Principal, ResponseWrapper, SELECT_BLANK_OPTION } from '../../shared';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { TablaMoneda } from '../tabla-moneda/tabla-moneda.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ExcelService } from '../../shared/excel/ExcelService';
import { TablaMonedaService } from '../tabla-moneda/tabla-moneda.service';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'jhi-letras-bonos',
    templateUrl: './letras-bonos.component.html',
    styleUrls: [
        'letras-bonos.css'
    ]
})
export class LetrasBonosComponent implements OnInit, OnDestroy {

    currentAccount: any;
    letrasBonos: LetrasBonos[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    annosTributarios: AnnoTributa[];
    tablaMonedas: TablaMoneda[];
    annoSelected: number;
    letrasBonosFlag = false;
    letrasBonosFlagSinData = false;
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    SELECT_BLANK_OPTION = SELECT_BLANK_OPTION;

    constructor(private letrasBonosService: LetrasBonosService, private parseLinks: JhiParseLinks, private jhiAlertService: JhiAlertService, private principal: Principal,
                private activatedRoute: ActivatedRoute, private router: Router, private eventManager: JhiEventManager, private spinnerService: Ng4LoadingSpinnerService,
                private annoTributaService: AnnoTributaService, private commonServices: CommonServices, private tablaMonedaService: TablaMonedaService,
                private excelService: ExcelService) {

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
    }

    loadAll() {
        this.spinnerService.show();
        this.letrasBonosService.findByAllByTaxYearIdPaginated(this.annoSelected, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        this.spinnerService.show();
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate([ 'letras-bonos' ], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage
                }
        });
        this.loadAll();
    }

    ngOnInit() {
        // this.loadAll();
        this.loadAllTablaMoneda();
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLetrasBonos();
    }

    loadAllTablaMoneda() {
        this.tablaMonedaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tablaMonedas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: LetrasBonos) {
        return item.id;
    }

    registerChangeInLetrasBonos() {
        this.eventSubscriber = this.eventManager.subscribe('letrasBonosListModification', (response) => this.loadAll());
    }

    filterByAnnoTributarioId(annoTributaId: HTMLSelectElement) {
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.page = 1;
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(yearId: number) {
        this.letrasBonosFlag = false;
        this.letrasBonosFlagSinData = false;
        this.spinnerService.show();
        this.letrasBonosService.findByAllByTaxYearIdPaginated(yearId, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    generateExcel2() {
        this.spinnerService.show();
        this.letrasBonosService.findByAllByTaxYearId(this.annoSelected).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, true),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    generateExcel() {
        this.commonServices.generateExcel(document, 'main-data-table', this.excelService, this.eventManager, 'Datos Bonos y Letras',
            'letrasBonosListModification', 'Deleted an letrasBonos', this.jhiAlertService, 'No existen registros para exportar');
    }

    convertCodMonToStr(codMon): string {
        codMon = parseInt(codMon, 10);
        const find = this.tablaMonedas.find((elem) => {
            return elem.codMon === codMon;
        });
        if (isNullOrUndefined(find)) {
            console.log('ERROR ' + codMon);
            return '---';
        }
        return find.glosa;
    }

    sort() {
        const result = [ this.predicate + ',' + (this.reverse ? 'asc' : 'desc') ];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    clear() {
        this.page = 0;
        this.router.navigate([ 'letras-bonos', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        } ]);
        this.loadAll();
    }

    private onSuccess(data, headers, downloadFile?) {
        if (downloadFile) {
            this.commonServices.download(data, 'Datos Bonos y Letras');
        } else {
            this.links = this.parseLinks.parse(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            // this.page = pagingParams.page;
            this.letrasBonos = data;
            if (this.letrasBonos.length > 0) {
                this.letrasBonosFlag = true;
            } else {
                this.letrasBonosFlagSinData = true;
            }
        }
        this.spinnerService.hide();
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}
