import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LetrasBonos } from './letras-bonos.model';
import { LetrasBonosService } from './letras-bonos.service';

@Component({
    selector: 'jhi-letras-bonos-detail',
    templateUrl: './letras-bonos-detail.component.html'
})
export class LetrasBonosDetailComponent implements OnInit, OnDestroy {

    letrasBonos: LetrasBonos;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private letrasBonosService: LetrasBonosService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLetrasBonos();
    }

    load(id) {
        this.letrasBonosService.find(id).subscribe((letrasBonos) => {
            this.letrasBonos = letrasBonos;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLetrasBonos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'letrasBonosListModification',
            (response) => this.load(this.letrasBonos.id)
        );
    }
}
