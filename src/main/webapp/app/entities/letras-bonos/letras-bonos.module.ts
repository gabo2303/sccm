import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    LetrasBonosService,
    LetrasBonosPopupService,
    LetrasBonosComponent,
    LetrasBonosDetailComponent,
    LetrasBonosDialogComponent,
    LetrasBonosPopupComponent,
    LetrasBonosDeletePopupComponent,
    LetrasBonosDeleteDialogComponent,
    letrasBonosRoute,
    letrasBonosPopupRoute,
    LetrasBonosResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...letrasBonosRoute,
    ...letrasBonosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LetrasBonosComponent,
        LetrasBonosDetailComponent,
        LetrasBonosDialogComponent,
        LetrasBonosDeleteDialogComponent,
        LetrasBonosPopupComponent,
        LetrasBonosDeletePopupComponent,
    ],
    entryComponents: [
        LetrasBonosComponent,
        LetrasBonosDialogComponent,
        LetrasBonosPopupComponent,
        LetrasBonosDeleteDialogComponent,
        LetrasBonosDeletePopupComponent,
    ],
    providers: [
        LetrasBonosService,
        LetrasBonosPopupService,
        LetrasBonosResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmLetrasBonosModule {}
