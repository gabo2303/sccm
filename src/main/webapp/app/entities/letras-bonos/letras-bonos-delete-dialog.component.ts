import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LetrasBonos } from './letras-bonos.model';
import { LetrasBonosPopupService } from './letras-bonos-popup.service';
import { LetrasBonosService } from './letras-bonos.service';

@Component({
    selector: 'jhi-letras-bonos-delete-dialog',
    templateUrl: './letras-bonos-delete-dialog.component.html'
})
export class LetrasBonosDeleteDialogComponent {

    letrasBonos: LetrasBonos;

    constructor(
        private letrasBonosService: LetrasBonosService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.letrasBonosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'letrasBonosListModification',
                content: 'Deleted an letrasBonos'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-letras-bonos-delete-popup',
    template: ''
})
export class LetrasBonosDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private letrasBonosPopupService: LetrasBonosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.letrasBonosPopupService
                .open(LetrasBonosDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
