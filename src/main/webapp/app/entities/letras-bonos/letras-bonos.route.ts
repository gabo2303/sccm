import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LetrasBonosComponent } from './letras-bonos.component';
import { LetrasBonosDetailComponent } from './letras-bonos-detail.component';
import { SCCM_1890_USER_BYL } from '../../app.constants';

@Injectable()
export class LetrasBonosResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const letrasBonosRoute: Routes = [
    {
        path: 'letras-bonos',
        component: LetrasBonosComponent,
        resolve: {
            'pagingParams': LetrasBonosResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'sccmApp.letrasBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'letras-bonos/:id',
        component: LetrasBonosDetailComponent,
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'sccmApp.letrasBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const letrasBonosPopupRoute: Routes = [
    /*{
        path: 'letras-bonos-new',
        component: LetrasBonosPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.letrasBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'letras-bonos/:id/edit',
        component: LetrasBonosPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.letrasBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'letras-bonos/:id/delete',
        component: LetrasBonosDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.letrasBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }*/
];
