import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { LetrasBonos } from './letras-bonos.model';
import { LetrasBonosService } from './letras-bonos.service';

@Injectable()
export class LetrasBonosPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private letrasBonosService: LetrasBonosService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.letrasBonosService.find(id).subscribe((letrasBonos) => {
                    letrasBonos.fechaInv = this.datePipe
                        .transform(letrasBonos.fechaInv, 'yyyy-MM-ddTHH:mm:ss');
                    letrasBonos.fechaPago = this.datePipe
                        .transform(letrasBonos.fechaPago, 'yyyy-MM-ddTHH:mm:ss');
                    letrasBonos.fecCont = this.datePipe
                        .transform(letrasBonos.fecCont, 'yyyy-MM-ddTHH:mm:ss');
                    letrasBonos.fecVcto = this.datePipe
                        .transform(letrasBonos.fecVcto, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.letrasBonosModalRef(component, letrasBonos);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.letrasBonosModalRef(component, new LetrasBonos());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    letrasBonosModalRef(component: Component, letrasBonos: LetrasBonos): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.letrasBonos = letrasBonos;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
