import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { LetrasBonos } from './letras-bonos.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { LetrasBonosDto } from './letras-bonos-dto.model';

@Injectable()
export class LetrasBonosService {

    private resourceUrl = SERVER_API_URL + 'api/letras-bonos';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findByAllByTaxYearId(idYear: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearId`, options).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    create(letrasBonos: LetrasBonos): Observable<LetrasBonos> {
        const copy = this.convert(letrasBonos);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(letrasBonos: LetrasBonos): Observable<LetrasBonos> {
        const copy = this.convert(letrasBonos);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<LetrasBonos> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LetrasBonos.
     */
    private convertItemFromServer(json: any): LetrasBonos {
        const entity: LetrasBonos = Object.assign(new LetrasBonos(), json);
        entity.fechaInv = this.dateUtils.convertDateTimeFromServer(json.fechaInv);
        entity.fechaPago = this.dateUtils.convertDateTimeFromServer(json.fechaPago);
        entity.fecCont = this.dateUtils.convertDateTimeFromServer(json.fecCont);
        entity.fecVcto = this.dateUtils.convertDateTimeFromServer(json.fecVcto);
        return entity;
    }

    private convertResponse2(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer2(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LetrasBonosDto.
     */
    private convertItemFromServer2(json: any): LetrasBonosDto {
        const entity: LetrasBonosDto = Object.assign(new LetrasBonosDto(), json);
        return entity;
    }

    /**
     * Convert a LetrasBonos to a JSON which can be sent to the server.
     */
    private convert(letrasBonos: LetrasBonos): LetrasBonos {
        const copy: LetrasBonos = Object.assign({}, letrasBonos);
        copy.fechaInv = this.dateUtils.toDate(letrasBonos.fechaInv);
        copy.fechaPago = this.dateUtils.toDate(letrasBonos.fechaPago);
        copy.fecCont = this.dateUtils.toDate(letrasBonos.fecCont);
        copy.fecVcto = this.dateUtils.toDate(letrasBonos.fecVcto);
        return copy;
    }
}
