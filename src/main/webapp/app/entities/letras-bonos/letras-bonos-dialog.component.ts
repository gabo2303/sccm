import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LetrasBonos } from './letras-bonos.model';
import { LetrasBonosPopupService } from './letras-bonos-popup.service';
import { LetrasBonosService } from './letras-bonos.service';

@Component({
    selector: 'jhi-letras-bonos-dialog',
    templateUrl: './letras-bonos-dialog.component.html'
})
export class LetrasBonosDialogComponent implements OnInit {

    letrasBonos: LetrasBonos;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private letrasBonosService: LetrasBonosService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.letrasBonos.id !== undefined) {
            this.subscribeToSaveResponse(
                this.letrasBonosService.update(this.letrasBonos));
        } else {
            this.subscribeToSaveResponse(
                this.letrasBonosService.create(this.letrasBonos));
        }
    }

    private subscribeToSaveResponse(result: Observable<LetrasBonos>) {
        result.subscribe((res: LetrasBonos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LetrasBonos) {
        this.eventManager.broadcast({ name: 'letrasBonosListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-letras-bonos-popup',
    template: ''
})
export class LetrasBonosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private letrasBonosPopupService: LetrasBonosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.letrasBonosPopupService
                    .open(LetrasBonosDialogComponent as Component, params['id']);
            } else {
                this.letrasBonosPopupService
                    .open(LetrasBonosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
