import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Fut } from './fut.model';
import { FutPopupService } from './fut-popup.service';
import { FutService } from './fut.service';

@Component({
    selector: 'jhi-fut-delete-dialog',
    templateUrl: './fut-delete-dialog.component.html'
})
export class FutDeleteDialogComponent {

    fut: Fut;

    constructor(
        private futService: FutService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.futService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'futListModification',
                content: 'Deleted an fut'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-fut-delete-popup',
    template: ''
})
export class FutDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private futPopupService: FutPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.futPopupService
                .open(FutDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
