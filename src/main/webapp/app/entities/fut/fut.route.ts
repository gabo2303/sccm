import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { FutComponent } from './fut.component';
import { FutDetailComponent } from './fut-detail.component';
import { FutPopupComponent } from './fut-dialog.component';
import { FutDeletePopupComponent } from './fut-delete-dialog.component';

export const futRoute: Routes = [
    {
        path: 'fut',
        component: FutComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.fut.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'fut/:id',
        component: FutDetailComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.fut.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const futPopupRoute: Routes = [
    {
        path: 'fut-new',
        component: FutPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.fut.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'fut/:id/edit',
        component: FutPopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.fut.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'fut/:id/delete',
        component: FutDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
            pageTitle: 'sccmApp.fut.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
