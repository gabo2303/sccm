import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Fut } from './fut.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FutService {

    private resourceUrl = SERVER_API_URL + 'api/futs';

    constructor(private http: Http) { }

    create(fut: Fut): Observable<Fut> {
        const copy = this.convert(fut);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(fut: Fut): Observable<Fut> {
        const copy = this.convert(fut);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Fut> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Fut.
     */
    private convertItemFromServer(json: any): Fut {
        const entity: Fut = Object.assign(new Fut(), json);
        return entity;
    }

    /**
     * Convert a Fut to a JSON which can be sent to the server.
     */
    private convert(fut: Fut): Fut {
        const copy: Fut = Object.assign({}, fut);
        return copy;
    }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> {
        // const options = createRequestOption(idAnno);
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`)
            .map((res: Response) => this.convertResponse(res));
    }
}
