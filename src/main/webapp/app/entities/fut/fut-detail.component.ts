import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Fut } from './fut.model';
import { FutService } from './fut.service';

@Component({
    selector: 'jhi-fut-detail',
    templateUrl: './fut-detail.component.html'
})
export class FutDetailComponent implements OnInit, OnDestroy {

    fut: Fut;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private futService: FutService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInFuts();
    }

    load(id) {
        this.futService.find(id).subscribe((fut) => {
            this.fut = fut;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInFuts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'futListModification',
            (response) => this.load(this.fut.id)
        );
    }
}
