import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Fut } from './fut.model';
import { FutService } from './fut.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { InstrumentoService } from '../instrumento/instrumento.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { Instrumento } from '../instrumento/instrumento.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-fut',
    templateUrl: './fut.component.html'
})
export class FutComponent implements OnInit, OnDestroy {
    futs: Fut[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    instrumentsList: Instrumento [];
    annoSelected: number;
    futFlag = false;
    futFlagSinData = false;

    constructor(private futService: FutService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private annoTributaService: AnnoTributaService, private route: ActivatedRoute, private instrumentoService: InstrumentoService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }

    loadAll() {
        this.futService.query().subscribe(
            (res: ResponseWrapper) => {
                this.futs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadAllInstrumentos();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInFuts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Fut) {
        return item.id;
    }

    registerChangeInFuts() {
        this.eventSubscriber = this.eventManager.subscribe('futListModification', (response) => this.loadAll());
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadAllInstrumentos() {
        this.instrumentoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.instrumentsList = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }

    filterByAnooTributario(annoTributaId: any) {
        this.spinnerService.show();
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.futFlag = false;
        this.futFlagSinData = false;
        this.futService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.futs = res.json;
                console.log('this.futs.length: ' + this.futs.length);
                if (this.futs.length > 0) {
                    this.futFlag = true;
                } else {
                    this.futFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}
