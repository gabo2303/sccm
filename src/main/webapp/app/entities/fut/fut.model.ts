import { BaseEntity } from './../../shared';

export class Fut implements BaseEntity {
    constructor(
        public id?: number,
        public valor?: number,
        public credImpCategoria?: number,
        public annoTributa?: BaseEntity,
        public instrumento?: BaseEntity,
    ) {
    }
}
