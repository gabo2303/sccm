import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    FutComponent,
    FutDeleteDialogComponent,
    FutDeletePopupComponent,
    FutDetailComponent,
    FutDialogComponent,
    FutPopupComponent,
    futPopupRoute,
    FutPopupService,
    futRoute,
    FutService,
} from './';

const ENTITY_STATES = [
    ...futRoute,
    ...futPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FutComponent,
        FutDetailComponent,
        FutDialogComponent,
        FutDeleteDialogComponent,
        FutPopupComponent,
        FutDeletePopupComponent,
    ],
    entryComponents: [
        FutComponent,
        FutDialogComponent,
        FutPopupComponent,
        FutDeleteDialogComponent,
        FutDeletePopupComponent,
    ],
    providers: [
        FutService,
        FutPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmFutModule {
}
