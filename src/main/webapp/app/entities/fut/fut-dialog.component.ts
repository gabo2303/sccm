import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Fut } from './fut.model';
import { FutPopupService } from './fut-popup.service';
import { FutService } from './fut.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { Instrumento, InstrumentoService } from '../instrumento';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-fut-dialog',
    templateUrl: './fut-dialog.component.html'
})
export class FutDialogComponent implements OnInit {

    fut: Fut;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    instrumentos: Instrumento[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private futService: FutService,
        private annoTributaService: AnnoTributaService,
        private instrumentoService: InstrumentoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.instrumentoService.query()
            .subscribe((res: ResponseWrapper) => { this.instrumentos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.fut.id !== undefined) {
            this.subscribeToSaveResponse(
                this.futService.update(this.fut));
        } else {
            this.subscribeToSaveResponse(
                this.futService.create(this.fut));
        }
    }

    private subscribeToSaveResponse(result: Observable<Fut>) {
        result.subscribe((res: Fut) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Fut) {
        this.eventManager.broadcast({ name: 'futListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackInstrumentoById(index: number, item: Instrumento) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-fut-popup',
    template: ''
})
export class FutPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private futPopupService: FutPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.futPopupService
                    .open(FutDialogComponent as Component, params['id']);
            } else {
                this.futPopupService
                    .open(FutDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
