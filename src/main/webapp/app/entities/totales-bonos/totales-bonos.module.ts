import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    TotalesBonosService,
    TotalesBonosPopupService,
    TotalesBonosComponent,
    TotalesBonosDetailComponent,
    TotalesBonosDialogComponent,
    TotalesBonosPopupComponent,
    TotalesBonosDeletePopupComponent,
    TotalesBonosDeleteDialogComponent,
    totalesBonosRoute,
    totalesBonosPopupRoute,
    TotalesBonosResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...totalesBonosRoute,
    ...totalesBonosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TotalesBonosComponent,
        TotalesBonosDetailComponent,
        TotalesBonosDialogComponent,
        TotalesBonosDeleteDialogComponent,
        TotalesBonosPopupComponent,
        TotalesBonosDeletePopupComponent,
    ],
    entryComponents: [
        TotalesBonosComponent,
        TotalesBonosDialogComponent,
        TotalesBonosPopupComponent,
        TotalesBonosDeleteDialogComponent,
        TotalesBonosDeletePopupComponent,
    ],
    providers: [
        TotalesBonosService,
        TotalesBonosPopupService,
        TotalesBonosResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmTotalesBonosModule {}
