export * from './totales-bonos.model';
export * from './totales-bonos-popup.service';
export * from './totales-bonos.service';
export * from './totales-bonos-dialog.component';
export * from './totales-bonos-delete-dialog.component';
export * from './totales-bonos-detail.component';
export * from './totales-bonos.component';
export * from './totales-bonos.route';
