import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TotalesBonos } from './totales-bonos.model';
import { TotalesBonosService } from './totales-bonos.service';

@Component({
    selector: 'jhi-totales-bonos-detail',
    templateUrl: './totales-bonos-detail.component.html'
})
export class TotalesBonosDetailComponent implements OnInit, OnDestroy {

    totalesBonos: TotalesBonos;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private totalesBonosService: TotalesBonosService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTotalesBonos();
    }

    load(id) {
        this.totalesBonosService.find(id).subscribe((totalesBonos) => {
            this.totalesBonos = totalesBonos;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTotalesBonos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'totalesBonosListModification',
            (response) => this.load(this.totalesBonos.id)
        );
    }
}
