import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TotalesBonos } from './totales-bonos.model';
import { TotalesBonosPopupService } from './totales-bonos-popup.service';
import { TotalesBonosService } from './totales-bonos.service';

@Component({
    selector: 'jhi-totales-bonos-dialog',
    templateUrl: './totales-bonos-dialog.component.html'
})
export class TotalesBonosDialogComponent implements OnInit {

    totalesBonos: TotalesBonos;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private totalesBonosService: TotalesBonosService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.totalesBonos.id !== undefined) {
            this.subscribeToSaveResponse(
                this.totalesBonosService.update(this.totalesBonos));
        } else {
            this.subscribeToSaveResponse(
                this.totalesBonosService.create(this.totalesBonos));
        }
    }

    private subscribeToSaveResponse(result: Observable<TotalesBonos>) {
        result.subscribe((res: TotalesBonos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TotalesBonos) {
        this.eventManager.broadcast({ name: 'totalesBonosListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-totales-bonos-popup',
    template: ''
})
export class TotalesBonosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private totalesBonosPopupService: TotalesBonosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.totalesBonosPopupService
                    .open(TotalesBonosDialogComponent as Component, params['id']);
            } else {
                this.totalesBonosPopupService
                    .open(TotalesBonosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
