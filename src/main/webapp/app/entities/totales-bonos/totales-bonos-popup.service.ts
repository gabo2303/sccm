import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { TotalesBonos } from './totales-bonos.model';
import { TotalesBonosService } from './totales-bonos.service';

@Injectable()
export class TotalesBonosPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private totalesBonosService: TotalesBonosService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.totalesBonosService.find(id).subscribe((totalesBonos) => {
                    totalesBonos.fechaEvento = this.datePipe
                        .transform(totalesBonos.fechaEvento, 'yyyy-MM-ddTHH:mm:ss');
                    totalesBonos.fechaInversion = this.datePipe
                        .transform(totalesBonos.fechaInversion, 'yyyy-MM-ddTHH:mm:ss');
                    totalesBonos.fechaPago = this.datePipe
                        .transform(totalesBonos.fechaPago, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.totalesBonosModalRef(component, totalesBonos);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.totalesBonosModalRef(component, new TotalesBonos());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    totalesBonosModalRef(component: Component, totalesBonos: TotalesBonos): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.totalesBonos = totalesBonos;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
