import { BaseEntity } from './../../shared';

export class TotalesBonos implements BaseEntity {
    constructor(
        public id?: number,
        public annoTributaId?: number,
        public fechaEvento?: any,
        public fechaInversion?: any,
        public fechaPago?: any,
        public instrumento?: string,
        public monedaPago?: number,
        public montoPagadoMo?: number,
        public montoPagadoValor?: number,
        public nombreBeneficiario?: string,
        public numCupon?: number,
        public origen?: string,
        public rutBeneficiario?: string,
        public serie?: number,
        public tipoIrf?: number,
        public valorNominal?: number,
    ) {
    }
}
