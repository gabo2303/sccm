import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TotalesBonos } from './totales-bonos.model';
import { TotalesBonosPopupService } from './totales-bonos-popup.service';
import { TotalesBonosService } from './totales-bonos.service';

@Component({
    selector: 'jhi-totales-bonos-delete-dialog',
    templateUrl: './totales-bonos-delete-dialog.component.html'
})
export class TotalesBonosDeleteDialogComponent {

    totalesBonos: TotalesBonos;

    constructor(
        private totalesBonosService: TotalesBonosService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.totalesBonosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'totalesBonosListModification',
                content: 'Deleted an totalesBonos'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-totales-bonos-delete-popup',
    template: ''
})
export class TotalesBonosDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private totalesBonosPopupService: TotalesBonosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.totalesBonosPopupService
                .open(TotalesBonosDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
