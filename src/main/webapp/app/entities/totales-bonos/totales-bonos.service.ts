import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TotalesBonos } from './totales-bonos.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { TotalesBonosDto } from './totales-bonos-dto.model';

@Injectable()
export class TotalesBonosService {

    private resourceUrl = SERVER_API_URL + 'api/totales-bonos';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findByAllByTaxYearId(idYear: number, queryOption: string, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const requestUrl = this.resourceUrl + '/findByAllByTaxYearId?idAnno=' + idYear + '&queryOption=' + queryOption;
        return this.http.get(requestUrl, options).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    create(totalesBonos: TotalesBonos): Observable<TotalesBonos> {
        const copy = this.convert(totalesBonos);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(totalesBonos: TotalesBonos): Observable<TotalesBonos> {
        const copy = this.convert(totalesBonos);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TotalesBonos> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse2(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer2(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TotalesBonos.
     */
    private convertItemFromServer(json: any): TotalesBonos {
        const entity: TotalesBonos = Object.assign(new TotalesBonos(), json);
        entity.fechaEvento = this.dateUtils.convertDateTimeFromServer(json.fechaEvento);
        entity.fechaInversion = this.dateUtils.convertDateTimeFromServer(json.fechaInversion);
        entity.fechaPago = this.dateUtils.convertDateTimeFromServer(json.fechaPago);
        return entity;
    }

    /**
     * Convert a returned JSON object to TotalesBonosDto.
     */
    private convertItemFromServer2(json: any): TotalesBonosDto {
        const entity: TotalesBonosDto = Object.assign(new TotalesBonosDto(), json);
        return entity;
    }

    /**
     * Convert a TotalesBonos to a JSON which can be sent to the server.
     */
    private convert(totalesBonos: TotalesBonos): TotalesBonos {
        const copy: TotalesBonos = Object.assign({}, totalesBonos);

        copy.fechaEvento = this.dateUtils.toDate(totalesBonos.fechaEvento);

        copy.fechaInversion = this.dateUtils.toDate(totalesBonos.fechaInversion);

        copy.fechaPago = this.dateUtils.toDate(totalesBonos.fechaPago);
        return copy;
    }
}
