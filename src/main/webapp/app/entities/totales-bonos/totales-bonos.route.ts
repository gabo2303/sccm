import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TotalesBonosComponent } from './totales-bonos.component';
import { TotalesBonosDetailComponent } from './totales-bonos-detail.component';
import { SCCM_1890_USER_BYL } from '../../app.constants';

@Injectable()
export class TotalesBonosResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const totalesBonosRoute: Routes = [
    {
        path: 'totales-bonos',
        component: TotalesBonosComponent,
        resolve: {
            'pagingParams': TotalesBonosResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'sccmApp.totalesBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'totales-bonos/:id',
        component: TotalesBonosDetailComponent,
        data: {
            authorities: [ SCCM_1890_USER_BYL ],
            pageTitle: 'sccmApp.totalesBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const totalesBonosPopupRoute: Routes = [
    /*{
        path: 'totales-bonos-new',
        component: TotalesBonosPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.totalesBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'totales-bonos/:id/edit',
        component: TotalesBonosPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.totalesBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'totales-bonos/:id/delete',
        component: TotalesBonosDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.totalesBonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }*/
];
