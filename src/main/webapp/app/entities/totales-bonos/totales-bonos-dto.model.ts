
export class TotalesBonosDto {
    constructor(
        public origen?: string,
        public fechaPago?: any,
        public fechaInversion?: any,
        public instrumento?: string,
        public valorNominal?: number,
        public montoPagadoMo?: number,
        public montoPagadoValor?: number,
        public monedaPago?: number,
        public fechaEvento?: any,
        public nombreBeneficiario?: string,
        public rutBeneficiario?: string,
        public numCupon?: number,
        public serie?: number,
        public tipoIrf?: number,
    ) {
    }
}
