import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DAPComponent } from './dap.component';
import { DAPDetailComponent } from './dap-detail.component';
import { DAPPopupComponent } from './dap-dialog.component';
import { DAPDeletePopupComponent } from './dap-delete-dialog.component';
import { SCCM_1890_USER_DAP } from '../../app.constants';

@Injectable()
export class DapResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const dAPRoute: Routes = [
    {
        path: 'dap',
        component: DAPComponent,
        resolve: {
            'pagingParams': DapResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dAP.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'dap/:id',
        component: DAPDetailComponent,
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dAP.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const dAPPopupRoute: Routes = [
    {
        path: 'dap-new',
        component: DAPPopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dAP.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'dap/:id/edit',
        component: DAPPopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dAP.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'dap/:id/delete',
        component: DAPDeletePopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dAP.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
