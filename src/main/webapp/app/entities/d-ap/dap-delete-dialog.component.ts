import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DAP } from './dap.model';
import { DAPPopupService } from './dap-popup.service';
import { DAPService } from './dap.service';

@Component({
    selector: 'jhi-dap-delete-dialog',
    templateUrl: './dap-delete-dialog.component.html'
})
export class DAPDeleteDialogComponent {

    dAP: DAP;

    constructor(private dAPService: DAPService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dAPService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'dAPListModification', content: 'Deleted an dAP' });
            this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK' });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dap-delete-popup',
    template: ''
})
export class DAPDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private dAPPopupService: DAPPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dAPPopupService.open(DAPDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
