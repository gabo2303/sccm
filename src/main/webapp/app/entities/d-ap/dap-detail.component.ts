import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DAP } from './dap.model';
import { DAPService } from './dap.service';

@Component({
    selector: 'jhi-dap-detail',
    templateUrl: './dap-detail.component.html'
})
export class DAPDetailComponent implements OnInit, OnDestroy {

    dAP: DAP;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private dAPService: DAPService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInDAPS();
    }

    load(id) {
        this.dAPService.find(id).subscribe((dAP) => {
            this.dAP = dAP;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDAPS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dAPListModification',
            (response) => this.load(this.dAP.id)
        );
    }
}
