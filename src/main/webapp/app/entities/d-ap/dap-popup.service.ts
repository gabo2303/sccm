import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DAP } from './dap.model';
import { DAPService } from './dap.service';

@Injectable()
export class DAPPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private dAPService: DAPService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dAPService.find(id).subscribe((dAP) => {
                    if (dAP.fechaPago) {
                        dAP.fechaPago = {
                            year: dAP.fechaPago.getFullYear(),
                            month: dAP.fechaPago.getMonth() + 1,
                            day: dAP.fechaPago.getDate()
                        };
                    }
                    if (dAP.fechaInv) {
                        dAP.fechaInv = {
                            year: dAP.fechaInv.getFullYear(),
                            month: dAP.fechaInv.getMonth() + 1,
                            day: dAP.fechaInv.getDate()
                        };
                    }
                    if (dAP.fecVcto) {
                        dAP.fecVcto = {
                            year: dAP.fecVcto.getFullYear(),
                            month: dAP.fecVcto.getMonth() + 1,
                            day: dAP.fecVcto.getDate()
                        };
                    }
                    if (dAP.fecCont) {
                        dAP.fecCont = {
                            year: dAP.fecCont.getFullYear(),
                            month: dAP.fecCont.getMonth() + 1,
                            day: dAP.fecCont.getDate()
                        };
                    }
                    this.ngbModalRef = this.dAPModalRef(component, dAP);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dAPModalRef(component, new DAP());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dAPModalRef(component: Component, dAP: DAP): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.dAP = dAP;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
