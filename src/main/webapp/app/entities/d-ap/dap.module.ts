import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    DAPComponent,
    DAPDeleteDialogComponent,
    DAPDeletePopupComponent,
    DAPDetailComponent,
    DAPDialogComponent,
    DAPPopupComponent,
    dAPPopupRoute,
    DAPPopupService,
    DapResolvePagingParams,
    dAPRoute,
    DAPService,
} from './';

const ENTITY_STATES = [
    ...dAPRoute,
    ...dAPPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DAPComponent,
        DAPDetailComponent,
        DAPDialogComponent,
        DAPDeleteDialogComponent,
        DAPPopupComponent,
        DAPDeletePopupComponent,
    ],
    entryComponents: [
        DAPComponent,
        DAPDialogComponent,
        DAPPopupComponent,
        DAPDeleteDialogComponent,
        DAPDeletePopupComponent,
    ],
    providers: [
        DAPService,
        DAPPopupService,
        DapResolvePagingParams,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmDAPModule {
}
