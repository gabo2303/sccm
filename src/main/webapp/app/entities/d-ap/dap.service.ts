import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DAP } from './dap.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { DapDto } from './dap-dto.model';
import { isNullOrUndefined } from 'util';

@Injectable()
export class DAPService 
{
    private resourceUrl = SERVER_API_URL + 'api/d-aps';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    findByAllByTaxYearId(idYear: number, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearId`, options).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdAndAlertTypeUniqueCode(idYear: number, alertTypeUniqueCode?: string, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdAndAlertTypeUniqueCode` +
            (isNullOrUndefined(alertTypeUniqueCode) ? '' : '?alarmUniqueCode=' + alertTypeUniqueCode), options).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(idYear: number, alertTypeUniqueCode?: string, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated` +
            (isNullOrUndefined(alertTypeUniqueCode) ? '' : '?alarmUniqueCode=' + alertTypeUniqueCode), options).map((res: Response) => this.convertResponse(res));
    }

    create(dAP: DAP): Observable<DAP> 
	{
        const copy = this.convert(dAP);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(dAP: DAP): Observable<DAP> 
	{
		console.log(dAP);
        const copy = this.convert(dAP);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updateDap(dAP: DAP): Observable<DAP> 
	{
        const copy = this.convert(dAP);
        
		return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<DAP> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }
	
	deleteDap(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[ i ])); }
		 
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse2(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer2(jsonResponse[ i ])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to DapDto.
     */
    private convertItemFromServer2(json: any): DapDto 
	{
        const entity: DapDto = Object.assign(new DapDto(), json);
        
		return entity;
    }

    /**
     * Convert a returned JSON object to DAP.
     */
    private convertItemFromServer(json: any): DAP 
	{
        const entity: DAP = Object.assign(new DAP(), json);
        
		entity.fechaPago = this.dateUtils.convertLocalDateFromServer(json.fechaPago);
        entity.fechaInv = this.dateUtils.convertLocalDateFromServer(json.fechaInv);
        entity.fecVcto = this.dateUtils.convertLocalDateFromServer(json.fecVcto);
        entity.fecCont = this.dateUtils.convertLocalDateFromServer(json.fecCont);
        
		return entity;
    }

    /**
     * Convert a DAP to a JSON which can be sent to the server.
     */
    private convert(dAP: DAP): DAP 
	{
        const copy: DAP = Object.assign({}, dAP);
        
		copy.fechaPago = this.dateUtils.convertLocalDateToServer(dAP.fechaPago);
        copy.fechaInv = this.dateUtils.convertLocalDateToServer(dAP.fechaInv);
        copy.fecVcto = this.dateUtils.convertLocalDateToServer(dAP.fecVcto);
        copy.fecCont = this.dateUtils.convertLocalDateToServer(dAP.fecCont);
        
		return copy;
    }
}
