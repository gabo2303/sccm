import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { DAP } from './dap.model';
import { DAPPopupService } from './dap-popup.service';
import { DAPService } from './dap.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-dap-dialog',
    templateUrl: './dap-dialog.component.html'
})
export class DAPDialogComponent implements OnInit {

    dAP: DAP;
    isSaving: boolean;

    annotributas: AnnoTributa[];
    fechaPagoDp: any;
    fechaInvDp: any;
    fecVctoDp: any;
    fecContDp: any;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private dAPService: DAPService, private annoTributaService: AnnoTributaService,
                private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annotributas = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dAP.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dAPService.update(this.dAP));
        } else {
            this.subscribeToSaveResponse(
                this.dAPService.create(this.dAP));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<DAP>) {
        result.subscribe((res: DAP) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DAP) {
        this.eventManager.broadcast({ name: 'dAPListModification', content: 'OK' });
        this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-dap-popup',
    template: ''
})
export class DAPPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private dAPPopupService: DAPPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.dAPPopupService.open(DAPDialogComponent as Component, params[ 'id' ]);
            } else {
                this.dAPPopupService.open(DAPDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
