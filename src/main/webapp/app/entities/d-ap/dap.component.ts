import { Component, OnInit } from '@angular/core';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { DAP } from './dap.model';
import { DAPService } from './dap.service';
import {
    ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS,
    ALARMA_DAP_REAJUSTES_NEGATIVOS,
    ALARMA_DAP_RENOVACIONES,
    CommonServices,
    ITEMS_PER_PAGE,
    PATTERN_DATE_SHORT,
    Principal,
    ResponseWrapper,
    SELECT_BLANK_OPTION
} from '../../shared';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { TablaMoneda } from '../tabla-moneda/tabla-moneda.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { TablaMonedaService } from '../tabla-moneda/tabla-moneda.service';
import { ExcelService } from '../../shared/excel/ExcelService';
import { isNullOrUndefined } from 'util';
import { TipoAlerta, TipoAlertaService } from '../tipo-alerta';

@Component({
    selector: 'jhi-dap',
    templateUrl: './dap.component.html'
})
export class DAPComponent implements OnInit {

    currentAccount: any;
    dAPS: DAP[];
    paginatedDaps: DAP[];
    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    annosTributarios: AnnoTributa[];
    tablaMonedas: TablaMoneda[];
    annoSelected: number;
    dapFlag = false;
    dapFlagSinData = false;
	dapFlagError = false;
    selectedAlertType: string = null;
    tiposAlertas: TipoAlerta[];
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    SELECT_BLANK_OPTION = SELECT_BLANK_OPTION;
    ALARMA_DAP_REAJUSTES_NEGATIVOS = ALARMA_DAP_REAJUSTES_NEGATIVOS;
    ALARMA_DAP_RENOVACIONES = ALARMA_DAP_RENOVACIONES;
    ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS = ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS;
    showReajNegativo = false;
    isPaginatedOnWeb = false;

    constructor(private dAPService: DAPService, private parseLinks: JhiParseLinks, private jhiAlertService: JhiAlertService, private principal: Principal,
                private activatedRoute: ActivatedRoute, private router: Router, private eventManager: JhiEventManager, private spinnerService: Ng4LoadingSpinnerService,
                private annoTributaService: AnnoTributaService, private commonServices: CommonServices, private tablaMonedaService: TablaMonedaService,
                private excelService: ExcelService, private tipoAlertaService: TipoAlertaService) {

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
    }

    loadAll() {
        this.spinnerService.show();
        this.dAPService.findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(this.annoSelected, this.selectedAlertType, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate([ 'dap' ], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                }
        });
        this.isPaginatedOnWeb ? this.loadPageStatic() : this.loadAll();
    }

    ngOnInit() {
        this.loadAllTablaMoneda();
        this.loadTiposAlertas();
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    loadAllTablaMoneda() {
        this.tablaMonedaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tablaMonedas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
		
		console.log("ANNIOS TRIBUTARIOS");
		console.log(this.annosTributarios);
    }

    loadTiposAlertas() {
        this.spinnerService.show();
        const isDap = true;
        this.tipoAlertaService.query(isDap).subscribe(
            (res: ResponseWrapper) => {
                this.tiposAlertas = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    trackId(index: number, item: DAP) {
        return item.id;
    }

    filterByAnnoTributarioId(annoTributaId: HTMLSelectElement, selectedAlertTypeSel: HTMLSelectElement) 
	{
        this.spinnerService.show();
        
		if (selectedAlertTypeSel.value !== this.SELECT_BLANK_OPTION) 
		{
            this.selectedAlertType = selectedAlertTypeSel.value;
        } 
		else { this.selectedAlertType = null; }
		
        this.showReajNegativo = !isNullOrUndefined(this.selectedAlertType) && this.selectedAlertType === this.ALARMA_DAP_REAJUSTES_NEGATIVOS;
        this.isPaginatedOnWeb = !isNullOrUndefined(this.selectedAlertType) && ((this.selectedAlertType === this.ALARMA_DAP_EXCLUIR_PAGOS_ANTICIPADOS_MENOR_A_7_DIAS) ||
            (this.selectedAlertType === this.ALARMA_DAP_RENOVACIONES));
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.page = 1;
		
        this.loadAllByAnnoTributaIdAndAlertType(this.annoSelected, this.selectedAlertType);
    }

    loadAllByAnnoTributaIdAndAlertType(yearId: number, alertTypeUniqueCode: string) 
	{
        this.dapFlag = false;
        this.dapFlagSinData = false;
        this.spinnerService.show();
        
		this.dAPService.findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(yearId, alertTypeUniqueCode, 
		{
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe
		(
			(res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res)
        );
    }

    generateExcel() 
	{
        this.spinnerService.show();
        this.dAPService.findByAllByTaxYearIdAndAlertTypeUniqueCode(this.annoSelected, this.selectedAlertType).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, true),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private loadPageStatic() {
        const start = (this.page - 1) * this.itemsPerPage;
        this.dAPS = this.paginatedDaps.slice(start, start + this.itemsPerPage);
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        		
        this.dapFlagError = true;
    }

    private onSuccess(data, headers, downloadFile?) 
	{
        if (downloadFile) 
		{
            this.commonServices.download(data, 'Datos de Depósito a Plazo' + (!isNullOrUndefined(this.selectedAlertType) ? ' por Tipo de Alerta' : ''));
        } 
		else 
		{
            this.links = this.parseLinks.parse(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.dAPS = data;
            this.paginatedDaps = data;
            
			if (this.dAPS.length > 0) 
			{
                if (this.isPaginatedOnWeb) { this.dAPS = this.paginatedDaps.slice(0, this.itemsPerPage); }
				
                this.page = 1;
                this.dapFlag = true;
            } 
			else { this.dapFlagSinData = true; }
        }
		
        this.spinnerService.hide();
    }
}
