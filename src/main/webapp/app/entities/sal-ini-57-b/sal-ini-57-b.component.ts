import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { SalIni57B } from './sal-ini-57-b.model';
import { SalIni57BService } from './sal-ini-57-b.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sal-ini-57-b',
    templateUrl: './sal-ini-57-b.component.html'
})
export class SalIni57BComponent implements OnInit, OnDestroy {
salIni57BS: SalIni57B[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private salIni57BService: SalIni57BService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.salIni57BService.query().subscribe(
            (res: ResponseWrapper) => {
                this.salIni57BS = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSalIni57BS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SalIni57B) {
        return item.id;
    }
    registerChangeInSalIni57BS() {
        this.eventSubscriber = this.eventManager.subscribe('salIni57BListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
