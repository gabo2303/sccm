import { BaseEntity } from './../../shared';

export class SalIni57B implements BaseEntity {
    constructor(
        public id?: number,
        public tipo?: string,
        public monedaNominal?: string,
        public saldoNetoPer?: number,
        public saldoProxPer?: number,
        public numCertificado?: number,
        public anoAct?: number,
        public mesAct?: number,
        public giroRentabNom?: number,
        public giroRentabPesos?: number,
        public annoTributa?: BaseEntity,
        public clienteBice?: BaseEntity,
    ) {
    }
}
