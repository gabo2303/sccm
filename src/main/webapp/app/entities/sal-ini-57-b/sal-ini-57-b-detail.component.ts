import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SalIni57B } from './sal-ini-57-b.model';
import { SalIni57BService } from './sal-ini-57-b.service';

@Component({
    selector: 'jhi-sal-ini-57-b-detail',
    templateUrl: './sal-ini-57-b-detail.component.html'
})
export class SalIni57BDetailComponent implements OnInit, OnDestroy {

    salIni57B: SalIni57B;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private salIni57BService: SalIni57BService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSalIni57BS();
    }

    load(id) {
        this.salIni57BService.find(id).subscribe((salIni57B) => {
            this.salIni57B = salIni57B;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSalIni57BS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'salIni57BListModification',
            (response) => this.load(this.salIni57B.id)
        );
    }
}
