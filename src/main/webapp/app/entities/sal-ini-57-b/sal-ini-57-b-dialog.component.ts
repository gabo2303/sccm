import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SalIni57B } from './sal-ini-57-b.model';
import { SalIni57BPopupService } from './sal-ini-57-b-popup.service';
import { SalIni57BService } from './sal-ini-57-b.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ClienteBice, ClienteBiceService } from '../cliente-bice';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sal-ini-57-b-dialog',
    templateUrl: './sal-ini-57-b-dialog.component.html'
})
export class SalIni57BDialogComponent implements OnInit {

    salIni57B: SalIni57B;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    clientebices: ClienteBice[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private salIni57BService: SalIni57BService,
        private annoTributaService: AnnoTributaService,
        private clienteBiceService: ClienteBiceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clienteBiceService.query()
            .subscribe((res: ResponseWrapper) => { this.clientebices = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.salIni57B.id !== undefined) {
            this.subscribeToSaveResponse(
                this.salIni57BService.update(this.salIni57B));
        } else {
            this.subscribeToSaveResponse(
                this.salIni57BService.create(this.salIni57B));
        }
    }

    private subscribeToSaveResponse(result: Observable<SalIni57B>) {
        result.subscribe((res: SalIni57B) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SalIni57B) {
        this.eventManager.broadcast({ name: 'salIni57BListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackClienteBiceById(index: number, item: ClienteBice) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-sal-ini-57-b-popup',
    template: ''
})
export class SalIni57BPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private salIni57BPopupService: SalIni57BPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salIni57BPopupService
                    .open(SalIni57BDialogComponent as Component, params['id']);
            } else {
                this.salIni57BPopupService
                    .open(SalIni57BDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
