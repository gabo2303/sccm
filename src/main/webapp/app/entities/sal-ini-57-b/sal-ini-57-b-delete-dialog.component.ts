import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SalIni57B } from './sal-ini-57-b.model';
import { SalIni57BPopupService } from './sal-ini-57-b-popup.service';
import { SalIni57BService } from './sal-ini-57-b.service';

@Component({
    selector: 'jhi-sal-ini-57-b-delete-dialog',
    templateUrl: './sal-ini-57-b-delete-dialog.component.html'
})
export class SalIni57BDeleteDialogComponent {

    salIni57B: SalIni57B;

    constructor(
        private salIni57BService: SalIni57BService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.salIni57BService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'salIni57BListModification',
                content: 'Deleted an salIni57B'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sal-ini-57-b-delete-popup',
    template: ''
})
export class SalIni57BDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private salIni57BPopupService: SalIni57BPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.salIni57BPopupService
                .open(SalIni57BDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
