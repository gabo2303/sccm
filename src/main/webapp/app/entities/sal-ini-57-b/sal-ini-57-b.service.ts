import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { SalIni57B } from './sal-ini-57-b.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SalIni57BService {

    private resourceUrl = SERVER_API_URL + 'api/sal-ini-57-bs';

    constructor(private http: Http) { }

    create(salIni57B: SalIni57B): Observable<SalIni57B> {
        const copy = this.convert(salIni57B);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(salIni57B: SalIni57B): Observable<SalIni57B> {
        const copy = this.convert(salIni57B);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<SalIni57B> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SalIni57B.
     */
    private convertItemFromServer(json: any): SalIni57B {
        const entity: SalIni57B = Object.assign(new SalIni57B(), json);
        return entity;
    }

    /**
     * Convert a SalIni57B to a JSON which can be sent to the server.
     */
    private convert(salIni57B: SalIni57B): SalIni57B {
        const copy: SalIni57B = Object.assign({}, salIni57B);
        return copy;
    }
}
