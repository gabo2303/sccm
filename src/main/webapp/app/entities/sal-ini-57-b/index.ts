export * from './sal-ini-57-b.model';
export * from './sal-ini-57-b-popup.service';
export * from './sal-ini-57-b.service';
export * from './sal-ini-57-b-dialog.component';
export * from './sal-ini-57-b-delete-dialog.component';
export * from './sal-ini-57-b-detail.component';
export * from './sal-ini-57-b.component';
export * from './sal-ini-57-b.route';
