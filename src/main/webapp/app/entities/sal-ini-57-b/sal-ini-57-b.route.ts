import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalIni57BComponent } from './sal-ini-57-b.component';
import { SalIni57BDetailComponent } from './sal-ini-57-b-detail.component';
import { SalIni57BPopupComponent } from './sal-ini-57-b-dialog.component';
import { SalIni57BDeletePopupComponent } from './sal-ini-57-b-delete-dialog.component';

export const salIni57BRoute: Routes = [
    {
        path: 'sal-ini-57-b',
        component: SalIni57BComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salIni57B.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sal-ini-57-b/:id',
        component: SalIni57BDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salIni57B.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salIni57BPopupRoute: Routes = [
    {
        path: 'sal-ini-57-b-new',
        component: SalIni57BPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salIni57B.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sal-ini-57-b/:id/edit',
        component: SalIni57BPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salIni57B.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sal-ini-57-b/:id/delete',
        component: SalIni57BDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.salIni57B.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
