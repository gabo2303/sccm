import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    SalIni57BService,
    SalIni57BPopupService,
    SalIni57BComponent,
    SalIni57BDetailComponent,
    SalIni57BDialogComponent,
    SalIni57BPopupComponent,
    SalIni57BDeletePopupComponent,
    SalIni57BDeleteDialogComponent,
    salIni57BRoute,
    salIni57BPopupRoute,
} from './';

const ENTITY_STATES = [
    ...salIni57BRoute,
    ...salIni57BPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SalIni57BComponent,
        SalIni57BDetailComponent,
        SalIni57BDialogComponent,
        SalIni57BDeleteDialogComponent,
        SalIni57BPopupComponent,
        SalIni57BDeletePopupComponent,
    ],
    entryComponents: [
        SalIni57BComponent,
        SalIni57BDialogComponent,
        SalIni57BPopupComponent,
        SalIni57BDeleteDialogComponent,
        SalIni57BDeletePopupComponent,
    ],
    providers: [
        SalIni57BService,
        SalIni57BPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmSalIni57BModule {}
