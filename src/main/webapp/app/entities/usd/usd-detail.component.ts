import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Usd } from './usd.model';
import { UsdService } from './usd.service';

@Component({
    selector: 'jhi-usd-detail',
    templateUrl: './usd-detail.component.html'
})
export class UsdDetailComponent implements OnInit, OnDestroy {

    usd: Usd;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private usdService: UsdService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUsds();
    }

    load(id) {
        this.usdService.find(id).subscribe((usd) => {
            this.usd = usd;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUsds() {
        this.eventSubscriber = this.eventManager.subscribe(
            'usdListModification',
            (response) => this.load(this.usd.id)
        );
    }
}
