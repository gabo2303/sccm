import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    UsdService,
    UsdPopupService,
    UsdComponent,
    UsdDetailComponent,
    UsdDialogComponent,
    UsdPopupComponent,
    UsdDeletePopupComponent,
    UsdDeleteDialogComponent,
    usdRoute,
    usdPopupRoute,
} from './';

const ENTITY_STATES = [
    ...usdRoute,
    ...usdPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UsdComponent,
        UsdDetailComponent,
        UsdDialogComponent,
        UsdDeleteDialogComponent,
        UsdPopupComponent,
        UsdDeletePopupComponent,
    ],
    entryComponents: [
        UsdComponent,
        UsdDialogComponent,
        UsdPopupComponent,
        UsdDeleteDialogComponent,
        UsdDeletePopupComponent,
    ],
    providers: [
        UsdService,
        UsdPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmUsdModule {}
