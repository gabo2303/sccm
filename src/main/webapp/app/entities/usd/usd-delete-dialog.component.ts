import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Usd } from './usd.model';
import { UsdPopupService } from './usd-popup.service';
import { UsdService } from './usd.service';

@Component({
    selector: 'jhi-usd-delete-dialog',
    templateUrl: './usd-delete-dialog.component.html'
})
export class UsdDeleteDialogComponent {

    usd: Usd;

    constructor(
        private usdService: UsdService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.usdService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'usdListModification',
                content: 'Deleted an usd'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-usd-delete-popup',
    template: ''
})
export class UsdDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private usdPopupService: UsdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.usdPopupService
                .open(UsdDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
