import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Usd } from './usd.model';
import { UsdService } from './usd.service';

@Injectable()
export class UsdPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private usdService: UsdService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.usdService.find(id).subscribe((usd) => {
                    if (usd.fecha) {
                        usd.fecha = {
                            year: usd.fecha.getFullYear(),
                            month: usd.fecha.getMonth() + 1,
                            day: usd.fecha.getDate()
                        };
                    }
                    this.ngbModalRef = this.usdModalRef(component, usd);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.usdModalRef(component, new Usd());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    usdModalRef(component: Component, usd: Usd): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.usd = usd;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
