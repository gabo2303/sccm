import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UsdComponent } from './usd.component';
import { UsdDetailComponent } from './usd-detail.component';
import { UsdPopupComponent } from './usd-dialog.component';
import { UsdDeletePopupComponent } from './usd-delete-dialog.component';

export const usdRoute: Routes = [
    {
        path: 'usd',
        component: UsdComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.usd.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'usd/:id',
        component: UsdDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.usd.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const usdPopupRoute: Routes = [
    {
        path: 'usd-new',
        component: UsdPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.usd.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'usd/:id/edit',
        component: UsdPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.usd.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'usd/:id/delete',
        component: UsdDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.usd.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
