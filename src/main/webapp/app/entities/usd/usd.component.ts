import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Usd } from './usd.model';
import { UsdService } from './usd.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-usd',
    templateUrl: './usd.component.html'
})
export class UsdComponent implements OnInit, OnDestroy {
usds: Usd[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private usdService: UsdService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.usdService.query().subscribe(
            (res: ResponseWrapper) => {
                this.usds = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUsds();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Usd) {
        return item.id;
    }
    registerChangeInUsds() {
        this.eventSubscriber = this.eventManager.subscribe('usdListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
