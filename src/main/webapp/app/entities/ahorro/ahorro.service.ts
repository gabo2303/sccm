import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Ahorro } from './ahorro.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { AhorroDto } from './ahorro-dto.model';
import { isNullOrUndefined } from 'util';

@Injectable()
export class AhorroService 
{
    private resourceUrl = SERVER_API_URL + 'api/ahorros';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    findByAllByTaxYearId(idYear: number, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearId`, options).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdAndAlertTypeUniqueCode(idYear: number, alertTypeUniqueCode?: string, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdAndAlertTypeUniqueCode` +
            (isNullOrUndefined(alertTypeUniqueCode) ? '' : '?alarmUniqueCode=' + alertTypeUniqueCode), options).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(idYear: number, alertTypeUniqueCode?: string, req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated` +
            (isNullOrUndefined(alertTypeUniqueCode) ? '' : '?alarmUniqueCode=' + alertTypeUniqueCode), options).map((res: Response) => this.convertResponse(res));
    }

    create(ahorro: Ahorro): Observable<Ahorro> 
	{
        const copy = this.convert(ahorro);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ahorro: Ahorro): Observable<Ahorro> 
	{
        const copy = this.convert(ahorro);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updateAhorro(ahorro: Ahorro): Observable<Ahorro> 
	{
        const copy = this.convert(ahorro);
        
		return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Ahorro> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }
	
	deleteAhorro(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> 
	{
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`).map( (res: Response) => this.convertResponse(res) );
    }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[ i ])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse2(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer2(jsonResponse[ i ])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AhorroDto.
     */
    private convertItemFromServer2(json: any): AhorroDto 
	{
        const entity: AhorroDto = Object.assign(new AhorroDto(), json);
        
		return entity;
    }

    /**
     * Convert a returned JSON object to Ahorro.
     */
    private convertItemFromServer(json: any): Ahorro 
	{
        const entity: Ahorro = Object.assign(new Ahorro(), json);
        entity.fechaPago = this.dateUtils.convertLocalDateFromServer(json.fechaPago);
        entity.fechaInv = this.dateUtils.convertLocalDateFromServer(json.fechaInv);
        entity.fechaApe = this.dateUtils.convertLocalDateFromServer(json.fechaApe);
        entity.fechaCon = this.dateUtils.convertLocalDateFromServer(json.fechaCon);
        entity.fechaVen = this.dateUtils.convertLocalDateFromServer(json.fechaVen);
        
		return entity;
    }

    /**
     * Convert a Ahorro to a JSON which can be sent to the server.
     */
    private convert(ahorro: Ahorro): Ahorro 
	{
        const copy: Ahorro = Object.assign({}, ahorro);
        copy.fechaPago = this.dateUtils.convertLocalDateToServer(ahorro.fechaPago);
        copy.fechaInv = this.dateUtils.convertLocalDateToServer(ahorro.fechaInv);
        copy.fechaApe = this.dateUtils.convertLocalDateToServer(ahorro.fechaApe);
        copy.fechaCon = this.dateUtils.convertLocalDateToServer(ahorro.fechaCon);
        copy.fechaVen = this.dateUtils.convertLocalDateToServer(ahorro.fechaVen);
        
		return copy;
    }
}