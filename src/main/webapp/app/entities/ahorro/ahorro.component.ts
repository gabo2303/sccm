import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { Ahorro } from './ahorro.model';
import { AhorroService } from './ahorro.service';
import { CommonServices, ITEMS_PER_PAGE, PATTERN_DATE_SHORT, Principal, ResponseWrapper, SELECT_BLANK_OPTION } from '../../shared';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ExcelService } from '../../shared/excel/ExcelService';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { TablaMoneda } from '../tabla-moneda/tabla-moneda.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoAlerta, TipoAlertaService } from '../tipo-alerta';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'jhi-ahorro',
    templateUrl: './ahorro.component.html'
})
export class AhorroComponent implements OnInit, OnDestroy {

    ahorros: Ahorro[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    tiposAlertas: TipoAlerta[];
    annoSelected: number;
    ahorrosFlag = false;
    ahorrosFlagSinData = false;
    selectedAlertType: string = null;

    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tablaMonedas: TablaMoneda[];
    PATTERN_DATE_SHORT = PATTERN_DATE_SHORT;
    SELECT_BLANK_OPTION = SELECT_BLANK_OPTION;

    constructor(private ahorroService: AhorroService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private excelService: ExcelService, private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService,
                private commonServices: CommonServices, private activatedRoute: ActivatedRoute, private parseLinks: JhiParseLinks, private router: Router,
                private tipoAlertaService: TipoAlertaService) {

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
    }

    loadAll() {
        this.spinnerService.show();
        this.ahorroService.findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(this.annoSelected, this.selectedAlertType, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        this.spinnerService.show();
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate([ 'ahorro' ], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([ 'ahorro', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        } ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.loadTiposAlertas();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAhorros();
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadTiposAlertas() {
        this.spinnerService.show();
        const isDap = false;
        this.tipoAlertaService.query(isDap).subscribe(
            (res: ResponseWrapper) => {
                this.tiposAlertas = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Ahorro) {
        return item.id;
    }

    registerChangeInAhorros() {
        this.eventSubscriber = this.eventManager.subscribe('ahorroListModification', (response) => this.loadAll());
    }

    filterByAnooTributario(annoTributaId: HTMLSelectElement, selectedAlertTypeSel: HTMLSelectElement) {
        this.spinnerService.show();
        if (selectedAlertTypeSel.value !== this.SELECT_BLANK_OPTION) {
            this.selectedAlertType = selectedAlertTypeSel.value;
        } else {
            this.selectedAlertType = null;
        }
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.page = 1;
        this.loadAllByAnnoTributaIdAndAlertType(this.annoSelected, this.selectedAlertType);
    }

    loadAllByAnnoTributaIdAndAlertType(yearId: number, alertTypeUniqueCode: string) {
        this.ahorrosFlag = false;
        this.ahorrosFlagSinData = false;
        this.spinnerService.show();
        this.ahorroService.findByAllByTaxYearIdAndAlertTypeUniqueCodePaginated(yearId, alertTypeUniqueCode, {
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    generateExcel(queryOption: string) {
        this.spinnerService.show();
        this.ahorroService.findByAllByTaxYearIdAndAlertTypeUniqueCode(this.annoSelected, this.selectedAlertType).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, true),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

    private onSuccess(data, headers, downloadFile?) {
        if (downloadFile) {
            this.commonServices.download(data, 'Datos Ahorro' + (!isNullOrUndefined(this.selectedAlertType) ? ' por Tipo de Alerta' : ''));
        } else {
            this.links = this.parseLinks.parse(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.ahorros = data;
            if (this.ahorros.length > 0) {
                this.ahorrosFlag = true;
            } else {
                this.ahorrosFlagSinData = true;
            }
        }
        this.spinnerService.hide();
    }
}
