import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Ahorro } from './ahorro.model';
import { AhorroPopupService } from './ahorro-popup.service';
import { AhorroService } from './ahorro.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ahorro-dialog',
    templateUrl: './ahorro-dialog.component.html'
})
export class AhorroDialogComponent implements OnInit {

    ahorro: Ahorro;
    isSaving: boolean;
    annotributas: AnnoTributa[];
    fechaPagoDp: any;
    fechaInvDp: any;
    fechaApeDp: any;
    fechaConDp: any;
    fechaVenDp: any;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private ahorroService: AhorroService, private annoTributaService: AnnoTributaService,
                private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
        .subscribe((res: ResponseWrapper) => {
            this.annotributas = res.json;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
		console.log(this.ahorro);
        this.isSaving = true;
        if (this.ahorro.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ahorroService.update(this.ahorro));
        } else {
            this.subscribeToSaveResponse(
                this.ahorroService.create(this.ahorro));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<Ahorro>) {
        result.subscribe((res: Ahorro) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Ahorro) {
        this.eventManager.broadcast({ name: 'ahorroListModification', content: 'OK' });
        this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-ahorro-popup',
    template: ''
})
export class AhorroPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private ahorroPopupService: AhorroPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.ahorroPopupService
                .open(AhorroDialogComponent as Component, params[ 'id' ]);
            } else {
                this.ahorroPopupService
                .open(AhorroDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
