import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Ahorro } from './ahorro.model';
import { AhorroPopupService } from './ahorro-popup.service';
import { AhorroService } from './ahorro.service';

@Component({
    selector: 'jhi-ahorro-delete-dialog',
    templateUrl: './ahorro-delete-dialog.component.html'
})
export class AhorroDeleteDialogComponent {

    ahorro: Ahorro;

    constructor(private ahorroService: AhorroService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ahorroService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'ahorroListModification', content: 'Deleted an ahorro' });
            this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK'});
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ahorro-delete-popup',
    template: ''
})
export class AhorroDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private ahorroPopupService: AhorroPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ahorroPopupService
            .open(AhorroDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
