import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { Injectable } from '@angular/core';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';

import { AhorroComponent } from './ahorro.component';
import { AhorroDetailComponent } from './ahorro-detail.component';
import { AhorroPopupComponent } from './ahorro-dialog.component';
import { AhorroDeletePopupComponent } from './ahorro-delete-dialog.component';
import { DapResolvePagingParams } from '../d-ap/dap.route';
import { SCCM_1890_USER_AHORRO } from '../../app.constants';

@Injectable()
export class AhorroResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const ahorroRoute: Routes = [
    {
        path: 'ahorro',
        component: AhorroComponent,
        resolve: {
            'pagingParams': DapResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'ahorro/:id',
        component: AhorroDetailComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const ahorroPopupRoute: Routes = [
    {
        path: 'ahorro-new',
        component: AhorroPopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'ahorro/:id/edit',
        component: AhorroPopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'ahorro/:id/delete',
        component: AhorroDeletePopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
