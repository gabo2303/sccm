import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    AhorroComponent,
    AhorroDeleteDialogComponent,
    AhorroDeletePopupComponent,
    AhorroDetailComponent,
    AhorroDialogComponent,
    AhorroPopupComponent,
    ahorroPopupRoute,
    AhorroPopupService, AhorroResolvePagingParams,
    ahorroRoute,
    AhorroService,
} from './';

const ENTITY_STATES = [
    ...ahorroRoute,
    ...ahorroPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AhorroComponent,
        AhorroDetailComponent,
        AhorroDialogComponent,
        AhorroDeleteDialogComponent,
        AhorroPopupComponent,
        AhorroDeletePopupComponent,
    ],
    entryComponents: [
        AhorroComponent,
        AhorroDialogComponent,
        AhorroPopupComponent,
        AhorroDeleteDialogComponent,
        AhorroDeletePopupComponent,
    ],
    providers: [
        AhorroService,
        AhorroPopupService,
        AhorroResolvePagingParams,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAhorroModule {
}
