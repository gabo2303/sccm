import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Ahorro } from './ahorro.model';
import { AhorroService } from './ahorro.service';

@Component({
    selector: 'jhi-ahorro-detail',
    templateUrl: './ahorro-detail.component.html'
})
export class AhorroDetailComponent implements OnInit, OnDestroy {

    ahorro: Ahorro;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private ahorroService: AhorroService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInAhorros();
    }

    load(id) {
        this.ahorroService.find(id).subscribe((ahorro) => {
            this.ahorro = ahorro;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAhorros() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ahorroListModification',
            (response) => this.load(this.ahorro.id)
        );
    }
}
