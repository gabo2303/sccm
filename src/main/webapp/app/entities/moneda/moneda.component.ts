import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Moneda } from './moneda.model';
import { MonedaService } from './moneda.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-moneda',
    templateUrl: './moneda.component.html'
})
export class MonedaComponent implements OnInit, OnDestroy {
monedas: Moneda[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private monedaService: MonedaService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.monedaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.monedas = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInMonedas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Moneda) {
        return item.id;
    }
    registerChangeInMonedas() {
        this.eventSubscriber = this.eventManager.subscribe('monedaListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
