import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MonedaComponent } from './moneda.component';
import { MonedaDetailComponent } from './moneda-detail.component';
import { MonedaPopupComponent } from './moneda-dialog.component';
import { MonedaDeletePopupComponent } from './moneda-delete-dialog.component';

export const monedaRoute: Routes = [
    {
        path: 'moneda',
        component: MonedaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'moneda/:id',
        component: MonedaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const monedaPopupRoute: Routes = [
    {
        path: 'moneda-new',
        component: MonedaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'moneda/:id/edit',
        component: MonedaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'moneda/:id/delete',
        component: MonedaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
