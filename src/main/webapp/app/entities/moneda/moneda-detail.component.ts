import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Moneda } from './moneda.model';
import { MonedaService } from './moneda.service';

@Component({
    selector: 'jhi-moneda-detail',
    templateUrl: './moneda-detail.component.html'
})
export class MonedaDetailComponent implements OnInit, OnDestroy {

    moneda: Moneda;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private monedaService: MonedaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMonedas();
    }

    load(id) {
        this.monedaService.find(id).subscribe((moneda) => {
            this.moneda = moneda;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMonedas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'monedaListModification',
            (response) => this.load(this.moneda.id)
        );
    }
}
