import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Moneda } from './moneda.model';
import { MonedaPopupService } from './moneda-popup.service';
import { MonedaService } from './moneda.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-moneda-dialog',
    templateUrl: './moneda-dialog.component.html'
})
export class MonedaDialogComponent implements OnInit {

    moneda: Moneda;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private monedaService: MonedaService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.moneda.id !== undefined) {
            this.subscribeToSaveResponse(
                this.monedaService.update(this.moneda));
        } else {
            this.subscribeToSaveResponse(
                this.monedaService.create(this.moneda));
        }
    }

    private subscribeToSaveResponse(result: Observable<Moneda>) {
        result.subscribe((res: Moneda) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Moneda) {
        this.eventManager.broadcast({ name: 'monedaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-moneda-popup',
    template: ''
})
export class MonedaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private monedaPopupService: MonedaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.monedaPopupService
                    .open(MonedaDialogComponent as Component, params['id']);
            } else {
                this.monedaPopupService
                    .open(MonedaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
