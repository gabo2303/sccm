import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    MonedaService,
    MonedaPopupService,
    MonedaComponent,
    MonedaDetailComponent,
    MonedaDialogComponent,
    MonedaPopupComponent,
    MonedaDeletePopupComponent,
    MonedaDeleteDialogComponent,
    monedaRoute,
    monedaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...monedaRoute,
    ...monedaPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MonedaComponent,
        MonedaDetailComponent,
        MonedaDialogComponent,
        MonedaDeleteDialogComponent,
        MonedaPopupComponent,
        MonedaDeletePopupComponent,
    ],
    entryComponents: [
        MonedaComponent,
        MonedaDialogComponent,
        MonedaPopupComponent,
        MonedaDeleteDialogComponent,
        MonedaDeletePopupComponent,
    ],
    providers: [
        MonedaService,
        MonedaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmMonedaModule {}
