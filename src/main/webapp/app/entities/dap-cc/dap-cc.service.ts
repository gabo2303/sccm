import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DapCC } from './dap-cc.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { isNullOrUndefined } from 'util';
import { DapCcDto } from './dap-cc-dto.model';

@Injectable()
export class DapCCService {

    private resourceUrl = SERVER_API_URL + 'api/dap-ccs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findByAllByTaxYearId(idYear: number): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearId`).map((res: Response) => this.convertResponse2(res));
    }

    findByAllByTaxYearIdPaginated(idYear: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/${idYear}/findByAllByTaxYearIdPaginated`, options).map((res: Response) => this.convertResponse(res));
    }

    findByAllByTaxYearIdCurrencyPaginated(idYear: number, currency: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const requestUrl = this.resourceUrl + '/findByAllByTaxYearIdCurrencyPaginated?idAnno=' + idYear + (isNullOrUndefined(currency) ? '' : '&currency=' + currency);
        return this.http.get(requestUrl, options).map((res: Response) => this.convertResponse(res));
    }

    create(dapCC: DapCC): Observable<DapCC> {
        const copy = this.convert(dapCC);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(dapCC: DapCC): Observable<DapCC> {
        const copy = this.convert(dapCC);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<DapCC> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse2(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer2(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to DapCcDto
     */
    private convertItemFromServer2(json: any): DapCcDto {
        const entity: DapCcDto = Object.assign(new DapCcDto(), json);
        return entity;
    }

    /**
     * Convert a returned JSON object to DapCC.
     */
    private convertItemFromServer(json: any): DapCC {
        const entity: DapCC = Object.assign(new DapCC(), json);
        entity.fechaContable = this.dateUtils.convertLocalDateFromServer(json.fechaContable);
        return entity;
    }

    /**
     * Convert a DapCC to a JSON which can be sent to the server.
     */
    private convert(dapCC: DapCC): DapCC {
        const copy: DapCC = Object.assign({}, dapCC);
        copy.fechaContable = this.dateUtils.convertLocalDateToServer(dapCC.fechaContable);
        return copy;
    }
}
