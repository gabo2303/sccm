import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DapCCComponent } from './dap-cc.component';
import { SCCM_1890_USER_DAP } from '../../app.constants';

@Injectable()
export class DapCCResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const dapCCRoute: Routes = [
    /*{
        path: 'dap-cc',
        component: DapCCComponent,
        resolve: {
            'pagingParams': DapCCResolvePagingParams
        },
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapCC.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'dap-cc/:id',
        component: DapCCDetailComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapCC.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    },*/
    {
        path: 'cuaContDap',
        component: DapCCComponent,
        resolve: {
            'pagingParams': DapCCResolvePagingParams
        },
        data: {
            authorities: [ SCCM_1890_USER_DAP ],
            pageTitle: 'sccmApp.dapCC.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const dapCCPopupRoute: Routes = [
    /*{
        path: 'dap-cc-new',
        component: DapCCPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapCC.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'dap-cc/:id/edit',
        component: DapCCPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapCC.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'dap-cc/:id/delete',
        component: DapCCDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'sccmApp.dapCC.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }*/
];
