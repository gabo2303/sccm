import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DapCC } from './dap-cc.model';
import { DapCCService } from './dap-cc.service';

@Injectable()
export class DapCCPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private dapCCService: DapCCService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dapCCService.find(id).subscribe((dapCC) => {
                    if (dapCC.fechaContable) {
                        dapCC.fechaContable = {
                            year: dapCC.fechaContable.getFullYear(),
                            month: dapCC.fechaContable.getMonth() + 1,
                            day: dapCC.fechaContable.getDate()
                        };
                    }
                    this.ngbModalRef = this.dapCCModalRef(component, dapCC);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dapCCModalRef(component, new DapCC());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dapCCModalRef(component: Component, dapCC: DapCC): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dapCC = dapCC;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
