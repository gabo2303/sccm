import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DapCC } from './dap-cc.model';
import { DapCCPopupService } from './dap-cc-popup.service';
import { DapCCService } from './dap-cc.service';

@Component({
    selector: 'jhi-dap-cc-delete-dialog',
    templateUrl: './dap-cc-delete-dialog.component.html'
})
export class DapCCDeleteDialogComponent {

    dapCC: DapCC;

    constructor(
        private dapCCService: DapCCService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dapCCService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dapCCListModification',
                content: 'Deleted an dapCC'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dap-cc-delete-popup',
    template: ''
})
export class DapCCDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dapCCPopupService: DapCCPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dapCCPopupService
                .open(DapCCDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
