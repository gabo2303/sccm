import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DapCC } from './dap-cc.model';
import { DapCCPopupService } from './dap-cc-popup.service';
import { DapCCService } from './dap-cc.service';

@Component({
    selector: 'jhi-dap-cc-dialog',
    templateUrl: './dap-cc-dialog.component.html'
})
export class DapCCDialogComponent implements OnInit {

    dapCC: DapCC;
    isSaving: boolean;
    fechaContableDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private dapCCService: DapCCService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dapCC.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dapCCService.update(this.dapCC));
        } else {
            this.subscribeToSaveResponse(
                this.dapCCService.create(this.dapCC));
        }
    }

    private subscribeToSaveResponse(result: Observable<DapCC>) {
        result.subscribe((res: DapCC) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DapCC) {
        this.eventManager.broadcast({ name: 'dapCCListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-dap-cc-popup',
    template: ''
})
export class DapCCPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dapCCPopupService: DapCCPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dapCCPopupService
                    .open(DapCCDialogComponent as Component, params['id']);
            } else {
                this.dapCCPopupService
                    .open(DapCCDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
