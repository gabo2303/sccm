import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    DapCCComponent,
    DapCCDeleteDialogComponent,
    DapCCDeletePopupComponent,
    DapCCDetailComponent,
    DapCCDialogComponent,
    DapCCPopupComponent,
    dapCCPopupRoute,
    DapCCPopupService,
    DapCCResolvePagingParams,
    dapCCRoute,
    DapCCService,
} from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...dapCCRoute,
    ...dapCCPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        DapCCComponent,
        DapCCDetailComponent,
        DapCCDialogComponent,
        DapCCDeleteDialogComponent,
        DapCCPopupComponent,
        DapCCDeletePopupComponent,
    ],
    entryComponents: [
        DapCCComponent,
        DapCCDialogComponent,
        DapCCPopupComponent,
        DapCCDeleteDialogComponent,
        DapCCDeletePopupComponent,
    ],
    providers: [
        DapCCService,
        DapCCPopupService,
        DapCCResolvePagingParams,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmDapCCModule {
}
