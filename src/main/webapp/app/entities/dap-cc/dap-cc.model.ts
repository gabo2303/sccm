import { BaseEntity } from './../../shared';

export class DapCC implements BaseEntity {
    constructor(
        public id?: number,
        public sucursal?: number,
        public cuentaInt?: number,
        public codMoneda?: number,
        public monedaContable?: number,
        public tipoPlazo?: string,
        public fechaContable?: any,
        public fcc?: number,
        public contabilidad?: number,
        public diferencia?: number,
        public diferenciaAbs?: number,
        public annoTributaId?: number,
    ) {
    }
}
