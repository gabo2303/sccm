import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DapCC } from './dap-cc.model';
import { DapCCService } from './dap-cc.service';

@Component({
    selector: 'jhi-dap-cc-detail',
    templateUrl: './dap-cc-detail.component.html'
})
export class DapCCDetailComponent implements OnInit, OnDestroy {

    dapCC: DapCC;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dapCCService: DapCCService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDapCCS();
    }

    load(id) {
        this.dapCCService.find(id).subscribe((dapCC) => {
            this.dapCC = dapCC;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDapCCS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dapCCListModification',
            (response) => this.load(this.dapCC.id)
        );
    }
}
