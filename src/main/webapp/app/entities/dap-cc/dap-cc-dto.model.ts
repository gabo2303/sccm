
export class DapCcDto {
    constructor(
        public sucursal?: number,
        public cuentaInt?: number,
        public codMoneda?: number,
        public monedaContable?: number,
        public tipoPlazo?: string,
        public fechaContable?: any,
        public fcc?: any,
        public contabilidad?: any,
        public diferencia?: any,
        public diferenciaAbs?: any,
    ) {
    }
}
