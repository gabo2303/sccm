import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { BonosComponent } from './bonos.component';
import { BonosDetailComponent } from './bonos-detail.component';
import { BonosPopupComponent } from './bonos-dialog.component';
import { BonosDeletePopupComponent } from './bonos-delete-dialog.component';

export const bonosRoute: Routes = [
    {
        path: 'bonos',
        component: BonosComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.bonos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'bonos/:id',
        component: BonosDetailComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.bonos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const bonosPopupRoute: Routes = [
    {
        path: 'bonos-new',
        component: BonosPopupComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.bonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'bonos/:id/edit',
        component: BonosPopupComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.bonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'bonos/:id/delete',
        component: BonosDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_USER', 'ROLE_ADMIN' ], 
            pageTitle: 'sccmApp.bonos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
