import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Bonos } from './bonos.model';
import { BonosService } from './bonos.service';

@Component({
    selector: 'jhi-bonos-detail',
    templateUrl: './bonos-detail.component.html'
})
export class BonosDetailComponent implements OnInit, OnDestroy {

    bonos: Bonos;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: JhiEventManager, private bonosService: BonosService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params[ 'id' ]);
        });
        this.registerChangeInBonos();
    }

    load(id) {
        this.bonosService.find(id).subscribe((bonos) => {
            this.bonos = bonos;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBonos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bonosListModification',
            (response) => this.load(this.bonos.id)
        );
    }
}
