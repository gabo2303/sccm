import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    BonosComponent,
    BonosDeleteDialogComponent,
    BonosDeletePopupComponent,
    BonosDetailComponent,
    BonosDialogComponent,
    BonosPopupComponent,
    bonosPopupRoute,
    BonosPopupService,
    bonosRoute,
    BonosService,
} from './';

const ENTITY_STATES = [
    ...bonosRoute,
    ...bonosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        BonosComponent,
        BonosDetailComponent,
        BonosDialogComponent,
        BonosDeleteDialogComponent,
        BonosPopupComponent,
        BonosDeletePopupComponent,
    ],
    entryComponents: [
        BonosComponent,
        BonosDialogComponent,
        BonosPopupComponent,
        BonosDeleteDialogComponent,
        BonosDeletePopupComponent,
    ],
    providers: [
        BonosService,
        BonosPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmBonosModule {
}
