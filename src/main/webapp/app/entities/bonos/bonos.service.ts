import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Bonos } from './bonos.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class BonosService 
{
    private resourceUrl = SERVER_API_URL + 'api/bonos';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {  }

    create(bonos: Bonos): Observable<Bonos> 
	{
        const copy = this.convert(bonos);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(bonos: Bonos): Observable<Bonos> 
	{
		console.log(bonos);
        const copy = this.convert(bonos);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updateBonos(bonos: Bonos): Observable<Bonos> 
	{
        const copy = this.convert(bonos);
        
		return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Bonos> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
    
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

	delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); } 
	
	deleteBonos(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }
	

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[ i ])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Bonos.
     */
    private convertItemFromServer(json: any): Bonos 
	{
        const entity: Bonos = Object.assign(new Bonos(), json);
        
		entity.fechaPago = this.dateUtils.convertLocalDateFromServer(json.fechaPago);
        entity.fechaInv = this.dateUtils.convertLocalDateFromServer(json.fechaInv);
        entity.fecVcto = this.dateUtils.convertLocalDateFromServer(json.fecVcto);
        entity.fecCont = this.dateUtils.convertLocalDateFromServer(json.fecCont);
        
		return entity;
    }

    /**
     * Convert a Bonos to a JSON which can be sent to the server.
     */
    private convert(bonos: Bonos): Bonos 
	{
        const copy: Bonos = Object.assign({}, bonos);
        
		copy.fechaPago = this.dateUtils.convertLocalDateToServer(bonos.fechaPago);
        copy.fechaInv = this.dateUtils.convertLocalDateToServer(bonos.fechaInv);
        copy.fecVcto = this.dateUtils.convertLocalDateToServer(bonos.fecVcto);
        copy.fecCont = this.dateUtils.convertLocalDateToServer(bonos.fecCont);
        
		return copy;
    }
}