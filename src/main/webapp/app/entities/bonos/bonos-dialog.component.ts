import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { BonosPopupService } from './bonos-popup.service';
import { BonosService } from './bonos.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';
import { Bonos } from './bonos.model';

@Component({
    selector: 'jhi-bonos-dialog',
    templateUrl: './bonos-dialog.component.html'
})
export class BonosDialogComponent implements OnInit {

    bonos: Bonos;
    isSaving: boolean;

    annotributas: AnnoTributa[];
    fechaPagoDp: any;
    fechaInvDp: any;
    fecVctoDp: any;
    fecContDp: any;

    constructor(public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private bonosService: BonosService, private annoTributaService: AnnoTributaService,
                private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annotributas = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bonos.id !== undefined) {
            this.subscribeToSaveResponse(this.bonosService.update(this.bonos));
        } else {
            this.subscribeToSaveResponse(this.bonosService.create(this.bonos));
        }
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<Bonos>) {
        result.subscribe((res: Bonos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Bonos) {
        this.eventManager.broadcast({ name: 'bonosListModification', content: 'OK' });
        this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-bonos-popup',
    template: ''
})
export class BonosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute, private bonosPopupService: BonosPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params[ 'id' ]) {
                this.bonosPopupService.open(BonosDialogComponent as Component, params[ 'id' ]);
            } else {
                this.bonosPopupService.open(BonosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
