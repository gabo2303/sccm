import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BonosService } from './bonos.service';
import { Bonos } from './bonos.model';

@Injectable()
export class BonosPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private bonosService: BonosService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bonosService.find(id).subscribe((bonos) => {
                    if (bonos.fechaPago) {
                        bonos.fechaPago = {
                            year: bonos.fechaPago.getFullYear(),
                            month: bonos.fechaPago.getMonth() + 1,
                            day: bonos.fechaPago.getDate()
                        };
                    }
                    if (bonos.fechaInv) {
                        bonos.fechaInv = {
                            year: bonos.fechaInv.getFullYear(),
                            month: bonos.fechaInv.getMonth() + 1,
                            day: bonos.fechaInv.getDate()
                        };
                    }
                    if (bonos.fecVcto) {
                        bonos.fecVcto = {
                            year: bonos.fecVcto.getFullYear(),
                            month: bonos.fecVcto.getMonth() + 1,
                            day: bonos.fecVcto.getDate()
                        };
                    }
                    if (bonos.fecCont) {
                        bonos.fecCont = {
                            year: bonos.fecCont.getFullYear(),
                            month: bonos.fecCont.getMonth() + 1,
                            day: bonos.fecCont.getDate()
                        };
                    }
                    this.ngbModalRef = this.bonosModalRef(component, bonos);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bonosModalRef(component, new Bonos());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bonosModalRef(component: Component, bonos: Bonos): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.bonos = bonos;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
