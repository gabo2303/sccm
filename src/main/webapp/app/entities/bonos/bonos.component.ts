import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Bonos } from './bonos.model';
import { BonosService } from './bonos.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-bonos',
    templateUrl: './bonos.component.html'
})
export class BonosComponent implements OnInit, OnDestroy {
    bonos: Bonos[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private bonosService: BonosService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal) {
    }

    loadAll() {
        this.bonosService.query().subscribe(
            (res: ResponseWrapper) => {
                this.bonos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInBonos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Bonos) {
        return item.id;
    }

    registerChangeInBonos() {
        this.eventSubscriber = this.eventManager.subscribe('bonosListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
