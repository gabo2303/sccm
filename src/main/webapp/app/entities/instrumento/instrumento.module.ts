import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    InstrumentoService,
    InstrumentoPopupService,
    InstrumentoComponent,
    InstrumentoDetailComponent,
    InstrumentoDialogComponent,
    InstrumentoPopupComponent,
    InstrumentoDeletePopupComponent,
    InstrumentoDeleteDialogComponent,
    instrumentoRoute,
    instrumentoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...instrumentoRoute,
    ...instrumentoPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        InstrumentoComponent,
        InstrumentoDetailComponent,
        InstrumentoDialogComponent,
        InstrumentoDeleteDialogComponent,
        InstrumentoPopupComponent,
        InstrumentoDeletePopupComponent,
    ],
    entryComponents: [
        InstrumentoComponent,
        InstrumentoDialogComponent,
        InstrumentoPopupComponent,
        InstrumentoDeleteDialogComponent,
        InstrumentoDeletePopupComponent,
    ],
    providers: [
        InstrumentoService,
        InstrumentoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmInstrumentoModule {}
