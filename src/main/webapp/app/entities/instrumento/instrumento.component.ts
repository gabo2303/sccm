import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Instrumento } from './instrumento.model';
import { InstrumentoService } from './instrumento.service';
import { Principal, ResponseWrapper } from '../../shared';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { ProductoService } from '../producto/producto.service';
import { Producto } from '../producto/producto.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'jhi-instrumento',
    templateUrl: './instrumento.component.html'
})
export class InstrumentoComponent implements OnInit, OnDestroy {

    instrumentos: Instrumento[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    products: Producto[];

    constructor(private instrumentoService: InstrumentoService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
        private annoTributaService: AnnoTributaService, private productoService: ProductoService, private spinnerService: Ng4LoadingSpinnerService) {
    }

    loadAll() {
        this.loadAnnosTributarios();
        this.loadAllProducts();
        this.instrumentoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.instrumentos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInInstrumentos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Instrumento) {
        return item.id;
    }

    registerChangeInInstrumentos() {
        this.eventSubscriber = this.eventManager.subscribe('instrumentoListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe((res: ResponseWrapper) => {
            this.annosTributarios = res.json;
            // this.annosTributarios.splice(0, 1);
            this.spinnerService.hide();
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadAllProducts() {
        this.productoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.products = res.json;
                this.products.forEach((value, index) => {
                    console.log('value: ' + value);
                });
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
}
