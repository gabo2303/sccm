import { BaseEntity } from './../../shared';

export class Instrumento implements BaseEntity {
    constructor(
        public id?: number,
        public nombreInstrumento?: string,
        public annoTributa?: BaseEntity,
        public futs?: BaseEntity[],
        public glosa1S?: BaseEntity[],
        public reps?: BaseEntity[],
        public producto?: BaseEntity,
    ) {
    }
}
