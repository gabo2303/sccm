export * from './instrumento.model';
export * from './instrumento-popup.service';
export * from './instrumento.service';
export * from './instrumento-dialog.component';
export * from './instrumento-delete-dialog.component';
export * from './instrumento-detail.component';
export * from './instrumento.component';
export * from './instrumento.route';
