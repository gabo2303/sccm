import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Instrumento } from './instrumento.model';
import { InstrumentoService } from './instrumento.service';

@Component({
    selector: 'jhi-instrumento-detail',
    templateUrl: './instrumento-detail.component.html'
})
export class InstrumentoDetailComponent implements OnInit, OnDestroy {

    instrumento: Instrumento;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private instrumentoService: InstrumentoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInInstrumentos();
    }

    load(id) {
        this.instrumentoService.find(id).subscribe((instrumento) => {
            this.instrumento = instrumento;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInInstrumentos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'instrumentoListModification',
            (response) => this.load(this.instrumento.id)
        );
    }
}
