import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { InstrumentoComponent } from './instrumento.component';
import { InstrumentoDetailComponent } from './instrumento-detail.component';
import { InstrumentoPopupComponent } from './instrumento-dialog.component';
import { InstrumentoDeletePopupComponent } from './instrumento-delete-dialog.component';

export const instrumentoRoute: Routes = [
    {
        path: 'instrumento',
        component: InstrumentoComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.instrumento.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'instrumento/:id',
        component: InstrumentoDetailComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.instrumento.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const instrumentoPopupRoute: Routes = [
    {
        path: 'instrumento-new',
        component: InstrumentoPopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.instrumento.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'instrumento/:id/edit',
        component: InstrumentoPopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.instrumento.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'instrumento/:id/delete',
        component: InstrumentoDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.instrumento.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
