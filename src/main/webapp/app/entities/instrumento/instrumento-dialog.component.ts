import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Instrumento } from './instrumento.model';
import { InstrumentoPopupService } from './instrumento-popup.service';
import { InstrumentoService } from './instrumento.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { Producto, ProductoService } from '../producto';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-instrumento-dialog',
    templateUrl: './instrumento-dialog.component.html'
})
export class InstrumentoDialogComponent implements OnInit {

    instrumento: Instrumento;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    productos: Producto[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private instrumentoService: InstrumentoService,
        private annoTributaService: AnnoTributaService,
        private productoService: ProductoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productoService.query()
            .subscribe((res: ResponseWrapper) => { this.productos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.instrumento.id !== undefined) {
            this.subscribeToSaveResponse(
                this.instrumentoService.update(this.instrumento));
        } else {
            this.subscribeToSaveResponse(
                this.instrumentoService.create(this.instrumento));
        }
    }

    private subscribeToSaveResponse(result: Observable<Instrumento>) {
        result.subscribe((res: Instrumento) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Instrumento) {
        this.eventManager.broadcast({ name: 'instrumentoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }

    trackProductoById(index: number, item: Producto) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-instrumento-popup',
    template: ''
})
export class InstrumentoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private instrumentoPopupService: InstrumentoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.instrumentoPopupService
                    .open(InstrumentoDialogComponent as Component, params['id']);
            } else {
                this.instrumentoPopupService
                    .open(InstrumentoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
