import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Instrumento } from './instrumento.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class InstrumentoService {

    private resourceUrl = SERVER_API_URL + 'api/instrumentos';

    constructor(private http: Http) { }

    create(instrumento: Instrumento): Observable<Instrumento> {
        const copy = this.convert(instrumento);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(instrumento: Instrumento): Observable<Instrumento> {
        const copy = this.convert(instrumento);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Instrumento> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Instrumento.
     */
    private convertItemFromServer(json: any): Instrumento {
        const entity: Instrumento = Object.assign(new Instrumento(), json);
        return entity;
    }

    /**
     * Convert a Instrumento to a JSON which can be sent to the server.
     */
    private convert(instrumento: Instrumento): Instrumento {
        const copy: Instrumento = Object.assign({}, instrumento);
        return copy;
    }
}
