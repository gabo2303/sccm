import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Instrumento } from './instrumento.model';
import { InstrumentoPopupService } from './instrumento-popup.service';
import { InstrumentoService } from './instrumento.service';

@Component({
    selector: 'jhi-instrumento-delete-dialog',
    templateUrl: './instrumento-delete-dialog.component.html'
})
export class InstrumentoDeleteDialogComponent {

    instrumento: Instrumento;

    constructor(
        private instrumentoService: InstrumentoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.instrumentoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'instrumentoListModification',
                content: 'Deleted an instrumento'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-instrumento-delete-popup',
    template: ''
})
export class InstrumentoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private instrumentoPopupService: InstrumentoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.instrumentoPopupService
                .open(InstrumentoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
