import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Pactos } from './pactos.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class PactosService 
{
    private resourceUrl = SERVER_API_URL + 'api/pactos';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(pactos: Pactos): Observable<Pactos> 
	{
        const copy = this.convert(pactos);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            
			return this.convertItemFromServer(jsonResponse);
        });
    }

    update(pactos: Pactos): Observable<Pactos> 
	{
        const copy = this.convert(pactos);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            
			return this.convertItemFromServer(jsonResponse);
        });
    }
	
	updatePactos(pactos: Pactos): Observable<Pactos> 
	{
        const copy = this.convert(pactos);
        
		return this.http.put(this.resourceUrl + "/actualizar", copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            
			return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Pactos> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            
			return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }
	
	deletePactos(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/eliminar/${id}`); }

    loadAllByAnnoTributaId(idAnno: number): Observable<ResponseWrapper> 
	{
        return this.http.get(`${this.resourceUrl}/${idAnno}/getByAnnoId`).map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		console.log(jsonResponse)
		
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[ i ])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Pactos.
     */
    private convertItemFromServer(json: any): Pactos 
	{
        const entity: Pactos = Object.assign(new Pactos(), json);
        
		entity.fechaPago = this.dateUtils.convertDateTimeFromServer(json.fechaPago);
        entity.fechaInv = this.dateUtils.convertDateTimeFromServer(json.fechaInv);
        
		return entity;
    }

    /**
     * Convert a Pactos to a JSON which can be sent to the server.
     */
    private convert(pactos: Pactos): Pactos 
	{
        const copy: Pactos = Object.assign({}, pactos);
        
		try
		{
			copy.fechaPago = this.dateUtils.toDate(pactos.fechaPago);
			copy.fechaInv = this.dateUtils.toDate(pactos.fechaInv);
        }
		catch
		{			
			copy.fechaPago = this.dateUtils.convertLocalDateToServer(pactos.fechaPago);
			copy.fechaInv = this.dateUtils.convertLocalDateToServer(pactos.fechaInv);
			copy.fecConta = this.dateUtils.convertLocalDateToServer(pactos.fecConta);
		}
		
		return copy;
    }
}