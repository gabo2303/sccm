import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Pactos } from './pactos.model';
import { PactosPopupService } from './pactos-popup.service';
import { PactosService } from './pactos.service';

@Component({
    selector: 'jhi-pactos-delete-dialog',
    templateUrl: './pactos-delete-dialog.component.html'
})
export class PactosDeleteDialogComponent {

    pactos: Pactos;

    constructor(
        private pactosService: PactosService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pactosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'pactosListModification', content: 'Deleted an pactos' });
            this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK' });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pactos-delete-popup',
    template: ''
})
export class PactosDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pactosPopupService: PactosPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pactosPopupService
            .open(PactosDeleteDialogComponent as Component, params[ 'id' ]);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
