import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Pactos } from './pactos.model';
import { PactosService } from './pactos.service';

@Injectable()
export class PactosPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private pactosService: PactosService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.pactosService.find(id).subscribe((pactos) => {
                    pactos.fechaPago = this.datePipe
                        .transform(pactos.fechaPago, 'yyyy-MM-ddTHH:mm:ss');
                    pactos.fechaInv = this.datePipe
                        .transform(pactos.fechaInv, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.pactosModalRef(component, pactos);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.pactosModalRef(component, new Pactos());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    pactosModalRef(component: Component, pactos: Pactos): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.pactos = pactos;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
