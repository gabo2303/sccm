import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { PactosComponent } from './pactos.component';
import { PactosDetailComponent } from './pactos-detail.component';
import { PactosPopupComponent } from './pactos-dialog.component';
import { PactosDeletePopupComponent } from './pactos-delete-dialog.component';
import { SCCM_1890_USER_PACTOS } from '../../app.constants';

export const pactosRoute: Routes = [
    {
        path: 'pactos',
        component: PactosComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'pactos/:id',
        component: PactosDetailComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const pactosPopupRoute: Routes = [
    {
        path: 'pactos-new',
        component: PactosPopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'pactos/:id/edit',
        component: PactosPopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'pactos/:id/delete',
        component: PactosDeletePopupComponent,
        data: {
            authorities: [ SCCM_1890_USER_PACTOS ],
            pageTitle: 'sccmApp.pactos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
