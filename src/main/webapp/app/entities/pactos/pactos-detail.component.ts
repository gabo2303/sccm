import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Pactos } from './pactos.model';
import { PactosService } from './pactos.service';

@Component({
    selector: 'jhi-pactos-detail',
    templateUrl: './pactos-detail.component.html'
})
export class PactosDetailComponent implements OnInit, OnDestroy {

    pactos: Pactos;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pactosService: PactosService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPactos();
    }

    load(id) {
        this.pactosService.find(id).subscribe((pactos) => {
            this.pactos = pactos;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPactos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pactosListModification',
            (response) => this.load(this.pactos.id)
        );
    }
}
