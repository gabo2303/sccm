import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Pactos } from './pactos.model';
import { PactosService } from './pactos.service';
import { CommonServices, Principal, ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { HttpErrorResponse } from '@angular/common/http';
import { TablaMonedaService } from '../tabla-moneda/tabla-moneda.service';
import { TablaMoneda } from '../tabla-moneda/tabla-moneda.model';
import { ExcelService } from '../../shared/excel/ExcelService';

@Component({ selector: 'jhi-pactos', templateUrl: './pactos.component.html' })

export class PactosComponent implements OnInit, OnDestroy 
{
    pactos: Pactos[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    pactosFlag = false;
    pactosFlagSinData = false;
    rutSelected: number;
    tablaMonedas: TablaMoneda[];

    @ViewChild('selectRutFilter') selectRutFilter: HTMLSelectElement;

    constructor(private commonServices: CommonServices, private pactosService: PactosService, private jhiAlertService: JhiAlertService, 
				private eventManager: JhiEventManager, private principal: Principal, private spinnerService: Ng4LoadingSpinnerService, 
				private annoTributaService: AnnoTributaService, private tablaMonedaService: TablaMonedaService, private excelService: ExcelService) { }

    loadAll() 
	{
        this.spinnerService.show();
        
		this.pactosService.query().subscribe
		(
            (res: ResponseWrapper) => 
			{
                this.pactos = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() 
	{
        this.loadAnnosTributarios();
        this.loadAll();
        this.loadAllTablaMoneda();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInPactos();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: Pactos) { return item.id; }

    registerChangeInPactos() { this.eventSubscriber = this.eventManager.subscribe('pactosListModification', (response) => this.loadAll()); }

    loadAnnosTributarios() 
	{
        this.spinnerService.show();
        
		this.annoTributaService.query().subscribe(
            
			(res: ResponseWrapper) => 
			{
                this.annosTributarios = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) 
	{
        this.annoSelected = parseInt(annoTributaId.value, 10);
        this.rutSelected = null;
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    filterByRut() 
	{
        this.spinnerService.show();
        this.rutSelected = parseInt(this.selectRutFilter.nativeElement.value, 10);
        this.spinnerService.hide();
    }

    loadAllByAnnoTributaId(annoId: number) 
	{
        this.pactosFlag = false;
        this.pactosFlagSinData = false;
        this.spinnerService.show();
        
		this.pactosService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => 
			{
                this.pactos = res.json;
                
				console.log(this.pactos);
				
				if (this.pactos.length > 0) { this.pactosFlag = true; } else { this.pactosFlagSinData = true; }
				
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => 
			{
                this.spinnerService.hide();
                this.onError(res.message);
            }
        );
    }

    loadAllTablaMoneda() 
	{
        this.spinnerService.show();
        this.tablaMonedaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tablaMonedas = res.json;
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    generateExcel() 
	{
        this.commonServices.generateExcel(document, 'table', this.excelService, this.eventManager, 'Datos Pactos',
            'entrada57bListModification', 'Deleted an entrada57b', this.jhiAlertService, 'No existen registros para exportar');
    }

    private onError(error) 
	{
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);    
	}
}