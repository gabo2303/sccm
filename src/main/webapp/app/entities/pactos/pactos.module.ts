import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    PactosComponent,
    PactosDeleteDialogComponent,
    PactosDeletePopupComponent,
    PactosDetailComponent,
    PactosDialogComponent,
    PactosPopupComponent,
    pactosPopupRoute,
    PactosPopupService,
    pactosRoute,
    PactosService,
} from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...pactosRoute,
    ...pactosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        PactosComponent,
        PactosDetailComponent,
        PactosDialogComponent,
        PactosDeleteDialogComponent,
        PactosPopupComponent,
        PactosDeletePopupComponent,
    ],
    entryComponents: [
        PactosComponent,
        PactosDialogComponent,
        PactosPopupComponent,
        PactosDeleteDialogComponent,
        PactosDeletePopupComponent,
    ],
    providers: [
        PactosService,
        PactosPopupService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmPactosModule {
}
