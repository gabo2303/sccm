import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Pactos } from './pactos.model';
import { PactosPopupService } from './pactos-popup.service';
import { PactosService } from './pactos.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-pactos-dialog',
    templateUrl: './pactos-dialog.component.html'
})
export class PactosDialogComponent implements OnInit {

    pactos: Pactos;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private pactosService: PactosService,
        private annoTributaService: AnnoTributaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
		console.log(this.pactos);
        this.isSaving = true;
        if (this.pactos.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pactosService.update(this.pactos));
        } else {
            this.subscribeToSaveResponse(
                this.pactosService.create(this.pactos));
        }
    }

    private subscribeToSaveResponse(result: Observable<Pactos>) {
        result.subscribe((res: Pactos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Pactos) {
        this.eventManager.broadcast({ name: 'pactosListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'operCapDataEmiCertListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAnnoTributaById(index: number, item: AnnoTributa) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pactos-popup',
    template: ''
})
export class PactosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pactosPopupService: PactosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pactosPopupService
                    .open(PactosDialogComponent as Component, params['id']);
            } else {
                this.pactosPopupService
                    .open(PactosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
