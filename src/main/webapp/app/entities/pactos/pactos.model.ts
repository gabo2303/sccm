import { BaseEntity } from './../../shared';

export class Pactos implements BaseEntity {
    constructor(
        public id?: number,
        public rut?: number,
        public dv?: string,
        public producto?: number,
        public moneda?: number,
        public fechaPago?: any,
        public correlativo?: number,
        public fechaInv?: any,
        public folio?: string,
        public filler?: string,
        public capital?: number,
        public capMonOri?: number,
        public intNom?: number,
        public intReal?: number,
        public monFi?: number,
        public numGiro?: number,
        public codMon?: number,
        public monPac?: string,
        public tasMon?: string,
        public tasa?: number,
        public perTasa?: number,
        public reajuste?: number,
        public tipoInt?: string,
        public codTipInt?: string,
        public fecConta?: any,
        public bipersonal?: string,
        public reajustabilidad?: string,
        public plazo?: number,
        public tipoPago?: string,
        public origen?: string,
        public annoTributa?: BaseEntity,
    ) {
    }
}
