import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Cliente1890Component } from './cliente-1890.component';
import { Cliente1890DetailComponent } from './cliente-1890-detail.component';
import { Cliente1890PopupComponent } from './cliente-1890-dialog.component';
import { Cliente1890DeletePopupComponent } from './cliente-1890-delete-dialog.component';

@Injectable()
export class Cliente1890ResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {  }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const cliente1890Route: Routes = 
[
    {
        path: 'cliente-1890',
        component: Cliente1890Component,
		resolve: {
            'pagingParams': Cliente1890ResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cliente1890.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cliente-1890/:id',
        component: Cliente1890DetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cliente1890.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cliente1890PopupRoute: Routes = 
[
    {
        path: 'cliente-1890-new',
        component: Cliente1890PopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cliente1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cliente-1890/:id/edit',
        component: Cliente1890PopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cliente1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cliente-1890/:id/delete',
        component: Cliente1890DeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.cliente1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
