export * from './cliente-1890.model';
export * from './cliente-1890-popup.service';
export * from './cliente-1890.service';
export * from './cliente-1890-dialog.component';
export * from './cliente-1890-delete-dialog.component';
export * from './cliente-1890-detail.component';
export * from './cliente-1890.component';
export * from './cliente-1890.route';
