import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Cliente1890 } from './cliente-1890.model';
import { Cliente1890PopupService } from './cliente-1890-popup.service';
import { Cliente1890Service } from './cliente-1890.service';

@Component({
    selector: 'jhi-cliente-1890-dialog',
    templateUrl: './cliente-1890-dialog.component.html'
})
export class Cliente1890DialogComponent implements OnInit {

    cliente1890: Cliente1890;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private cliente1890Service: Cliente1890Service,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cliente1890.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cliente1890Service.update(this.cliente1890));
        } else {
            this.subscribeToSaveResponse(
                this.cliente1890Service.create(this.cliente1890));
        }
    }

    private subscribeToSaveResponse(result: Observable<Cliente1890>) {
        result.subscribe((res: Cliente1890) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Cliente1890) {
        this.eventManager.broadcast({ name: 'cliente1890ListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-cliente-1890-popup',
    template: ''
})
export class Cliente1890PopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cliente1890PopupService: Cliente1890PopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cliente1890PopupService
                    .open(Cliente1890DialogComponent as Component, params['id']);
            } else {
                this.cliente1890PopupService
                    .open(Cliente1890DialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
