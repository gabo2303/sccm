import { BaseEntity } from './../../shared';

export class Cliente1890 implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public nacionalidad?: string,
        public impto35?: string,
        public direccion?: string,
        public comuna?: string,
        public ciudad?: string,
        public rut1890S?: BaseEntity[],
    ) {
    }
}
