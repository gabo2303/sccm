import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Cliente1890 } from './cliente-1890.model';
import { Cliente1890Service } from './cliente-1890.service';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cliente-1890',
    templateUrl: './cliente-1890.component.html'
})

export class Cliente1890Component implements OnInit, OnDestroy 
{
	cliente1890S: Cliente1890[];
	paginatedCliente1890S: Cliente1890[];
    currentAccount: any;
	routeData: any;
	itemsPerPage: any;
	page: any;
	predicate: any;
    previousPage: any;
    reverse: any;
	links: any;
	totalItems: any;
	queryCount: any;
    eventSubscriber: Subscription;
	
	cliente1890Flag = false;
    cliente1890FlagSinData = false;
	isPaginatedOnWeb = true;
	
    constructor (private cliente1890Service: Cliente1890Service, private parseLinks: JhiParseLinks, private jhiAlertService: JhiAlertService, 
				 private eventManager: JhiEventManager, private activatedRoute: ActivatedRoute, private principal: Principal, private router: Router,
				 private spinnerService: Ng4LoadingSpinnerService) { 
		
		this.itemsPerPage = ITEMS_PER_PAGE;
        
		this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
	}

    loadAll() 
	{				
        this.cliente1890Service.query({
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
	loadPage(page: number) 
	{
        if (page !== this.previousPage) 
		{
            this.previousPage = page;
            this.transition();
        }
    }
	
	transition() 
	{
        this.router.navigate([ 'cliente-1890' ], { queryParams: { page: this.page, size: this.itemsPerPage, } });
		
        this.isPaginatedOnWeb ? this.loadPageStatic() : this.loadAll();
    }
	
	private loadPageStatic() 
	{
        const start = (this.page - 1) * this.itemsPerPage;
        
		this.cliente1890S = this.paginatedCliente1890S.slice(start, start + this.itemsPerPage);
    }
	
    ngOnInit() 
	{		
		this.spinnerService.show();
		
        this.loadAll();
		
        this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInCliente1890S();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: Cliente1890) { return item.id; }
	
    registerChangeInCliente1890S() 
	{ 
		this.eventSubscriber = this.eventManager.subscribe('cliente1890ListModification', (response) => this.loadAll());
    }

    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
	
	private onSuccess(data, headers, downloadFile?) 
	{
		//this.links = this.parseLinks.parse(headers.get('link'));
		this.totalItems = data.length;
		this.queryCount = this.totalItems;
		this.cliente1890S = data;
		this.paginatedCliente1890S = data;
		
		if (this.cliente1890S.length > 0) 
		{
			if (this.isPaginatedOnWeb) { this.cliente1890S = this.paginatedCliente1890S.slice(0, this.itemsPerPage); }
			
			this.page = 1;
			this.cliente1890Flag = true;
		} 
		else { this.cliente1890FlagSinData = true; }
		
        this.spinnerService.hide();
    }
}