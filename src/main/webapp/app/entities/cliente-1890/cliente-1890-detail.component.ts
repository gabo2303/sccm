import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Cliente1890 } from './cliente-1890.model';
import { Cliente1890Service } from './cliente-1890.service';

@Component({
    selector: 'jhi-cliente-1890-detail',
    templateUrl: './cliente-1890-detail.component.html'
})
export class Cliente1890DetailComponent implements OnInit, OnDestroy {

    cliente1890: Cliente1890;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private cliente1890Service: Cliente1890Service,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCliente1890S();
    }

    load(id) {
        this.cliente1890Service.find(id).subscribe((cliente1890) => {
            this.cliente1890 = cliente1890;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCliente1890S() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cliente1890ListModification',
            (response) => this.load(this.cliente1890.id)
        );
    }
}
