import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Cliente1890 } from './cliente-1890.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class Cliente1890Service 
{
    private resourceUrl = SERVER_API_URL + 'api/cliente-1890-s';

    constructor(private http: Http) { }

    create(cliente1890: Cliente1890): Observable<Cliente1890> {
        
		const copy = this.convert(cliente1890);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(cliente1890: Cliente1890): Observable<Cliente1890> {
        
		const copy = this.convert(cliente1890);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Cliente1890> {
        
		return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        
		const options = createRequestOption(req);
        
		console.log("ENVIO EL REQUEST CON OPCION " + options);
		
		return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        console.log("RESPONDE LA WEA");
		const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
		console.log("RESULTADO DE TODO: ");
		console.log(result);
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Cliente1890.
     */
    private convertItemFromServer(json: any): Cliente1890 {
        
		const entity: Cliente1890 = Object.assign(new Cliente1890(), json);
        return entity;
    }

    /**
     * Convert a Cliente1890 to a JSON which can be sent to the server.
     */
    private convert(cliente1890: Cliente1890): Cliente1890 {
        
		const copy: Cliente1890 = Object.assign({}, cliente1890);
        return copy;
    }
}
