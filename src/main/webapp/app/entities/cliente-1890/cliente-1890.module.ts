import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../../shared';
import {
    Cliente1890Service,
    Cliente1890PopupService,
    Cliente1890Component,
    Cliente1890DetailComponent,
    Cliente1890DialogComponent,
    Cliente1890PopupComponent,
    Cliente1890DeletePopupComponent,
    Cliente1890DeleteDialogComponent,
	Cliente1890ResolvePagingParams,
    cliente1890Route,
    cliente1890PopupRoute,
} from './';

const ENTITY_STATES = [
    ...cliente1890Route,
    ...cliente1890PopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Cliente1890Component,
        Cliente1890DetailComponent,
        Cliente1890DialogComponent,
        Cliente1890DeleteDialogComponent,
        Cliente1890PopupComponent,
        Cliente1890DeletePopupComponent,
    ],
    entryComponents: [
        Cliente1890Component,
        Cliente1890DialogComponent,
        Cliente1890PopupComponent,
        Cliente1890DeleteDialogComponent,
        Cliente1890DeletePopupComponent,
    ],
    providers: [
        Cliente1890Service,
        Cliente1890PopupService,
		Cliente1890ResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class SccmCliente1890Module {}