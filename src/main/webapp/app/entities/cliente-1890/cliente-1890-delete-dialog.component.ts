import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Cliente1890 } from './cliente-1890.model';
import { Cliente1890PopupService } from './cliente-1890-popup.service';
import { Cliente1890Service } from './cliente-1890.service';

@Component({
    selector: 'jhi-cliente-1890-delete-dialog',
    templateUrl: './cliente-1890-delete-dialog.component.html'
})
export class Cliente1890DeleteDialogComponent {

    cliente1890: Cliente1890;

    constructor(
        private cliente1890Service: Cliente1890Service,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cliente1890Service.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cliente1890ListModification',
                content: 'Deleted an cliente1890'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cliente-1890-delete-popup',
    template: ''
})
export class Cliente1890DeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cliente1890PopupService: Cliente1890PopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cliente1890PopupService
                .open(Cliente1890DeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
