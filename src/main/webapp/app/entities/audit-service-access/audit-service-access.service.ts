import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { AuditServiceAccess } from './audit-service-access.model';
import { createRequestOption, ResponseWrapper } from '../../shared';

@Injectable()
export class AuditServiceAccessService {

    private resourceUrl = SERVER_API_URL + 'api/audit-service-accesses';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findAll(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/findAll`, options).map((res: Response) => this.convertResponse(res));
    }

    create(auditServiceAccess: AuditServiceAccess): Observable<AuditServiceAccess> {
        const copy = this.convert(auditServiceAccess);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(auditServiceAccess: AuditServiceAccess): Observable<AuditServiceAccess> {
        const copy = this.convert(auditServiceAccess);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AuditServiceAccess> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[ i ]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AuditServiceAccess.
     */
    private convertItemFromServer(json: any): AuditServiceAccess {
        const entity: AuditServiceAccess = Object.assign(new AuditServiceAccess(), json);
        entity.timestamp = this.dateUtils.convertDateTimeFromServer(json.timestamp);
        return entity;
    }

    /**
     * Convert a AuditServiceAccess to a JSON which can be sent to the server.
     */
    private convert(auditServiceAccess: AuditServiceAccess): AuditServiceAccess {
        const copy: AuditServiceAccess = Object.assign({}, auditServiceAccess);

        copy.timestamp = this.dateUtils.toDate(auditServiceAccess.timestamp);
        return copy;
    }
}
