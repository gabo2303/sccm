import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AuditServiceAccess } from './audit-service-access.model';
import { AuditServiceAccessPopupService } from './audit-service-access-popup.service';
import { AuditServiceAccessService } from './audit-service-access.service';

@Component({
    selector: 'jhi-audit-service-access-dialog',
    templateUrl: './audit-service-access-dialog.component.html'
})
export class AuditServiceAccessDialogComponent implements OnInit {

    auditServiceAccess: AuditServiceAccess;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private auditServiceAccessService: AuditServiceAccessService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.auditServiceAccess.id !== undefined) {
            this.subscribeToSaveResponse(
                this.auditServiceAccessService.update(this.auditServiceAccess));
        } else {
            this.subscribeToSaveResponse(
                this.auditServiceAccessService.create(this.auditServiceAccess));
        }
    }

    private subscribeToSaveResponse(result: Observable<AuditServiceAccess>) {
        result.subscribe((res: AuditServiceAccess) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AuditServiceAccess) {
        this.eventManager.broadcast({ name: 'auditServiceAccessListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-audit-service-access-popup',
    template: ''
})
export class AuditServiceAccessPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private auditServiceAccessPopupService: AuditServiceAccessPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.auditServiceAccessPopupService
                    .open(AuditServiceAccessDialogComponent as Component, params['id']);
            } else {
                this.auditServiceAccessPopupService
                    .open(AuditServiceAccessDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
