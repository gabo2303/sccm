import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { AuditServiceAccess } from './audit-service-access.model';
import { AuditServiceAccessService } from './audit-service-access.service';
import { CommonServices, ITEMS_PER_PAGE, PATTERN_DATE_TIME_EXTENDED, Principal, ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa';

@Component({
    selector: 'jhi-audit-service-access',
    templateUrl: './audit-service-access.component.html'
})
export class AuditServiceAccessComponent implements OnInit, OnDestroy {

    currentAccount: any;
    auditServiceAccesses: AuditServiceAccess[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    PATTERN_DATE_TIME_EXTENDED = PATTERN_DATE_TIME_EXTENDED;
    auditServiceAccessesFlag = false;
    auditServiceAccessesFlagSinData = false;

    constructor(private auditServiceAccessService: AuditServiceAccessService, private parseLinks: JhiParseLinks, private jhiAlertService: JhiAlertService,
                private principal: Principal, private activatedRoute: ActivatedRoute, private router: Router, private eventManager: JhiEventManager,
                private spinnerService: Ng4LoadingSpinnerService, private commonServices: CommonServices) {

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });
    }

    loadAll() {
        this.auditServiceAccessesFlag = false;
        this.auditServiceAccessesFlagSinData = false;
        this.auditServiceAccessService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate([ '/audit-service-access' ], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([ '/audit-service-access', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        } ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAuditServiceAccesses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AuditServiceAccess) {
        return item.id;
    }

    registerChangeInAuditServiceAccesses() {
        this.eventSubscriber = this.eventManager.subscribe('auditServiceAccessListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [ this.predicate + ',' + (this.reverse ? 'asc' : 'desc') ];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    generateExcel2() {
        this.spinnerService.show();
        this.auditServiceAccessService.findAll().subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, true),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccess(data, headers, downloadFile?) {
        if (downloadFile) {
            this.commonServices.download(data, 'Auditoría de Acceso a Servicios');
        } else {
            this.links = this.parseLinks.parse(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            // this.page = pagingParams.page;
            this.auditServiceAccesses = data;
            if (this.auditServiceAccesses.length > 0) {
                this.auditServiceAccessesFlag = true;
            } else {
                this.auditServiceAccessesFlagSinData = true;
            }
        }
        this.spinnerService.hide();
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }
}
