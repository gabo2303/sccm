import { BaseEntity } from './../../shared';

export class AuditServiceAccess implements BaseEntity {
    constructor(
        public id?: number,
        public timestamp?: any,
        public method?: string,
        public path?: string,
        public remoteAddress?: string,
        public timeTaken?: number,
        public userPrincipal?: string,
        public remoteUser?: string,
        public query?: string,
    ) {
    }
}
