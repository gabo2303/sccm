import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { AuditServiceAccess } from './audit-service-access.model';
import { AuditServiceAccessService } from './audit-service-access.service';

@Injectable()
export class AuditServiceAccessPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private datePipe: DatePipe, private modalService: NgbModal, private router: Router, private auditServiceAccessService: AuditServiceAccessService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.auditServiceAccessService.find(id).subscribe((auditServiceAccess) => {
                    auditServiceAccess.timestamp = this.datePipe.transform(auditServiceAccess.timestamp, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.auditServiceAccessModalRef(component, auditServiceAccess);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.auditServiceAccessModalRef(component, new AuditServiceAccess());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    auditServiceAccessModalRef(component: Component, auditServiceAccess: AuditServiceAccess): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.auditServiceAccess = auditServiceAccess;
        modalRef.result.then((result) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([ { outlets: { popup: null } } ], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
