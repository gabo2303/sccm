import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AuditServiceAccess } from './audit-service-access.model';
import { AuditServiceAccessService } from './audit-service-access.service';

@Component({
    selector: 'jhi-audit-service-access-detail',
    templateUrl: './audit-service-access-detail.component.html'
})
export class AuditServiceAccessDetailComponent implements OnInit, OnDestroy {

    auditServiceAccess: AuditServiceAccess;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private auditServiceAccessService: AuditServiceAccessService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAuditServiceAccesses();
    }

    load(id) {
        this.auditServiceAccessService.find(id).subscribe((auditServiceAccess) => {
            this.auditServiceAccess = auditServiceAccess;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAuditServiceAccesses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'auditServiceAccessListModification',
            (response) => this.load(this.auditServiceAccess.id)
        );
    }
}
