import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AuditServiceAccess } from './audit-service-access.model';
import { AuditServiceAccessPopupService } from './audit-service-access-popup.service';
import { AuditServiceAccessService } from './audit-service-access.service';

@Component({
    selector: 'jhi-audit-service-access-delete-dialog',
    templateUrl: './audit-service-access-delete-dialog.component.html'
})
export class AuditServiceAccessDeleteDialogComponent {

    auditServiceAccess: AuditServiceAccess;

    constructor(
        private auditServiceAccessService: AuditServiceAccessService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.auditServiceAccessService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'auditServiceAccessListModification',
                content: 'Deleted an auditServiceAccess'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-audit-service-access-delete-popup',
    template: ''
})
export class AuditServiceAccessDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private auditServiceAccessPopupService: AuditServiceAccessPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.auditServiceAccessPopupService
                .open(AuditServiceAccessDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
