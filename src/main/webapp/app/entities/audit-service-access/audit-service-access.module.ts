import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    AuditServiceAccessComponent,
    AuditServiceAccessDeleteDialogComponent,
    AuditServiceAccessDeletePopupComponent,
    AuditServiceAccessDetailComponent,
    AuditServiceAccessDialogComponent,
    AuditServiceAccessPopupComponent,
    auditServiceAccessPopupRoute,
    AuditServiceAccessPopupService,
    AuditServiceAccessResolvePagingParams,
    auditServiceAccessRoute,
    AuditServiceAccessService,
} from './';

const ENTITY_STATES = [
    ...auditServiceAccessRoute,
    ...auditServiceAccessPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AuditServiceAccessComponent,
        AuditServiceAccessDetailComponent,
        AuditServiceAccessDialogComponent,
        AuditServiceAccessDeleteDialogComponent,
        AuditServiceAccessPopupComponent,
        AuditServiceAccessDeletePopupComponent,
    ],
    entryComponents: [
        AuditServiceAccessComponent,
        AuditServiceAccessDialogComponent,
        AuditServiceAccessPopupComponent,
        AuditServiceAccessDeleteDialogComponent,
        AuditServiceAccessDeletePopupComponent,
    ],
    providers: [
        AuditServiceAccessService,
        AuditServiceAccessPopupService,
        AuditServiceAccessResolvePagingParams,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAuditServiceAccessModule {
}
