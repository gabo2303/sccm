import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AuditServiceAccessComponent } from './audit-service-access.component';
import { AuditServiceAccessDetailComponent } from './audit-service-access-detail.component';
import { AuditServiceAccessPopupComponent } from './audit-service-access-dialog.component';
import { AuditServiceAccessDeletePopupComponent } from './audit-service-access-delete-dialog.component';
import { ADMIN } from '../../app.constants';

@Injectable()
export class AuditServiceAccessResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const auditServiceAccessRoute: Routes = [
    {
        path: 'audit-service-access',
        component: AuditServiceAccessComponent,
        resolve: {
            'pagingParams': AuditServiceAccessResolvePagingParams
        },
        data: {
            authorities: [ ADMIN ],
            pageTitle: 'sccmApp.auditServiceAccess.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'audit-service-access/:id',
        component: AuditServiceAccessDetailComponent,
        data: {
            authorities: [ ADMIN ],
            pageTitle: 'sccmApp.auditServiceAccess.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const auditServiceAccessPopupRoute: Routes = [
    {
        path: 'audit-service-access-new',
        component: AuditServiceAccessPopupComponent,
        data: {
            authorities: [ ADMIN ],
            pageTitle: 'sccmApp.auditServiceAccess.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'audit-service-access/:id/edit',
        component: AuditServiceAccessPopupComponent,
        data: {
            authorities: [ ADMIN ],
            pageTitle: 'sccmApp.auditServiceAccess.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'audit-service-access/:id/delete',
        component: AuditServiceAccessDeletePopupComponent,
        data: {
            authorities: [ ADMIN ],
            pageTitle: 'sccmApp.auditServiceAccess.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
