import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Rut1890 } from './rut-1890.model';
import { Rut1890Service } from './rut-1890.service';
import { Clientes1890mantencionService } from '../clientes-1890-mantencion/clientes-1890-mantencion.service';

import { ExcelService } from '../../shared/excel/ExcelService';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonServices, } from '../../shared';

import { isNullOrUndefined } from 'util';

@Component({
    selector: 'jhi-rut-1890',
    templateUrl: './rut-1890.component.html'
})
export class Rut1890Component implements OnInit, OnDestroy 
{
	rut1890S: Rut1890[];
	paginatedRut1890S: Rut1890[];
	
	routeData: any;
	itemsPerPage: any;
	page: any;
	predicate: any;
    previousPage: any;
    reverse: any;
	links: any;
	totalItems: any;
	queryCount: any;
	rutFilterValue: string = null;
	
    currentAccount: any;
    eventSubscriber: Subscription;
	
	rut1890Flag = false;
	rut1890FlagSinData = false;
	isPaginatedOnWeb = true;
	
    constructor( private rut1890Service: Rut1890Service, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager,
				 private principal: Principal, private commonServices: CommonServices, private spinnerService: Ng4LoadingSpinnerService, 
				 private activatedRoute: ActivatedRoute, private excelService: ExcelService, private router: Router,
				 private clientes1890mantencionService: Clientes1890mantencionService,				 ) 
	{ 
		this.itemsPerPage = ITEMS_PER_PAGE;
        
		this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data[ 'pagingParams' ].page;
            this.previousPage = data[ 'pagingParams' ].page;
            this.reverse = data[ 'pagingParams' ].ascending;
            this.predicate = data[ 'pagingParams' ].predicate;
        });	
	}
	
	filterData() 
	{
        this.spinnerService.show();
        
		this.page = 1;
        this.rut1890Flag = false;
        this.rut1890FlagSinData = false;
        
		this.loadData();
    }

    loadData() 
	{
        this.spinnerService.show();
        const isRutNullOrEmpty = isNullOrUndefined(this.rutFilterValue) || this.rutFilterValue.length <= 0;
		
		if (isRutNullOrEmpty) 
		{
            const errorMsg = { message: 'error.rutOrFilterEmpty' };
            this.onError(errorMsg);
        } 
		else if (!isRutNullOrEmpty && isNaN(+this.rutFilterValue)) 
		{
            const errorMsg = { message: 'error.rutNotANumber' };
            this.onError(errorMsg);
        } 
		else 
		{
            const rutValue: number = isRutNullOrEmpty ? null : + this.rutFilterValue;
            
			this.rut1890Service.findAllByRut(rutValue).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
	
    loadAll() 
	{
        //this.rut1890Service.query({ page: this.page - 1, size: this.itemsPerPage })
		//.subscribe( (res: ResponseWrapper) => this.onSuccess(res.json, res.headers), (res: ResponseWrapper) => this.onError(res.json) );
    }
	
	loadPage(page: number) 
	{
        if (page !== this.previousPage) 
		{
            this.previousPage = page;
            this.transition();
        }
    }
	
	transition() 
	{
        this.router.navigate([ 'rut-1890' ], { queryParams: { page: this.page, size: this.itemsPerPage, } });
		
        this.isPaginatedOnWeb ? this.loadPageStatic() : this.loadAll();
    }
	
	private loadPageStatic() 
	{
        const start = (this.page - 1) * this.itemsPerPage;
        
		this.rut1890S = this.paginatedRut1890S.slice(start, start + this.itemsPerPage);
    }
	
    ngOnInit() 
	{
		console.log("ENTRA AL RUT 1890 ");
		this.spinnerService.show();
		
        //this.loadAll();
        
		this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInRut1890S();
		
		this.spinnerService.hide();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: Rut1890) { return item.id; }
	
    registerChangeInRut1890S() { this.eventSubscriber = this.eventManager.subscribe('rut1890ListModification', (response) => this.loadData()); }

	generateExcel() 
	{
        this.spinnerService.show();
        
		this.clientes1890mantencionService.query().subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, true),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
	
	private onSuccess(data, headers, downloadFile?) 
	{
		if (downloadFile) 
		{
			console.log(data);
			
            this.commonServices.download(data, 'Datos de Clientes en Mantencion');
        } 
		else 
		{
			this.totalItems = data.length;
			this.queryCount = this.totalItems;
			this.rut1890S = data;
			this.paginatedRut1890S = data;
			
			if (this.rut1890S.length > 0) 
			{
				console.log("ENTRA AL IF PARA PAGINAR");
				this.rut1890S = this.paginatedRut1890S.slice(0, this.itemsPerPage); 
				
				this.page = 1;
				this.rut1890Flag = true;
			} 
			else { this.rut1890FlagSinData = true; }			
		}
		
		this.spinnerService.hide();
    }
}