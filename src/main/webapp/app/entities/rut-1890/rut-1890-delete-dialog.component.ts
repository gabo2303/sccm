import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Rut1890 } from './rut-1890.model';
import { Rut1890PopupService } from './rut-1890-popup.service';
import { Rut1890Service } from './rut-1890.service';

@Component({
    selector: 'jhi-rut-1890-delete-dialog',
    templateUrl: './rut-1890-delete-dialog.component.html'
})
export class Rut1890DeleteDialogComponent {

    rut1890: Rut1890;

    constructor(
        private rut1890Service: Rut1890Service,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.rut1890Service.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rut1890ListModification',
                content: 'Deleted an rut1890'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-rut-1890-delete-popup',
    template: ''
})
export class Rut1890DeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rut1890PopupService: Rut1890PopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.rut1890PopupService
                .open(Rut1890DeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
