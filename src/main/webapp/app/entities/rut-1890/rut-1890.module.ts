import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    Rut1890Service,
    Rut1890PopupService,
    Rut1890Component,
    Rut1890DetailComponent,
    Rut1890DialogComponent,
    Rut1890PopupComponent,
    Rut1890DeletePopupComponent,
    Rut1890DeleteDialogComponent,
	Rut1890ResolvePagingParams,
    rut1890Route,
    rut1890PopupRoute,
} from './';

const ENTITY_STATES = [
    ...rut1890Route,
    ...rut1890PopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        Rut1890Component,
        Rut1890DetailComponent,
        Rut1890DialogComponent,
        Rut1890DeleteDialogComponent,
        Rut1890PopupComponent,
        Rut1890DeletePopupComponent,
    ],
    entryComponents: [
        Rut1890Component,
        Rut1890DialogComponent,
        Rut1890PopupComponent,
        Rut1890DeleteDialogComponent,
        Rut1890DeletePopupComponent,
    ],
    providers: [
        Rut1890Service,
        Rut1890PopupService,
		Rut1890ResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmRut1890Module {}
