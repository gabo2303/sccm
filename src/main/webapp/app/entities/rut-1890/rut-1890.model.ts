import { BaseEntity } from './../../shared';

export class Rut1890 implements BaseEntity 
{
    constructor( public id?: number, public rut?: number, public dv?: string, public cliente1890?: BaseEntity, ) { }
}