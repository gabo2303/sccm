import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Rut1890Component } from './rut-1890.component';
import { Rut1890DetailComponent } from './rut-1890-detail.component';
import { Rut1890PopupComponent } from './rut-1890-dialog.component';
import { Rut1890DeletePopupComponent } from './rut-1890-delete-dialog.component';

@Injectable()
export class Rut1890ResolvePagingParams implements Resolve<any> 
{
    constructor(private paginationUtil: JhiPaginationUtil) {  }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) 
	{
        const page = route.queryParams[ 'page' ] ? route.queryParams[ 'page' ] : '1';
        const sort = route.queryParams[ 'sort' ] ? route.queryParams[ 'sort' ] : 'id,asc';
        
		return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const rut1890Route: Routes = 
[
    {
        path: 'rut-1890',
        component: Rut1890Component,
		resolve: {
            'pagingParams': Rut1890ResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.rut1890.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rut-1890/:id',
        component: Rut1890DetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.rut1890.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rut1890PopupRoute: Routes = 
[
    {
        path: 'rut-1890-new',
        component: Rut1890PopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.rut1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rut-1890/:id/edit',
        component: Rut1890PopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.rut1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rut-1890/:id/delete',
        component: Rut1890DeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'],
            pageTitle: 'sccmApp.rut1890.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];