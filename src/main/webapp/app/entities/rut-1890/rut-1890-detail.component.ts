import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Rut1890 } from './rut-1890.model';
import { Rut1890Service } from './rut-1890.service';

@Component({
    selector: 'jhi-rut-1890-detail',
    templateUrl: './rut-1890-detail.component.html'
})
export class Rut1890DetailComponent implements OnInit, OnDestroy {

    rut1890: Rut1890;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private rut1890Service: Rut1890Service,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRut1890S();
    }

    load(id) {
        this.rut1890Service.find(id).subscribe((rut1890) => {
            this.rut1890 = rut1890;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRut1890S() {
        this.eventSubscriber = this.eventManager.subscribe(
            'rut1890ListModification',
            (response) => this.load(this.rut1890.id)
        );
    }
}
