import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Rut1890 } from './rut-1890.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import { isNullOrUndefined } from 'util';

@Injectable()
export class Rut1890Service 
{
    private resourceUrl = SERVER_API_URL + 'api/rut-1890-s';

    constructor(private http: Http) { }

    create(rut1890: Rut1890): Observable<Rut1890> 
	{
        const copy = this.convert(rut1890);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(rut1890: Rut1890): Observable<Rut1890> 
	{
        const copy = this.convert(rut1890);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Rut1890> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }
	
	findAllByRut(rut: number): Observable<ResponseWrapper> 
	{
        const rutFinal : string = (!isNullOrUndefined(rut) ? `rut=${rut}` : '');
        
		console.log("SE COMUNICA CON EL SERVICIO");
		
		return this.http.get(`${this.resourceUrl}/findAllByRutDataTypePaginated?${rutFinal}`).map(
            (res: Response) => this.convertResponse(res)
        );
    }
	
    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Rut1890.
     */
    private convertItemFromServer(json: any): Rut1890 
	{
        const entity: Rut1890 = Object.assign(new Rut1890(), json);
        
		return entity;
    }

    /**
     * Convert a Rut1890 to a JSON which can be sent to the server.
     */
    private convert(rut1890: Rut1890): Rut1890 
	{
        const copy: Rut1890 = Object.assign({}, rut1890);
        
		return copy;
    }
}