import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Rut1890 } from './rut-1890.model';
import { Rut1890PopupService } from './rut-1890-popup.service';
import { Rut1890Service } from './rut-1890.service';
import { Cliente1890, Cliente1890Service } from '../cliente-1890';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-rut-1890-dialog',
    templateUrl: './rut-1890-dialog.component.html'
})
export class Rut1890DialogComponent implements OnInit 
{
    rut1890: Rut1890;
    isSaving: boolean;
	
	impt35s: String[];
	nacionalidades: String[];
	
    constructor( public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, private rut1890Service: Rut1890Service,
				 private cliente1890Service: Cliente1890Service, private eventManager: JhiEventManager ) {  }

    ngOnInit() 
	{ 
		this.impt35s = ['N', 'S'];
		this.nacionalidades = ['N', 'S'];
		
		this.isSaving = false; 
	}

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.isSaving = true;
        
		if (this.rut1890.id !== undefined) 
		{
			console.log(this.rut1890.cliente1890);
			
			this.subscribeToSaveResponseCliente1890(this.cliente1890Service.update(this.rut1890.cliente1890));
			
            this.subscribeToSaveResponse(this.rut1890Service.update(this.rut1890));
        } 
		else { this.subscribeToSaveResponse(this.rut1890Service.create(this.rut1890)); }
    }

    private subscribeToSaveResponse(result: Observable<Rut1890>) 
	{
        result.subscribe((res: Rut1890) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

	private subscribeToSaveResponseCliente1890(result: Observable<Cliente1890>) 
	{
        result.subscribe((res: Cliente1890) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Rut1890) 
	{
        this.eventManager.broadcast({ name: 'rut1890ListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }

    trackCliente1890ById(index: number, item: Cliente1890) { return item.id; }
}

@Component({
    selector: 'jhi-rut-1890-popup',
    template: ''
})
export class Rut1890PopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;

    constructor( private route: ActivatedRoute, private rut1890PopupService: Rut1890PopupService ) {  }

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => 
		{
            if ( params['id'] ) 
			{
                this.rut1890PopupService.open(Rut1890DialogComponent as Component, params['id']);
            } 
			else { this.rut1890PopupService.open(Rut1890DialogComponent as Component); }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}