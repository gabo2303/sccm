export class AhorroIR {
    constructor(
        public id?: number,
        public folio?: string,
        public intReal?: number,
        public irFinalCal?: number,
        public diferencia?: number,
        public difAbs?: number,
        public siCalculado?: number,
        public giros?: number,
        public abonos?: number,
        public sfCalculado?: number,
        public annoTributa?: number
    ) {
    }
}
