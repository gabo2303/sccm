import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { IntRealAhorroComponent } from './intRealAhorro.component';
import { SCCM_1890_USER_AHORRO } from '../../app.constants';

export const intRealAhorroRoute: Routes = [
    {
        path: 'intRealAho',
        component: IntRealAhorroComponent,
        data: {
            authorities: [ SCCM_1890_USER_AHORRO ],
            pageTitle: 'sccmApp.ahorro.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];
