import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { AhorroIR } from './intRealAhorro.model';
import { IntRealAhorroService } from './intRealAhorro.service';
import { CommonServices, Principal, ResponseWrapper } from '../../shared';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AnnoTributaService } from '../anno-tributa/anno-tributa.service';
import { AnnoTributa } from '../anno-tributa/anno-tributa.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ExcelService } from '../../shared/excel/ExcelService';

@Component({
    selector: 'jhi-ahorrosIR',
    templateUrl: './intRealAhorro.component.html',
    styleUrls: [
        'intRealAhorro.css'
    ]
})
export class IntRealAhorroComponent implements OnInit, OnDestroy {
    ahorrosIR: AhorroIR[];
    currentAccount: any;
    eventSubscriber: Subscription;
    annosTributarios: AnnoTributa[];
    annoSelected: number;
    pactosFlag = false;
    pactosFlagSinData = false;
    rutSelected: number;

    constructor(private ahorroIRService: IntRealAhorroService, private jhiAlertService: JhiAlertService, private eventManager: JhiEventManager, private principal: Principal,
                private spinnerService: Ng4LoadingSpinnerService, private annoTributaService: AnnoTributaService, private commonServices: CommonServices,
                private excelService: ExcelService) {
    }

    ngOnInit() {
        this.loadAnnosTributarios();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPactos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AhorroIR) {
        return item.folio;
    }

    registerChangeInPactos() {
        this.eventSubscriber = this.eventManager.subscribe('ahorroIRListModification', (response) => this.loadAllByAnnoTributaId(this.annoSelected));
    }

    private onError(error) {
        this.spinnerService.hide();
        this.jhiAlertService.error(error.message, null, null);
    }

    loadAnnosTributarios() {
        this.spinnerService.show();
        this.annoTributaService.query().subscribe(
            (res: ResponseWrapper) => {
                this.annosTributarios = res.json;
                // this.annosTributarios.splice(0, 1);
                this.spinnerService.hide();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    filterByAnooTributario(annoTributaId: any) {
        this.rutSelected = null;
        this.annoSelected = annoTributaId.value;
        this.loadAllByAnnoTributaId(this.annoSelected);
    }

    loadAllByAnnoTributaId(annoId: number) {
        this.pactosFlag = false;
        this.pactosFlagSinData = false;
        this.spinnerService.show();
        this.ahorroIRService.loadAllByAnnoTributaId(annoId).subscribe(
            (res: ResponseWrapper) => {
                this.ahorrosIR = res.json;
                if (this.ahorrosIR.length > 0) {
                    this.pactosFlag = true;
                } else {
                    this.pactosFlagSinData = true;
                }
                this.spinnerService.hide();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    filterByRut(rutSelected: any) {
        this.spinnerService.show();
        this.rutSelected = parseInt(rutSelected.value, 10);
        this.spinnerService.hide();
    }

    verifyIsDate(dateToEval: any) {
        return dateToEval.getDate() > 0 || dateToEval.getDate() < 0 || dateToEval.getDate() === 0 ?
            dateToEval.getDate() + '/' + dateToEval.getMonth() + '/' + dateToEval.getFullYear() :
            'Sin fecha válida';
    }

    generateExcel() {
        this.commonServices.generateExcel(document, 'main-data-table', this.excelService, this.eventManager, 'Cuadratura Interés Real',
            'ahorroListModification', 'Deleted an ahorro', this.jhiAlertService, 'No existen registros para exportar');
    }

}
