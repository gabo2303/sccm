import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import { IntRealAhorroComponent, intRealAhorroRoute, IntRealAhorroService } from './';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';

const ENTITY_STATES = [
    ...intRealAhorroRoute
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        SharedPipesModule
    ],
    declarations: [
        IntRealAhorroComponent,
    ],
    entryComponents: [
        IntRealAhorroComponent,
    ],
    providers: [
        IntRealAhorroService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmIntRealAhorroModule {
}
