import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { UltDiaHabAnioUltDiaHabAnio } from './ult-dia-hab-anio-ult-dia-hab-anio.model';
import { UltDiaHabAnioUltDiaHabAnioService } from './ult-dia-hab-anio-ult-dia-hab-anio.service';

@Component({
    selector: 'jhi-ult-dia-hab-anio-ult-dia-hab-anio-detail',
    templateUrl: './ult-dia-hab-anio-ult-dia-hab-anio-detail.component.html'
})
export class UltDiaHabAnioUltDiaHabAnioDetailComponent implements OnInit, OnDestroy {

    ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ultDiaHabAnioService: UltDiaHabAnioUltDiaHabAnioService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUltDiaHabAnios();
    }

    load(id) {
        this.ultDiaHabAnioService.find(id).subscribe((ultDiaHabAnio) => {
            this.ultDiaHabAnio = ultDiaHabAnio;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUltDiaHabAnios() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ultDiaHabAnioListModification',
            (response) => this.load(this.ultDiaHabAnio.id)
        );
    }
}
