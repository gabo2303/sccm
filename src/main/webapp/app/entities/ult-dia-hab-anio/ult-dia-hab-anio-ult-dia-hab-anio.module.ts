import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    UltDiaHabAnioUltDiaHabAnioService,
    UltDiaHabAnioUltDiaHabAnioPopupService,
    UltDiaHabAnioUltDiaHabAnioComponent,
    UltDiaHabAnioUltDiaHabAnioDetailComponent,
    UltDiaHabAnioUltDiaHabAnioDialogComponent,
    UltDiaHabAnioUltDiaHabAnioPopupComponent,
    UltDiaHabAnioUltDiaHabAnioDeletePopupComponent,
    UltDiaHabAnioUltDiaHabAnioDeleteDialogComponent,
    ultDiaHabAnioRoute,
    ultDiaHabAnioPopupRoute,
    UltDiaHabAnioUltDiaHabAnioResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...ultDiaHabAnioRoute,
    ...ultDiaHabAnioPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UltDiaHabAnioUltDiaHabAnioComponent,
        UltDiaHabAnioUltDiaHabAnioDetailComponent,
        UltDiaHabAnioUltDiaHabAnioDialogComponent,
        UltDiaHabAnioUltDiaHabAnioDeleteDialogComponent,
        UltDiaHabAnioUltDiaHabAnioPopupComponent,
        UltDiaHabAnioUltDiaHabAnioDeletePopupComponent,
    ],
    entryComponents: [
        UltDiaHabAnioUltDiaHabAnioComponent,
        UltDiaHabAnioUltDiaHabAnioDialogComponent,
        UltDiaHabAnioUltDiaHabAnioPopupComponent,
        UltDiaHabAnioUltDiaHabAnioDeleteDialogComponent,
        UltDiaHabAnioUltDiaHabAnioDeletePopupComponent,
    ],
    providers: [
        UltDiaHabAnioUltDiaHabAnioService,
        UltDiaHabAnioUltDiaHabAnioPopupService,
        UltDiaHabAnioUltDiaHabAnioResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmUltDiaHabAnioUltDiaHabAnioModule {}
