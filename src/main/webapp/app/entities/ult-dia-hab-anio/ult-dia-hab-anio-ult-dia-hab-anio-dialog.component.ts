import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UltDiaHabAnioUltDiaHabAnio } from './ult-dia-hab-anio-ult-dia-hab-anio.model';
import { UltDiaHabAnioUltDiaHabAnioPopupService } from './ult-dia-hab-anio-ult-dia-hab-anio-popup.service';
import { UltDiaHabAnioUltDiaHabAnioService } from './ult-dia-hab-anio-ult-dia-hab-anio.service';
import { AnnoTributa, AnnoTributaService } from '../anno-tributa';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ult-dia-hab-anio-ult-dia-hab-anio-dialog',
    templateUrl: './ult-dia-hab-anio-ult-dia-hab-anio-dialog.component.html'
})
export class UltDiaHabAnioUltDiaHabAnioDialogComponent implements OnInit 
{
    ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio;
    isSaving: boolean;

    annotributas: AnnoTributa[];

    constructor( public activeModal: NgbActiveModal, private jhiAlertService: JhiAlertService, 
				 private ultDiaHabAnioService: UltDiaHabAnioUltDiaHabAnioService, private annoTributaService: AnnoTributaService, 
				 private eventManager: JhiEventManager ) { }

    ngOnInit() 
	{
        this.isSaving = false;
		
		this.annoTributaService.query()
            .subscribe((res: ResponseWrapper) => { this.annotributas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() { this.activeModal.dismiss('cancel'); }

    save() 
	{
        this.isSaving = true;
        
		if (this.ultDiaHabAnio.id !== undefined) 
		{
			this.ultDiaHabAnio.dia = this.ultDiaHabAnio.fecha.split('-')[2].split('T')[0];
			this.ultDiaHabAnio.mes = this.ultDiaHabAnio.fecha.split('-')[1];
			var annioIngresado = this.ultDiaHabAnio.fecha.split('-')[0];
			var annioFinal = null;
			
			for(let i = 0; i < this.annotributas.length; i++)				
			{
				if(this.annotributas[i].annoC == annioIngresado) { annioFinal = this.annotributas[i]; }
			}
			
			if(annioFinal != null)
			{
				this.ultDiaHabAnio.annoTributario = annioFinal;
				
				this.subscribeToSaveResponse(this.ultDiaHabAnioService.update(this.ultDiaHabAnio));
			}
			else
			{
				this.jhiAlertService.error("sccmApp.ultDiaHabAnio.annoNoExiste");
				this.activeModal.dismiss();
			}
        } 
		else 
		{ 
			this.ultDiaHabAnio.dia = this.ultDiaHabAnio.fecha.split('-')[2].split('T')[0];
			this.ultDiaHabAnio.mes = this.ultDiaHabAnio.fecha.split('-')[1];
			var annioIngresado = this.ultDiaHabAnio.fecha.split('-')[0];
			var annioFinal = null;
			
			for(let i = 0; i < this.annotributas.length; i++)				
			{
				if(this.annotributas[i].annoC == annioIngresado) { annioFinal = this.annotributas[i]; }
			}
			
			if(annioFinal != null)
			{
				this.ultDiaHabAnio.annoTributario = annioFinal;
				
				this.subscribeToSaveResponse(this.ultDiaHabAnioService.create(this.ultDiaHabAnio)); 
			}
			else
			{
				this.jhiAlertService.error("sccmApp.ultDiaHabAnio.annoNoExiste");
				this.activeModal.dismiss();
			}
		}
    }

    private subscribeToSaveResponse(result: Observable<UltDiaHabAnioUltDiaHabAnio>) 
	{
        result.subscribe
		(
			(res: UltDiaHabAnioUltDiaHabAnio) => this.onSaveSuccess(res), 
			(res: Response) => this.onSaveError()
		);
    }

    private onSaveSuccess(result: UltDiaHabAnioUltDiaHabAnio) 
	{
        this.eventManager.broadcast({ name: 'ultDiaHabAnioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() { this.isSaving = false; }

    private onError(error: any) { this.jhiAlertService.error(error.message, null, null); }

    trackAnnoTributaById(index: number, item: AnnoTributa) { return item.id; }
}

@Component({
    selector: 'jhi-ult-dia-hab-anio-ult-dia-hab-anio-popup',
    template: ''
})
export class UltDiaHabAnioUltDiaHabAnioPopupComponent implements OnInit, OnDestroy 
{
    routeSub: any;
	
    constructor( private route: ActivatedRoute, private ultDiaHabAnioPopupService: UltDiaHabAnioUltDiaHabAnioPopupService ) { }

    ngOnInit() 
	{
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) 
			{
                this.ultDiaHabAnioPopupService.open(UltDiaHabAnioUltDiaHabAnioDialogComponent as Component, params['id']);
            } 
			else { this.ultDiaHabAnioPopupService.open(UltDiaHabAnioUltDiaHabAnioDialogComponent as Component); }
        });
    }

    ngOnDestroy() { this.routeSub.unsubscribe(); }
}