import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UltDiaHabAnioUltDiaHabAnioComponent } from './ult-dia-hab-anio-ult-dia-hab-anio.component';
import { UltDiaHabAnioUltDiaHabAnioDetailComponent } from './ult-dia-hab-anio-ult-dia-hab-anio-detail.component';
import { UltDiaHabAnioUltDiaHabAnioPopupComponent } from './ult-dia-hab-anio-ult-dia-hab-anio-dialog.component';
import {
    UltDiaHabAnioUltDiaHabAnioDeletePopupComponent
} from './ult-dia-hab-anio-ult-dia-hab-anio-delete-dialog.component';

@Injectable()
export class UltDiaHabAnioUltDiaHabAnioResolvePagingParams implements Resolve<any> 
{
    constructor(private paginationUtil: JhiPaginationUtil) {  }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) 
	{
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        
		return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
		};
    }
}

export const ultDiaHabAnioRoute: Routes = 
[
    {
        path: 'ult-dia-hab-anio-ult-dia-hab-anio',
        component: UltDiaHabAnioUltDiaHabAnioComponent,
        resolve: {
            'pagingParams': UltDiaHabAnioUltDiaHabAnioResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.ultDiaHabAnio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, 
	{
        path: 'ult-dia-hab-anio-ult-dia-hab-anio/:id',
        component: UltDiaHabAnioUltDiaHabAnioDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.ultDiaHabAnio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ultDiaHabAnioPopupRoute: Routes = 
[
    {
        path: 'ult-dia-hab-anio-ult-dia-hab-anio-new',
        component: UltDiaHabAnioUltDiaHabAnioPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.ultDiaHabAnio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ult-dia-hab-anio-ult-dia-hab-anio/:id/edit',
        component: UltDiaHabAnioUltDiaHabAnioPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.ultDiaHabAnio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ult-dia-hab-anio-ult-dia-hab-anio/:id/delete',
        component: UltDiaHabAnioUltDiaHabAnioDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_SUPERADMIN', 'ROLE_ADMIN'],
            pageTitle: 'sccmApp.ultDiaHabAnio.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];