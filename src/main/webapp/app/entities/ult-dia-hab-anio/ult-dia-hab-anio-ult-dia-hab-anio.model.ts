import { BaseEntity } from './../../shared';

export class UltDiaHabAnioUltDiaHabAnio implements BaseEntity 
{ 
	constructor( 
		public id?: number, 
		public mes?: number, 
		public dia?: number, 
		public anno_tributa?: number, 
		public fecha?: any, 
		public annoTributario?: BaseEntity,
    ) 
	{  }
}