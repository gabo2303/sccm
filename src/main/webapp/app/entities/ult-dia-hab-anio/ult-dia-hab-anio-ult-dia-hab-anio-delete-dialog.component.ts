import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UltDiaHabAnioUltDiaHabAnio } from './ult-dia-hab-anio-ult-dia-hab-anio.model';
import { UltDiaHabAnioUltDiaHabAnioPopupService } from './ult-dia-hab-anio-ult-dia-hab-anio-popup.service';
import { UltDiaHabAnioUltDiaHabAnioService } from './ult-dia-hab-anio-ult-dia-hab-anio.service';

@Component({
    selector: 'jhi-ult-dia-hab-anio-ult-dia-hab-anio-delete-dialog',
    templateUrl: './ult-dia-hab-anio-ult-dia-hab-anio-delete-dialog.component.html'
})
export class UltDiaHabAnioUltDiaHabAnioDeleteDialogComponent {

    ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio;

    constructor(
        private ultDiaHabAnioService: UltDiaHabAnioUltDiaHabAnioService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ultDiaHabAnioService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ultDiaHabAnioListModification',
                content: 'Deleted an ultDiaHabAnio'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ult-dia-hab-anio-ult-dia-hab-anio-delete-popup',
    template: ''
})
export class UltDiaHabAnioUltDiaHabAnioDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ultDiaHabAnioPopupService: UltDiaHabAnioUltDiaHabAnioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ultDiaHabAnioPopupService
                .open(UltDiaHabAnioUltDiaHabAnioDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
