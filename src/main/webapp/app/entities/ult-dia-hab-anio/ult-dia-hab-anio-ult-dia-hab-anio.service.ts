import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UltDiaHabAnioUltDiaHabAnio } from './ult-dia-hab-anio-ult-dia-hab-anio.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UltDiaHabAnioUltDiaHabAnioService 
{
    private resourceUrl = SERVER_API_URL + 'api/ult-dia-hab-anios';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio): Observable<UltDiaHabAnioUltDiaHabAnio> 
	{
        const copy = this.convert(ultDiaHabAnio);
        
		return this.http.post(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            
			return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio): Observable<UltDiaHabAnioUltDiaHabAnio> 
	{
        const copy = this.convert(ultDiaHabAnio);
        
		return this.http.put(this.resourceUrl, copy).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<UltDiaHabAnioUltDiaHabAnio> 
	{
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => 
		{
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> 
	{
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> { return this.http.delete(`${this.resourceUrl}/${id}`); }

    private convertResponse(res: Response): ResponseWrapper 
	{
        const jsonResponse = res.json();
        const result = [];
        
		for (let i = 0; i < jsonResponse.length; i++) { result.push(this.convertItemFromServer(jsonResponse[i])); }
		
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UltDiaHabAnioUltDiaHabAnio.
     */
    private convertItemFromServer(json: any): UltDiaHabAnioUltDiaHabAnio 
	{
        const entity: UltDiaHabAnioUltDiaHabAnio = Object.assign(new UltDiaHabAnioUltDiaHabAnio(), json);
        
		entity.fecha = this.dateUtils.convertDateTimeFromServer(json.fecha);
		
        return entity;
    }

    /**
     * Convert a UltDiaHabAnioUltDiaHabAnio to a JSON which can be sent to the server.
     */
    private convert(ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio): UltDiaHabAnioUltDiaHabAnio 
	{
        const copy: UltDiaHabAnioUltDiaHabAnio = Object.assign({}, ultDiaHabAnio);

        copy.fecha = this.dateUtils.toDate(ultDiaHabAnio.fecha);
        
		return copy;
    }
}