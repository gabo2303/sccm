import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { UltDiaHabAnioUltDiaHabAnio } from './ult-dia-hab-anio-ult-dia-hab-anio.model';
import { UltDiaHabAnioUltDiaHabAnioService } from './ult-dia-hab-anio-ult-dia-hab-anio.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ult-dia-hab-anio-ult-dia-hab-anio',
    templateUrl: './ult-dia-hab-anio-ult-dia-hab-anio.component.html'
})
export class UltDiaHabAnioUltDiaHabAnioComponent implements OnInit, OnDestroy {

currentAccount: any;
    ultDiaHabAnios: UltDiaHabAnioUltDiaHabAnio[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor( private ultDiaHabAnioService: UltDiaHabAnioUltDiaHabAnioService, private parseLinks: JhiParseLinks, private jhiAlertService: JhiAlertService,
				 private principal: Principal, private activatedRoute: ActivatedRoute, private router: Router, private eventManager: JhiEventManager ) 
	{
        this.itemsPerPage = ITEMS_PER_PAGE;
        
		this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    loadAll() 
	{
        this.ultDiaHabAnioService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
	
    loadPage(page: number) 
	{
        if (page !== this.previousPage) 
		{
            this.previousPage = page;
            this.transition();
        }
    }
	
    transition() 
	{
        this.router.navigate(['/ult-dia-hab-anio-ult-dia-hab-anio'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
		
        this.loadAll();
    }

    clear() 
	{
        this.page = 0;
        
		this.router.navigate(['/ult-dia-hab-anio-ult-dia-hab-anio', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
		
        this.loadAll();
    }
	
    ngOnInit() 
	{
        this.loadAll();
		
        this.principal.identity().then((account) => { this.currentAccount = account; });
		
        this.registerChangeInUltDiaHabAnios();
    }

    ngOnDestroy() { this.eventManager.destroy(this.eventSubscriber); }

    trackId(index: number, item: UltDiaHabAnioUltDiaHabAnio) { return item.id; }
	
    registerChangeInUltDiaHabAnios() 
	{
        this.eventSubscriber = this.eventManager.subscribe('ultDiaHabAnioListModification', (response) => this.loadAll());
    }

    sort() 
	{
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        
		if (this.predicate !== 'id') { result.push('id'); }
		
        return result;
    }

    private onSuccess(data, headers) 
	{
		for(let i = 0; i < data.length; i++)
		{
			var fecha = new Date(data[i].fecha);
			data[i].fecha = fecha.setDate(fecha.getDate() + 1);
		}
		
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.ultDiaHabAnios = data;
    }
	
    private onError(error) { this.jhiAlertService.error(error.message, null, null); }
}