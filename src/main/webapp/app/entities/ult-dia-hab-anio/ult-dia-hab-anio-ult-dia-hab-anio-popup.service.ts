import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { UltDiaHabAnioUltDiaHabAnio } from './ult-dia-hab-anio-ult-dia-hab-anio.model';
import { UltDiaHabAnioUltDiaHabAnioService } from './ult-dia-hab-anio-ult-dia-hab-anio.service';

@Injectable()
export class UltDiaHabAnioUltDiaHabAnioPopupService 
{
    private ngbModalRef: NgbModalRef;

    constructor( private datePipe: DatePipe, private modalService: NgbModal, private router: Router, 
				 private ultDiaHabAnioService: UltDiaHabAnioUltDiaHabAnioService ) 
	{
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> 
	{
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) 
			{
                this.ultDiaHabAnioService.find(id).subscribe((ultDiaHabAnio) => {
                    ultDiaHabAnio.fecha = this.datePipe
                        .transform(ultDiaHabAnio.fecha, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.ultDiaHabAnioModalRef(component, ultDiaHabAnio);
                    resolve(this.ngbModalRef);
                });
            } 
			else 
			{
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.ultDiaHabAnioModalRef(component, new UltDiaHabAnioUltDiaHabAnio());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ultDiaHabAnioModalRef(component: Component, ultDiaHabAnio: UltDiaHabAnioUltDiaHabAnio): NgbModalRef 
	{
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        
		modalRef.componentInstance.ultDiaHabAnio = ultDiaHabAnio;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        
		return modalRef;
    }
}