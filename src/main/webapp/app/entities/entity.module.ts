import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { SccmDatosFijosModule } from './datos-fijos/datos-fijos.module';
import { SccmGlosaModule } from './glosa/glosa.module';
import { SccmClienteBiceModule } from './cliente-bice/cliente-bice.module';
import { SccmInstrumentoModule } from './instrumento/instrumento.module';
import { SccmRepresentanteModule } from './representante/representante.module';
import { SccmFutModule } from './fut/fut.module';
import { SccmProductoModule } from './producto/producto.module';
import { SccmSCCMUserRoleModule } from './s-ccm-user-role/sccm-user-role.module';
import { SccmAnnoTributaModule } from './anno-tributa/anno-tributa.module';
import { SccmEntPagdivModule } from './ent-pagdiv/ent-pagdiv.module';
import { SccmCertPagdivModule } from './cert-pagdiv/cert-pagdiv.module';
import { SccmCorrecMonetaModule } from './correc-moneta/correc-moneta.module';
import { SccmEntrada57bModule } from './entrada-57-b/entrada-57-b.module';
import { SccmSalIni57BModule } from './sal-ini-57-b/sal-ini-57-b.module';
import { SccmCerti57BFccModule } from './certi-57-b-fcc/certi-57-b-fcc.module';
import { SccmCert57BModule } from './cert-57-b/cert-57-b.module';
import { SccmCart57BModule } from './cart-57-b/cart-57-b.module';
import { SccmSalProxPerModule } from './sal-prox-per/sal-prox-per.module';
import { SccmCliente1890Module } from './cliente-1890/cliente-1890.module';
import { SccmRut1890Module } from './rut-1890/rut-1890.module';
import { SccmAhorroModule } from './ahorro/ahorro.module';
import { SccmAhorroMovModule } from './ahorro-mov/ahorro-mov.module';
import { SccmContabilidadModule } from './contabilidad/contabilidad.module';
import { SccmEuroModule } from './euro/euro.module';
import { SccmTotalProdModule } from './total-prod/total-prod.module';
import { SccmDAPModule } from './d-ap/dap.module';
import { SccmLHBonosModule } from './l-h-bonos/lh-bonos.module';
import { SccmLHDCVModule } from './l-hdcv/lhdcv.module';
import { SccmMonedaModule } from './moneda/moneda.module';
import { SccmPactosModule } from './pactos/pactos.module';
import { SccmTablaMonedaModule } from './tabla-moneda/tabla-moneda.module';
import { SccmUfModule } from './uf/uf.module';
import { SccmUsdModule } from './usd/usd.module';
import { SccmCertVersionModule } from './cert-version/cert-version.module';
import { SccmIntRealPactosModule } from './intRealPactos/intRealPactos.module';
import { SccmCuaContPactosModule } from './cuaContPactos/cuaContPactos.module';

import { SccmUltDiaHabMesModule } from './ult-dia-hab-mes/ult-dia-hab-mes.module';
import { SccmIntRealAhorroModule } from './intRealAhorro/intRealAhorro.module';
import { SccmCuaContAhorrosModule } from './cuaContAhorros/cuaContAhorros.module';

import { SccmDapIRModule } from './dap-ir/dap-ir.module';
import { SccmDapCCModule } from './dap-cc/dap-cc.module';
import { SccmLetrasBonosModule } from './letras-bonos/letras-bonos.module';
import { SccmLetrasBonosCuadraturaModule } from './letras-bonos-cuadratura/letras-bonos-cuadratura.module';
import { SccmTotalesBonosModule } from './totales-bonos/totales-bonos.module';
import { SccmTipoAlertaModule } from './tipo-alerta/tipo-alerta.module';
import { DatosEmisionCertificadoModule } from './datos-emision-certificado/datos-emision-certificado.module';
import { SccmBonosModule } from './bonos/bonos.module';

import { SccmAprobacionModule } from './aprobacion/aprobacion.module';

import { SccmAuditServiceAccessModule } from './audit-service-access/audit-service-access.module';
import { SccmUltDiaHabAnioUltDiaHabAnioModule } from './ult-dia-hab-anio/ult-dia-hab-anio-ult-dia-hab-anio.module';
import { SccmDirectoriosModule } from './directorios/directorios.module';
import { SccmCertificadosAnterioresModule } from './certificados-anteriores/certificados-anteriores.module';
import { SccmClientes1890mantencionModule } from './clientes-1890-mantencion/clientes-1890-mantencion.module';
import { SccmTemporal1890Module } from './temporal-1890/temporal-1890.module';
import { SccmRut_tempModule } from './rut-temp/rut-temp.module';
import { SccmCabeceraControlCambioModule } from './cabecera-control-cambio/cabecera-control-cambio.module';
import { SccmDetalleControlCambioModule } from './detalle-control-cambio/detalle-control-cambio.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        SccmDatosFijosModule,
        SccmGlosaModule,
        SccmClienteBiceModule,
        SccmInstrumentoModule,
        SccmRepresentanteModule,
        SccmFutModule,
        SccmProductoModule,
        SccmSCCMUserRoleModule,
        SccmAnnoTributaModule,
        SccmEntPagdivModule,
        SccmCertPagdivModule,
        SccmCorrecMonetaModule,
        SccmEntrada57bModule,
        SccmSalIni57BModule,
        SccmCerti57BFccModule,
        SccmCert57BModule,
        SccmCart57BModule,
        SccmSalProxPerModule,
        SccmCliente1890Module,
        SccmRut1890Module,
        SccmAhorroModule,
        SccmAhorroMovModule,
        SccmContabilidadModule,
        SccmEuroModule,
        SccmTotalProdModule,
        SccmDAPModule,
        SccmLHBonosModule,
        SccmBonosModule,
        SccmLHDCVModule,
        SccmMonedaModule,
        SccmPactosModule,
        SccmTablaMonedaModule,
        SccmUfModule,
        SccmUsdModule,
        SccmCertVersionModule,
        SccmIntRealPactosModule,
        SccmCuaContPactosModule,
        SccmUltDiaHabMesModule,
        SccmIntRealAhorroModule,
        SccmCuaContAhorrosModule,
        SccmDapIRModule,
        SccmDapCCModule,
        SccmLetrasBonosModule,
        SccmLetrasBonosCuadraturaModule,
        SccmTotalesBonosModule,
        SccmTipoAlertaModule,
        DatosEmisionCertificadoModule,
        SccmAprobacionModule,
        SccmAuditServiceAccessModule,
        SccmUltDiaHabAnioUltDiaHabAnioModule,
        SccmDirectoriosModule,
        SccmCertificadosAnterioresModule,
        SccmClientes1890mantencionModule,
        SccmTemporal1890Module,
        SccmRut_tempModule,
        SccmCabeceraControlCambioModule,
        SccmDetalleControlCambioModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmEntityModule {
}
