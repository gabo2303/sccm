import { BaseEntity } from './../../shared';

export class AhorroMov implements BaseEntity {
    constructor(
        public id?: number,
        public rut?: string,
        public cuenta?: number,
        public producto?: number,
        public fechaMov?: any,
        public tipo?: string,
        public codigo?: string,
        public glosa?: string,
        public capital?: number,
        public valorUf?: number,
        public capitalUf?: number,
        public codMoneda?: number,
        public origen?: string,
    ) {
    }
}
