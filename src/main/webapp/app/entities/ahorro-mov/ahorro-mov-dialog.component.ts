import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AhorroMov } from './ahorro-mov.model';
import { AhorroMovPopupService } from './ahorro-mov-popup.service';
import { AhorroMovService } from './ahorro-mov.service';

@Component({
    selector: 'jhi-ahorro-mov-dialog',
    templateUrl: './ahorro-mov-dialog.component.html'
})
export class AhorroMovDialogComponent implements OnInit {

    ahorroMov: AhorroMov;
    isSaving: boolean;
    fechaMovDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private ahorroMovService: AhorroMovService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ahorroMov.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ahorroMovService.update(this.ahorroMov));
        } else {
            this.subscribeToSaveResponse(
                this.ahorroMovService.create(this.ahorroMov));
        }
    }

    private subscribeToSaveResponse(result: Observable<AhorroMov>) {
        result.subscribe((res: AhorroMov) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AhorroMov) {
        this.eventManager.broadcast({ name: 'ahorroMovListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-ahorro-mov-popup',
    template: ''
})
export class AhorroMovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ahorroMovPopupService: AhorroMovPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ahorroMovPopupService
                    .open(AhorroMovDialogComponent as Component, params['id']);
            } else {
                this.ahorroMovPopupService
                    .open(AhorroMovDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
