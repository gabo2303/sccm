import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AhorroMov } from './ahorro-mov.model';
import { AhorroMovPopupService } from './ahorro-mov-popup.service';
import { AhorroMovService } from './ahorro-mov.service';

@Component({
    selector: 'jhi-ahorro-mov-delete-dialog',
    templateUrl: './ahorro-mov-delete-dialog.component.html'
})
export class AhorroMovDeleteDialogComponent {

    ahorroMov: AhorroMov;

    constructor(
        private ahorroMovService: AhorroMovService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ahorroMovService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ahorroMovListModification',
                content: 'Deleted an ahorroMov'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ahorro-mov-delete-popup',
    template: ''
})
export class AhorroMovDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ahorroMovPopupService: AhorroMovPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ahorroMovPopupService
                .open(AhorroMovDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
