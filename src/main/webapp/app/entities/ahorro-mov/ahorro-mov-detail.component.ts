import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AhorroMov } from './ahorro-mov.model';
import { AhorroMovService } from './ahorro-mov.service';

@Component({
    selector: 'jhi-ahorro-mov-detail',
    templateUrl: './ahorro-mov-detail.component.html'
})
export class AhorroMovDetailComponent implements OnInit, OnDestroy {

    ahorroMov: AhorroMov;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ahorroMovService: AhorroMovService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAhorroMovs();
    }

    load(id) {
        this.ahorroMovService.find(id).subscribe((ahorroMov) => {
            this.ahorroMov = ahorroMov;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAhorroMovs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ahorroMovListModification',
            (response) => this.load(this.ahorroMov.id)
        );
    }
}
