import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AhorroMov } from './ahorro-mov.model';
import { AhorroMovService } from './ahorro-mov.service';

@Injectable()
export class AhorroMovPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private ahorroMovService: AhorroMovService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.ahorroMovService.find(id).subscribe((ahorroMov) => {
                    if (ahorroMov.fechaMov) {
                        ahorroMov.fechaMov = {
                            year: ahorroMov.fechaMov.getFullYear(),
                            month: ahorroMov.fechaMov.getMonth() + 1,
                            day: ahorroMov.fechaMov.getDate()
                        };
                    }
                    this.ngbModalRef = this.ahorroMovModalRef(component, ahorroMov);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.ahorroMovModalRef(component, new AhorroMov());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ahorroMovModalRef(component: Component, ahorroMov: AhorroMov): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ahorroMov = ahorroMov;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
