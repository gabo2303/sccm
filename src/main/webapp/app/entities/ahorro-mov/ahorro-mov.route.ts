import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AhorroMovComponent } from './ahorro-mov.component';
import { AhorroMovDetailComponent } from './ahorro-mov-detail.component';
import { AhorroMovPopupComponent } from './ahorro-mov-dialog.component';
import { AhorroMovDeletePopupComponent } from './ahorro-mov-delete-dialog.component';

export const ahorroMovRoute: Routes = [
    {
        path: 'ahorro-mov',
        component: AhorroMovComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.ahorroMov.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ahorro-mov/:id',
        component: AhorroMovDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.ahorroMov.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ahorroMovPopupRoute: Routes = [
    {
        path: 'ahorro-mov-new',
        component: AhorroMovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.ahorroMov.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ahorro-mov/:id/edit',
        component: AhorroMovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.ahorroMov.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ahorro-mov/:id/delete',
        component: AhorroMovDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.ahorroMov.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
