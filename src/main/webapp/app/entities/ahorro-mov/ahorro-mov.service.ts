import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { AhorroMov } from './ahorro-mov.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AhorroMovService {

    private resourceUrl = SERVER_API_URL + 'api/ahorro-movs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(ahorroMov: AhorroMov): Observable<AhorroMov> {
        const copy = this.convert(ahorroMov);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ahorroMov: AhorroMov): Observable<AhorroMov> {
        const copy = this.convert(ahorroMov);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AhorroMov> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AhorroMov.
     */
    private convertItemFromServer(json: any): AhorroMov {
        const entity: AhorroMov = Object.assign(new AhorroMov(), json);
        entity.fechaMov = this.dateUtils
            .convertLocalDateFromServer(json.fechaMov);
        return entity;
    }

    /**
     * Convert a AhorroMov to a JSON which can be sent to the server.
     */
    private convert(ahorroMov: AhorroMov): AhorroMov {
        const copy: AhorroMov = Object.assign({}, ahorroMov);
        copy.fechaMov = this.dateUtils
            .convertLocalDateToServer(ahorroMov.fechaMov);
        return copy;
    }
}
