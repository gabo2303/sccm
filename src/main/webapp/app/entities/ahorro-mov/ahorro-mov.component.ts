import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { AhorroMov } from './ahorro-mov.model';
import { AhorroMovService } from './ahorro-mov.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ahorro-mov',
    templateUrl: './ahorro-mov.component.html'
})
export class AhorroMovComponent implements OnInit, OnDestroy {
ahorroMovs: AhorroMov[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ahorroMovService: AhorroMovService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.ahorroMovService.query().subscribe(
            (res: ResponseWrapper) => {
                this.ahorroMovs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAhorroMovs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AhorroMov) {
        return item.id;
    }
    registerChangeInAhorroMovs() {
        this.eventSubscriber = this.eventManager.subscribe('ahorroMovListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
