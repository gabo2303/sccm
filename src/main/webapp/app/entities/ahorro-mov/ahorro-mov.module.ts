import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    AhorroMovService,
    AhorroMovPopupService,
    AhorroMovComponent,
    AhorroMovDetailComponent,
    AhorroMovDialogComponent,
    AhorroMovPopupComponent,
    AhorroMovDeletePopupComponent,
    AhorroMovDeleteDialogComponent,
    ahorroMovRoute,
    ahorroMovPopupRoute,
} from './';

const ENTITY_STATES = [
    ...ahorroMovRoute,
    ...ahorroMovPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AhorroMovComponent,
        AhorroMovDetailComponent,
        AhorroMovDialogComponent,
        AhorroMovDeleteDialogComponent,
        AhorroMovPopupComponent,
        AhorroMovDeletePopupComponent,
    ],
    entryComponents: [
        AhorroMovComponent,
        AhorroMovDialogComponent,
        AhorroMovPopupComponent,
        AhorroMovDeleteDialogComponent,
        AhorroMovDeletePopupComponent,
    ],
    providers: [
        AhorroMovService,
        AhorroMovPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmAhorroMovModule {}
