import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DatosFijos } from './datos-fijos.model';
import { DatosFijosPopupService } from './datos-fijos-popup.service';
import { DatosFijosService } from './datos-fijos.service';

@Component({
    selector: 'jhi-datos-fijos-dialog',
    templateUrl: './datos-fijos-dialog.component.html'
})
export class DatosFijosDialogComponent implements OnInit {

    datosFijos: DatosFijos;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private datosFijosService: DatosFijosService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.datosFijos.id !== undefined) {
            this.subscribeToSaveResponse(
                this.datosFijosService.update(this.datosFijos));
        } else {
            this.subscribeToSaveResponse(
                this.datosFijosService.create(this.datosFijos));
        }
    }

    private subscribeToSaveResponse(result: Observable<DatosFijos>) {
        result.subscribe((res: DatosFijos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DatosFijos) {
        this.eventManager.broadcast({ name: 'datosFijosListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-datos-fijos-popup',
    template: ''
})
export class DatosFijosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private datosFijosPopupService: DatosFijosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.datosFijosPopupService
                    .open(DatosFijosDialogComponent as Component, params['id']);
            } else {
                this.datosFijosPopupService
                    .open(DatosFijosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
