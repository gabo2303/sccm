import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { DatosFijos } from './datos-fijos.model';
import { DatosFijosService } from './datos-fijos.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-datos-fijos',
    templateUrl: './datos-fijos.component.html'
})
export class DatosFijosComponent implements OnInit, OnDestroy {
    datosFijos: DatosFijos[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private datosFijosService: DatosFijosService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.datosFijosService.query().subscribe(
            (res: ResponseWrapper) => {
                this.datosFijos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDatosFijos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DatosFijos) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
		console.log("CONTENT ");
		console.log(contentType);
		console.log("FIELD ");
		console.log(field);
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInDatosFijos() {
        this.eventSubscriber = this.eventManager.subscribe('datosFijosListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

