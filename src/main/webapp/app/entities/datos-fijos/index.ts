export * from './datos-fijos.model';
export * from './datos-fijos-popup.service';
export * from './datos-fijos.service';
export * from './datos-fijos-dialog.component';
export * from './datos-fijos-delete-dialog.component';
export * from './datos-fijos-detail.component';
export * from './datos-fijos.component';
export * from './datos-fijos.route';
