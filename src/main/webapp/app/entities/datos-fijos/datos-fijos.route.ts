import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { DatosFijosComponent } from './datos-fijos.component';
import { DatosFijosDetailComponent } from './datos-fijos-detail.component';
import { DatosFijosPopupComponent } from './datos-fijos-dialog.component';
import { DatosFijosDeletePopupComponent } from './datos-fijos-delete-dialog.component';

export const datosFijosRoute: Routes = [
    {
        path: 'datos-fijos',
        component: DatosFijosComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.datosFijos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }, {
        path: 'datos-fijos/:id',
        component: DatosFijosDetailComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.datosFijos.home.title'
        },
        canActivate: [ UserRouteAccessService ]
    }
];

export const datosFijosPopupRoute: Routes = [
    {
        path: 'datos-fijos-new',
        component: DatosFijosPopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.datosFijos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'datos-fijos/:id/edit',
        component: DatosFijosPopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.datosFijos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    },
    {
        path: 'datos-fijos/:id/delete',
        component: DatosFijosDeletePopupComponent,
        data: {
            authorities: [ 'ROLE_ADMIN' ],
            pageTitle: 'sccmApp.datosFijos.home.title'
        },
        canActivate: [ UserRouteAccessService ],
        outlet: 'popup'
    }
];
