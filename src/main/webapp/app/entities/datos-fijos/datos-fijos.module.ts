import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    DatosFijosService,
    DatosFijosPopupService,
    DatosFijosComponent,
    DatosFijosDetailComponent,
    DatosFijosDialogComponent,
    DatosFijosPopupComponent,
    DatosFijosDeletePopupComponent,
    DatosFijosDeleteDialogComponent,
    datosFijosRoute,
    datosFijosPopupRoute,
} from './';

const ENTITY_STATES = [
    ...datosFijosRoute,
    ...datosFijosPopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DatosFijosComponent,
        DatosFijosDetailComponent,
        DatosFijosDialogComponent,
        DatosFijosDeleteDialogComponent,
        DatosFijosPopupComponent,
        DatosFijosDeletePopupComponent,
    ],
    entryComponents: [
        DatosFijosComponent,
        DatosFijosDialogComponent,
        DatosFijosPopupComponent,
        DatosFijosDeleteDialogComponent,
        DatosFijosDeletePopupComponent,
    ],
    providers: [
        DatosFijosService,
        DatosFijosPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmDatosFijosModule {}
