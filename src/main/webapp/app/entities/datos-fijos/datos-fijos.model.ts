import { BaseEntity } from './../../shared';

export class DatosFijos implements BaseEntity {
    constructor(
        public id?: number,
        public nombreBanco?: string,
        public rutBanco?: string,
        public direccion?: string,
        public giro?: string,
        public tipoSocAnonima?: string,
        public transaccionBolsa?: boolean,
        public imagenContentType?: string,
        public imagen?: any,
        public rutaExportacion?: string,
    ) {
        this.transaccionBolsa = false;
    }
}
