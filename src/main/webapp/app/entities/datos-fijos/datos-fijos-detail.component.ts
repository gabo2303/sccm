import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DatosFijos } from './datos-fijos.model';
import { DatosFijosService } from './datos-fijos.service';

@Component({
    selector: 'jhi-datos-fijos-detail',
    templateUrl: './datos-fijos-detail.component.html'
})
export class DatosFijosDetailComponent implements OnInit, OnDestroy {

    datosFijos: DatosFijos;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private datosFijosService: DatosFijosService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDatosFijos();
    }

    load(id) {
        this.datosFijosService.find(id).subscribe((datosFijos) => {
            this.datosFijos = datosFijos;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDatosFijos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'datosFijosListModification',
            (response) => this.load(this.datosFijos.id)
        );
    }
}
