import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DatosFijos } from './datos-fijos.model';
import { DatosFijosPopupService } from './datos-fijos-popup.service';
import { DatosFijosService } from './datos-fijos.service';

@Component({
    selector: 'jhi-datos-fijos-delete-dialog',
    templateUrl: './datos-fijos-delete-dialog.component.html'
})
export class DatosFijosDeleteDialogComponent {

    datosFijos: DatosFijos;

    constructor(
        private datosFijosService: DatosFijosService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.datosFijosService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'datosFijosListModification',
                content: 'Deleted an datosFijos'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-datos-fijos-delete-popup',
    template: ''
})
export class DatosFijosDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private datosFijosPopupService: DatosFijosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.datosFijosPopupService
                .open(DatosFijosDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
