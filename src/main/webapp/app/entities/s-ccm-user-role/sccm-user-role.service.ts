import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { SCCMUserRole } from './sccm-user-role.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SCCMUserRoleService {

    private resourceUrl = SERVER_API_URL + 'api/s-ccm-user-roles';

    constructor(private http: Http) { }

    create(sCCMUserRole: SCCMUserRole): Observable<SCCMUserRole> {
        const copy = this.convert(sCCMUserRole);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(sCCMUserRole: SCCMUserRole): Observable<SCCMUserRole> {
        const copy = this.convert(sCCMUserRole);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<SCCMUserRole> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SCCMUserRole.
     */
    private convertItemFromServer(json: any): SCCMUserRole {
        const entity: SCCMUserRole = Object.assign(new SCCMUserRole(), json);
        return entity;
    }

    /**
     * Convert a SCCMUserRole to a JSON which can be sent to the server.
     */
    private convert(sCCMUserRole: SCCMUserRole): SCCMUserRole {
        const copy: SCCMUserRole = Object.assign({}, sCCMUserRole);
        return copy;
    }
}
