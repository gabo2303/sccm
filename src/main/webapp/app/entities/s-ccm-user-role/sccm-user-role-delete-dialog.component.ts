import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SCCMUserRole } from './sccm-user-role.model';
import { SCCMUserRolePopupService } from './sccm-user-role-popup.service';
import { SCCMUserRoleService } from './sccm-user-role.service';

@Component({
    selector: 'jhi-sccm-user-role-delete-dialog',
    templateUrl: './sccm-user-role-delete-dialog.component.html'
})
export class SCCMUserRoleDeleteDialogComponent {

    sCCMUserRole: SCCMUserRole;

    constructor(
        private sCCMUserRoleService: SCCMUserRoleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sCCMUserRoleService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sCCMUserRoleListModification',
                content: 'Deleted an sCCMUserRole'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sccm-user-role-delete-popup',
    template: ''
})
export class SCCMUserRoleDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sCCMUserRolePopupService: SCCMUserRolePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sCCMUserRolePopupService
                .open(SCCMUserRoleDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
