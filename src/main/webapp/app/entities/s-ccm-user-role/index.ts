export * from './sccm-user-role.model';
export * from './sccm-user-role-popup.service';
export * from './sccm-user-role.service';
export * from './sccm-user-role-dialog.component';
export * from './sccm-user-role-delete-dialog.component';
export * from './sccm-user-role-detail.component';
export * from './sccm-user-role.component';
export * from './sccm-user-role.route';
