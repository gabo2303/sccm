import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SCCMUserRole } from './sccm-user-role.model';
import { SCCMUserRoleService } from './sccm-user-role.service';

@Component({
    selector: 'jhi-sccm-user-role-detail',
    templateUrl: './sccm-user-role-detail.component.html'
})
export class SCCMUserRoleDetailComponent implements OnInit, OnDestroy {

    sCCMUserRole: SCCMUserRole;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private sCCMUserRoleService: SCCMUserRoleService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSCCMUserRoles();
    }

    load(id) {
        this.sCCMUserRoleService.find(id).subscribe((sCCMUserRole) => {
            this.sCCMUserRole = sCCMUserRole;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSCCMUserRoles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sCCMUserRoleListModification',
            (response) => this.load(this.sCCMUserRole.id)
        );
    }
}
