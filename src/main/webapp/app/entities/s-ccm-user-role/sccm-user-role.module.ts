import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SccmSharedModule } from '../../shared';
import {
    SCCMUserRoleService,
    SCCMUserRolePopupService,
    SCCMUserRoleComponent,
    SCCMUserRoleDetailComponent,
    SCCMUserRoleDialogComponent,
    SCCMUserRolePopupComponent,
    SCCMUserRoleDeletePopupComponent,
    SCCMUserRoleDeleteDialogComponent,
    sCCMUserRoleRoute,
    sCCMUserRolePopupRoute,
} from './';

const ENTITY_STATES = [
    ...sCCMUserRoleRoute,
    ...sCCMUserRolePopupRoute,
];

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SCCMUserRoleComponent,
        SCCMUserRoleDetailComponent,
        SCCMUserRoleDialogComponent,
        SCCMUserRoleDeleteDialogComponent,
        SCCMUserRolePopupComponent,
        SCCMUserRoleDeletePopupComponent,
    ],
    entryComponents: [
        SCCMUserRoleComponent,
        SCCMUserRoleDialogComponent,
        SCCMUserRolePopupComponent,
        SCCMUserRoleDeleteDialogComponent,
        SCCMUserRoleDeletePopupComponent,
    ],
    providers: [
        SCCMUserRoleService,
        SCCMUserRolePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SccmSCCMUserRoleModule {}
