import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SCCMUserRole } from './sccm-user-role.model';
import { SCCMUserRoleService } from './sccm-user-role.service';

@Injectable()
export class SCCMUserRolePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private sCCMUserRoleService: SCCMUserRoleService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.sCCMUserRoleService.find(id).subscribe((sCCMUserRole) => {
                    this.ngbModalRef = this.sCCMUserRoleModalRef(component, sCCMUserRole);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.sCCMUserRoleModalRef(component, new SCCMUserRole());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    sCCMUserRoleModalRef(component: Component, sCCMUserRole: SCCMUserRole): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.sCCMUserRole = sCCMUserRole;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
