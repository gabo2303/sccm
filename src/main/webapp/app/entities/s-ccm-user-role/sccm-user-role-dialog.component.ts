import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SCCMUserRole } from './sccm-user-role.model';
import { SCCMUserRolePopupService } from './sccm-user-role-popup.service';
import { SCCMUserRoleService } from './sccm-user-role.service';

@Component({
    selector: 'jhi-sccm-user-role-dialog',
    templateUrl: './sccm-user-role-dialog.component.html'
})
export class SCCMUserRoleDialogComponent implements OnInit {

    sCCMUserRole: SCCMUserRole;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private sCCMUserRoleService: SCCMUserRoleService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sCCMUserRole.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sCCMUserRoleService.update(this.sCCMUserRole));
        } else {
            this.subscribeToSaveResponse(
                this.sCCMUserRoleService.create(this.sCCMUserRole));
        }
    }

    private subscribeToSaveResponse(result: Observable<SCCMUserRole>) {
        result.subscribe((res: SCCMUserRole) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SCCMUserRole) {
        this.eventManager.broadcast({ name: 'sCCMUserRoleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-sccm-user-role-popup',
    template: ''
})
export class SCCMUserRolePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sCCMUserRolePopupService: SCCMUserRolePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sCCMUserRolePopupService
                    .open(SCCMUserRoleDialogComponent as Component, params['id']);
            } else {
                this.sCCMUserRolePopupService
                    .open(SCCMUserRoleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
