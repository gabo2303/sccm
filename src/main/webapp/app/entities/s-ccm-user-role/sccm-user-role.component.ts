import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { SCCMUserRole } from './sccm-user-role.model';
import { SCCMUserRoleService } from './sccm-user-role.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sccm-user-role',
    templateUrl: './sccm-user-role.component.html'
})
export class SCCMUserRoleComponent implements OnInit, OnDestroy {
sCCMUserRoles: SCCMUserRole[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private sCCMUserRoleService: SCCMUserRoleService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.sCCMUserRoleService.query().subscribe(
            (res: ResponseWrapper) => {
                this.sCCMUserRoles = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSCCMUserRoles();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SCCMUserRole) {
        return item.id;
    }
    registerChangeInSCCMUserRoles() {
        this.eventSubscriber = this.eventManager.subscribe('sCCMUserRoleListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
