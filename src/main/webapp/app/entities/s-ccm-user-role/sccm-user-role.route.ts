import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SCCMUserRoleComponent } from './sccm-user-role.component';
import { SCCMUserRoleDetailComponent } from './sccm-user-role-detail.component';
import { SCCMUserRolePopupComponent } from './sccm-user-role-dialog.component';
import { SCCMUserRoleDeletePopupComponent } from './sccm-user-role-delete-dialog.component';

export const sCCMUserRoleRoute: Routes = [
    {
        path: 'sccm-user-role',
        component: SCCMUserRoleComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.sCCMUserRole.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sccm-user-role/:id',
        component: SCCMUserRoleDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.sCCMUserRole.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sCCMUserRolePopupRoute: Routes = [
    {
        path: 'sccm-user-role-new',
        component: SCCMUserRolePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.sCCMUserRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sccm-user-role/:id/edit',
        component: SCCMUserRolePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.sCCMUserRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sccm-user-role/:id/delete',
        component: SCCMUserRoleDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sccmApp.sCCMUserRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
