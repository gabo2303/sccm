import { BaseEntity } from './../../shared';

export const enum SCCMRole {
    'ROLE_SCCM_1941_ADMIN',
    'ROLE_SCCM_1941_USER',
    'ROLE_SCCM_1944_ADMIN',
    'ROLE_SCCM_1944_USER',
    'ROLE_SCCM_1890_ADMIN',
    'ROLE_SCCM_1890_USER',
    'ROLE_SCCM_ADMIN'
}

export class SCCMUserRole implements BaseEntity {
    constructor(
        public id?: number,
        public sccmSystemUser?: number,
        public sccmRole?: SCCMRole,
    ) {
    }
}
