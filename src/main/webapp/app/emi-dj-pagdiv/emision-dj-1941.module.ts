import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EMISION_CERTIFICADOS_ROUTE, EmisionDj1941Component } from './';
import { EmisionCertificadosService } from './emision-dj-1941.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmisionDj1941Component,
    ],
    entryComponents: [],
    providers: [
        EmisionCertificadosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppEmisionDj1941Module {
}
