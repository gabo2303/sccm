import { Route } from '@angular/router';
import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmisionDj1941Component } from './';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emision-dj-1941',
    component: EmisionDj1941Component,
    data: {
        authorities: [ 'ROLE_SCCM_1941_ADMIN', 'ROLE_SCCM_1941_USER' ],
        pageTitle: 'sccmApp.emision-certificados.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
