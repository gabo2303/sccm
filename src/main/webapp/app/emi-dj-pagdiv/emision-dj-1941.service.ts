import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../app.constants';
import { CommonServices } from '../shared';

@Injectable()
export class EmisionCertificadosService 
{
	private resourceUrl = SERVER_API_URL + 'api/storage';
	
    constructor(private http: HttpClient, private commonServices: CommonServices) {  }

    emiteDJ(idAnnoSelected: string): Observable<HttpEvent<{}>> {
        
		const formdata: FormData = new FormData();
        formdata.append('anno', idAnnoSelected);
        let headers = this.commonServices.generateAuthHeader();
        
		const req = new HttpRequest('POST', this.resourceUrl + '/emiteDJ', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }

    listaDJs(): Observable<HttpEvent<{}>> {
		
        const formdata: FormData = new FormData();
        let headers = this.commonServices.generateAuthHeader();
        
		const req = new HttpRequest('POST', this.resourceUrl + '/listaCertificados', formdata, {
            headers: headers,
            reportProgress: true,
            responseType: 'text'
        });
		
        return this.http.request(req);
    }
}