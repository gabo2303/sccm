import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PasswordComponent } from './password.component';
import {
    ADMIN,
    SCCM_1890_ADMIN,
    SCCM_1890_ADMIN_AHORRO,
    SCCM_1890_ADMIN_BYL,
    SCCM_1890_ADMIN_DAP,
    SCCM_1890_ADMIN_PACTOS,
    SCCM_1890_USER_AHORRO,
    SCCM_1890_USER_BYL,
    SCCM_1890_USER_DAP,
    SCCM_1890_USER_PACTOS,
    SCCM_1941_ADMIN,
    SCCM_1941_USER,
    SCCM_1944_ADMIN,
    SCCM_1944_USER,
    USER
} from '../../app.constants';

export const passwordRoute: Route = {
    path: 'password',
    component: PasswordComponent,
    data: {
        authorities: [ ADMIN, USER, SCCM_1890_ADMIN, SCCM_1890_ADMIN_AHORRO, SCCM_1890_USER_AHORRO, SCCM_1890_ADMIN_DAP, SCCM_1890_USER_DAP, SCCM_1890_ADMIN_BYL,
            SCCM_1890_USER_BYL, SCCM_1890_ADMIN_PACTOS, SCCM_1890_USER_PACTOS, SCCM_1941_ADMIN, SCCM_1941_USER, SCCM_1944_ADMIN, SCCM_1944_USER ],
        pageTitle: 'global.menu.account.password'
    },
    canActivate: [ UserRouteAccessService ]
};
