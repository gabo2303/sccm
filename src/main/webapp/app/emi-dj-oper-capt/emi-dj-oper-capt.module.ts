import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { EmiDjOperCaptComponent, EMISION_CERTIFICADOS_ROUTE } from './';
import { EmisionCertificadosService } from './emi-dj-oper-capt.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ EMISION_CERTIFICADOS_ROUTE ], { useHash: true })
    ],
    declarations: [
        EmiDjOperCaptComponent,
    ],
    entryComponents: [],
    providers: [
        EmisionCertificadosService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class EmiDjOperCaptModule {
}
