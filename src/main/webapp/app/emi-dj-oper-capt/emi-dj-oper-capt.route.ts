import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EmiDjOperCaptComponent } from './';
import { SCCM_1890_ADMIN } from '../app.constants';

export const EMISION_CERTIFICADOS_ROUTE: Route = {
    path: 'emi-dj-oper-capt',
    component: EmiDjOperCaptComponent,
    data: {
        authorities: [ SCCM_1890_ADMIN ],
        pageTitle: 'sccmApp.emision-certificados.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
