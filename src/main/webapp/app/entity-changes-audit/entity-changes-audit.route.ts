import { Route } from '@angular/router';
import { UserRouteAccessService } from '../shared/auth/user-route-access-service';
import { EntityChangesAuditComponent } from './entity-changes-audit.component';

export const ENTITY_CHANGES_AUDIT_ROUTE: Route = {
    path: 'entity-changes-audit',
    component: EntityChangesAuditComponent,
    data: {
        authorities: [ 'ROLE_ADMIN' ],
        pageTitle: 'sccmApp.entity-changes-audit.home.title'
    },
    canActivate: [ UserRouteAccessService ]
};
