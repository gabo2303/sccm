import { Component, OnInit } from '@angular/core';
import { EntityChangesAuditService } from './entity-changes-audit.service';
import { JhiAlertService } from 'ng-jhipster';
import { ResponseWrapper } from '../shared/model/response-wrapper.model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { TABLE_NAMES_AUDIT } from '../shared';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-emision-certificados',
    templateUrl: './entity-changes-audit.component.html',
    styleUrls: [
        'entity-changes-audit.css'
    ]
})
export class EntityChangesAuditComponent implements OnInit {

    tableNames: { displayName: string, value: string }[] = TABLE_NAMES_AUDIT;
    startDate: any;
    endDate: any;
    tableName: string;
    entityChangesAudit: any[];
    entityChangesAuditFlag = false;
    entityChangesAuditFlagSinData = false;
    columnNames: string[];

    constructor(private entityChangesAuditService: EntityChangesAuditService, private alertService: JhiAlertService, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
    }

    loadAllByTableName(tableNameSelected: any) {
        this.tableName = tableNameSelected.value;
        if ((!isNullOrUndefined(this.startDate) && isNullOrUndefined(this.endDate)) || (isNullOrUndefined(this.startDate) && !isNullOrUndefined(this.endDate))) {
            const errorMsg = { message: 'error.validateStartAndEndDates' };
            this.onError(errorMsg);
        } else if (!isNullOrUndefined(this.startDate) && !isNullOrUndefined(this.endDate) &&
            new Date(this.startDate.year + '-' + this.startDate.month + '-' + this.startDate.day) >
            new Date(this.endDate.year + '-' + this.endDate.month + '-' + this.endDate.day)) {

            const errorMsg = { message: 'error.validateStartDateAfterEndDate' };
            this.onError(errorMsg);
        } else {
            this.loadAll();
        }
    }

    loadAll() {
        this.entityChangesAuditFlag = false;
        this.entityChangesAuditFlagSinData = false;
        this.entityChangesAuditService.findAll(this.tableName, this.startDate, this.endDate).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccess(data) {
        this.entityChangesAudit = data;
        if (this.entityChangesAudit.length > 0) {
            this.entityChangesAuditFlag = true;
            this.columnNames = [];
            for (let obj of this.entityChangesAudit) {
                if (this.columnNames.length <= 0) {
                    for (let key in obj) {
                        this.columnNames.push(key);
                    }
                }
            }
        } else {
            this.entityChangesAuditFlagSinData = true;
        }
        this.spinnerService.hide();
    }

    private onError(error) {
        this.spinnerService.hide();
        this.alertService.error(error.message, null, null);
    }

    clearDates() {
        this.startDate = null;
        this.endDate = null;
        this.entityChangesAudit = [];
        this.entityChangesAuditFlag = false;
        this.entityChangesAuditFlagSinData = false;
    }

}
