import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { createRequestOption, ResponseWrapper } from '../shared';
import { Http, Response, URLSearchParams } from '@angular/http';
import { SERVER_API_URL } from '../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class EntityChangesAuditService {

    private resourceUrl = SERVER_API_URL + 'api/entity-changes-audit';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    findAll(tableName: string, startDate?: any, endDate?: any, req?: any): Observable<ResponseWrapper> {
        const options: any = createRequestOption(req);
        /*const params: URLSearchParams = new URLSearchParams();
        params.set('startDate', startDate);
        params.set('endDate', endDate);

        const options = {
            search: params
        };*/
        let url = `${this.resourceUrl}/findByTableName?tableName=${tableName}`;
        if (startDate && endDate) {
            url = `${this.resourceUrl}/findByTableNameAndDates?tableName=${tableName}`;
            // url += `&startDate=${startDate}`;
            // url += `&endDate=${endDate}`;
            url += `&startDate=${this.dateUtils.convertLocalDateToServer(startDate)}`;
            url += `&endDate=${this.dateUtils.convertLocalDateToServer(endDate)}`;
        }
        return this.http.get(url, options).map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(jsonResponse[ i ]);
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
}
