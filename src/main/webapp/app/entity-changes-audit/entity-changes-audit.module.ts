import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SccmSharedModule } from '../shared';
import { ENTITY_CHANGES_AUDIT_ROUTE, EntityChangesAuditComponent } from './';
import { EntityChangesAuditService } from './entity-changes-audit.service';

@NgModule({
    imports: [
        SccmSharedModule,
        RouterModule.forRoot([ ENTITY_CHANGES_AUDIT_ROUTE ], { useHash: true })
    ],
    declarations: [
        EntityChangesAuditComponent,
    ],
    entryComponents: [],
    providers: [
        EntityChangesAuditService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SccmAppEntityChangesAuditModule {
}
